---
title: Portada
bottom_card_title: "¿Quienes Somos?"
bottom_card_body: "Cuerpo y territorio es una revista digital que reúne a pensadorxs vinculados a diversas disciplinas con el fin de servir como medio para expresar ideas, testimonios, relatos, problemáticas e historias inspiradoras enmarcadas en temáticas sociales que visibilicen realidades y formas de vida."
---