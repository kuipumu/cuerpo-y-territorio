---
title: Comunidad migrante senegalesa
cover: images/uploads/comunidad-migrante-senegalesa.jpg
caption: "Fuente: El grito del sur"
date: 2020-08-10 00:00:00
authors:
  - Florencia Cencig
sticky: false
comments: true
subtitle: Mirarnos con ojos de hermandad
tags:
  - Migración
  - Argentina
  - Senegal
  - Derechos
  - Viajeros
  - Remesas
  - Legalidad
categories:
  - Cuerpo
description: Las personas transitan a lo largo de toda su vida, diversos
  procesos que van conformando sus subjetividades e identidades. Cuando esto
  implica alejarse de su lugar de origen, de sus raíces y de todo aquello que lo
  construye como un ser social perteneciente a una determinada cultura.
---
Las personas transitan a lo largo de toda su vida, diversos procesos que van conformando sus subjetividades e identidades. Cuando esto implica alejarse de su lugar de origen, de sus raíces y de todo aquello que lo construye como un ser social perteneciente a una determinada cultura, se producen transformaciones en la vida misma que provocan duelos, rupturas y desarraigo, como también, pueden generarse oportunidades, mejores condiciones de vida y nuevos proyectos. 

Migrar no significa, solamente, desplazarse del lugar de origen a otro nuevo. No se reduce a un hecho físico y geográfico. Es un fenómeno que conlleva una gran cantidad de significados y dimensiones complejas que involucran diversos cambios. 

En el caso de la comunidad migrante senegalesa en Argentina, podría mencionar varias problemáticas que atraviesan, siendo alguna de ellas: el racismo, la discriminación y la violencia institucional. Pero en esta oportunidad, me parece interesante ahondar en otros  aspectos que forman parte de los trayectos de vida de dicho colectivo en suelo extranjero.

Muchos de nuestros hermanos senegaleses, a los cuales he entrevistado, manifestaron que adaptarse y comprender las formas y dinámicas familiares características de Argentina y, por otro lado, poder sostener ciertos rituales y costumbres típicas de su cultura, implican procesos largos y complicados.

En cuanto a la composición familiar en Senegal, en su mayoría, son familias extensas, las cuales involucran hogares donde conviven tres generaciones. A su vez, son patrilocales y los matrimonios suelen ser polígamos, donde solo los hombres cuentan con la posibilidad de tener hasta cuatro esposas. Por lo que, es un gran cambio el que atraviesan, ya que suelen llegar solos y se reagrupan con otros senegaleses que se encuentran en la misma situación, siendo en su mayoría hombres.

En función a los roles y las relaciones de poder, desde varios autores que trabajan la temática y desde la propia voz de los migrantes entrevistados, refieren que su sociedad es sumamente machista y desigual en términos de género. Sin embargo, en el hogar senegalés el significado de cada integrante dentro de la estructura familiar tiene igual valor. Es decir, la importancia se basa en la unidad en sí, la familia como un todo. Es por ello que, cuando un pariente migra a otro país,  aparece la figura de la familia transnacional, manteniendo la unión y el afecto, más allá de la desterritorialización que sufren. Cumpliendo un rol fundamental de solidaridad la figura de las remesas.[^1]

A partir de un proceso de transculturación, los migrantes continúan reproduciendo durante su vida cotidiana, rituales característicos de su origen. Por ejemplo, el momento del almuerzo o de la cena se convierte en un espacio esencial, el cual implica compartir, acompañarse, y recordar su tradición familiar. Dicho esto, se vislumbra la necesidad de llevar a cabo esta práctica colectiva junto a otros compatriotas con los que conviven aquí. 

Lo mismo ocurre con la religiosidad; en su mayoría son musulmanes, de modo que, además de la importancia que tiene rezar cada día, la figura de la Mezquita[^2], como también de las Cofradías[^3], cumplen un rol privilegiado de cohesión social, ya que les otorga un sentido de pertenencia. Es allí donde se encuentran y reivindican su identidad, a través del culto y la fe islámica.

En cuanto a sus proyectos de reagrupación familiar, aquellos que tienen matrimonio polígamo piensan en ahorrar dinero y volver a su tierra natal y, generalmente, los que tienen matrimonio monógamo buscan como objetivo traer a su familia a Argentina. Siendo esto sumamente difícil ya que, por falta de políticas migratorias y por la inexistencia de un Consulado Argentino en Senegal, es muy complejo llevar adelante procesos migratorios legales.

Venir de un país perteneciente al continente africano, con el cual hay enormes diferencias en cuanto al idioma, la etnia, la religión y la cultura en sí misma, involucra pasar por muchas situaciones. En primer lugar, por su condición de inmigrante. En segundo lugar, por su condición de negro, en un país que mediante la invasión y el genocidio construyó sus bases sociales colocando, erróneamente, al “blanco” como hombre americano. Y, en tercer lugar, por las diferencias propias de cada cultura. Lo que demuestra, injustamente, que ser un inmigrante, en muchas ocasiones, implica convertirse en objeto de estigmatizaciones y prejuicios. Y, a la vez, ser un inmigrante negro no es lo mismo, para el imaginario social, que ser un inmigrante blanco o europeo.

Dicho esto, es urgente construir procesos que nos permitan, en términos de Rauber (2018), la descolonización de la subjetividad. Y creo que para romper con las subjetividades descalificantes es imprescindible mirar con ojos de familia. Quiero decir, mirarnos como hermanos. Entender que en ese Otro distinto a mí; hay un padre, hay una hija, hay una madre, hay un amigo lejos de su casa. En medio de este sistema capitalista macabro, el cual nos lleva a deshumanizar, fragmentar, y hasta romper los lazos sociales, es preciso generar procesos que construyan ciudadanías que involucren relaciones sociales inclusivas.

Por ello, conocer a las diferentes comunidades que eligen habitar nuestro país, es reconocernos como parte de una identidad intercultural. Migrar es un derecho humano y, visibilizar y respetar las diversidades étnicas y culturales, da lugar a promover sociedades independientes, emancipadoras y liberadoras. Como también, permite acompañar los cambios y las transformaciones de las estructuras sociales propias de la realidad dinámica, de manera que se respeten los principios humanísticos y democráticos.

Entender que en la diversidad cultural y en el respeto por todas las colectividades se generan pueblos integrales es, siguiendo los conceptos de Walsh (2005), desarrollar interacciones equitativas entre conocimientos y pueblos culturalmente diferentes, e impulsar espacios de encuentros entre seres, sentidos y saberes distintos. Continuando esta idea, confeccionar políticas públicas y lógicas sociales basadas en el enfoque intercultural, producen mecanismos que rompen con la hegemonía eurocéntrica y reconocen a las minorías con el mismo valor que se le otorga al resto de las comunidades.

Además de la absoluta obligación que tienen los estados y los gobiernos, es nuestra responsabilidad como ciudadanas y ciudadanos, involucrarnos con las vivencias y las problemáticas que atraviesan los colectivos migrantes y ganar terreno, todxs juntxs, en materia de derechos humanos. 

En esta oportunidad, adentrarse a la dimensión familiar de los senegaleses y conocer sus vivencias, dio lugar a descubrir sus orígenes y, así, identificarlos como parte de un todo colectivo. Por eso, estoy convencida que, la pluralidad y el respeto hacia lxs otrxs, es lo que nos habilita a generar procesos transformadores que permiten mirarnos como hermanxs de una misma humanidad y, de esta forma, acercarnos un poco más, a la tan ansiada justicia social.

[^1]: Ganancias que los inmigrantes envían a su país de origen.
[^2]: Lugar de culto para los seguidores de la fe islámica.
[^3]: Modo de agruparse a través de la hermandad siguiendo ciertas prácticas y creencias.