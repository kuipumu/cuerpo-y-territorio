---
title: Compro, uso y descarto
subtitle: Breve recorrido por el camino de nuestros residuos
cover: /images/uploads/foto_1-portada.jpg
date: 2020-10-01T01:03:26.986Z
authors:
  - Iris Mendizábal
tags:
  - Planeta
  - Residuos
  - Basura
  - Conciencia
  - Ambiente
  - Contaminación
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

> *Cuando nada nos quede, ni los árboles, ni las flores, ni el agua limpia; cuando la belleza haya desaparecido para siempre de este mundo doliente y los humanos nos sintamos solos y arrepentidos por tanto daño, todavía tendremos un as bajo la manga; algo que comer, un paisaje que mirar : nuestras inmensas montañas de basura.*

Hace falta una retórica de “aquellas” para abordar el tema que pretendo visibilizar sin perder la elegancia, por lo cual sopesé las opciones cuidadosamente. La conclusión a la que arribé fue que ni la retórica ni la elegancia constituyen cualidades que ostente por exceso; así que acá estoy, sin mucho que perder, desnuda de metáforas y vacía de artilugios literarios, dispuesta a escribir sobre basura. La tuya, la mía, la del vecino. Basura. Así, a secas.

Elegí este tema porque, como con muchos otros, pienso que estamos con los ojos cerrados, mirando para otro lado o ciegos de tanto ver, no lo sé muy bien. Lo que sí parece cierto, es que cuando los ojos no observan, el corazón no es capaz de sentir. (¿Acaso no pregona esa máxima una gastadísima frase que todos conocemos?)

Cuesta abrir los ojos, porque duele lo que hay que ver. Se estima que sólo en la República Argentina cada persona produce un kilo de residuos diarios. Así, el país produce más de 40 millones de kilos por día y más de 480 millones de kilos de basura por año.

Cifras que molestan, ¿no? Quizás lo más terrible no sean las cifras, sino el hecho de saber que la basura no desaparece. Los números son acumulativos. Consumimos y generamos residuos a un ritmo indigestible para los sistemas de procesamiento de los mismos. Pienso, por momentos, ¿a dónde fue a parar el kilo de basura que produje ayer?. ¿Andará rodando por algún misterioso espacio, presto a desaparecer en las próximas horas? ¿Qué hizo mi kilo de basura mientras yo me lavaba las manos y sonreía estúpidamente al espejo?

Hace dos meses decidí abrir los ojos y tomé un curso de gestión de residuos sólidos. Sí. Un curso entero sólo para saber qué pasa con mi basura, a dónde va, qué hacen con ella, dónde termina. Lo que tuve que aprender fue doloroso. Desde científicos que estudian las capacidades de una oruga para alimentarse de bolsas de nylon, pasando por la fitorremediación de suelos contaminados y por ingenieros que proyectan obras inmensas para procesar nuestros residuos con un mínimo impacto ambiental, hasta todas aquellas personas que ponen sus manos en nuestra basura para separarla, limpiarla y clasificarla. Pude palpar que el sistema está sangrando y que son pocos los actores sociales que están buscando desesperadamente contener la hemorragia.

La separación de residuos dentro del hogar es un camino extensamente transitado por comunicados de políticas ambientales. Si alguna persona todavía no está convencida de que el esfuerzo vale la pena, la invito a leer los próximos párrafos y a enterarse del destino de sus residuos. Cuando uno separa al menos los restos orgánicos de los inorgánicos, está colaborando enormemente con el reciclaje de estos últimos, aún cuando permanezcan mezclados los plásticos con metales y papeles. Lo importante es que se encuentren secos. Si no lo están, tienen menos probabilidades de ser tratados por el personal de los centros de procesamiento de la basura.

Existen dos estructuras básicas en las que se colocan los residuos urbanos. Una de ellas es el relleno sanitario, bien gestionado y con profesionales a cargo. La otra es el basural a cielo abierto. No es necesario tomar ningún curso para darse cuenta del nivel de contaminación que genera esta última estructura. Basta con pasar por uno de ellos, bajarse del auto y expandir los pulmones llenándolos del aire circundante. El deterioro del paisaje es lo de menos. Acucian problemas más importantes, como por ejemplo una napa de agua totalmente contaminada. El proceso para generar esa montaña de basura es bastante sencillo: consumimos, tiramos, el camión recolector recoge nuestros residuos y los vuelca en el basural. Fin del misterio. Nuestra basura sigue ahí y nosotros seguimos consumiendo. Toda una genialidad, ¿verdad? 

![](/images/uploads/foto_2.jpg)

Si viramos nuestra atención hacia un caso menos extremo, nos encontramos con el relleno sanitario. La ciudad en la que vivo cuenta con uno y los vecinos estamos bastante orgullosos de tenerlo. Sin embargo, lo que se esconde debajo de las onduladas lomas de pasto que lo caracterizan, es al menos, preocupante.

El comienzo de la historia es idéntico al caso anterior. Consumimos, tiramos, sacamos la bolsa de basura y el camión la recoge. No obstante, existen muchísimas diferencias en la continuación del proceso. Un relleno sanitario es una estructura proyectada y organizada, que cuenta con una o varias fosas producto del trabajo de retroexcavadoras. Las mismas, cuyas medidas están completamente calculadas por ingenieros, se encuentran recubiertas tanto en el suelo como en las paredes por materiales impermeables, como la arcilla y membranas de polietileno de alta densidad. Esas fosas actúan de vertederos de nuestros residuos, recibiendo alternadamente una capa de 30 cm de espesor de basura compactada con una capa de 20 cm de tierra. 

Un gran sandwich de basura y tierra. Cuando las fosas completan su capacidad de carga, son cerradas e impermeabilizadas. En la cubierta se coloca una capa de suelo vegetal. La impermeabilización de las fosas tiene el único objetivo de evitar que nuestros residuos tomen contacto con la masa de tierra que los rodea. Cuentan, además, con un sistema de recolección de líquidos que lixivian de nuestra basura, para evitar que alcancen y contaminen la napa de agua.

El procesamiento de los residuos se completa con el traslado de esos líquidos lixiviados a unas “lagunas de barro”, dedicadas exclusivamente al cultivo y crecimiento de colonias de bacterias que tienen la capacidad de extraer todo el contenido orgánico (y potencialmente contaminante) de aquellos líquidos, dejándolos completamente limpios y susceptibles de ser vertidos en el suelo, ya sin riesgos.

Aventurarme en las profundidades del tema me dejó con algunas reflexiones: ¿cuánto tiempo me llevó ir al supermercado, comprar un paquete de cualquier cosa, llegar a mi casa, usarlo y tirarlo a la basura? ¿Cuánto tiempo les lleva a los encargados del relleno sanitario sepultarlo y recoger sus líquidos? ¿A las bacterias, cuánto les cuesta descontaminarlo? Y, para cuando todo el sistema haya sacado de circulación mi paquete vacío, ¿cuántos habré generado ya? 

![](/images/uploads/foto_3_1_.jpg)

Las formas eficientes y seguras de eliminar nuestra basura implican procesos lentos, sostenidos por obras de ingeniería importantes y costosas. El problema es que no estuvimos ni estamos consumiendo al mismo ritmo en que los residuos desaparecen; y literalmente, los estamos teniendo que enterrar bajo nuestros suelos. No me parece una muy buena noticia…

Sin embargo, nosotros seguimos muy campantes, con los ojos cerrados y el corazón intacto, cambiando árboles por envases de gaseosa y aire puro por perfumina en aerosol.

**Sigamos así, *sapiens*, lo estamos haciendo realmente bien.**

Fuentes consultadas:

* Gestión Integral de Residuos Sólidos Urbanos-OPDS-Dirección General de Cultura y Educación-Provincia de Buenos Aires (Argentina)
* Ley de “Basura Cero” de la Provincia de Santa Fe (Argentina)-Ley N°13.055



<!--EndFragment-->

<!--EndFragment-->