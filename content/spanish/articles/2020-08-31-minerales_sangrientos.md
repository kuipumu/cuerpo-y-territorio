---
title: "Minerales sangrientos "
cover: /images/uploads/oro-billete.jpg
caption: "Foto: www.arcominerodelorinoco.com"
date: 2020-08-31T15:14:27.984Z
authors:
  - Jefersson Leal
tags:
  - Venezuela
  - Minería
  - Masacre
  - ArcoMinero
  - Bolívar
  - Crímenes
  - Minerales
  - Oro
  - GobiernoNacional
  - AMO
categories:
  - Territorio
  - Política y Economía
comments: true
sticky: true
---
<!--StartFragment-->

El Dorado, El Callao, Las Claritas y La Florinda son nombres de algunas de las muchas minas que están operativas al sur de Venezuela, en la amplia zona geográfica que ocupa el proyecto extractivista Arco Minero de Orinoco (AMO). El cual se ha convertido en uno de los mayores focos de violencia del país, siendo las masacres, degollamientos, mutilación de manos, la desaparición forzada y el asesinato, ley común en un territorio dominado por relaciones mafiosas.\[1]

Este proyecto comienza formalmente sus operaciones en el año 2016, bajo el decreto ejecutivo 2.248 que posibilitó la extracción y comercialización de diversos minerales como: oro, coltán y diamantes. Está ubicado en una extensión territorial de aproximadamente 111.843 kilómetros cuadrados, que representa alrededor del 12 % del territorio venezolano.\[2]

Gran parte del territorio del AMO se encuentra conformado por parques nacionales y zonas naturales que forman parte del patrimonio de la humanidad y territorios de comunidades indígenas. Estos se encuentran gravemente amenazados por las actividades mineras y toda la violencia que se ha tejido alrededor de esta. Si bien la minería legal e ilegal no es nueva en la zona, si resulta novedoso la escala del proyecto y los diversos mecanismo de implementación y control territorial por parte de agentes estatales y paraestatales.

Esta nueva vía de obtención de recursos a través de la comercialización de la naturaleza (minería) surge con posibilidad plausible a partir de la crisis política y económica de 2014-2015. En ese momento el clima social y político de Venezuela se encontraba en constante agitación, que se combinó con una drástica caída de los precios del petróleo, ocasionando una merma en los ingresos nacionales.

A partir de ese momento, los agentes controladores del poder político en el Estado comenzaron un proceso de reconfiguración, ya no se ordenarían en torno a la captación de la renta petrolera, sino a partir de la extracción de minerales del sur del país\[3]. Esta nueva fase del chavismo tardío en el poder, se caracterizará por una continua acumulación por desposesión.

La expansión de la frontera extractivista en Venezuela promovida desde el Estado Nacional representa una nueva etapa de colonización territorial y el método de conquista seleccionado emula a las nuevas técnicas del poder político, es decir, la imposición mediante la violencia. El AMO es la encarnación de la nueva distribución del poder en Venezuela, que tiene como principal característica la política de la muerte.

El amplio territorio del Arco Minero del Orinoco es controlado por diversos grupos de poder tanto estatales como paraestatales. Resaltan entre los grupos criminales los Sindicatos y Pranes, ambas son bandas organizadas que han cobrado fuerza en los últimos años en el país. Los primeros vienen del sindicato de la construcción de Ciudad Bolívar (capital del estado Bolívar, escenario del conflicto), que fue migrando y expandiendo su red mafiosa al negocio minero. Los segundos, Los Pranes son organizaciones delictivas que nacen en torno al sistema carcelario del país\[4]. Son estos grupos, en buena medida, los responsables de un gran número de atrocidades en la zona.

Recientemente, han aparecido una serie de investigaciones de organizaciones nacionales e internacionales que dan cuenta de la gravedad de las violaciones hacia los derechos humanos en la región del AMO. Algunas investigaciones afirman que desde el año 2012 al 2019 se han ejecutado al menos 31 masacres\[5], teniendo un repunte de casos desde el año 2016. Si se toman las cifras de decesos en masacres desde 2016 hasta 2019 resulta un total de 174 personas fallecidas.\[6] 

De las cifras anteriores resalta un caso particular conocido como “La Masacre de Tumeremo” donde fallecieron al menos 28 mineros. Según los testigos, las víctimas se encontraban en la mina Atena en la población de Tumeremo en el Estado Bolívar, cuando fue asaltada por un grupo armado con el objetivo de controlar el yacimiento.

Para los familiares de las víctimas, el asalto a la mina se produce con la autorización de la Fuerza Armada Nacional\[7]. Para algunos la penetración fue llevada a cabo por miembros de la guerrilla colombiana: Ejército de Liberación Nacional (ELN), aunque la versión oficial señala como responsable a una banda de paramilitares.

Los restos de las 28 víctimas fueron hallados en una fosa común. Las investigaciones iniciaron luego de una gran cantidad de denuncias por parte de familiares, vale acotar que el gobernador del estado Bolívar para ese momento Francisco Rangel Gomez (PSUV) negó públicamente que tales hechos hubiesen ocurridos\[8].

Según un informe presentado en julio de este año por la Alta Comisionada de las Naciones Unidas para los Derechos Humanos se han generado al menos 16 enfrentamientos armados entre el año 2016 y 2020, teniendo como resultado el deceso de 149 personas.\[9]

En el mismo informe se reseña que en la zona los tratos crueles e inhumanos son comunes y aplicados como correctivos por los grupos armados y las bandas criminales “…un minero fue golpeado en público por robar un cilindro de gas; a un joven le dispararon en ambas manos por robar un gramo de oro; una mujer fue apaleada por robar un teléfono a un miembro del sindicato y a un minero le cortaron una mano por no declarar una pepita de oro.…”\[10]

Muchos familiares temen denunciar por las represalias de los grupos criminales, aunado al hecho de que varias de las instituciones públicas encargadas de las investigaciones simplemente desestiman las denuncias: “En las minas matan a mucha gente. Los pranes matan a la gente como para salir de ellas y meter más”\[11], estas son palabras que las autoridades locales pronunciaron al momento de que la esposa de Wilmer González, fue a denunciar la desaparición de su esposo minero.

Lo que sucede al sur de Venezuela no es una anomalía, es la continuación de las nuevas políticas del Estado expresada en un territorio que hasta hace muy poco era virgen, es un forma de control territorial que tiene como objetivo brindar soporte económico a diversos grupos en el poder político.



- - -



\[1][](https://www.crisisgroup.org/es/latin-america-caribbean/andes/venezuela/073-gold-and-grief-venezuelas-violent-south)[](https://bit.ly/3be4E7Q) [crisisgroup.org ](https://bit.ly/3be4E7Q)\[18-8-2020]

\[2] Ver <http://transparencia.org.ve/oromortal/>

\[3] http://www.aporrea.org/actualidad/a281428.html

\[4] [](https://www.crisisgroup.org/es/latin-america-caribbean/andes/venezuela/073-gold-and-grief-venezuelas-violent-south)[crisisgroup.org](https://bit.ly/3bdeHKh) \[18-8-2020]

\[5] Cesar Romero, Evolución de la violencia en el entorno minero del estado Bolívar (marzo de 2016 – febrero de 2019), en: [](https://www.ecopoliticavenezuela.org/2020/05/26/evolucion-de-la-violencia-en-el-entorno-minero-del-estado-bolivar-marzo-de-2016-febrero-de-2019/#:~:text=La%20Guardia%20Nacional%20vigila%20de,a%20la%20miner%C3%ADa%20en%202016.&text=Espec%C3%ADficamente%20se%20abordar%C3%A1n%20hechos%20acontecidos,2016%20hasta%20febrero%20de%202019.)[Observatorio de Ecología Política de Venezuela](https://bit.ly/2QGbfyA)\[18-8-2020]

\[6] Ídem

\[7] “Tumeremo e Ikabarú, dos caras de la misma moneda”, Minerales del Conflicto (Sur del Orinoco soberanía perdida). Venezuela, N°1, junio de 2020, p. 7

\[8] ídem

\[9] Informe de la Alta Comisionada de las Naciones Unidas para los Derechos Humanos A/HRC/44/54

\[10] Ídem

\[11] Marcos David Valverde, “Mientras no vea un cuerpo no puedo decir que está muerto”, en [](https://www.lavidadenos.com/fosasdelsilencio/mientras-no-vea-un-cuerpo-no-puedo-decir-que-esta-muerto/)[La vida de nos](https://www.lavidadenos.com/fosasdelsilencio/mientras-no-vea-un-cuerpo-no-puedo-decir-que-esta-muerto/) \[18-8-2020]

<!--EndFragment-->