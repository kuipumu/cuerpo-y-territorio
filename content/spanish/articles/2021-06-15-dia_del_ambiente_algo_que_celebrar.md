---
title: "Día del Ambiente: ¿algo que celebrar?"
subtitle: '"El planeta está dañado y nosotres también"'
cover: /images/uploads/a_ver_.jpg
caption: "Autor: Joe Mesier “Donna Haraway” (Vanitas Series), 2017"
date: 2021-06-15T18:03:17.122Z
authors:
  - Paula Fausti
tags:
  - Ambiente
  - DiaMundialDelAmbiente
categories:
  - Territorio
comments: true
sticky: true
---


*Concretamente, a diferencia del Antropoceno o el Capitaloceno,*

*el Chtuluceno está hecho a partir de historias y prácticas multiespecies*

*en curso de devenir-con, en tiempos que permanecen en riesgo,*

*tiempos precarios en los que el mundo no está terminado*

*y el cielo no ha caído, todavía (…)*

*El orden ha sido entretejido: los seres humanos son de y están con la tierra,*

*y los poderes bióticos y abióticos de esta tierra*

*son la historia principal.*

**Donna Haraway en “Seguir con el Problema”**

Este 5 de junio se volvió a celebrar, una vez más, el **Día Mundial del Ambiente**. Pero **¿qué significa celebrar el ambiente en contextos de crisis ecológica?** Este día muchas veces se usa como efeméride predilecta por empresas, compañías e incluso gobiernos, para hacer greenwashing o un **“lavado de cara verde”**. Vemos que las redes y los medios se llenan de imágenes con un mundo frágil sostenido por dos manitos, acompañado de palabras como ecofriendly, “sustentable”, “amigable con el medioambiente”, “ecológico”, etc. **La realidad es que ya no tenemos tiempo de seguir creyendo en propagandas ni frases vacías**. Pero en esta ocasión no me voy a detener en cómo los discursos mediáticos nos manipulan, aunque es un tema que me gustaría abordar en el futuro, sino que me permitiré escribir sobre algo más íntimo. Tomaré como excusa este día para acercarles algunas reflexiones sobre **cómo transitar este momento tan complejo de crisis**.

Me interesa pensar qué nos genera encontrarnos y sabernos frente a un contexto de **crisis civilizatoria**, ecológica y climática. Hay quienes imaginan **desenlaces apocalípticos**, guerras civiles, enfrentamientos entre potencias mundiales, caos y hambre, catástrofes naturales impredecibles, etc. Estos escenarios se basan en datos empíricos pero también son exacerbados por algunos discursos, por ejemplo la industria del entretenimiento. Es importante informarse sobre lo que está sucediendo, pero está el riesgo de que **si nos centramos en todo lo negativo, nos puede invadir el miedo, la impotencia, el desamparo o la desesperanza**. Además, se pueden activar mecanismos de defensa como la **negación** o nos puede llevar directamente a un **cinismo o cómoda indiferencia**, pues “no podemos hacer nada para cambiarlo”. Si bien creo que cada quien encuentra la forma de tramitar esta información de la manera en que puede, con las herramientas que tiene, desde mi mirada propongo como **primera instancia** permitirnos tramitar **un duelo**. Duelar un mundo que ya no es, los propios proyectos de vida, los seres y ecosistemas que injustamente ya no están. **Puede costar encontrarnos con el dolor**, pero no hay nada más sanador que hacerle lugar y aceptarlo.

Como segunda instancia, comparto la idea que sostienen algunes pensadores actuales de que es momento de **inventar narrativas**, historias, que nos permitan imaginar otros mundos posibles. En la naturaleza las mutaciones son claves para la adaptación. Permitámonos mutar, imaginar, crear. **Nuevas historias son necesarias** para sacarnos de este embrollo, y para la construcción de **sociedades resilientes**. Podemos apostar a la transición o podemos seguir mirando para otro lado hasta el choque final.

**El planeta está dañado y nosotres también**, pero esta situación no es irreversible. **¿Contamos con la creatividad suficiente para atravesar este problema?** No lo sé, pero lo que sí creo es que la **fuerza está en lo colectivo.** La pandemia nos puede hacer creer que estamos solos, solas, y a veces cuesta sentir lo contrario, pero hay formas de juntarse a **co-crear** sin poner en peligro la salud de otres. Inventar espacios de intercambio, de encuentro, donde podamos llevar a la práctica formas de vincularnos más amorosas, empáticas y respetuosas, es una vía posible para la sanación nuestra y de la Tierra.

![](/images/uploads/donna-800x445.jpg)

En “Seguir con el problema: Generar parentesco en el Chthuluceno” (2019) Donna Haraway nos propone justamente nuevas narrativas, historias de recuperación, y formas de vivir y morir en un mundo dañado. Construir como **seres respons-hábiles**, habitando y guardianando nuestros territorios, armando parentescos no sólo con otros humanos, sino con **otras especies**. Plantea que los seres humanos no somos más importantes que otros seres vivos, por eso entreteje historias multiespecies, donde los humanos son sólo una parte más de todo este organismo vivo e interconectado que es Gaia. Algo similar nos cuenta **Moira Millán**, mujer mapuche y miembra del colectivo **Movimiento de Mujeres Indígenas por el Buen Vivir**, cuando en su libro el “Tren del Olvido” (2019), dice:

*Los mapuches nos relacionamos con la naturaleza y construimos nuestra identidad a partir de ella, fortaleciendo nuestra espiritualidad. (…) Creer que sólo los humanos se necesitan entre sí, que sólo los humanos pueden amar y escucharse es errado. Somos lof, humanos interactuando de manera recíproca con la naturaleza. (…) Somos lofche, el espacio en el que también habita la gente. Hay muchos otros cohabitando con el humano y con los cuales debemos acordar el arte de habitar.*

**Ojalá el próximo día del ambiente nos encuentre entretejiendo otras narrativas y sobre todo llevándolas a la práctica, con las manos en el barro, en el compost, en la tierra**. Construir otros mundos posibles y resistir con firmeza el avance de la **necropolítica,** es quizás nuestro desafío más importante en este momento histórico. Por mi parte ya no deseo más un feliz día del ambiente, por el contrario, **deseo que días así nos recuerden que nunca es tarde para sanar y que en ese sanar podamos reconciliar(nos), regenerar(nos) y reconstruir(nos)** otras formas de vivir y morir en el mundo, otras formas de relacionarnos con la naturaleza, porque somos parte de ella... **Tierra, Casa, Hogar, Vida, Diversidad, Energía, Movimiento, Amor, Gaia, Pachamama**.

Además de los dos libros mencionados, recomiendo mucho el documental “Donna Haraway: Cuentos para la supervivencia terrenal”, disponible en forma gratuita y online en el siguiente link: <https://lalulula.tv/cine/100076/donna-haraway-cuentos-para-la-supervivencia-terrenal>

{{< youtube oDvuTPUT0I0 >}}