---
title: ¿Las feministas todas somos iguales?
subtitle: 8M y medios de comunicación
cover: /images/uploads/articulo_iliara.jpeg
date: 2021-04-30T14:08:30.293Z
authors:
  - Iliara Montenegro Moreno
tags:
  - Genero
  - Cuerpo
  - Territorio
  - Feminismo
  - Mujeres
categories:
  - Genero
  - Cuerpo
  - Territorio
comments: true
sticky: true
---
El 8 de marzo las feministas de todo el mundo, en nuestra diversidad, conmemoramos la lucha por el reconocimiento y garantía de cumplimiento de los derechos de las mujeres, lucha por la que miles de ellas han dedicado –y dado— su vida. Visibilizamos y agradecemos a las mujeres que nos inspiran a seguir construyendo mundos más libres, dignos, justos y equitativos. 

También nos reconocemos entre nosotras, nos damos fuerza, gritamos, lloramos y reímos, le mostramos al mundo nuestra indignación, dolor y rabia, arengamos: **“Nos tienen miedo porque no tenemos miedo”**, a pesar de que a veces sí que tenemos miedo, y no es para menos, esta lucha a pesar de su amplio recorrido y de sus múltiples frentes no ha acabado: **En Colombia, entre el 01 de enero y el 28 de febrero de este año 37 mujeres fueron víctimas de feminicidio,** lo que implicó además un incremento del 8% respecto al 2020.

Las marchas convocadas por mujeres han sido, sin duda, una forma muy poderosa de conmemoración y frente de lucha feminista. Quizá la forma que más ha recibido atención por parte de los medios de comunicación Nacionales, pero, ¿cuál es el mensaje que noticieros como RCN, Caracol, El Tiempo y Semana están difundiendo a nivel nacional sobre las feministas a través de esta única forma de lucha?

**Cuando me acerqué al feminismo lo primero que aprendí es que no hay una única forma de ser feminista**, el volverse feminista es entonces, para mí, un camino de nunca acabar, ladrilludo, de confrontación y reflexión con una misma, de asumir que el patriarcado también se encarnó y encarna en nosotras, y que, además sus expresiones son diversas: no todas las mujeres experimentamos las violencias patriarcales de la misma forma, ni todas vivimos las mismas violencias. La diversidad es entonces un frente muy claro y básico dentro de las luchas feministas. ¿Parten de esta afirmación los medios de comunicación tradicionales del país para dar a conocer su mensaje?, No.

La marcha del 8M –o del 25 N— es un espacio muy poderoso porque permite mostrarnos ante el mundo como una gran fuerza unida donde nuestras diferencias pasan, aparentemente, a un segundo plano. Desde afuera, somos una mancha de mujeres homogéneas que piensan, sienten y actúan igual, somos “las feministas”. **Sin embargo, la marcha tiene matices, no es un espacio que se deba romantizar, hay disputas, desacuerdos y diálogos.** **La marcha nos pertenece, pero no tiene dueño,** así como las feministas somos diversas, la vida que se le da a la marcha es, igualmente, diversa. Acaso, ¿Semana o RCN se han interesado por mostrar estos matices?, No.

De los ejemplos más problemáticos y al mismo tiempo dicientes de esta situación fue el foco de atención que se le dio al bloque separatista el 8M. Entre las múltiples noticias y videos que circularon sobre la marcha, hubo una con particular popularidad: Diana Giraldo López, una periodista que cubría un trayecto de la marcha no pudo culminar su trabajo pues el camarógrafo que la acompañaba era hombre, y en esta parte particular de la marcha, los hombres no eran bienvenidos. Sobre esto, Semana, El TIEMPO, Publimetro y otros medios de comunicación publicaron algunas notas periodísticas en donde se mostraba al periodista como una víctima de estas mujeres feministas que no dejan en paz a los pobres hombres. Para hacernos una idea de ello vale la pena tener en cuenta los titulares de las publicaciones:

Por ser hombre, impiden trabajar a camarógrafo de CM& en marcha del 8M (El Tiempo)

“Se va ya’’: manifestantes de marchas 8M en Bogotá expulsaron a camarógrafo por no seguir sus peticiones (Semana)

“Me están cohibiendo a mí como mujer, ¿no puedo trabajar?”: periodista denuncia agresiones en marcha feminista (Publimetro)

Mujeres agreden a camarógrafo en marchas del ‘8M’ por ser hombre (Infobae)

¿Todas las feministas son separatistas?, No, sin embargo, las noticias no fueron claros al respecto, tampoco les pareció relevante tener en cuenta el trasfondo detrás de la decisión de ser separatistas: **¿qué motiva a una mujer dejar de relacionarse con hombres?, ¿es una decisión arbitraria?, ¿qué nos dice esa decisión de la vida de las mujeres y sus experiencias con hombres?** Todas estas preguntas fueron ignoradas y el foco siguió siendo la rabia con la que algunas mujeres no dejaron trabajar al susodicho camarógrafo.

Ósea que ¿no había hombres en la marcha?, de hecho bastantes, pero no en todos los bloques de la marcha eran bienvenidos, ¿por qué?, bueno, hay algunas feministas que consideran que los hombres no juegan un papel relevante en una marcha que conmemora la lucha liderada por mujeres, ¿todas pensamos lo mismo?, ¡No!. **Es más, el lugar de los hombres dentro del feminismo es de los debates más relevantes al interior de las luchas feministas** y sin embargo, las notas publicadas no se interesaron en lo absoluto en al menos evidenciar la inexistencia de un consenso sobre esto.

En efecto, la situación narrada entorno al camarógrafo de la periodista Diana Giraldo es apenas una expresión de un tipo de feminismo y en la que detrás conviven un montón de reflexiones, conflictos y complejidades sumamente profundas que fueron completamente ignoradas y banalizadas. Desde la ignorancia los medios de comunicación tradicionales difundieron un modelo de mujer feminista, **nos homogenizaron a todas y de paso informaron a medias una situación que daba para un análisis mucho más crítico** en el que se valorara el recorrido histórico y multiespacial del feminismo evitando invisibilizar un montón de frentes de lucha, formas de ser y concebirlo justo en un día tan crucial para nosotras.



#### Iliara Montenegro Moreno 

Iliara Montenegro Moreno es egresada del programa de historia de la Universidad Externado de Colombia y estudiante de Geografía de la misma universidad. Hace parte de la colectiva suculentas cuyo principal objetivo es transformar las formas machistas en las que se relacionan diferentes grupos sociales por medio de actividades pedagógicas como talleres organizados para diferentes contextos.