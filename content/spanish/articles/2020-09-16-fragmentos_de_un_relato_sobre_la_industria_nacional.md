---
title: Fragmentos de un relato sobre la Industria Nacional II
subtitle: El conurbano que se autogestiona sin trompa
cover: /images/uploads/resistir_copia.jpg
date: 2020-09-16T01:14:37.108Z
authors:
  - Verónica Vazquez
tags:
  - Política
  - Sindicatos
  - Trabajadores
  - Argentina
  - Cooperativas
  - TrabajadoresGráficos
categories:
  - Política y Economía
comments: true
sticky: true
---
##### EPISODIO II

<!--StartFragment-->

El cuerpo de esta cronista que relata, pretende seguir arrimando hasta acá brasas al fueguito que se armó en este territorio; poniendo el cuerpo para poder transformarlo.

Transformar el relato del territorio por cuyo cuerpo transcurren algunos hechos narrados a continuación, posiblemente causen alivio y reinterpretaciones en la misma medida. Porque es como decíamos en la mesa del comedor: *“cualquiera es Gardel con el diario del lunes”* y digo diario y pienso en papel, no prensa sino papel diario cuyo gramaje es menor a 60gms. Papel y tinta, tinta negra, titulares, noticias frescas; **el trabajo en una imprenta es tan dinámico, tiene un olor tan particular y tanta mística que une puede enamorarse.**

<!--EndFragment-->

![](/images/uploads/periodicos_2.jpg)

<!--StartFragment-->

**Se cumplen rutinas porque el tiempo es dinero y el tiempo está marcado por la velocidad de la máquina que escupe pliegos impresos** –de frente y de dorso a la vez- que si está las 24 horas funcionando sabemos que a fin de la quincena vamos a poder cubrir los costos, vamos a poder tener un extra para repartir: ¡les muchaches contentísimos!

Se cumple con rutinarios ruidos transformados en sinónimos de seguridad, cierta tranquilidad que habilita el chismerío o bien las historias tele novelescas porque nadie quiere ser la comidilla pero todos estamos formateados para opinar sobre la vida del otre porque para eso nos entrenan en esta era de la Big data: entonces algo hay que decir, un juicio de valor que estigmatiza la vida ajena según el mandato de la corrección política establecida. Es muy fácil opinar pero a nadie le gusta embarrarse, y menos trabajar.

<!--EndFragment-->

Entonces me pregunto: **¿Qué pasa con la imagen del patrón como enemigo cuando abandona el taller, de pronto?** ¿Cómo saber que la patronal está planeando una huida? ¿Qué nos une para seguir adelante? ¿Se arma una cooperativa por “simple” decisión de sus integrantes?

**Un taller perdido al norte del conurbano bonaerense es el escenario de operaciones de una historia alucinante, propia del relato sobre una Argentina industrial épica,** donde el peronismo es la ideología dominante y la necesidad de no perder la fuente de trabajo son suficientes para tomar la decisión correcta: hay que seguir dándole de comer a los pibes, no quedan muchas opciones. El obrero no pensado desde su individualidad sino concebido a la luz de un nuevo colectivo que debe surgir, aflorar, constituirse en un “acta constitutiva” y la distribución de cargos: una nueva forma jurídica que nace para reemplazar a la fallida que nos estafó con los sueldos, que además se llevó computadoras de la oficina –incluyendo al personal- y por supuesto a la cartera de clientes, a la confianza con los proveedores pero no las deudas pendientes.

<!--EndFragment-->

![](/images/uploads/maquinaria_obsoleta.jpg)

<!--StartFragment-->

Además somos todos del taller (**de abajo los trabajadores), tenemos más de 10 años de experiencia en el oficio de imprimir lo que baja de la oficina (arriba la patronal)**: libros, revistas, guías de calle o manuales sobre cosmiatría, mantelitos descartables para restaurantes o cuadernillos, folleterías varias, afiches. **Es un arriba (la oficina) que simboliza el poder de hacerte perder los mejores días de tu vida para ganarte el mango,** esa necesidad tan propia del que solo tiene su fuerza de trabajo para vender en el mercado, para recordar al gran Karl Marx. Pero el desafío que implica “arriba” pronto será develado realmente, es cuestión de tiempo una vez que se recupera la empresa y el taller deja de pertenecerle al patrón (del que está arriba) para ser nuestro, los que estábamos “abajo”: tenemos que aprender cómo es gestionar arriba.

![](/images/uploads/asamblea_arriba.jpg)

<!--StartFragment-->

Pero esto pasó mucho después. Hoy quiero contarles que durante el mes de septiembre, se celebra en mi país (el que vivo desde que nací, el mismo que amo en toda su extensión y con todos sus defectos incluidos) Argentina, **el día de la industria.**

La industria por supuesto que está siendo motivada e incentivada desde el actual gobierno a pesar de la pandemia, pero es imposible olvidar la huella de los últimos cuatro oligárquicos años a los que nadie sabe bien cómo pero sobrevivimos (aunque no todos). Para nosotros fue fatal, letal, pero de eso les voy a contar más adelante.

Por ahora me gustaría repasar los componentes de este escenario:

* **Un “arriba” que simboliza la opresión patronal paternalista** alimentada por muchos años de manejar la gráfica fundada por un padre italiano que vino a explotarnos con conducta y disciplina, con sabiduría antes que marxista más bien bondadosa (aprieta pero paga), esa mirada del amo bueno que cumple con los pagos, toda una manera de gestionar.
* **Un “abajo” que huele a sacrificio y resistencia,** a identidades forjadas por infinidad de horas de trabajo repetitivo, sistemático y rutinario respirando plomo, porque la máquina lo necesita y era obligatorio tomar un vaso de leche, tal como establece el CCT (Convenio Colectivo de Trabajo) 60/89:

> **Suministro de leche**

Art. 52 – A todo trabajador empleado en el bronceado, máquina aerograph industrial, estereotipia, fundición de metales, tipografía y linotipos, se le suministrará un litro de leche diario.

Esta defensa de la salud del obrero se hará extensiva a otras secciones o tareas si a criterio de las autoridades pertinentes fuere necesario como antídoto.

Fuente: [](http://federaciongrafica.com.ar/convenio)[Convenio Federación Gráfica](<http://federaciongrafica.com.ar/convenio>)

Un arriba empresarial y un abajo trabajador que no son lo mismo pero deben fusionarse para dar lugar a la nueva identidad: recuperada y autogestionada por sus propios trabajadores. **Les invito entonces a dejar los colores deterministas y abrir la mente para entender la riqueza de los grises que encierra esta experiencia que revolucionó por completo los esquemas teóricos sobre el deber ser de la realidad latinoamericana.** ¡Los espero con ansias en la próxima entrega!

![](/images/uploads/vero.jpg)

[\#ImpuestoALaRiquezaYa](<https://twitter.com/hashtag/ImpuestoALaRiquezaYa?src=hashtag_click>)



![](/images/uploads/cierra_nota.jpg)