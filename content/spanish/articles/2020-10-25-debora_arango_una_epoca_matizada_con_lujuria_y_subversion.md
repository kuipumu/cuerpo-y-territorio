---
title: "Débora Arango, una época matizada con lujuria y subversión "
subtitle: " "
cover: /images/uploads/deboora-arango-justicia-e1532462425881.jpg
caption: "Débora Arango: Casablanca"
date: 2020-10-30T11:30:54.579Z
authors:
  - Diana Carolina Beltrán
tags:
  - DeboraArango
  - Natalicio
  - Feminismo
  - Colombia
  - Medellin
  - Patriarcado
  - Cultura
  - Obra
  - Pintura
  - DesnudoFemenino
  - Latinoamerica
categories:
  - Género
  - Cultura
comments: true
sticky: true
---
<!--StartFragment-->

![Esquizofrenia en el manicomio. Débora Arango](/images/uploads/1447779762_662463_1447781372_noticia_normal.jpg "Esquizofrenia en el manicomio. Débora Arango")

Un día, en medio de una sociedad conservadora, religiosa, patriarcal y sumisa como la encerrada, literalmente, entre dos de los brazos de la Cordillera de Los Andes y un exótico bosque húmedo tropical, en el corazón antioqueño colombiano, **Débora Arango se tomó personal destruir el concepto de la civilización tradicional y católica.**

Entonces, no solo empezó a hacer ruido por practicar las mal vistas “modas femeninas” como montar a caballo usando pantalón o nunca sentir la necesidad de vivir para encontrar un marido y crear una familia numerosa para aparentar felicidad, ella quería ser quien marcara un antes y un después en las paredes de los museos ya que, desde que decidió dedicarse a la pintura, se empezaron a ver por primera vez gruesas pinceladas con senos descubiertos, cuerpos femeninos desnudos, escenas alusivas al trabajo de parto y sátiras a figuras religiosas y políticas. Se creía que a ningún colombiano se le hubiese ocurrido expresar una opinión de una forma tan “desvergonzada” e “inmoral” como esa, pero sin duda, **haberse convertido en la excepción a la regla dejó un legado inmortal para cada una de las niñas que han nacido desde entonces.**

Débora nunca quiso ser solo una pintora que pintaba. De una forma genuina y contundente en cada obra hizo énfasis en la necesidad de escuchar y valorar a las mujeres. Su militancia estaba argumentada con trazos gruesos, pezones, curvas, sotanas y calaveras que expresaban un mensaje claro y transgresor. Así causó un ruido ensordecedor capaz de expulsarla de la iglesia católica, llegar a afectar la situación económica familiar y **ser catalogada como “la mujer de los cuadros pornográficos, satánicos e impúdicos”.**

**Temas que la sociedad ha tratado como tabú le dieron vida a todas las pinturas que en su mayoría trató de sacar a la luz entre los años de 1930 y 1950.** En la pintura ***Esquizofrenia en el manicomio***, (el inicio de esta semblanza, por ejemplo) simboliza a una mujer en posición libidinosa, con las piernas abiertas, encerrada en una habitación con imágenes de mujeres desnudas pegadas en las paredes, recortes de revistas pornográficas, en el que pareciera ser un centro psiquiátrico de la ciudad.

Entendiendo un poco el contexto de la época y lo que significaba la homosexualidad para ese entonces, muy probablemente **la escena busca representar a una mujer lesbiana sometida a un tratamiento psiquiátrico,** lo que sería un alma aberrada para los patrones sociales y hasta clínicos de la época en varios países del mundo.

Ser autora de una colección de obras con un alto contenido polémico la castigó con un largo silencio, olvido y censura hasta bien entrados los años 80; luego del boom provocado por sus representaciones de prostitutas, mujeres desnudas, pordioseros y, por supuesto, un gran desfile de políticos corruptos e imágenes religiosas llenas de sátira, retratados desde una perspectiva para nada convencional. **Ningún museo colombiano, latinoamericano ni mundial quiso exhibir ni una sola de sus obras.**

Débora fue completamente ignorada por aquel submundo donde pasean los exigentes coleccionistas privados. Tampoco quiso vincularse con esas galerías para poder mostrar su trabajo.

Con una forma de ser bastante huraña, algo desconfiada e intensamente discreta, fue protagonizando de forma sistemática una serie de actos opositores y un real boicot a cualquiera de sus galas públicas.

Mientras corría el año 1939 en su natal Medellín, fue abiertamente saboteada por las conocidas “Damas de la Liga de la Decencia”, un selecto grupo de señoras que a toda costa y a cualquier precio defendían la moral y las buenas costumbres. Meses después hizo su debut en el majestuoso e histórico Teatro Colón de la capital colombiana, pero fue cerrado por estrictas órdenes del político ultra conservador Laureano Gómez, quien sería presidente de Colombia entre 1950 y 1951 y quien mantenía estrechos lazos de amistad con conocidas figuras religiosas del momento. Lo hizo porque juró no permitir las “infames” obras de la artista ya que claramente sus desnudos le parecían "inmorales", "perversos", "pornográficos" e "incorrectos".

**Débora tragaba grueso, no debió haber sido fácil declararse una completa antisistema y anti creencias.** Sin embargo, siempre estaba buscando la forma y el lugar para dar a conocer su opinión y su trabajo, entonces cruzó el Atlántico y expuso en el reconocido Instituto de Cultura Hispánica de Madrid en el año 1955. Pero una vez más fue vetada, esta vez por el dictatorial gobierno de Francisco Franco.

**Haber tomado la lujuria y la subversión como modo de vida recreó un tortuoso camino a recorrer.** Incluso, la renombrada influyente crítica argentina Marta Traba, residente neogranadina desde la década de los años 50, no tomó en consideración el valor de sus obras. Todo lo contrario, la hizo a un lado para dar paso a la nueva camada de artistas, todos eran hombres que años más tarde gozarían del renombre, respeto y alto reconocimiento nacional e internacional que Débora también merecía. Algunos de ellos fueron Fernando Botero, Alejandro Obregón, Édgar Negret y Eduardo Ramírez Villamizar.

![Trata de blancas. Débora Arango](/images/uploads/frine.jpg "Trata de blancas. Débora Arango")

Ésta, es una de sus obras más conocidas, la llamada "***Friné"*** o ***"Trata de blancas"*** pintada en 1940, donde se puede apreciar en el centro de la composición a una mujer desnuda que intenta cubrir sus senos con un trozo de tela blanca, mientras que la mano de uno de los dos hombres que la rodean intenta impedirlo.

\
Las figuras masculinas tienen los rostros elevados, sus frentes apuntan hacia arriba, mientras que la mujer protagonista baja su rostro y su mirada. Este contraste de direcciones visuales masculinas y femeninas podrían indicar el privilegio que ellos poseen de la mirada. Mientras tanto, ella está posiblemente resignada con la realidad que la acosa, la que la convierte en contra de su voluntad en un objeto disponible para  cualquier intención de dominio. 

**Débora, la mujer que decidió dedicar su vida a desnudar un país entero, murió sin hijos, soltera y sola en su alejada casita en el campo.** Un templo donde cada rincón está intervenido por su mirada; donde la vajilla, los jarrones, las materas y los ceniceros tienen su huella; un espacio que guarda más de 200 años de historia bien mantenida por Oscar, el jardinero que continúa sembrando orquídeas y por su sobrina Cecilia, que conserva la casa como si fuera un museo.

Esa pequeña casita fue su único hogar, aquel que fue testigo de alegrías, lágrimas y el típico sin sabor que pueden llegar a dejar las fuertes críticas por utilizar el arte como medio controversial de expresión. Algunas fuentes no oficiales afirman, a modo de murmuración, que era lesbiana, sin duda un asunto meramente personal y que hoy en día nadie se ha atrevido a confirmar y que, de ser cierto, tuvo que haber sido transversal en su modo de ver la vida, de expresarse ante ciertas realidades y significarle más de una dificultad en la Medellín de aquel momento.

Luego de un siglo, el Museo Nacional de Colombia rinde un merecido homenaje a la artista más polémica de Colombia. Se trata de un recorrido por 50 óleos y acuarelas en los que se pueden apreciar los diferentes temas que abordó, desde desnudos hasta retratos políticos con un alto contenido de crítica y social.

También, como un reconocimiento tardío, en el año 2016 el Banco de la República incluyó su imagen en la última familia de billetes, siendo ella la protagonista del ejemplar de $2000 pesos, junto a la representación de un árbol lechoso, un pájaro cardenal y el majestuoso Caño Cristales, conocido por ser el río de los cinco colores.

> *“Un desnudo no es sino la naturaleza sin disfraces, tal como es, tal como debe verlas el artista: un desnudo es un paisaje en carne humana. La prostituta es lo que queda cuando se borran las máscaras”*. **Débora Arango**

1. Colección de arte, [Banco de la República](https://www.banrepcultural.org/coleccion-de-arte/artista/debora-arango-perez)
2. [Portal de Débora Arango](https://deboraarango.edu.co/portal/index.php/nuestra-historia-debora-arango)
3. [BanRepCultural](https://enciclopedia.banrepcultural.org/index.php?title=D%C3%A9bora_Arango_P%C3%A9rez)
4. [Débora Arango, el retrato de una época](https://estado.co/articulo-revista/debora-arango-el-retrato-de-una-epoca/)