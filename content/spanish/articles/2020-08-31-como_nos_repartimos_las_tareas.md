---
title: ¿Cómo nos repartimos las tareas?
cover: /images/uploads/14103.jpg
caption: "Foto: Con sello patagónico"
date: 2020-08-31T13:55:39.954Z
authors:
  - Solmarena Torres
tags:
  - Feminismo
  - Inclusión
  - Machismo
  - TrabajoNoRemunerado
  - Mujeres
  - MaltratoFemenino
  - EquidadDeGenero
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

Cuando decidí escribir sobre el trabajo no remunerado sabía que se estaba hablando de este tema en diferentes niveles y que podemos acceder a datos porcentuales del Producto Interno Bruto (PIB) que esto representa para cada país o la mayoría de ellos. También podemos hablar de la relación de los trabajos de cuidado haciendo un análisis desde los mitos del amor romántico de las mujeres con dobles jornadas laborales.

Pero en realidad, quería traer a colación algunas experiencias de amigas y propias, compartir de forma más íntima lo que va sucediendo con nosotras y la forma en la que nos vamos relacionando heterosexualmente. Porque lo personal es político.

He leído y escuchado a compañeras que me cuentan sobre la actual distribución de las responsabilidades de los cuidados, asunto que sigue siendo en la mayoría de los casos desequilibrado. Pareciera que, aunque nosotras estamos trabajando continuamente en nuestra deconstrucción, leyendo, practicando feminismos y comprendiendo la importancia del trabajo no remunerado, no es así para ellos.

A pesar de la importancia de compartir los trabajos de cuidados, sigue siendo invisibilizado y desatendido en las políticas económicas y sociales, siendo esto un reflejo de la cultura machista que prevalece y ha aumentado en el marco de la contingencia por COVID–19 en el que las cargas de trabajo para las mujeres se ha incrementado. La ONU menciona que nosotras gastamos el triple de nuestro tiempo en estas tareas en relación con los varones en el marco de las jornadas de sana distancia.

Es en estos momentos que nos encontramos en casa, lo menos que se vive en algunos hogares es la equidad en la repartición de trabajos de cuidados, sin duda una triste situación que se vincula al alza en la violencia doméstica observada en varios países latinoamericanos. Lo peor de todo es que esto coloca a muchas otras situaciones igual de graves al descubierto.

Sin embargo, al centrarnos en el trabajo no remunerado podemos mirar que tan solo en México es el 23.5% de PIB nacional del 2018, según reportó el Instituto Nacional de Estadística y Geografía (Inegi), y el 99% quienes están insertas en el campo laboral también tienen dobles o triples jornadas.

¿Pueden imaginar lo agotador que esto resulta para todas esas mujeres que tienen que dejar de lado el gozo de la maternidad, del trabajo y del tiempo libre?

Si bien muchas relaciones más jóvenes van encontrando o vamos encontrando otras formas de amar, la carga de la repartición pareciera que sigue estando en nosotras, en la búsqueda del diálogo constante para la equidad, esto de pronto se vuelve casi una rebeldía dentro de los hogares más tradicionalistas, esos donde se nos siguen asignando roles de cuidadoras enmarcados en relaciones pasivas. Y es en este punto donde los varones deben cuestionar sus privilegios, generar grupos de encuentro donde se dialogue, cuestione y se generen propuestas para una vida libre de violencias de género y que estén encaminadas en una vida equitativa para todos, todas y todes, donde la ternura radical sea un continuo, ya que no basta con el trabajo que constantemente hacemos nosotras si no viene con el cambio de los varones que se relacionan heterosexualmente.

No me gustaría cerrar el texto aquí, si no abrirlo justo para una retroalimentación continua en la problematización de la repartición de cuidados en el hogar, de cómo esto genera un cambio primordial en una vida más justa para las mujeres. Cuéntame, ¿cómo es para ti?, ¿qué cosas podrías aportar a una vida libre de violencia de género?

<!--EndFragment-->