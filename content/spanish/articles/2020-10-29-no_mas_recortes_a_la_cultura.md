---
title: ¡No más recortes a la cultura!
subtitle: "  "
cover: /images/uploads/defendamoslasinfonica.jpeg
caption: "Foto: Radio JGM"
date: 2020-10-30T11:29:23.029Z
authors:
  - Karol Dinamarca
tags:
  - Chile
  - Música
  - Cultura
  - Educación
  - Presupuesto
  - Arte
  - Latinoamérica
  - Derechos
  - Sociedad
  - Política
  - Economía
categories:
  - Política y Economía
  - Cultura
comments: true
sticky: true
---
El día del concurso desperté animada, confiada y muy alegre. Algo dentro de mí me indicaba que era el momento de alcanzar mi anhelada meta; ganar un puesto en la Orquesta Sinfónica Nacional de Chile. Y asi fué. Después de 2 largos años de preparación y concentración constante, **¡logré mi objetivo!**

Pero este objetivo no fue regalado, ni mucho menos al azar; solo mi familia supo por todo lo que tuve que pasar para poder continuar mi sueño de convertirme en músic@ profesional. Como en ese entonces aún vivía en casa de mi padre, él me vió estudiar desde muy temprano hasta la tarde, e incluso, gracias a que él trabajaba por turnos, muchas veces pude estudiar durante toda la noche. Él también me consoló las veces que perdía algún concurso.

Recuerdo muy bien la frustración que me hacía vivir esa situación, porque era una especie de sensación un poco difusa. Nunca tuve la real certeza de saber si mi participación había sido muy mediocre, o había factores externos que jugaban en mi contra, dejándome fuera del concurso.

Mi maestro de violín de ese entonces, me vió llorar más de una vez, mientras yo intentaba reparar y mejorar los últimos detalles de las obras orquestales que estábamos estudiando para algún "futuro" concurso. Entre las exigencias, recuerdo frases como: *"esa nota quedó baja, tiene que dar con el armónico, búsquela otra vez y corrija, tiene que correjir, vuelve a tocar el pasaje desde mas atrás, no pase de largo ante un error"* etc.

A la vez que recibía exigencias de parte de mi maestro, también tuve su apoyo y comprensión. También recuerdo que una vez me dijo : *"va todo bien Karol, pero los detalles te llevarán a marcar la diferencia entre los demás concursantes y tú, hay que trabajar duro por ellos"*. Gracias a este concurso ganado, también pude alcanzar mi otro gran sueño : **VIVIR DE LA MÚSICA EN CHILE**.

Muy por el contrario de la estabilidad vivida durante el período anteriormente comentado, **hoy todos los elencos estables artísticos de mi país vivimos una dolorosa situación, la cual trata de recortes en los presupuestos, entregados por el Estado a nuestras instituciones culturales.** Supuestamente, debido a la pandemia, se han visto en la "obligación" de hacer un llamado a la austeridad, y han decidido provocar este descalabro económico e incluso la inviabilidad de continuidad sobre nuestros elencos.

Por supuesto, todos estamos dispuesto a entender que el Covid 19 ha provocado estragos en la economía y en la sociedad, y el tema principal no son las consecuencias de esta enfermedad, si no que el eterno menosprecio que viven las artes en general en Chile. **Justamente, Ciencias y Artes son los sectores mas golpeados económicamente, debido al paquete de medidas anunciados por el Gobierno.** Recortes a nivel nacional en lo artístico, probables despidos o desaparición de centros culturales, orquestas, ensambles, músicos sin trabajo. 

En Ciencias ya no habrán becas disponibles para investigación. ¿Cómo es posible que no se realicen más investigaciones y trabajos importantísimos para la Ciencia en Chile, sólo por que no hay dinero disponible en este ítem? **¿Cómo es posible que Chile siga sin invertir en su propia gente?.**

Las interrogantes que se me vienen a la cabeza con respecto a mi propia estabilidad laboral son muchas, pero las preguntas mas importantes son aquellas que tienen que ver con las futuras generaciones de músicos formados en distintas Universidades. Son muchos los titulados en Interpretación Musical que salen de su carrera a un mundo laboral lleno de incertidumbres, con muy pocas posilidades de ejercer su profesión y ganar un contrato en una orquesta, y no precisamente por falta de talento o esfuerzo, si no debido a las pocas orquestas con plazas disponibles que hay en Chile.

No puedo olvidar tampoco a todos los músicos del país que se desviven postulando a becas, fondos o créditos para poder continuar en sus proyectos musicales, para poder otorgar un sueldo a sus colaboradores, para poder continuar sus estudios en algún lugar del mundo, etc.

Como podemos ver, la precarización del mundo laboral artístico en Chile es un tema muy complejo, el cual a todos nos tiene en constante preocupación, porque siendo música, se me hace dificil pensar en trabajar en otro oficio; quise destinar mi vida a la música, y **creo firmemente en que las artes y la cultura, son detonantes en el positivo desarrollo de un país.** Y durante esta pandemia, la gente lo ha confirmado y ha buscado distracciones, entretenimiento, relajo, calma, un momento grato y ha accedido a un montón de espectáculos vía online, conciertos o recitales de distinto tipo de música, teatro y cine.

El obligado encierro nos llevó a refugiar nuestro espíritu en las artes, en un libro, en el aprendizaje de algún instrumento musical, y nos encontramos con la grata sorpresa de que sí, el arte está para ayudarnos, para protegernos de todo lo exterior, de todo lo que no nos hace bien y para nutrir nuestro mundo interior. **El arte nos ayuda a vivir una experiencia única, de mucho regocijo,** tal como escuchar a una orquesta tocando en vivo una revolucionaria sinfonía de Beethoven, o viendo una maravillosa pintura de Monet.

**Termino de escribir esta columna, mientras en Chile se celebra el aplastante triunfo post votación del "Apruebo, Convención Constituyente" Gracias a esto, podremos crear una nueva constitución para Chile, ya no bajo la sombra de Pinochet y sus secuaces, si no que bajo una esperanzadora democracia.**

Una nueva constitución paritaria, en donde realmente, todos tengamos cabida. Y hago un llamado a todos quienes se presenten como constituyentes en la redaccion y creación de esta constitución a que de una vez por todas, entiendan que un país sin cultura, no es nada. Sin arte, no hay desarrollo, tanto como si no hubieran constructoras o grandes empresas. Sin arte no hay salud mental, tanto como si no hubieran doctores o psicólogos. **Sin arte no hay refugio,** no hay forma de canalizar nuestra experiencia humana en este mundo.

**Queremos políticas culturales que nos permitan vivir dignamente de nuestra profesión.** Queremos garantías de que los elencos nacionales artísticos no serán castigados en sus sueldos o en sus puestos de trabajo ante una pandemia o revuelta social. **En fin, sólo queremos dignidad.**

![](/images/uploads/unnamed.jpg)

<!--EndFragment-->