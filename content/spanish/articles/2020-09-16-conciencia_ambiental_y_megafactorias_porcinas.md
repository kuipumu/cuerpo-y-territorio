---
title: "Conciencia ambiental y megafactorías porcinas "
subtitle: Reflexiones desde una perspectiva decolonial
cover: /images/uploads/cccccc.jpg
caption: "Imagen: Equipo de Comunicación del Movimiento de Mujeres Indígenas por
  el Buen Vivir. Ocupación pacífica del Ministerio del Interior de la Ciudad de
  Buenos Aires por parte de un grupo de mujeres pertenecientes al Movimiento de
  Mujeres Indígenas por el Buen Vivir – 11/10/20"
date: 2020-09-16T00:06:56.879Z
authors:
  - Paula Fausti
tags:
  - Argentina
  - China
  - MegaFactorias
  - ConcienciaAmbiental
  - Ambiente
  - MaltratoAnimal
  - MegaFactoriasPorcinas
  - DiaNacionalDeLaConcienciaAmbiental
  - Politica
  - Economia
  - China
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

**Día Nacional de la Conciencia Ambiental**

El 27 de septiembre de 1995, el Senado de la Nación Argentina y la Cámara de Diputados promulgaron la Ley 24.605, declarando el **"Día Nacional de la Conciencia Ambiental"**, en conmemoración al fallecimiento de siete personas a raíz de la inhalación de un gas altamente venenoso, en el partido de Avellaneda, Provincia de Buenos Aires, Argentina. Dicho gas emergió desde la rejilla de la casa de una de las víctimas y fue causado por la acción irresponsable de una empresa que, en forma completamente ilegal, vertió cianuro a las cañerías.

Esta medida buscó incentivar que los establecimientos educativos recuerden los **derechos y deberes constitucionales relacionados con el medioambiente.** La conciencia ambiental pretende conocer el ambiente y las problemáticas socio-ambientales actuales, con el fin de elaborar estrategias en forma conjunta e individual para intentar revertirlas o mejorarlas.

A lo largo de este artículo intentaremos generar una reflexión colectiva sobre **la importancia del cuidado del medioambiente, tomando los aportes de pensadores y pensadoras de Nuestramérica.** Para ello tomaremos como referencia un tema que en Argentina generó mucha repercusión en los últimos meses.

<!--StartFragment-->

### **¿Por qué adoptar una perspectiva decolonial?**

**En términos geopolíticos no es lo mismo habitar la mitad norte del globo, que la mitad sur.** Para entender el complejo entramado desigual y asimétrico en el que estamos, es necesario adoptar perspectivas decoloniales, y para ello necesitamos construir historias, narrativas y conocimientos desde marcos epistémicos propios. Citando al psicólogo Ignacio Martín-Baró: para leerse a sí mismos, los pueblos latinoamericanos necesitan escribir su propia historia.

Por su parte, la socióloga Maristella Svampa afirma que desde la colonización, en América Latina el imaginario de “desarrollo”, basándose en la idea de productividad y crecimiento infinito, operó bajo el ala de un modelo económico hegemónico impuesto. Ella manifiesta que **tanto los gobiernos progresistas, como los conservadores, han estado de acuerdo en aplicar este modelo, sin cuestionamientos o con algunas pocas críticas.** Es decir, que en forma pasiva se aceptó el lugar históricamente asignado por la división del trabajo internacional para América Latina: proveer materias primas. Resuena entonces la frase “Argentina es el granero del mundo”.

Resulta cínico pensar en alimentar al mundo, cuando ni siquiera estamos cerca de garantizar la seguridad alimentaria de nuestra propia población.

### **Megafactorías porcinas: un acuerdo sin licencia social**

**En junio de este año se hizo de público conocimiento un acuerdo entre Cancillería Argentina y el gobierno de China.** Se trata de un proyecto que busca instalar megagranjas industriales de carne porcina para abastecer el consumo de carne de la población en China.

**Dos años antes, en ese país se sacrificaban millones de cerdos de las formas más espantosas imaginables, a raíz de la emergencia de un virus muy contagioso y potencial patógeno humano.**

Sin embargo, en cuanto se tomó conocimiento sobre el acuerdo, no tardaron en hacerse escuchar las voces de miles de ciudadanxs argentinxs, mediante una carta firmada por 300 mil personas. Las redes se llenaron de frases como *“no queremos ser el chiquero del mundo”, “basta de falsas soluciones” y “no al acuerdo porcino con China”.* Desde el gobierno trataron de apaciguar la opinión pública bajando la cantidad de animales que se pretende explotar.

El 25 de agosto se llevó a cabo una movilización popular en forma pacífica en todo el país para tratar de poner en agenda la cuestión ambiental. Cinco días después, **la Cancillería publicó un tweet en las últimas horas del domingo, diciendo que a raíz de la incorporación de un artículo que asegure el respeto por las leyes de protección ambiental, la firma del acuerdo se iba a posponer hasta noviembre.** Si bien esto fue recibido parcialmente como una buena noticia, dejó un sabor amargo, ya que demostró que en el acuerdo original no se estaba teniendo en cuenta el impacto ambiental.

![](/images/uploads/dddd.jpg "Imagen: “Eco House” – Artivismo frente a Cancillería Argentina – 25/08/2020")

**La inconstitucionalidad del proyecto queda evidenciada cuando desde el gobierno no se transmite en forma clara las características del proyecto, quedando todo tras bambalinas en acuerdos cerrados.** A pesar de tantas idas y venidas, continúa sin haber información oficial. Los medios hegemónicos, por su parte, en connivencia con los intereses del agronegocio, difunden poco y nada de información. Se repiten los mismos discursos de siempre para defender prácticas extractivistas: necesitamos dólares para el desarrollo de la economía.

### **Entre los chanchos y la “nada”**

Los discursos de quienes defienden este acuerdo están repletos de maquillaje verde: “buenas prácticas”, “sustentabilidad”, “cuidado del medioambiente”, “desarrollo de las economías locales”. No hace falta hilar muy fino para darse cuenta de que son frases vacías. **¿Serán acaso conscientes de la inconsistencia de sus discursos, y por eso no se animaron a abrir debates públicos sobre el tema?.**

También me pregunto si estas personas consultaron a las comunidades si están de acuerdo con poner las megafactorías en sus territorios, o si escucharon las numerosas propuestas que tienen lxs productorxs locales de alimentos. **¿Habrán visitado aquellas miles de hectáreas destinadas a sembrar alimentos transgénicos, con el fin de alimentar a miles de cerdos hacinados?**

![](/images/uploads/sin_titulo.jpg "Imagen: Martina Perosa para Revista Lavaca – Manifestación pacífica frente a la Casa Rosada – 25/08/2020")

La periodista Soledad Barruti en una reunión que tuvo con funcionarios de Cancillería, cuenta que ellos dicen que en esos lugares no hay nada. ¿Qué será la nada? Cuesta imaginarla. Lo que sí imaginamos que hay son árboles, arbustos, flores, animales, aves, insectos, arroyos, lagunas… en conclusión: vida. **Muchas formas de vida, que sólo hacen eso: viven. Pero al parecer esto es un problema, porque no dan ganancias, no traen inversiones, ni dólares.** Y por eso se merecen el fatídico nombre de la “nada” o “terrenos improductivos” o “zonas de sacrificio”.

**Resuena de repente la llamada “Conquista del Desierto”, título que se le dio al conocido genocidio de pueblos originarios en la Patagonia del sur argentino, iniciada en 1878.** Parece que a pesar de los años hay cosas que no cambian tanto, pues siguen existiendo las “zonas de sacrificio”, la “nada”, el “desierto”, eufemismos usados para perpetuar un modelo genocida y ecocida. Moira Millán, referente mapuche y una de las creadoras del Movimiento de Mujeres Indígenas por el Buen Vivir, lo nombrará “terricidio”.

### **Reflexiones finales**

Svampa dice: *“La humanidad vive tiempos de descuento.”* Resulta abrumador tomar conciencia de lo que está sucediendo en el mundo y en Nuestramérica. Sin embargo, ante tanta desolación se responde con más resistencia y, sobre todo, con propuestas concretas, articulando prácticas y experiencias, combinando viejos y nuevos saberes. **La conciencia y la educación ambiental desde una perspectiva decolonial, nos pueden ayudar a emanciparnos del modelo desarrollista actual, impuesto como universal.**

Contamos con innumerables saberes y conocimientos que ya plantean otras maneras de re-vincularnos con la naturaleza y la Madre Tierra. **No tenemos tiempo que perder: necesitamos construir sociedades más justas y equitativas,** relacionarnos desde un paradigma de cuidados y elaborar políticas públicas orientadas a un “nuevo pacto ecosocial y económico”, que aborde conjuntamente la justicia social y ambiental.

**Considero que esta propuesta es la mejor manera de honrar a las siete víctimas que inspiraron la creación del Día Nacional de la Conciencia Ambiental**, así como a todas las millones de personas (y seres vivos en general) que sufren las consecuencias día a día de este modelo de desarrollo terricida que ya no da para más.

##### **Fuentes citadas:**

* Ignacio Martín-Baró, 1986 - “Hacia una psicología de la liberación”
* Maristella Svampa - \[“El Extractivismo y su resistencia en América Latina”](<* <https://www.youtube.com/watch?time_continue=3170&v=lkKpcIOpqW8&feature=emb_title>>) [](https://www.youtube.com/watch?time_continue=3170&v=lkKpcIOpqW8&feature=emb_title)
* “Terricidio” - \[Instagram live entre Voicot y Moira Millán](<* <https://www.youtube.com/watch?v=RGZ39DdO2b0>>) - [](https://www.youtube.com/watch?v=RGZ39DdO2b0)

<!--EndFragment-->