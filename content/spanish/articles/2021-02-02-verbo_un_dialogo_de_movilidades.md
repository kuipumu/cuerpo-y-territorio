---
title: "Verbo: un diálogo de movilidades"
subtitle: Entrevista a la bailarina y coreógrafa Selene Sánchez
cover: /images/uploads/jefersson_leal_foto_2.jpg
caption: "Fotografías: Jerfesson Leal"
date: 2021-02-02T18:34:18.587Z
authors:
  - Niyireé S Baptista S
tags:
  - Cultura
  - Danza
  - Artes
  - Crisis
  - Venezuela
  - Expresión
  - Género
  - Movimiento
  - Vanguardia
categories:
  - Cultura
comments: true
sticky: true
---
Desde finales de 2016 Venezuela ha experimentado una crisis social y una de profunda inestabilidad económica que ha traído como consecuencia una alta migración de venezolanos hacia otras latitudes. En ese contexto, el medio artístico se ha visto afectado, **grandes figuras de las artes escénicas en el país han asumido otros destinos fuera de este caribeño país**, así como jóvenes intérpretes y talentos nacientes que han decidido migrar en busca de nuevos proyectos, posibilidades de creación, mejoras en su formación y espacios en los cuales mostrar su trabajo artístico.

No obstante, a pesar de esta realidad, en el país aún son muchos quienes siguen en su día a día posibilitando una forma de expresión a través del arte, en especial, cuando se habla de la danza contemporánea: esa búsqueda de un nuevo lenguaje a través del movimiento que permite explorar nuevas formas de relacionamiento con el cuerpo y descubrir las movilidades emergentes. **Venezuela, que en los años 90 del siglo pasado fue bastión de la danza contemporánea en Latinoamérica,** hoy, a pesar de las dificultades, continúa forjando a intérpretes bailarines de gran nivel que incursionan en el medio artístico a contracorriente de la realidad nacional y mundial, lo que ha significado seguir el trabajo en medio de una pandemia y de los conflictos políticos sociales del siglo XXI.

En este punto me permito hablar de la necesidad de creación, del arte como un no lugar `(1)` donde el artista se encuentra con otros, pero crea solo. Me refiero a que a pesar de las circunstancias históricas y sociales en las que grupos de artistas se identifican con un movimiento artístico y que siendo las crisis espacios idóneos para los procesos creativos, también vemos cómo surgen otras formas de innovar en el arte, lo que la historiadora Katty Solorzano define como “la creación pura” `(2)`, **esa etapa de desvíos que no pueden ser encasillados en una periodización diacrónica y son propios de la forma de invención del artista.**

En medio de estas definiciones y contextualizaciones de la atribulada Venezuela, encontramos a la bailarina, intérprete y coreógrafa Selene Sánchez, mujer que danza caminos desde antes de ser parida por las entrañas de su madre. **Sus pies amaron el piso a su primer contacto con él y la cadencia de su cuerpo le dictó las frases a seguir en su formación como artista.** Venezolana, caraqueña de nacimiento, formada en la Universidad Nacional Experimental de las Artes, perteneció a la compañía de baile MUDANZA de Reynaldo Mijares, con la que se presentó en la pieza “Venimos con ellos los míos”.

Ha bailado para Rafael Nieves, Yuli Parica y Ronny Méndez, importantes coreógrafos venezolanos. Ha coreografiado e interpretado las piezas “Raíz” en la Bienal del Sur y recientemente, en el Festival de Mujeres Creadoras 2020, se presentó con “Verbo”, pieza que da continuidad al trabajo que ha desarrollado en los últimos 2 años.

**El trabajo de Selene Sánchez viene a constituir una avanzada en cuanto a la forma de hacer danza en el país.** Su intención es innovar en la danza contemporánea a partir de su técnica dancística, que ha denominado como “Verbo”, un trabajo físico investigativo que nace de su experiencia personal y que la ha hecho hacer consciente de su cuerpo al explorar los códigos que ha identificado en su práctica como bailarina. “La técnica está enfocada en la exploración, primeramente, de movimientos que se generan en las extremidades superiores y como su impulso y acción repercuten en el resto del cuerpo. ¿De qué manera se proyectan sobre él?, ¿hacia dónde me llevan? ¿Cómo es la forma?” A su vez, **“Verbo” busca explorar esa movilidad femenina que permite expresar la sensualidad de los cuerpos al bailar,** profundizando su trabajo a partir de los brazos como eje de acción de la movilidad.

![](/images/uploads/j.jpg)

**Nos interesa mostrar desde la Revista Cuerpo y Territorio las formas en que las mujeres van buscando un espacio para crear y desarrollar su trabajo,** más aún cuando estas formas involucran el cuerpo, lugar de enunciación y portador de múltiples lenguajes que manifiestan la potencia de vida que se abre paso en medio de las turbulencias de nuestros territorios. 

En palabras de Selene Sánchez, **queremos mostrar un pedacito de su quehacer como mujer, bailarina, latinoamericana y creadora en un país donde “están sucediendo muchos acontecimientos importantes, que nos están marcando a nivel general como población e individualmente.** Hechos que para bien o mal, nos hacen movernos hacia una nueva forma de mirar lo que veníamos haciendo. No solo hablo de la pandemia, también hablo de los ritmos musicales y culturales que nos invaden desde afuera y desde adentro”.

**¿Qué te impulsó o cuáles fueron los motivos que te llevaron a Verbo?**

> *Fueron muchos los motivos. Este despertar del cuerpo cuando lo descubres realmente, cuando lo sientes, cuando conectas contigo y entiendes que es lo único valioso que se tiene, es este templo que nos alberga, fue básicamente lo que me impulsó.* 
>
> *Estaba en una búsqueda interna, me encontré en una profunda necesidad de reconocerme y conocer quién era yo cuando bailaba. Esto surgió un tiempo después de haber sufrido una lesión en mi lumbar que paralizó mi trabajo dancístico por un tiempo prolongado, pero que también me hizo ver mis posibilidades desde otra perspectiva.* 
>
> *Empecé a buscar una forma de movilizarme que no expusiera mi cuerpo de una manera drástica o abrupta al bailar. Muchas preguntas empezaron a surgir, cuestionamientos hacia mí y hacia la danza que había venido haciendo: ¿Quién soy realmente al bailar?, ¿Cómo es mi lenguaje corporal?, ¿Poseo una movilidad propia?, etc. Entonces, cuando al fin me encerré en un espacio conmigo nada más, eso que tanto buscaba apareció, nunca de la manera que una se lo imagina, pero ahí se gestó todo.* 
>
> *Empezó mi Verbo, una acción corporal que cada vez se fue pareciendo más a mí, a lo que Selene es en movimiento. Ahora, básicamente lo que me lleva a seguir con esta investigación y creación artística, son mis deseos de hacer de Verbo una técnica de danza contemporánea, que pueda enseñar y transmitir a los y las bailarinas de hoy en día y a las futuras generaciones. Y que se pueda reproducir y reconocer como una técnica propia venezolana.*

![](/images/uploads/jefersson_leal_foto_1.jpg)

**¿Por qué Verbo?**

> *Verbo es lo que soy, en acción y palabra, en escrito y movimiento. Para mí, encontrar un término que definiera lo que estaba vivenciando y tratando de formar en el cuerpo, era necesario, es como si la palabra escrita me permitiera sentir más certero todo aquello que mi cuerpo reproduce en formas y movimientos.* 
>
> *Como palabra, Verbo tiene un gran significado, Verbo es acción, la acción es un movimiento, el movimiento es danza y la danza, no es más que el resultado de muchos verbos corporales (acciones) que vamos desarrollando en el transcurso de nuestras vidas. En la danza contemporánea se vive en la acción constante, bailar nos lleva a vivir experiencias, tener sensaciones, generar registros corporales que codificamos y volvemos parte de nuestro lenguaje; expresamos estados de ánimo, sentimientos, actitudes del día a día para generar un impacto en nuestra forma de movernos, en la interpretación, en la danza que hacemos.* 
>
> *Carlos Paolillo, crítico y promotor de la danza contemporánea del país, menciono que “movimiento y palabra definen la esencialidad del hombre. Cada gesto corporal puede tener su equivalente en la expresión literaria… y que el espíritu y la dinámica del cuerpo son similares a los del ámbito de las letras”. Para mí, sus palabras fueron muy acertadas cuando las leí, sentí que todo lo que yo estaba haciendo hasta ese momento, tenía mucho más sentido.*

**¿Qué significó crear en encierro?**

> *La verdad fue un proceso complejo, difícil, desganador y agotador. En un momento estaba bien, con la energía a tope y después, me sentía en un declive total, sin ganas de hacer algo, sin siquiera procurar volver al movimiento.* 
>
> *Los comienzos y mediados de la cuarentena fueron los perores para mí, para trabajar en mi corporalidad. El trabajo de Verbo se detuvo, quedo a medias muchas veces, no lograba concretar nada físicamente ni escritural. Me deprimí, tuve ataques de ansiedad y bloqueos emocionales que luego se transformaron en energía vital para avanzar y transformar lo que hasta ese momento tenía desarrollado. Crear en encierro representó un momento determinante en mi vida y en mi trabajo. Fue un tiempo de mucha lucha interna conmigo, pero también fue un momento que marcó un antes y un después de mí, de lo que era y lo que ahora soy como mujer y artista. La cuarentena me permitió avanzar en mi corporalidad, en el lenguaje que estaba componiendo y que aun continúo desarrollando. Pero mis ganas de volver a las tablas, a los salones de clases no se acaban. ¡No cambiare esa sensación que te genera estar en vivo y directo con el espacio al danzar!*

**¿Consideras que lo que estás haciendo puede considerarse como vanguardia en Venezuela?**

*Con Verbo, yo busco generar una movilidad que se pueda diferenciar a grandes rasgos de otras. Quiero que cuando las personas vean mis propuestas coreográficas, reconozcan que es mi trabajo por la forma del movimiento. Además, Verbo es una técnica que busca innovar la danza contemporánea del país a nivel de movilidad y contenido.* 

*Comenzando porque es la primera técnica creada por una mujer y, en segundo lugar, porque la movilidad que he construido hasta ahora se basa en las características femeninas que me constituyen. Dado que la danza contemporánea venezolana esta infundada sobre parámetros masculinos, yo busco desvincularme de ello, puesto que la dirección que tomó el estudio de mi movilidad me llevó a indagar en aspectos internos de la corporalidad femenina dejando de lado códigos aprendidos de otros cuerpos para reclamar la naturalidad de mi movimiento. Me gustaría seguir manteniendo esa visión sobre mi trabajo. También, hay otros componentes como el ritmo (música), la forma (del movimiento), el cuerpo (como su elemento), el espacio (uso) y el estilo (características) de la técnica, que hacen que el trabajo a desarrollar sea distinto e innovador.*

> Al finalizar esta entrevista Selene nos comenta: *“La nueva era de la danza contemporánea está llegando, ya no descomponemos el ballet o las figuras y posiciones rígidas que Isadora Duncan, con gran éxito, logró deformar para luego convertirlo en otra cosa. Acudimos a la improvisación, a la exploración de nuestras corporalidades, con otros ritmos, con otros tiempos diferentes a los de la década de los 80. En este nuevo siglo, acudimos al llamado animal, a lo terrenal, buscando posibilidades alternas, desglosando todos los estilos y géneros. Creo firmemente que hay mucho futuro para la danza y para la gente que la hacemos. Hay mucho talento en este país, mucha gente, mujeres y hombres que, como yo, andan en sus búsquedas personales, pero claros de que la evolución del movimiento es necesaria para transformar y mejorar nuestro arte.*

**Referencias:**

`(1)` Marc Augé, Los no lugares: espacios del anonimato. Una antropología de la sobremodernidad. Barcelona-España: Gedisa, 1993.

**`(2)`** Katty Solórzano, “Los abstractos: espacios políticos y sociales del nacimiento de la modernidad artística en Venezuela (1945-1960)”. En El siglo XX venezolano: análisis y proyección histórica de una centuria. Caracas: Fundación Celarg, 2014.