---
title: "Los feminismos en Venezuela "
subtitle: "una caracterización de su lucha en el siglo XXI "
cover: /images/uploads/jessika-paz-6.jpeg
caption: "Fotografía Jessika Paz "
date: 2021-03-31T13:26:24.676Z
authors:
  - Niyireé S Baptista S
tags:
  - Mujer
  - Feminismo
  - Poder
  - Estado
  - Politica
  - Territorio
  - Cuerpo
categories:
  - Cuerpo y Territorio
comments: true
sticky: true
---
El feminismo en Venezuela tiene sus antecedentes en la organización colectiva de mujeres que tomaron los espacios y marcaron sus agendas políticas y de lucha desde 1936, al finalizar la larga dictadura gomecista, hasta nuestros días. **Una de las diferencias del feminismo venezolano en comparación con los feminismos del resto de América Latina es, al decir de Espina (2005), que en el país no existe un movimiento feminista** (como en los casos de Argentina, México o Chile) sino organizaciones y pequeñas colectividades de mujeres que se han agrupado históricamente por la consolidación y demanda de sus derechos.

La trayectoria de las organizaciones feministas en Venezuela en las décadas de los 70, 80 y 90 del siglo XX se impulsó desde los círculos de mujeres pertenecientes a la intelectualidad, universitarias o de los partidos políticos tradicionales de la IV República, enmarcados en las oposiciones entre la izquierda revolucionaria y la derecha liberal: Acción Democrática, COPEI o el Partido Comunista de Venezuela fueron los partidos que protagonizaron el escenario político venezolano de entonces.

**En este contexto, las organizaciones de mujeres que impulsaron las agendas feministas comprendieron la necesidad de avanzar en materia de lucha por los derechos de las mujeres**, lo que significó la obtención de una gran cantidad de logros y la colocación de sus exigencias en la agenda pública. Entre estos avances se cuentan: el comienzo de un marco institucional para las mujeres; la inclusión de la categoría “género” en los ámbitos políticos, sociales e intelectuales; y el posicionamiento de debates como el derecho al aborto, la reforma del código civil o la mejora en las condiciones de vida de la mujer venezolana.

**Un hecho importante a resaltar es que las diferentes organizaciones de mujeres de los distintos sectores políticos del país se llegaron a unificar para el logro de objetivos comunes,** como fue el caso de la CONG (Coordinadora de Organizaciones No Gubernamentales de Mujeres) en el proceso constituyente de 1999, la cual buscaba la inclusión de una gran cantidad de artículos que favorecieran los derechos de las mujeres en la nueva constitución que se estaba gestando, tras el llamado para la elaboración de una nueva Carta Magna propuesta por Hugo Chávez.

## El feminismo desde la revolución

![Manifestación de mujeres feministas](/images/uploads/marcha_feminista_8_marzo_2020.jpg)

Hugo Chávez asume la presidencia de la nación en el año 1999. **Por la necesidad de legitimar su proyecto político, Chávez realizó un llamado a todos los actores de la sociedad civil incluidos los sectores subordinados**: mujeres, LGTB+, indígenas, etc.; que no habían sido tomados en cuenta por los gobiernos del Pacto de Punto Fijo y que eran necesarios para la consolidación del proyecto revolucionario del mandatario. 

En este sentido, el sector mujeres mostró una fuerza organizadora y de lucha en favor de sus derechos, potencial que vislumbró el presidente Chávez y que comenzó a integrar en su política de gobierno, de esta manera las mujeres fueron incluidas en diferentes ámbitos de la vida nacional. **Para el año 2008 Chávez se declara feminista y a partir de ese momento, la revolución bolivariana se apropió de un discurso feminista que impregnó todas sus políticas de gobierno,** a tal punto que se dijo que en Venezuela se estaba construyendo un “socialismo feminista”. 

Dentro de la efervescencia arrolladora de la revolución muchas mujeres se sintieron llamadas a participar y comenzaron a identificarse con las propuestas emanadas desde la institucionalidad para el logro de una revolución feminista y socialista. **En así como comenzaron a emerger actoras políticas que representaban la nueva ola del feminismo venezolano:** mujeres de todas las edades y de diferentes estratos sociales, cuya adolescencia e identidad política se definió en los primeros años del chavismo, se integraron en nuevas organizaciones, tomaron los espacios políticos y exigieron demandas populares y sociales en torno a las luchas de las mujeres.

Desde el gobierno y de la mano de estas feministas institucionales se empezó a orientar un discurso en torno al feminismo y a los logros de las mujeres que colocó a Hugo Chávez como el “Mesías” de la causa, el protagonista de la consolidación de dichos logros y se vació de significados las luchas históricas impulsadas por las mujeres en los años previos al proyecto bolivariano.

 Chávez se constituía como el nuevo significante, como el hombre, padre, hermano, hijo, e incluso, amante, que daba sentido a las luchas de las mujeres; era el símbolo, el mito, el discurso en la subjetividad del feminismo venezolano. En los códigos discursivos y en la forma de representación las mujeres y feministas agradecían a la figura de líder todas las dádivas entregadas a la causa.

 El chavismo logró captar y unificar bajo sus enunciados revolucionarios todas las identidades políticas; todas y todos se sentían representados en los códigos y formas de apropiación discursiva de la revolución. Se dejó a un lado la autonomía de las organizaciones y se establecieron las agendas políticas de la mano de los intereses del gobierno. 

La agenda propia de las organizaciones feministas se mimetizó en las exigencias cada vez mayores de la institucionalidad. El pacto estaba claro, lo que se debía mantener a toda costa era la revolución, la agenda de las mujeres podía esperar, ante un panorama cada vez más polarizado políticamente entre los sectores de oposición de derecha y el chavismo.

## El feminismo venezolano en la actualidad

![Fotografía Las Comadres Purpuras ](/images/uploads/las-comadres-purpuras-10.jpeg)

Después de la muerte de Chávez y la llegada de Nicolás Maduro al poder muchos de estos elementos han permanecido, incluso la declaración de Maduro hace algunos años como feminista y la “integración” de mujeres en el espacio político. **No obstante, estas acciones nada tienen que ver con un avance real del país hacia el feminismo** y menos con una intención de integrar realmente a las mujeres dentro de la política nacional. 

En todo caso, se trata de un intento por mantener el control de los diferentes sectores políticos cada vez más en desavenencia con las políticas de Estado. Las mujeres no son llamadas a integrar la nación, el discurso que ha permanecido desde la institucionalidad, y principalmente de la mano de Nicolás Maduro, ha sido a la reprimenda a los sectores de mujeres a que salvaguarden la revolución “como sea”, cuestión que si bien ya se hacía con Chávez ahora se ha intensificado y se pondera a la mujer como la defensora de la patria y en cuyos úteros descansa el futuro del país. 

**La política de Nicolas Maduro hacia las mujeres ha sido abiertamente maternalista, se les llama a cumplir los roles establecidos de madre, esposa y cuidadora, no solo de sus hijos y familia, sino también del país entero**, y ello en medio de la debacle política y de la situación de crisis que se vive actualmente, en donde son las mujeres las más afectadas.

En este escenario, los feminismos en el país han tomado diferentes caminos y son tres las posturas que la autora de este artículo ha identificado:

Una primera postura tiene que ver con las organizaciones e individualidades que, a pesar de no colindar plenamente con las medidas del gobierno y expresar, ya sea abiertamente o no, su molestia con las acciones de gobierno, en especial con las relacionadas a las mujeres, siguen ancladas a la identificación con un proyecto nacional revolucionario y a la figura de Chávez, con un vínculo amoroso y de respeto, paternal y edípico. En este sentido, para este sector asumir una ruptura total frente al gobierno de Maduro implica que sus identidades políticas se vean en juego.

**Estas actoras se dan cuenta de las vicisitudes de las mujeres en medio de las acciones gubernamentales y de las amplias políticas evangelizadoras y liberales,** que nada tienen que ver con feminismo y menos con el fin del capitalismo por parte del gobierno, pero aún se sienten llamadas desde algún lugar emocional a mantener los lazos y los vínculos con la institucionalidad, a pesar de que siguen exigiendo respuestas del Estado y pidiendo la visibilización de la agenda de las mujeres.

Por otro lado, está la postura de las feministas que han roto con el Estado y su gobernanza. Su lugar de enunciación es desde la protesta y la visibilización de las problemáticas de las mujeres frente a una cúpula gubernamental que no da respuestas. **Estas organizaciones han mostrado altos índices de violencia hacia las mujeres que se viven actualmente en el país, además de las condiciones infrahumanas en las que se encuentran los derechos de las mujeres.**

 En este grupo están feministas que antes se identificaron con el proyecto político iniciado por Chávez, así como mujeres de oposición no tradicional y nuevas identidades emergentes: jóvenes que han alcanzado su identidad política en la segunda década de lo que va de siglo. Este grupo aboga por la búsqueda de un punto en común en sus agendas de lucha dentro de la propia diversidad de sus feminismos para lograr acciones contundentes de denuncia al Estado venezolano.

Por último, se encuentra un grupo político de individualidades feministas que se han separado rotundamente del proceso político revolucionario y con las propuestas de la oposición. **Son sujetas que no hacen vida en ninguna organización, partido o colectivo y cuyas identidades políticas primigenias se fracturaron en medio de la gran crisis de significantes** que ha tenido el imaginario político venezolano. Son otredades que no encuentran un lugar de enunciación colectiva y que están en esa búsqueda de identificación con otras formas de hacer política y de vivir el feminismo.

**Ante este panorama es necesario evidenciar un sector de mujeres dentro de la institucionalidad estatal y cuya posición política sigue favoreciendo las actuaciones del gobierno venezolano y que se identifican con el feminism**o, pero que no han sido integradas dentro de la caracterización de los feminismos venezolanos actuales porque son mujeres que dentro de la institucionalidad han ejercido o ejercen posiciones de poder. 

**Un poder que va de la mano con las prebendas que el gobierno les ha otorgado en medio de la situación de crisis social.** Este grupo, en un intento por mantener un “discurso feminista”, que dista mucho de serlo y más se acerca a una caricaturización del feminismo liberal con máscara de revolucionario, son actoras que integran la hegemonía del discurso del gobierno, sujetas que han venido acaparando y totalizando los diferentes espacios de representación del feminismo venezolano y se han autoimpuesto arbitrariamente el derecho de hablar por “todas” las mujeres en Venezuela. 

Este grupo de mujeres, si bien es una minoría política, cuentan con el apoyo de la dirigencia gobernante y defienden un feminismo de papel. Este sector de mujeres, a pesar de darse el epíteto de feminista, no critica la evangelización del Estado venezolano en el gobierno de Maduro, no habla de los altos índices de muertes de mujeres por las condiciones infrahumanas del sistema de salud, no expone cifras sobre los niveles de violencia hacia las mujeres y mucho menos denuncia la violencia que ejerce el propio Estado venezolano contra las mujeres y el resto de la población.

A los feminismos venezolanos aún le queda mucho camino por recorrer y ahora más que nunca es necesario tomar el espacio que les corresponde.

### Referencias

JIMÉNEZ, Morelba; Mujeres protagonista y el proceso constituyente en Venezuela (2000).

ESPINA, Gioconda; ¿Movimiento de mujeres o mujeres en movimiento? El caso Venezuela. (2005).