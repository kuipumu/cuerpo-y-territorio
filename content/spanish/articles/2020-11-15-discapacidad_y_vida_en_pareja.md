---
title: Discapacidad y vida en pareja
subtitle: "  "
cover: /images/uploads/discapacidad-1-1170x764.jpg
caption: "Imagen: Feminacida"
date: 2020-11-14T23:12:36.031Z
authors:
  - Federico Scheuer
tags:
  - Discapacidad
  - DiscapacidadIntelectual
  - Inclusion
  - Amor
  - Diversidad
  - Argentina
categories:
  - Diversidad
comments: true
sticky: true
---
> *`“No se casa el que quiere; hay pocas probabilidades de conseguir fundar una familia cuando no se reúnen ciertas condiciones de salud, de fortuna y de moralidad. Los que no las satisfacen (…) resultan, de buen o mal grado, devueltos a la categoría de los solteros, que comprende, de este modo, todo el desecho humano del país. En ella se encuentran los disminuidos, los incurables, la gente demasiado pobre o notoriamente tarada.”``(Emile Durkheim, 1897)`*

El Estudio Nacional sobre el perfil de las Personas con Discapacidad en Argentina, llevado a cabo por el Indec `[1]`, se dio a conocer en 2018 y constituye la más reciente y representativa matriz de datos respecto a la población con discapacidad. **En esta oportunidad intentaré poner la lupa sobre la situación de las personas con discapacidad intelectual (PCDI) en cuanto al lugar que ocupan en el entramado de nuestra sociedad** (y más precisamente en torno a una de sus instituciones más “primarias”), sirviéndome de los datos que arrojó el mencionado trabajo. **En entregas anteriores he procurado avivar este debate tan valioso para los tiempos actuales.**

**La variable cuyo comportamiento observaremos es la convivencia en pareja en la población con discapacidad.** A condición de aceptar que el grado de convivencia en pareja de la población estudiada “podría constituir un indicador de su integración social”`[2]`, tal como manifiesta el informe del Indec, será plausible avanzar en busca de algunas correlaciones, pero caben algunos comentarios a este respecto.

Si consideramos la pareja como forma específica que asume el lazo social; como relación y regulación promotora de solidaridad y reciprocidad; como interacción del deseo propio con el deseo del otre; y la convivencia como instancia de consolidación del vínculo entre pares y pasaje a la vida adulta; estaríamos ante puntos sustantivos en lo que hace al tejido social. De la amistad podría elaborarse una aproximación similar, pero no se suele contar con datos al respecto.

La premisa que guía este trabajo podría formularse de la manera siguiente: **la frecuencia con que determinada categoría poblacional convive en pareja expresa de alguna manera la intensidad con que se le incluye en el lazo social.** ¿Son debatibles estos supuestos?, indudablemente sí. Infinidad de proposiciones podrían esgrimirse sobre la institución de la pareja, acerca de sus transformaciones en la historia, así como de la sexualidad, los comportamientos y las violencias que la tienen por escenario. Nos conformaremos, entonces, con la simple consideración de que la vida en pareja es una forma de unión lo suficientemente difundida y valorada como para tomarla por indicador de ese “lugar” en el entramado social que estamos intentado captar.

En ese sentido, grados relativamente altos de estigmatización (entendida como disvalor socialmente atribuido “a simple vista”) se corresponderían con proporciones de convivencia en pareja por debajo de los valores convencionales para el conjunto de determinada sociedad. **Esta proposición implica el enfático rechazo de lecturas infantilizantes que pudieran sugerir que las PCDI no ingresan en la vida adulta a causa de su propia condición, desestimando la centralidad de los condicionamientos sociales que en el campo de la discapacidad solemos denominar barreras.**

![](/images/uploads/sin_titulo.jpg)

<!--StartFragment-->

Observemos el gráfico 1. La barra superior, que exhibe las cifras del censo 2010 para toda la población mayor de 14 años, puede servir hasta cierto punto como referencia para el análisis de los grupos correspondientes a distintos tipos de dificultad en torno a la convivencia en pareja. En ese sentido, si comparamos la población con discapacidad visual, auditiva o motriz (bastante cercanas a los valores del censo) con aquella que el estudio denomina mental-cognitiva, vemos una caída llamativa en detrimento de ésta última. **Algo del orden del estigma empieza a vislumbrarse.** Pero es necesario avanzar todavía más.

Para refinar este análisis y ver que la caída es aún más pronunciada para algunes, vamos a procurar identificar con mayor grado de precisión a la población con discapacidad intelectual, distinguiéndola de aquellos casos también contenidos en la categoría “dificultad mental-cognitiva” por tener algún padecimiento mental pero sin estar comprometidas las capacidades intelectuales (por ejemplo, personas con diagnóstico de depresión, trastorno de la personalidad, etc).

Para lograr dicha aproximación a una distinción no contemplada en el estudio del Indec, nos serviremos del cruce de nuestra variable (la convivencia en pareja) con otra de gran interés, la edad a la que se presentó la dificultad.

Por definición de AAIDD`[3]`, la discapacidad intelectual se origina antes de los 18 años, de manera que atendiendo a la segmentación según edad de inicio de la dificultad del subgrupo “mental-cognitivo” nos acercamos más al fenómeno de interés.

![](/images/uploads/sin_titulo2.jpg)



**A medida que nos acercamos a la edad de inicio de la dificultad que caracteriza a la discapacidad intelectual, vemos disminuir drásticamente la frecuencia de la convivencia en pareja.** Llegando, para aquellos cuya dificultad se manifestó entre el primer y los catorce años (*categoría que contendría al grueso de las PCDI) apenas a un 11%.

Ahora que la brecha está mejor captada, se puede sostener que **existe cierta lamentable excepcionalidad de las PCDI en cuanto a la intensidad del estigma,** incluso respecto del resto del colectivo de la discapacidad cuyo grado de convivencia no dista demasiado de los valores convencionales para Argentina.

*“No tienen que decidir por nosotros. Tenemos que decidir nosotros si queremos tener novio o novia”* opinaba Federico Rodríguez, miembro del Movimiento Argentino de Autogestores, en conversación con Cuerpo y Territorio semanas atrás. `[4]` Está claro que hay una arena política donde disputar esas habilitaciones a nivel del micropoder; donde las instituciones “a cargo” de las personas (familias y profesionales tratantes) deben honrar las decisiones de sus miembros o usuarios en torno a sus expectativas de tener pareja o no.

Pero yendo un poco más lejos (y aquí abandonamos los datos para adentrarnos en el denso magma de la opinión) **cabe considerar en qué grado persiste cierta tendencia racialista que alimenta un imaginario social donde, en el “mejor” de los casos, las personas con discapacidad intelectual deberían formar parejas entre ellas en tanto seres “especiales” que se merecen mutuamente.** Este imperativo de no mezcla se despliega en varias esferas de la vida de las PCDI como la amistad, el aprendizaje y las ocupaciones.

Para peor, encontramos discursos que apoyándose en la idea de diversidad (distorsionada de manera antojadiza) reproducen la permanencia en el oneroso circuito de instituciones “para diversos” que configuran el ya mencionado apartheid sutil. En ese sentido, **el cambio cultural necesario implica promover, instituir (y financiar, claro) con determinación y creatividad, la convivencia entre personas con y sin discapacidad en todas las esferas de la vida.** En la medida en que sigamos sin conocernos mutuamente en condiciones de simetría y reciprocidad, difícilmente tengamos la sociedad diversa que supuestamente nos interesa tanto construir. ¿O acaso somos, en ciertos aspectos, asimilables a esa sociedad cuyos valores describía Durkheim a fines del Siglo XIX a través del lente positivista?...

\---

`[1]` Instituto Nacional de Estadística Y Censos de la República Argentina.

`[2]` [](https://www.indec.gob.ar/ftp/cuadros/poblacion/estudio_discapacidad_12_18.pdf)[](https://www.indec.gob.ar/ftp/cuadros/poblacion/estudio_discapacidad_12_18.pdf)[INDEC](https://www.indec.gob.ar/ftp/cuadros/poblacion/estudio_discapacidad_12_18.pdf)

\[3] [AAIDD](https://www.aaidd.org/docs/default-source/sis-docs/aaiddfaqonid_template.pdf?sfvrsn=9a63a874_2#:~:text=What%20is%20the%20official%20AAIDD,before%20the%20age%20of%2018.)

\[4] [](https://revistacuerpoyterritorio.com/2020/10/16/ningun-lugar-en-especial/)["Ningún lugar en especial" - Revista Cuerpo y Territorio](<https://revistacuerpoyterritorio.com/2020/10/16/ningun-lugar-en-especial/>)

Durkheim, Emile (2013); “El suicidio”. Buenos Aires. Terramar.