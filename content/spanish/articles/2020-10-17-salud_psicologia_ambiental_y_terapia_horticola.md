---
title: Salud, Psicología Ambiental y Terapia Hortícola
subtitle: " "
cover: /images/uploads/00.jpg
caption: " "
date: 2020-10-17T18:44:25.794Z
authors:
  - Paula Fausti
tags:
  - Naturaleza
  - Ambiente
  - Ecología
  - SaludMental
  - PsicologiaAmbiental
  - Tierra
  - Naturaleza
  - Territorio
  - Salud
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

¿Alguna vez oyeron hablar de la Psicología Ambiental? ¿Y de la Terapia Hortícola? En esta nota voy a contar un poco de qué tratan estas ramas de la psicología que no son muy conocidas, pero que creo cobrarán más visibilidad en los próximos años. Sin embargo, antes de entrar en definiciones, me gustaría compartir algo de mi recorrido personal para que se entienda por qué creo que es tan importante darlas a conocer.

Hace ya más de 10 años elegí estudiar Psicología en una de las universidad públicas más prestigiosas de Lationamérica y Argentina. El recorrido no fue fácil, los dos marcos conceptuales más ofrecidos eran el psicoanálisis y la teoría cognitiva-conductual, ambos presentados desde el trabajo clínico y privado.

En lo personal, por una u otra razón, no me sentía cómoda con ninguna de las dos, ni tampoco con ejercer la profesión de esa forma. Afortunadamente en el último año de la carrera tuve la suerte de cursar dos materias optativas (en relación al trabajo comunitario y a los estudios de género) que me encantaron y gracias a ellas no desistí de ejercer la profesión.

Sin embargo, me esperaba aún un largo camino por delante, tratando de encontrar la forma de volcar mis conocimientos en la práctica. **Me continué formando por fuera de la academia, enriquecí los saberes adquiridos con la experiencia, busqué en espacios poco conocidos (y reconocidos) dentro del ámbito de la salud, moviéndome en las periferias de la profesión.** Gracias a esta búsqueda, en los últimos años pude conocer la Terapia Hortícola y más recientemente la Psicología Ambiental, especializaciones que lejos están de siquiera ser mencionadas en la academia y en universidades de prestigio.

![](/images/uploads/01.jpg)

Desde chica empecé a desarrollar sensibilidad hacia la naturaleza y los problemas ambientales. Sin embargo, como mi fuerte nunca fueron las “ciencias duras”, cuando me tocó elegir una carrera para seguir estudiando, opté por las ciencias sociales. Me cuestioné mucho la decisión de estudiar Psicología, pues si bien me gustaba, sentía que una parte muy importante de mi vida estaba siendo relegada. **¿Y es qué por qué tenemos que dividir tan tajantemente los estudios humanísticos de los estudios de las ciencias naturales?**

Afortunadamente en mi vida pude viajar mucho y eso me permitió seguir nutriendo el vínculo con el mundo natural. Me llené de ricas experiencias que re-afirmaron mi amor por la naturaleza. Pero algo no andaba bien… **A pesar de estar estudiando la carrera de psicología, de conocer numerosas teorías sobre la psiquis, la conducta y el comportamiento humano, yo no era capaz de entender lo que me estaba pasando.** Me había sumido en una leve depresión de la que no lograba salir. Me costaba comer, levantarme de la cama, salir a la calle era prácticamente un esfuerzo sobrehumano.Tuve el privilegio de contar con una familia y amistades que me apoyaron. También pude empezar terapia con una psicóloga que me ayudó muchísimo. 

Sin embargo, fue a raíz de dos modificaciones sustanciales que adopté en mi vida cotidiana que empecé a sentir cierta movilización interna: por un lado dejé de tomarme colectivos y empecé a moverme en bicicleta; y por otro lado, comencé a cultivar plantas en el jardín de mi casa. Dos grandes cambios: mover el cuerpo y amasar la tierra con mis manos. Puede sonar bastante básico, pero eso no estaba en ningún libro de psicología (de hecho en la carrera prácticamente el cuerpo no se menciona y menos que menos la capacidad de sanarnos que tiene la naturaleza). **Re-conectarme con la naturaleza me permitió re-conectarme conmigo misma y con las ganas de vivir.**

![](/images/uploads/02.jpg)

Con el tiempo me las ingenié para hacer diversos cursos y talleres sobre jardinería, huerta agroecológica y fitocosmética. **Aprendí a compostar, a tener huerta en la terraza, y a armar mis propios preparados medicinales.** Poco a poco pude ir conociendo la flora y fauna que nos rodea. Aprendí los nombres de las aves que todos los días se hacen presentes con sus melodías y colores en el cielo. Descubrí plantas comestibles que crecen entre las baldosas y terrenos baldíos. Aprendí a identificar los árboles que nos brindan sombra en verano. Recolecté semillas, planté árboles nativos y traté de contagiar el amor por la naturaleza en cada lugar al que iba.

Hundir mis manos en la tierra, observar los colores de las mariposas y las flores, oler la tierra mojada luego de regarla, oír el canto de los pájaros. **Amar a la tierra es amarla con todos los sentidos, es sentirnos parte de ella y sentirla a ella como parte nuestra.** Es sentir empatía por todos los seres vivos que la habitan. Es querer cuidarla y respetarla.

Existe dentro de la Psicología Ambiental el concepto de **“ambientes restauradores”**,que se puede definir como la capacidad de los ambientes que incluyen elementos naturales, de proporcionar bienestar (Mozobancyk; 2016). Numerosos artículos y estudios fundamentan empíricamente que estar en conexión con la naturaleza afecta positivamente a nuestra salud y calidad de vida, e incluso puede ayudar a recuperarnos de enfermedades. Por eso se dice que la naturaleza tiene una capacidad “restauradora” sobre nuestro cuerpo/mente/espíritu.

**Antes de conocer este concepto basado en evidencia empírica, yo ya lo había experimentado en carne propia. Y creo que no debería sorprendernos, ya que somos y formamos parte de la naturaleza,** aunque como humanidad nos hemos alejado drásticamente de ella.

Sabemos que en las ciudades se manejan ritmos vertiginosos y nos vemos constantemente bombardeadxs por estímulos, generando muchas veces sensaciones de estrés y sufrimiento mental de diferente tipo. También el hecho de vivir fragmentadxs, sin sentirnos parte de una comunidad que nos de identidad, algo muy común en las grandes ciudades, nos puede llevar a tener sentimientos de soledad, a pesar de que paradójicamente estemos rodeadxs de personas.

![](/images/uploads/03.jpg)

**La Psicología Ambiental, ahora me pongo un poco más específica, se ocupa de los aspectos psicológicos y psicosociales del ambiente.** Es decir que se centra en la dimensión psicológica del ambiente, estudiando a individuos y comunidades en relación con su entorno físico y social. Es una rama de la Salud Ambiental, que por su parte estudia los efectos que el ambiente tiene sobre la salud de las personas. 

**Si consideramos que la salud es integral y no sólo la ausencia de enfermedad, desde esta disciplina podemos focalizarnos en promover ambientes saludables, contribuyendo a mejorar la calidad de vida de la población.** Esto implica considerar al ambiente como generador de salud, bienestar y calidad de vida (Mozobancyk, 2016), desde un paradigma de promoción de la salud. Algunas de las contribuciones posibles de la Psicología Ambiental pueden clasificarse en tres grandes áreas: educación ambiental, gestión y planificación ambiental e investigación (Mozobancyk, 2016).

En relación a la Terapia Hortícola, la página de la Asociación Argentina de Terapia Hortícola la define como una actividad de encuentro de lo humano con su propia naturaleza, es decir con el mundo natural que nos precede y del que formamos parte. Este tipo de terapia reconoce el trabajo en la naturaleza como un medio terapéutico de rehabilitación mental, física y social. Se desarrolla a través de actividades que contactan a las personas con la naturaleza, como la jardinería, la huerta, los paseos o visitas, talleres, encuentros grupales, etc. Busca la conexión con el aquí y ahora, tratando de generar un momento de puro presente, a través del trabajo con la tierra. **Por eso para la Terapia Hortícola no es importante aprenderse el nombre de las plantas, sino más bien conectarse con ellas mediante los sentidos: oliendo, tocando, observando, incluso oyendo y degustando.**

Podría continuar definiendo y explicando, pero no me quiero extender mucho. Espero con estas breves palabras haber sembrado alguna semillita de curiosidad y deseos de profundizar en el aprendizaje sobre ellas.

Creo que necesitamos replantearnos mil cosas sobre cómo queremos vivir, y ya lo he mencionado en artículos anteriores. **A veces es tanto lo que hay que hacer que abruma. Pero podemos empezar por lo chiquito: desde andar en bici hasta empezar a compostar.** Comprar bolsones de verdura agroecólogica o a cooperativas, averiguar por ferias barriales, juntarse con lxs vecinxs y organizarse, armar comunidad. Priorizar la existencia de espacios verdes también es fundamental, sobre todo si tenemos en cuenta que en CABA en los últimos años se vendieron 500 hectáreas de espacios públicos que hoy podrían ser parques y plazas.

El contexto de pandemia que nos toca atravesar trajo en algunas personas la necesidad de re-valorizar los ambientes naturales, los espacios verdes, las horas de recibir el sol sobre la piel. Esta necesidad de valorar la naturaleza viene acompañada también de la toma de conciencia de que como humanidad estamos atravesando un momento histórico de colapso ecológico.

![](/images/uploads/04_1_.jpg)

**Es urgente responder al llamado de volver a la tierra, para proteger lo que nos queda y para re-conectarnos con esa parte nuestra que sigue latente en nuestros cuerpos y espíritus.** Como mencioné en un artículo anterior, citando a la feminista comunitaria Lorena Cabnal, necesitamos sanar nuestros territorios-cuerpo-tierra. Para ello lxs invito a comenzar por escuchar sus propios procesos vitales, psíquicos, espirituales, físicos. Y re-conectar poco a poco con la naturaleza.

**Fuentes citadas:**

* “De la salud ambiental a los ambientes saludables. Aportes desde la psicología

ambiental.” (2016) Schelica Mozobancyk - Revista diálogos

* [ Blog de la Asociación Argentina de Terapia Hortícola](<* <http://terapiahorticola.blogspot.com/>*>)
* Lorena Cabnal 

  {{< youtube v=6CSiW1wrKiI >}}



<!--EndFragment-->