---
title: El verde en el gris
subtitle: Espacios verdes urbanos, salud e infancias
cover: /images/uploads/00_1_.jpg
date: 2020-11-15T02:09:14.487Z
authors:
  - Paula Fausti
tags:
  - Territorio
  - Salud
  - Ambiente
  - Buenos Aires
  - Ciudad
  - EspacioPúblico
  - AireLibre
  - Parques
  - EspacioVerde
  - Urbe
categories:
  - Territorio
comments: true
sticky: true
---
¿En qué se relaciona el ambiente y la salud?, ¿influye en nuestro desarrollo el contexto en el que vivimos?, ¿qué implica construir “ciudades más saludables”?, ¿cómo se relaciona esto con los espacios verdes y las infancias?

**Numerosos estudios científicos han revelado que en la primera infancia, particularmente hasta los dos primeros años de vida, existe el llamado “período crítico”**. Es en esta etapa cuando se produce la organización neuronal, base de todos los aspectos del desarrollo: cognitivo, emocional, comportamental, etc. Esto quiere decir que si el cerebro no llegara a completar adecuadamente sus etapas iniciales de desarrollo, se entra en riesgo de generar déficits permanentes para el crecimiento posterior de la persona. Las consecuencias no necesariamente serán irreversibles, ya que el cerebro cuenta con una enorme capacidad de plasticidad. Sin embargo, en términos de prevención y promoción de la salud, resulta clave abordar las problemáticas relacionadas con el desarrollo integral de las infancias.

Como señalé en mi último artículo (ver [“Salud, Psicología Ambiental y Terapia Hortícola](https://revistacuerpoyterritorio.com/2020/10/17/salud-psicologia-ambiental-y-terapia-horticola/)) la psicología ambiental se encarga de la dimensión psicológica del ambiente, estudiando a individuos y comunidades en relación con su entorno físico y social. Los ambientes pueden clasificarse (por ejemplo, en vivienda, barrio, escuela y ciudad) y son muy relevantes para el desarrollo de lxs niñxs. Esto se debe a la gran cantidad de tiempo que pasan en ellos y a que están dotados de afecto, pues es en estos lugares donde las niñeces entablan la mayor parte de sus relaciones significativas. De allí se evidencia su capacidad de promover o inhibir el desarrollo infantil.

**Los espacios públicos de la ciudad más cercanos a lxs niñxs son la calle, las plazas y los parques. Sin embargo, es un problema cuando dejan de ser adecuados para la exploración y el juego, y pasan a tener potenciales peligros.**

![](/images/uploads/01_1_.jpg)

Posiblemente recordemos los relatos de nuestros abuelos o abuelas de cuando eran niñxs y jugaban en la calle con amigxs del barrio. La calle en antaño era un lugar de socialización y encuentro, sin embargo esto ha ido cambiando con el tiempo. A través del proceso de privatización del espacio público y de la priorización de la circulación del transporte automovilístico, se ha ido desplazando a las personas del espacio urbano. **La calle se terminó conformando en un mero lugar de paso.**

En cuanto a los parques y plazas, también se observó un proceso de decrecimiento en su utilización. Esto se debe a que en ocasiones son percibidos como espacios inseguros, debido a por ejemplo el mal mantenimiento de los juegos, el tráfico cercano, la suciedad, la presencia de adultxs desconocidxs, etc. De esta forma los lugares de encuentro fueron transfiriéndose progresivamente al ámbito privado, por ejemplo shoppings, clubes y escuelas. **Las infancias han ido perdiendo progresivamente autonomía para circular por la ciudad, mientras que al mismo tiempo se fueron profundizando las desigualdades según la clase social.**

![](/images/uploads/02_-_parque_avellaneda.jpg "Parque Avellaneda, Buenos Aires.")

En la actualidad existen estudios provenientes de la psicología ambiental que revelan la importancia del contacto con la naturaleza para un desarrollo infantil saludable. Una serie de investigaciones señalan que dicho contacto fortalece la promoción de la salud, en tanto mejora las capacidades perceptuales, de concentración, expresivas, imaginativas y de vinculación interpersonal. En cuanto a las patologías, los ambientes naturales contribuyen a soportar factores estresantes e, incluso a prevenir trastornos psicológicos como la ansiedad y la depresión. **Por último, cabe destacar que cuando el juego tiene lugar en entornos naturales suele ser más imaginativo y creativo, que cuando se da en entornos construidos, además de favorecer la agilidad física y promover la conexión con el mundo natural.**

Sabemos que las ciudades pueden ser grandes causantes de estrés, debido a por ejemplo la cantidad e intensidad de estímulos con los cuales vivimos cotidianamente, tales como la contaminación sonora, el tráfico automovilístico, la intensidad de circulación peatonal, las altas construcciones, etc. En este sentido, los beneficios de contar con espacios verdes públicos son numerosos e invaluables no sólo para las infancias, sino para todxs lxs habitantxs. **Cabe resaltar en este punto que una ciudad hecha a la medida de las infancias es una ciudad inclusiva para todxs.**

![](/images/uploads/03_-_plaza_lezama.jpg "Plaza Lezama, Buenos Aires.")

Los espacios públicos verdes además traen otros beneficios tales como la absorción de dióxido de carbono, la amortiguación de la contaminación sonora, la regulación de la temperatura, la absorción hídrica, el refugio de flora y fauna nativa, etc. No podemos dejar de mencionar el actual contexto de pandemia que nos toca atravesar, donde se hizo evidente la necesidad de contar con ambientes públicos amplios y de libre circulación.

La OMS fija como óptimo la existencia de entre 15 y 20 m² de espacio verde por habitante, sin embargo en CABA se computa que apenas existen 4m². **En los últimos 13 años el Gobierno de la Ciudad, junto a otros bloques políticos, permitió e incentivó la privatización (y concesión) de prácticamente 500 hectáreas de espacio público.**

Actualmente existe el proyecto, presentado en la Legislatura por el gobierno porteño, de privatizar terrenos públicos que dan al río, para construir un barrio de lujo en Costa Salguero. Si bien el proyecto incluye la existencia de un parque, en realidad se trata de una nueva avanzada sobre los espacios públicos, con el agravante de que son pocos los terrenos que quedan al lado del río. **Este negociado claramente favorece sólo a los grandes desarrolladores inmobiliarios, en detrimento de la ciudadanía, pues tampoco ayuda a resolver el déficit habitacional.** Afortunadamente la justicia porteña concedió una medida cautelar que frenó temporalmente la venta de estos terrenos, sin embargo es importante aclarar que la sentencia no suspende el tratamiento del proyecto de ley en la Legislatura, sino que establece que hasta la finalización de la causa judicial no se puede vender el predio.

![](/images/uploads/04_-_parque_de_la_memoria.jpg "Parque de la Memoria, Buenos Aires.")

En contraposición a esto existe un proyecto de ley presentado por la Coordinadora “La Ciudad Somos Quienes La Habitamos” que busca la creación del parque público “Nuestro Río” y la desprivatización de la costanera norte. Esta organización nuclea diferentes asociaciones civiles y vecinxs autoconvocadxs que buscan generar una mayor participación ciudadana. Para más información recomiendo mucho seguirles en sus redes y visitar su página [/observatoriociudad.org](https://observatoriociudad.org/)

**Considero que esta iniciativa es muy valiosa, ya que busca generar soluciones en forma transversal, participativa y a través de vías democráticas, que es lo que se necesita para cambiar el rumbo de las políticas de privatización llevada a cabo por las últimas gestiones de gobierno.** Sin embargo, la avanzada es muy grande, por lo que es necesario que desde la ciudadanía se tome conciencia de esta problemática y también sobre los recursos disponibles para transformarla.

![](/images/uploads/05_parque_de_la_ciudad.jpg "Parque de la ciudad, Buenos Aires")

En conclusión, desde un punto de vista salutogénico, se entiende que una ciudad es saludable cuando brinda cada vez mejores condiciones en la calidad de su ambiente físico y social, promoviendo la salud y calidad de vida de sus ciudadanxs. En este sentido, la psicología ambiental nos aporta valiosas herramientas y resulta imperante **comprender que priorizar la existencia de espacios verdes también es pensar en salud.**