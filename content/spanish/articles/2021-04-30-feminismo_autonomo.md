---
title: Feminismo Autónomo
subtitle: Encontrarnos en la diversidad desde las prácticas solidarias.
cover: /images/uploads/7675856a-229c-46d4-9b10-c3f0b01ad08f.jpg
date: 2021-04-30T21:49:25.546Z
authors:
  - Las Comadres Púrpuras
tags:
  - Genero
  - Feminismo
  - Venezuela
  - Mujeres
  - Derechos
  - Salarios
categories:
  - Genero
  - Cuerpo
  - territorio
comments: true
sticky: true
---
La organización feminista autónoma **Las Comadres Púrpuras** inicia en el año 2016, nos desarrollamos políticamente en **Venezuela** en un contexto profundamente violento y sobre-explotador marcado por la **desigualdad social, precarización de los servicios públicos, la pauperización de los salarios, migración forzada, despidos injustificados, una economía de mafias y lealtades política**s… Se estaba estructurando un Estado mucho más controlador, totalitario y violento.

**La crisis económica** que empezó a finales del año 2014, a partir del agotamiento de un ciclo rentista con fuertes índices de corrupción, endeudamiento, dependencia de las importaciones, la agudización del control cambiario, la inflación estructural y luego la caída de los precios del petróleo, generó la intensificación de las desigualdades sociales y el incremento del descontento social, que a su vez, recrudeció el ejercicio gubernamental de la violencia y el control para amedrentar a la población.

Es en este proceso de crisis que los mecanismos clientelares y la lealtad política constituida bajo el liderazgo carismático en el período de bonanza, pasa a ser la base sobre la cual se estructura la **violencia de Estado** en Venezuela. La lealtad construida a partir de la distribución de recursos, pasa a convertirse en un mecanismo de **sobrevivencia y chantaje**, mientras que la violencia se ejerce como recurso **disciplinario**.

**La violencia de Estado es estructural y sistemática**, y emana dentro de un contexto de vulneración de los derechos humanos, ausencia de información y comunicación por parte de los entes responsables, una economía inestable que prioriza la acumulación privada de riquezas por encima de la atención a las necesidades básicas de la población, la vida y el bien común.

**Los cuerpos de las mujeres** suele ser el centro donde se manifiesta la violencia de Estado. El incremento de los femicidios, feminicidios, la desatención a la salud integral de las mujeres, el embarazo en adolescentes y niñas, el incremento de la mortalidad materna, la violencia sexual, la precarización de la vida, la prostitución, la desterritorialización de las comunidades, la migración forzada que expone a niñas y a mujeres a condiciones de vulnerabilidad frente a las redes de tráfico y explotación sexual, son síntomas del sistema **patriarcal y capitalista**. Es sobre el cuerpo de las mujeres donde se manifiesta la impunidad y la corrupción de los gobiernos, quedando en evidencia durante la pandemia que el mantenimiento de la educación a distancia, la deficiencia de los servicios básicos aunada a las labores de cuidado, recaen y sobreexplotan cotidianamente a las **mujeres**.

El sistema de **lealtades políticas** ha desarrollado una estructura **para-institucional**, paulatinamente el Estado se ha venido fragmentando en cadenas de intereses mafiosos, en donde intervienen la corrupción estatal y el saqueo de los recursos públicos, cuerpos policiales y militares o grupos armados ilegales que ejercen control territorial y mecanismos de extorsión sobre la población, una economía de importación privilegiada por su alianza con el gobierno. **La lealtad** ha sido la **relación política** fomentada tras años de caudillismo y polarización, al vincular la violencia como una práctica política a medida que se profundiza la crisis, dio paso a una gobernanza marcada por la aparición de mafias que concentran el ejercicio del poder.

Un ejemplo de la legitimidad de esta para-institucionalidad por parte del Estado es El Arco Minero del Orinoco.

**El Arco Minero del Orinoco** se estableció en el año 2014 por el gobierno de Nicolás Maduro, pero también fue planteado por el gobierno de Chávez en el año 2011 (1-2). Este proyecto busca regularizar la **minería ilegal**, donde ya existía una minería artesanal en muchos lugares de los estados **Bolívar y Amazonas**. Este proyecto, en principio, era una entrada para las transnacionales en Venezuela. Así fue el caso de transnacionales como Barrick Gold (3), Gold Reserve, Barbados Inc (4), entre otras, privatizando estas zonas y desplazando a mineros ilegales. Pero no ocurrió de esta manera, sin embargo, la minería tendió a organizarse a través de múltiples grupos armados irregulares que se identifican como “sindicatos”, que conviven con el Estado y las transnacionales que comercializan los minerales extraídos. Una legitimación y promoción de las mafias como gobernanza estatal y paraestatal.

La violencia de Estado es la representación más concreta de cómo opera la lógica patriarcal y capitalista sobre los **cuerpos y territorios**. Entendemos los espacios de poder según como estén jerarquizados, y dentro de esa jerarquía, analizamos las desigualdades. La lógica del poder pasa por la legitimación del **principio de autoridad**, en la medida que reproducen mecanismos de dependencia que atan a la sociedad a una rutina disciplinaria, al mismo tiempo que margina formas de autonomía que puedan formar una experiencia política alternativa**. Es una autoridad que no consulta, no dialoga sobre su legitimidad o que no se pregunta por su necesidad, sino que se propone organizar la obediencia, anular la posibilidad de crítica –o mejor dicho hacerla funcional- y fomentar la dependencia de la sociedad**. Sucede así en las relaciones sociales, relaciones familiares, relaciones políticas, estructuras inconscientes y en las subjetividades que jerarquizan, polarizan y controlan: incidir en estas estructuras es tocar-resistiendo la Violencia de Estado.

*Frente al Silencio | Gritamos*

*Frente a la imposición | Resistencia*

*Frente a la política de la muerte | Vivimos*

*Frente al cansancio | Cuidado, relevo y descanso*

*La organización del goce*

En todo ese contexto de precariedad existía la urgencia de contribuir a la continuidad histórica de una política de lxs de abajo; encontrarnos desde las **disidencias, la diversidad** y en los desacuerdos, coincidir no nos era suficiente para la política en donde nos queremos encontrar.

**Las Comadres Púrpuras** provenimos de diversos espacios de la militancia social en donde la construcción colectiva siempre ha sido más importante que las verdades ideológicas. Nos fuimos encontrando en todos nuestros desaciertos, porque nos dimos cuenta que cada vez era más fuerte la convicción de poder vivir nuestras utopías. En medio de tanta precariedad, el amor y el **goce por la vid**a se fue trazando como una política que no es hija de una, sino de muchas experiencias que se reconocían entre sí. **Las Comadres** nos vemos como células de articulación que van labrando en múltiples espacios de encuentro social, son híbridas que pueden dialogar desde un nosotrxs que se construye en causas comunes, un ejercicio cotidiano de **autonomía**. Y así entendemos el potencial que tienen los feminismos como una fuerza que reencuentra a la sociedad, desde los problemas que nacen en los espacios íntimos hasta los que enfrentamos colectivamente en las luchas por la vida.

![](/images/uploads/msyd7.jpeg)

La abrumadora realidad que nos envuelve, ha generado una profunda reflexión y reconstrucción de las luchas sociales, en general y **feminista** en particular, a partir de una crítica a las estructuras del Estado y los mecanismos partidistas que han supeditado las condiciones de vida y de dignidad de la población venezolana y sobretodo de **las mujeres y grupos vulnerables**. Al mismo tiempo la sobrevivencia ha generado redes de solidaridad y apoyo entre colectivos feministas, organizaciones sociales e individualidades que han permitido la articulación, donde el sentido de comunidad y de apoyo mutuo resiste frente a las dinámicas de sobrevivencias que han debilitado en gran medida el tejido social. Creemos que los grupos feministas pueden reconstruir ese tejido, con una perspectiva diversa y autónoma, que pone a las mujeres como autoras, protagonistas activas, para la defensa de la vida, para la construcción de otra narrativa, una narrativa **antiautoritaria y colectiva**, en comunicación y articulación con las diversas luchas **sociales autónomas** que se dan en el país, como alternativa emergente, para la construcción del **nosotrxs**.

**Salud, goce y libertad**

### **Referencias**

1. <http://www.pdvsa.com/index.php?option=com_content&view=article&id=4427:9476&catid=10&Itemid=589&lang=es>
2. <https://www.youtube.com/watch?v=XITu4pe2sZQ>
3. <https://financialtribune.com/articles/world-economy/48559/venezuela-signs-55b-mining-deals>
4. **<http://www.desarrollominero.gob.ve/inversiones-en-el-amo-2/>**

   ![](/images/uploads/las_comadres_purpuras_logo_2_.png)

**Las Comadres Púrpuras** 

Las Comadres Púrpuras son una organización popular autónoma, contracultural, despatriarcal de feministas insurgentes y urbanas, que se activan política y artísticamente en Venezuela articulándonos con distintas realidades y luchas del mundo. Desde el artivismo como forma de denuncia social buscan posicionar la problematización del machismo en la sociedad venezolana y sus consecuencias en la vida de las mujeres, así como el fortalecimiento de una sociedad crítica y autónoma frente al poder estatal, político y económico