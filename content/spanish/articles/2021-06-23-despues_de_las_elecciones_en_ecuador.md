---
title: "Después de las elecciones en Ecuador "
subtitle: "Panorama futuro "
cover: /images/uploads/shutterstock_1752775373-scaled.jpg
caption: Imagen dialogopolítico.org
date: 2021-06-23T16:04:18.025Z
authors:
  - Ivonne Yánez
  - Acción Ecológica
tags:
  - Ecuador
  - Elecciones
  - Lasso
  - Feminismo
categories:
  - Politica y Economia
comments: true
sticky: true
---
En febrero de 2021, en Ecuador se realizó la primera vuelta electoral. Por primera vez, desde el regreso a la democracia en 1978, hubo una tercera fuerza política opcionada, que incorporaba temas como la ecología y género en el debate electoral. Esta candidatura, apoyada por el movimiento indígena, ecologista y feminista, puso en el tapete temas como la defensa del agua o la despenalización del aborto. El candidato Yaku Pérez no pasó a la segunda vuelta, pero sus propuestas quedaron a tal punto que los dos candidatos finalistas para la segunda vuelta tuvieron que asumir posiciones relativas a estos temas para ganar el apoyo de los movimientos y organizaciones sociales.

Finalmente, ganó Guillermo Lasso, conocido banquero, representante de la oligarquía costeña, quien, como estrategia de campaña, se vistió con pantalón rosado y moldeó su discurso hacia posiciones menos conservadoras. Inclusive, antes de asumir el cargo de presidente, ante la decisión de la Corte Constitucional de que es inconstitucional criminalizar y sancionar con privación de libertad a las mujeres que han interrumpido un embarazo producto de una violación, señaló que respetaba esta disposición, aunque estaba en contra de sus principios cristianos.

Pero esto no es más que eso, un discurso. Sabemos que el nuevo presidente y sus políticas neoliberales agudizarán la situación de vulnerabilidad de las niñas y mujeres en el Ecuador. No solamente porque es conocido su apego al Opus Dei (sabemos lo que es eso, cuando el misógino gobierno de Rafael Correa puso al frente del Plan Familia a una persona que apelaba a la moral sexual y educación católica en el Ecuador citando siempre a conocidas figuras de esa poderosa secta católica), sino porque el modelo económico de Lasso profundizará las presentes políticas de privatización de la salud, educación, la expansión de la agroindustria de exportación, o la ampliación del extractivismo petrolero y minero, con, además, mucha dosis de capitalismo verde.

Como todos los gobiernos, Lasso cree que el desarrollo es el camino, pero debo decir que, para muchas organizaciones de América Latina, el desarrollo ha dejado de ser un objetivo y, por el contrario, desde hace varias décadas que se lo concibe como un justificativo para la imposición de políticas de despojo y vulneración de sus derechos y afectaciones a la naturaleza. Para esto, en las últimas décadas apelan a la “sustentabilidad”, a la “resiliencia”, o a la “inclusión” de mujeres al desarrollo productivo.

Lo que no logran concebir es que las luchas antiextractivistas, por los derechos de las mujeres, por la defensa de los cuerpos-territorios, las reivindicaciones de los feminismos comunitarios y territoriales son, en la práctica, batallas anti-desarrollistas, en el mejor de los sentidos. De hecho, los feminismos del Sur -en plural-, los ecologismos populares, territoriales y diversos, aportan para detener el desarrollo, que solo se sostiene con más extractivismo, con la profundización del heteropatriarcado, y con el colonialismo.

Durante los próximos cuatro años de gobierno, las mujeres, sus organizaciones, sus prácticas, sus reflexiones colectivas, seguirán con lo que siempre han hecho, contribuir a los lineamientos, a las narrativas de los movimientos sociales, y a marcar pautas de hacia dónde se deben orientar las propuestas de construcción de sociedades más justas, más ecológicas, en las cuales en el centro se encuentra el cuidado de la vida, y no el lucro ni el individualismo.

Es más, muchas mujeres organizadas -aún sin autoreconocerse como feministas- desde siempre, pero sobre todo durante la pandemia han sido quienes han sostenido las labores de protección y cuidado de la salud colectiva, de la alimentación, de la reproducción social en las comunidades y pueblos.

Otro punto importante es que, cada vez más, se dan alianzas entre ecologismos y feminismos. Por ejemplo a través del uso de herramientas como la interseccionalidad, como forma de análisis de múltiples opresiones sobre unos mismos sujetos, o en el encuentro por la defensa de los cuerpos y los territorios ante el avance del desarrollo.

Porque, así como no puede haber justicia social sin justicia económica, tampoco puede haber justicia ambiental sin justicia para las mujeres.

Otro espacio de convergencia es la idea de la reproducción. Cuando nos juntamos feministas y ecologistas, por ejemplo, podemos pensar si estamos hablando de lo mismo cuando nos referimos a la reproducción y sostenimiento de la vida. Esto hace de los encuentros y diálogos muy interesantes, pero sobre todo importantes pues de lo que se trata es de construir alianzas y caminares conjuntas.

La pandemia ha sido un momento durísimo para las mujeres en el mundo pero, al mismo tiempo, una oportunidad ideal para encontrar puntos de unión alrededor del sostenimiento, cuidado y reproducción de la vida. De la vida misma. Si bien esta ha recaído nuevamente, y sin parangón, en los cuerpos de las mujeres, al mismo tiempo, hay una conexión inmediata con el trabajo de las ecologistas populares y territoriales en la defensa, protección y cuidado de los espacios en los que precisamente se reproduce la vida, y los que para las mujeres son esenciales para la provisión de alimentos, de medicinas, de espacios de distensión… Por eso es frecuente mirar que las mujeres son las primeras en pararse duro para defender el agua, la tierra, ante la arremetida del capitalismo desarrollista.

Son estas mismas mujeres las que, en Ecuador, seguirán dando la batalla frente a lo que se nos viene encima. En primera línea en los territorios, luchando frente a las políticas del Estado, peleando dentro de las mismas organizaciones comunitarias, y como siempre también en sus espacios familiares.

Entonces, el nuevo gobierno, no es más que un continuo de las arremetidas en contra de las mujeres, de los territorios y por lo tanto, la lucha de las mujeres continuará.