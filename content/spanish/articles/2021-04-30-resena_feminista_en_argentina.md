---
title: Reseña feminista en Argentina
subtitle: "Los movimientos feministas desde el siglo XIX hasta la actualidad "
cover: /images/uploads/fotopertenecienteacampanaabortolegal.png
date: 2021-04-30T13:25:36.048Z
authors:
  - Paula Fausti y Florencia Cencig
tags:
  - Genero
  - feminismo
  - mujeres
  - cuerpo
  - territorio
categories:
  - Genero
  - Cuerpo
  - territorio
comments: true
sticky: true
---
La historia del feminismo mundial puede trasladarnos a diferentes partes del planeta y diversos inicios, como por ejemplo, el conocimiento de las “brujas” y los procesos de persecución y aniquilamiento de estas mujeres en Europa, entre los siglos XVI y XVII. Pero en esta oportunidad, nos toca hacer un alto en Argentina, para unir los hilos de la historia e interpretar los diferentes tejidos feministas que se desarrollaron a través del tiempo.

Si bien consideramos como punto de origen las prácticas y luchas de las mujeres originarias latinoamericanas, que pelearon contra la colonización y el propio machismo dentro de sus tribus, por medio de feminismos ancestrales y colectivos; **nos centraremos en los movimientos desencadenados a partir de fines del siglo XIX y principios del XX hasta la actualidad de nuestro país.**

**Al finalizar el siglo XIX ya nos encontrábamos con un código civil que colocaba a la mujer por debajo de la figura masculina**, relanzándonos al plano de la injusticia e inferioridad jurídica, social, económica, cultural y laboral. En este sentido, la historia del feminismo es una historia de lucha inmersa en dichas dimensiones. Pelear por decidir sobre nuestros cuerpos, por condiciones laborales dignas, y por tener voz y voto e igualdad de oportunidades como ciudadanxs y sujetxs de derechos.

Aunque aún queda un largo camino por recorrer, a lo largo del tiempo, los movimientos feministas lograron colocar en agenda pública el tratamiento de diversas problemáticas que influyen y hasta determinan la vida de las mujeres y las disidencias.

Desde el sufragio femenino que dio lugar a votar a las mujeres, por primera vez en 1951, hasta la sanción de la ley de interrupción voluntaria del embarazo en diciembre del 2020; las m**ujeres y personas de diversos géneros que se autoperciben por fuera de la heteronorma impuesta, fueron y son pionerxs en la conquista de derechos** que permiten una sociedad argentina más justa e igualitaria.

**De esta forma, es inevitable no vincular al feminismo argentino con la figura de la mujer en la lucha obrera**. Entre los años 1970 y 1975 aparecieron mujeres provenientes de diversos sectores sociales y grupos militantes, que visibilizaron y retomaron luchas de décadas pasadas, dentro de un escenario socio-político particular y trágico.

La Unión Feminista Argentina (UFA), fundada en 1970 por Nelly Bugallo, Leonor Calvera y María Luisa Bemberg, entre otras; tenía como objetivo, además de dejar a la luz la opresión de clase, exponer la subordinación del género femenino a lo largo de la historia, especificando la desigualdad de poder a favor de los hombres como estrato que subyace a la opresión de las mujeres. Buscaban ir más allá de la política partidaria tradicional al autodefinirse como un movimiento policlasista que criticaba tanto al capitalismo como al socialismo, problematizando al poder machista establecido por la izquierda y el peronismo (Vassallo, 2005)

Además, podemos mencionar otras organizaciones como el Movimiento de Liberación Femenina (MLF), Movimiento Feminista Popular (MOFEP) dentro del Frente de Izquierda popular y la Asociación para la Liberación de la Mujer Argentina (ALMA), entre otras tantas, en aquella época. Pero lo importante es remarcar que, más allá de las creencias e ideologías políticas, las diferentes agrupaciones feministas que fueron surgiendo, luchaban por un mismo objetivo; **ganar terreno en materia de derechos humanos y justicia social, basándose en la igualdad de géneros.**

![](/images/uploads/fotodeautoriacampanaabortolegal.png)

Dicho esto, si de fuerza y perseverancia se trata, cómo no mencionar a la Asociación **Madres de Plaza de Mayo**, la cual fue conformada durante la dictadura militar de Jorge Rafael Videla. Junto con la Asociación Civil Abuelas de Plaza de Mayo son ejemplos de agrupaciones integradas por mujeres repletas de conciencia social y comprometidas en la búsqueda de la verdad y justicia; que aunque en su inicio no se proclamaban explícitamente como feministas, claramente representan la esencia y el alma de su lucha y fortaleza.

En cuanto a los grupos feministas más contemporáneos, han avanzado y ganando terreno en diferentes ámbitos de la vida misma. Empezaron a gestarse agrupaciones de mujeres conformadas en diversos entornos de trabajo; como por ejemplo, las colectivas feministas creadas en los ámbitos de profesiones artísticas, o nuevas organizaciones surgidas dentro de la sociedad civil y al interior de los propios partidos políticos. Respecto a esto, **es indiscutible que el colectivo de protesta nacido en el año 2015, “Ni una menos”, fue pionero en exigir la erradicación de la violencia hacia la mujer** y su consecuencia más grave, el femicidio.

![](/images/uploads/niunamenos.png)

**En esta línea no podemos pasar por alto los históricos Encuentros Nacionales de Mujeres, que se realizaron anualmente en Argentina desde 1986**. Los cuales son de carácter autónomo, autogestionado, pluralista, federal y horizontal. Sus objetivos siempre apuntaron a generar un espacio de intercambio dentro del colectivo, donde confluyen diferentes estrategias e ideas que, sin ponerse bajo banderas partidistas, permiten discutir una agenda específica que trate las problemáticas coyunturales.

Cabe aclarar que, en el año 2019, luego de varios debates, se cambió el nombre por Encuentro Plurinacional de Mujeres Lesbianas, Trans, Travestis, Bisexuales y No Binaries, dando así lugar a la visibilización y el reconocimiento de, por un lado, las identidades disidentes y, por el otro, a las luchas antirracistas y descoloniales.

En gran medida es gracias a dichos encuentros que en el país surgieron diferentes activismos, militancias y movimientos que unieron sus luchas en pos de una sociedad más democrática, diversa, horizontal, justa y plural. Algunos de ellos son: **el Movimiento de Mujeres Indígenas por el Buen Vivir, el transfeminismo, los ecofeminismos, el feminismo antiespecista, el activismo gordx** y más.

Todas estas agrupaciones que se fueron desarrollando a lo largo del tiempo y que integran el movimiento feminista en Argentina son las responsables de desencadenar diferentes puntos de inflexión, que dieron lugar al debate y a exigir la elaboración de políticas públicas, que respondan a las problemáticas que conciernen a las mujeres y a las disidencias.

Si bien la brecha de la desigualdad de géneros continúa latente e inmensa, cuando la frustración y el abatimiento nos golpee, y sintamos que a pesar de nuestras luchas seguimos paradas en el mismo lugar, no debemos olvidar todo lo conquistado:

En el año 2006 se sancionó la Ley de Educación Sexual Integral, que exige en los establecimientos educativos la enseñanza sobre la igualdad entre los géneros y el respeto por la diversidad, porque hubo lucha. En el año 2010 se sancionó la Ley de Matrimonio Igualitario, porque hubo lucha. En el año 2012 se sancionó la Ley de Identidad de Género, porque hubo lucha. En el año 2018 se sancionó la Ley Micaela, la cual establece la capacitación obligatoria en materia de género para todos los que integran los tres poderes del Estado, porque hubo lucha. En el año 2019, se creó el Ministerio de las Mujeres, Géneros y Diversidad, porque hubo lucha. Y, en el año 2020, se sancionó la Ley de Interrupción Voluntaria del Embarazo, porque hubo lucha.

Como bien establece Rita Segato: “Las mujeres no somos ciudadanas plenas, no somos personas siquiera, así nos ve el juez cuando juzga los casos de feminicidios o los otros crímenes contra nosotras. Incluso grandes juristas, que he criticado con bastante saña en mi país, ven los crímenes contra las mujeres como crímenes de la libido, del deseo. **Los crímenes contra nosotras no son crímenes de la libido, son crímenes políticos de ese orden patriarcal**”. Y si fuera un “crimen del deseo, en todo caso se trata de otro deseo: el del poder” (2019)

![](/images/uploads/encuentrodemujeres.png)

Según datos oficiales del Ministerio de Mujeres, Géneros y Diversidad de la Nación; durante el año 2020, se recibieron 108.403 comunicaciones por violencia de género a la línea de atención 144. Donde el 95% de las personas refirieron atravesar situaciones de violencia por razones de género de forma Psicológica, el 67% de forma física, el 37% violencia económica y patrimonial, 34% violencia simbólica, y el 13% manifestó hechos de violencia sexual.

A su vez, según la información oficial del Observatorio de las violencias de género “Ahora que sí nos ven”, hubo 70 femicidios en el periodo de enero a marzo de 2021. Por lo que, cada 31 horas, muere una mujer por violencia de género en nuestro país.

¡La lucha debe continuar!. Próxima parada: Reforma Judicial Feminista Argentina.

### Referencias: 

Stephanie Demirdjian, S. (2019) “Rita Segato: La violencia de género es la primera escuela de todas las otras formas de violencia”. Movimientos. Uruguay.

Vassallo, Alejandra. (2005). “‘Las mujeres dicen basta’: movilización, política y orígenes del feminismo argentino en los '70”. En Andújar, Andrea, D'Antonio, Débora, Domínguez, Nora, Grammático, Karin, Gil Lozano, Fernanda, Pita, Valeria, Rodríguez, María Inés y Vassallo, Alejandra (compiladoras) Historia, género y política en los '70. Buenos Aires: Feminaria Editora.