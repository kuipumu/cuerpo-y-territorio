---
title: " Cancha, balón y amigues"
subtitle: Por amor al mixto.
cover: /images/uploads/2_2_.jpg
caption: "Ilustración: Heizel Zamora"
date: 2020-09-06T22:38:39.841Z
authors:
  - Heizel Daniella
tags:
  - Deporte
  - Futbol
  - Pasión
  - Equipo
  - FutbolMixto
  - FutbolFemenino
  - Argentina
categories:
  - Género
comments: true
sticky: false
---
<!--StartFragment-->

Hubo una vez un mundo, en donde jugadores y jugadoras de fútbol amateur se reunían a compartir la cancha y hacer rodar el balón. Pero el mundo en el que vivimos hoy, no es algo que sea posible. En Argentina, así como en muchos países, el asunto adquirió otra dimensión al implementarse las medidas de aislamiento tras el suceso pandémico, afectando a todos y a todas, incluyendo a los amateurs del fútbol mixto. Se frenaron los encuentros.

El desarrollo físico y técnico también se frenó. Hay quienes cuentan con más espacio y balón en sus casas, pero pocas y pocos logran encontrar motivación para ejercitarse. No es lo mismo trabajar técnicas futbolísticas en soledad y espacio reducido, que practicar en equipo. Y estas limitantes de recursos (espacio, balón y compañerismo), no ayudan a la formación estratégica, táctica ni técnica.

Mientras tanto, dentro de la cancha quedan solo recuerdos a disposición de estos y estas deportistas: aquel tiro libre en donde el balón recibía murmullos del viento que lo impulsó, hipnotizado, hacia el travesaño; abrazos de victoria o de derrota, abrazos por solo compartir el momento... Se anhela el regreso de los buenos tiempos, con los zapatos de fútbol puestos. Es y será duro para ellos acoplarse a la nueva forma de vida.

Se espera con desorbita revivir los partidos entre amigos y amigas, pero también revivir ese sentimiento solaz que genera estar dentro de una cancha mixta, símbolo de paridad, igualdad e integración, en donde jugadoras y jugadores, al tener el balón entre sus pies, no solo juegan y comparten, sino que también reposan de lo habitual. Se abstraen de los problemas de la vida cotidiana mediante un “escape” saludable y, además, intervienen su semana viendo la verdadera cara de la pasión futbolera.

El fútbol mixto se trata de un juego estratégico que reúne las búsquedas de crecer y mejorar como equipo, como jugador/a y como persona, en un ambiente de respeto y tolerancia. Esto lo ha vuelto un atractivo moderno donde muchas personas se iniciaron a experimentar el entorno mixto.

Experiencias reales:

* > *“Me gustó la adrenalina que sentí al estar ahí parada defendiendo a mi equipo, pertenecer a uno y lo significativo de sentir el apoyo de mis compañeros y espectadores, cada uno dando lo mejor de sí para ganar o simplemente pasarla bien”, contaba Andrea Zamora, una joven que se adentró en el mixto como arquera principiante, ganando la copa estudiantil junto a su equipo.*
* > *“Al principio fue difícil jugar y fallar en cada patada; tropezar, viendo que tu compa hombre de al lado la pisaba tan bien. ¿Qué pensábamos con las pibas que recién iniciamos? ‘ellos juegan desde que saben caminar’ ‘tranquilas’, ‘paciencia’.*

Pero tomamos lo hermoso del mixto para que nuestros compas hombres nos enseñaran, guiaran, y motivaran para avanzar y aprender. Y, además, solo viéndolos, con algo de observación minuciosa, se fue aprendiendo.

De hecho, a partir de los mixtos que jugábamos cada semana, se organizó desde la comisión de género de la Facultad de Agronomía de la Universidad de Buenos Aires (FAUBA) el primer torneo de fútbol mixto, en el marco de la presentación del Proyecto de Ley de Interrupción Voluntaria del Embarazo de Argentina (IVE), con el objetivo de recaudar fondos para una biblioteca feminista en el centro de estudiantes”, comentó, la futbolista amateur Rocío Valdettaro, concluyendo en que la comunidad mixta permite expandirse e interpelar otras temáticas, como la diversidad de género.

Un equipo de fútbol mixto, el Drink Team Mix, nos planteó que, mediante una encuesta por Instagram a sus seguidores y seguidoras resolvieron que aquello que más motivaba a jugar al fútbol mixto con los equipos era, en su mayoría, el hecho de sumergirse en un mundo abierto lleno de gente amistosa, amable, receptiva y diversa, donde el interés no estaba puesto en qué género jugaba ni qué habilidad individual había, sino en la idea de grupo, de estrategias y de saber balancear el equipo para que sea divertido. Y, lo más importante, el “Tercer tiempo”, al que se llegaba de forma ‘obligada’ habiendo ganado o perdido.

“El famoso tercer tiempo es uno de los momentos más esperados por los y las deportistas para compartir fuera de la cancha. Podía ser lunes, miércoles o viernes, pero el tercer tiempo siempre existiría” comentaba el Drink Team Mix a partir de la encuesta realizada. Por último, coincidieron en que motiva jugar al mixto el hecho de romper con la imposición sociocultural de que el fútbol es solo masculino.

* En Twitter, una reconocida futbolista profesional, actual jugadora de San Lorenzo y promotora de la profesionalización del fútbol femenino, disparó una discusión interesante mediante una pregunta que realizó de forma abierta y que movilizó a todos y a todas: *“¿El fútbol profesional debería ser mixto en todas sus divisiones?”*. Hubo quienes comentaron que, aparte del masculino y femenino, podría desarrollarse el fútbol mixto, empezando por los clubes de barrio, con entrenamientos y tácticas para el proceso de formación.

Pero también otras personas opinaron que no debería ser mixto el fútbol para, así, preservar la idea de que se juega solo por diversión y de corazón, dejando en un plano inferior la competitividad y todo el círculo de intereses que genera el profesionalismo.

* “En ciertos torneos en los que he participado, en el reglamento se tiende a favorecer más a un género que al otro, y me parece que eso está mal. La idea fundamental del fútbol mixto se basa justamente en la igualdad.

Obviamente que hay quienes se dejan llevar por la competitividad, pero eso pasa en todos los deportes y, creo que en este caso, por uno no deben pagar todos, ya que la mayoría vamos a divertirnos y a competir sanamente”, aportó Leicen Zamora, jugador de fútbol en relación a algunos reglamentos de torneos de fútbol mixto, que muchas veces generan polémicas por alteraciones de aquel oficial en el que se basan de fútbol sala, establecido por la Asociación de Fútbol Argentino (AFA), como por ejemplo que, en caso de ser un jugador hombre, solo se le permite meter goles de cabeza, o que las mujeres no tengan permitido atajar.

Como reflexión final, la pandemia que nos llevó a gran parte del mundo a aislarnos nos hizo repensar, bajo mucho criterio, el modo en que vivíamos, así como también valorar aún más aquellas cosas que amábamos hacer. Pronto llegará el momento en que la frase de inicio: “Hubo una vez un mundo, en donde jugadores y jugadoras de fútbol amateur…”, se transformará en: “regresó el fútbol a este mundo, regresó el mixto”.



<!--EndFragment-->