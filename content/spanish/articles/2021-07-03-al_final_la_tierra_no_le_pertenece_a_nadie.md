---
title: “Al final la tierra no le pertenece a nadie”
subtitle: Las circunstancias me hicieron apreciar más la tierra
cover: /images/uploads/epatukunuko.jpg
caption: "Epatu Konuko "
date: 2021-07-03T16:37:11.018Z
authors:
  - Laura Gonzalez Morales
tags:
  - Ambiente
  - Tierra
  - Siembra
  - Ciudad
categories:
  - Territorio
comments: true
sticky: false
---
“Al final la tierra no le pertenece a nadie” es lo que siempre digo cuando me preguntan o me
felicitan por el trabajo de siembra que hago. Lo hago sin pretensiones de recibir algo a
cambio, lo único que me gusta recibir es el agradecimiento. Además, sembramos para
sentirnos plenos y felices, por meter nuestras manos en la tierra, por echarle agua a las
plantas, por cultivar un árbol, por cosechar las semillas y el fruto, por entender el proceso que
tienen las maticas en el entorno que comparten con nosotros. Fácilmente puedo decir que
tengo un apego desde mi niñez con las plantas; mi mamá y mi papá sembraban en la casa y,
creo que indirectamente, me transmitieron el gusto. Siempre que cocinaban caraotas o
cualquier tipo de granos en casa, iba al jardín a buscar los aliños y mis padres me explican
para qué servía cada uno.

Mi mamá me decía: ¡Ven para acá a ver cuál es cada mata, y sepas para qué sirve cuando te
sientas mal del estómago o para cuando vayas aliñar!

Yo de niña/adolescente no les escuchaba tanto, pero igual esa constante trasmisión oral que
tenía por parte de mis padres existía. Pasaba más cuando alguien se sentía mal de
estómago, y yo le decía: “Mi mamá tiene una mata sembrada que sirve para el estómago”.
Hoy en día, todo ese conocimiento que me transmitieron mis padres, y que sigo utilizando,
me ha servido para resolver situaciones circunstanciales que me ha tocado vivir. Digo esto
para no caer en una visión moral sobre mi condición como persona, ciudadana, mujer e hija.
Actualmente, vivo en Venezuela, el lugar donde crecí es un pueblito semindustrial y rural; la
práctica de siembra que se realiza allí es el conuco, que consiste en un trozo de tierra
pequeño para cultivar alimentos de autoconsumo. En mi casa siempre había plantas
ornamentales y varias especies de plantas medicinales. Con el tiempo nuestro conuco se fue
transformando a raíz de la coyuntura del país, y pasó a tener un peso responsable e
importante dentro de las responsabilidades del hogar.

Con honestidad yo nunca pensé que tendría un respaldo de ese cultivo, y que me ayudaría a
resolver los alimentos. Al principio me costó entender esta dinámica de tener que sembrar lo
que me tenía que comer; realmente, yo no me sentía campesina, ni conuquera, ni nada que
me asociara con el trabajo de la tierra. Además, era más una “opción circunstancial” para
solucionar el tema alimenticio sin tener que gastar dinero, ni tener que convivir con el
sobreprecio que había en los mercados del país en los momentos más fuertes de la crisis.
Todo este panorama me hizo repensar el tema de la siembra, a verlo como algo que está
presente diariamente en la vida, y donde no tenemos ningún tipo de apego a pesar de que
nos proporciona alimentos todos los días. Creo que a raíz de vivir una crisis como la que
tenemos en Venezuela que nos afectó los hábitos alimenticios, y donde se tiene un serio
problema con la subsistencia alimenticia, donde mucha gente se vio afectada y comenzó a
resolver con lo que podía incluyendo el conuco.

Mis vecinos me comentan que tienen una receta de arepa con yuca y ahuyama donde la
mezcla de ambas queda muy parecida a la contextura “Harina Pan”. También me han dicho
de una receta de arepa de plátano verde. Todo esto fue una adaptación muy violenta.
Recuerdo que mi hermano y yo salíamos a buscar mangos por toda la urbanización por la
escasez de alimentos.

A partir de ese momento, hubo una configuración en la conciencia, porque mucha gente pasó
hambre y, pues, nadie quería volver a pasar por eso. A pesar de que la gente vive en la
ciudad o en urbanismos cercanos a zonas rurales no considera como alternativa tener un
patio para sembrar. La mayoría de la comida que se come en las ciudades es del
supermercado, la gente no piensa que tiene que sembrar algo para comérselo, porque
simplemente va al supermercado y listo. ¿Qué quiero con todo esto? Explicar que todo este
contexto tan difícil que tuve que vivir con mi familia no nos limitó a resolver nuestra
alimentación.

De esta forma, mi mamá es una persona que no se limita por este tipo de circunstancias.
Esto la llevó a buscar personas que tenían un trabajo con el conuco y la siembra desde la
ciudad -aquí en Venezuela se ha hablado mucho de la autogestión y de la comuna como

alternativa- muchas de estas personas con las que mi mamá se vinculó fueron una nueva
alternativa para resolver algo tan elemental como la alimentación.

Y llegamos a un espacio llamado Epatu Konuko (https://www.facebook.com/EpatuKonuko/) ¿Qué es Epatu Konuko? Es un colectivo de siembra, enfocado en el tema de agricultura urbana y la permacultura desde la ciudad.

### Como colectivo, ¿qué hace Epatu?

En Epatu se practican procesos tales como el trueque, la cayapa gastronómica, talleres de
formación y prácticas sustentables, aparte de tener un desempeño artesanal importante con
diseños propios que son producidos con algunos elementos del conuco. Es preciso aclarar
que muchos de los compañeros y compañeras que nos desempeñamos dentro del espacio
de Epatu Konuko, tenemos conucos en nuestros hogares donde está el mayor aporte y es
donde se desempeña el mayor tiempo con respecto a la siembra.

Para mi el espacio de Epatu Konuko me hizo repensar mi posición como habitante en la
cuidad, ¿qué hacemos cuando sembramos? y ¿qué hacemos cuando tenemos un espacio
propio para hacerlo? Además, de cómo luchamos y protegemos el valioso aporte que
tenemos por parte de la tierra y la cosecha. La emoción que sentimos cuando comienzan a
germinar las semillas, cuando sale ese primer brote de raíz, de cómo nos preocupamos
cuando comienzan a secarse la matica y además de entender su espacio de vida y cómo
vamos descubriendo ese coexistir.

Toda esta experiencia para mí ha sido amorosa y política. Yo siento que reivindicó a cada
una de las matitas cuando las cuido, aunque suene muy hippie. De hecho, he percibido que
este tipo de procesos han querido vincularlos con los planes del gobierno para invisibilizarlos.
Realmente estos procesos son necesarios y valiosos, aparte los saberes ancestrales no han
desaparecido, siguen vigentes con cada aporte que tiene la gente, así como históricamente
lo han hecho los pueblos indígenas y los campesinos protegiendo y cuidando la tierra.

Por último, rescato mucho que se visibilice este tipo de prácticas y vivencias, ya que sirven
como colchón de desahogo para cada uno de nosotros que transmitimos con las palabras

ese vivir y ese sentir, donde ciertas circunstancias llegaron a transformar nuestra condición
de vida y existencia.

“Las semillas son nuestras hermanas y la tierra nuestra dulce madre”