---
title: "Sylvia Molloy y la autobiografía "
subtitle: " "
cover: /images/uploads/2_sylvia_molloy.jpg
date: 2020-12-19T19:02:37.077Z
authors:
  - Niyireé S Baptista S
tags:
  - Autobiografia
  - Escritora
  - Escribir
  - Latinoamerica
  - GéneroLiterario
  - Literatura
  - Narración
  - CríticaLiteraria
categories:
  - Cultura
comments: true
sticky: true
---
> *`“El lenguaje es la única forma de que dispongo para ‘ver’ mi existencia. En cierta forma, ya he sido ‘relatado’ por la misma historia que estoy narrando”.`*

#### **A modo de introducción**

Cuando comencé este artículo no tenía claridad en la idea de hacía dónde debía dirigir mis esfuerzos de investigación. En principio, pensaba trabajar con la escritura de mujeres y el cuerpo, pero, paradójicamente, se me presentó la oportunidad de trabajar a **Sylvia Molloy**, narradora y crítica literaria desconocida para mí. Decidí embarcarme en la tarea de asumir quién era esta mujer, qué representaba su escritura y sobre qué temas escribía.

Al comenzar a indagar, lo primero que encontré fueron sus trabajos referidos a la crítica literaria, que es muy conocida dentro del medio académico y tiene gran trayectoria en este ámbito; **en su crítica está presente el tema del género y la importancia que le da a esta perspectiva dejada de lado por el canon latinoamericano.** En el transcurrir de esos días leí dos novelas de Molloy: *Desarticulaciones y En breve cárcel;* ambas se consumieron en pocas horas ante mis ojos. 

Algo en mí había surtido efecto al leer aquellas páginas llenas de una configuración de memorias. Justo por esos días me acechaba la revolución de los recuerdos, pues me encontraba en una casa que había dejado de habitar desde hacía cinco años y ahora las imágenes pasaban una y otra vez por mi cabeza en una constante obsesión por evocar el pasado, pero mis pensamientos me tomaban en medio de una extraña calma, sosiego, muy diferentes a como recordaba haberlos vivido. **Los textos de Molloy parecían vitales para comprender el estado de rememoraciones a los que me sometía mi propio existir.** Ambas novelas, y los descubrimientos que para mí representaron, me llevaron a retomar el artículo que intentaba hacer.

#### **Pasajes de memoria: la ficción autobiográfica**

**Mucho se ha escrito sobre la obra narrativa de Sylvia Molloy, considerada una de las escritoras más importantes de la actualidad latinoamericana.** Ella se inició dentro de la crítica literaria y, posteriormente, devino escritura creativa o ficcional, en la cual ha producido cinco novelas. 

Una de las cosas que ha caracterizado su escritura ficcional, y que ha llamado la atención de la crítica, es la referida al uso de la memoria y la recuperación del pasado que hace a través de las historias de sus personajes y que va hilando en un tiempo que parece querer recobrar lo perdido. **En este sentido, Molloy ha afirmado que su escritura recoge pasajes de su propia vida, recuerdos suyos y ajenos que contribuyen a escribir la ficción que narra en sus novelas.** Sus textos son autobiográficos, articulan la memoria e identidad a partir del recurso del recuerdo. La autora construye toda una trama que da cuenta de una relación entre la narración y el recuerdo. De esta manera, he establecido dos puntos pertinentes de análisis respecto a la autobiografía ficcional. El primero, relacionado con los aspectos más teóricos del género autobiográfico, y el segundo, que aborda la autobiografía como herramienta narrativa para construir la memoria e identidad de los personajes en la escritura creativa o ficcional.

#### **Autobiografía y ficción**

**El acto de narrar es una de las formas más complejas de escritura.** Crear historias que se amalgaman en un universo de ficción, y en algunos casos, de realidad, es un ejercicio propio de dioses creadores de mundos. Pero como en toda creación, esa mano que mueve a su antojo montañas, luz y oscuridad, también empapa de sí la obra creada, toma elementos del propio universo subjetivo para dar forma a las complejidades humanas que se desarrollan en la historia. 

Es así como la naturaleza escritural de lo narrado puede decirse que corresponde a un fragmento de la vida de su creador: **la escritora o el escritor que se desdobla para trascender en la palabra, dejar en el relato algo suyo, pero que a su vez es otra cosa, no se pertenece, solo hay huellas de una representación de la realidad.** Al hablar de lo imaginado y lo real, surge una palabra en medio de esta frontera, nos encontramos situados frente a la autobiografía ficcional, en la que cada vez es menos previsible saber qué es lo acaecido verdaderamente y qué corresponde al ámbito de lo no real. Dibujar ese límite se torna una acción de grandes dificultades porque los momentos que marcan la diferencia se hacen cada vez más impenetrados: “son más numerosos los híbridos inclasificables, los objetos narrativos, cuya naturaleza misma radica en la oscilación entre la función estética y la comunicativa y cuyo ‘destino específico es mantenerse en el límite’” `(1)`.

En su texto *Acto de presencia.* La escritura autobiográfica en Hispanoamérica (1996), Molloy define a la autobiografía como *“una manera de leer tanto como una manera de escribir”* `(2)`. Llama la atención el propio carácter autobiográfico que toman los textos ficcionales de la autora. **Ella se da a la tarea de construir un mundo que parte del suyo, de experiencias y recuerdos que relata sobre sí misma y que vienen a ser el detonante de las narraciones a las cuales nos somete.** 

![](/images/uploads/1_desarticulaciones.jpg)

El acto de escribir se convierte en una forma de enunciación de la experiencia propia, en una representación fabulada de las imágenes que estallan en su recuerdo. Así lo describe: *“Yo trabajo con fragmentos, los acumulo, y escenas que voy hilvanando. Trabajo mucho con material autobiográfico o con memoria, personal o de otros, a veces acudo, robo recuerdos, digamos. Me interesa ficcionalizar el pasado que yo recuerdo, fragmentos del pasado que recuerdo, como digo, no sé inventar, no me interesa inventar. Pienso que ya hay suficientes cosas interesantes en lo que pueda yo recordar”*.

La autobiografía es, pues, una construcción narrativa, el relato que contamos o nos cuentan, y el medio de acercarnos a este relato es la rememoración, el recuerdo de tiempos, espacios y personas. Molloy sostiene que el pensar que la autobiografía es el más referencial de los géneros termina siendo una ingenuidad, pues **esta siempre está atravesada por una construcción que se basa en recuerdos, muchas veces imaginados o inventados de las vidas de quien la escribe o de aquel o aquella que se la ha contado.** 

Este juego entre la autobiografía y la ficción es uno de los intereses de la autora, la cual comenzó a indagar en el género autobiográfico, en sus modos escriturales, como los diarios íntimos, los testimonios y la escritura epistolar, todas aquellas narraciones hechas desde la primera persona. En sus palabras: *“quería acudir a esos géneros que juegan con el roce entre escritura y realidad y que, en el caso de los diarios, también juegan con la recreación de lo cotidiano”*. 

**La autora asevera que la autobiografía ha sido uno de los géneros poco estudiados en Latinoamérica y es por ello que no se ha percibido su riqueza dentro del universo del discurso narrativo.** En su trabajo crítico sobre la autobiografía, Molloy deja claro que uno de sus intereses es conocer las características de este género, las formas culturales que usan las y los escritores, qué se puede apreciar en ellos y qué datos componen lo dicho en las autobiografías estudiadas para forjar la imagen que se entrega al lector.

Pensando en esas características, es recurrente notar en sus novelas rasgos o elementos con los cuales suele cargar a sus personajes y que, además, son categorías que también están presentes, y muy marcados, en sus trabajos de crítica literaria. **Dichos elementos son la memoria y el recuerdo, las lenguas habladas o perdidas, la identidad relacionada con el ser migrante, el género y las sexualidades disruptivas.** Todas estas categorías hilvanan y dan forma a sus personajes, además, forman parte de su existencia, como mujer, de orientación sexual lesbiana, nacida en la Argentina, pero migrante desde su juventud, y que se repiten una y otra vez en la adecuación de su ficción y son inquietudes que de alguna u otra forma se evidencian en sus trabajos no ficcionales.

**Referencias:**

`1.`Pacheco, Carlos; “Autobiografía ficcional e historia alternativa, en el diario íntimo de Francisca Malabar”, en La patria y el parricidio, Caracas, Ediciones el otro, el mismo, 2001.

`2.`Sylvia, Molloy; “Introducción”, Acto de presencia. La escritura autobiografíca en Hispanoamerica, México, Fondo de Cultura Económica, 1996.

`3.` Sylvia, Molloy; Desarticulaciones, Buenos Aires, Eterna Cadencia Editorial, 2010.

`4.` Gusdorf, George; “Condiciones y límites de la autobiografía”, en La autobiografía y sus problemas teóricos. Estudio e investigación documental, España, Suplementos Anthropos, 1991.

`5.` [Los recuerdos que sabemos crearnos](<https://www.lanacion.com.ar/cultura/los-recuerdos-que-sabemos-crearnos-nid221652/ [14-03-2020]>) - La Nación 

![](/images/uploads/3_en_breve_carcel.jpg)