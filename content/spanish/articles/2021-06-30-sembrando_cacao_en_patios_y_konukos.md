---
title: Sembrando Cacao en Patios y Konukos
subtitle: "La siembra de Cacao comunitario "
cover: /images/uploads/foto_1.jpg
caption: Planta de Cacao sembrada el 21 de junio de 2020, Día del Solsticio, en
  casa de la Artesana y cantante Yovi Bustamante en Santa Cruz de Aragua
date: 2021-06-30T20:08:43.444Z
authors:
  - Laura Morales
tags:
  - Cacao
  - siembra
  - ambiente
categories:
  - Territorio
comments: true
sticky: true
---
Konuko (Conuco) y konukero (Conuquero)
CACAO... (Theobroma cacao L)
*“mazorquitas danzarinas que se abren a la vida
coquetean al taparo que sombrea a sus madres
y acarician el alma de quien las mira”
Una planta de cacao en nuestro konuko es:
encontrarse con la esperanza,
reconciliarnos con nuestros ancestros,
cultivar en libertad,
amar nuestro Ser,
dar rienda suelta a nuestro poder creador.
Y, tener a la mano*
“El alimento de los Dioses”

No imaginan el placer que me embriaga, ver a través del ventanal de mi casa, a mis plantas de cacao con sus mazorcas y chirelitos danzando junto al taparo que les abriga, y me digo, "Laurita, los sueños si pueden hacerse realidad". Es que no preciso, cuando nació mi amor por el cacao, tanto así que cultivo tres plantas en mi hogar, además, de hacer voluntariado con el Proyecto autofinanciado: [Diseminando Cacao en patios y konukos de Venezuela (EPATU KONUKO/TRUKEKE)](https://www.facebook.com/groups/1218049071634562), idea que iniciamos en el 2017 junto a otros konukeros, y que a la fecha, ya ha permitido aproximadamente a 70 familias cultivarlo en sus casas en varios lugares de nuestro país, teniendo como epicentro, el estado Aragua. Es de resaltar, que además de sembrar dicha especie, acompañamos a los konukeros en la parte agronómica, la cosecha, el manejo postcosecha y en la formación artesanal para la elaboración de derivados de cacao, esto ha incentivado a muchas familias a querer integrarse al Proyecto, por lo que tenemos una lista de espera para dar inicio a la siembra para el mes de agosto del 2021 en distintas localidades del país.

![El Dr. Teófilo, vecino de la Urb. Corocito en santa Cruz de Aragua, recibiendo su planta de Cacao para sembrar en agosto de 2020](/images/uploads/foto_2.jpg)

![La Familia Oliveros mostrando Orgullosa la barra de chocolate, elaborada con el cacao cultivado en su casa en Santa Cruz de Aragua, la cual constituyó ser el primer Konuko en elaborar sus chocolates tree to bar (19 de junio de 2021)](/images/uploads/foto_3.jpg)

¿Y por qué ese empeño en que la gente siembre el cacao en sus patios?, soy konukera, practicante de la siembra en casa, y, siempre soñé tener una planta de cacao, pero nunca podía lograr obtener ni una planta, por lo que recurrentemente me preguntaba, ¿Cómo es posible que sea tan difícil tenerla en casa?, por otra parte, como artesana y Maestra Chocolatera, debo recalcar lo difícil de acceder a la semilla de cacao fermentada para la elaboración de chocolate, por lo que una debe hacer magia para obtenerla, corriendo el riesgo algunas veces de comprar una semilla con problemas de fermentación, almacenamiento, insectos y hasta con hongos.

Sabían ustedes, que el país con el mejor cacao del mundo le niega su sabor y aroma a sus habitantes, somos privilegiados los venezolanxs que hemos probado el real cacao, ya que este se destina en un 80% a la exportación, más un aproximado de 17% sale de contrabando, quedando apenas un 3% para la labor artesanal. Privando al artesano la posibilidad de producir auténtico chocolate, por lo que la mayoría de nuestra población, conoce es el chocolate industrial, al cual generalmente, le sustraen la manteca, sustituyéndola por grasa vegetal, agregándole saborizantes artificiales y mucha azúcar, es decir, el/la venezolanx come es golosina, trayendo como consecuencias adicción, obesidad, entre otros daños, por lo que no es de extrañar, que siempre le achaquen al chocolate la presencia de muchos problemas de salud, sobre todo en niños, niñas y adolescentes.

Algunas personas me han manifestado, ¿cómo creen ustedes que con dos o tres plantas en casa va a ser suficiente que el cacao llegue a todxs?, ya que es tan poquito que no vas a poder sostener ni una mini producción artesanal, siempre les respondo, que el propósito no es tener una gran plantación, ya que el espacio familiar no lo permite, les digo, sembrar cacao en tu patio o konuko, es como una semilla pequeñita de Samán, de la cual nacerá un hermoso árbol, que dará sombra y alimento a todo aquel que se le acerque, ya que luego de que una familia cultive unas plantas de cacao, estará salvaguardando esa semilla en el nicho familiar, dado que se cultiva de manera orgánica, además que, aprenderá un oficio a través de la elaboración artesanal de sus deliciosos chocolates, permitiéndole el privilegio de conocer el real sabor y aroma del cacao.

Por otra parte, se alimentaran sanamente y contribuirán a mejorar el suelo, pues sus hojas son excelente abono para el mismo, también esas mismas hojas tomadas a través de infusiones mejorarán su circulación, y del fruto, se aprovecha su mucílago para comérselo en su estado natural, o a través de bebidas y licores artesanales, con las semillas fermentadas o no, se obtienen grandes beneficios para la salud, por sus altos valores nutricionales que están presente en la grasa, el licor o pasta de cacao y la cascarilla.

También esta idea de sembrar cacao, se amplía a través de la formación artesanal en chocolate y otros saberes, a personas interesadas en aprender el oficio de hacerse chocolatero, así no tengan plantas sembradas en sus casas, ya que sus espacios no les permite, sin embargo, esto les entusiasma, y procuran sembrar en casa de familiares que tengan patio, o en terrenos comunitarios.

Como Maestra Chocolatera, decidí hacer Escuela en la comunidad donde vivo, a través de la formación de discípulos, teniendo como lema, si Venezuela tiene el mejor cacao del mundo, hagamos entonces, el mejor chocolate, ¿y como así?, bueno, “tropicalizando el arte de la chocolatería”, esto ha motivado a varios jóvenes, y a no tan jóvenes, a incursionar en el mundo del chocolate con ideas originales, para hacer de esta experiencia, una sin igual travesía culinaria, entre ellos, destaca, la artesana y artista de Calle [Jennifer Foster](https://www.facebook.com/jennifer.foster.1023611), con sus Chokocucas, una especie de catalinas elaboradas con los ingredientes clásicos conocidos, pero con el toque de sabor que brinda la cascarillas de cacao, también produce unas barras de chocolates intervenidas con hojuelas de yuca y otros frutos del trópico.

![Barra intervenida con frutos del trópico, yuca, coco, cambur y Nibs de cacao, elaborada por Jennifer Foster ](/images/uploads/foto_4.jpg)

![La Internacionalista Jennifer Foster mostrando una deliciosa barra de pasta de cacao del Productor y Maestro Chocolatero ECHAALBA Ricardo Murillo, quien nos facilita la semilla a varios artesanos para desarrollar chocolate artesanal](/images/uploads/foto_5_1_.jpg)

Por otra parte, el joven gaitero y exponente del género Ska [Diego García](https://www.facebook.com/diegohulk.dg), desarrolló un arte chocolatero asociado a la gaita y al Ska que son su devoción, elabora los Skacos que son unos chocolatines intervenidos con hojuelas de Yuca y coco deshidratados. Los Punkos unos ponquecitos elaborados con cascarilla de cacao. Las Colosales, como homenaje a la gaitas, unas barras de chocolate intervenidas con plátano verde y maduro y las Chococos que son galletas con cascarilla de cacao y harina de coco.

![Skacos, punkos y galletas chococos por Diego García, junio 2021](/images/uploads/foto_6_2_.jpg)

Al compartir nuestra experiencia, queremos invitarte a conocer de nuestro [Proyecto de Diseminar Cacao en Patios y Konukos de toda Venezuela](https://www.facebook.com/groups/1218049071634562), y si deseas incorporarte serás bienvenido y bienvenida.

#### Dra. Laura Morales (Sembradora de Árboles)

Correo: [sembradoradearboles@gmail.com](mailto:sembradoradearboles@gmail.com)

IG: <https://www.instagram.com/laurasembradoradearboles/>

Telegram: @Sembradora