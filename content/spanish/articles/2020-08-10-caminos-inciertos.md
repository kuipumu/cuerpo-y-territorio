---
title: Caminos inciertos
subtitle: Cómo emigrar me enseñó la lección más grande de mi vida
cover: images/uploads/caminos-inciertos.jpg
date: 2020-08-10 00:00:00
authors:
  - Verónica Escudero
tags:
  - Argentina
  - Venezuela
  - Migración
  - Medicina
  - Viaje
categories:
  - Cuerpo
comments: true
sticky: false
description: Desde muy joven no he sido especialmente buena para tomar
  decisiones, siempre estaban mis padres para sacarme de duda cuando las cosas
  me complicaban la cabeza. No era segura de mi misma y eso traía como
  consecuencia no tener claro lo que realmente quería.
---
Desde muy joven no he sido especialmente buena para tomar decisiones, siempre estaban mis padres para sacarme de duda cuando las cosas me complicaban la cabeza. No era segura de mi misma y eso traía como consecuencia no tener claro lo que realmente quería. 

Así que optaba por preguntar a otros qué consideraban ellos, qué era mejor para mí, hasta que un día me di cuenta que mi vida era un sin fin de decisiones que no me definían, que estaba en lugares a los que no pertenecía, y con personas que no me hacían sentir en casa.

Ya entrada en cierta edad, tuve que poner en práctica una tarea para poder ser la dueña de mis decisiones, me hacía tres preguntas - al estilo Marie kondo, escritora del libro "La magia del orden" – ¿lo necesitas?, ¿lo quieres?, ¿te hace feliz?, y así fue como transformé mi vida en lo que realmente soñé.

## De dónde vengo

Nací en Caracas Venezuela, acompañada de 2.000.000 habitantes. Viví toda mi vida en el centro de esa ciudad. Era una joven común con problemas comunes. No se me hizo sencillo elegir mi profesión, en principio quería ser abogada o politóloga y estudié Medicina igual que mi padre y mis hermanos, (quienes con su práctica médica me inculcaron el valor de la vocación de servicio), esta decisión fue influenciada por mi madre, por lo que la carrera no fue sencilla para mí. 

Tenía mucho interés en generar un cambio en la sociedad, mi vocecita interna me decía “tienes que hacer algo grande” y por alguna razón equivocada, creí que como médico no iba a lograrlo. Pero continúe, sin saber que se empezaba a gestar ese propósito.

Desde que era una adolescente quería conocer Argentina. Quizás las series que pasaban por los canales internacionales o los autores de libros reconocidos me dieron la idea. Recuerdo a mi hermano mayor preguntarme: ¿qué quieres hacer de grande?, mi respuesta fue: “quiero vivir en Argentina”. No sabía exactamente qué era lo que me atraía de este país, pero era claro que me gustaba. 

No vengo de una familia acaudalada, todo lo que tuvimos fue logrado con mucho esfuerzo de mis padres. Recuerdo de niña pasar largas madrugadas tomada de la mano de mi mamá en terminales de la ciudad (poco seguros) esperando que llegara transporte para volver a casa. Mi familia es católica, moralista, y un poco ortodoxa, así que la idea de migrar sola o irme de casa, era inconcebible.

Mis años como estudiante de medicina en la Universidad Central de Venezuela, pasaron sin pena ni gloria. Y en mis épocas de joven universitaria estando frente a mi laptop, apareció un anuncio en las páginas de la universidad para participar en un Modelo de Naciones Unidas en la ciudad de Buenos Aires, me postulé, como quien salta al vacío con los ojos cerrados, y un año más tarde después de una travesía entre preparación y recolección de recursos económicos, estaba en Argentina. Ya para ese entonces había alcanzado 2 objetivos: conocer Argentina e incursionar en el mundo de las políticas internacionales. 

Fui Jefa de la delegación al regresar a Venezuela, nos había ido tan bien que junto a mis compañeros encontramos nuevos integrantes para representar nuevamente, por segundo año, a mi universidad en Argentina.
 Al disfrutar esta experiencia dos veces seguidas, comprendí que todo lo que uno aspira puede ser alcanzado.

## Mi vida como médica

Finalmente me gradué, tuve la oportunidad de ejercer solo un año y medio en mi país en una zona humilde de barrios populares conocida como Las Minas de Baruta, en el estado Miranda. Mi primera experiencia médica fue maravillosa gracias a mis colegas, mi equipo de salud, y la institución. 

Sin embargo, la situación de Venezuela en el 2017, fue un momento de mucha tensión política y social, la caída de los servicios médicos y los servicios esenciales, la falta de insumos, las manifestaciones, la represión y el hambre en las calles, hicieron de esa experiencia algo distinto, en nosotros estaba el propósito de lucha y entrega por salvaguardar la vida de quienes padecían la ira política y social del Estado. 

En mí, ya estaba despierta la  intención de servicio a  mi país, en mente siempre llevaba la frase del Dr. Arnoldo Gabaldón (quien fuese el precursor de los programas de salud que erradicaron la malaria): "servir a Venezuela, no servirse de ella”.

 Mi meta era iniciar residencia médica como cirujana y seguir apostando por el país. Hasta que un día mi mundo colapsó la pérdida de un ser amado gracias al contexto de salud destruyó mi aspiración de servir, solo veía dolor en el rostro de mis pacientes, ese mismo dolor que yo reflejaba por mi pérdida. Era el rostro de la desesperanza.

Así que entre dudas y miedo tome la decisión de irme lejos de todo aquello que había sembrado dentro de mí ese sentimiento de hostilidad y tristeza.  

## Caminos inciertos

26 años. No tenía los recursos y el apoyo era escaso. El miedo de emigrar e iniciar de cero eran inevitables pero en un abrir y cerrar de ojos había vuelto a Argentina, que más que ser un sueño se había convertido esta vez, en mi hogar.

Viví la experiencia de todo emigrante, aprender sobre el verdadero significado de la palabra "resiliencia". Pasé nueve meses en la capital luchando contra mi ego y trabajando sin descanso en oficios que me hicieron desarrollar un nuevo sentido de identidad y humildad.

Pasó el tiempo y la vida me obligó a decidir entre la comodidad de la capital o volver a ejercer mi profesión. Así que ahí estaba tomando un avión y un bus a un pueblo al norte de Argentina, a casi 24 horas de distancia de la capital en carretera, mi nuevo hogar era una localidad con 16.000 habitantes.

Y fue en ese lugar donde la humildad, el cansancio, el hambre en el rostro, los pies descalzos, las inundaciones, las pérdidas de siembras, el barro, los suelos de tierra, los perros haciendo de las calles su hogar, los asentamientos, las altas temperaturas del verano (alcanzando los 47°C a la sombra muchas veces y siendo la deshidratación una de las causa más común de consulta hospitalaria), se contrastaba al mismo tiempo con el dinero en las calles, las miles de hectáreas de trigo y soja, la agricultura y la ganadería, que convierten a ésta zona en una de las de mayor producción del país, las camionetas de último modelo, el saludo fiel del que pasea por la plaza, los asados que inundaban todo el lugar con un olor particular los domingos y la benevolencia del que te tiende la mano sin tener mucho más que su cariño para ofrecerte, me obligaron a ver el mismo rostro con el que ya me había topado años atrás.

## La lección

Sí, claramente el contexto socio político era otro, las políticas de salud están enmarcadas en beneficio de la población, pero el sentimiento de desesperanza que sentí fue el mismo. No entendía cómo, a diferencia de la capital de mi país, esta zona tan apartada tenía equipos y suministros para atender a sus habitantes, carecían únicamente de personal y por eso yo estaba allí  junto a otro colega venezolano. Fue la ironía más grande con la que me he topado en mi vida.

Mi lugar de nacimiento no tenía nada que ver con este espacio, pero fue ahí, a las 4 de la mañana, sola, en una parada esperando el bus que, después de un viaje de cuatro horas, me dejaría en  la capital de esa provincia, que  sentí nuevamente  tomada de la mano de mi madre y fue cuando me di cuenta que  la vida me había preparado para entender y ayudar a que otros entendieran, que se puede estar en cualquier lugar del mundo, con lujos o no y la única razón por la cual no somos capaces de crecer como sociedad y como individuos, es nuestra falta de curiosidad y valentía, que lo único por lo que pueden llamarte pobre, es por falta de espíritu.

La vida te va a llevar por caminos inciertos, de eso no hay duda. Vas a necesitar estar preparadx para continuar sin entender muy claramente qué es lo que sucede pero con la convicción de que no hay otra opción por la que valga la pena vivir.