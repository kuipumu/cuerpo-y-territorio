---
title: Relaciones sociales del trabajo en la contemporaneidad
subtitle: Primera parte Sobre el trabajo y la producción
cover: /images/uploads/pexels-mike-417827.jpg
date: 2021-05-16T21:36:31.016Z
authors:
  - Luis Gabriel Aparicio
tags:
  - Trabajo
  - Produccion
  - automatizacion
  - teletrabajo
categories:
  - Politica y Economia
  - territorio
comments: true
sticky: true
---
Todos los 1° de mayo se celebra en el mundo el **Día del Trabajador**. En esta fecha se conmemoran los eventos sucedidos en la ciudad de **Chicago, Estados Unidos, en mayo de 1886**. En aquellos días miles de obreros en huelga solicitaban el establecimiento de la jornada laboral de 8 horas, cuando el día 4 de ese mes, en una manifestación que luego se conocería como la “Revuelta de Haymarket”, fueron reprimidos y asesinados cientos de trabajadores.

Más de 100 años después, los movimientos obreros siguen haciendo visibles en este día sus luchas y reivindicaciones por condiciones laborales más justas para las y los trabajadores en todas partes del mundo.

Desde el siglo XVIII hasta mediados del siglo XX, el capitalismo industrial y sus relaciones entre Estado, producción, capital y trabajo fueron el centro de análisis para una gran cantidad de movimientos, doctrinas, y teorías políticas y económicas, que tuvieron como principios teóricos las **ideas plasmadas** en las corrientes del **liberalismo, el anarquismo, el socialismo y el comunismo.**

Una de las principales doctrinas teóricas que hace su aporte al análisis social del trabajo, es la del socialismo científico o **marxismo**, en donde **Karl Marx** plantea la relación dialéctica entre la creación de valor y el trabajo en el capitalismo.

El autor, teniendo como contexto la fase industrial de este sistema, coloca como sujeto principal de su teoría a la **clase proletaria** que trabaja produciendo en las fábricas. Está clase social, según Marx, se encuentra a su vez oprimida por la clase burguesa explotadora, que se apropia y se beneficia de la plusvalía del valor generada por el trabajo de los proletarios en el proceso de producción.

En la teoría de Marx la clase proletaria pugna de manera permanente por ser liberada, y en ese proceso de liberación está llamada a tomar los medios de producción y el aparato del Estado, para establecer un nuevo sistema de producción superior al capitalismo, con nuevas y **distintas relaciones sociales**, superando la división de clases para dar lugar al surgimiento de una sociedad socialista o comunista.

**El liberalismo** económico, por su parte, es la otra doctrina teórica por excelencia que interpreta y propone cuáles deben ser las relaciones sociales del trabajo en el capitalismo, cuyos mayores representantes son los teóricos clásicos de la economía **Adam Smith y David Ricardo**. Esta teoría despliega una visión muy distinta sobre estos temas, ya que plantea que el capitalismo es el único sistema capaz de lograr en pleno el desarrollo del individuo y la sociedad, a través del trabajo, el libre comercio y la acumulación de capital.

Sus postulados principales son: la **libertad individual** tanto en el campo social como el económico para el desarrollo de las capacidades de cada individuo; y la mínima o nula intromisión del Estado en la dinámica económica, ya que el mercado con sus leyes tiene la capacidad de autorregularse.

**El neoliberalismo**, que surge a mediados del siglo XX, podemos decir que es una renovación teórica del liberalismo económico clásico, y plantea un relacionamiento distinto entre el Estado y el mercado.

La producción se convierte en el propio sentido de la existencia, es decir, la vida misma se centra en la producción de valor. El capital se diversifica y ya no es solo el que produce la actividad industrial, sino que también surge el **capitalismo financiero** y con la llegada de la **era digital**, la economía digital y digitalización de las finanzas.

En cuanto al trabajo, al igual que sucede con el capital, en el neoliberalismo las relaciones tradicionales del trabajo se amplían, no sólo existe la relación entre patrono y trabajador, sino que surgen nuevas formas de trabajo, nuevos relacionamientos y novedosas formas de explotación.

Con este preámbulo, y con el temor de ser en extremo reduccionista, hemos tratado de contextualizar a nuestros lectores sobre las **relaciones sociales del trabajo** en las que se desenvuelven los individuos en la sociedad contemporánea, y bajo qué corrientes de pensamiento han sido interpretadas hasta llegar a la actualidad, para así poder desarrollar con mayor claridad algunas ideas sobre las cuales hablaremos más adelante.

### La era digital y la automatización de los procesos

En las dos décadas que lleva el siglo XXI la sociedad ha sufrido grandes transformaciones, y las relaciones con la producción y el trabajo no se han quedado fuera de estos cambios. Hay quienes hablan de la **cuarta revolución industrial**, haciendo referencia a la creación y desarrollo de las nuevas tecnologías digitales aplicadas a la producción, la hiper conectividad en un mundo globalizado, la robótica, la Big data y el mundo 4.0.

Según estimaciones del **Foro Económico Mundial** (1) se perderán millones de puestos de trabajo debido a este proceso de automatización y digitalización de los procesos en los próximos años. Aunque debido al mismo proceso se espera la creación de otros millones de puestos de trabajo más, asociados a la producción de tecnología y sus insumos, pero también vinculados al mundo digital.

Sobre esto existen distintos autores que pudiésemos llamar **“pesimistas y optimistas tecnológicos”** (2), que defienden distintas posturas frente a la seguridad laboral durante los próximos años, debido al ritmo y equilibrio entre la creación de nuevos empleos, y la obsolescencia y desaparición de otros.

Ahora, lo que sí es cierto es que la transformación de millones de puestos de trabajo a gran escala se está realizando en este momento en una parte importante del mundo, y su impacto será cada vez más notable en la siguiente década. Siendo los más afectados en este proceso las y los trabajadores dedicados a actividades susceptibles a ser automatizadas y/o robotizadas tanto en la extracción y producción de materias primas como en los servicios y la manufactura.

### Teletrabajo y COVID 19

La pandemia del COVID-19 ha acelerado de manera vertiginosa este **proceso de cambio**. El cierre o los límites impuestos a las actividades económicas en casi todas sus ramas, debido a la pandemia, han hecho que millones de trabajadores pierdan sus empleos, pero también han llevado a que una gran cantidad de personas comenzaran a realizar todas sus actividades en el hogar en modalidades de teletrabajo. Valiéndose de las TICs, las redes sociales, las herramientas de videoconferencias, y aplicaciones de mensajería instantánea, esta forma de trabajo ha venido a tomar un lugar más grande del que ya tenía en la cotidianidad de las personas.

Este proceso de aislamiento de las y los sujetos, en este caso producido por la **pandemia**, potencia en gran medida las relaciones que propone el neoliberalismo para los individuos, hablamos de la atomización del individuo, la idea de la libertad personal por encima de la social, la hiper productividad de las personas, y el consumo masivo de bienes y servicios.

Y bajo estas mismas premisas se comienzan también a desarrollar las actividades y relaciones de trabajo, que es el tema que nos interesa reflexionar en este momento.

Hay una masa importante de trabajadores que se encuentran actualmente bajo la modalidad del **teletrabajo**, con todas las condiciones que esto implica: Poner al servicio del patrono sus propios medios de producción (equipos, servicios pagos en el hogar, etc.); además de desdibujar la línea entre el tiempo dedicado a lo doméstico y personal, frente a los requerimientos y horarios laborales, que lejos de disminuir aumentan.

Pudiendo ser estas condiciones de explotación mucho más visibles y marcadas para las **mujeres** dedicadas al teletrabajo, que además de compartir el tiempo con las actividades laborales, deben dedicarse a las labores vinculadas a la economía del cuidado, y por si fuese poco a asumir un rol directo en la educación de las y los hijos, la cual es delegada por las escuelas debido al cierre de las instituciones educativas.

Este panorama de por sí complejo, en donde casi nada se encuentra regulado por leyes salvo en casos y lugares muy específicos, no es para nada el peor de los escenarios. Estos **teletrabajadores** aún mantienen cierta estabilidad en sus puestos de trabajo, y son amparados bajo un marco legal y contractual entre patrono y trabajador, que de alguna forma se enmarca en las relaciones tradicionales del trabajo en el capitalismo y en la estructura social regulada por el Estado, beneficios que una gran cantidad de trabajadores no poseen.

Seguiremos reflexionando sobre estos temas en la segunda parte de este ensayo corto, donde abordaremos las implicaciones de las relaciones de trabajo bajo modalidades “libres” o **“freelance”** de trabajo, y la creciente tendencia social hacia el “emprendimiento” como forma de entender el trabajo y crear valor desde la auto-explotación en las sociedades neoliberales.

### Referencias y fuentes

(1) World Economic Forum, página web oficial.[](https://es.weforum.org/agenda/archive/fourth-industrial-revolution)

<https://es.weforum.org/agenda/archive/fourth-industrial-revolution>

(2) Rubbi, L., Barlaro Rovati, B. y Petraglia, A. (2020). Perdidos o salvados? El futuro del trabajo frente a la cuarta Revolución Industrial. Desde el Sur 12(1), pp. 307-276. <http://www.scielo.org.pe/scielo.php?script=sci_arttext&pid=S2415-09592020000100307>