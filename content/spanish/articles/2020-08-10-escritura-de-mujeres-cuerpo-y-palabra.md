---
title: "Escritura de mujeres: cuerpo y palabra"
cover: /images/uploads/imagen_para_redes._paula_bonet_pintura.jpg
caption: Pintura de Paula Bonet
date: 2020-08-10 00:00:00
authors:
  - Niyireé S Baptista S
tags:
  - Genero
  - Cuerpo
  - Mujeres
  - Feminismo
categories:
  - Género
sticky: true
comments: true
description: "Desde que comencé a indagar en la escritura de mujeres me ha
  acompañado una de las frases articuladas por Adrienne Rich que dice: “Escribir
  ‘mi cuerpo’ me sumerge en la experiencia vivida”."
---
> Siento, luego puedo ser libre.
>
> Audre Lorde

Desde que comencé a indagar en la escritura de mujeres me ha acompañado una de las frases articuladas por Adrienne Rich que dice: “Escribir ‘mi cuerpo’ me sumerge en la experiencia vivida”. Quizás, en cuestión de privilegios, el espacio que se interpone entre Rich y yo es grande. Bien lo dice ella: es blanca, judía, con acceso a una educación privilegiada, nacida y crecida en los Estados Unidos. Yo, en cambio, soy latina, mestiza, de ascendencia negra e indígena, que camina sostenida en medio de las turbulencias del ajetreado país que me vio nacer. Entonces, ¿qué me puede unir con esta mujer? La respuesta parece sencilla, pero dolorosa: el cuerpo, primer lugar de enunciación, de ubicación y de identidad.

Es en el cuerpo donde se establece la identidad sexual (hombre/mujer) que da lugar a la diferencia sexual y relega a las mujeres al espacio “privado”. Son los cuerpos de las mujeres territorios de conquista y han sido modelados y trabajados desde el sistema patriarcal para ser sometidos por el dominio masculino. Es justo, en ese sentido, que Rich y yo podemos acercarnos desde nuestra experiencia en un cuerpo de mujer, sin olvidar las otras identidades que se esgrimen sobre ellos. 

Nombrar y dar voz a la experiencia de las mujeres es un proceso complejo que pasa necesariamente por hacer visible las diferentes formas de opresión que se gestan sobre las identidades del cuerpo. 

La escritura posibilita el relato de la experiencia propia y ajena, desde aquellas mujeres que pueden narrarse y, a su vez, narrar a otras que aún están imposibilitadas porque sus cuerpos han sido acallados. 

Partiendo de esta reflexión, me pregunto: ¿desde dónde escriben las mujeres?, ¿acaso existe un lugar particular en donde nace la potencia que regurgita en las palabras que escribimos? Estas inquietudes me han llevado a la afanosa tarea de indagar en la narrativa de mujeres, en sus formas, en sus tonalidades, en los sentires y en la expresividad que manifiestan.

La narrativa de mujeres amalgama un entramado lenguaje de sentimientos, emocionalidad y experiencias que forman una textualidad sublime y dolorosa al mismo tiempo; es un tipo de escritura que habla de vivencias, algunas son experiencias autobiográficas o ficcionales que tienen la característica de converger en el dolor. Entonces, una certeza parece recorrerme: las mujeres escribimos nuestro cuerpo y el cuerpo tiene memoria.

## Cuerpo y razón en la escritura de mujeres

Hablar del cuerpo, del dolor que se gesta en él, de su contacto con otros, de su apertura a la vida y a la muerte, de su deseo y de su erotismo, es una forma de nombrar lo que se nos había negado a las mujeres. 
El cuerpo y la palabra fueron separados por la razón Occidental; con el advenimiento de la modernidad se dio entrada al racionalismo cartesiano y se dividió el cuerpo de la consciencia, nos dijeron que el primero era naturaleza y animalidad, y se correspondía a lo femenino, mientras que lo segundo era razón y pensamiento y pertenecía a lo masculino. 

Estas categorías, social y políticamente construidas, han impuesto que el hombre es cultura y la mujer naturaleza, como sostiene la antropóloga estadounidense Sherry Ortner en una de sus investigaciones. Hablar de la experiencia del cuerpo quedó vedado para las mujeres, el ojo masculino construyó a su imagen y semejanza la idea de la feminidad y escribió sobre ella asumiéndola como “el bello sexo”. 

Pero la pulsión de la escritura no puede ser aplacada tan fácilmente y con lo poco que nos dejaron, las mujeres se convirtieron también en escritoras y, por ende, en herejes, al subvertir el orden preestablecido sometiendo la opresión del silencio. Es por ello que se nos condena a ser clasificadas con los epítetos de “escritura femenina, de mujeres o íntima” en un afán de categorizar y regular el propio acto de escribir, en cánones que dictan qué es la literatura universal y qué se deja por fuera de ella.

La palabra de las mujeres toma de las experiencias del cuerpo un asidero, un lugar desde el cual narrar y narrar(se), lo que les permite fundar su discurso en el cuerpo y las opresiones que vive, no solo las que provienen de lo masculino, sino también desde el Estado y la sociedad. 

Es la escritura una manera de reivindicar el cuerpo como un territorio político, es un acto de enunciación y a la vez parece ser una manera, consciente o inconsciente, de transmutar el dolor de ser mujer. “Los más amplios horizontes de nuestras esperanzas y miedos están empedrados con nuestros poemas, labrados en la roca de las experiencias cotidianas.” 

## El cuerpo y las temáticas de la escritura de mujeres

Carolina Bruck, escritora argentina, comentó alguna vez que le gusta el cuerpo en la literatura y le gusta la literatura que pone el cuerpo, y cerró la frase diciendo que siempre se sintió como los personajes que escribe en sus cuentos.

Sus palabras me hacen reflexionar sobre los temas que se abordan desde la escritura femenina. Es cotidiano encontrar narrativas que incomodan por la dureza y crudeza con la que se presentan: aborto, maternidad, sexualidad, violación, femicidios, maltratos, pobreza, migración, racismo y estética son elementos que se conjugan y que suelen encontrarse en los textos de mujeres escritoras; tales son los casos de Socorro Venegas (México, 1972), Selva Almada (Argentina, 1973), Milagros Mata Gil (Venezuela, 1951), Jamaica Kincaid (Antigua y Barbuda, 1949) o Laura Restrepo (Colombia-1950), por mencionar algunas. 

Las escritoras mujeres entienden un mundo que se desdibuja en experiencias corpóreas; la propia fuerza de nuestra palabra y la convicción que en ella emanamos han hecho comprender que el cuerpo es el territorio más político que existe y que nos ofrece una ubicación, nos dice lo que somos y cómo nos definirán a lo largo de nuestras vidas. Por lo pronto, muchas son las inquietudes que me llevan a pensar y reflexionar de la escritura de mujeres, de cómo un cuerpo habla y muchas veces escribe:

> Me paro y he de parirme una y otra vez/ Soy animal que grita silencioso/Soy murmullo que estalla en risas y llantos/ Yo no he venido sola, estoy acompañada por una y por miles/ gritan todas, a la vez/ Soy cuerpo que siente desde sus entrañas, que emana vida y palabra, yo he venido a decir.