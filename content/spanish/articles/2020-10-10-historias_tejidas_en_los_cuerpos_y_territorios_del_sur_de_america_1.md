---
title: Historias tejidas en los cuerpos y territorios del sur de América (Parte II)
subtitle: "  "
cover: /images/uploads/d2b31c0d8a22e81926992d278bfa8906.jpg
caption: "Mural: Julián Rouras"
date: 2020-10-10T04:03:21.140Z
authors:
  - Juan Manuel Barreto
tags:
  - EdiciónEspecial
  - 12deoctubre
  - Identidad
  - Latinoamerica
  - Continente
  - Experiencias
  - Viajes
  - América
  - Historia
  - Argentina
  - Pampa
  - Cultura
  - PueblosOriginarios"
categories:
  - Territorio
comments: true
sticky: true
---
> *`“Como un tremendo viento`*
>
> *`dicen que fue el malón`*
>
> *`un torbellino en contra de los días`*
>
> *`y eso que los antiguos eran duros`*
>
> *`como rocas`*
>
> *`firmes`*
>
> *`ahí quedó su sangre`*
>
> *`desparramada`*
>
> *`me decías abuela`*
>
> *`y tu recuerdo es el lago`*
>
> *`al que me asomo`*
>
> *`para sorber un trago`*

**“Las mujeres y el viento”**

***Liliana Ancalao***

La conquista de la Patagonia y su incorporación definitiva al territorio nacional se llevó a cabo entre 1878 y 1885 en sucesivas campañas militares que pasaron a la historia oficial argentina con la denominación de Conquista del Desierto, donde “desierto” debía entenderse como sinónimo de “barbarie” o “espacio vacío”.

Para aquel entonces, las bases de la ciencia moderna europea ya habían sido establecidas, desplazando a la religión y a la tradición como fuente legítima de todo conocimiento. La fe depositada en el método científico aplicado a las ciencias sociales hizo surgir la promesa de una nueva redención secular en la que el progreso y la modernización eran simplemente el resultado lógico de aplicar la ciencia a los problemas de la sociedad. 

El positivismo ejerció una influencia tan insoslayable en la élite de intelectuales de la segunda mitad del siglo XIX, que creyeron encontrar en él la fórmula para solucionar todos los males. Sin embargo, por detrás de las proposiciones antropológicas y sociológicas de las cuales partieron sus diagnósticos y recetas para enmendar la nación, lo que había eran categorías de análisis que justificaban y profundizaban la dominación del hombre europeo en desmedro de lo que fuera diferente. La ciencia era la teoría de la evolución y la selección natural.

La frenología, el darwinismo social y el auge de las teorías raciales eran consideradas el último estadio del saber humano. Es a partir de este entendimiento de la ciencia -con su etnocentrismo implícito en ella- que se estableció el fundamento del genocidio perpetrado en la Patagonia.

Valiéndose de imágenes y estereotipos que ya circulaban en la época, la clase dirigente se esforzó en explotar, a través de los diarios y la platea política, el retrato del indígena malonero, saqueador, salvaje y violento como raza inferior opuesta al progreso de la humanidad que debía incorporarse al plan civilizatorio del Estado Argentino.

 Pero de verdad, la relación de los pueblos originarios de la Patagonia con el hombre blanco era mucho más compleja de lo que esta imagen sugiere y, prueba de ello, son los intercambios, las buenas relaciones y los acuerdos firmados desde tiempo atrás entre el gobierno y poderosos caciques del sur como Sayhueque y Casimiro Biguá. 

Estos lonkos no sólo reconocían a las autoridades nacionales sino que incluso decían considerarse argentinos y llegaron a comprometerse con la protección y la defensa de los pueblos desde el sur de Carmen de Patagones hasta el estrecho de Magallanes.

Las campañas militares organizadas por el Estado nacional tuvieron por objetivo eliminar todo vestigio de “barbarie” para que el territorio pudiera ser ocupado por grandes estancieros que abastecieran de materias primas a un mercado internacional en constante expansión. De a poco, las expediciones del ejército fueron quebrando la resistencia indígena y entre febrero de 1883 y enero de 1885 la mayoría de los jefes mapuches y tehuelches fueron eliminados en combate, pues muchos prefirieron morir a caer en manos de sus enemigos. Finalmente, ya acorralados y desprovistos de cualquier esperanza, los que no murieron acabaron por rendirse y fueron tomados como prisioneros de guerra.

Los campos de concentración en Valcheta, Fortín Castro y Choele Choel, todos en Río Negro, fueron la antesala para la deportación y destierro de miles de prisionerxs indígenas hacia lugares como la isla Martín García y el cuartel del Retiro, centros de recepción y distribución ubicados en Buenos Aires desde los cuales, posteriormente, se ejecutaron traslados forzosos a distintas provincias de la Argentina. Familias desmembradas, niñxs en adopción, fuerza de trabajo esclava esparcida por doquier, fueron los mecanismos implementados para borrar las raíces y la memoria. Con la desestructuración social del espacio indígena se los despojó (parcial o totalmente) de sus formas de vida, de sus tierras, de su orden social y económico, de su universo cultural.

Un caso paradigmático resultó ser el de Modesto Inacayal (cacique tehuelche) quien, luego de haber sido víctima de numerosos traslados, fue recluido junto a su familia en la Isla Martín García. No permanecieron, de todos modos, largo tiempo en aquel sitio ya que el perito Francisco P. Moreno, bajo el pretexto de rescatarlos, gestionó su traslado al Museo de Ciencias Naturales de La Plata. Moreno había conocido a Inacayal en sus viajes por el sur del territorio argentino y ocupaba en ese entonces el puesto de director en el museo. 

Desde ese espacio de poder, fijó un plan preciso para el cacique y los suyos (alrededor de 20 integrantes): durante el día fueron obligados a trabajar en diferentes áreas y tareas (desde la construcción a la limpieza) y comenzaron a ser objeto de estudios antropológicos (los medían, los evaluaban, los desnudaban, los exhibían en las vidrieras del museo y los hacían posar para fotógrafos); durante la noche, eran encadenados en los sótanos.

Circulan variadas versiones en relación a la muerte de Inacayal. Algunas de ellas refieren al suicidio mientras otras sostienen que fue empujado por las escaleras del museo en plena realización de un ritual. Existen certezas, en cambio, de que Inacayal tuvo que transitar los últimos días de su vida observando en las vitrinas los esqueletos de sus seres queridos. En definitiva, lo que buscó ser enterrado con la muerte de Inacayal es la evocación de un mundo.

> *`Mataron a mis hermanos`*
>
> *`Destruyeron las casas de rocas y piedras`*
>
> *`Dice el boldo llorando.`*
>
> *`Es el costo del progreso`*
>
> *`Para llegar más rápido a las ciudades`*
>
> *`hay que destruir la naturaleza.`*

**“Rvpv (Carretera)”**

***Mari Teresa Panchillo Neculwal***

> *`Escribo masacrándome,`*
>
> *`mostrando,`*
>
> *`abriendo llagas en qué llorar`*
>
> *`Y golpear en tantos pechos.`*
>
> *`Plegaria en los murmullos.`*
>
> *`Escribo con velas en los ojos.`*

**“La seducción de los venenos”**

 ***Roxana Miranda Rupailaf***

Si pensáramos en nuestra América como un gran cuerpo y territorio que decide mirarse en un espejo continental, desaparecer una parte de su historia pasada, presente y futura e implantar una visión monolítica de la vida y del mundo, sería equivalente a imaginar que el reflejo que nos devuelve ese cristal tiene mutiladas sus piernas, sus brazos o su cabeza.

El advenimiento de la colonialidad/modernidad ha puesto al individuo por sobre la idea de comunidad y con ella se han procurado silenciar los diversos modos de vida y tradiciones que afianzaban este sentido de pertenencia y que, a su vez, hacían distintas y particulares a unas comunidades de otras.

¿Por qué insistimos en seccionar nuestra identidad a una sola porción de tierra delimitada por un territorio particular y costumbres propias cuando estas últimas se replican hasta en el otro extremo del continente? ¿Cuál es el miedo detrás de asumir nuestras diferencias? ¿Será lo mismo que no nos permite hoy en día aceptar las diversidades sexuales?

Gran parte de esta negación de lo diverso se la debemos al exterminio sistemático de los tiempos de la conquista, pero resulta interesante pensar, también, de qué formas estas lógicas persisten y se repiten cíclicamente a través de la historia.

¿Cómo ser consecuentes, entonces, con este pensamiento de aceptación de la otredad?

Quizá la pregunta deba volver sobre unx mismx, ya que como nos muestra el feminismo, la opresión no es un gran monstruo lejano sino que se presenta escondido en las pequeñas acciones cotidianas. Será necesario, por lo tanto, revisar nuestras propias prácticas para poder deconstruir progresivamente la herencia colonial.

¿Cómo nombrar lo que nos duele y desgarra pero que es también constitutivo de las propias identidades? ¿Cómo fortalecer y valorar esos otros relatos sin que signifiquen operaciones e intervenciones desde el exterior hacia los pueblos originarios? ¿Qué imágenes reconstruir con miras a juntar esos fragmentos dispersos para reconectarlos con la historia de América?

El eurocentrismo nos impone pensar los vínculos con lxs otrxs en términos binarios. Rechazar esa concepción nos reclama construir opuestos dialógicos y complementarios, donde ninguno de los términos se torne universal.

Progreso y desarrollo, ¿para quiénes y a qué costo?

El sometimiento de las comunidades originarias y la negación de tierras es una dimensión de la reproducción de un sistema económico, político y social que perpetúa la dominación y explotación de lxs más poderosxs sobre lxs más débiles. A pesar de esto, aquello que pretendió clasificarse como belleza muerta no ha sido enterrado en los confines de otros tiempos; resiste y renace en cada veneración de la naturaleza, en cada ritual que es punto de encuentro y de socialización, en cada narración que conversa con la conciencia del universo donde el ser humano forma una trama con otros seres vivos e inanimados, en cada voz o colectivo que promueva la libre determinación de los pueblos, de los cuerpos y de los territorios, en cada resistencia anti-colonial, en cada lucha anti-patriarcal.

Parece oportuno recordar lo que unas señoras mayores nos cuentan del tiempo “cuando se perdió el mundo”. Somos hijos de la Tierra, pero la Tierra no nos pertenece. Somos parte de un tejido que ha venido siendo dañado durante siglos y, aún así, la tierra nos cobija. Aprender a escucharla significa, entonces, redescubrir todo lo sagrado que ha sido profanado. Raíces, pájaros cantores, árboles añejos, voces de ancianxs que transmiten y revelan otra forma de habitar el mundo. Todo a nuestro alrededor habla. Oralitura. Mapuzungun. Idiomas de la tierra. Aprendiendo a escuchar percibiremos, quizás, que desde los suelos brotan los gritos que reclaman con urgencia una nueva matriz civilizatoria.

**Texto colectivo creado por Sebastián Casco, Antonela Perri, Joaquín Pico y Juan Manuel Barreto.**

#### ***Otras entregas de esta edición especial “12 de octubre”***

[Historias tejidas en los cuerpos y territorios del sur de América (Parte I)](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america-parte-i/)

[¡Más que nunca, las venas están abiertas](https://revistacuerpoyterritorio.com/2020/10/10/mas-que-nunca-las-venas-estan-abiertas/)!

[Las doce Malinches o el mito que seremos ](https://revistacuerpoyterritorio.com/2020/10/10/las-doce-malinches-o-el-mito-que-seremos/)

<!--StartFragment-->

#### Además, los invitamos a conectarse a la tertulia de YouTube live este 12 de octubre para conversar, desde otras perspectivas, sobre esta fecha tan importante en América haciendo clic [AQUÍ](https://www.youtube.com/watch?v=Bddgfi3tARU&feature=youtu.be)

![](/images/uploads/12_de_octubre_la_historia_no_contada_de_america_9_.png)