---
title: '"Me enfermé", una forma de nombrar la menstruación'
subtitle: " "
cover: /images/uploads/foto_3.jpg
date: 2020-09-16T00:41:10.156Z
authors:
  - Shirly Dana
tags:
  - Menstruación
  - Género
  - Mujeres
  - Patriarcado
  - Sociedad
  - Tabu
  - Cuerpo
  - Feminismo
  - Cultura
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

Un día, hace algunos meses, me encontré con una persona boliviana que me dijo **“me enfermé el siete y me curé el diez”** para hacer referencia a su menstruación. Ese día quedé muy sorprendida por la forma en la que esta persona menstruante la nombraba.

**Pensar que este proceso fisiológico que sucede en los cuerpos con útero cada mes tiene esta connotación de enfermedad**, lo asocia nada inocentemente con energías negativas o malas, relacionadas con la implicancia que tiene para las personas estar enfermas. Además, puede llevarnos a pensar que cuando une está enferme, necesita cuidados y, habitualmente, dichas tareas se encuentran muy feminizadas, pero este sería un debate para otro artículo.

Cuando comenté la situación a mis amigues, trajeron a mi atención las formas en las que nos referimos a la menstruación les porteñes, acá en Argentina. *“Estoy en esos días”, “me vino”, “vino Andrés”, “estoy con la regla”, “estoy indispuesta”, “estoy con la luna”* son las que resuenan en casi todos los ámbitos. **Algunas personas incluso me dijeron que lo dicen así, tal cual, sin vueltas “estoy menstruando”.**

Al principio, sentía muy ajena la connotación de enfermedad que me había quedado de aquella entrevista, pero revisando nuestros modismos fue inevitable detenerse en la frase **“estoy indispuesta”**. **El discurso y nuestras palabras no son inocentes.** Están cargadas de diversos significados que llevan consigo marcas y determinan cómo interpretamos los procesos que atravesamos, que atraviesa nuestra corporalidad en este punto en particular.

Cuando la sacamos de contexto y la pensamos o la buscamos en los diccionarios, la palabra indisposición hace referencia a no estar dispueste o preparade para algo. Entonces, no tan lejana a la concepción que circula en algunas personas de la cultura boliviana, **acá también los días menstruales de nuestros ciclos son vistos como una enfermedad, como limitantes.**

Aprendemos rápidamente de nuestro entorno familiar y/o amistoso que en esos días podemos sentirnos mal, podemos quedarnos en la cama. Las publicidades y los nombres de los productos publicitados fomentan esos sentires y promocionan formas de liberación (como por ejemplo, la marca de toallitas “siempre libre”). **En las viejas publicidades incluso utilizaban un líquido azul para representar la sangre menstrual, incrementando el tabú generado sobre este sangrado.**

Incluso la industria farmacéutica desarrolló medicamentos específicos para los dolores menstruales, elevando el costo por la marca bajo la cual se venden. Las molestias asociadas a esos días de nuestros ciclos pueden ser sobrellevadas con medicamentos habituales, sin estar sobrevaluados por la marca que poseen, así como también con distintas medicinas naturales, de fácil acceso y transmisión entre generaciones.

**En el libro Ginecologia Natural profundizan en este aspecto y en la necesidad de deconstruir las creencias alrededor de la malignidad de la sangre menstrual**, buscando pensarla y sentirla como un proceso que llega para limpiar esos males, y no como su representación. Poder revisar las creencias existentes en nuestra cultura sobre la sangre menstrual nos permite comprenderla, resignificarla y generar nuevas tradiciones que sintonicen con nuestra forma de entender la menstruación, desarmando los estigmas impuestos por la sociedad y la cultura que nos rodea.

Tal es así que conocer las creencias, construcciones sociales, culturales y políticas que se estructuran en torno a este suceso fisiológico y oculto es fundamental para poder analizar el impacto que tiene en las personas con útero. Además, tener esta información es **el puntapié inicial para desarticular el discurso patriarcal que gobierna estos cuerpos y que perpetúa la desigualdad sexogenérica, incluyendo a la menstruación como una situación más de desventaja para las mujeres y las personas con útero menstruantes.**

No obstante, como explica Tarzibachi, la menstruación no puede reducirse únicamente a un suceso fisiológico que ocurre en las personas con útero en su intimidad, menstruar también es un hecho político. La imposibilidad de nombrarla perpetúa su ocultamiento y extiende los tabúes que se arman a su alrededor, pretendiendo encubrirla. Todo lo mencionado impacta en la vida de las personas menstruantes y profundiza las diferencias ya existentes y mencionadas entre géneros. **Las limitaciones que desarrolla continúan posicionando a las mujeres y diversas feminidades como seres inferiores, patologizando a estos cuerpos y sus procesos.**

En esta misma línea de pensamiento, es imposible continuar el análisis sin hacer un alto en las diferencias que implica a nivel socioeconómico menstruar. Las dificultades en el acceso a adecuados instrumentos de gestión menstrual es uno de los factores que influyen en el ausentismo escolar y laboral. Los motivos son variados, entre algunos se encuentra la falta de producción y entrega estatal de dichos productos, su inclusión en la canasta básica y la falta de difusión poco ingenua de los instrumentos ecológicos y económicamente rentables, como la copa menstrual y las toallitas de tela.

Como respuesta a algunas de estas problemáticas en Argentina, desde EcoFeminita se lanzó una campaña llamada **MenstruAcción, buscando visibilizar la misma, luchando por la producción y provisión estatal de instrumentos adecuados para la gestión menstrual.** Realizaron diversas colectas de donaciones de productos para distribuirlos entre las personas con mayores dificultades en el acceso.

En el documental ***Period. End of sentece** (**), cuentan cómo un grupo de mujeres de India montan una fábrica autónoma de desarrollo de toallitas menstruales dirigida por ellas mismas. Ellas no sólo vendían dichos productos a precios mucho más accesibles, sino que además **llevaban adelante charlas grupales para hablar de la menstruación, buscando desmitificar y acompañar a les más pequeñes en su aprendizaje y tránsito.**

Estos son algunos ejemplos de muchos otros que están sucediendo alrededor del mundo. Así como también la producción de información y su difusión, buscando dar voz a distintas personas, hecho que logra cuestionar las autoridades epistémicas que pueden escribir sobre menstruación y sus impactos. Creo que no hay mejores personas para hablar de menstruar y de la sangre menstrual que quienes transitamos esta situación cada mes.

**La menstruación y el ciclo menstrual forman parte de la vida cotidiana de las personas con útero,** sobre este hecho no hay ninguna duda. Pero, ¿alguna vez te preguntaste cuál es la implicancia de la menstruación en las vidas de estas personas? ¿Qué construcciones socioculturales giran en torno a este hecho “inevitable”? Y de la misma forma que hablar de sexo, nombrar la vulva y los genitales femeninos genera incomodidad, algo parecido pasa con la menstruación. Vos, ¿cómo la nombras?

*(*) Traducción a español: Punto. Fin de la oración. // Período. Fin de la sentencia*

**Bibliografía:** 

* [Revista Bordes](http://revistabordes.com.ar/menstruar-tambien-es-politico/?fb_comment_id=1782169641794185_1789399247737891)
* [Economia Feminita](http://economiafeminita.com/menstruaccion/)
* [La caravana roja](https://lacaravanaroja.com/que-es-la-cultura-menstrual)
* [Revista Crítica](https://revistacitrica.com/menstruar-en-tiempos-de-aislamiento-social.html#:~:text=Es%20preciso%20considerar%20que%20menstruar,audiovisuales%20que%20nos%20orienten%20hacia)
* Eugenia Tarzibachi, Cosa de Mujeres 
* Pabla Perez San Martin, Manual introductorio a la Ginecología Natural

<!--EndFragment-->