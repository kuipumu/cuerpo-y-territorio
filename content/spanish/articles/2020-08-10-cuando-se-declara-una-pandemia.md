---
title: 'Cuando se declara una pandemia'
subtitle: 'y el camino del trabajo a casa tiene miles de kilómetros'
description: 'Según datos de la ITF (International Transport Federation) en el mes de julio, 300.000 marinos se hallaban "atrapados" en sus lugares de trabajo.'
cover: 'images/uploads/cuando-se-declara-una-pandemia.jpg'
authors:
  - Patricia Lepratti
categories: 
  - Política y Economia
tags:
  - Marinos
  - Trabajadores del Mar
  - COVID-19
  - Pandemia
  - Derechos
date: 2020-07-16 00:00:00
comments: true
sticky: true
---
Según datos de la ITF (International Transport Federation) en el mes de julio, 300.000 marinos se hallaban "atrapados" en sus lugares de trabajo. Desde que en marzo de 2020 la OMS (Organización Mundial de la Salud) caracterizó a la situación sanitaria desatada por el COVID-19 como pandémica, los trabajadores del mar han visto coartadas sus posibilidades de abandonar las naves en las que realizan su labor, incluso después de haber finalizado sus contratos. 

Los trabajadores del transporte marítimo, de la pesca industrial y los tripulantes de cruceros normalmente trabajan y viven a bordo de un buque hasta 10 meses seguidos. Pero en este momento, las limitaciones a los desplazamientos internacionales de las personas en muchos países, han restringido el acceso a tierra de estos trabajadores y, por ende, su posibilidad de retornar a sus hogares. 

En este contexto, los marinos fueron declarados trabajadores esenciales por la OIT (Organización Internacional del Trabajo), institución que junto a otras viene llamando a los gobiernos a introducir procesos que les permitan abandonar sus lugares de trabajo y viajar a sus hogares.

La pandemia y las medidas para controlarla han expuesto diversas situaciones. Una de ellas ha sido la magnitud del número de trabajadores que realizan su labor cruzando fronteras y las dificultades que encuentran al momento de hacer oír sus demandas. Son extranjeros en su lugar de trabajo y están ausentes para su estado o nación de origen. “La doble ausencia” de la experiencia migratoria, como la definió el sociólogo argelino y francés (o no del todo argelino, ni del todo francés) Abdemalek Sayad.

En este sentido, el caso de los trabajadores marítimos resulta paradigmático si se quieren abordar las experiencias laborales transnacionales de la actualidad, pues los marinos pueden viajar a otro país a embarcarse en un buque que, a su vez, tendrá una bandera de un tercer país. 

Es decir que, como sucede habitualmente, los marinos trabajarán bajo una jurisdicción diferente a la de sus países de origen, a la de aquel en la que han realizado el embarque y, cuando se encuentren en altamar, navegarán por aguas territoriales de otros países, así como por aguas internacionales. Sin embargo, quizás nunca lleguen a tener contacto con personas o instituciones del país del pabellón del buque, ni con aquellas de las aguas por las que navegarán; tampoco serán considerados migrantes por ninguna de las jurisdicciones involucradas en su tiempo y espacio de trabajo.

Desde el comienzo de mis estudios de posgrado en ciencias sociales a mediados del año 2012 (primero maestría y luego doctorado), he estado profundizando en el estudio de esta realidad laboral en la que el cruce de fronteras y el confinamiento en un mismo espacio por períodos prolongados se dan de forma simultánea. Una realidad laboral que, por otra parte, determina estilos de vida específicos, pero a la vez representativos de un mundo en el que producción y reproducción social se encuentran cada vez más transnacionalizados y, al mismo tiempo, fragmentados.

A partir de la revolución industrial la disponibilidad de trabajadores y su movilidad –ya sea al interior de las fronteras nacionales o a través de ellas- han estado estrechamente vinculadas. Pero si el cruce de fronteras representa, entonces, una estrategia laboral más de entre las tantas existentes para el mantenimiento y la reproducción de la vida en la actualidad ¿qué pasará con las posibilidades de movilizarse en busca de trabajo ante los nuevos dispositivos de seguridad sanitaria?, ¿devendrán en nuevas limitaciones a la circulación de trabajadores?.

Si se cierran las posibilidades a la circulación del trabajo, ¿también se cierran determinadas estrategias de reproducción social y de acceso a derechos?. En situaciones en las que los derechos de los trabajadores extranjeros - o incluso de los nacionales - sean vistos como una amenaza a la seguridad sanitaria dentro de un territorio determinado, ¿qué criterios prevalecerán: el respeto de los derechos de los trabajadores o el de las normas de seguridad sanitaria?.   

Estas y otras tantas preguntas disparadas por el actual escenario de respuesta al COVID-19 necesitan ser respondidas por la observación, el análisis y la búsqueda de soluciones en común. Como nos lo muestra el caso de los 300.000 marinos imposibilitados de retornar a sus hogares, la futura “normalidad” no podrá seguir haciendo la vista gorda ante tantas vidas para las que el trabajo y la familia están separados por miles de kilómetros. 

Con el objetivo de dar mayor visibilidad y profundizar en el análisis de estas realidades, les invito a que a través de las próximas columnas pensemos juntos sus múltiples dimensiones. El 2020 nos puede tener aturdidos, ansiosos, desorientados, pero también nos toca estar atentos. 