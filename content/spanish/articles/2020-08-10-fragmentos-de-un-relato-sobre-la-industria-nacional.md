---
title: 'Fragmentos de un relato sobre la Industria Nacional'
subtitle: 'Argentina Siglo XXI'
description: 'Me arrimo hasta acá, hasta este fueguito en plena gestación de un cuerpo cuyos territorios serán hechos de palabras, con la intención de contar una historia basada en hechos reales o verídicos, o sea, que sucedieron en tiempo y espacios concretos, palpables y visibles.'
cover: 'images/uploads/fragmentos-de-un-relato-sobre-la-industria-nacional.jpg'
authors:
  - Verónica Vazquez
categories: 
  - Política y Economia  
tags:
  - Política 
  - Sindicatos 
  - Trabajadores 
  - Argentina  
  - Cooperativas 
  - Trabajadores Gráficos
date: 2020-08-10 00:00:00
comments: true
sticky: true
---
## Episodio I

Me arrimo hasta acá, hasta este fueguito en plena gestación de un cuerpo cuyos territorios serán hechos de palabras, con la intención de contar una historia basada en hechos reales o verídicos, o sea, que sucedieron en tiempo y espacios concretos, palpables y visibles.

Lo fáctico o real puesto en cuestión desde una mirada suspendida en la sucesión de días en cuarentena; la versión de una historia forjada en base al sudor y las lágrimas, pero también en espantos y necesidades. Es la historia de una experiencia muy particular durante la cual muchas veces no podía creer que estuviera ocurriendo, que fuera de verdad. Pero la realidad es siempre la única verdad: y esta historia realmente ocurrió.

Me refiero a una hecho  donde el denominador común son otro tipo de relaciones sociales: las marcadas por el carácter autogestivo signadas a fuego por la impronta del cierre, vaciamiento o quiebra de la unidad productiva donde, durante años, nos hemos desarrollado en nuestras funciones como trabajadores en relación de dependencia, esto quiere decir que la pregunta por el capital no recaía sobre nosotros: era la pregunta de los jefes, de los dueños de la empresa. 

Nosotros simplemente cumplíamos nuestro trabajo durante cierta cantidad de horas a cambio de recibir un salario. Tal es así que de pronto nos vimos frente al reto de tener que responder estas preguntas: ¿de dónde y cómo conseguir el capital para que la empresa pueda continuar funcionando? ¿Cuál es la mejor manera para administrarlo? ¿Cómo hacer para aumentar ese capital y poder mejorar su distribución? ¿Es una empresa recuperada una cooperativa de trabajo con la misma lógica de funcionamiento empresarial?. 

>“Las empresas en quiebra son ocupadas por obreros reales, de carne y hueso, formados ideológica y políticamente en el movimiento sindical argentino tradicional, o en ninguno, obligados a iniciar el camino de la autogestión, con todos los enormes desafíos que ello implica en una sociedad capitalista dependiente y en crisis como la Argentina, forzados por las circunstancias y por la imposibilidad de hacer otra cosa que **tomar el futuro entre sus manos**”
>
>Lic. Andres Ruggeri (FFyL-UBA 2005).

Vengo a contarles mi experiencia de laburo cotidiano asociada a una cooperativa de trabajo (ERT – Empresa Recuperada por sus Trabajadores) durante ocho de sus gloriosos casi diez años de existencia. Vengo a contarles qué se siente y cómo es trabajar para levantar una empresa nueva, surgida de las cenizas de otra empresa privada que no tenía fecha de vencimiento ni propósito de ser gestionada por 11 de sus más de 50 empleados que, luego de un fin de semana de ocupación, pasó a ser dirigida por elles mismos. 

Hubo que hacerse dueño a la fuerza, hubo que encarar la desconocida aventura de tomar decisiones -¿es casi como tomar el poder, tomar decisiones?-  sobre los bienes que el patrón no se pudo llevar y que le disputamos como compensación de salarios y aguinaldos jamás pagados. 

Llevar a cabo una idea implica necesariamente que tengamos la suficiente flexibilidad como para no enloquecer en el intento y aceptar que las cosas son como son y no como deberían ser según tu propio criterio. Es importante contar con preparación en el rubro y “reparación del corazón”. ¿Cuándo no soñamos en grande, en abundancia para las mayorías a las que se pertenece por prepotencia de clase?.

Es que vivo en un país donde la historia se repite: los gobiernos populares pagan la fiesta de la oligarquía. En el caso de los cuatro años de macrismo, no solo 88 mil millones de dólares se fueron por la canaleta de la fuga de capitales. La deuda es con el Pueblo, aunque siempre la terminan pagando a costa de ajustarnos.