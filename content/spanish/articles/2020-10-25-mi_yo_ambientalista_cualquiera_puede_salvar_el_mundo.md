---
title: Mi "yo" ambientalista, cualquiera puede salvar el mundo
subtitle: " "
cover: /images/uploads/relaxing-in-nature.jpg
date: 2020-10-30T11:30:12.227Z
authors:
  - Catalina Medina Barrios
tags:
  - Ambiente
  - Hábitos
  - Acciones
  - Entorno
  - Limpieza
  - Cuidado
  - SaludAmbiental
  - GestiónAmbiental
categories:
  - Territorio
  - Cuerpo
comments: true
sticky: true
---
Hay dos cosas que pocos saben, una mi primer nombre -que aún no te lo contaré- y la segunda que estudié Gestión Ambiental. La verdad es que desde mi proyecto ambiental en la farmacéutica donde realicé mi práctica profesional no he trabajado en nada ambiental y no por falta de oportunidades, simplemente es un tema que aunque me cuestiona, leo y sigo nunca me ha interesado trabajar de manera colectiva, me gusta trabajarlo en mí, sé que puede sonar un poco egoísta, **pero he aprendido que no hay mejor manera de impactar y transformar el entorno que con nuestras acciones,** y mis acciones no serían lo que son si no tuviera el conocimiento ambiental que tengo y que aprendo constantemente.

Por eso en este artículo quiero salir del closet en términos ambientales y lo quiero hacer conmemorando el **Día de Acción Global sobre la Educación para el Consumo Sustentable**, y lo haré compartiendo algunos de mis hábitos.

El día de Acción Global sobre Educación para el Consumo Sustentable fue impulsado por la organización Customer International con el ánimo de promover la enseñanza de un consumo sustentable en los curriculum de los colegios de cada país, con el propósito de **educar en el consumo y estilos de vida sostenibles fomentando desde una edad temprana decisiones y hábitos de compra responsables, por eso elegí este día, porque mi propósito hoy es compartir conocimiento.**

Toda mi vida he estado en lugares con mucho verde, mi niñez y adolescencia viví cerca a un humedal en Bogotá y en mi casa tenía un árbol gigante donde podía bajar limones para jugar con mis primas y primos. Estudié en un colegio que quedaba en la montaña, en donde era constante escuchar sobre el cuidado con las basuras para no generar algún incendio forestal y desde los 16 años vivo en un pueblo cerca de la ciudad donde mis vecinos son vacas, caballos además de animales más pequeños. 

![Foto: Catalina Medina Barrios](/images/uploads/cata_linamb_15625544_1824759411140887_510321124983177216_n.jpg)

**Esta cercanía me dio una base de la importancia de mis actos en relación con todos los seres vivos forjando hábitos en mí en torno al cuidado del medio ambiente, algunos de esos hábitos que han evolucionado o mutado, esos los que voy a compartir contigo.**

**El primer hábito** **es con respecto a los alimentos**, seguramente lo primero que se te viene a la cabeza con alimentos es el consumo de carne, sin embargo, te cuento que no soy vegetariana, ni vegana, -como varios suponen cuando me conocen- la verdad es que amo la carne, aunque no la consumo constantemente, por eso lo que te voy a compartir es algo más sencillo -para mí- que dejar la carne.

Y es compartir de dónde vienen, cómo aprovechamos y dónde terminan los alimentos que consumimos en mi casa, realizamos el mercado de verduras en un lugar no muy grande, pero que trae productos directamente del campo, producciones pequeñas porque no utilizan productos químicos, seguramente nos queda cerca porque vivimos en un pueblo, pero en las ciudades también podemos encontrar este tipo de alimentos, en mercados campesinos y en plazas de mercados, es una manera de garantizar buenos productos para nuestros hogares y apoyar a los campesinos de nuestro país.

**En mi casa no se desperdicia nada, hay una crema inventada en mi casa que se llama “Todo lo que encontré”** en donde se licúan las verduras que hayan quedado de alguna preparación anterior, creando un plato para el almuerzo diferente y sin tener que botar comida, así mismo los residuos orgánicos tienen su propia caneca, una caneca verde en donde semanalmente sacamos el líquido lixiviado que genera y con un poco de agua -por que huele muy mal- se convierte en el riego para las plantas y jardines de la casa, lo sólido se utiliza para hacer compost, nosotros no realizamos compost y seguramente en tu casa tampoco, pero existen lugares donde sí y que constantemente están abiertos a recibir estos residuos.

**El segundo** lo he ido perfeccionando y **es mi hábito de compra,** como todos, he comprado cosas que no necesito, no voy a negarlo, soy amante de las agendas, los libros y los bolígrafos así que tengo mi talón de Aquiles, solo que ahora los compro cuando se me terminan y no simplemente porque me gustó y los quiero tener, pero como digo, todo es evolución y perfeccionamiento, actualmente antes de cualquier compra -así sea una suscripción virtual- lo analizo, si lo voy a utilizar, cuánto tiempo lo voy a utilizar o si es necesario comprar algo o alguien más puede prestármelo, con mi ropa algo parecido, si veo algún outfit que me gusta reviso dentro de lo que tengo, si ya tengo algo que no me gusta lo intervengo pintando algo, cociendo algún aplique o transformándolo, así cambio de closet sin comprar prendas y no desecho prendas que están en perfectas condiciones, las que no me quedan las lavo bien y les encuentro un nuevo dueño.

**Y el tercer hábito** que quiero compartir contigo es muy nuevo y aún estoy trabajando en él, **es mi administración de la nube**, constantemente a nuestras bandejas de entrada del correo y en nuestros computadores mantenemos un mundo de información que si estuviera en físico, seguramente a tí y a mí nos tendría locos el desorden, sin embargo, como no ocupa espacio físico en nuestro ambiente los ignoramos, pero todos esos documentos, fotos y demás archivos generan un impacto, es por eso que además de limpiar el computador y el celular de aplicaciones que no utilizo constantemente y que ocupan espacio en mis dispositivos.

A inicios de 2020 hice una limpieza de mi bandeja de entrada y del Drive; agrupé los correos de mis proyectos en uno solo para no generar doble información que ocupe espacio y mantengo mis documentos, el escritorio y mi papelera limpio, hago esta limpieza semanalmente porque aún género durante mi trabajo mucha basura digital -así lo llamo yo-, las aplicaciones que puedo trabajar en las páginas las trabajo desde ahí y no las descargo en mi computador esto también hace que no duplique información, las fotos aunque dolió, las eliminé, las que no estaban en Instagram las dejé y el resto las boté, ya que si en algún momento las necesito mi álbum será la cuenta de Instagram, seguramente hay más que aprender, así que por ahora esto es lo que hago.

Estos son solo 3 acciones que quise compartir contigo **pero hay muchas más cosas que podemos aprender y hacer para modificar nuestros hábitos de consumo y de cuidado de nuestro entorno,** seguramente no es sencillo transformar hábitos pero paso a paso se llega lejos por eso, para que no solo tengas estos ejemplos quiero compartir contigo estos podcast [Be Clá](https://open.spotify.com/show/1XzoWJmVsSieXuvQpb4XcP?si=pVyA5mX5RzCr6RmhsErNwg), [Ideas A Granel](https://open.spotify.com/show/1lVPIELQYeKTXUwRkNXOVd?si=_2qJatnIQQKHpr7imD06bA) y [Lo de Adentro Importa](https://open.spotify.com/show/2lqLm2DkQ4yaXqqn5bkqEO?si=Xj9mp1H8SwyR0KvEhYvjQA), este blog: [animaldeisla.com](http://animaldeisla.com) y la [Guia de los vagos para salvar el mundo](https://www.un.org/sustainabledevelopment/es/takeaction/), **todo el contenido es genial para que tengas más conocimiento de lo que cada uno de nosotros podemos hacer.**