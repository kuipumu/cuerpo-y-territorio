---
title: El monstruo y las brujas
subtitle: " "
cover: /images/uploads/ilustracion_jorobado.jpg
date: 2020-12-19T19:56:32.441Z
authors:
  - Roberto Salazar
tags:
  - Paris
  - Psicoanáilisis
  - Freud
  - NuestraSeñoraDeParis
  - Género
categories:
  - Cultura
comments: true
sticky: true
---
No tenía un aspecto demasiado amenazador. Yacía sentado en un sillón rojo mientras era presentado al auditorio. Estaba serio, pero a ratos mostraba una sonrisa generosa. Miraba a los lados, entre desafiante y complacido, aunque no dejaba de mostrar cierta intranquilidad en su postura.

Se trataba de un hombre de mediana edad, delgado, de rostro pálido, cabello entrecano, vestido ligeramente informal, con ropa holgada de colores oscuros.

Es presentado como Paul B. Preciado. Su fama le precede. Es uno de los hombres trans más conocidos de Europa. Escritor, filósofo y activista.

Será el monstruo.

El escenario es el Palacio de Congresos, en París, durante las jornadas anuales de la Escuela de la Causa Freudiana `[1]`. Uno de los institutos psicoanalíticos más renombradas de Francia y, ergo, del mundo. Cerca de 3500 asistentes concurrieron al evento titulado “Mujeres en Psicoanálisis”. Aunque no todos son psicoanalistas, la mayoría de los presentes ejercen el psicoanálisis y se reconocen como mujer.

Serán las brujas `[2]`.

¿Qué mejor lugar para juntar a monstruos y brujas que la gótica París? ¿Acaso hay un mejor paraje para que la gárgola abra la boca y expulse su incandescencia sobre aquellos buenos vecinos que se ganan la vida como lectores de los surcos del inconsciente?

Pero no son exactamente Quasimodo y Esmeralda `[3]`.

Preciado no es deforme, aunque ha transformado su cuerpo extraordinariamente, y tiene una elocuencia que no hace sino jorobar a los demás.

El monstruo someterá a acusaciones y señalamientos a aquellos prestidigitadores del alma, denunciados de conducir una práctica llena de violencia hetero-patriarcal, normativizante y colonial. El psicoanálisis de hoy día, dice Preciado, no puede dejar de pensar en hombre/mujer. Y cómo se les ocurre en pleno siglo XXI, nombrar unas jornadas institucionales “Mujeres en Psicoanálisis”, cuando el mundo procura cada vez más rápido correrse del binarismo de la diferencia sexual.

Preciado saluda a las brujas diciendo que no tienen entre sus filas a ningún trans. ¿Estará a la altura de su tiempo un psicoanálisis en que además de la inexistencia de miembros trans, tiene pocos practicantes abiertamente queer?

¿No hace acaso el psicoanálisis legitimar el statu quo de la psiquiatría/psicología con sus prácticas patologizantes de encajar a sus pacientes en categorías de género cerradas a cal y canto que, por cierto, vienen ya en crisis desde mediados del siglo pasado, al que ha sabido leerlo? ¿Qué diferencia existe entre los manuales de diagnóstico de psicopatología de los 50 que incluían a la homosexualidad como una enfermedad `[4]` y la teoría lacaniana todavía actual pero que se dice entre dientes que los transexuales son estructuralmente psicóticos?

Las brujas, anonadadas, se ríen nerviosas. Ríen, callan, aplauden. El cazador es cazado en su propio terreno. El analista le toca pasar al diván, se retuerce sobre el capitoneado de la tela, se trata de ajustar al encuentro de su propio discurso invertido. Las ulceras no son agradables.

Finalmente, el monstruo se llama a silencio, luego de varios intentos por detenerlo. Su intervención ha tenido el efecto provocador deseado, aunque no sabe en qué dirección. La interpretación no está para ser comprendida, si no para causar oleajes, decía Lacan. Poco más de un año después, siguen siendo transparente las ondas en el agua de aquel temporal.

Por supuesto que el monstruo fue tachado de no-iniciado. De ignorante en las artes místicas, de desconocer las escrituras de los maestros a profundidad. Miran el dedo y no la luna, ¡Y tanta afinidad que tienen las brujas con la luna! Algunas sacerdotisas no tardaron en replicar el mensaje integrándolo a lo ya escrito. Una nueva actualización, más fresca, del viejo ímpetu de Freud. O del ultimísimo Lacan, o del anterior al ultimísimo, aquel en que “la femme n’exist pas” `[5]`. Un movimiento muy barroco, como suele proceder el psicoanálisis. La ataraxia vigorosa.

Mientras tanto el monstruo se nombró monstruo. Publicó su intervención parisina titulándola “Yo soy el monstruo que os habla” `[6]`. El monstruo procede de una manera muy zizeksiana, integrando su producción al Capital. Gucci, la afamada casa de moda, elige a principios de este año a Paul B. Preciado, nacido como Beatriz, para su campaña en favor del movimiento queer `[7]`. “Servirse del padre para prescindir de él”, sería el apotegma lacaniano para la ocasión. Si es que uno puede realmente prescindir del padre.

El extravío de las hechiceras no es reciente. Como el cine, el futbol, el rock, el capitalismo y la democracia, el psicoanálisis está siempre por entrar en una crisis. La época de oro siempre es pasada, y la sensación de decadencia es permanente. El psicoanálisis está a punto de perecer, de ser llevado por la marea de las psicoterapias, los psicofármacos, los coaching ontológicos o las neurociencias. Amén de los grandes cismas en cada movimiento psicoanalítico. Si hay tres psicoanalistas juntos, al rato habrá tres escuelas psicoanalíticas distintas, se dice.

“Mejor pues que renuncie quien no pueda unir a su horizonte la subjetividad de su época” `[8]` decía un famoso psicoanalista. ¿Están para renunciar?

------------------------------------------------------------------------------------------------------------------------------

Pues no aún.

¿Qué monstruo va a ir a hablarles a aquellos seres incapaces de un acto de magia? ¿Cómo se reúnen 3500 personas a hablar de un temario tan ombliguista como “Las mujeres en psicoanálisis” o tan fundamentalista como “El inconsciente y el cerebro: nada en común” del 5to Congreso de Psicoanálisis Europeo `[9]` sino hay algo aún vivo y que tiene algo por decir?

El psicoanálisis no ha dejado de tener relevancia más allá de sus sacrosantas capitales, París y Buenos Aires. Aun batiéndose en retirada, es un discurso despierto, que circula, que logra erosionar los tejidos de las prácticas clínicas, filosóficas, literarias e incluso políticas. Decía Freud que llevaba la peste a América con aquel famoso viaje trasatlántico a EEUU `[10]`. Quizás encontraron en algunos lugares la vacuna, pero el psicoanálisis no ha sido erradicado.

El diván sigue siendo aún el lugar para lo inquietante. Preciado dice que no hay monstruos en las practicas psicoanalíticas, pero es la calle donde convergen las monstruosidades. No se trata solo del mutante, se trata también de la mutación. Y se trata de las posibilidades de las mutaciones.

Y los monstruos europeos de Madrid y París, o norteamericanos de Los Ángeles y Toronto están en una lógica distinta a la mutación que es en sí misma ser un hombre hetero-normado en Managua o una mujer cis en Antofagasta. La ampulosa presentación de Preciado contrasta con aquellas intervenciones artesanales, precarias, que puede ofrecer el analista que viaja cada dos semanas a un pueblo de las profundidades de la selva amazónica para prestar su oído y su palabra a aquel queer rural. Preciado celebra, con razón, la última legislación de identidad de género en California mientras que a orillas del rio Orinoco no hay posibilidad de dar siquiera un documento de identidad a algún niño que le gustan los niños ni ninguna oportunidad para una sofisticada escucha analítica.

El monstruo dice que la teoría psicoanalítica es esencialista (eterna) y no entiende que es una construcción histórica y situada en su época. Tiene razón, por supuesto, pero omite algo clave: el psicoanálisis es fundamentalmente una práctica. Y una práctica es en lo llevado a cabo en lo concreto, es decir, opuesto a lo abstracto puro `[11]`. Es allí sobre el terreno, allí en el pedazo de tierra que labra y no en los escritos de sus bienhechurías. Es en la espacialización del tiempo `[12]` y no en su revés, puesto que el tiempo es de los dioses, pero el espacio de los mortales, de brujas, de monstruos, de bujarrones, ninfos o filenos `[13]`

Algunas las brujas también han olvidado esto. Se aseñoraron en sus grandes congresos y cómodos despachos, se olvidaron de su arte rupestre, de sus conjuros y trucos más elementales, de trabajar como herejes y no como curas. Se concentraron más en el libro de hechizo que en el hechizo mismo.

El psicoanálisis podrá pensarse progresista, podrá hacer una discriminación positiva de sus miembros para tener mayores representantes de minorías en sus practicantes, podrá sustituir sus conceptos heteropatriarcales por algunos otros nombres más acorde a lo actual. Podrá sacar el diván a la plaza. Podrá transformarse completamente, como se transforma la época. Podrá ser un transpsicoanalisis. Podrá ser un monstruo.

Pero no deja de ser brujería `[14]`. Y ahí, en sus actos de magia, en sus rituales, en sus más extraordinarios sortilegios es que podrá darle cabida a los Paul B. Preciado, con su notable camino por construirse un nombre y un cuerpo `[15]`, pero también a aquellos que no han contado con la suerte de tener a 3000 brujas escuchando su más íntima monstruosidad.

\*Escrito presentado como trabajo final al curso “Psicoanálisis y género: un dialogo fructífero” del Centro de Salud Mental Dr. Ameghino, coordinado por Pablo Tajman.



**Notas**

**`1.`** Se puede escuchar la intervención de Preciado en el francés original [aquí ](<https://www.youtube.com/watch?v=vqNJbZR_QZ4&t=154s>)o escuchar una transcripción traducida al castellano [aquí.](<http://revoluciondelirante.blogspot.com/2020/08/conferencia-paul-b-preciado-49jornadas.html>)

`2.` Aunque evidentemente existen muchos psicoanalistas practicantes que se reconocen como hombre, se utiliza el femenino como inclusivo en esta ocasión.

`3.` De los personajes de Victor Hugo en su novela “Nuestra Señora de París”.

`4.` Para la primera edición del manual de psiquiatría norteamericano DSM, la homosexualidad sería considerada una patología. Recién sale del manual de clasificación en los años 70.

`5.` En castellano, “La mujer no existe”, célebre frase en el mundo psicoanalítico, largamente interpretada, pronunciada en Italia en 1972

`6.` “Yo soy el monstruo que os habla. Informe para una academia de psicoanalistas” (2020). Editorial Anagrama. El texto tiene algunas modificaciones con el original, referido en la primera nota

`7.` El cineasta Gus Van Sant retrata en este [video](<https://www.gucci.com/es/es/st/stories/article/guccifest-episode-1>) a Preciado y parte de la campaña de Gucci.

`8.` Planteada en el “Discurso de Roma”, de Jacques Lacan de 1953.

`9.` Congreso llevado a cabo en Bruselas en julio de 2019 y organizado por la Asociación Mundial de Psicoanálisis.

`10.` Se tratan de las conferencias dictadas por Freud en la Universidad de Clark, Massachusetts en 1909. Reunidas en el volumen 11 de las obras completas de Freud de Amorrortu Editores.

`11. `Para diferenciarlo del abstracto social, trabajado por Omar Acha en sus ensayos de psicoanálisis y marxismo (2018), que requeriría de unas precisiones ulteriores.

`12.` Para un extenso trabajo sobre el tema, véase “La técnica y el tiempo” (1994), de Bernard Stiegler y una brillante lectura de la técnica, la filosofía y lo humano.

`13.` Sobrescribiendo aquella clasificación de poetas como queers de distintas castas en “Los detectives salvajes” de Roberto Bolaño.

`14.` Ver al respecto, en un ámbito no estrictamente psicoanalítico, el interesante libro de Mark Fisher y Matt Lee “Deleuze y la brujería” (2009), de editorial Las Cuarenta.

`15.` Preciado relata parte de su experiencia en varios escritos, pero planteada desde distintos momentos está reunida en “Un apartamento en Urano” (2019), de editorial Anagrama y con prólogo de Virginie Despentes.