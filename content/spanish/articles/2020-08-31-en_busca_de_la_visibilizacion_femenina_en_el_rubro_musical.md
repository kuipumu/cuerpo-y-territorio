---
title: En busca de la visibilización femenina en el rubro musical
cover: /images/uploads/jenni-jones-jwgwmovwfzi-unsplash.jpg
date: 2020-08-31T11:37:07.041Z
authors:
  - Karol Dinamarca
tags:
  - Musica
  - Cultura
  - Inclusion
  - Feminismo
categories:
  - Género
comments: true  
sticky: true
---
<!--StartFragment-->

Sin duda, en Chile la música se vive y se respira. A lo largo de su historia, este país se ha convertido en un productor de grandes músic@s, compositor@s e intérpretes, y dentro de este contexto, no pasa desapercibida la nula participación femenina a la postulación al Premio Nacional de Música 2020.

El país está repleto de mujeres que tuvieron una larga y exitosa trayectoria musical, así como colegas totalmente vigentes dedicándose por completo y desde distintas plataformas al rubro. Sin embargo, hace ya varias semanas, un medio de comunicación conocido en el país, anunció que habían al menos seis músicos postulados a recibir el premio y, entre ellos, no había ni una mujer postulada al galardón.

A pesar de que esta noticia generó un notable malestar en el mundo de la música en general, a la vez también esta situación dio paso a una afortunada acción, ya que un conjunto de personas dieron el puntapié inicial para trabajar en la visibilización de múltiples nombres de mujeres ligadas a las artes musicales y, de este modo, comenzaron un trabajo de recopilación e investigación profundo, obteniendo como resultado el resurgimiento de notables artistas nacionales, pero muy olvidadas en el tiempo.

Aquellas colegas han dado vida a la Coordinadora por la Visibilización de la Mujer en el Arte en Chile COVIMA y ahora, gracias a este grupo humano, nombres como Nora López Von Vriessen, soprano; Mireya Alegría, directora de orquesta y pedagoga; Isabel Fuentes, folclorista y un largo etcétera de mujeres han renacido, tal cual ave fénix, para dar paso a sus merecidas postulaciones al Premio Nacional de Música 2020.

Otra gran labor con mucho tiempo de existencia es la realizada por el colectivo de mujeres llamado "Resonancia Femenina", que se dedica a la creación de nuevos espacios para la difusión de música hecha por mujeres. Este concepto se ha transformado en su consigna, ya que buscan visibilizar el trabajo realizado en la composición, interpretación, docencia, etc.

Gracias a esta labor han realizado diferentes proyectos de mucha importancia, tales como la instauración del primer encuentro internacional de mujeres en la música el año 2017 y la creación del Premio Añañuca, reconocimiento a la trayectoria de una mujer en la música chilena.

Un gran aporte ha sido también el de Ninoska Medel, una joven directora orquestal que dirige la Orquesta de Mujeres de Chile OMCH. Ellas realizan una activa labor de charlas y ciclos de clases magistrales, actividades que se han intensificado en este período de pandemia.

Durante estas jornadas, grandes compositoras y directoras son invitadas a participar para otorgar un momento de retroalimentación necesaria en la formación de jóvenes music@s. Además, han realizado encuentro de orquestas femeninas, conciertos y actividades musicales variadas, siempre con el fiel compromiso de crear espacios seguros para que distintas mujeres puedan mostrar sus obras.

Así podría seguir mencionando un sin fin de conjuntos y ensambles que han nacido de la necesidad de dar a conocer sus producciones musicales.

Han sido muchos los esfuerzos realizados por distintas mujeres para crear conciencia de las pocas oportunidades de difusión musical formal. Podríamos quedarnos con un ligero sabor amargo en la boca debido a esta situación de invisibilización del trabajo de la mujer en la música pero, gracias a la ardua labor de estos colectivos, han surgido grandes proyectos como los anteriormente mencionados, lo cual me da una pequeña luz de esperanza; primero como mujer y luego como intérprete.

Al parecer, la autogestión es la única forma viable de posicionarnos dignamente en el rubro musical y, ante esto, no puedo terminar de escribir esta columna sin agradecerles públicamente por tan valiente y concreto trabajo, el cual nos motiva a seguir invirtiendo nuestras energías en la eterna lucha de vivir de lo que amamos: la música.

<!--EndFragment-->