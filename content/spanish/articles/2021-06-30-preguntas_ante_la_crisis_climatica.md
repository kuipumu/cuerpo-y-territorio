---
title: "Preguntas ante la crisis climática "
subtitle: "Una apróximación desde el activismo ambiental "
cover: /images/uploads/cumulus-499176_960_720.jpg
caption: ""
date: 2021-06-30T20:37:44.343Z
authors:
  - Oscar Gavidia
tags:
  - Ambiente
  - CrisisClimática
categories:
  - Territorio
comments: true
sticky: true
---
La crisis climática, es dinámica, compleja y arrolladora. En mi opinión, es el resultado de la explotación y expoliación de nuestra biodiversidad. Se trata de violencia extrema, alevosa y premeditada hacia nuestro planeta “Dagas corporativas que se hunde en el corazón de la Pachamama”

Una de las causas principales de la alteración climática es el Extravimos puro y rancio, que responde a centros y élites de poder: Transnacionales, Trust, monopolios, oligopolios; que están por encima de los gobiernos… con el único fin de obtener el dominio total y absoluto de los recursos renovables y no renovables; que sustentan un estilo de vida de privilegiados y elegidos que nos están llevando a la extinción de las especies y en definitiva nuestra propia extinción.

La crisis climática, es la crisis del medio ambiente, es la crisis del modelo social que nos engendra y reproduce “La Civilización” Las consecuencias inmediatas del extractivismo -que va en contra de las fuentes primarias y sustento de la vida- las podemos observar a diario: deforestación, tala, quema, extracción de minerales, explotación petrolera, ciudades, latifundios, guerras, Privatización… provocando la desertificación, contaminación de las aguas, aire, suelos y subsuelos; que concluyen en la pérdida de la delgada capa de la biomasa y consecuente liberación del CO2 y otros gases que bloquean la salida de la luz solar, afiebrando el planeta y aumentando el calentamiento global.

Las grandes e institucionales políticas científico-ambientalistas se conocen desde hace muchos años: energías solar, aprovechar el movimiento de las olas del mar, energía Eólica, biocombustible…son más solidarias con el ambiente; sin embargo, estas fuentes inagotables de recursos que se puede ampliar y socializar en teorías y prácticas... no son un buen negocio, porque quiebran el orden que detenta el poder.

Frente a esta realidad e “inminente” catástrofe, surgen cuestiones importantes que necesitamos problematizar como por ejemplo; ¿qué podemos hacer los ciudadanos de a pie, el pueblo? ¿Los que vivimos en las ciudades qué podemos aportar? ¿Cómo construimos consciencia de esta crisis? ¿Cómo trascendemos el consumismo? ¿Eco NO mía vs eco SI nuestra? ¿Qué hacemos con las semillas modificadas genéticamente y los pesticidas? ¿Cómo nos organizamos hasta que las semillas broten del asfalto?

EPATU KONUKO, Significa Espirales para las Artes y tradiciones Universales del Konuko. Espacio para el encuentro, juntadera, socialización de saberes y quehaceres que promuevan el buen vivir. Tomando como referentes el conuco ancestral y milenario de nuestros originarios como eje central en el conjunto de nuestras relaciones.

Una de la dinámicas más importante dentro de la EPATU, y que nos permite asumir desde las bases; son los Espirales Kreativos Urbano Familiar EKUF. Porque toma como bandera a la familia, ¡núcleo endógeno primario! como fuente para activar una consciencia y consecuente transformación del estilo de vida.

En cada EKUF, se gestan actividades cotidianas que apuntan hacia la autogestión. En un 1er momento, se diagnostica el potencial y posibilidades de cada espacio. Para muestra un botón, en el caso que expongo permite por ejemplo: siembra y cosecha de plantas aromáticas y medicinales (Albahaca, poleo, llantén, cúrcuma, orégano, malojillo, flores, suculentas, Kalanchoe… ají picante, lechosas, auyamas), producción de abono a través de la recolección de desechos orgánicos, guardianes de semillas.

Igualmente la labor artesanal es permanente: deshidratación y pulverización de las hojas para condimentos, infusiones. Producción de brotes y germinados de frijolitos. Elaboración de dulces, vinos, chichas, fermentados salados, pan, salsas, galletas entre otros... que denominamos gastronomía de la abundancia. El EKUF, también es un espacio para el aprendizaje y desarrollo de expresiones artesanales como el tejido, macramé, talla de madera, jabón, modelado de arcilla, dibujo, pintura, fotografía, música, poesía. La forma de relacionarnos, las hacemos patente a través del trueque, mano vuelta, Cayapa, cooperativismo, todo este proceso ECO SI NUESTRA, se resume en el necesario encuentro solidario para aprendernos y compartirnos en convivencia.

Este espacio familiar, es una fuente de alimento para los polinizadores como lo son: colibríes, meliponas, abejas, pegones, mariposas. También es habitación de pájaros, palomitas y lagartijas. Cuando hacemos abonos, te puedes encontrar a los insectos como lombrices, cucaracha de tierra, ciempiés, grillos y micro organismos… que van biodegradando los desechos orgánicos y generan biomasa, abono rico para nuestras plantas.

Fíjate, todo lo que se puede lograr en un Espiral, si sumamos cientos de espirales, cientos o millones de conucos… estaríamos hablando de cientos y millones de familias (núcleos endógenos) vinculandose, relacionándose y articulandose. Cada espacio sumando en colectivo, bajo una praxis y estilo de vida por lo pronto, más cercana y solidaria a Pachamama. Cada espacio se convierte en INSISTENCIA, suma colectiva, que en mi opinión, contrarrestan la crisis ambiental. Poder constituyente preparado en lógicas anti depredadoras del ambiente.



*Somos la gente*

*y soñamos juntos*

*el nuevo amanecer* 

*donde un cielo de besos*

*le adorna el corazón a los niños y*

*niñas*

*somos la gente*

*que despierta con su música*

*un horizonte al pueblo*

*que busca sus raíces*

*somos la gente*

*mujeres, hombres, niños y niñas*

*con calor en el pecho*

*y una palabra de aliento*

*que disipan las sombras*

*Somos la gente*

*diversidad en sueños y posibilidades*

*manos con sabor a papelón*

*que siembran la tierra*

*y gritan la verdad*

*en nombre del AMOR*