---
title: "El desprecio hacia la docencia: declaraciones de una ministra"
subtitle: " "
cover: /images/uploads/soledad_acuna_horizontal-768x460.jpg
caption: Ministra de Educación de la Ciudad de Buenos Aires
date: 2020-11-19T02:21:41.940Z
authors:
  - Juan Manuel Barreto
tags:
  - MinistraDeEducación
  - Educación
  - Cultura
  - Declaraciones
  - Argentina
  - Latinoamerica
  - SoledadAcuña
  - Docencia
categories:
  - Educación
comments: true
sticky: true
---
<!--StartFragment-->

**Según la ministra de Educación de la Ciudad de Buenos Aires, Soledad Acuña, soy un fracasado.** No me lo dice de forma directa ni mirándome a los ojos. Tampoco me lo dice a mí, exclusivamente. Se refiere a lxs docentes. A aquellxs que eligen la docencia como tercera o cuarta carrera, aún más. Lo hace frente a una cámara, en una charla descontracturada con el diputado nacional Fernando Iglesias. El desprecio hacia lxs docentes, hay que reconocerlo, no lo esconde. Utiliza todas las estigmatizaciones que conoce para que no queden dudas. Prescinde del marketing (esta vez) para relatar sus verdades “ideologizadas”.

De acuerdo a las declaraciones de Acuña, quien no accede a la docencia en primer término, es decir, a partir de una primera elección de carrera, es una clara muestra de alguien que fracasó en sus opciones profesionales anteriores y busca, por lo tanto, caer (con la ayuda de un paracaídas) en el oficio docente como mal menor, como refugio que torne menos visibles sus falencias, sus incapacidades, su mediocridad.

No dice solo eso la ministra. Desea ser tajante al respecto, entonces agrega: ***"la docencia es un trabajo realizado por personas que provienen de clases socioeconómicas bajas que no acreditan ni el capital cultural ni experiencias enriquecedoras para ofrecer en el aula"***. Detengámonos un momento aquí y preguntémonos: de ser fidedigno el dato que da cuenta que la mayor parte de lxs docentes provienen de extractos sociales bajos, ¿por qué razón la movilidad social ascendente sería un peligro o problema?, ¿para quiénes? A su vez, **¿en qué se basa para afirmar que el nivel socioeconómico determina la calidad de las prácticas didácticas y pedagógicas que unx docente puede construir dentro del aula?** ¿Qué pedagogxs y sociólogxs de cabecera se apilan en la biblioteca de Acuña?

La ministra pronuncia la palabra fracaso con asco, expulsándola de la boca como quien sabe que rechaza algo que no le pertenece, que está destinado a otrxs. A lxs plebeyxs, a lxs marginales quizás. A lxs que no poseen contactos en el poder que les faciliten el arribo a áreas de gobierno para las cuales no reúnen formación, trayectoria y especialización alguna. Automáticamente, la imagen de mis estudiantes irrumpe. Parecieran no valer la pena sus esfuerzos, sus intentos, sus errores, sus repitencias, sus idas y vueltas. Sus fracasos. La ministra de Educación les habla también a ellxs.

**Recurro a indagar a qué mitos o construcciones del imaginario colectivo responden este tipo de discursos.** Porque, si hay algo que flota con claridad en este asunto, es que el lenguaje utilizado por la ministra teje una alianza con ciertas ideas, creencias o sensaciones imperantes en la sociedad argentina, siendo constitutivo de un entramado que opera con fuerza de sentido común.

Para ello, apelo a una mínima investigación artesanal buceando en redes sociales, diarios y televisión nacionales. Tomo nota de los posicionamientos frente al tema en programas informativos, artículos de opinión, intercambios vía Facebook y Twitter.

Durante la búsqueda encuentro, por ejemplo, una máxima que se replica en algunos de esos espacios y que refuerza las declaraciones de Acuña: quienes eligen la docencia lo harían, fundamentalmente, por la facilidad para acceder a un empleo seguro. 

Profundizo un tanto más y advierto que muchxs de quienes esgrimen este argumento desconocen las condiciones en las que se lleva adelante la tarea docente en la jurisdicción de la ministra Acuña. No solo existe, paso a contarles, una gran cantidad de barreras al procurar titularizar horas cátedra (odisea que suele durar años y años) sino que lxs docentes, en su infinita mayoría, deben trabajar en diferentes escuelas/institutos/universidades largas jornadas diarias para lograr un sueldo que a fin de mes le venza la pulseada a la canasta básica de alimentos y servicios. 

Porque, para sorpresa de lxs que compran estos buzones, la precarización laboral es moneda corriente en este ámbito y, vaya extrañeza, se promueve desde las políticas públicas. Pero eso no es todo: desde el año 2008, la formación docente en CABA requiere cursar y aprobar una carrera de cuatro años de duración como mínimo; no se trata como verán de un trámite rápido.

Encuentro, también, variados comentarios que remiten a la politización de la educación, a la militancia política en las aulas. Acuña echa luz acerca de esto y nos indica que allí se halla la raíz del problema. En los institutos de formación docente. **Según su mirada,** ***“un docente que aprende bien, sabe que lo que tiene que hacer es enseñar a pensar, no decirles a los chicos que pensar”***. 

Esta frase me resulta llamativa en distintos niveles: por un lado, dado que basta acercarse a las escuelas para verificar la falsedad de estas afirmaciones. En segundo lugar, denota un notorio desconocimiento de las dinámicas que se generan dentro de las aulas. Aunque todxs lxs docentes de la ciudad de Buenos Aires realmente quisieran adoctrinar a sus estudiantes en una determinada ideología o fraternidad partidaria, sería un cometido imposible de alcanzar; **lxs chicxs no repiten como un mantra lo que escuchan de sus profes, todo lo contrario, me animaría a asegurar: cuestionan, dudan, repreguntan, se muestran escépticxs.** 

En un tercer sentido, la frase ignora deliberadamente que la tarea docente se enmarca dentro de un diseño curricular específico; no hay una decisión caprichosa respecto de los contenidos que se desarrollan a lo largo del calendario escolar. Asimismo, la mención a los institutos de formación docente no es inocente, más bien transparenta el férreo objetivo de reemplazarlos por una Universidad Docente de la Ciudad de Buenos Aires (UNICABA), a espaldas de las necesidades y demandas de las comunidades educativas.

En una dimensión de análisis adicional, y en sentido inverso a lo que sostiene Acuña, es necesario continuar trabajando para que la educación sea un acto político, de transformación personal y colectiva, que provoque y estimule de forma crítica la conciencia de lxs alumnxs, y que lxs acompañe en el diseño de sus proyectos de vida.

Sigamos ahora con las declaraciones de la ministra y el pasaje donde manifiesta su preocupación en relación a la edad de lxs docentes. A partir de (dudosas) estadísticas que maneja Acuña, afirma que lxs docentes son personas cada vez más grandes. Eso también le molesta a la ministra. Lo que oculta su enojo, sin embargo, es que tanto en el nivel inicial, primario y medio de la Ciudad de Buenos Aires el rol docente es ejercido, principalmente, por mujeres, las cuales probablemente trabajan y se hacen cargo de las tareas de cuidado en su familia y hogar en paralelo a cursar sus estudios. Y menciono esto sin dejar de preguntarme el motivo por el cual la edad puede significar un condicionante negativo al momento pensar el oficio de lxs que enseñan.

**Hay otro pasaje de la charla que reviste de una gravedad institucional y humana que aterra: la ministra insta a las familias a controlar y denunciar a lxs docentes.** La mayor autoridad educativa de la Ciudad de Buenos Aires alienta una persecución. Para ella, las familias tienen que mirar lo que sucede dentro del aula a modo de fiscalización para luego denunciar lo que consideran incorrecto. **La mayor autoridad educativa de la Ciudad de Buenos Aires no tiende un puente entre familias y escuelas.** No está allí para generar consensos. Propone un enfrentamiento en el que lo que menos interesa es la calidad de la educación y el presente y futuro de lxs estudiantes.

Soledad Acuña nos habla de fracaso, de sobreedad, de denuncia. No nos explica, eso no, por qué elige ser la ministra de una comunidad que no valora, que menosprecia. Tampoco hace referencia a lxs ministrxs de educación que no han transitado una trayectoria pedagógica, que no han pisado jamás un aula como docentes.

¿Se imaginan a unx ministrx de Economía que no haya estudiado economía o a unx ministrx de Salud que no se haya especializado en medicina o en políticas sanitarias? ¿Por qué cuesta tanto imaginar a los ministerios de educación conducidos por personas que demuestren experiencia y conocimientos en pedagogía, en políticas públicas educativas, en escuelas, institutos, universidades, sin importar su edad, si han elegido la educación como primera o cuarta carrera, si pertenecen a clases socioeconómicas bajas, medias o altas?

**Las declaraciones de la ministra no representan, de todos modos, un hecho aislado.** Sus palabras, su lenguaje y su posicionamiento, se nutren y potencian distintos discursos que circulan en la sociedad como verdades absolutas cuando no son más que construcciones sociales. **Las declaraciones de la ministra son la expresión de una violencia clasista y racista en consonancia con las políticas públicas educativas llevadas a cabo desde su gestión ministerial.**