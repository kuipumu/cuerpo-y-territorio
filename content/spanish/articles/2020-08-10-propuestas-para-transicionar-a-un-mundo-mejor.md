---
title: 'Propuestas para transicionar a un mundo mejor'
subtitle: 'Algunos aportes desde la Salud Comunitaria y el Ecofeminismo'
description: 'Cuerpo y territorio. Dos palabras con muchos significados y fuerza, que juntas se potencian aún más. El primer territorio que habitamos cuando llegamos al mundo es nuestro cuerpo.'
cover: 'images/uploads/propuestas-para-transicionar-a-un-mundo-mejor.jpg'
caption: 'Captura del documental “Sembradoras de Vida”'
authors:
  - Paula Fausti
categories:
  - Diversidad
tags:
  - Diversidad
  - Ecofeminismo
  - Genero
  - Medio Ambiente
date: 2020-08-10 00:00:00
comments: true
sticky: true
---
>“Sanarnos es un acto personal y político. Y aporta a tejer
la red de la vida. Cuerpos sanados son cuerpos que se
emancipan. Sanando tú, sano yo y sanando yo, sanas tú.” 
>
>Lorena Cabnal – Feminista Comunitaria de Guatemala.

Cuerpo y territorio. Dos palabras con muchos significados y fuerza, que juntas se potencian aún más. El primer territorio que habitamos cuando llegamos al mundo es nuestro cuerpo. Sin embargo, resuena de pronto la frase “ni la tierra, ni las mujeres somos territorio de conquista” y emerge el grito de denuncia: históricamente la tierra y las mujeres han sido territorios en disputa.

En nuestros territorios-cuerpo habitan los efectos de la opresión y la violencia del sistema capitalista-patriarcal, pero también radica la energía y la fuerza para resistir. Afortunadamente en la actualidad existen varias propuestas que a modo de bálsamo vienen a ofrecernos sanación y empoderamiento.

Para sanar nuestrxs cuerpos y cuerpas primero necesitamos escucharnos. Escuchar nuestros procesos vitales, psíquicos, espirituales, físicos. Y acuerparnos, porque la salida es colectiva. Este escrito invita a pensar juntxs qué tipo(s) de mundo(s) queremos co-habitar, esbozando algunas propuestas para transicionar mejor.

## ¿Por qué transicionar?

El sistema de producción alimentaria, los desmontes, y la deforestación, el agronegocio, las megagranjas industriales, el saqueamiento de los mares, los agrotóxicos, la megaminería, el fracking y todas las prácticas extractivistas, responden a un modelo económico desarrollista que plantea un crecimiento infinito basado en recursos naturales finitos.

Hemos sobrepasado por demás la capacidad regenerativa de los ecosistemas. No es mi intención detenerme en esto, porque  ya existen numerosas investigaciones científicas que lo hacen. La evidencia habla por sí misma: estamos ante una crisis civilizatoria y nos encaminamos al colapso del sistema socioeconómico capitalista. Y si cabe alguna duda recordemos que estamos en medio de una pandemia.

Ante este panorama es lógico sentir miedo, angustia, ansiedad, enojo y, tristeza. No patologicemos nuestras emociones, al contrario, nos sentimos así porque estamos vivxs y tenemos sensibilidad. Sin embargo, no son tiempos de guardarse ni de esconderse en las pantallas o la televisión. Hay que actuar y rápido. Necesitamos juntarnos y tomar decisiones que aseguren el sostenimiento de la vida, de nuestro futuro y el de próximas generaciones, anticipándonos a lo que se viene para que no nos tome desprevenidxs.

## Aportes desde el ecofeminismo

La propuesta desde el ecofeminismo es la de transicionar hacia sociedades más igualitarias y sostenibles, ya que estamos ante una crisis socioecológica y una crisis de cuidados. 

- La primera implica visibilizar que somos ecodependientes, es decir no podemos vivir sin la naturaleza. Sin embargo, hemos causado el agotamiento de los bienes comunes, el cambio climático antropogénico y la pérdida extrema de biodiversidad.

Todo esto de la mano de una creciente desigualdad social, basada en la acumulación de las riquezas en un pequeño sector de la población, en detrimento de otros cada vez más empobrecidos, que además terminan sufriendo directamente los daños ambientales, al vivir en las llamadas “zonas de sacrificio”.

- La segunda plantea que, al igual que el planeta tierra, el trabajo humano tiene límites. El cuidado de los cuerpos es fundamental para nuestra supervivencia. Desde que nacemos somos radicalmente interdependientes (Herrero, 2012). 
Las tareas de reproducción social y del mantenimiento de la vida cotidiana en el ámbito privado, históricamente fueron asignadas a las mujeres. Sin embargo, el capitalismo-patriarcal se encargó de naturalizarlo y no reconocerlo. 

Si bien las mujeres lograron insertarse dentro del mercado laboral, los cuidados no fueron socializados ni distribuidos. Resuena ahora la frase “eso que llaman amor es trabajo no pago.” La crisis de cuidados visibiliza esta problemática. La doble jornada laboral, no remunerada, recae sobre las mujeres, pero también sobre las abuelas y sobre otras mujeres pertenecientes a sectores marginados (migrantes, racializadas, de sectores populares, etc.).

El ecofeminismo y la economía feminista destacan el paralelismo entre la explotación de la mujer y la de la naturaleza (Svampa, 2015).
La propuesta entonces es repensar los trabajos de cuidado como un conjunto de saberes extraordinariamente valiosos, aún más en el contexto actual. 

Hoy más que nunca necesitamos revalorizar estos conocimientos, visibilizarlos y socializarlos, para configurar entre todxs una salud comunitaria autogestiva y autoorganizada.

## Aportes desde la Salud Comunitaria

La salud comunitaria prioriza la protección y promoción de la salud, poniendo en el centro a la comunidad. Dentro de ella, la psicología se presenta como una herramienta clave, ya que, entre otras cuestiones, puede acompañar la producción de nuevas subjetividades y contribuir al fortalecimiento de las comunidades.

La psicóloga comunitaria venezolana Maritza Montero define al “fortalecimiento comunitario” como el proceso mediante el cual lxs miembros de una comunidad desarrollan conjuntamente capacidades y recursos, actuando en forma comprometida, consciente y crítica, para lograr la transformación de su entorno según sus necesidades y aspiraciones, transformándose al mismo tiempo a sí mismxs (Montero, 2003),. eEsto implica pensar a la comunidad como un colectivo de sujetos políticos con capacidad de leer críticamente su realidad y transformarla.

Además, la psicología comunitaria puede acompañar la producción de nuevas subjetividades, esto es relevante porque la transición implica también un “proceso interno” (J. A. Etxagibel, T. Sloan, P. Belloy, A. Loyola, 2012). Incorporar la perspectiva psicológica es reconocer que el orden capitalista-patriarcal produce subjetividades funcionales al sistema, ya que el capitalismo no sólo fabrica productos, también fabrica consumidores. 

El modelo económico se basa en la premisa (falsa) de que las personas tenemos necesidades infinitas que debemos satisfacer. A esta altura ya sabemos que no nos hace felices consumir objetos, sino que consumimos el valor simbólico que representan. Esto el capitalismo lo ha explotado muy bien de la mano del marketing.

- La psicología comunitaria-ambiental entonces puede ayudarnos a transicionar mejor, contribuyendo a la construcción de otras formas de organización social, prácticas y valores (Mozobancyk, 2011).

## La vuelta a nuestros territorios-cuerpo-tierra es resistencia y emancipación.

Quizás hablar de transición socioecológica a algunxs todavía les suene raro, inviable o utópico pero la realidad es que varias de estas propuestas no son nuevas, ni se crearon desde la nada. Es más, la mayoría ya existía hace tiempo y se trata de viejos-nuevos saberes de los pueblos originarios, los movimientos de lxs campesinxs y obrerxs, los colectivos afroamericanos, los movimientos feministas y ambientalistas, lxs jóvenes, las organizaciones sociales, los colectivos culturales, lxs intelectuales y académicxs, las ciencias, el zapatismo o, las mujeres kurdas. 

Los “saberes expertos” y los saberes locales, populares, ancestrales se han unido y cooperan mutuamente, produciendo nuevas narrativas y subjetividades, proponiendo prácticas y soluciones concretas, porque hoy el llamado es el de re-habitar nuestros territorios-cuerpo-tierra.

### Bibliografía:

- Montero, M. (2003), “Teoría y práctica de la psicología comunitaria. La tensión entre comunidad y sociedad”, Paidós, Buenos Aires.
- Joseba Azkarraga Etxagibel, Tod Sloan, Patricio Belloy y Aitzol Loyola, (2012) «Eco-localismos y resiliencia comunitaria frente a la crisis civilizatoria », Polis [En línea], 33 | Editor: Centro de Investigación Sociedad y Politicas Públicas (CISPO), http://polis.revues.org/8400
- Schelica Mozobancyk (2011), “Problemas ambientales y psicología ambiental. Reflexiones para la construcción de una psicología de la sustentabilidad en Argentina”. PSIENCIA. Revista Latinoamericana de Ciencia Psicológica, Volumen 3, N°2 | Editor: Ezequiel Benito - Universidad Favaloro (Argentina) www.psiencia.org
- Yayo Herrero (2012), “Propuestas ecofeministas para un sistema cargado de de deudas”, Revista de Economía Crítica, nº13, primer semestre 2011, ISNN 2013-5254
- Maristella Svampa (2015), “Feminismos del Sur y ecofeminismo”, texto publicado en la revista Nueva Sociedad No 256, marzo-abril de 2015, ISSN: 0251-3552, <www.nuso.org>. Forma parte del capítulo final del libro “Maldesarrollo. La Argentina del extractivismo y el despojo”, publicado en coautoría con Enrique Viale (Katz, Buenos Aires, 2014).
