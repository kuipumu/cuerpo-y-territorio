---
title: "Mapa latinoamericano de feminicidios: Una herramienta de articulación
  feminista "
subtitle: "Visibilizando las violencias hacía las mujeres en América Latina "
cover: /images/uploads/mlf-portada_final-1024x341.png
date: 2020-11-19T01:50:13.466Z
authors:
  - Niyireé S Baptista S
tags:
  - Feminicios
  - Latinoamerica
  - DDHH
  - Mujeres
  - Feminismos
  - Género
  - Derechos
  - Violencia
  - Homicidios
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

*`“Nos sabemos diversas, pero nos hermana la lucha. Y es en este punto en el que invitamos a encontrarnos”`*

> *Mapa Latinoamericano de Feminicidios.*

El femicidio es una realidad en América y el mundo, y consiste en una violación directa a los derechos humanos de las mujeres. Las cifras actuales de femicidios, según el informe del Mapa Latinoamericano de Feminicidios (MLF), en abril de 2020 ascendía a 1352 `(1)`. **Las violencias cometidas hacia mujeres y niñas se centran en sus cuerpos, como territorios de dominio.** Violencia sexual, desapariciones y femicidios son tres de las formas comúnmente usadas para violentar los cuerpos de mujeres. Lamentablemente, la mayoría de estos actos de violencia quedan impunes, pues, dentro del orden patriarcal, la sociedad protege a los victimarios con leyes obsoletas, sistemas de justica ineficaces, discursos inmersos en una cultura que naturaliza los hechos de violencia, entre otros elementos, que siguen sosteniendo el sistema patriarcal y sus injusticias.

El femicidio, considerado como la máxima expresión de violencia, parece esconderse tras un velo de ausencia de datos y responsabilidades por parte de los diferentes Estados, sin importar si está tipificado en ley o no. **El asesinato de mujeres por misoginia es probablemente una de las principales causas de muerte de mujeres y niñas en el continente.** 

Ante esta realidad las organizaciones, tanto de mujeres como de feministas de América Latina (AL), tras años de luchas, han logrado tejer mecanismos de resistencia y visibilización de la violencia a la que somos sujetas las mujeres. La historia de nuestros movimientos, organizaciones y colectivos, se cuenta, entonces, desde la exigencia de una vida libre de violencias, que se nos considere ciudadanas en igualdad de derechos y que dejen de matarnos.

Es así como nace el Mapa Latinoamericano de Feminicidios, una plataforma de mapeo a nivel de AL y el Caribe de los casos de femicidios, creado por la organización regional MundoSur `(2)`; en este mapeo se muestran los datos cuantitativos a nivel nacional y subnacionales de las mujeres asesinadas por la violencia machista. El mapeo realizado es nutrido por la Red Latinoamericana contra la Violencia de Género que está conformada por 25 organizaciones civiles de diferentes países de AL (Argentina, Bolivia, Chile, Ecuador, El Salvador, Guatemala, Nicaragua, México, Panamá, Puerto Rico y Venezuela), MundoSur y dos organizaciones regionales. 

Desde que fue pensado, la intención del mapeo ha sido sacar a la luz los datos sobre femicidios en el continente y a partir de esa premisa han logrado grandes articulaciones regionales que les permiten seguir recopilando datos y extender los pilares del proyecto. El MLF “*es una herramienta en constante construcción y perfeccionamiento, que se sigue nutriendo de los aportes de nuevas organizaciones territoriales que se suman al esfuerzo de la Red Latinoamericana contra la Violencia de Género”.*

#### Cómo funciona el mapeo

El MLF tiene tres pilares que lo conforman: 

1. La visualización de datos que registra todos los casos de femicidios que han surgido de portales estatales desde 2019, hasta lo que va del confinamiento en 13 países. 
2. El contraste de la información proveniente de los Estados con la que producen las organizaciones territoriales. La información aportada por las organizaciones se actualiza semanalmente y la manera en que cada organización recoge los datos a nivel territorial varía; sin embargo, la mayoría lo hace por registros de prensa. El apoyo de las organizaciones permite saber cuáles están activas, llevar un registro de su accionar, informar a las autoridades concernientes en caso de cualquier eventualidad (como cese de reportes u otros) y alertar de posibles irregularidades en sistemas informáticos de las instituciones gubernamentales. 
3. Finalmente, el tercer pilar es la Mesa de Diálogo Interamericano para la Construcción de Políticas Públicas de Género, un espacio centrado en el diálogo para especialistas, funcionarias/os, relacionadas/os con temas de género; esta mesa, además, es un espacio de co-creación de políticas públicas y diálogo entre los gobiernos y las organizaciones de la sociedad civil que conforman la Red.

![](/images/uploads/mapa_latinoamericano_de_femicidios.png)

#### Logros, articulaciones y avances

**El MLF es la única plataforma de libre acceso que funciona actualmente para visibilizar y proporcionar datos de femicidios en AL y el Caribe.** Este hecho les ha permitido generar un impacto real al establecer las alarmas sobre el aumento de los casos. 

Los datos estadísticos son sumamente necesarios para darle fuerza a la realidad y así buscar que los diferentes gobiernos puedan interesarse en la formulación de leyes, planes y políticas públicas en favor de las mujeres. *“El MLF nos permite producir información confiable, actualizada, accesible y con perspectiva de género que es indispensable para pensar más y mejores políticas públicas para hacerle frente a las violencias de género”*. En este sentido, el trabajo del MLF ha comenzado a cobrar fuerza; varios representantes de diversos Estados han pedido la ayuda de MundoSur para articular trabajos de sensibilización y formación en el área de mujeres y género para funcionarias y funcionarios. 

Por otro lado, el mapeo ha tenido un impacto importante en los medios de comunicación y cada día son más los medios que se interesan por el trabajo del MLF. *“En este punto quiero destacar el rol fundamental que desempeña el periodismo para que las violencias de género se conviertan en un tema de la agencia social en la región”.* Es así como destacan la importancia de hacer un registro desde los medios y canales informativos sobre los femicidios en vista de la ausencia de información proveniente de la policía, de la justicia o del Estado en general.

#### **Limitaciones en el camino**

Una de las limitantes a la hora de construir el mapeo es el hecho de que cada país tiene un marco legal referente al femicidio, por otra parte, cada organización regional de la Red tiene distintas formas de tratar los casos. Es por ello que han venido trabajando en el establecimiento de una metodología común, un formulario que unifique la información para el registro de los casos en la región.

**Este formulario les permitirá integrar datos cualitativos y de georreferenciación dentro de los países que conforman la Red, especificando el contexto en el que se produce el acto de violencia.** Otras de las dificultades que se han presentado para la realización del mapeo tiene que ver con posibles datos faltantes dentro de los que se encuentran Brasil y México, territorios donde existen altos índices de violencia y un peso poblacional importante, además de países centroamericanos como El Salvador, Nicaragua, Honduras y Guatemala. 

A pesar de ello, el MLF sigue en la búsqueda de articulaciones para ampliar la mirada latinoamericana e incluir a países como Cuba y Haití.

En ocasiones, **los datos proporcionados por los Estados representan limitantes con respecto a las datas; pueden ser diferentes al compararlos con los de las organizaciones civiles,** tales son los casos de Colombia y Ecuador. Uno de los motivos de esta discordancia de las datas es la falta de actualización de las cifras estatales. Por otra parte, también se halla la diferencia existente en cuanto a la legislación del femicidio y este es uno de los principales obstáculos, ya que se deben señalar todos los marcos legales a la hora de integrar un mapeo comparativo regional.

#### **El papel del Estado**

Las legislaciones de los distintos Estados de AL son diferentes respecto a los femicidios, sin embargo, desde el MLF se reconoce que las políticas públicas, en su mayoría, no tienen incidencia en la realidad estructural de la violencia contra las mujeres, es decir, en las bases del problema: la cultura.

**El tema se centra en la óptica patriarcal a la hora de legislar y de crear políticas públicas de erradicación de la violencia.** También incide la falta de voluntad política expresada en la reducción de presupuestos para estos temas o la invisibilización de las realidades y los diferentes contextos de las mujeres. Por ejemplo, muchos de los crímenes tienen antecedentes de denuncia y los Estados no dan respuesta efectiva porque no entienden la problemática; en esos casos, mujeres han sido asesinadas a pesar de haber denunciado y alertado de la situación.

*“Gran parte del problema radica en que el Estado y sus instituciones siguen teniendo una lógica patriarcal en su funcionamiento. Esto da cuenta de lo estructural y complejo de las transformaciones por las que estamos trabajando”.* Chile, por mencionar alguno, solo hasta este año consideró el feminicidio `(3)` cuando ocurría en manos de un cónyuge o pareja. En relación con esto, el MFL señala que existe un desconocimiento total de la problemática, dejando por fuera otros casos que ocurren en contextos diferentes. 

**Por otra parte, no se incluyen en las estadísticas los casos provenientes de las personas LGBTTTIQ+, ya que no se consideran casos de “violencia de género” porque esta realidad se simplifica solo a las mujeres, excluyendo a una población que sufre los embates del sistema patriarcal.** *“Para desterrar esta mirada machista en las leyes es indispensable que dejen de estar hechas por y para los hombres”.*

Desde el MLF **se hace un llamado a que las políticas públicas sean pensadas para actuar contundentemente, que estén centradas en las víctimas**, que se trabaje profundamente con las masculinidades y que haya medidas integrales e interseccionales que tomen en cuenta la prevención, sanción, erradicación y reparación en caso de violencias. 

*“Es necesario que las mujeres y las organizaciones feministas y del colectivo LGBTTTIQ+ potenciemos nuestras luchas para impulsar la participación de todas y todes en la política. Este es el único camino para transformar al Estado y a sus leyes; no podemos quedarnos cruzadas de brazos esperando que esto suceda”.*

***Agradecimientos especiales a MundoSur y Aimee Zambrano por su valiosa colaboración para este artículo.***

![](/images/uploads/redla-color-1024x324.png)

*\--*

`(1)` Todos los datos proporcionados en este artículo fueron proporcionados por MundoSur y el Mapa Latinoamericano de Feminicidios en entrevista a exclusiva a nuestra revista.

`(2)` [MundoSur](https://mundosur.org/sobre-mundosur/) se define como un equipo interdisciplinario que elabora proyectos con impacto social desde distintos puntos de América Latina, permitiendo fortalecer políticas públicas con enfoque en derechos humanos y perspectiva de género. 

`(3)` El término femicidio o feminicidio varía de acuerdo a la legislación del país. En el caso de Chile se habla es de feminicidios, y en México, por ejemplo, se tipifican las dos acepciones.