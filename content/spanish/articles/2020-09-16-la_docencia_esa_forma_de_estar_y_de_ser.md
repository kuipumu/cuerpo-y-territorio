---
title: La docencia, esa forma de estar y de ser
subtitle: " "
cover: /images/uploads/foto_adriana_souza.png
caption: "Foto: Adriana Souza"
date: 2020-09-16T00:36:00.957Z
authors:
  - Juan Manuel Barreto
tags:
  - Educación
  - SistemaEducativo
  - Enseñanza
  - PauloFreire
  - Libertad
  - Reflexión
  - Docencia
  - Aprendizaje
categories:
  - Educación
comments: true
sticky: true
---
<!--StartFragment-->

El oficio de maestrx como un modo de estar en el mundo, como una elección que atraviesa la propia identidad, como una práctica que despliega y encarna un sentido de libertad. **El oficio de maestrx como objeto de estudio decodificado desde la experiencia del sujeto que propone su abordaje.**

¿Qué nos hace ser educadorxs, maestrxs, profesorxs?, ¿qué rasgos esenciales nos constituyen?

¿Cuáles son las razones que nos inducen a aceptar ciertas construcciones que operan en el sentido común colectivo (y se convalidan, también, desde la formación docente), en las cuales se manifiesta un acercamiento deficitario hacia la tarea?, ¿por qué se plantean como equivalentes el desarrollo de una mirada crítica de la práctica docente y la adopción de una posición de sanción constante desde donde se formulan una infinita lista de cuestiones que lxs educadorxs (y las escuelas) deberían cambiar para permanecer, para sobrevivir?

Preguntas que son puertas de entrada a otros interrogantes, a debates inacabados, a territorios en disputa.

**Imaginar una vida sin maestrxs es proyectar un imposible.** Y acá la palabra maestrx trasciende el espacio escolar y se vincula con la necesidad de encontrar en nuestras vidas referentes con quienes conversar y construir saberes y conocimientos, con quienes descubrir qué mundo nos conmueve y qué seres humanos deseamos ser, con quienes sentirnos cuidadxs y protegidxs.

**Si aproximarse al ejercicio de la docencia es partir, fundamentalmente, de la idea de amor, ser docente es poner en juego la afectividad y amorosidad, ofreciéndola no solo a lxs estudiantes sino al proceso de enseñanza.** Y en ese mismo instante en el que nos ponemos en juego nos tornamos frágiles, vulnerables, nos exponemos.

Esxs otrxs dejan de ser una representación abstracta para convertirse en presencias vivas que nos requieren interpretar los signos que se presentan alrededor, sumergirnos en existencias e historias ajenas, estar atentxs a lo que se escapa y se revela insondable. Hacerle lugar a nuestro cuerpo vibrátil (término desarrollado en 1989 por la psicoanalista brasileña Suely Rolnik, en su libro Cartografía Sentimental, transformaciones contemporáneas del deseo) para ser capaces de percibir las vibraciones de la realidad en cuanto campo de fuerzas, de dejarnos afectar, de registrar lo que (nos) sucede, de brindarle valor a lo intuitivo.

En el fondo, **la enseñanza implica un acto de donación** que nos desafía a preguntarnos por lxs otrxs pero, también, por nosotrxs mismxs y es, éste, un posicionamiento pedagógico, ético y político.

**¡Qué necesario e indispensable resulta, a su vez, cultivar una pedagogía de la pregunta!** Y a través de ella, promover la curiosidad en nuestrxs estudiantes (y en nosotrxs), habilitar el espacio mediante la construcción de nuevas significaciones, recreando las palabras, reflexionando si las respuestas que estamos ensayando lxs docentes guardan algún vínculo con lo que les sucede a lxs educandxs en su vida cotidiana, si se insertan en sus códigos, si son sensibles a sus preocupaciones y anhelos.

Hay un fantasma que persigue a maestrxs y profesorxs: el riesgo latente de convertirnos en farsantes, de estar siéndolo, de sentirlo en las entrañas. **¿Qué docente no vivió alguna vez la sensación de no estar capacitado para enseñar, para generar mejores condiciones para el aprendizaje, para lidiar con determinadas situaciones aúlicas, para vincularse y propiciar encuentros con sus alumnxs?**

Pareciera no haber recetas predeterminadas que nos alejen de ese fantasma; la docencia se mueve en una ruta de prueba y error, en un camino sinuoso, inestable, incierto, en el que se procura encontrar una manera singular de ser maestrx, de imprimir nuestra impronta, nuestros gestos, nuestra voz, nuestro cuerpo, nuestra humanidad.

Sin embargo, ese recorrido contiene, probablemente, dos certezas: por un lado, nos exige una formación técnica y una preparación afectiva, emocional, física continuas; por el otro, nos reclama una cierta coherencia entre nuestras palabras y acciones. ¿Es posible, por ejemplo, fomentar en lxs estudiantes una conciencia crítica si nos resignamos mansamente a aceptar condiciones de trabajo injustas que desvalorizan nuestra tarea? **¿Resulta creíble predicar acerca del aprendizaje colaborativo cuando no logramos pensarnos por fuera de una lógica individualista, cuando rehuimos del trabajo colectivo junto a otrx docentes?** Así como la escuela no es una sumatoria de aulas, tampoco se corresponde con una sumatoria de docentes encerradxs en sus cápsulas espacio-temporales.

Indagar acerca del oficio docente nos conduce, también, a develar el momento a partir del cual nos consideramos/sentimos maestrxs, profesorxs, educadorxs. ¿Será el día en que recibimos nuestro título habilitante o la docencia responde a una dimensión ontológica, manteniéndose independiente de la dinámica burocrática? **¿Basta un diploma para transformarse en docente o ser maestrx es un rasgo identitario, una forma de ser y de transitar el mundo, un compromiso y una responsabilidad que no puede ser reducida a un sello de goma?** Quizás un día cualquiera de nuestras vidas nos miremos en retrospectiva y descubramos que ser maestrxs (y aprendices) ha devenido en una de nuestras huellas distintivas.

Emergen otros dos aspectos al pensar la originalidad del quehacer docente. El primero, asociado a la convicción de que la práctica educativa no puede desvincularse del entorno en donde se lleva a cabo ni de las condiciones materiales, sociales y culturales de nuestrxs estudiantes; más bien se distingue ésta por la transformación de estrategias y postulados didácticos generales en función de un contexto determinado, de un grupo de estudiantes específico. El segundo, conectado a la idea del maestrx como quien se encomienda a atender (en simultáneo) la singularidad y lo colectivo, tendiendo un puente desde su rol articulador para que los aprendizajes circulen y para encontrar lo común en la diversidad.

El oficio de maestrx como un modo de rebelarse ante lo predestinado, ante las desigualdades de origen, ante los futuros predecibles. **El oficio de maestrx como inspiración para que cada quien luche por la construcción de su propia historia.**

El 19 de Septiembre de 1921 nace en Recife, capital del Estado de Pernambuco (noroeste de Brasil), **Paulo Freire,** unx de lxs pedagogxs centrales del siglo XX. Sin intención de repasar su vasta biografía, nos incito a reavivar su legado, su teoría y su praxis, su sentido de la justicia, el carácter político del acto educativo y el imperativo ético de construir una sociedad más igualitaria.

<!--EndFragment-->