---
title: ¿Dónde están las personas con discapacidad intelectual?
cover: /images/uploads/worried-girl-413690_1280.jpg
date: 2020-08-31T11:54:10.958Z
authors:
  - Federico Scheuer
tags:
  - Inclusion
  - PersonasConDiscapacidad
  - DiscapacidadIntelectual
  - Sociedad
  - Educacion
categories:
  - Diversidad
comments: true
sticky: true
---
<!--StartFragment-->

A partir de la premisa, tan difundida actualmente, según la cual “crisis supone oportunidad”, intentaré abordar, desde este espacio de pensamiento crítico, la cuestión de la discapacidad intelectual.

La idea es hacer un llamado de atención sobre dicha problemática, partiendo de planteos un tanto abstractos (y éticos, podría decirse) más que de datos empíricos e invitando a la reflexión en les lectores. Es el momento adecuado para este ejercicio, ya que aunque pueda parecer una temática sectorial que incumbe a un grupo específico de familias y profesionales tratantes, ciertamente nos atraviesa e interpela a todes en torno a qué sociedad queremos (interrogante cuya resonancia se amplificó con el advenimiento de la pandemia).

Las personas con discapacidad intelectual (PCDI), hoy más que nunca, deben visibilizarse y hallar su lugar en los heterogéneos espacios reivindicatorios que afortunadamente proliferan. Siendo precisos, “discapacidad intelectual” es una subcategoría al interior de la ya heterogénea población con discapacidad (que incluye a personas ciegas, sordas, con limitaciones motrices, entre otras, que encuentran barreras para su efectiva inclusión social), pero tal vez ha sido la más “sub” de las “categorías” de dicho colectivo en términos de participación y, de esa forma, requiere sus propias reivindicaciones, sin desestimar la valiosa articulación ni las luchas de las personas con discapacidad en sentido amplio.

Por su parte, las sociedades que emerjan de la crisis tienen el chance, dada la “puesta en entredicho” sistémica que la masividad de la pandemia conlleva, de volverse más inclusivas, lo cual les demanda reflexión y acción desde todas las esferas (pública, privada, sociedad civil). Las PCDI tienen mucho que aportar, pero ese valor humano se dilapidará si persisten las tendencias segregatorias y capacitistas especialmente en el tramo de la vida adulta. Caso contrario, la convivencia entre personas con y sin discapacidad en las instituciones comunes y en la vida cotidiana, nos ayudará a captar y crear formas de habitar el mundo bajo preceptos diferentes y transformar el lazo social.

Quisiera aportar algunos interrogantes que dinamicen este debate especialmente pertinente para pensar el mundo pos-pandemia; ello sin perjuicio de ofrecer respuestas, al menos tentativas, con la mira puesta más en la plena inclusión que en la rehabilitación -metas que, aunque debieran confluir, suelen colisionar en el campo de la discapacidad intelectual en la medida en que se subordina la primera a la segunda-. Cabe añadir que no se me ocurre un medio más propicio para emprender mi tarea que esta naciente revista, cuyo sugerente título nos ubica en el eje central de la cuestión, habilitando las primeras formulaciones: ¿dónde están las personas con discapacidad intelectual?, ¿dónde transcurren sus días, sus vidas?

Partir del dónde, para nuestra reflexión, implica preguntarnos por el territorio y por los cuerpos en él distribuidos según valores y dinámicas específicas. El territorio en tanto espacio físico, social y simbólico, configura un abanico de lugares que ocupamos y que no ocupamos, corporal, subjetiva e intersubjetivamente. Preguntar dónde nos conduce a visibilizar trayectorias, instituciones, categorizaciones, encuentros, desencuentros, estereotipos y discursos que se deslizan sobre esas experiencias, entramados de sentido tejidos desde posiciones de poder. A cada dónde le corresponde un para qué (acaso un para quién) que posibilita un estado de cosas determinado.

Es probable que al intentar dar respuesta a este cuestionamiento en torno al paradero de las PCDI, a nivel de nuestras comunidades, nos topemos con ciertas formas de segregación sostenidas por discursos terapéuticos que esgrimen una retórica “inclusiva” que no materializan. Necesitamos reconocer este grave problema y colocarlo decididamente en la agenda de la nueva normalidad, pues se trata de la respuesta predominante que en mis años de trabajo en el campo de la discapacidad en la Ciudad de Buenos Aires he podido encontrarle a ese dónde que aún me desvela.

Seré franco en este punto ya que es extremadamente fácil caer en verbalismos inútiles y mi propuesta no es tan sofisticada como para arriesgarme a ello: las personas con discapacidad intelectual, en cuanto tales, no necesitan estar separadas del resto de la comunidad, ni ser colocadas en instituciones aparte para desarrollar actividades “especiales”.

Dicho esquema artificial de sociabilidad -que dilapida capital social- debería empezar a transitar un prolijo y progresivo desmantelamiento análogo a las transformaciones que experimenta actualmente el sistema educativo hacia la escuela inclusiva. Las generaciones que egresen de esta nueva experiencia escolar deben encontrar lugares para ocupar que sean valorados por la sociedad, en instituciones comunes (mezclados con personas sin discapacidad, que quede claro) y en calidad de adultes y ciudadanes, y no sólo de “pacientes” cuyo deseo está siempre supeditado al dispositivo terapéutico.

Asumiendo con creatividad este desafío desde múltiples ámbitos, podremos habilitar la participación y restituir ese capital social colectivo. No alcanza con el optimismo de mercado que mediante la Responsabilidad Social Empresarial (casi un oxímoron) sueña una utopía de pleno empleo con apoyo, y menos aún en un contexto donde el mundo del trabajo, cada vez más excluyente, enfrenta grandes desafíos.

La tarea transformadora que propongo no es antagónica pero sí irreductible a la fórmula sobresimplificada de “participación social = escuela + mundo del trabajo”. Hay más, debe haberlo. La reeditada discusión por el ingreso universal da cuenta de la vigencia de una inquietud que hunde sus raíces en lo más profundo de nuestra cultura, en el sentido mismo que atribuimos a nuestra existencia e interacción.

¿Qué argumentos pueden echar por tierra esta voluntad de convivir? Adelantémonos: por supuesto que existe gente que necesita ser asistida, cuidada, con pleno derecho a recibir los servicios pertinentes y, que a su vez, los estados partes de la “Convención sobre los Derechos de las Personas con Discapacidad” tienen la obligación de garantizar. Pero ¡cuidado!, que este loable cumplimiento no nos lleve a legitimar modalidades de apartheid terapéutico que se extralimiten de las competencias asistenciales o pedagógicas (tan valiosas, no hay duda de ello), y acaben reproduciendo una alter-comunidad separada del resto “por su bien”.

Miremos con detenimiento las organizaciones especializadas en adultes con discapacidad intelectual, preguntemos cuánto tiempo (cotidiano y biográfico) demandan a sus usuaries, y empecemos a reformarlas para que tiendan a la inclusión, tal como se viene reinventando la educación especial a tal efecto.

Estos apuntes han sido sólo un puntapié inicial, meramente retórico, un lente distinto a través del cual observar la cuestión e intentar desnaturalizar las “vidas terapéuticas” de algunas personas. Simplemente, cuando detectemos intervenciones que congregan a las PCDI, les asignan actividades estereotipadas o aducen que “ellos son de tal o cual manera” (instalando una identidad artificial para subordinarles), nos cabe una nueva responsabilidad en tanto seres con conciencia social: sospechar. Habremos dado un importante paso hacia la incorporación de ese falso “ellos” a un mejor y más amplio nosotres.

<!--EndFragment-->