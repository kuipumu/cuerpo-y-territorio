---
title: Te has preguntado si, ¿estamos en el mismo barco?
subtitle: " "
cover: /images/uploads/hands-peace-sign-in-water.jpg
date: 2021-02-12T12:48:42.356Z
authors:
  - Catalina Medina Barrios
tags:
  - Empatía
  - Experiencias
  - CrecimientoPersonal
  - Equipo
  - Valores
  - TransformaciónCultural
categories:
  - Cuerpo
comments: true
sticky: true
---
Desde el 2019 he estado conociendo y estudiando sobre contracultura en las organizaciones, que en general, como lo he entendido **es una transformación cultural a organizaciones autogestionadas.** Este tema me ha llevado a muchos cuestionamientos internos de perspectiva y a revisar las dinámicas que tenemos en nuestra sociedad, porque para llegar a ese nivel de autogestión en organizaciones, las personas que hacemos parte de ellas debemos tener una relación de confianza y responsabilidad, **es un modelo de organización totalmente enfocado en la persona.**

Entendiendo esto me surgió una pregunta que hoy quiero compartir contigo **¿Cuántas veces recuerdo que los demás están en la misma búsqueda mía?**

Aquí te va mi respuesta, siendo 100% transparente muy pocas veces y la primera vez que caí en cuenta de esto, estaba moderando una conversación entre el sector público y el privado.

Estamos tan preocupados por lo que nos “hace falta” envuelto en “mis sueños”, “mis pasiones”, “quién quiero ser”, “qué quiero tener”, que no logramos entender lo que tenemos en el instante, lo que somos y lo que eso significa o el impacto que eso tiene en nuestra comunidad, familia, sociedad o empresa. Buscamos constantemente soluciones a problemas que no entendemos, nombres de culpables a fracasos que son de colectividad, etc. **buscamos afuera lo que realmente no entendemos adentro.**

En un evento que organicé hace unas semanas, uno de los ponentes dijo: *“en el sector público es esencial innovar de adentro hacia adentro”* y hoy diría que no solo en el sector público - que debo ser sincera, le damos palo (criticamos, juzgamos, ofendemos) pero ¿Cuántos queremos trabajar en él?, ¿Cuántos creamos soluciones reales para los errores que vemos en él? ¿Cuántos entendemos realmente el papel del sector? - el innovar de adentro hacia adentro va en todo, **no podemos esperar un cambio afuera que no entendamos por dentro.** Y esto lo quiero llevar  al concepto más simple que actualmente hablamos: **La empatía**, que aunque hablamos, me cuestione qué tanto entendemos lo que esto significa.

Lo digo por experiencia propia, siempre me consideré muy empática, entendiendo y escuchando, pero descubrí que si en mis conversaciones o mi visión había juzgamiento, crítica y/o rechazo, no estaba siendo empática realmente con la situación o la persona, **solo estoy escuchando dos posiciones pero empatizando con una sola de ellas,** olvidando entonces lo que la otra parte cuida, la crisis que esta otra situación ve y vive, que no veo con claridad porque no conecto fácilmente lo que me aleja de realmente empatizar.

Ver mi respuesta me llamó mucho la atención y verme en diferentes actividades y situaciones en donde claramente se notaba mi posición no empática con la situación, **me abrió los ojos para trabajar en mí conscientemente en un proceso de autogestión donde pueda ir abriéndome a entender la búsqueda,** el miedo, lo que cuida el otro a través de la empatía y la transparencia en las conversaciones, porque lograr encontrar una visión más amplia de mi comunidad, mi familia, mi empresa puede mostrarme que lo único que nos hace diferentes es que **yo estoy en la proa y el otro en la popa del mismo barco.**

Esto es lo que vi y me respondí de la pregunta, sin embargo, sería increíble que pudieras hacerte esta pregunta y ver qué tan lejanos están los por qué y para qué  además de los tuyos.