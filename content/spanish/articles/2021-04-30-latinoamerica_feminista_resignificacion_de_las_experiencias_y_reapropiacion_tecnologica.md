---
title: "Latinoamérica feminista: resignificación de las experiencias y
  reapropiación  tecnológica"
subtitle: "México "
cover: /images/uploads/28.oct.2017._foto_tomada_en_el_hacklab_rancho_electronico_en_la_ciudad_de_mexico..jpg
date: 2021-04-30T21:56:38.455Z
authors:
  - Aurea Páramo
tags:
  - Feminismos
  - Cuerpo
  - Territorio
  - Mujeres
categories:
  - Genero
  - Cuerpo
  - Territorio
comments: true
sticky: true
---
Como en otras partes del mundo influencias del **pensamiento crítico** impregnan espacios de reflexión, discusión y crítica que no se agotan ante una realidad que nos rebasa. El ejercicio del pensamiento crítico, el hartazgo por las injusticias y el **rechazo a la violencia** nos impulsa a cuestionar la sociedad radicalmente, desde la raíz, y asumir la responsabilidad que de ello se deriva. La crítica social se vuelve inherente, la manifestación de desacuerdo ante los poderes establecidos y las exigencias de justicia se convierten en las herramientas de transformación más comunes, más efectivas, de todos, incluidas las mujeres.

**El movimiento feminista**, como todos, tiene sus momentos de algidez y de reflujo, a veces parece cauto, como brisa, y algunas otras se vuelve tempestad absoluta. La toma de conciencia del mundo que habitamos, del **cuerpo que somos y de los territorios que ocupamos** nos llevan a cuestionar nuestro estar individual y colectivo, a reconocer nuestras vidas y las experiencias que hemos transitado.

Los acercamientos a las discusiones y reflexiones **feministas** ocurren desde distintos territorios, en distintos momentos, también por azar; por estar haciendo o diciendo, escuchando, mirando o sintiendo algo que resuena en otras y que resuena en una misma. Las experiencias evocan la memoria, en ellas resuenan los ecos. Aquello que parecía tan particular, tan íntimo, se vuelve parte de un todo, se inscribe dentro de otras tradiciones, otras mujeres, otras historias que también es la propia.

**La lucha política** en la que nos encontramos implica decisiones, transformaciones. Impregna los espacios y detona experiencias. Al compartirlas, revivirlas, escuchar nuestros pensamientos y sentires, nuestras voces y palabras, nuestro afecto y cariño nos descubrimos distintas y parecidas. **Ese mirarnos** nos ha ayudado a encontrarnos y descubrirnos juntas (aunque diversas), y por ello, fuertes al reconocer nuestra capacidad de acción y transformación no sólo del mundo sino también de nuestras propias vidas.

Es por ello que la experiencia entendida como “conjunto de significados y sentidos de lo vivido” (Díaz-Barriga, 1991, citado en Baz, 1993: 87), como fuente de conocimiento y como resultado de códigos y esquemas de significación e interpretación de la cultura vueltos inteligibles para la propia conciencia, adquiere relevancia como lugar común de encuentro, **como espacio lleno de sentido tanto individual como colectivamente.** Aparece ya no únicamente como experiencia de una vida vivida, individual, sino como experiencia política compartida que emerge junto con la decisión activa de accionar sobre los ámbitos de la vida íntima, social y política.

Sin embargo, resulta necesario reconocer las condiciones y tiempos distintos de estas experiencias, explorar los contextos específicos para reconocer las diferencias, las similitudes, las secuencias y entramados que persisten y/o se transforman como parte de los procesos que van pautando formas distintas del devenir feminista.

**El feminismo**, al ser una práctica política que confronta el **discurso dominante** pone en cuestión la dificultad del nombrar y nombrarse. Aunque compleja, la decisión de enunciarse feminista constituiría por sí misma una acción política consciente. El nombrarse se vuelve problemático pues no sólo representa responsabilidad sobre la construcción de sí misma sino la responsabilidad de la acción congruente para con los otros.

![](/images/uploads/08.marzo.2019._dia_internacional_de_la_mujer._marcha_morelia_michoacan..jpg)

Nombrarse como feminista no necesariamente implica el asumir una identidad ya construida, completa, estable, es parte de un proceso de construcción de sí misma, de exploración y reconciliación, no sólo en soledad sino también con otras, **reconocerse y reconocer** a las otras que nos anteceden y acompañan, reconocer nuestras genealogías y reconocerse parte de ellas, como proceso infinito.

El peso contenido que la **palabra** evoca, la saturación de su significación y el compromiso que de ella se deriva continúa siendo nebuloso no sólo para las feministas sino también para las relaciones próximas y los espacios habitados, sin embargo, parece que al paso de los años, al abrigo de las experiencias vividas, de los momentos de lucha recordados, de las relaciones construidas sobre bases más francas y libres deja de ser un problema para convertirse en una revelación de transformación asumida, de identificación y convergencia.

No obstante, la práctica política, el posicionamiento asumido, hace que surjan una serie de inquietudes, de dudas, frustraciones y sentires diversos que, como parte de un proceso de cuestionamiento, critica y construcción de sí permean la vida individual, el sentido de las acciones y las relaciones sociales. Por ello, la incorporación reflexiva de las experiencias como procesos de lucha colectiva se vuelve inminente.

Los espacios compartidos han impulsado el aprender a usar la propia voz no sólo para nombrarse y reconocerse sino también para denunciar, **manifestar, expresar, opinar y gritar**, pero también para **escuchar la voz** de las otras con las que se comparte y construye.

La reflexión no es puramente individual, es necesariamente colectiva, no sólo en el sentido de diálogo colectivo “real”, sino en su **carácter dialógico**, de posicionamiento frente a otros, de comprobación de las diferencias, de la afirmación de los cruces, de las similitudes, pero también del reconocimiento de la propia particularidad y la particularidad de las otras.

No resulta extraño que las estrategias de lucha (pensando en las formas de organización y las prácticas políticas) vayan generando nuevos modos de expresión de acuerdo a los contextos, intereses, demandas y urgencias de los distintos actores sociales, siendo éstos al mismo tiempo acordes a las transformaciones sociales, políticas y tecnológicas desde donde surgen.

Es por ello que frente a un panorama de ascenso de la **violencia de género** las mujeres que se autodenominan feministas han tenido que definir e implementar distintas estrategias para afrontar el problema feminicida y de violencia, violencia directamente ejercida sobre los cuerpos y las vidas de las niñas y mujeres.

La difusión ha sido un aspecto clave para el desarrollo del feminismo a niveles nacionales y locales (no es una excepción para el caso de **México y de Michoacán**). Las feministas de entonces tuvieron como objetivo llevar los pensamientos críticos y reflexio



nes más allá de sus círculos de autoconciencia, de los espacios de estudio y posibilitar así el contacto directo con otras mujeres. Compartir las experiencias al mismo tiempo que los aprendizajes, preocupaciones y posturas.

Es así como el uso de diversas estrategias comunicativas permite que las demandas de los colectivos feministas permeen en espacios de enunciación y diálogo que las hace aparecer y las vuelve inteligibles para diversos sectores que se constituyen como interlocutores.

![](/images/uploads/la_boletina_de_morelia._organo_informativo_de_la_red_nacional_de_mujeres._enero_1983_numero_3_ano_2._archivos_historico_del_feminismo_-_unam-cieg.jpeg)

Los folletos, cuadernillo y publicaciones impresas constantes fueron parte de las **estrategias de comunicación** no sólo como estrategia de intervención sino además en respuesta a la necesidad de **sistematizar** y dar cuenta de las actividades, avances y pasos en la consolidación de espacios y desarrollo de iniciativas para contrarrestar la violencia y confirmar la lucha por la **defensa de los derechos de las mujeres**.

Si bien es cierto que en un momento las estrategias y **medios de comunicación** e intervención se centraron en el uso de la radio, la imprenta y la acción dirigida a sectores específicos es necesario reconocer que las nuevas tecnologías de la comunicación (el internet y las redes sociales digitales) así como la apropiación por parte de diversos actores sociales hacen posible el surgimiento de **nuevas prácticas políticas**.

Las estrategias de comunicación que se han implementado en los últimos años y que marcan los tránsitos generacionales de un sin número de feministas en búsqueda de estrategias acordes a los contextos nos obligan a reconocer la relevancia de las **redes digitales** ya que “las plataformas de redes sociales cambian el modo de operar de la comunicación no sólo en el momento de su producción y distribución sino en el momento de su consumo” (Rovira, 2017:131).

 El uso de plataformas como Facebook, Twitter, Instagram WhatsApp además de plataformas de difusión, divulgación y **denuncia digital** como estrategia política feminista nos llevan a la discusión en torno al acceso y uso de Internet, examinando las posibilidades, alcances y limitaciones para los movimientos sociales y las diversas prácticas políticas. La importancia de la implementación de espacios de **alfabetización y reapropiación tecnológica**, sistemas de data abierta y software libre sigue siendo explorada.

Estas expresiones vienen a renovar la importancia del **internet y las redes sociales** pasando del uso ocioso e individual a uno explícitamente político y colectivo, haciendo posible el quiebre de las **fronteras digitales** para verse reflejadas en las acciones en los espacios públicos. Redefiniendo y cuestionando, además, las nociones de centralidad, organización y acción política predominantes durante los siglos XIX y XX.

El desarrollo de las nuevas tecnologías añade nuevas dimensiones para los procesos de lucha que implican no sólo la circulación y difusión de todo tipo de información sino además el uso de plataformas comunicativas que abren paso a la **posibilidad de interacción**, articulación, cooperación, diferenciación y confrontación entre posicionamientos políticos públicos, individuales y colectivos, así como la manifestación, también pública, de demandas sociales y denuncia de los diferentes tipos de violencia.

1. ![](/images/uploads/28.nov.2017._en_el_acto_politico_con_la_presencia_de_marichuy_vocera_del_cig_en_la_caseta_telefonica_de_ciudad_universitaria_donde_fue_encontrado_el_cuerpo_sin_vida_de_lesvy_berlin_osorio.jpg)

El camino recorrido parece largo, pero creemos que en el transcurrir del tiempo el nombrarnos, el compartir nuestras experiencias, el encontrarnos en y desde los espacios que decidimos habitar y desde las estrategias que implementamos podamos afirmarnos, como potencias en acto, como fuerza de transformación de la sociedad y del mundo que nos invisibiliza y somete.

Encontrarnos, reconocernos, tomar la palabra, los espacios, las tecnologías; nombrarnos, alzar la vos, gritar, gestionar el llanto, organizar la rabia; hacer uso de los conocimientos y las herramientas a nuestro alcance; todo ello, cada pequeño y grande intento va constituyéndose como parte de la construcción de **nuevas realidades**, más dignas y libres para todas. Así, transformando nuestra tristeza y nuestra rabia en fuerza, recuperando nuestra alegría como impulso vivo para encontramos diversas, unidas y combativas teñidas de verde y violeta vamos caminando juntas desde todos los nodos de América Latina.

### Referencias

Baz, M. “La entrevista de investigación en el campo de la subjetividad”. En Jáidar, I. (comp.) (1993). Caleidoscopio de subjetividades. México: Universidad Autónoma Metropolitana, Unidad Xochimilco.

Rovira, G. (2017). Activismo en red y multitudes conectadas. México: Icaria Editorial y Universidad Autónoma Metropolitana.

#### Aurea Páramo

(Morelia-Michoacán, 1990) Licenciada en Psicología por la Universidad Michoacana de San Nicolás de Hidalgo. Maestra en Comunicación y Política por la Universidad Autónoma Metropolitana, Unidad Xochimilco. Las líneas de interés que enmarcan sus investigaciones giran en torno al psicoanálisis, la subjetividad, el género y la política. Activista antifascista, anticapitalista y antipatriarcal. Cinéfila y lectora apasionada. Bailarina y practicante de natación por voluntad.