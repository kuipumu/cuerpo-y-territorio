---
title: ¡Más que nunca, las venas están abiertas!
subtitle: "Pintando América Latina "
cover: /images/uploads/img-20201005-wa0001.jpg
date: 2020-10-10T02:27:55.809Z
authors:
  - Florencia Cencig
tags:
  - EdiciónEspecial
  - 12deoctubre
  - Identidad
  - Latinoamerica
  - Continente
  - Cronica
  - Relato
  - Experiencias
  - Viajes
  - América
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

Mientras la megaminería, los mayores poderosos dueños del capital, y los monstruos empresarios que decretaron ser los propietarios de nuestra Latinoamérica se van comiendo al mundo, aprovechamos el mes de octubre para recordar, urgentemente, que la Tierra no nos pertenece. ¡Perdonanos, Pachamama! Pero por suerte, todavía nos quedan grandes maestros que nos recuerdan cuál es el verdadero origen.

Heber Fleitas; artista- escenógrafo, muralista-pintor, actor y técnico teatral, pero sobre todas las cosas, HUMANO, nos presta sus ojos para mirar la verdadera América Latina y, sus andanzas, para expresar aquello que el capitalismo se topó por completo y los conquistadores quisieron exterminar.

![](/images/uploads/20201003_212032.png)

Él nos ayuda a desandar los caminos invadidos, para adentrarnos en aquellas comunidades que aún siguen resistiendo para no ser olvidadas, y nos enseña que el arte, además de ser un medio de aprendizaje y expresión, es un recurso primordial para perpetuar la identidad de nuestras culturas.

Cuando uno habla con Heber, puede aprender sobre la “Danza del Sol” y los indios lakotas (EE.UU y Canadá), sobre la cosmovisión Andina y Amazónica, y sobre los templos y túneles subterráneos ubicados en las Sierras de Perú, Chavín de Huántar. Puede enterarse que la zona de Pucallpa, ubicada en la selva amazónica peruana, está habitada por comunidades ancestrales de la etnia “Shipibo-Conibo”, y que a 14 kilómetros, aproximadamente, de Puerto Callao en Yarinacocha, se encuentra la comunidad nativa San Francisco.

“San Francisco”, obviamente, nombre otorgado en la conquista de la iglesia católica, la cual se encargó de robar la cantidad de oro perteneciente a dicho territorio, y las perlas que se encontraban en la laguna “Yarinacocha”. Donde, actualmente, viven de la venta de tejidos y cerámicas e intentan transmitir la lengua shipibo, de generación en generación, para que no se extinga.

Este viajero argentino, apasionado del arte y respetuoso de los saberes que nos dejaron los diversos pueblos originarios de nuestro continente, aprende y se nutre de las costumbres y las creencias características de cada población a la que visita. Las pinturas y los dibujos que diseña y deja plasmados en las construcciones, combinan la sabiduría ancestral, la historia y la identidad propia de cada poblado al que concurre.

![](/images/uploads/20201003_212129.png)

En Shipibo, al igual que en muchas otras poblaciones, fueron construyendo sus formas de vida, a partir de la fusión de varias cuestiones; las medicinas naturales, los rituales, el conocimiento espiritual y las plantas maestras. Así fue como, la combinación de todo ello, dio lugar a que el lenguaje, las edificaciones y el arte respondan a una determinada cosmovisión.

A partir de toda la riqueza que lleva Heber guardada en su mochila viajera, la cual solo puede explicarse a través de las palabras y los sentimientos, y a 528 años de aquella invasión europea y genocidio al pueblo latinoamericano, creo que es imperioso preguntarnos: ¿Hasta dónde?, ¿cuál es el punto de inflexión?, ¿cuánto tiempo más vamos a mirar para otro lado?.

Las nuevas y crecientes problemáticas sociales, culturales, económicas y políticas dejan evidenciado que este sistema capitalista y eurocéntrico, prácticamente, nos ha consumido. La propia naturaleza y el medioambiente nos ha demostrado que si no modificamos la forma mercantilista con la que miramos al mundo, no hay mucho más resto.

Entonces, me pregunto; ¿Por qué no contamos con un sistema educativo que nos enseñe desde pequeños las lenguas de nuestros pueblos originarios?, ¿por qué en pleno 2020 hay gente que todavía cree que el 12 de octubre se conmemora el “Descubrimiento de América”?, ¿por qué en las Universidades no nos forman a partir de las filosofías contra-hegemónicas y los paradigmas de la tierra?.

Porque todo eso, no vende. Porque todo eso, no produce ni consume. Porque todo eso, no explota, no individualiza y no enriquece a los que mueven los hilos del planeta.

Incendios forestales masivos generados por el hombre, en pos de negocios millonarios. Megaminería que conlleva impactos gravísimos en todo el mundo, atentando contra la salud de las personas y violando los derechos humanos. Pandemias, crisis climática y calentamiento global.

![](/images/uploads/20201003_212054.png)

¿Nada es suficiente para detenernos a interpelar la manera en la cual vivimos? Pero es aquí, donde llega Heber, y nos sacude. Nos demuestra que los pueblos nativos tienen el verdadero conocimiento de cómo relacionarnos con la tierra, y la capacidad de manejar los recursos de manera responsable, obteniendo lo que necesitamos para vivir, sin dañar al ecosistema y sin provocar nuestra autodestrucción, o peor aún, la destrucción de nuestra naturaleza.

Llega Heber con su arte, para inmortalizar en el tiempo la cultura de aquellos que le dieron la identidad a América Latina y recordarnos de dónde venimos. Llega Heber, con su consciencia y su experiencia, para transmitirnos el verdadero origen de todo lo que nos rodea.

Llega él y nos hace parar el acelerador, para sentirnos un poco más humanos. Un poco más despiertos y menos egoístas. Ojalá todos nos topemos con un Heber en el camino, que nos preste su mirada y, así, eternizar la otra historia. La verdadera. La que no te cuentan en los libros. La que quisieron borrarnos. Pero siempre, hay un Heber, que la salva y la grita.

Como nuestro querido Galeano nos decía…“mucha gente pequeña, en lugares pequeños, haciendo cosas pequeñas, puede cambiar el mundo”.

**Podemos encontrar el arte de Heber en IG: @heberfleitas.art**

#### ***Otras entregas de esta edición especial "12 de octubre"***

[Las doce Malinches o el mito que seremos](https://revistacuerpoyterritorio.com/2020/10/10/las-doce-malinches-o-el-mito-que-seremos/) 

[Historias tejidas en los cuerpos y territorios del sur de América PARTE I](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america/)

[Historias tejidas en los cuerpos y territorios del sur de América PARTE II](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america-parte-ii/)

<!--StartFragment-->

#### Además, los invitamos a conectarse a la tertulia de YouTube live este 12 de octubre para conversar, desde otras perspectivas, sobre esta fecha tan importante en América haciendo clic [AQUÍ](https://www.youtube.com/watch?v=Bddgfi3tARU&feature=youtu.be)

![](/images/uploads/12_de_octubre_la_historia_no_contada_de_america_9_.png)