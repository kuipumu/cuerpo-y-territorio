---
title: Mujeres del Séptimo Arte
subtitle: "Cine hecho por mujeres "
cover: /images/uploads/sofia-coppola.jpg
caption: Sofía Coppola
date: 2021-03-30T20:32:26.663Z
authors:
  - Yuliana Fuentes
tags:
  - Cine
  - Mujeres
  - Arte
  - Latianomerica
  - Directoras
categories:
  - Cultura
  - Género
  - Territorio
comments: true
sticky: true
---
Sabemos que el 8 de marzo se considera el Día Internacional de la Mujer. Más allá de esa fecha, el mes de marzo se considera un mes de lucha por los derechos de las mujeres. Resulta importante resaltar el rol histórico que hemos tenido las mujeres dentro de la industria cinematográfica. **El aporte de las mujeres en el cine ha sido clave, aunque con mucha frecuencia se pasa por alto. No solo en estar frente a la cámara sino que detrás de ella.**

En américa Latina no son pocos los nombres ni las conquistas alcanzadas por mujeres: la pionera brasileña **Carmen Santos**, las chilenas **Alicia Armstrong de Vicuña, Gaby Von Bussenius Vegas**, así como las mexicanas **Mimí Derba, Cándida Beltrán Rendón y Adela Sequeyro**. Todas mujeres productoras, guionistas y directoras del periodo del cine silente que lograron progresar gracias a su tenacidad y su necesidad de expresión creativa.

La venezolana **Margot Benacerraf** obtuvo dos premios en Cannes por su película **Araya (1958);** la argentina **María Luisa Bemberg** fue nominada a dos Oscar por **Camila (1984);** la también venezolana **Fina Torres** logró fama internacional con su ópera prima, **Oriana (1985)**; la brasileña **Suzana Amaral** fue reconocida por **La hora de la estrella**, que recuperó la tradición del Cinema Novo y obtuvo el Oso de Plata a la Mejor Actriz en Berlín en 1986.

La mayoría de estas cineastas vivieron las grandes reconstrucciones sociales y los proyectos políticos de los 60 y 70, cuando se abrió un amplio espacio para los movimientos feministas y se manifestaron en colectivos de cineastas mujeres, muchas veces orientados al género documental. En el camino, algunas posiciones radicalmente feministas se movieron de lugar y es así que encontramos visiones comprometidas con lo social y la innovación creativa, pero despojadas de una atadura feminista.

Hoy las mujeres ya no tienen que demostrar que manejan las artes del oficio: los éxitos internacionales, las innovaciones estilísticas y los recursos narrativos originales que han aportado así lo demuestran.

En esta entrega hago un breve repaso por la historia de la participación de la mujer en el cine con algunas recomendaciones de películas dirigidas por mujeres que es imprescindible ver. Films con historias que tienen que ser difundidas para lograr conexión con la práctica fílmica actual y así revalorar el trabajo de las mujeres en el cine y sus significativas contribuciones en las áreas de la producción, guión, edición y dirección.

#### Cleo de 5 a 7 (1962). Dir: Agnès Varda.

![](/images/uploads/agnes_varda.jpg)

* #### Chocolat (1988). Dir: Claire Denis.

  ![](/images/uploads/media.jpg)
* #### El Piano (1993). Dir: Jane Campion

  ![](/images/uploads/el-piano.jpg)
* #### Las vírgenes suicidas (1999). Dir: Sofía Coppola

  ![](/images/uploads/f9910f03a2dba9cef0ea0cd3a39500b1.jpg)
* #### La ciénaga (2001). Dir: Lucrecia Martel

  ![](/images/uploads/la_ci_naga-411901291-large.jpg)
* #### La Teta Asustada (2009). Dir: Claudia Llosa

  ![](/images/uploads/la_teta_asustada.jpg)
* #### Pelo Malo (2014). Dir: Mariana Rondón

  ![](/images/uploads/prlo_malo.jpg)
* #### El verano de los peces voladores (2014). Dir: Marcela Said

  ![](/images/uploads/verano_poster.jpg)
* #### Mustang (2015). Dir: Deniz Gamze Ergüven

  ![](/images/uploads/mustang_poster.jpg)
* #### Línea 137 (2019). Dir: Lucía Vasallo

  ![](/images/uploads/840339774.webp)
* #### Canela (2020). Dir: Cecilia del Valle

  ![](/images/uploads/mv5byze0yzq4mzktmzqxmi00nwjllwjjogetmmm1zde3mzdlymuyxkeyxkfqcgdeqxvymjc0njk4mw_._v1_.jpg)
* #### Nomadland (2020). Dir: Chloé Zhao

  ![](/images/uploads/unnamed.jpg)
