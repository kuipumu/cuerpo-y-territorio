---
title: "Descubrir para crear desde la neurodiversidad "
cover: /images/uploads/equipo_casa_de_carlota_medellin.jpeg
caption: Equipo de Casa de Carlota. Medellin, Colombia.
date: 2020-08-31T10:00:20.850Z
authors:
  - Catalina Medina Barrios
tags:
  - Neurodiversidad
  - Inclusion
  - Creatividad
  - Empresas
  - Emprendimientos
  - ComunidadNeurodiversa
categories:
  - Diversidad
comments: true
sticky: true
---
<!--StartFragment-->

La creatividad es la habilidad que primero comenzamos a explotar, porque es a través de ella que entendemos el nuevo mundo que vamos descubriendo y a crear la forma en la que viviremos en él. 

Creamos mundos imaginarios, creamos historias con elementos sin vida, creamos ambientes, amistades, creamos un futuro, creamos enemigos, miedos, con ella solucionamos todo, en nuestra infancia el único límite para ella es cuando cerramos los ojos al dormir.

<!--EndFragment--><!--StartFragment-->

Esta habilidad base de todo lo que somos, tenemos y creemos, ha evolucionado en importancia durante las diferentes épocas. Sin embargo, durante la llamada Industria 4.0 o la cuarta revolución industrial liderada por el internet de las cosas, la creatividad ha tomado un rol primordial en la existencia y permanencia de empresas, emprendimientos y hasta en nosotros como humanos, como el título del libro de Andrés Oppenheimer estamos en la época de “Crear o Morir”, así que constantemente me encuentro con talleres, metodologías, estructuras que fomentan la creatividad y que activan nuestra habilidad innata.

Por mucho tiempo lo supuse así, supuse que llegaba un momento en donde cortamos la creatividad, así que era necesario, con factores y esquemas externos, crear espacios donde las personas pudiéramos activar esa habilidad que habíamos rezagado. No obstante, hace un mes esto cambió para mí, traer a primera línea la creatividad solo es cuestión de abrirnos a descubrir.

En julio de este año (2020), se realizó la segunda edición virtual de un evento que organizo llamado Pechakucha Night*, en este evento tuvimos como invitado a Nelson Correa (Nel), un emprendedor que venía siguiendo desde un año atrás, bueno no tanto a él, si no a su empresa, [La Casa de Carlota](https://www.lacasadecarlotaandfriends.com/medellin/) , en su intervención él comentó que su equipo era neurodiverso, ese concepto llamó mi atención y se quedo en mi cabeza por varios días.

Estaba acostumbrada a escuchar diversidad en los equipos de trabajo como una de las “reglas” o “tips” para habilitar la creatividad y llegar a la innovación, pero no neurodiversidad en equipos de trabajo, además que si nos cuesta gestionar las diferencias en los equipos, no me imagino en equipos neurodiversos.

Nel nos mostró que esa era la magia detrás del éxito creativo de su empresa, compartiendo de igual manera un poco del detrás de cámaras de las incidencias de la gestión de su equipo.

Llevaba mucho tiempo siguiendo el trabajo de La Casa de Carlota, sin embargo, nunca me había puesto a pensar y procesar la gestión de la creatividad en esta empresa, hasta esa noche en donde Nel nos dio ese abre bocas, así que decidí ponerme a averiguar un poco más del concepto.

Uno de mis descubrimientos más grandes sobre este tema, fue encontrarme a mí como persona neurodiversa. Este término nace con Judy Singer, quien a finales de los 90, rechazó la idea que las personas con autismo eran discapacitadas, diciendo que sus cerebros (los de las personas con autismo o alguna condición dentro del espectro del autismo) simplemente funcionaban de manera diferente.

La neurodiversidad es un concepto que refiere a las personas con rasgos neurológicos no comunes a nivel del sistema nervioso central (el cerebro), presentando patrones de comportamiento diferentes a los impuestos por los estándares sociales a este grupo pertenecemos las personas con dislexia, dispraxia, déficit de atención, hiperactividad (TDAH) o pertenecientes al espectro autista.

Entrar a conocer más sobre este concepto e identificarme en él, también me llevó a encontrar artículos y empresas dedicadas a fomentar la neurodiversidad en las compañías y entonces fue ahí cuando mi creencia sobre la creatividad, que les compartí al inicio de este escrito, comenzó a modificarse.

A pesar de esto, aún tenía algo en mi cabeza que no quedaba claro. En el equipo de Nel hay personas con síndrome de Down, ¿por qué él dice equipo neurodiverso si en lo que encuentro no incluyen esta población?.

No voy a responder en este escrito el descubrimiento científico o médico, pero sí lo que esta pregunta modificó en mi visión de la creatividad y el uso que le damos diariamente.

Lo que descubrí entre artículos, leyes y una charla más intensa con Nel, es que el proceso de aprendizaje y de conducta de las personas con síndrome de Down es mucho más conflictiva para los estándares sociales con los que vivimos y trabajamos.

Así que para no entrar en la difícil tarea de entender sus procesos, ideamos alternativas para el relacionamiento y desarrollo en la sociedad de esta comunidad, limitando su libre desarrollo y por ende limitándonos a nosotros la posibilidad de navegar formas de ver el mundo, limitando nuestra creatividad y llevándonos a crear estructuras, metodologías y ambientes propicios o controlados para trabajarla, en vez de simplemente abrirnos a comportamientos, razonamientos y procesos de aprendizajes diferentes que nos lleven a como cuando éramos niños y descubrimos el mundo, porque con el simple hecho de generar contacto e interactuar con algo nuevo, nuestra creatividad va a surgir de la manera como siempre lo ha hecho, solo le hemos minimizado su magia, entre procesos y estándares de como tendría que funcionar.

Este es un tema que tiene bastante contenido para conversar y explotar. Varias empresas han querido comenzar a trabajar, algunas van poco a poco, otras lo dejan a un lado porque las barreras burocráticas lo hacen más pesado. Sin embargo, mi invitación con este escrito es a que nos permitamos navegar sin orden y sin miedo en las estructuras mentales, sociales y culturales propias y en las de los demás, porque es al mezclar el talento que encontramos en estas y en su espontaneidad y autenticidad la inspiración que necesitamos para crear e innovar.

## Links interesantes:

* <https://www.sindromedownvidaadulta.org/>
* <https://www.down21.org/>
* <http://www.secretariasenado.gov.co/senado/basedoc/ley_1996_2019.html>
* <https://www.bbc.com/mundo/noticias-51483201>
* <https://neurodoza.com/>

<!--EndFragment-->