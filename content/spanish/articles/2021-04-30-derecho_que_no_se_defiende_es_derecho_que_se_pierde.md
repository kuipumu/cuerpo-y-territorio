---
title: "Derecho que no se defiende es derecho que se pierde "
subtitle: "Las mujeres en Honduras "
cover: /images/uploads/golpe_2.jpg
date: 2021-04-30T21:34:48.286Z
authors:
  - Lídice Ortega
tags:
  - Mujeres
  - Genero
  - Feminismos
  - Latinaomerica
  - Derechos
categories:
  - Genero
  - Cuerpo
  - Territorio
comments: true
sticky: true
---
La realidad **hondureña** y especialmente la de las **mujeres**, nosotras las invisibilizadas y oprimidas es de constante batalla, vivimos en la sombra al no contar con datos oficiales, confiables y sistematizados que nos permitan conocer de manera documental las múltiples realidades de las personas y especialmente de las mujeres que viven montaña adentro. Las que enfrentan batallas en territorios y cuerpos.

Vamos a ir por partes, desde el **golpe de estado** en el 2009 mi amada Honduras es objeto de una situación encierro, como si no fuera suficiente ser una República empobrecida, también desaparecimos del foco público internacional. Los golpistas se lavaron las manos, la comunidad internacional dio por hecho que todo estaba resuelto y la sociedad hondureña debía seguir trabajando para comer.

Ese golpe de Estado nos arrebató en gran medida esa sensación de tener un país…¿democrático?, esto desde mi voz de mujer capitalina y más pendiente de encontrar un crédito para el carro que de la realidad, realidad, pero también de cierto modo, el presidente Zelaya, es el culpable por darnos la probadita de un sistema de gobierno participativo, eso nos gustó y queríamos más. Yo crecí pensando que a Honduras le faltaba poco para superar la desigualdad. Tenía 18 años, no me culpo.

La frustración que nos dejó la lucha contra el golpe de Estado y luego la instalación de gobiernos nacionalistas e incluso la reelección en un país donde está prohibido, fomenta lo que tenemos hoy día que llamar algo así como **crisis social-económica-politica-juridica**. En este sentido, las montañas dentro, donde habitan las comunidades de **hondureñas y hondureños** que aún resguardan el vínculo ancestral y sus relaciones con la tierra son de respeto, ojo, dije con la tierra… con las mujeres ya es otra cosa.

Sin duda es en estos territorios en los que las batallas contra la explotación desmedida tienen arraigo, con la **ruptura del orden constitucional** los negocios empresariales y gubernamentales se expandieron y el procesos de concesiones de bienes comunes para diferentes explotaciones: Agua (hidroeléctricas), minerales (minería), luz (fotovoltaicas), fertilidad de la tierra (monocultivos), aire (eólicos), fuerza de trabajo (maquilas y ZEDE ́s), paisaje y cultura (industria turística transnacional) y carreteras (peajes); son solo los algunos de los flagelos con los que libran una batalla a muerte las comunidades indígenas hondureñas, las mujeres lencas, pech y misquitas entre otras…

**Las sociedad hondureña** parece silente, sin embargo, solo falta ver que no existe pandemia que pueda detener al pueblo que requiere del día a día para resolverse unas fichitas para comprar comida para darse cuenta que lo que hay aquí es una sociedad convulsa . La ausencia de **medidas de protección** y una intervención gubernamental y no gubernamental son los más eficientes detonadores que ha encontrado la inquietud social, para explosionar a niveles locales, regionales y nacionales.

**No hay Estado de derecho en Honduras**, las soluciones no están a la mano. Son las ONGs que en sus intervenciones/acciones/implementaciones/proyectos hacen un abordaje de esta crisis y creanme no siempre representa esto una solución sistémica, ya que en algunos casos las soluciones no comprenden hacer incidencia para presionar las estructuras de Estado, no contemplan una incidencia que cuente las costillas a la política y menos exige rendición de cuentas para evaluar las múltiples realidades de pobreza en los territorios. Ojo, hay actuaciones en este campo que no se quedan conformes con finalizar proyectos y se están preguntando por la sostenibilidad.

**No somos una, somos millones Cuenten bien**

![](/images/uploads/mujeres_lenca_honduras.jpg)

En Honduras las mujeres se organizan para reclamar, para denunciar y también en para migrar a Estados Unidos en caravanas, ellas afirman que no es sólo la pobreza, también es a causa de la **violencia desmedida** y la desalentadora sensación de estar desprotegidas, miles de mujeres de las comunidades más pobres son empujadas a cargar con sus criaturas y sus morrales buscando una vida que quizá puede ser mejor. En lo personal tengo media familia allá en la USA y mis vecinas del barrio en que crecí

(Las que quedan vivas) están trabajando en alguna construcción en Nueva York, es así como nos toca crecer a las hondureñas… conocemos el desarraigo y aunque es doloroso se convierte en la única forma de alimentar los sueños futuristas.

**Las mujeres Lencas son unas guardianas de La Esperanza** (así se llama el municipio donde tiene sus raíces), la lucha de ellas, por mantener las tierras ancestrales tiene orígenes antiguos como la misma Honduras. **La defensa de la tierra,** de los bienes comunes desde su cosmovisión es defender la vida, nacimos de la tierra y ahí es donde terminaremos es la conclusión de estas aguerridas mujeres que día a día transforman la tierra en alimento.

Desde que comencé mi camino hacia el feminismo, tuve la fortuna de conocer a estas compañeras LENCAS, indígenas campesinas y han vinculado la causa de la liberación de las mujeres con el desalambramiento de sus tierras originarias. La lucha histórica contra una economía de anclaje que no revira en ofrecer tierras, ríos y gente como mano de obra de excelente calidad porque culturalmente el trabajo es un valor sobrevalorado a conveniencia de cuanto empresario puede adquirirlo todo Bueno, Bonito y Barato.

Ahí vive la señora **Maria Santos Paz**, una mujer que se auto reconoce como lenca, conversando me comentó que ya han pasado catorce años desde que es parte de la **Red de Mujeres Contra la Violencia de Intibuca** y con una gran sonrisa en la cara me confirma que ella es una lideresa, que ella ha luchado porque las mujeres de su comunidad rompan este silencio y destruyan sus vínculos basados en la violencia patriarcal de su cultura, « Yo aquí soy una figura de lucha, me invitan a la televisión y les digo a las mujeres que no peleen entre ellas, que nosotras debemos estar unidas» siempre sonriendo me dijo, aquí a los hombres no les gusta que sus esposas hablen conmigo, pero eso a mí no me da miedo porque yo sé que no estoy haciendo nada malo y después muy seria dijo « vamos a seguir haciendo esto, usted »

**La criminalización del activismo feministas** y en general cualquier oposición a los intereses de quienes controlan el poder en estas zonas ricas en recursos naturales, ha dejado muchas mártires, **Berta Cáceres** una de ellas… la lista es larga y es una pregunta en muchas cabezas **¿cómo se defienden las defensoras?** La concentración de poder económico y de fuerza violenta anula cualquier actuación gubernamental, lo que facilita la corrupción y la impunidad.

Esto facilita la aplicación de 4 estrategias dirigidas a neutralizar la oposición contra los intereses que controlan el Estado:

   **  a. Estigmatización**: utilización de campañas de desprestigio, bien a nivel de opinión pública local o nacional desde el uso del discurso oficial (gobierno, academia, iglesias), como desde los medios de comunicación masiva.

 **\    b. Militarización**: uso de militares, policías, empresas privadas de seguridad y grupos con poder relativo para reprimir a las personas que defienden sus derechos.

 **\    c. Criminalización/Judicialización**: uso del derecho penal y del sistema de justicia como herramienta política para neutralizar opositores.

 **\    d. Actividad clandestina e ilegal:** uso de cuerpos ilegales y aparatos clandestinos para eliminar opositores, protegida esta situación por colusión e impunidad desde el Estado.

**¿Y dónde está el Gobierno?** Haciendo pactos y flexibilizando lo que sea necesario para permitir operaciones ilegales, de injusticia para hacer posible que este engranaje opere en total impunidad. Las mujeres indígenas son objeto de un sistema que las criminaliza por defenderse, se ensaña contra ellas por organizarse y formarse en el reconocimiento de sus derechos humanos y las discrimina al no permitirles acceder a la justicia.

Honduras es una **tierra hermosa**, sin embargo, la hostilidad con la que se nos enseña a las hondureñas y hondureños lo esencial de «cómo es la vida» y en esa lección tan básica siempre nos presentan el enfoque con dureza, maldad y sufrimiento. Así es la vida.

### Lídice Ortega

Feminista latinoamericana, migrante y madre por decisión, amada en casa soy coach y neurofacilitadora. humorista y comediante  cuando tengo tiempo, De-formación universitaria en el área de las ciencias políticas con experiencia en acciones de calle, resistencia organizada y desorganizada. A veces escribo y principalmente sobrevivo a la pandemia