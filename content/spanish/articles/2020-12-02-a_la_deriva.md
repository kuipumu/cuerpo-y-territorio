---
title: A la deriva
subtitle: " "
cover: /images/uploads/dead-trees-947331_1920.jpg
date: 2020-12-02T02:44:29.290Z
authors:
  - Jefersson Leal
tags:
  - Venezuela
  - TrinidadYTobago
  - Caribe
  - Latinoamerica
  - DDHH
  - Niños
  - Migración
  - Emergencia
  - MigraciónForzada
  - Xenofobía
  - CrisisHumanitaria
categories:
  - Cuerpo
comments: true
sticky: true
---


> *`Entre tu pueblo y el mío,`*
>
> *`hay un punto y una raya,`*
>
> *`la raya dice «no hay paso»,`*
>
> *`el punto, «vía cerrada».`*
>
> *`Y así, entre todos los pueblos,`*
>
> *`raya y punto, punto y raya,`*
>
> *`con tantas rayas y puntos,`*
>
> *`el mapa es un telegrama.`*
>
> *`Caminando por el mundo,`*
>
> *`se ven ríos y montañas,`*
>
> *`se ven selvas y desiertos,`*
>
> *`pero ni puntos ni rayas.`*
>
> *`Porque estas cosas no existen,`*
>
> *`sino que fueron forzadas,`*
>
> *`para que mi hambre y la tuya`*
>
> *`estén siempre separadas.`*
>
> **Anibal Nazoa**



**“Me voy” es quizás una de las expresiones más comunes hoy en día en Venezuela.** Las habituales fiestas de despedidas de hace unos años para dar el último abrazo a tíos/as, primos/as, hermanos/as y amigos/as del alma, ya son cosas del pasado, - eso también se lo llevo la crisis-; desde hace unos meses, ya solo se anuncia la partida, llega un mensaje al Whatsapp con el aviso. Una noticia que siempre duele, un dolor que se va registrando en los diarios del alma.

**Bien hemos aprendido los habitantes de este país, que la economía también exilia y de pronto te saca de tu casa, te quita a tus afectos y hasta te deja sin país.** Es que la profunda crisis social, económica y política que atraviesa Venezuela ha dejado a la población en una situación de orfandad. En una nación donde comer, vestirse, ir al médico, tener agua potable y electricidad son un verdadero lujo, la migración ha sido un método “efectivo” para poder salvarse del caos.

Quien se va, siempre encuentra los medios: en avión, en bus, a pie o hasta en lancha. Unos se van a vivir definitivamente a otras latitudes, en cambios otros son como un péndulo, van trabajan y vuelven. En cualquier caso, la partida siempre tiene riesgo, sobre todo si vas por “los caminos verdes”, por las trochas terrestres entre Colombia y Brasil. Hay otros/as que se aventuran a irse por mar a una de las Antillas más cercanas: Aruba, Curazao o Trinidad y Tobago.

Recientemente, se conoció un caso que refleja en múltiples aspectos la realidad de los migrantes venezolanos/as. **Un grupo de 25 personas, entre ellos 16 menores de edad, incluidos niños de 2, 4 y 8 años, se embarcaron en un “canoa” con motor, a la isla de Trinidad.** Según la información ofrecida por los portales digitales, los niños y niñas iban al encuentro con sus padres, quienes estaban en situación de migrantes desde hace algún tiempo en la nación caribeña.

Luego de haber llegado, por fin, a Trinidad y Tobago, las autoridades de la isla procedieron inmediatamente a deportar a los menores de edad junto con los adultos que los acompañada, en el mismo medio por donde llegaron: **en canoa.** Allí comenzó de nuevo la travesía. Pasaron 8 horas hasta que se conoció que el grupo de persona había logrado llegar a “salvo” a tierras venezolanas.

Por varias horas el grupo de migrantes permaneció a la deriva, entre el país que los expulso y el otro que no quería recibirlos. **Como metáfora del horror, la deriva de este grupo de venezolanos en el mar, representa en buena medida la metamorfosis en la cual se ha convertido el país petrolero.** Una nación errante, casi movida por el viendo y sus habitantes unos náufragos que resistente la inclemencia de la desolación. **La patria se convirtió en Saturno y día a día se devora a sus hijos.**

Lamentablemente, este no ha sido el único caso de venezolanos que se han visto en la obligación de estar sin rumbo fijo. En el mes de septiembre, 24 venezolanos fueron rescatados por autoridades de Chile y motoqueros en el desierto de Atacama, donde fueron abandonados por los “coyotes”, sin agua ni comida.

**El grupo trataba de cruzar el desierto más seco del planeta, donde ni las bacterias resisten.** Habían recorrido a pie durante tres días el lugar, cuando fueron encontrados por casualidad por un grupo de motoqueros que realizaban una ruta por el terreno. La zona que recorrían se conoce como la “Frontera X” un lugar con duras condiciones climáticas que fue minada por Augusto Pinochet. Uno de los voluntarios que ayudaron en el rescate cuenta: “si no hubiéramos llegado, ellos podrían haber perdido su vida”.

Es que son múltiples los riesgos a los cuales se ven expuestos los ya 4.6 millones de venezolanos que han abandonado forzosamente el país producto del profundo caos que vive la nación. Según La encuesta ENCOVI 2019 – 2020 **al menos el 19 % de los hogares en Venezuela reportó que uno de sus familiares salió del país como emigrante** entre 2014 – 2019.

Según refleja la misma encuesta la mayoría de la población que ha emigrado son fundamentalmente jóvenes entre 15 a 39 años de edad, en buena medida profesionales técnicos, licenciados y magister. **Esto le ha acarreado al país múltiples consecuencias, una de ella es la pulverización del bono demográfico;** este bono consiste en que la población más joven con edad de trabajar supera a la población menor de 15 años y a los adultos mayores, es decir la mayoría de la población esta activa económicamente y dispuesta a producir.

Estas ventajas demográficas que se dan solo una vez en la historia, resultan fundamentales para el mejoramiento del nivel de vida de la población. Pero dicha ventaja ha desaparecido trágicamente en Venezuela. La pirámide poblacional del país luce en el 2020 envejecida como tendría que lucir en el 2050, en 6 años el país se ha adelantado 30 años en niveles de envejecimiento. **Lo que produce mayor dependencia, menor producción de nacimientos y más mortalidad.**

Otra de las consecuencias del fenómeno migratorio venezolano son los episodios de xenofobias en los países receptores. Por ejemplo, la alcaldesa de Bogotá, Claudia López, acusó a los venezolanos del incremento de la inseguridad en la ciudad *“Yo no quiero estigmatizar, ni más faltaba, a los venezolanos, pero hay unos inmigrantes metidos en criminalidad que nos están haciendo la vida cuadritos”*. En el 2018, en la ciudad de Boa Vista, Brasil fue incendiada una casa que daba albergue a 31 refugiados de Venezuela.

En Ecuador un grupo de personas, llegaron a un pequeño edifico de departamentos donde vivían migrantes venezolanos, diciendo: *“Mátenlos, hijueputas, asesinos, les vamos a sacar la madre (…) Sáquenlo, sáquenlo para quemarlo”.*

**En la actualidad, los más de 4 millones de compatriotas que están repartidos por el mundo intentan diariamente salvarse, no perderse, mantenerse de pie ante las poderosas dificultades.** Mientras tanto, nos toca lo mismo a los que hemos decidido quedarnos, mantenernos en calma pese a la tempestad, seguir construyendo y hacer bien lo que hacemos, la salida no luce fácil, pero diariamente hay más movimientos, hay más reclamos, hay menos miedo.

\
**Fuente:**

`(1)` Indira Rojas, [Diez Claves sobre los 16 niños deportados en Trinidad y Tobago](https://prodavinci.com/diez-claves-sobre-los-16-ninos-venezolanos-deportados-en-trinidad-y-tobago/?platform=hootsuite)\
`(2)` [www.dw.com - Rescatan grupo de venezolanos abandonados en el desierto chileno](https://www.dw.com/es/rescatan-a-un-grupo-de-24-venezolanos-abandonados-en-el-desierto-chileno/a-55017043)

`(3)`Ídem

`(4)` [Cifra suministrada por ACNUR, citada en portal web del Banco Mundial](https://www.bancomundial.org/es/region/lac/brief/la-migracion-venezolana-mas-alla-de-las-fronteras)

`(5)`Encuesta ENCOVI 2019 – 2020 /Emigración internacional

`(6)` Ídem

`(7)` Ídem

`(8)`[runrun.es - Alcaldesa de Bogotá estigmatiza a venezolanos](https://runrun.es/rr-es-plus/428008/3-x-3-alcaldesa-de-bogota-estigmatiza-a-venezolanos-aunque-no-quiera/)

`(9)` [Notiamerica.com - Prenden fuego a casa de refugiados venezolanos ](https://www.notimerica.com/sociedad/noticia-prenden-fuego-casa-31-refugiados-venezolanos-interior-norte-brasil-20180209183626.html)

`(10) `José María León Cabrera,[ La xenofobia en Ecuador empuja a migrantes venezolanos a salir del país.](https://www.nytimes.com/es/2019/01/28/espanol/ecuador-ibarra-venezolanos.html)