---
title: La escritura, ese viento que empuja
subtitle: " "
cover: /images/uploads/la-caja-de-herramientas-introduccion-a-la-escritura-artes-y-humanidades-humanidades-y-literatura-banner_movil.jpg
caption: "Imagen: Uniandes"
date: 2021-02-24T20:29:12.997Z
authors:
  - Juan Manuel Barreto
tags:
  - Escritura
  - Inspiración
  - Arte
  - Expresión
  - GénerosLiterarios
categories:
  - Cuerpo
comments: true
sticky: true
---
`Escribir lo que se hace. Escribir lo que se busca.`

`Escribir lo que nunca va a encontrarse.`

`“La inundación”`, *Eugenia Almeida*

`El cuerpo es un texto que se resiste a la publicación.`

*Pina Bausch*

<!--StartFragment-->

**¿Podemos escribir aisladxs de todo, encerradxs en nuestras cápsulas, como si viviésemos en tiempos de no pandemia?**

¿Podemos pensar la escritura en pandemia como algo excepcional o escribir es un proceso que se construye, la mayoría de las veces, de forma individual, en un microcosmos que no coincide, necesariamente, con los tiempos e imposiciones del mundo exterior?

¿Es acaso la escritura, también, un acto social en el que precisamos de lxs otrxs como interlocutores para construir de manera colectiva ese arte para narrar historias?

¿Escribimos porque una fuerza vital nos impulsa a registrar aquello que deambula a nuestro alrededor, eso que nos conmueve y sacude, que deseamos sacarnos del cuerpo, o tan solo intentamos cubrir las demandas infiltradas por los fantasmas de la productividad?

¿Qué significa escribir en estos tiempos? ¿Qué razón de ser se revela durante la puesta en acto del oficio?

Lejos de buscar respuestas concluyentes, me interesa pensar estas preguntas como guía y estas líneas como una exploración, una excavación, un acercamiento que, respondiendo a una esencia de la escritura, **se vincule más con una búsqueda que con un resultado.**

Recurro a ideas, reflexiones, sentires que otrxs han transitado y compartido, voces que resuenan en relación a la escritura, pero, también, a la lectura. Porque, sin dudas, ambas experiencias se sostienen mutuamente, se retroalimentan, se enriquecen, se tornan indisolubles.

#### La soledad y el tiempo

*“Comprendí que yo era una persona sola con mi escritura, sola muy lejos de todo”*, dice Marguerite Duras al comienzo de Escribir. Y luego: *“Siempre he llevado mi escritura conmigo, dondequiera que haya ido” (…) “La soledad de la escritura es una soledad sin la que el escribir no se produce…” (…) “Hallarse en un agujero, en el fondo de un agujero, en una soledad casi total, y descubrir que solo la escritura te salvará”*. `(1)`

Duras no concibe su vida sin la escritura. No hay contraposición entre ambas. La segunda potencia la primera, la sostiene, le brinda un sentido.

Para Eugenia Almeida, *“conseguir momentos para escribir no se relaciona tanto con la soledad sino con el tiempo. Con el excesivo uso del tiempo que hay que hacer para sobrevivir económicamente. Mi pelea es ésa: cómo conseguir más tiempo para escribir. Hay quien trabaja para comprar viajes, ropa, teléfonos, una casa. Yo trabajo para comprar tiempo. Tiempo para escribir: tiempo de felicidad, una especie de tiempo multiplicado”*. `(2)`

**Conciliar los tiempos dedicados a la supervivencia económica con los tiempos para escribir expone una cuestión difícil de eludir**, una barrera objetiva, material -producto de un sistema que abruma- por la que navegan quienes desarrollan el hábito de la escritura.

#### ¿Para qué escribir?

Stephen King recorre en "Mientras escribo" sus inicios y su formación como escritor. En 1954, a sus siete años, pasa la mayor parte del tiempo en cama o sin salir de su casa a raíz de una enfermedad en la garganta.

 Aprovecha ese tiempo para leer y copiar en su libreta algunos pasajes de libros agregando, cada tanto, algunas líneas de su creación. Cuando le muestra esos escritos a su madre, ella primero se sorprende, se muestra orgullosa del talento de su hijo, pero luego le pregunta si son de su autoría. 

Él reconoce que ha transcripto la mayor parte y su madre, al parecer algo decepcionada, lo invita a escribir su propio relato. King recuerda *“haber acogido la idea con la sensación abrumadora de que abría mil posibilidades, como si me hubieran dejado entrar en un edificio muy grande y con muchas puertas cerradas, dándome permiso para abrir la que quisiera. Pensaba (y sigo pensando) que había tantas puertas que no bastaba una vida para abrirlas todas”.* `(3)`

**La escritura ligada a la libertad para habitar territorios seguros e inestables, conocidos y extraños; para abrir puertas**. La escritura entreverada con la alteridad, como una tensión entre la identidad y la diferencia, entre lo propio y lo ajeno. Escribir como una forma de revisar significados, de pensar las otredades, de jugar a ser otrxs.

Fabio Morabito afirma que *“sólo debería escribirse para paliar alguna carencia de lectura. Ahí donde advertimos un hueco en nuestra biblioteca, la falta de cierto libro en particular, se justifica que tomemos la pluma para, de la manera más decorosa posible, escribirlo nosotros. Escribir, pues, como un correctivo. Escribir para seguir leyendo”*. `(4)`

Morabito nos propone escribir lo que nosotrxs sentimos que falta, lo que nos gustaría leer, lo que no está escrito, aquello que permitiría establecer una relación con un vacío o con una ausencia (exterior e interior).

#### El arte de contar historias

Para Ricardo Piglia (escritor, crítico literario) -alguien infaltable a la hora de merodear la escritura y su oficio- es posible reconocer, desde los orígenes de los tiempos, dos modos diferentes de narrar: quien narra lo que conoce y quien narra con el afán de conocer.

Por un lado, el relato como un viaje: alguien que ha decidido visitar un lugar nuevo y, al volver, cuenta esa travesía a sus amigxs y familiares. *“No hay viaje sin narración”*, comenta Piglia; *“se viaja para narrar”* `(5)` -agregaría: y se narra para viajar-.

Por el otro, en una línea paralela y complementaria, se narra para comprender, para desentrañar, para descifrar, **para intentar reconstruir una historia cifrada a partir de determinados indicios y elementos disponibles**; como una suerte de adivinx que lee signos.

En la contratapa del libro Turistas `(6)`, aparecen dos elocuentes apreciaciones en torno a la figura de Hebe Uhart que son, al mismo tiempo, una foto de la gran escritora argentina y una toma de posición frente a la escritura.

Por un lado, el escritor y académico Martín Kohan condensa la mirada narrativa de Hebe Uhart, citándola: *“¿Quién dictamina qué cosas son mínimas o máximas? No hay jerarquía de lo que es importante para escribir. La importancia la da el que escribe”*.

Elvio Gandolfo, por su parte, en su descripción de la narradora esboza, asimismo, una forma de entender el arte de la narración: *“Hebe Uhart se ubica entre aquellos donde un “modo de mirar” segrega un “modo de decir”*, un estilo. La mirada de la autora ve algo, y en la búsqueda del mejor modo de ponerlo en palabras, va construyendo un articulado, discreto lenguaje propio, que no se impone a lo percibido, sino que se origina en ese mundo”.

Un modo de decir donde el lenguaje y la palabra no se imponen al mundo, más bien, parten de él, colocándose al servicio de la experiencia.

Liliana Villanueva -arquitecta y periodista que realizó talleres de escritura con Hebe Uhart.- fue tomando apuntes a lo largo de los años y, luego, sistematizó todas esas enseñanzas en un libro que no solo recoge la voz de Uhart sino que nos invita a pensar cuan significativos e indispensables son esos espacios colectivos de construcción, intercambio y disfrute.

Dice Uhart: *“Todo arte es el arte de escuchar. Cuanto más miro, más salgo de mi prejuicio. Es difícil mirar lo real sin postergar el juicio, pero para escribir es necesario hacerlo”. (…) “¿Por qué hacemos juicios rápidos? Porque nos da angustia mantenernos en la duda. Para escribir el juicio rápido no sirve”.* `(7)`

Flannery O´Connor –otra magistral escritora- arroja más luz sobre el tema: *“Enseñar cualquier tipo de escritura es, primordialmente, ayudar al aprendiz a desarrollar el hábito del arte. Creo que el arte es mucho más que una disciplina, aunque de hecho también lo sea; creo que es un modo de mirar al mundo creado, y de usar los sentidos de modo que éstos puedan encontrar en las cosas tantos significados como sea posible”*. `(8)`

#### Decir

*“Decir es un acto potente. Crea o destruye mundos. Hace crecer la semilla, hace llover, trae o aleja al mundo, a los vientos. Decir es un acto potente. Trae a la vida y abre la senda al país de los muertos. Cuando la palabra es verdadera, cuando ha sido fortalecida en el interior de nuestro corazón, cuando ha crecido en el silencio”*. `(9)`

#### Lo que está oculto

**¿Qué no hubiéramos descubierto si no hubiéramos escrito? ¿De qué nos hubiéramos perdido?**

Escribir es arrimarse a lo desconocido que llevamos dentro.

Otra vez Duras: *“Si se supiera algo de lo que se va escribir, antes de hacerlo, antes de escribir, nunca se escribiría. No valdría la pena”*. `(10)`

#### Escritura y cuerpo

*“La escritura se hace con el cuerpo”*, dice Almeida. *“Es un movimiento, un desplazarse en el espacio, un hacer. Y sin embargo, cuando hablamos de la escritura hablamos del resultado, de lo ya hecho, de lo escrito, de algo fijo que ya tomó forma. A mí lo que me interesa es deambular en torno a la idea de escritura en sí, el acto, la disposición del cuerpo, lo que pasa en ese momento. No después. La mirada a posteriori siempre deforma algo de la experiencia”*. `(11)`

Hay una creencia, sumamente instalada, que **supone que leer y escribir son meros actos del intelecto**, sin considerar que ambas prácticas están enmarcadas en un contexto determinado y son llevadas adelante desde un cuerpo y un territorio.

Resulta vital apropiarse de la escritura y la lectura, no solo como de una cierta cantidad de libros apilados en una biblioteca sino como de un acontecimiento, de un momento en el que se pone en juego una visión de la vida y de las cosas. De una forma de estar disponibles y abiertos, de aproximarnos al mundo y de construir conocimiento. **De transitar lo que sucede con unx mismx, con las relaciones, con las identidades, con las formas en que miramos.** De un lugar, de un territorio, de un estado.

Según Guillermo Saccomano, *“ningún escritor puede dar cuenta del todo de lo que ese texto puede decir para los otros, que seguramente le encontrarán más de un sentido diferente al que pueda haber imaginado su autor. Los textos siempre traicionan a sus responsables. Lo que se quiso decir no es lo que el texto habrá de decir”. (…) “Sin embargo, puedo contar quien creo haber sido yo durante esa escritura, no lo que pretendí lograr sino las condiciones de producción de la misma escritura”*. `(12)`

Separar la escritura de los azares, interpretaciones y efectos que sobrevendrán con el texto. **Escribir aunque eso no se publique o no vaya a parar a ningún lado.** Escribir sin que sea para llenar una medida. Escribir para explorar, para experimentar con nosotros mismxs.

#### La pulsión y el deseo

Al principio de la cuarentena, daba la sensación que cualquier posibilidad de relato parecía orientarse a la pandemia. Sin embargo, luego de un tiempo, la situación fue naturalizándose y formando parte de nuestras rutinas.

La reflexión acerca del sentido de escribir o no en este contexto precisó, entonces, de una mutación: ¿puede no escribirse en alguna circunstancia si la escritura irrumpe como una expresión del deseo, como una pulsión?

Pareciera que cuando la escritura se vincula con una necesidad o deseo y no, con una deuda, **la circunstancia específica queda desplazada por esa fuerza que nos impulsa a escribir**.

El deseo como lo único que el sujeto no puede transferir, aquello que desnuda su verdadera ética. La vida como el despliegue de las formas propias del deseo.

La escritura no reducida a una labor intelectual; un hacer experimentado en el cuerpo, un ejercicio, un hábito, un deseo que se sostiene a pesar de las dificultades, un dejarse ir, una búsqueda antes que un resultado, algo que se escapa, algo trascendente, un suceso, un asomarse a lo desconocido, un ida y vuelta entre lo propio y lo ajeno, una comunión, un juego, **una historia que nos contamos del mundo, un modo de nombrar lo que ya no está.**

**Referencias**

1. Marguerite Duras, Escribir. Barcelona, 1994.
2. [Lo que habitualmente llamamos autobiográfico es pura ficción](<1. https://www.eternacadencia.com.ar/blog/contenidos-originales/entrevistas/item/lo-que-habitualmente-llamamos-autobiografico-es-pura-ficcion.html>)
3. Stephen King, Mientras escribo. 2004.
4. Fabio Morabito, El idioma materno. Madrid, 2014.
5. Ricardo Piglia, Modos de narrar. Universidad de Talca , 2005.
6. Hebe Uhart, Turistas. Buenos Aires, 2008.
7. Liliana Villanueva, Las clases de Hebe Uhart (Blatt & Rios). Buenos Aires, 2015.
8. Flannery O´Connor, El arte del cuento.
9. Diana Bellesi, El poder mágico de la palabra.
10. Op. Cit
11. [](https://www.diariohuarpe.com/nota/eugenia-almeida-la-escritura-se-hace-con-el-cuerpo-es-un-desplazarse-en-el-espacio--2019102315460)[Eugenia Almeida: La escritura se hace con el cuerpo, es un desplazarse en el cuerpo.](https://www.diariohuarpe.com/nota/eugenia-almeida-la-escritura-se-hace-con-el-cuerpo-es-un-desplazarse-en-el-espacio--2019102315460)
12. [Soy la peste](https://www.pagina12.com.ar/315074-soy-la-peste)