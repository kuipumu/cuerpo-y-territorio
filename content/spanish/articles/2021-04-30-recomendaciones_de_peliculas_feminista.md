---
title: Recomendaciones de películas feministas
subtitle: "Cine que inspira al movimiento feminista "
cover: /images/uploads/portada.jpg
date: 2021-04-30T21:20:28.049Z
authors:
  - Yuliana Fuentes Fuguet
tags:
  - Cine
  - Mujeres
  - Genero
  - Feminismo
categories:
  - Cultura
  - Genero
comments: true
sticky: true
---
El cine como herramienta artística contribuye a construir un mundo más justo e igualitario. Desde Agnès Varda, Chantal Ackerman, Jane Campion, el mismo Pedro Almodóvar entre muchos otros nombres conocidos en el séptimo arte, han desarrollado historias libres de prejuicios o estereotipos, que hablan de las mujeres como lo que son, como lo que somos todas y todos: personas complejas con historias propias y autónomas.

Aprovecho la ocasión para compartirles un seleccionado de algunos títulos cinematográficos que inspiran al movimiento feminista y que tocan temas de equidad de género y racismo. Películas que son un modo de concienciación para la sociedad, sobre todo para aquella parte que aún ignora los principios del feminismo, los rechaza y siguen atrapados en el heteropatriarcado.

### Women without men (2009)

![](/images/uploads/women-without-men-1.jpg)

Adaptación de la novela de la iraní Shahrnush Parsipur, esta película alemana nos lleva al Irán de los años 50, momento en el que un Golpe de Estado desata el caos. Allí, cuatro mujeres se encontrarán en un refugio y lanzarán con su convivencia interesantes mensajes de sororidad en un entorno sin hombres. El papel de la mujer en esta época se revela aquí con toda su amargura, a la vez que se elabora un acertado retrato histórico.

### Jeanne Dielman, 23 quai du Commerce, 1080 Bruxelles (1976)

![](/images/uploads/jeanne_dielman.jpg)

Es posiblemente la gran obra de la cineasta Chantal Akerman, una interesantísima directora de cine belga que experimentó con la imagen a través de una mirada profundamente femenina y feminista. En este film de 200 minutos de duración -ahí es nada- narra a monótona vida de una madre viuda que ejerce de ama de casa por las mañanas, y de prostituta por las tardes.

### Persepolis (2007)

![](/images/uploads/persepolis.jpg)

Marjane Satrapi dirige esta adaptación de su novela gráfica -basada, a su vez, en su propia vida- que se ha convertido en una de las películas de animación más rebeldes de este siglo. Al mismo tiempo que usa de telón de fondo la revolución islámica, Satrapi nos cuenta la historia de su infancia y adolescencia, en la que tuvo que convivir con la intolerancia y la represión mientras se declaraba fan absoluta del punk.

### La eterna noche de las doce lunas (2013)

![](/images/uploads/descarga.jpg)

Dirigida por la colombiana Priscila Padilla, este film cuenta la historia de como durante doce lunas (un año), que fue el tiempo que permaneció encerrada la niña indígena Wayuu, Fila Rosa Uriana, La llegada de su primera menstruación marcó su entrada. En este largo periodo de aislamiento la pequeña Fili, fue sometida a unos rituales indígenas propios de esta cultura. Aprender a ser mujer en su soledad, es el gran objetivo de este rito ancestral milenario.

### Thelma & Louise (1992)

![](/images/uploads/20419610.jpg)

La película feminista 'mainstream' por excelencia, y una maravillosa aventura de dos mujeres en busca de la libertad. Susan Sarandon y Geena Davis escapan de las tareas domésticas, los hombres sexistas y las realidades infelices para empezar a escribir su propia historia.

### Una canta, la otra no (1977)

![](/images/uploads/una_canta_la_otra_no.jpg)

La cineasta Agnès Varda escribe y dirige este potente manifiesto feminista y pro-abortista, en el que dos mujeres comparten a través de los años una amistad inquebrantable.

### Born in flames (1983)

![](/images/uploads/born_in_flames_kanopy.jpg)

Esta distopía ochentera de Lizzie Borden nos lleva a unos Estados Unidos dominados por un gobierno más socialista, pero que -como los anteriores- sigue ignorando las minorías. Un grupo de mujeres intentará dar la vuelta a la situación. Una historia heredera de la segunda ola del feminismo, y una joya del séptimo arte que vale la pena revisitar.

### A question of silence (1982)

![](/images/uploads/descarga_1_.jpg)

Escrita y dirigida por Marleen Gorris, es un estudio de los comportamientos extremos y la violencia, pero sobre todo de cómo ha de reaccionar una mujer ante la constatación de una sociedad completamente machista. Uno de los alegatos feministas cinematográficos más desconocidos e interesantes de la historia.

### Gente de pelo duro (2010)

![](/images/uploads/gentedepeloduro.jpg)

Gente de pelo duro es un documental de opinión, que aborda el tema del pelo afro a través de los testimonios, sobretodo de mujeres, en diferentes partes del mundo: Guinea Ecuatorial, España, Uruguay, Cuba y Estados Unidos. El filme busca crear un puente entre África y su diáspora en torno a la identidad, abordando así un problema común con el que viven africanos y afrodescendientes.

### Orlando (1992)

![](/images/uploads/orlando-856274799-mmed.jpg)

La cineasta Sally Potter adapta la novela homónima de Virgina Woolf, un alegato feminista en un entorno fantástico que conquistó, entre otros, el Festival de Sitges de aquel año. Tilda Swinton interpreta a una ¿mujer? que atraviesa las fronteras del tiempo y el espacio, cambiando de género y apariencia, para reflexionar sobre el arte y la vida sumida en una constante melancolía.

### La princesa Mononoke (1997)

![](/images/uploads/princesa_mononoke.jpg)

Si de algo se caracteriza Hayao Miyazaki, además de sus alegatos medioambientales y su sensibilidad casi cósmica, es por construir personajes femeninos fuertes e independientes. Así es Nausicaa, así es Sophie ('El castillo ambulante'), así es Ponyo, y así es sin duda la princesa Mononoke: una princesa lobo que lucha cuchillo en mano contra la destrucción de su hogar.

### Sweetie (1989)

![](/images/uploads/sweetie.jpg)

De nuevo Jane Campion aparece en esta lista para demostrar lo mucho que tiene que decir sobre las mujeres y su encaje en un mundo dominado por hombres. Fue su debut en el largometraje, y un estudio afilado sobre las relaciones humanas, las inseguridades y el amor.

### Tomates verdes fritos (1991)

![](/images/uploads/tomates_verdes_fritos.jpg)

Este clásico de los 90, adaptación de la novela de Fannie Flag, nos lleva de nuevo a historias de mujeres, de amistad y amor, pero también de infelicidad, inseguridades y tristeza. Su retrato femenino llegó a una gran audiencia en Estados Unidos, demostrando que las historias de mujeres son las historias de todos.

### Todo sobre mi madre (1999)

![](/images/uploads/todo_sobre_mi_madre.jpg)

Algo que siempre ha caracterizado a Pedro Almodóvar es su habilidad para retratar la psique femenina. Su gusto por las historias de mujeres. Esta película, una de las más célebres de su filmografía, no es una excepción: la pérdida, el dolor, los recuerdos y la soledad de expanden en la soberbia interpretación de Cecilia Roth.