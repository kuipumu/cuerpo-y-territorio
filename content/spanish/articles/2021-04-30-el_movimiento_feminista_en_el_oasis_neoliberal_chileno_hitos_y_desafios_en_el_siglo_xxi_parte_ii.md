---
title: "El Movimiento Feminista en el “oasis” neoliberal chileno: hitos y
  desafíos en el siglo XXI (Parte II) "
subtitle: "Una mirada actual "
cover: /images/uploads/foto_historicas_creditos_equipo_audiovisual_cf8m.jpg
date: 2021-04-30T15:54:44.520Z
authors:
  - Constanza Vega Neira y Jessabel Guamán Flores
tags:
  - Feminismos
  - Mujeres
  - Genero
  - Movimiento
  - Chile
categories:
  - Genero
  - Cuerpo
  - Territorio
  - Cuerpo y Territorio
comments: true
sticky: true
---


### ¿Cómo caracterizar al movimiento feminista de Chile en el siglo XXI?

Comenzaremos planteando que el movimiento ha tenido un carácter diverso y transgeneracional. Dicha diversidad asociada al contexto de la emergencia de las disidencias, del mundo popular o de las migraciones, y es posible reconocer en ellas la participación activa de adolescentes, jóvenes, adultas y adultas mayores, aunque sin duda las estudiantes universitarias prendieron la mecha ese 2018 tras las tomas de espacios y performances callejeras. Para el desarrollo de esta caracterización, daremos cuenta de algunas demandas e hitos claves que han movilizado a las feministas en esta tierra del cono sur americano:

1. Reconocimiento de las sobrevivientes a la **violencia política sexual** perpetrada en dictadura (articular memoria-feminista)
2. **Lucha por la despenalización del aborto** (situación actual aborto tres causales y la objeción de conciencia)
3. **Denuncia de acoso y abuso sexual** en espacios universitarios y públicos. Demandas y creación de protocolos
4. El fenómeno global de las tesis
5. **Feminicidios** (red de mujeres contra la violencia)
6.  La emergencia de las disidencias y el feminismo popular
7. “Por una constitución feminista” proceso constituyente y el cuestionamiento interno del movimiento (coordinadoras feministas en lucha) sobre la representatividad

En primer lugar, se ha visibilizado la temática sobre la violencia político sexual aplicada en el ejercicio del Terrorismo de Estado de la dictadura cívico-militar por las agrupaciones de mujeres sobrevivientes, y los procesos de recuperación de memorias donde se inscribe el informe Valech (2004). La historiadora Ximena Goecke (2019) nos devela que recién el año **2016 se tipificó la violencia sexual como forma de tortura**, y se diferenció de la violación a los derechos humanos hacia hombres y mujeres.

Algunas de estas mujeres sobrevivientes en el periodo postdictadura se aproximaron al feminismo (https://cutt.ly/LvIsZ0n) y se han organizado con un discurso que ha sido incorporado por las nuevas generaciones de feministas, reconociendo en ellas, a referentes de lucha como sujetas políticas contraponiendo las funciones que a las mujeres “les correspondía” tener en el cuidado del hogar y formación de los hijos según el ideal reforzado por el régimen. Este tema cobra especial sentido considerando las denuncias sobre abuso sexual y violaciones cometidas por agentes del Estado durante la revuelta social de 2019 (https://cutt.ly/KvIdRls).

En este proceso de visibilidad de las temáticas que afectan a las mujeres históricamente se encuentra **el aborto** que ha sido una de las premisas del movimiento feminista ayer y hoy, y en la actualidad se enmarca en tres consigas: **aborto libre, gratuito y seguro**. Esto porque Chile es uno de los países en que el aborto es considerado un delito desde 1989. Desde los años noventa y desde inicios del siglo XXI, muchas organizaciones feministas lucharon por revertir esta situación, siendo en el gobierno de la presidenta Michelle Bachelet bajo la ley 21.030 promulgada el 14 de septiembre de 2017 permitido en tres causales:

a) **inviabilidad del feto**, esto quiere decir que, si el feto tiene una enfermedad congénita letal, y solo si más de un médico determina estas características, es posible la irrupción del embarazo, no tiene límite de edad gestacional.

b) **riesgo de muerte de la mujer**, que el embarazo ponga en riesgo a la mujer y por ende es necesaria su interrupción con una orden de un médico que avale que la mujer está en peligro de morir y no tiene límite de edad gestacional.

c) **embarazo por violación**, en esta situación según la ley se respetará la voluntad de la mujer, no obstante, debe estar respaldada por un equipo médico en relación al relato de la mujer y la edad gestacional (https:/ [www.minsal.cl/informacion-para-la-ciudadania/](http://www.minsal.cl/informacion-para-la-ciudadania/)).

El avance logrado sobre esta ley se logró tras un arduo debate al interior de la sociedad chilena (organizaciones feministas, sociedades cientíﬁcas, academia, políticos), evidenciando posturas aún demasiado conservadoras en un porcentaje no menor de la población. Aquellas posturas conservadoras encontraron un eco de apoyo en términos de respaldo jurídico: se abrió paso a lo que se denominó objeción de conciencia, entendida como un derecho para que el/la profesional de la salud se niegue a interrumpir el embarazo (https:/ cutt.ly/nvIhqNT). Hasta la actualidad la decisión de las mujeres sobre sus cuerpos puede legalmente ser obstaculizado, quedando pendiente la lucha por un aborto libre, seguro y gratuito.

Durante el 2017 el movimiento feminista nuevamente puso en el debate público otra de las grandes temáticas en contra de las mujeres: **el acoso, abuso y violencia de género**. Especíﬁcamente el debate estalló dentro de las universidades chilenas, tras las denuncias de estudiantes contra académicos que eran sus profesores de cátedra (https:/ www.latercera.com/nacional/noticia/acoso-sexual-investigan-132-cas os-16-universidades/252893). A ﬁnales de ese año las jóvenes habían estado organizándose, ensayando y elaborando protocolos contra estas enraizadas malas prácticas, y llevando a cabo las llamadas “funas” o denuncias públicas. 

![](/images/uploads/foto_agencia_uno_.jpg)

Un ejemplo de esta acción coordinada de feministas universitarias se dio con especial revuelo en el mundo académico durante las XXII Jornadas de Historia de Chile en la ciudad de Valdivia, donde se irrumpió en una charla de un profesor acusado de acoso sexual. En ese mismo escenario, surge la Red de Historiadoras Feministas de Chile tras una reunión donde se abordaron estas problemáticas y la necesidad de visibilizar la disparidad de oportunidades para las mujeres en estos espacios. La RHF actualmente se encuentra en un proceso de defensa tras la publicación de una columna donde se reﬂexiona críticamente la adjudicación de fondos a proyectos de académicos vinculados a acoso sexual ([http://redhistoriadorasfeministas.cl/2021/03/12/carta-publica-sobre-querella-criminal-por-injurias-co ntra-la-red-de-historiadoras-feministas/](http://redhistoriadorasfeministas.cl/2021/03/12/carta-publica-sobre-querella-criminal-por-injurias-co%20ntra-la-red-de-historiadoras-feministas/)).

Esto evidencia que el tema sigue más vigente que siempre y también que aún es una demanda del movimiento feminista, ya que en varias instituciones aún esperan por ello en el 2021.

Sin duda ese fue el impulso para el movido mayo feminista del 2018, por las nuevas reivindicaciones y temáticas que las mujeres expusieron e interpelaron a la sociedad chilena con un estilo nunca antes visto. Y en ese nuevo estilo emerge un colectivo de la ciudad de Valparaíso, llamado **Las Tesis,** cuya performance **“un violador en tu camino”** realizada en debut simbólicamente frente al Palacio del Tribunal de Justicia en 2019 dio la vuelta al mundo y se viralizó en redes sociales cada versión en diferentes idiomas. Esta performance es un himno de protesta que increpa la institucionalidad del sistema patriarcal: “a los pacos (policía), los jueces, el Estado, el presidente” porque el **“Estado opresor es un macho violador**”. Se sumaron mujeres de todas las edades a esta performance callejera y especial fue la convocatoria a mujeres sobrevivientes de la dictadura frente al ex campo de prisioneros en el Estado Nacional: lo transgeneracional del movimiento se evidencia en esta puesta en escena: todas han vivido la opresión del patriarcado (Lerner, 1985), hay un continuo histórico.

El himno de Las Tesis signiﬁcó una especie de catarsis, fue una performance realizada por millones de mujeres en todo el mundo (https:/ cutt.ly/8vIn5Ho) dando cuenta que todas vivimos la cotidianidad de estas violencias del patriarcado, siendo la violación y el femicidio una constante. En Chile desde el comienzo de la pandemia del COVID 19 hasta hoy, hay más de 100 feminicidios constatados.

(http:/ [www.nomasviolenciacontramujeres.cl/registro-de-femicidios/](http://www.nomasviolenciacontramujeres.cl/registro-de-femicidios/)) siendo una problemática lejos de acabar y donde los mecanismos institucionales no son eﬁcaces en resguardar la vida de las mujeres.

La emergencia de las disidencias sexuales (Gómez, 2019) se ha incorporado en cierto grado al movimiento feminista pero no del todo dado que existen divergencias respecto a demandas o por diferencias en las prioridades de dichas demandas. Esto tiene relación con la representativad del movimiento feminista enunciado en la reseña, pero, de todos modos, la visibilidad de las lesbofeministas y sus aportes al movimiento son indiscutibles (Pisano, 2015). Dicha visibilidad también ha signiﬁcado identiﬁcar y denunciar en los crímenes feminicidas el lesbo-odio, en un país cuya homofobia persiste. Han aportado a este movimiento surgido en la cuna del neoliberalismo las perspectivas descolonizadoras; las realidades de las mujeres migrantes y la rearticulación de feminismos de base o feminismo popular (Hiner, 2019; Fabbri, 2018), destacando la organización de las ollas comunes como acción de resistencia histórica a las crisis del capital.

Por último, no podemos dejar al margen los avances respecto a la demanda de una **pedagogía Feminista** (Korol, 2007) y educación no sexista, urgentes para avanzar en ese mundo soñado por las feministas donde las lógicas del poder patriarcal sean erradicadas, lógicas que afectan a toda la humanidad y no sólo a las mujeres (Segato, 2018).

Todo lo que hemos planteado en este esbozo del movimiento feminista de Chile da cuenta de los desafíos presentes para el mismo, y esos desafíos se articulan en el camino que se ha abierto tras el estallido social con el proceso constituyente (Brito, 2020) : pese a que existan perspicacias en torno a la representatividad del mismo, es necesaria la presencia de las mujeres en este proceso que está por vivir Chile, no quedar ausentes, y pensar en una nueva Carta Fundamental donde las demandas feministas de todas las mujeres y disidencias ,sin distinción de clase y raza (Davis, 1981) , sean oídas, avaladas y construidas por todes nosotres para acabar con el patriarcado y sus violencias históricas, físicas, psicológicas y simbólicas.

 **¡Que todo el territorio se vuelva feminista!**

**Referencias** 

COLECTIVO Catrileo+Carrión(comp.), Torcer la palabra. Escrituras obrera-feministas,(2018)

CORVALÁN M, Luis, Del anticapitalismo al neoliberalismo en Chile. Izquierda, Centro y derecha en la lucha entre los proyectos globales. 1950-2000, (2018)

ELTIT, Diamela, Crónica del sufragio femenino en Chile, (2018)

GÁLVEZ, Ana, Historia del movimiento feminista en Chile en el siglo XX y su quiebre en la postdictadura chilena (1990-2010), En: “Transiciones. Perspectivas historiográﬁcas sobre la postdictadura chilena, 1988-2018”, PONCE, José Ignacio; Pérez; Aníbal y ACEVEDO, Nicolás, (2018).

GALVEZ, Ana, HINNER, Hillary, TORO, María Estela, LOPEZ, Ana, CERDA, Karelia, ALFARO, Karen, BARRIENTOS, Panchiba y INOSTROZA, Gina, Históricas: Movimientos Feministas y de mujeres en Chile, 1850-2020, (2021)

GREZ, Sergio y Foro por la Asamblea Constituyente, Asamblea Constituyente. La alternativa democrática para Chile, (2015)

HUNEEUS, Carlos, La Guerra Fría Chilena. Gabriel González Videla y la Ley Maldita,(2009)

KIRKWOOD, Julieta, Ser política en Chile. Las feministas y los partidos, (1986)

LAGOS, MANUEL, El anarquismo y la emancipación de la mujer en Chile (1890-1927),(2017)

LAVRIN, Asunción, Mujeres, Feminismo y cambio social en Argentina, Chile y Uruguay, 1890-1940, (2005)

LERNER, Gerda, La creación del patriarcado, (1985)

LIDID, Sandra y MALDONADO, Kira (Ed), Movimiento Feminista Autónomo, (1997)

MEMCH, ¿Qué es el Memch?: qué ha hecho el Memch, (1938)

MONTERO, Adela, VERGARA, Jorge, RÍOS, Mauricio y VILLAROEL, Raúl: La objeción de conciencia en el debate sobre la despenalización del aborto por tres causales en Chile. En: “Rev. chil. obstet. ginecol. vol.82 no.4, 350-360. (2017)

MOULIÁN, Tomás, Chile actual: anatomía de un mito, (1997)

SARTORI, Gioviani, Partidos y sistemas de partidos, (1980)

VEGA, Constanza, La Guerra Fría y su impacto en la Federación Chilena de Instituciones Femeninas: política y anticomunismo en la lucha por el voto, 1944-1949”, I Congreso Red Historiadoras Feministas, (2018)

VEGA, Constanza; Alvarado María José, El encuentro de la divergencia: las disputas entre feministas autónomas y feministas institucionales en el pasado reciente. Cartagena, 1997, VI Jornadas de Historia Regional y Social Cehycso, (2021)

BERMUDEZ, Angel (2019): Protestas en Chile. “La tortura, los malos tratos en comisarías y la violencia con connotación sexual son preocupantes” https:/ www.bbc.com/mundo/noticias-america-latina-50178678

MINISTERIO DE SALUD (2018) Ley que despenaliza la interrupción voluntaria del embarazo en 3 causales. Información para la ciudadanía https:/ www.minsal.cl/informacion-para-la-ciudadania/

MUÑOZ, Daniela (2018): Acoso sexual: investigan 132 casos en 16 universidades. https:/ www.latercera.com/nacional/noticia/acoso-sexual-investigan-132-casos-16-u universidades/252893/