---
title: Pedro Lemebel, una memoria desde los márgenes
subtitle: "  "
cover: /images/uploads/pedro.jpg
caption: "Ilustración: Juan Pablo Ojeda @dibujosdelcosmos"
date: 2020-09-15T19:36:01.355Z
authors:
  - Alexis Moreira
tags:
  - Diversidad
  - Chile
  - DerechosHumanos
  - Dictadura
  - PedroLemebel
  - LGBTIQ
  - AugustoPinochet
  - SalvadorAllende
categories:
  - Diversidad
  - ""
comments: true
sticky: true
---
<!--StartFragment-->

Los primeros días de septiembre han estado cargados de nostalgia. Seguro debe ser así para la gran mayoría de lxs chilenxs que, por distintos motivos, nos encontramos en el exterior. Y no lo digo porque el 18 de este mes se conmemoran las fiestas patrias (¿qué es eso a lo que llaman “patria”?), sino que lo hago porque septiembre guarda la que para mí es la fecha más dolorosa de su historia reciente: el 11 de septiembre de 1973 fue el golpe de Estado que derrocó al gobierno de Salvador Allende. En su lugar una dictadura cívico militar, encabezada por el general Augusto Pinochet y en complicidad directa con el gobierno estadounidense, se hizo del poder por 17 años.

**La dictadura de Pinochet con el monopolio de las fuerzas armadas y de inteligencia, instauró políticas de persecución, detención, exilio, tortura, censura y desaparición de personas ligadas en su mayoría a agrupaciones políticas de izquierda.** Junto con ello, se implantó por primera vez en el mundo un experimental modelo económico, el neoliberalismo, el cual se mantiene sin mayores modificaciones hasta nuestros días y que propicia las violentas desigualdades sociales que se evidencian en el país.

Las violaciones a los derechos humanos fueron sistemáticas en los distintos ámbitos de la vida de lxs chilenxs. Según el Instituto Nacional de Derechos Humanos las víctimas de la dictadura superan las 40.000 personas, de las cuales más de 3.000 corresponden a muertxs y desaparecidxs.Junto con lo escalofriante e indignante que significa esto, 47 años después algunas interrogantes me inquietan y me interpelan de manera directa, **¿qué pasó con las comunidades LGBTIQ durante la dictadura?, ¿existió una política específica de persecución y represión contra los cuerpos e identidades disidentes?**

Comienzo a indagar con cierto temor. Ese temor propio de quien se acerca a conocer una verdad que ya intuye. Pero ese temor se diluye y se convierte en incertidumbre al darme cuenta de que no existe una estadística oficial que permita dimensionar las consecuencias de la represión dictatorial sobre las comunidades LGBTIQ. Sólo existen investigaciones exploratorias que dan cuenta de testimonios fragmentados de lo vivido. Coincidentemente, el mismo día en que cumplí 26 años el diario “El Dinamito”, publica una nota sobre **“el único asesinato por orientación sexual ocurrido durante la dictadura en Chile”**. Se describe como un hecho aislado que ocurrió en Arica en 1975. **¿Cómo va a ser el único?**

Han existido esfuerzos desde distintos actores sociales como activistas, periodistas y organizaciones de la sociedad civil por recabar información y así visibilizar cómo las diversidades y disidencias sexuales sobrevivieron a la represión del Estado. Si en algo han concordado, es que a la fecha no existe evidencia consistente que permita asegurar que durante la dictadura existió una política particular de persecución y represión contra gays, lesbianas, travestis y transexuales. Pero como contraparte, si es posible asegurar que **la violencia y la tortura era mayor por parte de las fuerzas represoras del Estado cuando se les identificaba como parte de estas comunidades.**

A pesar de lo frustrante que puede resultar el no poder dar respuesta a lo que deseo conocer, existe un nombre que se repite de manera obligada en los artículos visitados para abordar esta sencilla investigación. Les hablo de **Pedro Lemebel**. Y aquí se me presenta otra dificultad, que es tratar de definir su figura, porque definirlo sólo como escritor resulta insuficiente, y definirlo como artista puede ser muy amplio como magnificente, algo que precisamente a Pedro no le gustaría.

En lo concreto, Pedro Mardones Lemebel, fue profesor de artes plásticas egresado de la Universidad de Chile, profesión que sólo ejerció por cuatro años a comienzo de los ‘80, ya que en el sistema escolar de la época no habia cabida para un profesor marica. Según sus cercanos, esto generó tanta rabia, frustración y resentimiento que lo volcó a escribir convirtiéndose en el prolífico narrador, cronista, poeta, performer y,por sobre todo, en **un incansable activista político en derechos humanos y derechos sexuales.**

Publicó diversos cuentos y crónicas, siendo su obra más reconocida la novela “Tengo miedo torero”, con la cual alcanzó renombre tanto en Chile como en el mundo. Para muchxs fue un vanguardista, **personalmente creo que Pedro por sobre todo fue un luchador.** Fue la voz de los sin voz, la voz de la marginalidad.

Y cuando hablo de marginalidad, no me refiero solo a quienes viven en condiciones de pobreza, de la cual proviene Pedro quien vivió su niñez entre basurales en las orillas del Zanjón de La Aguada, un canal que cruza la zona sur de Santiago. Sino que hablo de aquellos que están por fuera de las normas sociales, fuera de la heteronorma. Y desde aqui es que siento con profunda convicción que Lemebel fue, sino la primera, seguro **la más importante figura marica que ha tenido Chile, una “mariquita linda” en sus propias palabras.**

Toda su obra está teñida, además de un sentido trasfondo político/histórico, de una militancia disidente. Y porque **hablar de disidencias no es lo mismo que hablar de diversidades**. Porque la lucha disidente también es lucha de clases. Es una lucha contra cómo está concebido el sistema heteropatriarcal. Porque no es lo mismo ser un varón cis homosexual con rasgos caucásicos de alguna comuna del sector oriente de la región metropolitana (zona en la que viven las personas de mayores ingresos de la capital), que ser una corporalidad trans de rasgos mestizos de alguna población de las comunas populosas del pais. Las luchas son distintas porque sus orígenes también lo son. **Unxs luchan por la igualdad, y otrxs como lo hizo Pedro, luchan por la diferencia.**

Ya lo decía en una entrevista publicada en 2014 un año antes de su muerte, cuando fue consultado a propósito del proyecto de ley sobre matrimonio igualitario: “Se pueden enamorar, convivir, amancebar y pueden adoptar hijos o mascotas, pero más que repetir la ceremonia nauseabunda de la boda, debe existir un universo cambiante, múltiple, trans, libertario y diferenciado, más progresista, más arriesgado, locas políticas que se casen con la revolución del deseo, de todos los deseos sociales de los oprimidos.”

Pienso en Pedro, disidente en dictadura. Y así comprendo el porqué de esa mirada que conjuga tristeza y rebeldía. Resistiendo no solo desde la persecución por sus convicciones políticas sino que también luchando contra la represión a su identidad frente a la heteronorma idealizada en la figura del dictador Pinochet. Luchando por su vida y por su identidad, **porque su cuerpo fue un campo de batalla**.

Este mes se estrena la primera adaptación al cine de “Tengo miedo torero”, sin duda la película chilena más esperada por lxs chilenxs en lo que va del año. Y entonces septiembre se vuelve más nostálgico aún. Porque la obra de Pedro es mucho más contingente al proceso sociopolítico que está viviendo el país. Por tanto, **si queremos avanzar hacia una sociedad realmente inclusiva desde nuestras diferencias, debemos ser más como Pedro, ser más disidentes. Al menos así quiero ser**.

<!--StartFragment-->

###### **Fragmento de “Manifiesto (hablo por mi diferencia)”** por Pedro Lemebel, 1986.

*“No soy un marica disfrazado de poeta*

*No necesito disfraz*

*Aquí está mi cara*

*Hablo por mi diferencia*

*Defiendo lo que soy*

*Y no soy tan raro*

*Me apesta la injusticia*

*Y sospecho de esta cueca democrática*

*Pero no me hable del proletariado*

*Porque ser pobre y maricón es peor*

*Hay que ser ácido para soportarlo*

*Es darle un rodeo a los machitos de la esquina*

*Es un padre que te odia*

*Porque al hijo se le dobla la patita*

*Es tener una madre de manos tajeadas por el cloro*

*Envejecidas de limpieza*

*Acunandote enfermo*

*Como la dictadura*

*Porque la dictadura para*

*Y viene la democracia”*

**Bibliografía:**

* [Las cifras de la dictadura](https://www.lavanguardia.com/vida/junior-report/20200226/473795281381/dictadura-chile-victimas-asesinados-desaparecidos.html), La Vanguardia (2020)
* [Diversidad sexual en dictadura militar](<* <http://www.cedocmuseodelamemoria.cl/wp-content/uploads/2019/01/Redacci%C3%B3n-final.pdf>>) (1973-1990) Museo de la Memoria y DDHH [](http://www.cedocmuseodelamemoria.cl/wp-content/uploads/2019/01/Redacci%C3%B3n-final.pdf)
* [Históricamente nos han querido invisibilizar: violaciones a DDHH contra población LGBTIQ en dictadura, El Desconcierto 2019](https://www.eldesconcierto.cl/2019/09/11/historicamente-nos-han-querido-invisibilizar-violaciones-a-ddhh-contra-poblacion-lgbtiq-en-dictadura/)
* [No queremos ser más ausentes en la historia](<* <http://palabrapublica.uchile.cl/2019/10/09/no-queremos-mas-ausentes-en-la-historia/>>), Palabra Pública Universidad de Chile, 2019 [](http://palabrapublica.uchile.cl/2019/10/09/no-queremos-mas-ausentes-en-la-historia/)
* \[Pasados suspendidos. Estrategias represivas y tecnologías biopolíticas sobre las disidencias sexogenéricas durante la dictadura de Augusto Pinochet en Chile](<* <https://revistapaginas.unr.edu.ar/index.php/RevPaginas/article/view/366>>) [](https://revistapaginas.unr.edu.ar/index.php/RevPaginas/article/view/366)
* Pedro Lemebel, [Memoria Chilena](http://www.memoriachilena.gob.cl/602/w3-article-3651.html#presentacion)
* Pedro Lemebel: [el corazón rabioso del hombre loca](https://www.ciperchile.cl/2015/01/23/pedro-lemebel-el-corazon-rabioso-del-hombre-loca/), Ciper Chile 2015
* Pedro Lemebel:

{{< youtube gfQRLKtHIw0 >}}

<!--EndFragment-->