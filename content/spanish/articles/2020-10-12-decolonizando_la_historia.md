---
title: "Decolonizando la historia "
subtitle: " "
cover: /images/uploads/1431970806_143197_1431970806_noticia_normal.jpg
caption: Mural en las calles de Ecuador
date: 2020-10-12T14:14:38.296Z
authors:
  - Jefersson Leal
tags:
  - EdiciónEspecial
  - 12deoctubre
  - Identidad
  - Latinoamerica
  - Continente
  - Cronica
  - Relato
  - Experiencias
  - América
categories:
  - Cuerpo
comments: true
sticky: true
---
<!--StartFragment-->

Si la memoria no me falla yo tendría 4 o 5 años para ese momento, ese año había llegado por fin al salón del tercer piso, allí siempre estuvieron los más grandes, y en ese momento estaba yo. Como cualquier otro niño, ir a la escuela era mitad tragedia por el dolor de ver partir a mi madre y mitad alegría por los juegos constantes.

Recuerdo que entre todas las festividades que se hacen en una escuela, había una especial; la maestra nos mandaba a llevar periódicos de nuestras casas, tijera y pega blanca. Por lo general llevaba dos de tres, siempre faltaba algo, o la tijera o la pega que siempre fue la más cara.

Todos esos materiales eran para realizar un penacho indígena, la maestra en honor a la verdad hacia la mayor parte del trabajo, pero todos siempre salíamos orgullosos con el penacho y haciendo sonidos con la boca para parecer nativos. Todo esto, claro está, era para celebrar el 12 de octubre, el día “De la Raza” al menos ese fue el nombre por unos años.

Ese día la maestra nos contaba de la Pinta, la Niña y la Santa María, nos contaba de Colón y su hazaña, de su llegada a Tierra Firme y, como no, del famoso intercambio del oro y el espejito. Todo aquello era maravilloso para un niño de mi edad, era una fantasía, era poder viajar al pasado.

Siempre llegaba con el penacho y el dibujo de las tres carabelas a casa, se lo mostraba con cierto orgullo a mi madre y continuaba la fiesta con mis onomatopeyas, para seguir en mi celebración.

Esta felicidad, admito, no duró mucho, el país, poco tiempo después, cambió el nombre a “El encuentro de dos mundos”, y ya en la Escuela no se celebraba como antes. Sin duda, el cambio de nombre fue una sorpresa para mí, pero la historia de la maestra siguió siendo la misma, para ella solo había cambiado el nombre, nada más.

De esta forma, llegó a mi vida el 12 de octubre envuelto en esa pátina de felicidad infantil. Pasaron los años y esa fecha no pasó por alto en estas tierras, los sucesivos cambios de nombre y la pérdida de importancia lo convirtieron en el centro de una disputa social y política.

El 12 de octubre retumba cada año, en la historia que me contaron de chico, había unos héroes, los europeos y unos salvajes los indígenas. En esa historia los españoles eran unos seres de luz, que venían a expiar los pecados de los desnudos habitantes de esta parte del mundo. Esa versión para algunos parece caricaturesca, está firmemente arraigada en la mente de los americanos y de los europeos.

No hace falta excavar muchos en los archivos o saber de paleografía para darse cuenta que esa pretendida historia del “descubrimiento” no es otra cosa que el relato de un vencedor, donde se esfuerza por mostrar sus virtudes y lo beneficioso que fue su llegada al territorio hoy americano. En esa versión ampliamente difundida no aparece, ni en los pies de página, la relevancia que tuvo para el desarrollo de esas naciones la invasión de este vasto espacio geográfico.

Los colonos y su sífilis, fueron poco a poco nombrando todo, y entre tanto nombre también se nombraron a ellos. La invasión representó entre tantas otras cosas, la formación identitaria de Europa la cual se formó en función del otro, ellos civilizados, el otro salvaje, ellos representando el progreso los otros la oscuridad.

Nuestra historia duele, sobre todo porque la han robado, mutilado, y así sin mucha vergüenza nos la han devuelto en los libros y relatos históricos como “el descubrimiento”, un término groseramente eurocéntrico, que pone en el foco de la dinámica histórica al blanco, antes de ese hecho, para esta versión histórica, está la nada, un pasado el cual hay que forzosamente olvidar.

Los españoles “descubrieron” un continente en función de su desconocimiento, pero no en función de la realidad, de la verdad. Esta tierra ya desde hace mucho tiempo vivía gente que construía historia. Pero la estrategia del vencedor siempre es borrar la memoria, quizá por que sea el mejor método de colonización.

Hoy en día ya quemé el penacho y los barquitos, pero aún se sigue preocupando aquella profesora, que de seguro sigue repitiendo los mismo, sin darse cuenta, al menos es mi esperanza, que reproduce un versión eurocéntrica, racista y positivista que en nada contribuye a la formación de nuestros escolares.

**No hay nada que celebrar en esta fecha, en todo caso se conmemora el inicio de la destrucción.**

<!--StartFragment-->

#### ***Otras entregas de esta edición especial “12 de octubre”***

<!--StartFragment-->

[¡Más que nunca, las venas están abiertas!](https://revistacuerpoyterritorio.com/2020/10/10/mas-que-nunca-las-venas-estan-abiertas/)

[Las doce Malinches o el mito que seremos](https://revistacuerpoyterritorio.com/2020/10/10/las-doce-malinches-o-el-mito-que-seremos/)

[Historias tejidas en los cuerpos y territorios del sur de América Parte I](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america/)

[Historias tejidas en los cuerpos y territorios del sur de América Parte II](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america-parte-ii/)

<!--EndFragment-->

#### Además, los invitamos a conectarse a la tertulia de YouTube live este 12 de octubre para conversar, desde otras perspectivas, sobre esta fecha tan importante en América haciendo clic [AQUÍ](https://www.youtube.com/watch?v=Bddgfi3tARU&feature=youtu.be)

![](/images/uploads/12_de_octubre_la_historia_no_contada_de_america_9_.png)

<!--EndFragment-->