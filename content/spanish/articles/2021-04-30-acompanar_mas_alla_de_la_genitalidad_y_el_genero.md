---
title: Acompañar más allá de la genitalidad y el género
subtitle: El acompañamiento debe ser efectivo, afectuoso y consensuado
cover: /images/uploads/03_plannedparenthood.jpg
date: 2021-04-30T14:19:09.372Z
authors:
  - Jesús Gutierrez
tags:
  - Mujer
  - Cuerpo
  - Territorio
  - Genero
  - Acompañamiento
categories:
  - Genero
  - Cuerpo y Territorio
comments: true
sticky: true
---
Acompañar siendo un varón cis tiene muchas implicaciones que lo vuelven más complejo, hay un universo diferente con sus dificultades que van de la mano a todo lo que implica una corporalidad distinta al cuerpo a acompañar. Es muy necesario el desarrollo de habilidades y capacidades adicionales, tener en cuenta una mayor empatía que permita acercarse al proceso que está viviendo la persona y que nunca vas a poder vivir, que no vas a vivir -entre otras cosas- porque la construcción histórica, social, cultural, que llevas sobre tus hombros es otra, no estás construido como una mujer por lo que no sientes como una mujer, pero puedes sentir y desde ahí crear puentes para que la otra persona te sienta presente.

Para empezar hay que generar empatía, confianza, seguridad, ¿cómo podemos hacer que confíe en nosotros?, sobre todo porque no hay un proceso de auto reconocimiento de ni autorreferencia, en especial porque la referencia que da nuestro cuerpo es ajena a su experiencia personal y se parece más a la de su agresor que a la de un aliado. En mi caso, particularmente gran parte de los casos que he acompañado han sido casos referenciados, contando con mujeres que depositaron su confianza en mí y han servido como interlocutoras, lo que construye puentes de comunicación y confianza.

Con el tiempo las propias vivencias ayudan a generar vínculos, contar las experiencias vividas con otras mujeres que han depositado su confianza en mí, me ha ayudado a darles a entender que aunque no puedo estar en su lugar empatizo con ellas, conozco esa realidad y puedo darles fe testimonial que es una situación de la que pueden salir airosas. En ese momento entienden algo muy importante, **yo también estoy poniendo el cuerpo**, estoy ahí presente, no son extrañas para mí y ese lugar en donde están lo conozco, no de la misma manera, pero al conocer el camino entienden que puedo acompañarlas a salir.

Es muy importante entender que cada historia es diferente, las experiencias previas nos sensibilizan, nos dan herramientas de escucha para identificar detalles, dan una idea de que palabras podemos usar, de que manera y en que tono usarlas, dependiendo del tipo de acompañamiento incluso el factor biológico, el cuerpo hace que todo sea diferente. La experiencia no nos gradúa de acompañantes, pero si da la suficiente seguridad para abordar amorosamente, sabemos que decir, que preguntar y cómo preguntar para tener la información necesaria sin hacerla sentir transgredida, leemos entre líneas, leemos expresiones, gestos y resignificamos cada lágrima que se presente en cada situación.

**Acompañar cuerpos que sobreviven a la violencia**

![](/images/uploads/02_albaciudad.jpg)

El acompañamiento debe ser efectivo, afectuoso y consensuado, **el acompañamiento será amoroso o no será**, sobre todo por la manera en la que los varones somos socializados no entendemos las situaciones de violencia a las que son expuestas las mujeres de la misma manera, nos es muy difícil asimilar que la violencia tiene múltiples caras, formas, niveles, nos cuesta entender que conductas de nuestros congéneres son violencia. Por esto, la conversación con un amigo varón minimizará lo planteado, revictimizando a la mujer, ejerciendo nuevamente violencia, por lo que acompañar desde un cuerpo masculino arrastra con los estigmas de las heridas, cicatrices que otros hombres han dejado, como ya dije nuestro cuerpo es el mismo cuerpo del agresor, es difícil que nos vean como la solución a una situación que uno de mis pares inició.

Gran parte de los casos de este tipo que he acompañado han sido amigas, mujeres cercanas, lo que me ayudó a sensibilizarme en el tema y empezar a reconocer las múltiples violencias que viven día a día. Esta parte también ha sido muy difícil, ya que es encarar al dolor, a la violencia y hasta la muerte misma en la cara de un ser querido, implica estabilizarse uno para poder estabilizar y calmar a quien acompañamos, es oír activamente todo lo que pasó, lo que puede aún estar viviendo, **acompañar es un acto de amor y entrega que ayuda a sanar una herida, pero que resquebraja un poco por dentro a quien acompaña**, cuando es un ser querido es difícil no dejarse llevar por las emociones.

El acompañamiento de este tipo, implica tener herramientas de varios tipos, debemos por ejemplo conocer la ruta para hacer denuncias con los entes competentes en la materia, los requisitos para la misma y saber que no nos la van a poner fácil por lo que hay que armarse de paciencia. Hay que saber escuchar, saber ponerse en el lugar de la otra persona sin juzgar, eliminar frases como **“yo no hubiese hecho eso”** es fundamental, entender los límites de la otra persona, que puede o no denunciar, que puede o no en este momento dejar a su agresor, por las razones que sea y es difícil entender y acepta eso, **pero acompañar es respetar y aceptar la decisión de la otra persona**, no hay buena intención que justifique el violentar a la persona que ya está en situación de violencia, exigiéndole salir de este ciclo, presionarle para que deje a la otra persona, no es lo que queremos es lo que es mejor para la mujer que nos brindó su confianza, y cuando entienden las implicaciones de lo que están viviendo es el momento ellas mismas deciden.

La escucha activa es fundamental, al ser varón contarnos algunas cosas les da más vergüenza, por lo que es necesario unir ideas, historias, reacciones, saber cuando es necesario abrazar y cuando definitivamente no. “¿Qué crees que es lo mejor para ti?, ¿y tú crees que mereces eso?, ¿y antes cuando te ha dicho que va a cambiar lo ha hecho?”, son las interrogantes permitidas, las que les permitan cuestionar lo que están viviendo y no cuestionarse su amor propio o que las lleven a juzgarse y avergonzarse, la voz del varón es más contundente a la hora de emitir juicios de valores, por lo que hay un trabajo de fondo de aprender a construir discursos desde la sensibilidad, la empatía y el amor, ir contra a nuestra socialización que nos ha enseñado a mirar por encima del hombro a las mujeres y criticarlas por sus decisiones, acompañar ha sido un proceso de pensarme, pensar al sistema, replantearme mi manera de relacionarme y volver hacerlo cada vez que se pueda, **a los varones nos crían para ser violentos, no para acompañar.**

![](/images/uploads/01_masculinidades.jpg)

Es muy difícil luchar contra el entorno de la mujer, un cuerpo socializado como varón genera desconfianza, a veces esto complica el tener que explicarle a la amiga, la prima o la mamá que él no va a cambiar, que no es que uno porque es homosexual, porque no se puede casar o porque uno “no cree en el amor” -dicen-, es que uno quiere que hablen y se arreglen. Es más cuesta arriba cuando te pareces al agresor y no lo defiendes, sales del esquema de lo normal, les cuesta entender más todavía que a uno le interesa ayudar además de que esa ayuda no es una conciliación de partes, uno como varón que transgrede la norma y rompe el pacto patriarcal es visto como el enemigo, es atacado por ese entorno que sigue socializando y perpetuando la violencia hacía las mujeres.

**Acompañar cuerpos que deciden**

Al hablar de aborto se vienen un montón de conceptos, frases y palabras a la mente, entre ellas “varón acompañante” no es una. El movimiento feminista es autónomo y está claro en que **“el aborto es un tema de las mujeres y los cuerpos gestantes”**, ¿qué hace acá un varón cis? Acompañar a mujeres en situación de violencia me llevó por este camino, la violencia física, la pobreza, los proyectos de vida, entre otros motivos llevan a una mujer a decidir interrumpir de manera voluntaria un embarazo, sobre todo cuando es violencia sexual el embarazo es parte de ese proceso, es parte de esa situación, ¿cómo no iba a acompañar?

Si acompañar violencias es difícil por no poder generar familiaridad desde los cuerpos, **el aborto es la síntesis de todas las dificultades que implican el no ser mujer acompañante**. Ya en definitiva no hay manera de relacionarse desde los cuerpos, no hay puntos de comparación, no hay una anécdota que genere empatía en función de “entiendo por lo que estás pasando”. Al igual que con las violencias empecé con amigas, da mucho miedo cuando eres inexperto y sigue dando mucho miedo cuando ya lo has vivido antes, pero igual vas adquiriendo tacto, entendiendo reacciones, sabiendo que hacer aunque nunca se está lo suficientemente seguro.

**Cuando se acompaña presencialmente es una experiencia única, no es solamente hacer que se sienta segura, que se sienta contenida y en calma, es ayudar a aliviar el malestar físico, tener información científica** que garantice que todo el proceso será seguro, que todo va a estar bien, que las dudas se disipen. Acá ya se está poniendo el cuerpo, un cuerpo varón, pero es involucrarse con el tema aborto en primera persona -yo acompaño-, mi cuerpo está presente, me estoy exponiendo a lo que pueda pasar, es la única manera que creo, que siento y tengo entendido que puedo participar de manera activa, todo lo demás son redes sociales.

Los acompañamientos desde la virtualidad tienen otras connotaciones, ya es otra realidad en la que el nivel de exposición baja considerablemente y la vergüenza o miedo que genera que el otro sea varón baja, “si no le ves la cara no te sientes expuesta”, el anonimato que da la virtualidad permite además vincularse más rápido, con mayor facilidad, en la era digital es más fácil empatizar a través de una pantalla. La dificultad ahora pasa a lo impersonal del proceso, no poder estar para ayudar con los síntomas físicos, no poder dar palabras de aliento si la persona no está en su dispositivo, mayor alcance menor interrelación.

La ilegalidad precariza todo, dificulta el acceso a la información, genera más miedo, incertidumbre, dificulta mucho una tarea que no debe ser tan complicada, no hay una ruta de atención como para colocar una denuncia, no es algo que puedas ni debas comentar a familia y amigos, es vivir en la clandestinidad y acompañar a la mujer con el estigma social, darle más apoyo y comprensión porque afuera no hay muchas personas con quien hablarlo.

**Acompañar es algo maravilloso, te construye constantemente, te acerca a otras realidades, es un proceso doloroso, pero el caos previo a la creación lo es**. En mi experiencia como varón acompañante debo decir que es necesario de la incorporación de otros, sin que se entienda como la invasión de espacios, creo necesario entendamos las otras realidades, las vivamos, pongamos el cuerpo para entender, comprender, respetar y aceptar.

#### Jesús Gutierrez

**Acogido por la Ciudad de Caracas, egresado de la Universidad Centroccidental Lisandro Alvarado como Licenciado en Desarrollo Humano, Educador Popular desde los 17 años, recreador, activista por los Derechos Humanos y en particular por los derechos de la población LGBTI, investigador. Disidente.**