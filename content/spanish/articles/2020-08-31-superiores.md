---
title: ¿Superiores?
subtitle: Reflexiones sobre la especie que se adueñó del mundo.
cover: /images/uploads/imagen.jpg
caption: ""
date: 2020-08-31T15:34:02.304Z
authors:
  - Iris Mendizábal
tags:
  - EspecieHumana
  - Conservación. Naturaleza
  - Humanos
  - Ecosistemas
  - Ambiente
  - Tierras
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

> *Los océanos, invadidos por el plástico. Los ríos, llevándose nuestros desechos industriales. Los animales, perseguidos y asesinados. Los árboles, desapareciendo detrás de las topadoras. Cientos de especies, al borde de la extinción. Y nosotros, encerrados en nuestras cajas de cemento, respirando resignados el hollín de las ciudades y mirando con desesperación los metros cuadrados de cielo que nos toca mirar, sin horizontes… Permítanme dudar, al menos, cuando nos queremos convencer de que estamos evolucionando.*

Leer a Thomas Kuhn (1922-1996) cambió mi forma de ver el mundo. Este físico, filósofo e historiador estadounidense postuló que la realidad es explicada, entendida y ampliamente aceptada según el paradigma que gobierne nuestros aires, nuestros libros y nuestras mentes.

En otras palabras, un paradigma establece ciertos conceptos robustos bajo los cuales uno se suscribe a trabajar, dando por hecho que determinadas cosas son como son y las podemos usar de base para la investigación o para darle forma a nuestros pensamientos. Sin embargo, en presencia de evidencias suficientemente contundentes que demuestren lo contrario, los paradigmas pueden caer y cuando lo hacen, arrastran consigo a una enorme cantidad de conceptos que anteriormente fueron aceptados y aplaudidos.

¿Qué nos pasa transitando nuestro aún joven siglo XXI, con nuestros paradigmas morales? ¿Qué sucede con esas creencias, saberes e ideas instaladas desde hace años? ¿Se te ocurre alguna vez pensar que podríamos haber equivocado el camino? ¿Y que podríamos haber construido, una moral que justifique comportamientos equivocados, violentos e insensibles, no sólo con nosotrxs mismxs, sino con otras especies? ¿Qué opinión te merecen nuestras enraizadas ideas de superioridad humana frente al resto de los seres vivos? Y además, ¿Cuándo dejamos de sensibilizarnos ante el ingenio de la naturaleza para hacernos a todxs un poco distintxs, y a la vez, un poco iguales?

El concepto de superioridad de la especie humana y de inferioridad de cualquier otra especie diferente a la nuestra está tan arraigado en nuestra cultura, que se necesitaría una evidencia del tamaño de otro sistema solar para hacerlo caer. Es paradigmático y punto. Construimos nuestra sociedad dando por sentado que las demás especies que habitan el planeta no tienen ni una pizca de la extrema lucidez que nos caracteriza. ¿No es, acaso, un poco soberbio pensar que las únicas criaturas conscientes, capaces de analizar, de comprender y de responder a su entorno seamos los seres humanos? Además, ¿Cómo es posible que la tierra albergue a tantas especies que no entienden nada sobre el funcionamiento de la vida y, que al mismo tiempo, albergue a una sola que sí lo comprende todo?.

Es que no sólo es soberbio, además no tiene sentido desde el punto de vista biológico. No es ninguna novedad, pero derivamos por evolución de los pocos primates que con suerte todavía podes encontrar en alguna selva de Tanzania en estado salvaje. Esto significa que somos “hijas e hijos” de todos nuestros antepasados animales, y que con un poco de paciencia y de proyección en el tiempo podemos ver que somos parientes no sólo de cualquier animal, sino también de las plantas, de las bacterias y de los virus.

Bucear en las profundidades de la biología nos deja a todos perplejos, y a todas maravilladas. No sólo por la gran complejidad y capacidad de adaptación que caracteriza a los seres vivos, sino también por la enorme diversidad de colores, tamaños, formas y habilidades que estos presumen.

Pero además, por esa “magia” que se esconde ahí, en los rincones de una célula apoptótica, capaz de desencadenar toda una cascada de reacciones que la lleven a su propia destrucción, de un óvulo recién fecundado debiendo reprogramar su existencia entera para dar origen a un nuevo ser vivo o de un glóbulo blanco, practicando tácticas y estrategias para combatir a un agente patógeno. La vida no es una maquinaria perfecta. Es mucho más apasionante que eso. Estudiarla y conocerla lo coloca a uno de rodillas, aceptando la propia pequeñez.

Las consecuencias de la falta de humildad humana exceden al error que acarrea el hecho de vivir una vida entorpecida por el propio ego. Son mucho peores porque perjudican seria y dolorosamente a otros seres vivos: esta falta de humildad nos posiciona no sólo como seres superiores, sino también, como los dueños y las dueñas de todas las decisiones sobre quién puede vivir y quién debe morir en este mundo

Por todo esto quiero pedirte que antes de afirmar que los seres humanos somos superiores a todos los reinos de la vida porque hacemos ingeniería de construcciones, porque inventamos formas de cruzar el océano o porque fuimos a la luna, reflexionemos juntos por un momento sobre cuánto tiempo podemos estar debajo del agua sin respirar: en promedio, alrededor de unos miserables tres minutos indaga por cualquier página web la cantidad de kilómetros en el océano que las ballenas pueden recorrer sin necesidad de barcos, ni de combustibles fósiles, ni de marineros, ni de toda la pompa que los humanos y humanas le ponemos a todo.

¿No te sentís un poquito más pequeñx? ¿Y si te digo que existen animalitos que son capaces de resistir una radiación tan enorme que a nosotrxs nos dejaría hechos añicos en unos minutos, sin modificar apenas su material genético? ¿Y qué pensarías, con tus escasos 80 años de esperanza de vida, de los ejemplares de Cyca revoluta (una planta similar a una palmera) que se estima que tienen 1000 años? Sí, 1000. ¿Y si te digo que una de las técnicas más utilizadas en biología molecular para diagnosticar enfermedades y para investigar posibles curas depende de la existencia de una proteína que pertenece a una bacteria de aguas termales y, que sin ella, y sin el genio al que se le ocurrió la técnica, miles de avances científicos en salud humana no hubiesen sido posibles? ¿Y qué sensación te produce saber que dependemos de bacterias aeróbicas y anaeróbicas para no quedar sepultados para siempre bajo nuestras toneladas de basura, ya que ellas realizan el noble trabajo de purificarla?.

… Me podrías contestar que el ser humano se las ingenió de maravillas para suplir todas las falencias y que igual conquistó el mundo. Y tengo que decirte que lamentablemente tendrías razón, pero que dadas las peculiares habilidades que todos los seres de otras especies demuestran tener, podríamos empezar a reconocer que estamos pecando de soberbia al creernos dueñxs de todo y mejores que todos.

En última instancia, y ya que nos creemos tan superiores y con un sistema nervioso tan de avanzada, podríamos poner nuestros talentos al servicio de encontrar la forma de seguir viviendo sin exigirle tanto a nuestras tierras, a los animales, a las plantas y a los bichitos microscópicos.

Finalmente, somos todo hermanxs, porque descendemos de una misma unidad de vida, que gracias a la energía de las tormentas, a los gases de la atmósfera y a una pizca de magia que nunca vamos a terminar de comprender, evolucionó para dar origen a todos los seres que habitamos este mundo.

Lo que creo que nos pasa es que el misterio de la vida es tan inmenso que, al no ser capaces de explicarlo, preferimos ignorarlo elucubrando explicaciones de autosuperioridad que no resisten un buen análisis de pruebas científicas y tampoco de pruebas del corazón. Sí, del corazón. Ese que te late en el medio del pecho… ¿O acaso creíste que después de tanto progreso lo que te mantiene con vida es un cable “usb” conectado a un tomacorrientes?

<!--EndFragment-->