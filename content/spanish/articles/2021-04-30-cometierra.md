---
title: "Cometierra "
subtitle: "Reseña literaria "
cover: /images/uploads/cometierra.jpg
date: 2021-04-30T16:42:29.769Z
authors:
  - Juan Manuel Barreto y Roberto Salazar
tags:
  - Cultura
  - Lietarura
  - Mujeres
  - Genero
categories:
  - Genero
  - Cuerpo
  - Territorio
  - Cuerpo y Territorio
  - Cultura
comments: true
sticky: true
---
*Autora: Dolores Reyes*

*Fecha de publicación: 2019*

*Editorial: Sigilo*

**Dolores Reyes parece meterse adentro del cuerpo de sus personajes para experimentar lo que están sintiendo**, lo que les pasa, lo que los remueve. De ahí la maestría de esta hermosa narración que da nombre a su personaje principal: **Cometierra.**

Hay una escena iniciática en el relato. Un femicidio marca la vida de Cometierra, siendo ella una niña. El de su propia madre cometido por las manos asesinas de su padre:

“Veo los golpes aunque no los sienta. La furia de los puños hundiéndose como pozos en la carne. Veo a papá, manos iguales a mis manos, brazos fuertes para el puño, que se enganchó en tu corazón y en tu carne como un anzuelo. Y algo, como un río, que empieza a irse”.

En esas primeras líneas presenciamos el entierro materno. **Cometierra traga tierra en el cementerio**. El suelo acumula esas historias y experiencias que ella incorpora para su adivinación. Se revela para que Cometierra pueda ver o leer:

“Nunca me había parecido que lo que hacía fuera adivinar. Adivinar era algo raro. Como creer que podía acertar el número de la quiniela. Nada que ver con cerrar los ojos y estar frente a un cuerpo desnudo sobre la tierra”.

**Cometierra no tiene un nombre propio**. Y su falta apunta a una identidad cercenada, una de las tantas formas en las que se puede desaparecer.

\*\**

“Nada de lo que un escritor crea puede escapar de lo que es” (1).

**Reyes escribe desde Argentina -un país donde ocurre un femicidio cada 31 horas**-, y la historia se ambienta en el conurbano bonaerense. En la misma dedicatoria del libro aparecen los nombres de Araceli Ramos y Melina Romero, quienes fueron asesinadas por femicidas y enterradas a pocos metros de la Escuela 41 donde trabaja Reyes. La influencia de estos hechos resulta determinante en la concepción de su obra, en palabras de la propia autora.

En Cometierra los femicidios son narrados desde múltiples voces y, según Reyes, hay una voluntad de que el relato se cuente desde la perspectiva de la hija de un femicidio, **una víctima-testigo.**

El contexto social descripto es tan crudo y agobiante como esas historias singulares que recorren las páginas de este libro. Cometierra y su hermano “El Walter” dejaron la escuela y se fueron quedando solxs. Primero, lxs abandonó su padre. Luego, perdieron a su madre. La última en irse, un día cualquiera, fue su tía. Dice Cometierra, en referencia a la escuela:

**“La mitad de los chicos del barrio la habían dejado. Pero yo no trabajaba ni estaba embarazada”**.

A medida que leemos la novela, nos encontramos con muchxs pibxs que andan por ahí, vagando. Rancheando. No hay presente. Ni futuro. Es la precariedad propia de los más vulnerables.

Surge, entonces, la pregunta por el Estado, el cual brilla por ausencia o connivencia. **Cometierra y el ejercicio de su don intervienen en una realidad en el que las responsabilidades colectivas se diluyen.**

\*\**

Pero si la violencia impune y la pobreza abyecta enmarcan la novela y cruzan a sus personajes, la tierra y los poderes que conferidos a Cometierra resultan una tregua, una pausa, un sendero que dibuja la posibilidad de revertir, al menos por momentos, el sufrimiento de las víctimas o de sus familias. El costo que tiene que pagar nuestra protagonista no es menor.

**Si la tierra es símbolo de lo primario, de lo nutritivo, de lo materno, comer tierra es precisamente el retorno a esa escena perdida, remota, en que hubo comunión con lo originario.** Tragar tierra es para nuestro personaje una vuelta inevitable a esa unión con la madre fallecida, con la madre enterrada, violentada, un enlace que además le devuelve no solo a su madre sino a otras víctimas que claman, que gritan, que suplican un lugar en la superficie, un final, cualquiera que sea este, que les dignifique.

Los dones de Cometierra no tardan en hacerse eco en la comunidad y los requerimientos de su poder se multiplican. Son muchxs, muchísimxs, lxs dolientes. Envían botellas, como contenedores de mensajes desesperados echados en un mar ignoto, sin saber si algún día serán leídos:

“Descalza, la acompañé hasta la reja para despedirla, y descalza me quedé haciendo tiempo y mirando las botellas escondidas entre las plantas. Algunas estaban hacía mucho y se iban como enterrando, clavadas, con el agua y el tiempo que dañaban letras, nombres, números de teléfono, que borraban todo menos el dolor del que las había traído hasta ahí y sus ganas —todas idas menos una— de saber dónde está.”

Cometierra vacila, retrocede, hace lo que puede. Los mensajes de la tierra están escritos en líneas torcidas, como se hará evidente en algún pasaje:

“La pared del fondo, pegada a la cama en donde María estaba mirándome, tenía algo escrito que no llegaba a leer. ¿Podía leer? En los sueños no. Las letras se ponían raras. No se quedaban quietas. Si podía entender una palabra, la siguiente cambiaba. Leer en los sueños me era casi imposible.”

Y a esta asombrosa capacidad se repliegan todxs: recibe tanto visitantes de zonas privilegiadas de la ciudad, como miembros de la policía y hasta humildes amas de casa. **Cometierra es la “mae” de los olvidados, vengan de donde vengan.**

\*\**

La gracia de Cometierra transcurre en una suerte de normalidad. No hay extravagancia en la vida de esta chica. Ella no es una bruja: es una joven más del Conurbano. Escucha cumbia, juega Playstation, le gustan los chicos, toma cerveza. **Su poder corre en paralelo a la precariedad**. En los pies descalzos en un descampado, en la inundación de unos rieles de tren, en la búsqueda de una adolescencia ya adulta.

Por cada policía desestimando una denuncia por violencia de género, de las entrañas de la tierra retorna una vidente con capacidad para encontrar a un desaparecido.

**Cometierra traga tierra y cierra los ojos para encontrarse con la verdad.** Con lo que nadie ha querido ver o ya ha dejado de ver. Puesto que en las visiones de mujeres desaparecidas no hace sino probar la hechura de esa tierra misma que somos todxs.

El pasaje de la novela en el delta del Paraná será significativo. Hay un equilibrio en la naturaleza, en lo primigenio que debe ser restaurado. Lo simbólico es siempre un intercambio y, a la vez, una pérdida. Cometierra es menos la instauración de la ritualidad mágica en nuestras comunidades más depauperadas, que lo insostenible de tales aspiraciones: ella no puede sola. **No se puede en soledad**.

Al final, tal vez, la novela trate, en parte, del tiempo que transcurre entre lo excepcional de ciertos dones individuales y la impotencia de los mismos para resolver las más terribles desgracias. A las víctimas que aún nos reclaman sepultura, a lxs ausentes que aún nos piden una oportunidad, a lxs extraviadxs que aún nos demandan un nombre, a esxs, siempre, debemos responderles entre todxs.

##### Dolores Reyes

![](/images/uploads/descarga_2_.jpg)

Nació en Buenos Aires en 1978. Es docente, feminista, activista de izquierda y madre de siete hijos. Estudió letras clásicas en la Universidad de Buenos Aires. En la actualidad, vive en Caseros, provincia de Buenos Aires. Cometierra es su primera novela.

### Referencias:

(1) María Teresa Andruetto, Extraño oficio. Buenos Aires, 2021.