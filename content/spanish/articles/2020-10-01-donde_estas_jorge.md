---
title: ¿Dónde estás, Jorge?
subtitle: " "
cover: /images/uploads/music-5475168_1920.jpg
date: 2020-10-01T00:37:49.672Z
authors:
  - Karol Dinamarca
tags:
  - Musica
  - Dictadura
  - Chile
  - FiestrasPatrias
  - Memoria
  - Latinoamerica
  - DictaduraChilena
  - SalvadorAllende
  - Educación
categories:
  - Cultura
comments: true
sticky: true
---
<!--StartFragment-->

El mes de septiembre nos trae las ineludibles "Fiestas Patrias" en Chile, pero debido a esta rebelde pandemia mundial llamada Covid 19, nos hemos refugiado, casi inevitablemente, en nuestras casas para celebrar escuetamente o, de lo contrario, realizar breves visitas a nuestros familiares.

Pero antes de vivir estas celebraciones, nos debemos remontar a otra fecha sumamente importante para la historia de nuestro país. Me refiero a la jornada del **11 de septiembre de 1973, día en que fue realizado el golpe de Estado al gobierno del presidente Salvador Allende.** Ese día, los ejecutantes del golpe y las respectivas cabezas al mando, nos quitaron la democracia para dar paso a la dictadura comandada por el general Augusto Pinochet. Desde este trágico episodio, ya nada volvió a ser igual.

Miles de violentos asesinatos, desapariciones y violaciones a los derechos humanos se llevarían la vida de miles de compatriotas, de los cuáles muchos no han aparecido aún. Cada 11 de septiembre, se abre una vieja pero actual herida, la cual no puede sanar porque la justicia y la verdad no quieren hacer su trabajo, que es la de esclarecer los hechos brutalmente violentos que ocurrieron durante la dictadura militar.

Por suerte, no tengo ningún familiar detenido desaparecido, pero la empatía me hizo vivir este 11 de septiembre de una manera muy especial.

Ese día (como casi todos los días) ingresé a mi cuenta de Instagram ya más o menos suponiendo el contenido con el cual me iba a encontrar. Como era evidente, la mayoría de mis contactos ofrecía un sin fin de información con respecto al 11 de septiembre. Entre ellas, representaciones artísticas audiovisuales in memoriam a los detenidos desaparecidos (con las que lloré emocionadamente al reproducirlas), registros históricos que relatan el minuto a minuto del golpe de estado y así sucesivamente.

Pero hubo una publicación que llamó mi total atención, esa imagen compartida en muchos perfiles e historias de Instagram: **la imagen del maestro Jorge Peña Hen.**

Este músico, compositor y gestor cultural chileno, **fue fundador de la primera orquesta sinfónica estudiantil de Latinoamérica**. Fue un gran difusor también de la música y la cultura en la actual región de Coquimbo y gracias a esto, provocó que la ciudad de La Serena se fuera convirtiendo en un centro musical y cultural de aquella época.

Al instalarse definitivamente en la ciudad de La Serena, comenzó su gran labor de difusión artística, naciendo así la Sociedad Bach; proyecto que fomentaba la vida musical en la zona y en 1946 logró instalar la Escuela Experimental de Música de La Serena, la cual fue replicada en la región de Ovalle y Copiapó. Esta escuela fue cuna de grandes músicos y luego muchos de ellos se trasladaron a diversos lugares del país y el mundo para dar continuidad a sus carreras artísticas.

Si hablamos con muchas de estas personas que tuvieron la oportunidad de aprender en esta escuela o de conocer a este importante gestor cultural, ellos nos hablarán con mucho amor y profunda admiración de él, ya que fue el precursor del despertar cultural docto de esa región, **dándole la oportunidad a muchos niños y jóvenes de escasos recursos de conocer la música a través de un instrumento.**

Y una faceta que no puede dejar de ser nombrada es la de compositor, ya que el Maestro aparte de escribir obras complejas como lo es el cuarteto de cuerdas o la sonata para violín y piano, también dedicó su escritura para las juventudes musicales, creando concertinos para violín y piano, una ópera infantil, entre otros.

Pero al Maestro le tocó vivir directamente las consecuencias post golpe de estado. **Fue acusado de internar armas al país en los estuches de instrumentos de los niños** (previa gira artística a Perú, Argentina y Cuba) y debido a esta acusación fue ejecutado, junto a 14 personas en el regimiento "Arica" de La Serena el día 16 de octubre de 1973.

La dictadura se llevó a una importante figura artística de nuestro país, pero no logró borrar la memoria de muchas personas que conocieron la obra del Maestro. Si bien fue una completa injusticia la desaparición y muerte de Jorge Peña Hen, a la vez pude sentir una especie de "alivio" al ver que muchos amigos, conocidos y contactos de mi perfil viven intensamente esta terrible fecha, recordando con mucho cariño y respeto la vida y legado del Maestro.

**El legado de este gestor es inconmensurable porque creó un movimiento sociocultural en donde todos tuvieron cabida, sin importar su procedencia ni su nivel educacional.**

Las nuevas generaciones están más conscientes que nunca de que en Chile existieron y existen un montón de figuras muy importantes en el rubro musical y, lo trascendental de este fenómeno, es que muchos de ellos quieren hacer vívida la memoria de estos personajes, lo cual agradezco infinitamente porque me ha permitido reencontrarme con la historia musical de mi país.

Y si me preguntan en **dónde está el Maestro, podría responder que está en todos los profesores de música que imparten con mucho amor y paciencia sus clases** a niños y jóvenes de distintas regiones y estratos socioeconómicos; también está en todos los músicos que lo recuerdan y que interpretan sus obras continuamente; su espíritu vive a través de cada joven que lo recordó el pasado 11 de septiembre y, por supuesto, el Maestro está en todo niño chileno con ansias de aprender, investigar y experimentar. 

Todo niño tiene derecho a conocer la música y las artes en general y estoy segura de que **este legado dejado por el Maestro seguirá latente mientras transcurran los años** y ahora la responsabilidad de compartir la música desde el amor, inclusión y vocación es de todos nosotros los músicos chilenos.

<!--EndFragment-->