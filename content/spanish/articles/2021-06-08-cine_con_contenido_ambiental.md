---
title: "Cine con contenido ambiental "
subtitle: "Películas que advierten de las consecuencias de la crisis climática "
cover: /images/uploads/cine-verde-1-de-1.jpg
date: 2021-06-07T23:53:02.184Z
authors:
  - Yuliana Fuentes Fuguet
tags:
  - Cultura
  - Cine
  - Ambiente
categories:
  - Cultura
comments: true
sticky: true
---
Los documentales y las películas relacionadas con el medioambiente son una buena forma de profundizar en la importancia de tomar conciencia como sociedad y ocuparnos cada vez más en salvar nuestro planeta. Ecofeminismo, derechos humanos, madre tierra, soberanía alimentaria, somos agua y econciencia, son algunas de las temáticas principales que aborda el **cine ambiental o cine verde**, un género cinematográfico en expansión.

Es sorprendente que el cambio climático actualmente sigua pareciendo para muchos un problema lejano, solo una amenaza. La realidad es que la vida de muchas comunidades enteras de personas y animales en todo el mundo están siendo afectadas.

Cada vez es más necesario que los ciudadanos y las ciudadanas, así como las diversas organizaciones sociales y ambientalistas nos comprometamos a seguir produciendo y consumiendo de forma más responsable. Para que todo esto sea posible se necesita una transformación de conciencia que nos permita aprovechar los recursos naturales de manera consciente, es decir, reconociendo el impacto ambiental de nuestras acciones y las consecuencias que éste ejerce sobre nuestra vida cotidiana.

Películas que advierten de las consecuencias de la crisis climática y documentales que muestran la difícil relación de la humanidad con la naturaleza, son algunas de las temáticas que seleccioné para hacer un acercamiento al cine ambientalista.

### ¿Qué tiene que ver la producción industrial de carne con el cambio climático?

Dir: Vacabonsai Colectivo Audiovisual

ARGENTINA / 2020 / 4´55´´ / ANIMACIÓN

Comer carne es un símbolo de estatus en la sociedad de consumo. Este video animado analiza cómo funciona la producción de carne a escala industrial, los modos de producción que se utilizan, los procesos tecnológicos que intervienen y todas las formas en las que la industria opera financieramente. También describe las consecuencias de la producción industrial de la carne y cómo se relaciona con la crisis del cambio climático.

{{< youtube pY6LefL7Ceo >}}

### **Cholitas**

Dir: Jaime Murciego, Pablo Iraburu

ESPAÑA / 2019 / 80´ / DOCUMENTAL

Cinco mujeres indígenas bolivianas protagonizan una expedición única. Como símbolo de liberación y empoderamiento, se proponen escalar El Aconcagua, la montaña más alta de América. Su imagen es sorprendente: escalan vistiendo su falda tradicional. Son algo más que escaladoras, son mujeres valientes que
encuentran en la montaña un espacio para sentirse libres, felices y vivas. Su aventura mostrará al mundo una manera inspiradora de ser mujer, de vivir la tradición y de relacionarse con la madre naturaleza.

{{< youtube _21ygA8CXNY >}}

### **Interdependencia**

Dir: Adelina Von Fürstenberg

BRASIL EN COPRODUCCIÓN CON AFGANISTÁN, CHAD, ISLANDIA, MARRUECOS, PORTUGAL, SUIZA / 2020 / 99´ / FICCIÓN

La alegría es la vieja paradoja entre el ser humano y la naturaleza. Hace mucho tiempo, las personas se separaron de su entorno natural y desde entonces han sufrido la falta del sentimiento esencial de unidad y pertenencia. Este film poético acepta la brutalidad como parte del mundo para así experimentar la unidad que nos libera de los sufrimientos del aislamiento. Todos somos parte de las mismas luchas y armonía, de esta danza frenética de euforia y aceptación.

{{< youtube kPd-4_dDNeI >}}

### **Alegría**

Dir: Katalin Egely

HUNGRÍA EN COPRODUCCIÓN CON ARGENTINA / 2018 / 4′ / ANIMACIÓN

La alegría es la vieja paradoja entre el ser humano y la naturaleza. Hace mucho tiempo, las personas se separaron de su entorno natural y desde entonces han sufrido la falta del sentimiento esencial de unidad y pertenencia. Este film poético acepta la brutalidad como parte del mundo para así experimentar la unidad que nos libera de los sufrimientos del aislamiento. Todos somos parte de las mismas luchas y armonía, de esta danza frenética de euforia y aceptación.

{{< youtube uXBj2SQUvfg >}}

### **E – Transición y Soberaní­a / Capí­tulo 1 / Comodoro Rivadavia**

Vacabonsai Colectivo Audiovisual

ARGENTINA / 2019 / 8’15” / DOCUMENTAL

Comodoro Rivadavia es una ciudad construida sobre un yacimiento hidrocarburífero. Su centenaria historia, recorrida de la mano con la explotación petrolera, muestra que los únicos derrames son de hidrocarburos. Las desigualdades que aparecen en este territorio están latentes, a partir de una industria que ordena la vida de quienes lo habitan. Comodoro es una metáfora del mundo, en donde los impactos de la quema de combustibles fósiles está llegando a un punto crítico para el ambiente.

{{< youtube NGDNyZtgnao >}}

### **Onde Tem Índio Tem Floresta/Donde hay indios, hay selva**

Dir: Fausto Alves Junior

BRASIL / 2019 / 3´ 33´ / VIDEOARTE

La preservación de las selvas está directamente relacionada con la presencia de los pueblos originarios, que poseen conocimientos ancestrales sobre cómo vivir en armonía y unidad con el medio ambiente natural que los rodea. El título de la película es transparente y refleja la obviedad que muchas personas insisten en ignorar: Onde Tem Indio Tem Floresta

{{< youtube gnKFM_0KMgc >}}

### **La vuelta al campo, luchas campesinas por el buen vivir**

Dir: Juan Pablo Lepore

ARGENTINA EN COPRODUCCIÓN CON BRASIL / 2020 / 73´ / DOCUMENTAL

Este documental narra las historias de diferentes movimientos sociales que reivindican la ´vuelta al campo´ como parte de su lucha por la justicia, la democracia y la libertad tras ser expulsados del sistema como consecuencia de las políticas neoliberales. Los campesinos, que fueron desalojados por el avance de la frontera agropecuaria, vuelven a la ruralidad para construir una alternativa frente a tanta desigualdad.

{{< youtube WW3vcKwYr4o >}}

### **Sumercé**

Dir: Victoria Solano Ortega

COLOMBIA EN COPRODUCCIÓN CON REINO UNIDO / 2019 / 83′ / DOCUMENTAL

Como lo hizo en su ópera prima 9.70, Victoria Solano vuelve a plantearse en Sumercé, su segundo largometraje, los derechos y la soberanía alimentaria en nuestra región, especialmente en Colombia. En esta ocasión sigue al veterano activista Don Eduardo, al líder político en ascenso César Pachón y a la líder campesina Rosita en su lucha contra la decisión del gobierno de permitir que compañías mineras exploten las zonas donde nace el agua dulce de Colombia: los páramos.

{{< youtube gF9fh_bLP90 >}}

### **Mañana (Tomorrow)**

Dir Cyril Dion y Mélanie Laurent. 2016

La actriz Mélanie Laurent viaja por el mundo para conocer diferentes proyectos que quieren revertir los daños ambientales infligidos al planeta. Se trata de proyectos relacionados con la agricultura, la energía, la economía, la educación y la gobernanza. El film transmite un mensaje positivo e invita a descubrir iniciativas que cada uno de nosotros y nosotras podríamos llevar a cabo para ayudar al medio ambiente. Documental disponible en Filmin.

{{< youtube bmL89rs0mSI >}}

### **Tierra prometida (Promised Land)**

Dir. Gus Van Sant. 2013

Dos compañeros de ventas corporativas llegan a una pequeña ciudad para negociar los derechos de perforación de una compañía de gas natural. Creen que con el declive económico la gente del pueblo aceptará cualquier oferta que se les haga. No obstante, no cuentan con las objeciones de un respetado maestro y líder de una campaña contra la empresa.

{{< youtube r8JESA2rlrA >}}

### **Bicicletas vs. coches (Bikes vs. cars)**

Dir. Fredrik Gertten. 2015

Estamos inmersos en una gran crisis global debida al cambio climático, el agotamiento de los recursos fósiles y la contaminación urbana y medioambiental. La bicicleta es una gran herramienta para el cambio, pero la industria del automóvil invierte anualmente millones de euros para favorecer su sector. Este documental nos muestra cómo diversos activistas y pensadores reivindican una movilidad sostenible mediante la bicicleta para lograr ciudades más sostenibles. Plataformas donde se puede ver: Filmin.

{{< youtube hImnXB9CqfM >}}