---
title: Lo que un bosque hace por nosotros
subtitle: " "
cover: /images/uploads/portada.jpg
date: 2020-10-30T11:29:41.099Z
authors:
  - Iris Mendizábal
tags:
  - Conservación
  - Bosques
  - OrdenamientoTerritorial
  - Argentina
  - Naturaleza
  - Salud
  - MedioAmbiente
  - Ambiente
  - Habitat
  - CalidadDeVida
  - Vida
  - Territorio
  - ReservaNatural
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

La página web del “National Center for Biotechnology Information” (uno de los sitios bioinformáticos más consultados del mundo en el ámbito científico-biotecnológico) arroja nada menos que **20.586 resultados ante la búsqueda de información relacionada con terapia de bosques.** Para el mundo occidental, para todos aquellos escépticos cuando se les habla de naturaleza y para quienes piensan que el tema es demasiado espiritual como para prestarle atención, cabe aclarar que todos los artículos se corresponden con investigaciones científicas en las que, valga la redundancia, se siguió un método estricto de evaluación, se plantearon hipótesis susceptibles de ser refutadas, se tomaron muestras representativas de lo que se pretendió estudiar y los resultados se ajustaron con valores cuantitativos que fueron interpretados con los modos de la ciencia: fría, rigurosa y objetivamente.

En otras palabras, **son investigaciones académicas serias, publicadas para la comunidad científica en uno de los sitios más importantes del mundo en el tema.** Con esto quiero decir que, más allá de que podamos despreciar su importancia al final del día y más allá de que los poderosos de los países puedan ver en ellos sólo fuente de rédito para algún gran emprendimiento, **para el estrecho círculo de estudiosos del mundo, los bosques son importantes.**

Entre todas las investigaciones publicadas (y disponibles para cualquiera que quiera zambullirse en las aguas de los datos “duros”) pretendo destacar algunas en particular, dada la sencillez de su método y la contundencia de sus resultados.

En una de ellas, grupos de personas de diferentes edades, géneros y ocupaciones debieron emprender caminatas por zonas de bosques y por áreas urbanas. Antes y después de los paseos, los investigadores tomaron datos de los valores que los individuos presentaban en la variación del ritmo cardíaco (asociada con actividad del sistema nervioso simpático y parasimpático \[relacionado con el estrés y la relajación, respectivamente]), la tensión arterial y los niveles de cortisol salivales (hormona relacionada con el estrés). En todos los casos, los resultados indicaron un aumento del estado de relajación fisiológica de los participantes (traducido en valores menores de tensión arterial y de niveles de cortisol) cuando regresaban de los paseos por el bosque en comparación con las caminatas urbanas. `[1]`

De manera más particular pero similar, un grupo de personas con hipertensión participó de una investigación en la que se le propuso realizar caminatas por los senderos de un bosque de coníferas, así como también por las calles de una zona urbana. Se encontraron cambios notorios en los valores de tensión arterial, con presión sistólica de 123.9 mmHg (caminata por el bosque) sobre 140.1 mm Hg (caminata en zonas urbanas), y presión diastólica de 76.6 mmHg (caminata por el bosque) sobre 84.4 mmHg (caminata en zonas urbanas). `[2]`

Otras investigaciones apuntaron a eliminar el factor de la locomoción del proceso y apostaron a estudiar las mismas variables pero en grupos de personas que fueron expuestas a estímulos visuales relacionados con el bosque, como arreglos florales o imágenes de árboles, durante varios minutos. En otros casos eligieron estudiar los efectos del aroma de los aceites de algunas plantas. Para todos los casos, los resultados revelaron una disminución en los niveles de estrés en las personas al estar frente a imágenes o aromas de la naturaleza, en relación a otro tipo de estímulos. `[3]`

Quizás uno de los datos más interesantes corresponda a un trabajo (Li et al., 2008) en el que se investigó específicamente el efecto sobre las células NK (Natural Killer) que produce la terapia de bosques, en pacientes con función inmunitaria debilitada. Estás células forman parte del sistema inmunológico humano, y juegan un papel crucial en la defensa del cuerpo frente a infecciones, como así también frente a tumores cancerígenos. Los resultados fueron contundentes: (1) el contacto con la naturaleza mejora la actividad de células NK en los participantes, (2) los efectos de esta mejoría se pueden detectar incluso a un mes de haber recibido el estímulo y, (3) ninguna de estas mejorías en la actividad de las células inmunes fueron observadas en poblaciones expuestas a estímulos urbanos. `[4]`

**¿Mi conclusión?, la ciencia nos grita en la cara que estar en contacto con la naturaleza de un bosque mejora nuestra salud y nuestro bienestar.**

Entonces, **¿qué hacemos nosotros por los bosques?**

En Argentina existe la ley n° 26.331, de “Presupuestos mínimos de protección ambiental de bosques nativos” `[5]`. Con sus 44 artículos, su anexo y sus modificaciones pretende establecer criterios claros para la preservación de áreas de bosques nativos, y las condiciones para explotarlos. En el artículo 4 de la misma, se introduce el concepto de “ordenamiento territorial”, mediante el cual se hace un llamado a las provincias para que determinen las áreas de bosques nativos que tienen en sus territorios, y para que las categoricen según indica esta misma ley en el artículo 9: dentro de la categoría I (rojo) se encuentran incluidos “sectores de muy alto valor de conservación que no deben transformarse y que (…) ameritan su persistencia como bosques a perpetuidad.” En la categoría II (amarillo) habitan aquellos “sectores de mediano valor de conservación que (…) con la implementación de actividades de restauración pueden tener un alto valor de conservación”. 

Por último, en la categoría III (verde) se encuentran “sectores de muy bajo valor de conservación que pueden transformarse parcialmente o en su totalidad dentro de los criterios de la presente ley”.

Revisando los datos del ordenamiento territorial que la ley obligó a realizar a las provincias en el momento en que fue aplicada, encontré que tenemos más de 54 millones de hectáreas declaradas como bosque nativo bajo alguna de estas tres categorías `[6]`. **Uno podría decir que cada argentino tiene al menos una hectárea de bosque nativo para sí mismo.** Lo que me resultó interesante fue evaluar de qué modo las provincias asignaron las categorías a sus bosques. Destacan dos casos para mí, por su condición diametralmente opuesta: la provincia de Córdoba, con sus casi tres millones de hectáreas declaradas como bosque nativo, de las cuales ubica al 81,8% en categoría I (rojo), y al 0% de sus hectáreas en categoría III (verde). 

![](/images/uploads/mapa_territorial_cordoba.png)

Esto quiere decir que casi la totalidad de los bosques nativos de Córdoba no se pueden tocar, y que aquellas poquitas hectáreas que sí, deben ser restauradas. Ninguna otra provincia del país cuida sus bosques de esa manera (aunque en estos últimos meses eso no haya sido suficiente). En el otro extremo se encuentra Formosa, con más de cuatro millones de hectáreas declaradas como bosque nativo, pero ubicando en categoría rojo solo al 9% de las mismas, mientras que casi el 75% corresponden a la categoría verde, es decir que es bosque susceptible de ser transformado parcial o totalmente.

![](/images/uploads/mapa_territorial_formosa.png)

Lo cierto es que embellecen el paisaje, regulan la temperatura del planeta y generan las lluvias que reciben los cultivos de los cuales nos alimentamos. **Son el reservorio de biodiversidad más importante que tenemos, a partir del cual podría descubrirse la próxima droga que obsequie la cura para las enfermedades que aquejan al mundo.**

 Por si fuera poco, mejoran nuestros indicadores de salud. Sin quererlo pero sin oponerse, ofrecen los recursos naturales que explotamos para mantener el estilo de vida que elegimos tener, y además son profundamente resilientes, sólo basta con observar algunos de esos árboles que talados hasta la base, siguen ganándole a la muerte y vuelven a brotar. **Si todo eso no es suficiente para que dejemos de atropellarlos, quemarlos y destruirlos, entonces ya no sé qué lo es…**

`[1]` – Park BJ; Tsunetsung Y; Kasetani T; Kagawa T; Miyazaki Y. The physiological effects of Shinrin-yoku (taking in the forest atmosphere or forest bathing): Evidence from field experiments in 24 forests across Japan. Environ. Health Prev. Med. 2010

`[2]` – Ochiai H; Ikei H; Song C; Kobayashi M; Miura T; Kagawa T; Li Q; Kumeda S; Imai M; et al. Physiological and psychological effects of forest therapy on middle-age males with high-normal blood presure. Int. J. Environ. Res. Public Health 2015

`[3]`– I kei H; et al. The physiological relaxing effects of visual stimulation with foliage plants in high school students. Adv. Hortic. Sci. 2014

`[4]` – Li Q; Morimoto K; Nakadai A; et al. Forest bathing enhaces human natural killer activity and expression of anti-cancer proteins. Int. J. Immunopathol. Pharmacol. 2008

`[5]`– Ley Nacional n° 26.331 de “Presupuestos mínimos para la protección ambiental de los bosques nativos” (argentina.gob.ar)

`[6]` – CREA- Mapa de ordenamiento territorial.

<!--EndFragment-->