---
title: "Selección de Poemas Inéditos "
subtitle: " Fermina Ponce "
cover: /images/uploads/trees-5605176_1280.jpg
date: 2021-04-30T14:46:10.556Z
authors:
  - Fermina Ponce
tags:
  - Genero
  - Cuerpo
  - territorio
  - poesia
  - cultura
  - poemas
categories:
  - Genero
  - Cultura
  - Cuerpo
  - Territorio
comments: true
sticky: true
---


### A ciegas

Sin luz en la mirada

escribiré mi mejor poema sobre tu cuerpo

con la humedad de mis besos

indeleble

mi marca de “mía” aunque otras manos te toquen

mía en tu todo con mi universo

en tu vientre

y mi amor en tus rodillas.





### La “larguitud” de las horas

Ya es otoño y los árboles se dedican al abandono

se alistan para lo que será el invierno

los días se hacen más cortos y oscuros

todo se confina

el sol brilla

es de los venados.

Ya es otoño y los días parecen acoplarse a una monotonía

los días se llenan de horas apeñuscadas hasta las dos y media de la tarde

la longitud de las horas parece eterna

sé que es infinita

no puedo llenarla con lo que me habita

mis palabras son insuficientes

los quehaceres se van llenando de un tedio insoportable.

Ya es otoño y la “larguitud” de las horas hace trifulca en mi cabeza

las horas me ahorcan

me hacen vaciar el optimismo de varios días

bailan en un sólo cuadro

ya no cabe la clepsidra.



### También soy el poema



Tus besos besaron los besos dormidos

de plumas y sangre llenos están

tu piel constelación infinita en la mitad de un mírame a los ojos

tus ojos y mis ojos son lunas llenas de plata y rosa

tu olor se inmola en mi espalda como una ofrenda

tu aliento se quedó en mi cuello

en la mitad de la calle de mis hombros

cerca de la esquina de mi último pensamiento nocturno

soy pavimento de un poema.



#### Fermina Ponce – (Bogotá, Colombia 1972)

Comunicadora Social y periodista, y Máster en gerencia de la comunicación organizacional Universidad de la Sabana, Bogotá. Máster en escritura creativa Universidad de Salamanca, España. Sus poemarios Al desnudo y Mar de (L)una, obtuvieron mención de honor en los premios ILBA 2018 (International Latino Books Awards) e ILBA 2019, sucesivamente. Fermina aborda temas universales; habla de la naturaleza humana, de sus dualidades, incluyendo las enfermedades mentales como en su obra Poemas SIN NOMBRE. Ponce incursionó en la prosa con dos cuentos publicados por MAGMA editorial (2019), España. Y en 2021 es parte de la de la compilación de cuentos Féminas Antología de infidelidades y mentiras escritas por mujeres, Editorial Ars Communis. A principios de 2020 recibió el nombramiento de Diputada Poeta Laureada de Aurora, Illinois, ciudad en la que reside. Fue nominada por el Consulado Colombiano en Chicago al “Premio Los 22 más” en 2017, en la categoría de cultura, por su contribución cultural en el área de Chicago. La poesía de Ponce ha sido publicada al francés para la revista La Piraña, México.