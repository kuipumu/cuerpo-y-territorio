---
title: "San Andrés y Providencia: las islas más paradisíacas de Colombia en
  medio de la adversidad"
subtitle: " "
cover: /images/uploads/tatiana-zanon-rr8jv5whfro-unsplash.jpg
caption: "Foto: Tatiana Zanón/Unplash - San Andrés"
date: 2020-11-29T21:01:22.794Z
authors:
  - Valentina Mena Castro
tags:
  - Colombia
  - SanAndres
  - Providencia
  - Islas
  - Historia
  - Latinoamerica
  - HuracánLota
  - DesastreNatural
  - Covid19
  - Turismo
  - Economía
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

La historia de San Andrés, Providencia y Santa Catalina con el territorio colombiano ha sido exótica, ya que a pesar de ser parte de este territorio desde la época de la Nueva Granada, **su historia no es enseñada en las aulas de las escuelas ni en las universidades.** Es así como el relato histórico de estas islas es contado desde adentro de las mismas y muchos de nosotros como colombianos no comprendemos **cómo los isleños siguen queriendo ser parte de Colombia, cuando la mayoría de su comercio y de sus entradas económicas provienen de países de Centroamérica como Guatemala, Panamá y Nicaragua.** Por ello, es necesario retomar a una sanandresana que cuenta a partir de sus experiencias la historia de su origen.

Hazel Robinson Abrahams (2010) escribió desde los años cincuenta y sesenta crónicas en el periódico El Espectador. Allí, narraba diferentes hitos de la historia sanandresana en un tono de novela originaria muy romántico, retomando desde la abolición de la esclavitud que llegó de la mano de los neogranadinos; **por esta razón, se encuentran en gran “deuda” ya que somos el único territorio que se ha importado por sus condiciones a pesar de la lejanía de Tierra firme.** Este relato tiene la particularidad de empezar tras un gran huracán que azota las islas, representando un nuevo inicio tras la abolición de la esclavitud.

San Andrés es reconocida por sus diferentes rutas comerciales siendo un puerto muy importante para los británicos que llegaron a la isla en 1630, acompañados de los primeros africanos esclavos a quienes obligaron a plantar algodón, reemplazando el coco que naturalmente creció en la isla. Estas plantaciones de algodón estaban acompañadas de siembras de piña, fruta de pan, algunas hortalizas y una mínima cantidad de coco.

**Los cambios culturales fueron tremendamente importantes para el desarrollo de los nuevos habitantes de la isla**, ya que los colonos británicos trajeron la religión, practicando el evangelismo por medio de la iglesia Bautista. Esto representaba un nuevo reto para los peregrinos, ya que buscaban la “salvación de las almas” de los esclavos. Sin embargo, esto no se logró totalmente debido a que los esclavos, ahora isleños, mantenían sus prácticas y creencias africanas. De esta forma, nace un sincretismo entre sus creencias junto con las del evangelismo creando su propia visión de la religión acompañadas de cantos en su lenguaje, ya que éste era lo único que les quedaba de lo que alguna vez fue su hogar.

**Viajando en el tiempo, a partir de 1953 la Isla fue declarada Puerto Libre de Impuestos,** lo que significó una oleada de turistas que llegan cada año y en mayor cantidad buscando por una parte, el mar de los siete colores de San Andrés y las hermosas y exuberantes playas en medio de la jungla de Providencia, acompañadas de una gran cantidad de productos sin impuestos. En la actualidad, **el 80% de la isla se encuentra dedicada al turismo**; aún sobreviven algunas plantaciones de fruta de pan y de unos pocos víveres. Sin embargo, la mayoría de ellos provienen de Panamá, Guatemala, Nicaragua, Estados Unidos y por supuesto de Colombia.

Las hermosas playas sanandresanas y providencianas son reconocidas por tener la arena más blanca que puedes encontrar en el país, proclamando que por su gran belleza y abundantes recursos naturales (en donde se incluyen ecosistemas de manglar y la gran barrera de coral) **sea declarado el lugar No.1 del turismo en Colombia durante 2019. Pero con la llegada del 2020 todo esto cambió.**

A causa de la gran cantidad de turistas, **la crisis del COVID-19 llega a las islas el 22 de marzo de 2020.** Durante el pico de la pandemia, la situación se agudiza cada vez más ya que con el cierre del aeropuerto Gustavo Rojas Pinilla y de los puertos marítimos, no había forma de que la economía funcionase normalmente. Junto esto, **desde hace años los isleños se vienen quejando de la falta de recursos médicos para el hospital general de la isla,** haciendo que los casos que existieran no tuviesen tratamiento, incrementando así los casos y no hubiese forma de atender a todas las personas contagiadas.

Una vez los casos bajaron, el turismo se reactivó, el aeropuerto se abrió de nuevo a los turistas que después de pasar 100 días encerrados estaban buscando las aventuras proporcionadas por las dos Islas paradisíacas del Caribe. Sin embargo, con la llegada de turistas los casos volvieron a aparecer y los isleños volvieron a entrar en la misma crisis de la que habían salido tan sólo unas semanas antes.

Pero las desgracias no cesaron para estos dos territorios marítimos, **el huracán Lota pasó por la isla de San Andrés y devastó las islas de Providencia y Santa Catalina el 17 de noviembre de 2020 dejándolas destruidas en un 90% tanto las viviendas como también los recursos naturales.** Desde ese entonces hasta la actualidad, la migración hacia diferentes partes del territorio colombiano por parte de los providencianos ha sido masiva, las personas prefieren dejar la isla a tener que reconstruir las cenizas que dejó a su paso del huracán. No obstante, una de las características más importantes de los isleños, como narra Hazel Robinson, es que después de que un huracán azota a las islas llega un momento de transformación y, como la primera vez, **los y las colombianos y colombianas estaremos apoyando a que se reconstruyan con el fin de que vuelvan los isleños a su paraíso en medio del mar de los siete colores.**

Referencias y Bibliografía:

[AS.com (2020) Coronavirus en Colombia: resumen y noticias de hoy, 22 de marzo.](<https://colombia.as.com/colombia/2020/03/22/tikitakas/1584876058_755945.html>) 

[El Tiempo (2020) San Andrés, en crisis sanitaria por aumento de contagios de covid-19. ](<https://www.eltiempo.com/colombia/otras-ciudades/san-andres-crisis-sanitaria-por-aumento-de-contagio-de-covid-19-540521>)

[Ministerio de Salud (2020) Isla de San Andrés pasa a afectación alta de covid-19. ](<https://www.minsalud.gov.co/Paginas/Isla-de-San-Andres-pasa-a-afectaci%C3%B3n-alta-de-covid-19.aspx>)

[Portafolio (2020) Devastación en San Andrés y Providencia tras el paso del huracán Lota.](<https://www.portafolio.co/economia/san-andres-y-providencia-devastacion-tras-el-paso-del-huracan-iota-noticias-san-andres-546689>) 

[Radio Nacional de Colombia (2020) San Andrés: de cero casos de Covid-19 a dos nuevos contagios.](<https://www.radionacional.co/noticia/covid-san-andres>)

Robinson, Hazel (2010) ¡No give up man! Bogotá, Colombia, Ministerio de Cultura.