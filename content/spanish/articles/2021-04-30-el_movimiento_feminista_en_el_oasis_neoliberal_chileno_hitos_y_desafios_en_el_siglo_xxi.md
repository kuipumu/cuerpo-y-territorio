---
title: "El Movimiento Feminista en el “oasis” neoliberal chileno: hitos y
  desafíos en el siglo XXI (Parte I) "
subtitle: "Un acercamiento histórico "
cover: /images/uploads/resena_feminismo_siglo_xx.jpg
date: 2021-04-30T15:23:21.327Z
authors:
  - Constanza Vega y Jessabel Guamán Flores
tags:
  - Feminismo
  - Genero
  - Mujeres
  - Movimiento
categories:
  - Genero
  - Cuerpo
  - Territorio
comments: true
sticky: true
---
### Una breve reseña: la trayectoria histórica del movimiento

El pasado 8M, la Red de Historiadoras Feministas (http:/ redhistoriadorasfeministas.cl) de Chile, lanzó la publicación del libro “Históricas. Movimientos Feministas y de Mujeres en Chile (1850-2020)”. Este libro contrapone la idea de los “silencios feministas” (Ríos, Godoy y Guerrero, 2020), dando cuenta de una trayectoria histórica que se reconoce desde los movimientos de mujeres a finales del siglo XIX los cuales dieron un impulso para la conformación de movimientos propiamente feministas desde principios del siglo XX, y que tienen como vanguardia la prensa de obreras (Colectivo Catrileo+Carrión, 2018); las sindicalistas y anarquistas (Lagos, 2017). Más adelante, se reconoce el ingreso y participación de mujeres en el sistema de partidos (Sartori, 1980): comunistas, socialistas, y también se forman partidos únicamente conformados por mujeres como el Partido Cívico Femenino de los años veinte y el Partido Femenino de Chile en los años cuarenta.

La participación de las mujeres en los partidos políticos mencionados evidencia una apertura de democratización de la sociedad chilena en esas décadas, y los intereses y demandas resguardados por tanto tiempo en la esfera privada pasan a la esfera pública (Lavrin, 2005). Si bien en estas organizaciones de partidos las mujeres pusieron en la mesa temas alusivos a los derechos civiles, laborales y políticos, será el Movimiento Pro Emancipación de las Mujeres de Chile (MEMCH) surgido en 1935 el que se caracterizó por un feminismo militante que manifestó explícitamente una postura **sobre el aborto como derecho sexual y reproductivo,** y al divorcio como bandera de liberación contra el abuso y violencia hacia las mujeres (MEMCH, 1938). 

El MEMCH fue un movimiento que deﬁnitivamente no se identiﬁcó como defensor del rol tradicional de la mujer pero su acción siempre fue respetuosa con las demás organizaciones femeninas (Eltit,2018) y lidera indiscutiblemente la lucha sufragista promoviendo junto a otras agrupaciones de mujeres la Federación de Instituciones Femeninas de Chile(FECHIF), la más grande coalición de mujeres en una deﬁnitiva señal de obtener un derecho civil que demoró más de veinte años hasta ser promulgado recién en 1949.

**El voto “universal” para las ciudadanas chilenas surge en los primeros años de la Guerra Fría** ( Huneeus, 2009) , con un costo elevado para algunas militantes del MEMCH y del Partido comunista que fueron expulsadas de la FECHIF y por supuesto, la alianza femenina quebrada por la determinación política del Presidente Gabriel González Videla que un año antes proscribió al PCChileno y marginó a la líder sufragista Elena Caffarena de la ceremonia por su coherencia y defensa hacia las compañeras expulsadas. 

En deﬁnitiva, las determinaciones de partidos políticos en que los hombres eran la primera voz, llevaron a que las mujeres de dichos partidos y partícipes de la FECHIF acataron y aplicaron una praxis antidemocrática contra quienes fueron sus compañeras de organización (Vega, 2018). Este hecho daría varias décadas después, luces de reﬂexión a las feministas chilenas sobre su participación en organizaciones políticas mixtas. La visibilidad de las mujeres en la institucionalidad política quizás no fue tan mediática en los años cincuenta o sesenta, pero ello no signiﬁca que **hayan dejado su participación en esos espacios.**

Lo que ocurre en los años setenta como reﬂexiona Julieta Kirwood (1986) es que, si bien existe una activa participación en partidos u organizaciones de inspiración revolucionaria, “los partidos de izquierda en ese periodo logran con diﬁcultad expresar la problemática femenina. La desconocen, presumen que no existe. Las mujeres mismas desde la izquierda tampoco lo admiten: ya se han integrado a protestar por cambios en la sociedad y no hablan más de problemas femeninos” (Kirkwood, 1986).

 **Las “compañeras” ven rotos sus anhelos y luchas con el golpe de Estado de 1973,** pero será el periodo de la dictadura cívico-militar donde estaban proscritos los partidos políticos, cuando las mujeres comenzaron a “mirarse políticamente desde otro lugar” (Gálvez, 2018), activando y organizándose como mujeres en resistencia. Una vez puesto ﬁn el régimen de Augusto Pinochet tras el plebiscito, se abre el periodo conocido como la transición a la democracia y el movimiento feminista experimenta una fractura importante de mencionar ya que pueden reconocerse huellas hasta nuestros días: la oposición de las feministas radicales (Pisano,2015) y autónomas (Lidid, 1997) a la inserción de otras feministas en el sistema político tradicional (Gálvez, 2018) o institucional como es el caso de las ONGés, es decir, la cuestión sobre la representatividad del movimiento feminista que se vio notoriamente evidenciado en el VII Encuentro Feminista Latinoamericano y del Caribe en la Ciudad de Cartagena a mediados de los años noventa ( Alvarado y Vega, 2021) y que tuvo una línea continua hasta ﬁn de siglo.

### Feminismo en la cuna del laboratorio neoliberal

El modelo político y económico de Chile, y en deﬁnitiva, la sociedad chilena, experimentó lo que se denominó un **“estallido social” en octubre de 2019**: una revuelta popular y ciudadana que tuvo como premisa la frase “el neoliberalismo nace y muere en Chile”, dando cuenta que el origen de las demandas que hicieron sacudir a la institucionalidad y al gobierno de derecha de Sebastián Piñera tenía sus antecedentes décadas previas: el oasis sudamericano jamás existió (https:/ cutt.ly/5vU6LDa).

Acorde al planteamiento del historiador Luis Corvalán Márquez (2018), se sostiene que el proyecto global del gran empresariado formado y adoctrinado por la escuela neoliberal de Milton Friedman con sede en la Universidad de Chicago se fundó en una dictadura feroz, pero se consolidó desde el periodo de transición a la democracia: la contrarrevolución de la derecha chilena (Valdivia, Pinto y ´Álvarez, 2006) dio lugar a un nuevo Chile, no para ejercer ciudadanía, sino para ejercer capacidad de consumo, en otras palabras, toda la institucionalidad, todos los derechos, el tejido social por completo regidos por la lógica de mercado, donde existe abundancia para pocos, y precarización para las mayorías (Moulián, 1997).

Considerando lo anterior, y desde una postura crítica feminista sostenemos que si la sociedad chilena se ha visto afectada por el régimen neoliberal sostenido por los pilares de una constitución de origen dictatorial (Grez, 2015) y moralista, **las mujeres, las disidencias y los pueblos indígenas lo han padecido aún peor.**

Antes del “estallido social” chileno, el 2018 se vio sacudido por lo que llamaron el “mayo feminista” o la revolución feminista, cuando vieron a miles de mujeres en las calles advirtiendo que no nos callábamos más: comenzó una agitación mediática que parecía desconocer el largo aliento de luchas del movimiento feminista en este territorio enunciadas en la reseña.



#### Constanza Vega Neira

Profesora de Historia y Ciencias Sociales; Licenciada en Historia por la Universidad de Valparaíso; Diplomada en Cultura, Política y Sociedad en América Latina y Magíster en Historia de la Universidad de Chile.

Línea de investigación en temáticas sobre Historia Política, Historia Reciente e Historia Local, enfocada en movimientos políticos, sociales y de mujeres; Feminismo; Memoria y Derechos Humanos. Integrante de la Red de Historiadoras Feministas, la Asociación Latinoamericana de Historia y la Sociedad de Historia de San Antonio.

Lectura de alguno de sus estudios y seminarios pueden ser leídos en <https://independent.academia.edu/ConstanzaVega16>

#### Jessabel Guamán Flores

Descendiente quechua.Magíster en Historia, diplomada en educación, memoria y ddhh y en Lingüística y culturas indígenas en la Universidad de Chile. Profesora de Estado en Historia y Geografía y Licenciada en educación por la Universidad de La Serena. 

Desde el año 2013 se ha dedicado a investigar en las temáticas referidas a la memoria, historia regional reciente, pueblos originarios del norte de Chile y género, siendo integrante del grupo de trabajo Kuifike ,red de historiadoras Feministas y tutora y ayudante del diplomado de educación, memoria y DDHH, Universidad de Chile. Sus trabajos pueden ser leídos en <https://uchile.academia.edu/JessabelLeticiaGuam%C3%A1nFlores> y visitar su página de difusión en instagram de eventos históricos, culturales y educativos: @dialogosobrehistoria.