---
title: Las doce Malinches o el mito que seremos
subtitle: " "
cover: /images/uploads/93b41037-a969-4fc1-bccf-4f68c000fb95.jpeg
caption: "Imagen: Desinformémonos Mx"
date: 2020-10-10T13:53:02.185Z
authors:
  - Roberto Salazar
tags:
  - EdiciónEspecial
  - 12deoctubre
  - Identidad
  - Latinoamerica
  - Continente
  - América
  - Historia
  - Cultura
  - PueblosOriginarios
  - Malinche
  - México
  - Violencia
  - Mujeres
  - Feminismo
  - Lucha
categories:
  - Cuerpo
comments: true
sticky: true
---
<!--StartFragment-->

1. Para los desprevenidos que no conocen a ninguna de las Malinches: mujer azteca, náhuatl para más señas. Se cree que nació 1500 años después de Cristo, en algún lugar de lo que es hoy Veracruz, México. Nombrada Malinalli, en honor a la diosa de la hierba. Hija de un importante miembro de su tribu, al morir este es vendida por su madre siendo todavía una niña al estar en riesgo su relación con su nuevo marido, por ser memoria de una relación anterior. Tuvo que aprender idioma maya para hablar con sus nuevos dueños, puesto que Malinalli fue, desde el vamos, propiedad de los hombres.
2. La Malinalli más recordada es aquella mujer que fungió de intérprete para la empresa de Conquista de Hernán Cortés. Regalada como una de las 20 mujeres de ofrenda a los hombres blancos llegados del este, Cortés la elige para que le acompañe por su facilidad para dominar las lenguas de la región y así ampliar los dominios de la Corona. Sabía Cortés que no bastaba con sus avanzados arcabuces o sus relucientes armaduras. Necesitaba imponerse con la lengua, con el signo. Más puede la maña que la fuerza, reza el viejo dicho. Pues en el centro de la astucia cortesiana, está Malinalli. La apodada por su pueblo Tenepal, que significa “la que habla con vitalidad”. La que fue descrita como “entremetida y desenvuelta” por los españoles. La lengua de Cortés.
3. Malinalli Tenepal se ve obligada a aprender castellano. Lo hace en tiempo record. Es bautizada como manda la tradición católica europea. Marina es su nombre de pila bautismal. No será la primera nativa bautizada, pero sí la más importante desde el 12 de octubre de 1492. Marina servirá a Cortés en la caída del imperio azteca para fundar otro imperio y otra religión: el español y el cristianismo. Quizás esa sea la historia de los pueblos latinoamericano. Una imposición tras otra de ideas, tradiciones y formas de vida por el vencedor.
4. La preeminencia de Marina la convierte en doña Marina. Para los originarios, pasa a ser ya Malintzin, con ese sufijo que podríamos traducir que significa señora Malinalli. Malintzin es tomada como mujer por Cortés del cual engendrará un hijo, Martín Cortés. Martín será el mestizo con más abolengo del mundo hispánico. La confluencia de lo europeo y lo americano. Malitzin es, para México y para Latinoamérica, la madre de la confluencia de los dos mundos. Todos somos trasuntos simbólicos de Martín Cortés. Cortés era un hombre casado por lo cual Martín nace con una legitimidad cuestionada. A Malintzin la casan luego con Juan Jaramillo y no supimos mucho más de ella luego de eso, salvo que tuvo un segundo hijo, María.
5. No podemos obviar que Malinalli-Malintzin-Marina no es tomada por Cortes. Es violada. Como nos remacha Octavio Paz, somos todos hijos de la chingada, de la violada. A Malitzin no le quedó alternativa si no consentir a ser propiedad de Cortes, amante de Cortés y madre del hijo de Cortés. El acceso violento al cuerpo de Marina es accesorio. Malinalli se entrega a Cortés con aquella sumisión propia de la subordinación. La violación de Marina es similar a la que nos describe Neruda en Ceilán con una mucama. El encuentro de un hombre con una estatua.

   ![](/images/uploads/03-7-850x585.jpg)
6. Y no contentos con ser hijos de la chingada, seriamos según Paz hijos de la traidora. La que abandona su cultura originaria, la que vende a su pueblo, la que engaña a todo un continente por vaya usted a saber qué razones. Venganza, oportunismo o supervivencia. El séptimo círculo del infierno es para los traidores, dice Dante. La Eva del Nuevo Mundo. Símbolo de toda deslealtad patria. El malinchismo es precisamente eso: el abrazar cualquier costumbre extranjera, el abandonar lo propio por lo ajeno. Los malinches son aquellos que, explicando al forastero, son los primeros en ser colonizados. Altercentrismo, según Maritza Montero. Más actual, tenemos el malinchismo del libre mercado, globalización y fuga de divisas.
7. Pero como la historia también puede ser una eterna disputa por los significados, Malinalli es de las primeras rompedoras del patriarcado en ambos lados del Atlántico. Porque pocas veces antes, si es que nunca, una mujer nativa de un pueblo por ser dominado había ocupado el lugar de intérprete oficial y con poder. Aguilar, el otro traductor de Cortés, no tenía la habilidad de la Malinche. Malinche significa “señor de Malinalli” y es usado para el español y no para Malitzin, como se cree. 

   Es decir, Malinche es llamado Cortés a partir del nombre de la mujer mexica. Es una suerte de patronímico invertido. Malintzin se alza a hablar en grandes palacios aztecas, a ver a la cara al emperador, a dar órdenes, actos absolutamente impensados para su cultura de origen y solo reservados para el monarca femenino ibérico que, so pena de un heredero varón nunca llegado, podía llegar a gobernar en Europa. Acusarla de traidora es propio de la vieja tradición judeo-cristiana de la bíblica Eva. Acusarla de chingada solo pone en la palestra el orgullo herido de los hombres que les violaron a sus mujeres, sus antiguas propiedades. Elegir entre un patriarcado u otro, para Malinalli no fue una verdadera elección.
8. Si la Malinche fue mujer, aborigen, intérprete, católica, madre, traidora, la Malinche es signo. Es decir, una representación, un mensaje, una posibilidad. La Malinche nos dice algo. Sabemos que nos habla. La Malinche también es aquella que hace estallar por los aires, al menos una vez, el lugar asignado a la mujer en la cultura mesoamericana. La mujer regalada, la plebeya que se emparenta con Cortés y que se hace señora. No la costilla si no la lengua. La que es hábil interpretando. La que vive entre una cultura y otra. Marina hizo sincretismo entre su vieja religión y la nueva pero Marina no dejó sus ropajes, Marina no fue al viejo mundo y no se sometió a la aculturación de los indios de Colón. La Malinche transcultural: fue todas y ninguna.
9. La Malinche es personaje histórico, pero también mito. La rastreamos en La Llorona y La Virgen de Guadalupe. La interpretamos en Manuela Sáenz y Mariquita Sánchez de Thompson. La Malinche es la inscripción permanente de la mujer en la historia de nuestros pueblos latinoamericanos, reactualizada cada vez que nos violan y nos traicionan, cada vez que destruimos el molde y conquistamos nuevos espacios, cada vez que nos corren de nuestros lugares y nos lo volvemos a ocupar. El malinchismo, lejos de traición es cosmovisión. Es el palpitar de lo que no cesa de resignificarse. Malinalli es disputa por contarnos el mito que somos.
10. Decía Freud que los artistas se nos adelantan en comprender al mundo. Quizás los murales de Diego Rivera nos muestran ambiguamente a una Malinche jefa pero azteca. Paz nos dejó una Malinche irrecuperablemente vejada, Rosario Castellanos a una Malinche hermosamente feminista. La Malinche de la literatura chicana, de esa literatura al border de lo gringo y lo mexicano. Quizás la Malinche literaria más abierta no las lega Carlos Fuentes: de la Malinche pasiva de Todos los gatos son pardos a la Malinche traidora y esperanzada de Las dos orillas. Remata con la Malinche atronadoramente contemporánea de Malintzin de las maquilas. La Malinche es un Boom literario, histórico, semiótico. La Malinche es ficción y es realidad.
11. Todorov planteaba que los españoles se habían apropiado de América a base de una “tecnología del simbolismo”. De su notable capacidad para interpretar lo humano, la comunicación hombre-hombre. Hacían leer los designios de la realidad según su conveniencia a diferencia de los pueblos originarios que tenían una lectura mucho más hombre-naturaleza, en definitiva, más rica y menos antropocéntrica. No hay duda cual cultura prevaleció. Pero lo que tal vez conquistó de una manera tan rápida al imperio azteca fue menos el simbolismo europeo pragmático y más lo que Rita Segato llamó “tecnologías de la sociabilidad”. 

    Aquella experticia en hacerse confiable, en regentar lo íntimo, lo doméstico, lo imaginativo, lo abierto que puede transmitir el arraigo de lo femenino. Cortés no fue exitoso con tan poco por tener una lengua y poder manipular a los nativos. Quizás lo fue porque al final de las cosas, la mujer y ese saber especializado de la proximidad, de lo cálido, permitió combinar la tecnología armamentística y simbólica de lo europeo con los conocimientos que aún 528 años después no sabemos desentrañar y solo transmiten ellas. Malinche tecnóloga. Malinche 2.0.
12. La última Malinche es la que está en todos lados. La que va hacia el presente y el futuro. La propietaria y la asalariada, la capitalina y la provinciana, la lesbiana y la hetero, la blanca y la negra, la hábil y la débil, la que vive y la que está por morir. La que es actualidad y porvenir, cuerpo y alma, mar y tierra. La Malinche es aquel viejo palimpsesto, escritura sobre escritura, genética y epigenética, doce que son una, en la que se traza lo eterno. Es la insistencia de lo insumiso, que sobrevive y que retorna. Invicta siempre.

<!--StartFragment-->

#### ***Otras entregas de esta edición especial “12 de octubre”***

[¡Más que nunca, las venas están abiertas!](https://revistacuerpoyterritorio.com/2020/10/10/mas-que-nunca-las-venas-estan-abiertas/)

[Historias tejidas en los cuerpos y territorios del sur de América Parte I](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america/)

[Historias tejidas en los cuerpos y territorios del sur de América Parte II](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america-parte-ii/)

#### Además, los invitamos a conectarse a la tertulia de YouTube live este 12 de octubre para conversar, desde otras perspectivas, sobre esta fecha tan importante en América haciendo clic [AQUÍ](https://www.youtube.com/watch?v=Bddgfi3tARU&feature=youtu.be)

![](/images/uploads/12_de_octubre_la_historia_no_contada_de_america_9_.png)