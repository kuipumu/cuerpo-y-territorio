---
title: "Congreso de Psicología Ambiental y un balance indispensable del 2020:"
subtitle: " Tejamos redes y cuestionemos todo"
cover: /images/uploads/01somoshumedal.jpg
caption: Manifestación y activismo en Rosario por el ecocidio en el Delta por
  Sebastián Pancheri - Multisectorial humedales.
date: 2021-01-26T02:55:30.287Z
authors:
  - Paula Fausti
tags:
  - Ambiente
  - Argentina
  - Humedales
  - Ecologia
  - Ecocidio
  - Activismo
  - Latinoamérica
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

En los últimos artículos que escribí para Revista Cuerpo y Territorio ([Salud, Psicología Ambiental y Terapia Hortícola](https://revistacuerpoyterritorio.com/2020/10/17/salud-psicologia-ambiental-y-terapia-horticola/), y [El Verde en el Gris](http://./El%20verde%20en%20el%20gris)) estuve hablando sobre la psicología ambiental, con el objetivo de dar a conocer un poco esta disciplina de desarrollo incipiente en Argentina. 

**Entre el 9 y 12 de diciembre del 2020 se llevó a cabo el IV Congreso de Psicología Ambiental, en el marco del CLACIP (Congreso Latinoamericano de Ciencia Psicológica de la Asociación para el Avance de la Ciencia Psicológica), del cual tuve el honor de integrar la comisión organizadora.** A continuación, voy a presentar brevemente los temas que se hablaron durante el congreso, para dar un pantallazo de lo que estudia esta disciplina, y relacionarlos con algunos eventos importantes en materia social y ambiental que surgieron durante el pasado 2020 en el país.

El congreso se realizó en forma gratuita y virtual, con la presencia de numerosas invitadas e invitados de diferentes países. Participaron personas de siete nacionalidades: Argentina, Uruguay, Chile, Brasil, Colombia, Ecuador y España. Además tuvo un carácter marcadamente federal, pues contamos con la participación de expositores, expositoras y participantes de diferentes provincias del país.

Se expusieron numerosos trabajos de investigación, se debatió y conversó sobre diversos temas como el desarrollo sostenible, las transiciones hacia la sostenibilidad, la comunicación de problemáticas socioambientales, las militancias y activismos en relación a ese tema, los ecofeminismos, la terapia hortícola, las huertas comunitarias, la agroecología en ambientes rurales, los conflictos socioambientales y su relación con la salud desde miradas con perspectiva de género, la ecología política, la crisis climática y ecológica, la educación ambiental, los ambientes urbanos y construidos. **Los temas fueron tan variados como sus disertantes, lo que permitió un enriquecimiento recíproco entre quienes participamos del congreso.**

El título para nombrar a este congreso: “Psicología Ambiental en Tiempos de Crisis Civilizatoria” fue elegido, entre otras cuestiones, por el contexto de pandemia que estamos atravesando. **Hubo un alto consenso en que la degradación de los ecosistemas y la pérdida de biodiversidad nos está llevando a un punto de no retorno, donde necesitamos tomar cartas en el asunto antes de que sea demasiado tarde.** Crece la preocupación por el deterioro ambiental y la destrucción de los ambientes naturales, por un lado, y al mismo tiempo en los espacios urbanos surgen tendencias a transicionar hacia formas de vida más sostenibles, intentando contrarrestar los devastadores efectos de la globalización y el capitalismo neoliberal.

![](/images/uploads/02flyercongreso.jpeg)

Mientras se daba el congreso, en Argentina se debatía la Ley de Interrupción Voluntaria del Embarazo en la Cámara de Diputados y el pueblo de la provincia de Chubut se movilizaba una vez más en contra de la mega minería y a favor del agua. 

Este año no sólo vimos nuestras vidas detenerse por la propagación de un virus y la profundización de la crisis socioeconómica, que tiene a más del 40% de la población argentina bajo la línea de pobreza `1`. **Casi un millón de hectáreas fueron incendiadas, hecho que se vio agravado por la gran sequía que hubo. Las fumigaciones y la deforestación continuaron como antes, e incluso se vieron incrementadas, siendo decretadas “actividades esenciales”.** 

En la misma línea el gobierno nacional anunció la **aprobación del primer trigo transgénico del mundo**, cuyo paquete tecnológico incluye un herbicida llamado glufosinato de amonio, prohibido por la Unión Europea por los efectos que causa, entre ellos: intoxicaciones, convulsiones, pérdida de memoria, desequilibrio intestinal, alteración en la respiración. 

Dentro de las flamantes medidas que nos sacarían de la crisis, el gobierno comunicó mediante la Cancillería el **Acuerdo Porcino con China, que promete instalar megafactorías de carne porcina para abastecer el consumo de carne de la población en China.** Esto ignora que las megafábricas de animales, además de ser lugares donde se practica tortura animal, son potenciales fuentes de otras pandemias, utilizan toneladas de agua por día y allí donde se instalan contaminan tierra, aire, agua y todo lo que se encuentra alrededor, convirtiendo esos territorios en las llamadas “zonas de sacrificio”. 

Por otro lado, presenciamos una bajante histórica del Río Paraná y el Delta se vio consumido en llamas, aniquilando no sólo uno de los humedales más importantes que tenemos, sino también llevándose consigo toda la biodiversidad que alberga. El Río de la Plata se tiñó de verde por la presencia de cianobacterias, envenenando aguas, impidiendo su consumo a humanos y siendo potencialmente venenosa para otras formas de vida como peces, pájaros, anfibios y mamíferos. **Como si esto fuera poco, nos enteramos que en E.E.U.U el agua potable comenzó a cotizar en la bolsa de Wall Street.** ¿Se vio alguna vez un pueblo que sobreviva sin agua? El agua es vida y como tal vale más que el oro, y cualquier símbolo del capital. El agua no sólo es un indicador de salud de los pueblos, también es un derecho humano (y no humano) básico que debería respetarse y garantizarse, no contaminarse ni venderse.

![](/images/uploads/03elaguaesvida.jpg "Ilustración: Claudia Fausti")

*Ilustración por Claudia Fausti*

Lo sabemos, es momento de despertar, cuestionarlo todo en este sistema, porque es el que nos está llevando a nuestra propia destrucción (y la de miles de otras especies, que día a día se extinguen por nuestros efectos). **Si una pandemia global no es suficiente para alarmarnos y darnos cuenta que el momento de actuar es ahora, es porque estamos cegades, manipulades y poco informades sobre lo que está pasando… y lo que se viene si no hacemos nada.** Pero no caigamos en la desesperación o la depresión: ¿qué otra cosa más valiosa podemos hacer en este momento si no es unirnos y proteger a nuestra Madre Tierra? **En lo personal creo que nada tiene más sentido en este momento que luchar por el cuidado de nuestro gran hogar que es el Planeta Tierra, Gaia, Pachamama.**

Afortunadamente no todo fueron malas noticias este año: además de la ley del aborto, se promulgaron otras leyes importantes como la **Ley Yolanda** que, al igual que la Ley Micaela, es transversal y establececapacitaciones obligatorias para todos los funcionarios públicos en materia ambiental, buscando así concientizar a quienes en este momento representan un gran peligro para todes debido a su analfabetismo ambiental y el poder que tienen: los representantes políticos. 

También se aprobó la **Ley de Manejo del Fuego**, instrumento que impone prohibiciones en la utilización de suelos que fueron víctimas de incendios. Además se ratificó el **Acuerdo de Escazú**, tratado regional que trabaja sobre la participación ciudadana, el acceso a la justicia y a la información en materia ambiental, algo tan menoscabado hoy en día por los medios de masivos, las publicidades, las fake news y el monopolio cada vez más grande de la redes. 

Por último, a fines de diciembre Argentina presentó la Segunda Contribución Determinada a Nivel Nacional (NDC) elaborada en el marco del Gabinete Nacional de Cambio Climático (GNCC), espacio de trabajo interinstitucional que tiene como objetivo el **diseño y la implementación de políticas públicas para contribuir a la acción climática; y presentando una meta concreta de emisiones para 2030 que consiste en no exceder las 359 megatoneladas de dióxido de carbono, lo que implicaría una reducción del 25,7% respecto de la NDC** previamente comprometida en 2016, reafirmando así su compromiso con el Acuerdo de París `2`. No olvidemos que hay que continuar movilizándose para que salga la Ley de Humedales y la Ley de Etiquetado Frontal; ambas quedaron como deudas a cumplir en este 2021.

Si bien todas estas leyes y acuerdos son muy importantes, recordemos que Argentina ya cuenta con una nutrida legislación en materia ambiental que no se cumple. Por lo tanto, citando a uno de los disertantes del congreso: *“acá cuando se aprueba una ley no es el fin de una lucha, es el comienzo de ella para hacerla cumplir”*. Sin embargo, sabemos que **las verdaderas luchas no se dan en los foros políticos, se dan en la calle y en el día a día.** Necesitamos profundizar nuestra tan bastardeada democracia, exigir mayor participación de la comunidad; dejamos en manos de políticos y empresarios nuestro destino, y ya comprobamos cómo nos fue.

La psicología, como cualquier profesión, puede ser usada para mantener el status quo, o bien para cuestionarlo y proponer alternativas. En esta segunda línea es, bajo mi criterio, que se ubica la psicología ambiental, al igual que la psicología comunitaria, la psicología rural o la psicología con perspectiva de género.

A la luz de los eventos mencionados se evidencia con más fuerza **la relevancia de adoptar perspectivas críticas desde cada una de las áreas de la vida y de los campos disciplinares, para revertir la tendencia hacia el colapso inminente.** De aquí la importancia de la construcción de espacios de encuentro colectivo como lo fue el IV Congreso de Psicología Ambiental.

Poner en común las formas diversas en que las comunidades a lo largo y ancho de nuestra región resisten las avanzadas del terricidio capitalista, y plantean formas alternativas de vinculación con la naturaleza y entre las personas, permite armar redes, tender puentes y desarrollar un repertorio más sólido de respuestas frente a esta grave situación en la que nos encontramos.

Luego del congreso me quedé con muchas dudas, más preguntas que respuestas, pero si hay una cosa que logré afirmar es que necesitamos más ciencias dignas, al servicio de las comunidades, críticas, políticas, ecologistas, feministas, decoloniales, con conciencia de clase. **Y que la salida es colectiva.**

**Necesitamos visibilizar las voces de quienes están en los territorios y recuperar sus saberes.** Acompañar desde el amor y la armonía con la naturaleza, sin necesidad de idealizarla, simplemente entendiendo que somos une con ella. Volvamos a poner en el centro a la vida y el respeto por la diversidad. **Tejamos redes. Cuestionemos todo.**

**`2`** *El Acuerdo de París se firmó en el año 2016 en el marco de la Convención Marco de las Naciones Unidas sobre el Cambio Climático que establece medidas para la reducción de las emisiones de gases de efecto invernadero (GEI). El acuerdo busca mantener el aumento de la temperatura global promedio por debajo de los 2 °C y perseguir esfuerzos para limitar el aumento a 1.5 °C, reconociendo que esto reduciría significativamente los riesgos y efectos de la crisis climática. Además de establecer la reducción de emisiones de gases de efecto invernadero, el Acuerdo propone aumentar medidas de mitigación, adaptación y resiliencia al cambio climático.*

##### Algunos links para mayor información:

* [Este extractivismo que no se quita](<http://www.laizquierdadiario.com/Este-extractivismo-que-no-se-quita-balance-ambiental-del-2020-segun-especialistas>)
* [No queremos megagranjas bomba](<http://huerquen.com.ar/no-queremos-megagranjas-bomba/>)
* [El colapso de nuestra civilización es inevitable](<https://www.infobae.com/sociedad/2021/01/03/flavia-broffoni-el-colapso-de-nuestra-civilizacion-es-inevitable-pero-podemos-aprender-a-colapsar-mejor/>)
* [Las tres salidas a la pandemia...](<https://www.pagina12.com.ar/310091-las-tres-salidas-a-la-pandemia-segun-boaventura-de-sousa-san>)
* [El Delta en llamas](<http://noticias.unsam.edu.ar/2020/08/10/el-delta-en-llamas-incendios-en-las-islas-del-bajo-parana/>)
* [El pueblo vs la justicia, el trigo transgénico](<https://posdata.ar/el-pueblo-vs-la-justicia-y-el-trigo-transgenico/>)

<!--EndFragment-->