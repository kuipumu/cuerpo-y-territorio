---
title: Por una sobreexposición en modo avión
subtitle: " "
cover: /images/uploads/fake-news.jpg
date: 2020-09-17T01:07:11.952Z
authors:
  - Roberto Salazar
tags:
  - Sociedad
  - Infodemia
  - sobreexposición
  - información
  - noticias
  - hechossociales
  - redessociales
  - latinoamerica
  - mundo
  - economía
  - política
categories:
  - Política y Economía
comments: true
sticky: true
---
<!--StartFragment-->

> *“En el mundo realmente invertido lo verdadero es un momento de lo falso.”*
>
> ***Guy Debord***

<!--StartFragment-->

**Llamemos sobreexposición al exceso de presentación de determinada noticia**, idea, producto. A cierta desmesura de aquello que pretende ser visto. A usar un telescopio para mirar a una araña.

El término viene del lado de la fotografía. Es exponer en demasía una superficie a la luz. ¿Recuerdan qué era velar un rollo fotográfico en la cámara? Sobreexponer a la luz. Demasiada luz dejaba una ceguera blanca. Como la de Saramago, precisamente.

**Hagamos una indigestión de trendingtopics.** Pandemia. Deuda. Presidente. Ministro. Proyectos de ley. Crisis. Femicidio. Desapariciones. Policia. Desigualdad. Golpe de Estado. Latinoamérica. Hambre. Elecciones. Derecha. Izquierda. Democracia. Libertad. Sexo. Amor. Muerte.

Todo permanente, todo sobreexpuesto.

Georges Didi-Huberman nos orienta magistralmente en que *“los pueblos están expuestos a desaparecer porque están* (fenómeno hoy muy flagrante, intolerablemente triunfante en su equivocidad misma) *subexpuestos a la sombra de sus puestas bajo la censura o, a lo mejor, pero con un resultado equivalente, sobreexpuestos a la luz de sus puestas en espectáculo. \[…] **Los pueblos expuestos a la reiteración estereotipada de las imágenes son también pueblos expuestos a desaparecer.** Por ejemplo, el pobre pueblo humilde de las telerrealidades”*.

La sociedad del espectáculo, la llamó Guy Debord. Aquella sociedad yuxtapuesta de espectáculos y roles destinadas a una trivialización de la realidad. Una “falsa abundancia” o más bien una sobreabundancia. Una orgia de significados vacios por una tropelía de acontecimientos que aparecen uno tras otros, lo que Marc Auge llamó la sobremodernidad.

Sobreacumulación, sobreproducción, sobreadaptación. El exceso, lo plus. Lo peligroso. Lo mortal.

![](/images/uploads/seangladwell.jpg)

Adam Curtis lo muestra en su documental “Hypernormalisation”. Políticos, financieros, tecnócratas han construido un mundo sobrenormal, simplificado, que nos aleja de las complejidades del mundo real. El mundo de la “perception management”, del manejo controlado de las narrativas para hacer todo más simple. Más tribal. Más polar.

Y cuando los activistas, los artistas, los anarquistas se quieren sacudir de semejante burbuja, no hacen sino enfangarse en las arenas movedizas de la maquinaria esquizofrenizante que en sobre aviso nos pusieron Deleuze y Guattari. Quizás también ellos se excedieron.

Hay grandes tragedias subexpuestas. Dolorosamente ignoradas. Lejos estamos de haberle dado luz suficiente aquellas realidades que nos incomodan, a los malestares más incordiantes de nuestra época. Algunas por impensables para nuestra sociedad, algunas por incomunicables para nosotros mismos. Ay, ¡el gran Celan!

Aun superando lo incomprensible propio y ajeno, **lo que era una realidad invisible ahora se hace mercancía.** Es propaganda o es publicidad. Pero potable. Que se ajuste a la economía del like y del fav. Que genere identificaciones. Que cree productos. Que crezca. Que sobrecrezca.

No erremos el camino. No es el abandono en la disputa por el capital cultural o simbólico a aquellos que ponen sobremarcha a la vieja mass-media y sus derivados, a esa hegemonía aparentemente infranqueable. Gramsci apunta el carácter contingencial de las mismas. Es que aquella razón populista laclausiana se enfrenta a una sobreexigencia de aceleración dantesca.

En palabras del personaje de la serie danesa Borgen, el cínico Kasper: *“Las campañas políticas no son sino una simplificación de la realidad. Gobernar es otra cosa”*. **Se trata de esa campaña permanente en la que están secuestradas nuestras mejores ideas**. Nuestros mejores líderes. Nuestros mejores artistas.

Georg Simmel decía que toda realidad social tiene como único destino “tomar forma”. Eso nos interroga a pensar que forma están tomando las de nuestra época.

En clave de lectura de Walter Benjamin: El valor de (sobre)exposición canibalizando el valor cultural. Es decir, el aura de nuestras realidades más dolorosa, de lo traumático, de la belleza de la idea de justicia, de democracia, esta obcecado con el gran reflector de las redes sociales y sus implicaciones pret-a-porter. El crowdfunding, los memes y los hilos de Twitter.

No seremos luditas. Los détox tecnológicos son la nueva dieta mágica para bajar de peso. Un ajustazo. **¿Podremos apropiarnos del algoritmo para manejar la sobreexposición?** ¿Nos servirá de algo hacerlo?

Merleau-Ponty se reiría de nosotros tratando de hacer visible lo invisible. Lo visible y lo invisible son consustanciales: cuando visibilizo algo queda otra parte a obscuras. En lo decible puede haber mucho ruido para escuchar aquello indecible que también clama. Lo muestra muy bien el psicoanálisis. Aquello que callo, que olvido, que equivoco, ilumina mejor que la perorata sobre mi miseria. Un minimalismo de lo invisible.

Alerta de spoiler. Godot, el personaje del título de la obra de Samuel Beckett “Esperando a Godot”, nunca llega. No aparece ni una sola vez. Pero su espera es el leitmotiv de todo el drama. **Su ausencia era atronadoramente presente.** Una contra-pornografia de la presencia, de lo visible, de lo entendible, de lo expuesto.

Mucho ruido y pocas nueces, nos grita un fantasma shakesperiano. Pero estamos muy atentos al feed para poder advertirlo.

**La economía de lo expuesto.** Atrapados en el publish or perish de las realidades sociales. Esperamos una especie de teoría del goteo: aquella que reza que si la economía crece y los que tienen más capacidad de producción crecen, se derramara la abundancia a aquellos que menos poseen.

Derramo propaganda, doy batalla en lo cultural, creo hegemonía, gano elecciones, apruebo leyes, derramo cambios sociales. Pero entonces, **George Floyd o Facundo Castro.**

Dicen los grandes ciclos de la historia (y los que creen en ellos) que vendrá el estancamiento. Que la renta decrece. Que la sobreexposición dejará de ser tal. Pero más bien retrocedimos a la época benjaminiana de los totalitarismos. En donde ganan la vedette, el futbolista y el dictador. La de Goebbels y su famosa y posiblemente apócrifa “una mentira dicha mil veces se hace verdad”.

Bueno, parece que también puede ser que una verdad dicha mil veces puede terminar mentira. Y es nuestro fracaso en transmitir las ignominias de nuestra sociedad. **La revolución será transmitida por Netflix. Y en maratón**.

**Reapropiémonos de un significante concedido a los conservadores: austeridad.** ¿Sera posible rescatar de las garras de los reaccionarios lo austero para nombrar nuestro tratamiento a la intoxicación de lo abundante de las realidades sociales? Quizás alguna palabra menos mancillada. Sostenible. ¿Cómo haremos sostenible la superación de la atroz censura a temas que urgen por tener voz y mantener espacios de las causas que ya tienen lugar sin hacer que la maquinaria, lejos de explotar, nos absorba?

En 1848 el mundo estaba, posiblemente, tan decadente y al mismo tiempo tan abierto a nuevas posibilidades como ahora. El que sería uno de los más grandes novelistas de su tiempo, **Gustave Flaubert, se embarcaba en buscar Le mot juste**. La palabra justa, aquella que encontrara expresar la idea artistica a cabalidad, sin que le sobre ni falte. La que mostrara lo visible y señalara lo invisible. Quizás habrá que intentar hacer como él. **Sobreflaubertear.**

**Títulos y referencias de interés:**

* Saramago, J. Ensayo sobre la ceguera. 1995
* Didi-Huberman, G. Pueblos expuesto, pueblos figurantes.2014
* Debord, Guy. La sociedad del espectáculo. 1967
* Auge, M. Los no-lugares. Espacios de anonimat. 1992
* Curtis, A. Hipernormalizacion. 2016.
* Deleuze, G.; Guattari, F. El antiedipo. 1972
* Gramsci, A. Los cuadernos de la cárcel. 1975
* Laclau, E. La razón populista. 2005
* Price, A. Borgen. 2010
* Simmel, G. De individualidad y formas sociales. 1986
* Benjamin, W. La obra de arte en su época de reproductibilidad técnica. 1936
* Merleau-Ponty, M. Lo visible y lo invisible. 1964
* Beckett, S. Esperando a Godot. 1955
* Flaubert, G. La educación sentimental. 1869

<!--EndFragment-->