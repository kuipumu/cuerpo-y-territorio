---
title: Escritoras, maestras y publicistas
subtitle: Educación y sociabilidades femeninas en Salta  Segunda mitad del siglo
  XIX y principios del XX
cover: /images/uploads/foto_1alumnas_de_3er_grado_en_el_ano_1896_con_la_docente_rosa_brizuela._calle_espana_714._reproduccion_lacsi_donado_archivo_por_cesar_abregu_brizuela.jpg
caption: ALUMNAS DE 3ER GRADO EN EL AÑO 1896 CON LA DOCENTE ROSA BRIZUELA. CALLE
  ESPAÑA 714. REPRODUCCIÓN LACSI DONADO ARCHIVO POR CESAR ABREGU BRIZUELA
date: 2021-04-30T19:51:32.770Z
authors:
  - Sofía Guantay Estrabis
tags:
  - Mujeres
  - Genero
  - Feminismo
  - Cuerpo
  - Territorio
categories:
  - Genero
  - Cuerpo
  - Territorio
  - Cuerpo y Territorio
comments: true
sticky: true
---
*“Quiero y he de probar que la inteligencia de la mujer,*

*lejos de ser un absurdo o un defecto,*

*un crimen o un desatino, es su mejor adorno,*

*es la verdadera fuente de su virtud y de la felicidad”*

*(Manso, 1854, pág. 1)*

Desde el llamado “Siglo de la Historia” (Iggers, 2012) hasta la crisis de los grandes paradigmas del estructuralismo y cuantitativismo; el campo historiográfico ha sufrido sustanciales transformaciones en sus principios de inteligibilidad acerca del pasado. Como afirma Roger Chartier (1995), la pérdida de los modelos de comprensión dio paso a **nuevas formas de hacer historia en torno al resurgimiento de la subjetividad**.

Nutridos por los aportes interdisciplinarios propugnados por este giro antropológico y lingüístico (Chartier, 1995), la historiografía comenzó a interesarse por el estudio de sujetos y grupos subalternos -tales como las clases populares, el campesinado, los obreros, las minorías étnicas, las mujeres- a fin de lograr una visión más holística de la historia. 

Dentro de este marco, la Historia de las Mujeres y los Estudios de Género que, bregaron en el impulso de los movimientos feministas, contribuyeron a resquebrajar el **sesgo androcéntrico de la epistemología tradicional.** Desde la pluma de los historiadores, las mujeres han sido abordadas desde la excepcionalidad o la complementariedad respecto de los “grandes hombres”; pero siempre desde un lugar de subordinación. Desde esta lógica, las mujeres hemos sido “parias” de nuestra propia historia como civilización, al erigirse lo masculino como arquetipo de lo humano-universal.

De esta manera, para quienes nos abocamos al estudio de las mujeres y su historia, éste constituye no sólo una tarea académica sino reivindicativa; en pos de hacer tangible en la narrativa histórica la evanescente presencia femenina. **En este sentido, recuperar a las mujeres como sujetos históricos implica reinsertarlas en un horizonte mayor de acción colectiva**, que implique la construcción de esos “mundos femeninos”. 

Nuestro interés por analizar dichos “mundos” en alusión a las formas de sociabilidad en las cuales participaron activamente mujeres educadas; radica en la posibilidad de reconstruir sus representaciones y experiencias colectivas y subjetivas en la trama de las trasformaciones que fue sufriendo la condición de las mujeres en el periodo. Al respecto de tal condición, la extensión de la educación femenina y su correlato en el acceso de las mujeres a otros ámbitos de formación educativa; permitió la formación de espacios de sociabilidad femenina desde mediados del siglo XIX en la provincia de Salta.

\    \*\**

Hacia mediados del siglo XIX, en Latinoamérica la conclusión de los ciclos revolucionarios independistas dio paso al proceso de formación de los **estados nacionales** que, en términos generales, culminaron en el establecimiento de **gobiernos oligárquicos** cerrando el siglo. En el espacio rioplatense (actual Argentina), el rol de las elites tardocoloniales en el juego político de las Provincias Unidas –primero- y en la Confederación -después- fueron definiendo su perfil como clase dominante. 

De modo que, hacia 1880 con la presidencia de Julio Argentino Roca, se consolidó un “régimen conservador” (Botana, 1986). Este orden de matriz oligárquica, desde la mirada de Natalio Botana, se caracterizó por “el control efectivo del poder por parte de las elites, consolidándose en un régimen político” (Botana, 1986, pág. 40). Bajo el lema de “Paz y Administración”, la elite dirigente que nucleaba el Partido Autonomista Nacional (PAN), emprendió un proceso de modernización; en el cual la educación ocupaba un lugar privilegiado.

Es preciso advertir, que este proyecto no fue una invención decimonónica, muy por el contrario, sigue un continium del discurso ideológico liberal de mediados del siglo XIX. **El mismo consideró la educación como un instrumento insoslayable para construir una nueva sociedad** sobre los cánones de la modernidad, en la cual la mujer no podía estar excluida. 

En este sentido, Domingo Faustino Sarmiento fue uno de los principales promotores en pos de elevar la condición de las mujeres privadas de educación; de hecho, era sustancial “redimirlas con la elevación cultural para que pudieran constituir la polea de transmisión que llevará a erradicar la barbarie” (Barrancos, 2010, pág. 107). Desde la lógica sarmientina, la mujer en su función de madre y esposa, tenía la misión primordial de transmitir los valores republicanos en la familia -en cuya calidad de grupo social primigenio- permitiría la formación de ciudadanos de la nueva sociedad moderna. 

Por lo tanto, retomando los aportes de Barrancos (2010) y Biancalana de Castelli (1999), **la educación femenina se concibió en términos patriarcales, ya que se refuerza un “mensaje unívoco sobre la esfera y el rol asignado a la mujer en su calidad de madres y esposas”** en el mundo doméstico. (Biancalana de Castelli, 1998, pág. 552). Desde nuestra perspectiva, este impulso de la educación femenina debe leerse en dos momentos: Durante las primeras décadas del siglo XIX, ligado a las **“artes de adorno”**, que recibían las damas de las clases encumbradas. Posteriormente, una etapa más expansiva, relacionada con **cierta democratización** de la educación emanada desde las políticas estatales; cuyos hitos fundantes fueron: La sanción de la Ley 1.420 de “Educación común, gratuita y obligatoria” (1884) y la fundación sistemática de escuelas normales en todo el territorio nacional.

Históricamente, la educación femenina tuvo un carácter plenamente restrictivo en la forma de “artes de adorno”, ya que estaba destinada a la formación de las damas de las elites. Precisamente, el propio término pone en evidencia que, el aprendizaje de ciertas habilidades apuntaba a generar competencias útiles en la carrera matrimonial; por el contrario, no se buscaba el desarrollo personal e intelectual de estas mujeres. Pertinente resulta citar a Jean Jacques Rousseau, cuando en su Emilio o De la Educación, se refiere a la educación de las mujeres que, estructurada sobre un supuesto esencialismo femenino, las coloca en plena subordinación del imperativo masculino:

“Toda educación de las mujeres debe ser relativa a los hombres. Complacerlos, serles útiles. Hacerse amar y honrar por ellos, criarlos de jóvenes, cuidarlos de ancianos, aconsejarlos, consolarlos, hacerles agradable y dulce la vida: estos son los deberes de las mujeres en todas las épocas, y lo que han de aprender desde la infancia” (Rousseau, 1762)

En este sentido, emulando los modelos **aristocráticos europeos**, los linajes argentinos formaron a sus mujeres a fin de alcanzar un enlace matrimonial ventajoso capaz de engendrar hijos sanos, y en consecuencia las signaba al universo doméstico. No obstante, como indica Barrancos (2010) bordeando la segunda mitad del siglo XIX, asistimos a una mejora en la educación femenina en las clases altas, por lo cual un mayor número de mujeres se dedicaron a escribir, y en algunos casos a obtener algunos ingresos por ello. 

Entre las pioneras figuran **Rosa Guerra**, directora de el periódico **La Camelia** y autora de **Julia- un manual de educación femenina- publicado en 1852**; **Juana Manso** y sus periódicos literarios: **Álbum de Señoritas (1854), Flor del Aire (1864) y Siempre Viva (1865)** recogieron un amplio programa de reivindicación de los derechos femeninos. Sin embargo, Barrancos (2010) sostiene que, el carácter efímero de estas empresas y la constante dificultad para obtener el reconocimiento entre sus pares-en su mayoría hombres- evidencia el androcentrismo imperante en los albores de la creación del campo literario argentino.

Para el caso de Salta, si bien es innegable el talante patriarcal y eurocéntrico de la educación y la cotidianeidad de las mujeres de la elite, se puede observar una activa participación femenina en la incipiente sociabilidad norteña. En este sentido, Elsa Saravia de Solá –una maestra normal perteneciente a las familias patricias- recopila crónicas de viajeros en su escrito “la Mujer Salteña y su vocación por la enseñanza” (1956). 

En uno de sus viajes, Woodbide Parish, relató que, en “Salta nunca falta una regular porción de personas dotadas de capacidad e instrucción tan variada como ventajosa, debida a la afición por los viajes \[…] ganan mayor realce respecto a las damas salteñas con esa hermosura y elegancia tan común en sus formas” (Castellanos de Solá, 1956, pág. 27). Así también, Víctor Gálvez narra en 1853, que “la sociabilidad de Salta era muy adelantada, las damas muy cultas olían a pergaminos (…) la mujer salteña recuerda a la matrona antigua repartiendo su afecto entre el hogar y los deberes cívicos, formando sus hijos en la ilustración, las creencias cristianas y sus relaciones con la Patria” (Castellanos de Solá, 1956, pág. 27). De este modo, ambas citas ilustran, no sólo la autopercepción que tienen las élites de sí mismas acerca de su rol civilizatorio y la incipiente sociabilidad salteña; sino que destaca la activa participación de las mujeres en estos círculos culturales y sociales.

Uno de los primeros bosquejos para definir teóricamente las sociabilidades, fue el realizado por el sociólogo George Simmel (1950) quien delineó el campo de la sociabilidad como espacios para el desarrollo de la interacción y acción social generalizada, propio de las sociedades urbanas modernas capitalistas. Con posterioridad, el historiador francés Maurice Agulhon (1981) planteó que, la sociabilidad se define por las relaciones interindividuales que se desarrollan en el seno de los grupos intermedios de las sociedades urbanas, insertos entre la intimidad del núcleo familiar y el nivel más abstracto de las instituciones políticas- especialmente aquellas de carácter estatal- y que no tienen una finalidad o interés expreso de carácter económico o político.

De acuerdo con estos postulados, resulta operativo el concepto de **sociabilidades femeninas** para la realidad argentina y salteña del siglo XIX, ya que perfila espacios, formas y prácticas de sociabilidad entre mujeres, que han podido acceder a distintos ámbitos de educación; y cuya formación les ha permitido una mayor participación en el espacio público en el marco de los procesos de modernización estatal. Ejemplos ilustrativos de esta incipiente sociabilidad femenina fueron las tertulias y el salón literario de la escritora salteña Juana Manuela Gorriti, durante su exilio en Perú durante 1876-1877. Michaud-Mastoras (2017) señalo que este fue un espacio de intercambios intelectuales fundamental en la construcción de la nación y de la identidad americana, por haber difundido, a través de las artes, ideas de progreso social tales como la importancia de la instrucción de la mujer y de su rol esencial en la patria.

Durante la presidencia de Domingo Faustino Sarmiento (1868-1874) la cuestión de la educación fue meridiana, continuada posteriormente por Nicolás Avellaneda (1774-1880). Dentro de este marco, la labor intelectual y pedagógica de Juana Manso y el arribo al país de maestras norteamericanas- como Mary Mann, Mary O’Graham, Jennie Howard y Jannet Stevens- y la fundación de la Escuela Normal del Paraná; perfilaron un proyecto nacional en materia educativa orquestada desde el propio estado. 

![ESCUELA NORMAL 1909 FACHADA](/images/uploads/foto_3escuela_normal_1909_fachada.jpg)

La cristalización de las transformaciones precedentes tuvo lugar en tiempos del régimen oligárquico, ya que la educación -en términos generales- alcanzó un mayor nivel de democratización. **En este contexto, la educación femenina fue perdiendo su original carácter elitista, al permitir la alfabetización de niñas y la posibilidad de acceder a otras instancias de formación educativa**- como la opción a cursar la carrera normalista. En alusión a este último punto, la acelerada feminización del normalismo en argentina, no solo constituyó un instrumento de movilidad social ascendente para muchas mujeres de los sectores medios y bajos, sino también implicó un mecanismo de ingreso a la vida pública. Desde nuestra perspectiva, la carrera normal y el ejercicio del magisterio, actuó como una suerte de polea hacia el espacio público en la forma de diversos espacios de sociabilidad.

**La Escuela Normal de Salta creada en 1882**, se enmarca dentro del ciclo fundacional del normalismo argentino y como tal fue considerada como un dispositivo formador de docentes (Fiorucci, 2014); la institución también se constituyó como un espacio de sociabilidad femenina. Desde esta perspectiva, la escuela normal salteña se define como una “institución formadora de personas, a quienes no sólo le permitía el acceso a una fuente laboral a través de la docencia; sino también a diferentes espacios de circulación de información -como la prensa, literatura, y cargos dentro del Ministerio de Instrucción. 

En ese sentido el capital cultural adquirido tras su paso por la institución les valió como bien para acceder a otros espacios de sociabilidad, haciendo valer su preeminencia social y cultural en el espacio público” (Maciel y Guantay Estrabis, 2020, pag.11) Muchas maestras normalistas, combinaron el ejercicio del magisterio, con la participación activa en el asociacionismo benéfico y espacios de sociabilidad cultural -como publicistas, escritoras, periodistas además de maestras-. 

Entre las muchas a las que podríamos hacer alusión se encuentran: la escritora Zulema Usandivaras de Torino; la educacionista Carmen Niño, la poetisa María Torres Frías, y la periodista Benita Campos (Vitry,2000) Todas ellas fueron egresadas de la Escuela Normal, ocuparon cargos dentro de las instituciones educativas y en la Sociedad de Beneficencia, asimismo, publicaron sus escritos en diversas revistas literarias – La Revista (1897), El Búcaro Salteño (1875-1879)-; Boletines educativos -El amigo de la Infancia (1879), el Boletín de la Educación (1890)- y periódicos- La Reforma (1875), El Intransigente (1886). 

Un caso particular, fue el de **Benita Campos** quien, aparte de dedicarse fervorosamente a las labores cívicas en conmemoración al héroe de la independencia Martin Miguel de Güemes, emprendió bajo su dirección La Revista Güemes. Dicha publicación se erigió como un espacio de diálogo e intercambio de saberes entre diferentes mujeres ilustres y personalidades destacadas del mundo cultural y político; así como también integró una red de sociabilidades de alcance regional y nacional.

![](/images/uploads/editadafoto2revista_guemes-_benita_campos.jpg)

\    \*\**

Recapitulando, hacia la segunda mitad del siglo XIX, se inicia un proceso de mejora en la educación femenina, lo cual les permitió a ciertas mujeres alcanzar otros niveles de formación educativa, intelectual y cultural; fenómeno que tuvo su correlato en una gradual participación de las mujeres en el ámbito público. Particularmente, el proyecto sarmientino, la sanción de la Ley 1.420 y la fundación sistemática de escuelas normales fueron puntos nodales en favor de la educación de las mujeres, no obstante, seguía sustentándose sobre el ideal de la mujer como educadora de la familia. A pesar de ello, el acceso a la carrera normalista, no sólo fue un mecanismo de ascenso social para las mujeres, sino también un canal de ingreso a otros espacios y formas de sociabilidad femeninas.

A modo de hipótesis, planteamos que, dichos espacios tenían una doble modalidad: Por un lado, como ámbitos de circulación de saberes y conformación de relaciones estratégicas entre los distintos miembros. Por otro lado, como canales de legitimación política, a través de la proyección al espacio público de los presupuestos republicanos; y en este sentido las mujeres actuaron como interlocutoras, productoras de saberes y agentes civilizatorios. Finalmente, como señala Marcela Vignoli (2011) estos espacios de sociabilidad fueron canales de denuncia en contra del imperativo social que las colocaba en un lugar subordinado respecto del varón y abogan a favor de la democratización e importancia de la educación femenina.

#### Referencia 

Agulhon, Maurice y Bodiguel, Maryvonne (1981) Les associations au Village. Edition Acte-Sud, Le Paradou

Barrancos, D. (2010). Mujeres en la sociedad argentina. Una Historia de cinco siglos. . Buenos Aires : Sudamericana .

Biancalana de Castelli, M. M. (1998). (“La educación femenina victoriana: Interdicción a la autonomía” . TEMAS DE MUJERES. Perspectivas de género, C.E.H.I.M, Tucumán, pp. 552-610.

Botana, N. (1986). El Orden conservador. Buenos Aires : Hyspamérica, Biblioteca Argentina de Historia y Política.

Elsa Castellanos de Sola “la Mujer Salteña y su vocación por la enseñanza”. Biblioteca Joaquín Castellanos. Inst. de Educ. Superior Nº 6001 “Manuel Belgrano”; Cuaderno de “Bodas de Diamante” (1881-1956), Hoja 27

Chartier, R. (1995). “La Historia hoy en día: desafíos, propuestas” . Annales de Historia Antigua y Medieval, Vol. 28.

Fiorucci, F. (2014). Maestros para el sistema de educación pública. La fundación de escuelas normales en Argentina (1890-1930).

Iggers, G. (2012). La historiografía del siglo XX, desde la objetividad científica al desafío posmoderno. Chile: Fondo de Cultura Económica Chile S.A.

Maciel, Maria Magdalena y Guantay Estrabis, Sofía. (2020). “La Escuela Normal como “Espacio de sociabilidad femenina . Salta a fines del XIX y mediados del XX. Workshop interdisciplinario “Espacios de sociabilidad, procesos de institucionalización y agencias políticas, culturales y religiosas en el NOA (1850-1950), (págs. 1-16). Salta.

Manso, J. P. (1854). Álbum de Señoritas. Buenos Aires, Año I, N° 1 (1/I/1854). Buenos Aires .

Michaud-Mastoras, D. (2017). Voces femeninas de la América decimonónica: Juana Manuela Gorriti, Soledad Acosta de Samper y Joséphine Marchand-Dandurand en sus diarios íntimos y publicaciones periódicas. Montreal: Département de littératures et de langues du monde.

Rousseau, J.-J. (1762). Emile ou De L'Education. Paris.



#### Sofía Guantay Estrabis

Estudiante de grado por las carreras de Prof. Y Lic. En Historia por la Universidad Nacional de Salta. (Argentina). El presente artículo se desarrolla en el marco de las investigaciones realizadas por el proyecto C.I.U.N.Sa N°2512:“Construcción histórica del poder en el noroeste argentino: Elites locales, proceso de institucionalización y agencias políticas, culturales y religiosas (fines del siglo XIX principios del XX)