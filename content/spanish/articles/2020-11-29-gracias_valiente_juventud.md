---
title: "  ¡Gracias, valiente juventud!"
subtitle: " "
cover: /images/uploads/earqqg6xqaagmf_.jpg
date: 2020-11-29T20:24:31.970Z
authors:
  - Karol Dinamarca
tags:
  - Chile
  - Represion
  - Carabineros
  - Juventud
  - Cultura
  - Presupuesto
  - Protestas
  - Gobiernos
  - Declaraciones
categories:
  - Política y Economía
comments: true
sticky: true
---
<!--StartFragment-->

Como ya es costumbre en mi país, **los chilenos comenzamos nuestra rutina del día a día "desayunándonos" célebres y desatinadas frases de nuestros gobernadores, especialmente, de nuestros Ministros.** Un ejemplo de ellos, fue el burlesco comentario del, en ese entonces, ministro de Economía, Juan Andrés Fontaine, quien dijo *"El que madrugue, será ayudado",* aludiendo al alza de precio del transporte público.

El boleto de metro en Santiago en horario bajo tiene un valor un poco menor del boleto de horario " valle " o " punta " (hora de mayor tránsito de personas). El problema, es que el horario bajo comienza a las 06:00 am y termina a las 06:59 am...

La siguiente frase se la adjudica Luis Castillo, cuando ejercía el cargo de subsecretario de Redes Asistenciales mientras era entrevistado por la Radio Santa María de Coyhaique: *" Los pacientes siempre quieren ir temprano a un consultorio, algunos de ellos, porque no solamente van a ver al médico, sino que es un elemento social, de reunión social".*

Según mi experiencia y la de amigos o familiares cercanos que hemos visitado consultorios o recintos médicos públicos, nunca hemos llegado temprano precisamente porque hemos deseado encontrarnos con algún amig@ para conversar de la vida.

Pero la siguiente frase enmarca el total desinterés del gobierno sobre el arte en Chile y corresponde al emitido por la actual ministra de Cultura, Consuelo Valdés al ser entrevistada por CNN Chile: ***"Un peso que se coloque en cultura, es porque se deja de colocar en otro programa o necesidad de los ciudadanos".*** 

¿Fuerte, no? Por supuesto que este dicho causó indignación en los trabajadores del área de cultura.

Los artistas no somos seres de segunda categoría que no necesitamos nada más que una palmada en la espalda y las respectivas felicitaciones por nuestra labor. Por supuesto que eso nos llena de alegría y es una razón para seguir siendo artistas, pero cada peso que va dirigido a un programa cultural, no es solo para producir cultura, con ello se les paga un sueldo a toda la gente que trabaja en esto, y que son una infinidad de profesionales l@s  incluidos: músic@s, actores, sonidistas, utiler@s, maquillador@s, coreógraf@s, bailarin@s...

**Y como cualquier otro responsable ciudadano, nosotros no quedamos exentos de pagar servicios básicos** como la luz, agua, gas, internet, arriendo de vivienda, salud, alimentación, movilización, educación, etc. ¿Cómo se le puede ocurrir a la Ministra que los artistas no necesitamos dinero para subsistir, o que el dinero presupuestado para cultura debería ser asignado a "otros" ciudadanos?

**Nos hemos convertido en nuestros propios mecenas, amigos.** Nosotros trabajamos en distintas disciplinas para poder subsistir, y así poder llegar a casa y abrirle paso al artista creador que llevamos dentro. Somos gestores de nuestro propio camino artístico.

**Debido a esta mala gestión y desprotección al rubro artístico, la autogestión de proyectos culturales se ha convertido en la única vía de sobrevivencia para los artistas chilenos.** Ya no podemos ni siquiera ilusionarnos con la idea de estudiar una carrera musical en una universidad de prestigio, titularnos, y poder salir al mundo laboral con la total fe de encontrar trabajo en nuestro rubro. Se me hace un juego macabro, en donde solo importa generar ingresos a las universidades, sin siquiera detenerse a pensar un poco en la integridad del alumno, en sus sueños, en sus sacrificios personales y familiares.

**Como la autogestión se ha convertido en la carta bajo la manga obligada, muchos músicos han salido a la calle a tocar para ganar algún dinero.** Este es el caso de la O*rquesta de la calle*, grupo de estudiantes músicos de distintas universidades que han decidido juntarse para llevar música a las calles y, con el dinero ganado gracias al aporte de los transeúntes, poder incluso pagar parte de la mensualidad de su carrera universitaria.

Pero lamentablemente, hace algunos días, jóvenes integrantes de esta agrupación fueron aprehendidos por carabineros, ellos se encontraban realizando las debidas gestiones en la municipalidad de Santiago, para poder obtener el permiso necesario para continuar tocando en las calles y asi generar algo de ingreso económico.

**O sea, aparte de no haber más dinero ni inversión para cultura, se te toma preso por llevar música a las calles.**

**No es primera vez que la juventud Santiaguina se ve menoscabada.** Durante el año 2019, jóvenes estudiantes de emblemáticos liceos debieron vivir la dura represión policial liderada por el aún alcalde de esa comuna. Esto debido al alza de manifestaciones en contra de medidas como el financiamiento por asistencia a establecimientos educacionales y poner fin a la criminalización del movimiento estudiantil.

#### **Ley aula Segura**

Al parecer, la gobernación no encontró una mejor solución al requerimiento de los estudiantes que recurrir a la atroz intervención policial.

Como resumen, podemos concretar que las dinámicas de acción de las autoridades se llevan a cabo de la siguiente forma; comienza una protesta por causas totalmente justas e importantes para el pueblo y las autoridades te lanzan represión policial, la más descarnada y cruel posible. 

**Recordemos que muy pocos policías o autoridades han sido procesadas por los crímenes de lesa humanidad que se han cometido durante el estallido social de 2019.** Según medios de prenda escritos, la represión policial ha dejado al menos 3.649 heridos, 405 de ellos con lesiones en ojos, más 194 casos de violencia sexual durante la detención, la mayoría desnudos forzados. 

Y si llevas música a las calles, eres detenido. Personalmente, debo decir que me parece una situación indignante, la verdad es que me asusté mucho cuando supe que habían llevado detenidos a estos chicos y el miedo tiene muchas razones. Mientras yo no vea que los responsables de muertes y lesiones no pagan de alguna forma, por supuesto que voy a desconfiar del accionar de Carabineros. **Mis colegas de profesión de la *Orquesta de la calle* merecen respeto y dignidad.** Un futuro para poder dibujar sus sueños en él.

Sólo puedo despedir esta columna, agradeciéndole a nuestra hermosa juventud chilena, a los "primera línea" que aguantan cara a cara la violencia estatal por medio de carabineros; a los jóvenes que siempre participan en las marchas, protestando por un país mejor, a los jóvenes músicos, que acercan el arte de la música a gente que no tiene acceso a ella.

**A ellos, y a todos, muchas gracias, y perdón por tan poco.**