---
title: Los colores de la tumba
subtitle: " "
cover: /images/uploads/110101_10_pm1.jpg
date: 2021-01-26T05:37:29.702Z
authors:
  - Roberto Salazar
tags:
  - Venezuela
  - Colombia
  - EEUU
  - TerrorismoDeEstado
  - Dictadura
  - LaTumba
  - PoliciaPolitica
  - España
  - PresosPoliticos
  - Injusticia
  - Tortura
  - DDHH
  - Política
categories:
  - Política y Economía
comments: true
sticky: true
---
*`los pasos y los golpes se repiten,`*\
*`y aún ignoro si estaré en el banquete`*\
*`como embutidor embutido. Larga es la espera`*\
*`y mi sueño de ti no ha terminado.`*

`El sueño del prisionero`

Eugenio Montale

*5 de septiembre de 2014. Cúcuta, Colombia. Frontera con Venezuela.*

El gobierno del presidente Juan Manuel Santos decide deportar de manera inmediata al activista venezolano Gabriel Vallés. Vallés aún no se recuperaba de la extradición de su compañero Lorent Saleh, también activista y férreo opositor al gobierno de Nicolás Maduro, que lo tomó por sorpresa sobre todo por la forma arbitraria en que ocurrió. En menos de 24 horas, Valles iba a tener el mismo destino que su compatriota. Entregado a las fuerzas de seguridad de Maduro en la línea imaginaria que separa a los dos países. Poco después, ya en territorio venezolano, Vallés sería subido a un avión con destino a Caracas. Allí el suplicio blanco estaría por empezar.

####  **\*\****

1950. Washington, EEUU.

Resultaba desconcertante para el alto mando militar de los EEUU, en plenos años posteriores a la guerra en Europa, ver como sus muy bien entrenados infantes de marina podían quebrarse y traicionar a su país de esa forma según observaban en las grabaciones que recibieron. No parecían haber sido sometidos a grandes tormentos. Sus cuerpos lucían intactos, ni deformados por castigos detectables por el ojo humano ni tampoco había rastros de ciertos espasmos que delatarían daños internos productos de las más salvajes sesiones de interrogatorio. Ahí estaban soldados ejemplares, valientes, con hojas de servicios impecables, develando secretos militares y jurando lealtad a una potencia extranjera sin chistar. Era un lavado de cerebro como nunca lo habían visto ni las agencias de inteligencia ni los jerarcas del estado mayor del ejército.

#### **\*\****

2014. Sede del servicio de inteligencia venezolano. Caracas.

Nos metieron en La Tumba. Cinco pisos debajo de la superficie. Incluso por debajo del Metro, que es lo que se lograba escuchar apenas. Un pasillo largo. Siete celdas, piso blanco, suelo blanco. Ventilación mecánica fría, friísima. Un uniforme caqui. Cámaras de vigilancia en cada celda, dos cámaras en el pasillo. No había luz solar, solo aquella iluminación artificial. Blanca, invariablemente blanca.

#### **\*\****

2010. Caracas, Venezuela.

Años antes, a Saleh y a Vallés los habían detenido fuerzas de seguridad del estado y al revisar el vehículo en que se movilizaban, encontraron unos panfletos con la frase “Chávez miente”, parte de una campaña estudiantil de oposición al entonces presidente, el día antes de la celebración de las elecciones parlamentarias de entonces. Fueron acusados de divulgación de información falsa, se les abrió un expediente judicial, pero lograron quedar en libertad. Aquel documento sería la única causa legal en curso durante su estadía en la sede del Servicio Bolivariano de Inteligencia (SEBIN), el organismo de inteligencia del estado.

#### **\*\****

Década de 1950. Washington, EEUU.

Allen Dulles, el por entonces director de la CIA, da marcha a una operación militar secreta. El Proyecto MK Ultra, al mando del operador Sidney Gottlieb y con el apoyo psiquiátrico del Dr. Ewen Cameron. El objetivo no podía ser sino replicar y superar los efectos de los estremecedores videos provenientes de Corea y la URSS. Dicho de otra manera, lograr el total control mental de los prisioneros.

Para tal fin no se escatimó ni recursos humanos ni materiales. Más de 80 institutos y casi 200 investigadores, además del personal operativo y logístico que formaron parte, consciente o inconscientemente, de la carrera por ganar el dominio absoluto de la voluntad de su adversario.

#### **\*\****

2014. Sede del SEBIN, Caracas.

No nos hablaban, ni nos informaban cuando era de día o cuando de noche. Siempre la misma luz, las mismas paredes, el mismo sonido del aire gélido de la rejilla de ventilación. Nos cambiaban los horarios en que recibíamos la comida para no poder tener una noción del tiempo. Y nos observaban. Nos observaban todo el tiempo o eso creíamos nosotros en esas cámaras que nos ponían en la celda. No nos dejaban poner nada encima para cubrirnos del frío. No había ni libros, ni radio, ni salidas. Siempre dentro de la celda, solo para salir al baño cuando estábamos por reventarnos. Quedaba por tratar de hablar el uno con el otro, a Lorent lo pusieron en la celda de al lado y se oía si gritaba. Pero si lo hacíamos mucho tiempo nos amenazaban. Está uno solo, solísimo, con la celda. Todo blanco. Siempre blanco.

#### **\*\****

2014. Caracas, Venezuela.

El 12 de febrero de 2014 ocurre la primera movilización opositora a Maduro de magnitud. La marcha decide dirigirse al oeste de Caracas, zona tabú debido al recuerdo de los eventos de abril de 2002 que terminaron en un golpe de estado. Al entrar al centro capitalino, la marcha empieza a perder rumbo y se inicia una fase más de confrontación con las autoridades policiales, que seguían de cerca el desarrollo de los eventos. No pasa mucho tiempo y se disgregan la mayoría de los asistentes, dejando lugar a pequeños grupos que de la protesta enérgica pasan a la asonada. Y entonces ocurre. Bassil Da Costa es asesinado de un disparo en la cabeza. Por la precisión del balazo solo podría haber sido ejecutado por algún funcionario del orden público. Se desata el caos, se reprimen a los manifestantes. Alguien más muere, Montoya. A la noche, en pequeños focos de protestas que lindaban en motín, los opositores a Maduro mantienen la confrontación con la policía. Muere otro más, Roberto Redman. Empiezan las detenciones de estudiantes universitarios que, en rigor, son los participantes más contestatarios al poder del estado. Ese día, esa noche, inicia la robusta maquinaria de intimidación, coacción y miedo a la que se tendrán que enfrentar los insumisos que se resistan al control gubernamental.

#### **\*\****

Década de los 50. Washington, EEUU.

El Dr. Cameron, de la CIA, utilizó todo el arsenal disponible. Privación de sueño, LSD, descargas eléctricas, aislamiento celular. La idea era poder quebrar la psique de los participantes en los experimentos, que habían pasado de ser espías enemigos a supuestos voluntarios. Los resultados no eran particularmente alentadores, pero se continuaba y hasta redoblaba esfuerzos. Pronto se otorgó a los oficiales de campo de las agencias de inteligencia parte de los resultados de las investigaciones para elaborar sus propios manuales de suplicio. Las necesidades geopolíticas fueron requiriendo una premura e improvisación mayor, alejado de las investigaciones estériles de laboratorio. Pero el desarrollo, de cierta forma, había avanzado. Se había sofisticado la manera de someter a situaciones estresantes a los interrogados. Se sistematizó no sólo su uso, si no se exportó a gobiernos latinoamericanos que necesitasen someter a los insurgentes. Y los Pedro Estrada, Manuel Contreras y Johnny Abbes no tardaron en aparecer.

#### **\*\****

2014. Sede del SEBIN, Caracas.

Al principio nos apoyábamos Lorent y yo, mucho. Pero poco a poco fue muy difícil sostener todo. Aislado, sin luz solar, sin comunicación con el mundo. Sentíamos que empezábamos a perder la razón. Lorent creo que antes que yo. Le oía golpearse la cabeza contra la pared en su celda. No lo entendía al principio. Luego lo imité. Era para sentir dolor, sentir que estabas vivo, que no eras un muerto andante. Que tenías una cabeza que podía sentirse distinta a la otra parte del cuerpo porque se empezaba a hinchar a base de golpes. Además, sudabas un poco. Sudar, con aquel frio, que difícil era. La piel reseca, casi marchita, por ese congelamiento espantoso. La sensación con el frío era que te paralizabas, que se te iba quedando el cuerpo en pausa y de a poco la mente, no sabías muy bien dónde estabas, por qué estabas ahí, para qué. A Lorent se le fue complicando, provocaba a los guardias, quizás para poder intercambiar palabras con ellos. Después escuche que quería que le dieran una paliza. Pensaba que si se sentía sangrando por una ronda de patadas podría comprobar que estaba vivo y que si se hallaba jadeando por una golpiza podía calentar el cuerpo un poco. Nunca me lo dijo, pero lo intuí. Luego, eventualmente, encontró la manera de cortarse con alguna hojilla que pudo robar en los pocos minutos que nos las permitían usar para afeitarnos. No era un suicidio, estoy seguro que era para poder sangrar. Para sentir algo distinto en esa celda de todos los días iguales. Para poder ver que algo se desplazaba, que corría desde adentro hacia afuera. Para al menos ver el rojo gotear, entre toda esa inmensidad blanca que siempre nos rodeaba.

#### **\*\****

2017. Caracas, Venezuela.

En 2017, el gobierno de Maduro decide inhabilitar, con una estratagema legal, el funcionamiento del parlamento que está en manos de la oposición política. El movimiento desata una ola de protestas que sume al país en un espiral de violencia con centenares de muertos. La crisis política se solapa con la económica, que viene de una caída de producción de petróleo e ingresos de divisas al país, lo que prepara a la economía venezolana a una hiperinflación en ciernes. 

El aparataje del estado no deja de aplastar con fuerza cualquier resistencia seria por parte de sus adversarios. La sofisticación de La Tumba da paso al Helicoide, cárcel para más de 300 presos políticos, hacinados y en condiciones deplorables, y la vuelta a las viejas tundas de garrote y bota hasta la inconsciencia. Los encarcelamientos masivos van mutando y se convierten en visitas de las fuerzas de operaciones especiales a las casas de los activistas más radicales. En muchos casos, terminaban en ejecuciones extrajudiciales en una barriada popular, en la más abyecta impunidad. Las aspiraciones de poder dominar la psicología del opositor a través de las técnicas más modernas de sometimiento son abandonadas por el ejercicio puro de la violencia.

#### **\*\****

Década de 2000. Washington, EEUU.

El fracaso de MK Ultra, su exposición parcial a la prensa a lo largo de la Guerra Fría y finalmente la derrota de sus enemigos políticos al desintegrarse la URSS hicieron que se pudiera trabajar en tratados internacionales más creíbles para evitar los tratos crueles en cualquier ser humano. Hasta el 11 de septiembre de 2001. La guerra contra el terrorismo y la lógica bélica de aplastar como sea a los enemigos de la nación, además de las urgencias de resultados positivos para mostrar, llevaron al desarrollo de nuevos métodos avanzados de interrogatorio. Mitchell y Jenssen, dos psicólogos, se encargaron de diseñar situaciones mentalmente apremiantes sin recurrir a suplicios bárbaros en el cuerpo de los sospechosos para poder obtener confesiones que eviten desastres a la seguridad de la patria. 

El submarino, técnica de ahogamiento particularmente atroz, fue defendida por el mismo presidente George Bush para poder desactivar células terroristas. Luego las técnicas se hicieron cada vez más intensas, más brutales y menos efectivas. Los programas de interrogatorio avanzado fueron suspendidos, no sin dejar los nombres de Guantánamo y Abu Ghraib asociados al horror de crímenes de lesa humanidad, que ciertamente no han sido satisfactoriamente resueltos.

#### **\*\****

2014-2015. Sede del SEBIN, Caracas.

Ya con el último intento de suicidio de Lorent ellos empezaron a flaquear. Los empezábamos a ver nerviosos. Indecisos. Empezamos a hacer exigencias. Las visitas con los familiares, que por lo corta y espaciadas terminaban siendo otro tormento, pudieron extenderse. Con una huelga de hambre que supimos sostener semanas, pudimos empezar a meter cosas, abrigos para el frio. Libros. Qué maravilla es poder leer. Leíamos el pequeño libro que nos dejaban hasta casi memorizarlo todo. Ya no estaban yendo aquellos esbirros que parecían extranjeros y parecían supervisar el funcionamiento de todo. Lorent decía que eran rusos, por lo blanco de sus pieles, a mí me parecían cubanos y le aclaraba que en La Habana había muchísimos blancos. En realidad, nunca supimos, capaz eran de Mérida. 

Las revistas podían estar más tiempo. A veces la ventilación mecánica fallaba y hacía calor. En la última visita con el abogado nos insinuó que era posible un traslado. Pero que eso podría llevar aún muchos meses. No sé qué habrá pensado Lorent, pero yo no sabía si alegrarme o no. En esa visita me pudieron dar unos colores. Unos colores de madera, simples, creo que Faber-Castell. No hice más que pintar todos los espacios en blanco que tenía en el libro y la revista que tenía en la celda. Pintaba fuerte, hasta traspasar la hoja y romperla. Rojo, verde, azul. Cualquier color me bastaba, me hacía ensancharme de emoción, de ver como de a poco el violeta iba invadiendo lo blanco del libro, lo naranja iba ahogando cada parte clara del borde de la revista que tenía. No me atreví a tocar las paredes de la celda, pensé que me podían quitar todo, pero antes de acostarme a dormir y viendo el techo blanquísimo y la luz irremediablemente encendida, tomaba algún color en mis manos y boca arriba en la cama empezaba a moverlo en el aire para cubrir todo el paisaje, que se iba llenando del color, se iba saturando de cromatismo, como un oleaje en que paredes, rejilla, mesa, puerta, aire y luz se trazaban, todo, completo, hasta que mi mano se cansaba de moverse en el aire, pero ya repleto de un color intenso que me servía, al menos en ese momento, del más bonito cielo que alguna vez pude pintar.

#### **\*\****

2016. Caracas, Venezuela.

Luego de dos años y medio en La Tumba, en la sede del SEBIN, Lorent Saleh y Gabriel Vallés son trasladados a la sede del Helicoide, otro edificio carcelario del organismo de inteligencia. De esas experiencias relatan, más que algún trato inhumano que hayan sufrido por sí mismos, haber sido testigos de palizas, humillaciones, violencia sexual, abandono sanitario y robo a pertenencias de los otros reclusos. Quizás su juventud para el momento y el haber llegado con la fama de su pasaje por La Tumba, los protegió de la saña de los carceleros. Finalmente, en octubre de 2018, obtienen la libertad a cambio de, dicho concretamente, salir del país. Saleh va a España y Vallés a Colombia. Su liberación obedeció entre otras cosas al control de daño que realizó el gobierno venezolano ante la muerte en manos del SEBIN, unos días antes, del concejal opositor Fernando Albán al caer al vacío desde un décimo piso en la misma sede en que Saleh y Valles estuvieron subterráneamente encerrados.

#### **\*\****

2020. En algún lugar de América.

La violencia absoluta, el sadismo desmedido, la disposición de un cuerpo o una mente a la voluntad más irrefrenable de un verdugo han acompañado la historia aquellas naciones que se han sentido elegidas para reclamar su merecido lugar en la tierra. La historia de los tormentos acompaña a aquellos pueblos hegemónicos que, como parte de su leitmotiv, deben inventarse contrincantes dignos de ser sometidos a las mayores crueldades. 

Recientemente, su sofisticación o mejoramiento técnico, además de delatar un deseo ciego de sometimiento absoluto al poder, es propio del espíritu de nuestra época y su tendencia a mejorar la eficiencia, es decir, a menor esfuerzo con mejor resultado. Aun así, el horror más intenso suele venir cuando es el mismo estado el que aplica los castigos más severos sobre su población. Ahí no existe solamente supremacía política por ganar, ni fondos públicos que ahorrar. Ahí es el amedrentamiento y castigo en su fase más perversa, que atraviesa a todos los cuadros de quienes ejercen el poder político y los hace cómplices. Los cuerpos y las almas de todos los atormentados penan para siempre en sus torturadores. En los torturadores del estado. Y los ciudadanos están para recordárselos.

#### **\*\****

2021. Colombia.

Si, por supuesto, eso me acompaña siempre. No solo al salir de ese lugar físicamente, que te reencuentras con los tuyos, con la ciudad, con el campo, con los árboles, con el mar. Ah, el mar. No. Además lo difícil es que salga de tu mente. Que no quedes atrapado allí. Saber vivir con eso. Recién puesto en libertad haciendo un trámite para procurar mi estado legal sufro un ataque de pánico. Hay mucha gente que sufre estrés postraumático, creo que lo llaman los psicólogos. Puedes ir a terapia con un profesional o lo que te funcione.

 Hay gente que renueva la lucha política, más bien los potencia. Lorent y yo, y muchos otros seguimos. Es como inevitable. Pero no, de esas heridas no te curas nunca, las tratas, las trabajas, las atiendes, pero están ahí. Hay cosas incluso que no se pueden contar. Uno hace el esfuerzo, pero no siempre es posible, porque uno quiero contarse la historia para uno mismo, que fue lo que pasó. No es posible contarse todo, no es como una película que la puedes poner para atrás y ver completa. Lorent encontró una vía, se armó algo con teatro, se hizo un performance, se representó a sí mismo en la celda. En Bélgica, gente de la Unión Europea lo fue a ver. Muy intenso, muy atrevido. No todos podrían hacer algo así. Ojalá tuviera más repercusión de la que tuvo, luego ocurrió la pandemia y pasamos a otra cosa. Está en internet, creo. Yo también me fui al arte.

 Yo soy ingeniero en informática, pero ahora estudio arte. Mi experiencia con los colores y la importancia que tuvieron ya no la puedo dejar atrás. No sé si vaya a pintar o algo. Puede ser que sea para echarme el cuento de mi propia vida con colores. Hay mucha arte terapia muy buena, yo no he hecho, pero es innegable que me ha interesado. Vamos a ver, no sé si hay ingenieros que pinten después y que sean buenos. Vamos a ver. Yo creo que puedo servir de color. Eso. Servir de color a alguien o de alguien, aunque suene cursi. Aun con el color blanco, no me importa. Me gustaría ayudar a pintar a mi país de nuevo. Empezar por los márgenes y luego ir al centro, hasta que quede bien bonito. Soy capaz de ir a pintar el SEBIN completo, te lo juro. Y pintaría a Fernando Albán y Acosta Arévalo y a tantos otros. Y pintaría una frase que diría “El color salva”. El color salva.

*`Texto construido a partir de los relatos públicos reales de las víctimas. El agregado ficcional, en algunos casos indistinguible de los hechos, espera ser respetuoso con la casi inenarrable experiencia que con valentía se atreven a denunciar al mundo Lorent y Gabriel.`*

<!--EndFragment-->