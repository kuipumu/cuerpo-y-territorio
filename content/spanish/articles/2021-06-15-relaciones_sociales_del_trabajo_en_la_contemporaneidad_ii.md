---
title: "Relaciones sociales del trabajo en la contemporaneidad (II) "
subtitle: La auto-explotación en el neoliberalismo
cover: /images/uploads/pexels-photo-3194519.jpeg
caption: pexels.com
date: 2021-06-15T15:03:46.214Z
authors:
  - Luis Gabriel Aparicio
tags:
  - Trabajo
  - Neoliberalismo
  - Autoexplotacion
categories:
  - Politica y Economia
comments: true
sticky: true
---
Hemos hablado un poco sobre la búsqueda de la liberación de la clase trabajadora frente al patrono en la teoría marxista.

En el neoliberalismo el propio sistema parece ofrecer, al menos en apariencia, **ciertas libertades a las y los trabajadores para poder ser independientes y ejercer libremente sus capacidades creadoras** y generar valor, riqueza y sus propios medios de existencia, con el solo uso de sus capacidades individuales de trabajo puestas al servicio del mercado.

Con la aparición del neoliberalismo y la era digital se van desarrollando **nuevos sujetos y nuevas relaciones de trabajo**. Aunque siempre han existido personas que ejercen trabajos y labores independientes, en este momento de la historia parece que ese sujeto “libre” es el modelo que se pretende **imponer como rol o modelo de trabajo a toda la sociedad**, y es sobre estas modalidades libres de trabajo que queremos profundizar en este momento:

Específicamente las y los trabajadores **“freelance” y el “emprendimiento”** como forma de trabajo y de producir valor, y la manera en que estas actividades se enmarcan en las nuevas relaciones de trabajo en las sociedades neoliberales.

El sujeto ideal que plantea el neoliberalismo es aquel que se convierte en un **empresario de sí mismo**, es decir, ella o él cómo los únicos productores de su propia riqueza.

Ya no es solo su fuerza de trabajo lo que se ofrece a la producción o tal vez su capital (que puede existir o no), sino también todo el tiempo, energías, capacidad creativa, y finalmente su propio ser. **La producción se vuelve la parte más importante en la existencia de los sujetos neoliberale**s, pues a través de la construcción de sus empresas se van construyendo a sí mismos.

Es por eso que el sujeto propuesto por el neoliberalismo no depende solo de la inversión de capital y trabajo, si no de la inversión en ellos mismos, es decir, la **auto inversión y la autoformación**, pero no la formación en cualquier ámbito sino en algunos campos muy específicos dirigidos a la producción de valor.

Al depender de sí mismos mientras más aptitudes, capacidades y habilidades aplicadas a la producción se aprendan y dominen, se puede ser más competitivo o competitiva en el mercado, lo que se enmarca perfectamente en la teoría del **“capital humano”.**

Estos sujetos aparentemente libres, pues **no dependen de un jefe o figura de autoridad** que los controle o en palabras de Foucault los **“discipline”**, se relacionan con el trabajo de manera distinta que en las sociedades disciplinarias planteadas por este autor.

La autodisciplina, la propia gestión del tiempo y la hiperproductivad terminan por convertirse en la **dinámica laboral de estos sujetos**, que generan consigo mismos una relación que podemos definir como **“auto explotación”**.

### El emprendimiento

El emprendimiento no es para nada una categoría contemporánea, de hecho, emprender significa según la RAE “acometer y comenzar una obra, un negocio, un empeño, especialmente si encierran dificultad o peligro” (1).

Por lo que el emprendimiento como actividad es una de las características más antiguas de las sociedades humanas.

Ahora, el emprendimiento en términos económicos es una **concepción que aparece en las sociedades donde ya existe la mercancía, el intercambio y la acumulación**. Debemos también señalar que esta concepción económica siempre viene acompañada con otro término que la complementa: “negocio”.

Del latín necotium, compuesto de nec (negar/no), y otium (ocio/tiempo libre), puede traducirse como lo contrario al ocio o lo que no es ocio, por ende, el término “negocio” viene vinculado con cualquier actividad, ocupación o trabajo dirigida a fines lucrativos.

En el neoliberalismo el hombre emprendedor o **“homo redemptoris”** (2), se distingue del **“homo economicus”** que es el término acuñado por el economista John Stuart Mill en el siglo XIX que describe al capitalista tradicional como un individuo frío y calculador, al cual solo le interesa la acumulación y reproducción del capital. **El sujeto emprendedor en cambio, tiene unas características distintas.**

El papel de este sujeto emprendedor en la economía moderna lo describe Joseph Schumpeter, en su obra Teoría del Desenvolvimiento Económico (3). Ahí **plantea el papel central de los empresario**s como motores para dinamizar los procesos económicos, a través de la creación e innovación, no solo de nuevas formas de crear valor, sino de procesos novedosos en áreas tradicionales de la economía.

“Para Schumpeter, el empresario más que un capitalista o un inventor, es un liderazgo emprendedor y creativo. No necesariamente inventa cosas nuevas, **pero si aplica innovaciones al proceso económico**. Mientras un capitalista, o bien puede serlo por herencia, o bien disfruta de un viejo emprendimiento ya consolidado, y solo se ha dedicado a la acumulación; el emprendedor empresarial es en buena medida **un aventurero**, un pionero, que moviliza fuerzas materiales y humanas a la realización de innovaciones en el campo de los negocios.” (4)

Según lo aquí planteado, no es solo la explotación de los medios de producción lo que genera valor en la economía, sino que con la incorporación de elementos como la **“innovación” y “creatividad”** al proceso de producción, se puede explicar de forma más adecuada el proceso de creación de valor en el mercado contemporáneo.

Estos planteamientos son centrales para el neoliberalismo y su discurso del emprendimiento y los sujetos emprendedores. **Pues dicen que no es necesario el poseer grandes capitales o producir a gran escala para poder tener impacto en la economía y acumular capital**, ya que a través del uso de la creatividad y la innovación de nuevos procesos se puede alcanzar a ser un empresario exitoso.

Se disemina y populariza la idea de que **cualquiera** con los conocimientos y herramientas adecuadas, además de su fuerza de trabajo, y sin importar su origen social **puede convertirse es un empresario exitoso**, millonario, y también un líder.

Aunque esos nuevos liderazgos tienen unas características y valores distintos a los vistos en el capitalismo tradicional. Es la creatividad y la capacidad de crear impacto en el mundo, generalmente a través del uso e **implementación de nuevas tecnologías en la sociedad y la economía**, lo que prevalece como valores positivos.

Los emprendimientos que promueve el neoliberalismo son de **dos tipos** principalmente: los emprendimientos de **tipo tecnológicos** donde la innovación en esa área es su centro; y los emprendimientos más pequeños o **comunitarios** que no requieren altos niveles de capacitación en esas áreas, pero con la posibilidad y potencialidad de escalar hasta convertirse en PYMES.

Lo que tienen en común ambos es que son proyectos individuales e individualizantes en los cuales el emprendimiento o la empresa se vuelve una parte indisoluble del individuo. Es el individuo empresario de sí mismo, es decir, la empresa es una **extensión de él** o ella y al serlo solo depende de sí mismo para producir sus medios de vida.

La acción de emprender viene asociada e **impulsada por ideas y sentimientos de libertad**, superación y autorrealización. Es por eso que la o el sujeto emprendedor al no depender económicamente de un salario, sino de sí mismo, debe auto regular sus ideas, funciones y acciones a su proceso productivo, es decir, **se autoexplota para poder tener un mayor nivel de ingresos, productividad y rentabilidad**.

### Las nuevas profesiones en la era digital

La creación e implementación de **nuevas tecnologías** en los procesos económicos ha generado grandes cambios en la sociedad durante las últimas décadas Gracias al desarrollo de las Tecnologías de la Información y la Comunicación (TIC) y el uso masivo del internet podemos hablar de una sociedad globalizada.

Estos avances tecnológicos van de la mano con el cambio de paradigma que plantea el neoliberalismo, de hecho, podemos decir que son parte de un mismo proceso ya que se complementan y retroalimentan uno del otro.

Por una parte, el desarrollo tecnológico y su implementación en los distintos campos de la economía y la sociedad, y por otro lado, las formas en que se construyen nuevas relaciones sociales y otras maneras de entender el mundo a partir de estos hechos.

En el ámbito laboral y la creación de valor, que es lo que nos atañe, en las últimas décadas se han creado una gran cantidad de formas de **generar valor** a través del uso del **internet y las TICs**, e innumerables nuevas profesiones han surgido a partir de esto.

La economía digital y la digitalización de las finanzas han dado apertura a los mercados de intercambio de valores, haciendo que cualquier persona pueda desde un ordenador realizar operaciones bursátiles.

Otro ejemplo es la creación de la **cripto economía** a partir de la cual ya no es necesaria la emisión de moneda respaldada por el Estado y los bancos, sino que se genera valor e intercambio con el uso de herramientas tecnológicas, aunque algunas requieran enormes cantidades de energía eléctrica para poder mantenerse como es el caso del Bitcoin.

Por otro lado una cantidad cada vez mayor de trabajadores son llevados al campo laboral en modalidades “freelance” para ofrecer su trabajo en distintas plataformas virtuales.

**Ana Rameri** define la actividad freelance de la siguiente forma:

*Los freelancers son mayormente ocupaciones independientes, de carácter flexible, expuestas a una vertiginosa aunque inestable cantidad de trabajo que promueve niveles de autoexplotación desmedidos para enfrentarse a los estándares de mercado* (5).

Las actividades en este mercado son en su mayoría las asociadas a profesiones digitales (diseño web, marketing, redes sociales, etc.). Estas relaciones de trabajo parecen **descolocarse en el territorio**, ya que pueden realizarse desde cualquier parte del mundo y la interacción entre empleador y empleado es casi siempre a través redes digitales.

Es importante señalar que el ejercicio de estas actividades se concentra sobre todo en las **ciudades**, pues es allí en donde se encuentran las mejores condiciones de servicios para realizar estas tareas (conexiones óptimas a internet y servicios eléctricos de mayor calidad).

En este mercado global las y los trabajadores no solo colocan su fuerza de trabajo y tiempo a la producción, sino también sus propios equipamientos para llevar a cabo las labores por las cuales se contrata.

Ya que no existen casi regulaciones sobre este tipo de labores, en este mercado rigen las leyes de oferta y demanda, que en la inmensa mayoría beneficia al contratante frente a quienes prestan algún servicio.

Como ya dijimos este mercado, aunque parece estar “descolocado” a nivel geográfico, mantiene igual que en la división internacional del trabajo **centros en donde se encuentra el capital y periferias donde se concentra la fuerza de trabajo.**

Finalizaremos este ensayo en la **tercera** y última parte, donde hablaremos sobre la división internacional del trabajo, la desregularización del mercado laboral y sus implicaciones económicas, sociales y psicológicas para las y los trabajadores.

### Referencias y fuentes

(1) Diccionario de la Lengua Española, Real Academia Española, página oficial.

<https://dle.rae.es/emprender>

(2) Puello-Socarras, José Francisco (2008) ¿Un nuevo neo-liberalismo? Emprendimiento y Nueva Administración de “lo público”. Ver.

<https://www.researchgate.net/publication/306292387_Un_nuevo_neo-liberalismo_Emprendimiento_y_Nueva_Administracion_de_lo_publico>

(3) Schumpeter, J. (1997) Teoría del Desenvolvimiento Económico. Fondo de Cultura Económica. México.

(4) Delgado, Luis (2019) Sobre el emprendimiento. Manuscrito no publicado.

(5) Rameri, Ana (2018) El emprendedurismo: El nuevo ropaje neoliberal, La Causa Laboral.