---
title: "“¿Sueñan los posthumanos con androides rizomáticos?”  Por Tomas de Kempis "
subtitle: "Prólogo a la primera edición "
cover: /images/uploads/sigmund-freud-negacion-surrealismo.jpg
date: 2021-03-31T15:42:23.934Z
authors:
  - Roberto Salazar
tags:
  - Cultura
  - Literatura
  - Humanidad
categories:
  - Cultura
comments: true
sticky: true
---
*Yo soy inmenso…*\
*y contengo multitudes.*

  Walt Whitman



Probablemente los libros que suelo disfrutar más, aquellos que permanecen en mi memoria durante periodos inusitadamente largos y sus enseñanzas son recuperadas con amena facilidad, esos que insisten en retornar en los momentos en que la realidad cruje,se desvanecen y nos quedamos sin nada útil o certero que decir, son aquellos libros en los que se habla de todo y de nada. No, no hablo de cualquier escrito de Nietzsche.

Creo que tuve una educación “borgeana”, rodeado de cualquier cantidad de enciclopedias o manuales de instrucción o incluso prólogos a traducciones del francés o del alemán. Había poca, por no decir ninguna, obra de ficción, pero era generosa la cantidad de textos de filosofía, medicina oriental, antropología, lingüística y cocina, entre otros, que podría encontrar en la vieja biblioteca de cierto familiar que prefiero no nombrar. 

Siempre que me preguntaban que estaba leyendo, y como la explicación no era ni fácil ni corta de dar, terminaba tajantemente con  “Un libro de muchas cosas”. Probablemente la portada de ese viejo libro no ayudaba a descifrar el tema en el que me veía inmerso, por lo cual mi interlocutor no insistía mucho más en conocer mi objeto de interés.

Creo que fue cuando tenía unos 15 o 17 años en que me topé con aquel voluminoso ejemplar de Douglas Hofstadter “Godel, Escher, Bach: an Eternal Golden Brain”, obra notable que terminó marcando mi desarrollo intelectual y profesional.

 Se trata de uno de esos libros que hablan de todo, de la inmensidad misma, del universo, el inicio y del fin, pero al mismo tiempo terminan siendo una gran broma, un acertijo, termina versando también sobre la nada misma. Sería un exabrupto de mi parte decir que habla de todo y de nada, pero más de 700 páginas de recorrido hacen que tenga un toque agustiniano: si me preguntan de qué se trata, no sé qué responder. Si no me preguntan, lo recuerdo todo. Y hago un retruco: como la canción de Gardel, siempre se vuelve al primer amor y es mi caso con el libro de Hofstadter. Cualquier fenómeno político, económico, biológico, filosófico, puede estar contenido en esas páginas, en algún grado de especificidad según su compleja arquitectura.

Hay gente que mira la realidad según aquellos lentes kantianos trascendentales. Mis viejos profesores lo veían según el cristal marxista (muchos de ellos todavía lo ven). Luego otros compañeros de estudios entendían el mundo a través de una comprensión einsteniana. Mi mujer, sin ir tan lejos, lee mucho en clave freudiana. 

Soy de los que piensa que mientras más diversa y heterogénea sea esa clave de lectura, más poderoso será el alcance para poder entender la realidad y, como sostenía más arriba, más cómoda la facilidad para volver a estas lecturas cruciales.

El segundo exabrupto que voy a decir acá va a ser que precisamente este libro que tiene usted en sus manos, sea digital o en físico, que en rigor no se ha terminado de escribir, porque como libro abierto es libro incompleto o más bien libro-potencia, que solo puede continuar pulsando; este libro sigue la estela de aquellas obras que buscaron hablar de un amplio número de temas de forma rigurosa, pero al mismo tiempo con una cadencia no propia de los libros técnicos o incluso de textos de divulgación. Es decir, hay un esfuerzo por circunscribir el Todo en un libro, o como decía el poeta Blake “abarca el infinito en la palma de tu mano y la eternidad en una hora”

Hacer un prólogo de semejante propuesta podría ser no menos que una labor titánica, puesto que su heterogeneidad es digna de la mente de un Ireneo Funes o un Monsieur Teste. Sin embargo, en las siguientes líneas voy a proponer un argumento general sobre “ Sueñan los posthumanos con androides rizomáticos?” y un desglosado de los apartados que lo conforman. 

Juzgará el lector si este esfuerzo de anticipar el contenido se condice con su propia experiencia de lectura. Lo performativo, las meta referencias que plantea su autor, Tomás de Kempis, imitando a su modo al ya mencionado Hofstadter, se verán resentidos como aquellos forzamientos que se hacen cuando se privilegia el fondo por encima de la forma. Y hete aquí, precisamente, el sentido general de la obra: el antiesencialismo.

Kempis va al grano al plantear que el origen de todos los males vendría a ser esa vieja rémora de la filosofía: pensar el ser como ente, es decir algo cerrado, completo, acabado. Desde Parménides a Heidegger, Kempis en la introducción nos va ir desglosando de manera muy lúdica, casi a base de juegos de palabras lo que sería la historia del ser o la historia de la metafísica. 

Lo inmutable versus lo que cambia. Ser o devenir. Y esto cruza finalmente en Charles Darwin y la evolución de las especies: finalmente el sujeto humano sale de sí mismo y quizás no fue nunca un ser acabado sino un producto de una sucesiva e imperceptible cadena de cambios y adaptaciones que cristalizan en el Homo Sapiens. Y acá dispara la primera hipótesis controvertida: nunca entendimos realmente a Darwin y su descubrimiento.

 No estuvimos a la altura del descentramiento humano, de entender que somos una especie más que habita el planeta. Precisamente cuando aparece el descubrimiento darwiniano se redoblan las ideologías: cientificismo, mercantilismo, fundamentalismo, nacionalismo. ¿Es acaso la mente humana, si es que se le puede nombrar así, inevitablemente ontologizante, monoteísta, patriarcal y de cualquier otro orden cerrado y absoluto?

En la primera parte del libro Kempis retoma a un autor casi olvidado hoy día, Piotyr Kropotkin, y los esfuerzos de este para poder hacer una lectura digna del giro copernicano de Darwin. La propuesta del anarquista ruso es interesante en tanto que privilegia una lectura romantizada hoy día pero que a finales del siglo XIX estaba lejos de ser obvia y que trata sobre la dificultad de pensar que el darwinismo es ante todo supervivencia del más fuerte y el egoísmo supino. 

Más bien todo lo contrario: es la adaptación extrema, la adaptación por encima de todo incluso a fuerza de no necesariamente parecer una evolución. Más se han ayudado a sobrevivir las especies colaborando entre sus miembros que enfrentándose.

 Los ejemplos proporcionados por Kropotkin (de formación geógrafo) y otros son inagotables en este sentido: el devenir de la vida no es sin la ayuda mutua, la cooperación, hasta la simbiosis, entre especies. La lectura del ser vivo desarrollado como depredador voraz no es sino una construcción interesada, antropomórfica, de la realidad.

Antes de hacer un interesantísimo análisis del mundo vegetal, Kempis no deja de hacer notar que las ideas políticas modernas de altruismo, cuidado al prójimo, asistencialismo, incluso cierto estatismo, son en sí mismo humanizaciones, son ideas hasta cierto punto estériles, demasiado metafísicas, inamovibles, rígidas, litúrgicas, de ese intercambio mutual que se da entre otros seres vivos.

 Es la trampa binaria del individualismo o el colectivismo en la que termina gravitando las formaciones partidarias, al menos desde que se empezó a distinguir entre izquierda y derecha a casi 100 años de los trabajos de Kropotkin. En esto insistirá Kempis una y otra vez, en la testarudez de nuestra cultura en la Verdad.

Dos investigadores de nuestra época, Stefano Mancuso y Ed Yong, serán los guías de lo que resta de la primera parte de “¿Suenan los posthumanos con androides rizomáticos?”. Entendiendo cómo se organizan las plantas y las bacterias, comprendiendo cómo los seres supuestamente menos evolucionados logran tener una inusitada longevidad y capacidad de adaptación, Kempis asomará otro de los grandes problemas de nuestra especie: la organización centralizada.

En esto conviene detenerse. Las plantas y otros organismos inferiores tienden a distribuir sus funciones de una manera no orgánica, es decir, no tienen un órgano central que coordine ciertas funciones vitales y que sea irremplazable.

 A diferencia del cerebro o del corazón en los humanos, que al sufrir un daño severo acabará con la vida del cuerpo que lo aloja, las plantas pueden reconstruirse y reformarse aun cuando gran parte de su organismo se vea casi destruido.

 Esta extrema adaptabilidad, esa concentración de funciones vitales en varios lugares, les redunda en menores esfuerzos energéticos y mayor adaptabilidad a un sinfín de condiciones a los que los humanos apenas podemos soñar lograr (con la excepción, quizás, de la vida en el espacio exterior). 

La organización centralizada, jerárquica, funcional, permitirá logros en el animal humano inéditos, sobre todo a nivel cerebral. Pero a un costo que no nos atrevemos a decir y es el precio que paradójicamente tenemos que pagar: una extrema fragilidad.

Así, nuestro organismo ha evolucionado de forma centralizada, con órganos dominantes para llevar a cabo ciertas funciones específicas. Pero nuestra inteligencia, digna de mejor causa, ha replicado la comprensión de la realidad de la misma manera. 

Organizamos la concepción del mundo que nos rodea como si existieran instancias más elevadas, órganos mayores, que ordena ciertas funciones, que estas a su vez van a regular otras funciones, y así hasta hacer un esquema burocrático de los fenómenos de la vida. 

Con de Kempis podemos empezar a extraer el corolario de que las grandes organizaciones sociales y culturales de la humanidad, a saber, la religión, la filosofía, la guerra, el patriarcado, el lenguaje, la familia y el parentesco, el saber, los imperios, el feudalismo, el Estado, los sistemas presidencialistas, finalizan en el gran agujero negro que todo lo atrae: el orden.

“Si los bueyes pudieran hacer dioses, estos tendrían cabezas de vaca” decía Jenofanes. Porque para el humanismo autocomplaciente no puede ser de otra manera de que la realidad coincida con lo que ya creen. Richard Rorty se esforzará en desmentir esto en algún lugar, dice Kempis, pero no nos interesa: nuestro antropocentrismo es un viejo síntoma que se despliega en cualquier obra que vamos construyendo. Con esto irá cerrando el primer apartado de “¿Suenan los posthumanos con androides rizomáticos?”. No de una forma muy esperanzadora, definitivamente.

La segunda parte del libro resulta aún más densa que la primera puesto que es una cavilación entre la filosofía de Gilles Deleuze y los desarrollos más modernos de lo que da parte del nombre del libro, el posthumanismo. Deleuze será un digno sucesor del cuestionamiento al esencialismo que primaen la filosofía en primer lugar y en otros campos del saber, como el psicoanálisis, la antropología y las otras puntas de lanza de la French Theory. 

Se trata para Deleuze de oponer a la organización jerárquica el rizoma, ese concepto performativo que propone para salir de los límites del significado definitivo y el saber expuesto e inapelable. Precisamente el rizoma viene de una forma de organización horizontal de las plantas, de las raíces, de la carencia de un centro. Incluso va más allá Deleuze y sostiene que muchos fenómenos sociales, la economía, las luchas políticas, ciertos fenómenos de masa, ciertos lenguajes, el cuerpo, las identidades sexuales, siguen la lógica no jerárquica y sí la del rizoma.

¿Es posible pensar un posthumanismo? Un verdadero posthumanismo, en tanto que se deconstruya el lugar que tiene el ser humano dentro la comprensión de la vida y la realidad del mundo. Kempis es ambiguo en esto. Hemos aprendido de otras especies y hemos acumulado conocimientos de una manera que nos han ayudado a mejorar nuestra propia adaptabilidad. Pero no sin ir demasiado lejos en un posicionamiento que termine de dejar atrás ese viejo humanismo y sus taras ya conocidas. 

Quizás acá el libro se torne un poco oscuro al tratar de llegar al límite de pensamiento, al límite del lenguaje, al encontrarse con el viejo problema de la metafísica, es decir, trata de hacer filosofía sin metafísica. Es el nivel ad infinutum que siempre habrá al tratar de superar la reflexión y la meta reflexión, como bien lo formuló en términos matemáticos el Kurt Godel narrado por Hofstadter.

La tercera parte del libro es definitivamente una provocación. Es ir a romper con el antropocentrismo y su organización de la realidad a través de un producto de sí mismo. La técnica. A partir de una lectura de Bernard Stiegler y Martin Heidegger, Kempis propone desterrar para siempre los cristales humanistas al acelerar, al apurar el desarrollo técnico (no necesariamente el tecnológico), es decir extremar la organización de la información de modo que puedan aparecer lugares novedosos desde donde operar sobre la realidad. 

Esto parece acercarlo al movimiento del aceleracionismo, que es una suerte de optimismo tecnológico, o ponerlo a Kempis en un lugar de futurista. Lejos de esto, Kempis lo resuelve muy bien. No se trata, dice, de hacer un nuevo movimiento, hacer otra nueva esencializacion de la realidad, otro nuevo invento de cómo organizarnos, se trata más bien de apostar a los pequeños cambios, a las pequeñas técnicas, a los pequeños espacios en donde las invenciones puedan tener lugar en un ecosistema de información que fluyen desde lo animal, de lo vegetal, desde lo microscópico. Se trata en el fondo, de causar entropía para precisamente evitar la entropía mayor: el humanismo.

No podía cerrar un libro tan audaz sino con un largo test para hacerse uno mismo, respondiendo a 75 preguntas de diversa índole, tan graciosas como ingeniosas, para uno terminar ubicado en algo que Kempis dirá que es una “no-categoría” de lo posthumano. Es decir, que tan humanocéntrico o no resulta el lector. No le adelantaremos ninguna pregunta acá, aunque puede pasar directamente a responderlas si los temas planteados por el autor les parecen de lo más aburridos.

El caso es que encontrará en este libro las disertaciones más barrocas, las anécdotas más significativas, los juegos de palabras más extraños y las conclusiones más provocadoras, una invitación a pensar por sí mismo, a atreverse a estar de acuerdo con todo lo que dice Kempis o con nada de lo que dice, a encontrar eco en la realidad que usted se encuentre sumergido o más bien a repudiar otro ejercicio claro de vanidad intelectual. Lo que probablemente pasará es que no lo dejará indiferente.

En todo caso, “¿ Sueñan los posthumanos con androides rizomáticos?” lo construirá cada uno en su propia mente humana. O posthumana.

Jan van Ruusbroec

2021