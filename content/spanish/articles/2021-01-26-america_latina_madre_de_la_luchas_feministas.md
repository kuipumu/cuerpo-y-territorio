---
title: América Latina, madre de la luchas feministas
subtitle: " "
cover: /images/uploads/03_web_1024x768.jpg
caption: "Imagen: Fondo de Acción Urgente"
date: 2021-01-26T02:26:44.959Z
authors:
  - Florencia Cencig
tags:
  - Argentina
  - Género
  - Feminismos
  - Latinoamérica
  - Derechos
  - Lucha
  - Historia
  - DDHH
  - AbortoLegal
  - Ley
  - IVE
  - Mujeres
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

A un mes de que el Senado argentino haya aprobado la **Ley de Interrupción Voluntaria del Embarazo**, es imposible no reflexionar respecto a larga lucha por los derechos de las mujeres, y el recorrido que tiene el feminismo en nuestra querida Latinoamérica.

Sin duda, creo que **romper con el machismo y la discriminación hacia la mujer latinoamericana implica, primeramente, deconstruir el lugar desde el cual nos situamos al observar a nuestro propio continente,** y reconocer que las desigualdades y diferencias también se trasladan al mundo del feminismo.

Así como a los ojos del sistema mundial existen países de primera y de segunda, y ciudadanos de primera y de segunda, deberíamos problematizar cómo esta lógica se reproduce dentro del universo feminista. De la misma forma en la que el mundo está determinado por el pensamiento eurocéntrico, occidental y heterosexual, **el feminismo tiene como figura representativa a la mujer blanca, hetero y de clase media.**

Es por ello, que me parece esencial darle voz a aquellas mujeres silenciadas por una historia que arrasó sobre ellas, pausando y aniquilando sus saberes más preciados. **¿Cuánto sabemos del rol que tenía la mujer indígena dentro de sus comunidades?**, ¿cuánto conocemos de las mujeres nativas que pelearon por mantener la identidad de nuestra tierra?, ¿y cuánto de aquellas negras migrantes que se vinieron violentadas y esclavizadas, y no tuvieron otra opción que construir sus vidas en un nuevo territorio, resistiendo a todo tipo de violencia?

En concordancia con la feminista colombiana Moore Tores en “Feminismos del Sur, abriendo horizontes de descolonización. Los feminismos indígenas y los feminismos comunitarios” (2018), **las mujeres del tercer mundo no somos ahistóricas ni monolíticas y, por sobre todas las cosas, sabemos muy bien que la opresión sexual que padecemos es consecuencia de la dominación colonial que vivimos a lo largo del tiempo.** Nuestra historia es una historia del patriarcado. Patriarcado ancestral o indígena, patriarcado colonial, y patriarcado capitalista.

Así, notamos como somos el resultado de una lucha constante que se inicia desde la subordinación dentro de las propias comunidades originarias, pasando por la brutalidad de la colonización, y sufriendo las desigualdades que genera actualmente el capital. Por lo tanto, **es imprescindible formar pensamientos contrahegemónicos y descolonizantes que rompan con la sistemática violencia hacia la mujer.**

Para llegar a esto, creo que es elemental establecer nuestros posicionamientos desde el feminismo comunitario. El cual, no solo exige una vida libre de violencia, sino el cumplimiento del derecho a una vivienda digna, a la educación, a la salud, al agua y, sobre todo, el respeto por el territorio de los pueblos originarios, la vida en comunidad, y la reivindicación de la fuerza de sus ancestras.

Por eso, **conocer y adoptar la mirada de aquellas minorías y colectivos desplazados históricamente, da lugar a acompañar sus luchas** de forma que puedan recuperar su lugar, su poder y visibilidad, como sujetas y sujetos creadores de la identidad de nuestro continente.

Para promover realmente el cambio, la cohesión social y la liberación de las personas; debemos acompañar procesos de transformación en los cuales nuestra población originaria recupere sus territorios, accedan a espacios de organización y poder político, formen parte de todas las decisiones que afecten a su integridad cultural y a sus tierras y, no sólo tengan un lugar de poder en sus propias instituciones, sino en las organizaciones del Estado.

**El día en que los cargos políticos y espacios de representación ciudadana tengan rostro de mujer indígena feminista y no nos parezca extraño, será entonces, el día en que realmente hayamos ganado terreno en materia de igualdad, derechos humanos y justicia social.**

<!--EndFragment-->