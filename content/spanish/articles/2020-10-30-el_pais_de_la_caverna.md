---
title: El país de la Caverna
subtitle: " "
cover: /images/uploads/portada.png
date: 2020-10-30T12:06:20.665Z
authors:
  - Jefersson Leal
tags:
  - Política
  - Economía
  - Elecciones
  - Estado
  - Venezuela
  - Propaganda
  - Poder
  - Dictadura
  - Medios
  - Pobreza
  - CanalesDelEstado
  - Latinoamérica
categories:
  - Política y Economía
comments: true
sticky: true
---
La *mass media* (medios de difusión) desde siempre ha actuado como el mejor mecanismo de imposición de la ideología dominante y en cada contexto histórico posicionan ideas y creencias que son favorables a los que controlan el poder político y económico de una sociedad. Esto se realiza con el objetivo tácito de **mantener a la sociedad en el cauce de la “normalidad”.**

**El Estado venezolano no ha perdido tiempo ni ganas en tratar de mantener a la población en la caverna de la cual hablaba Platón.** Su control férreo de la crítica mediática, se ha hecho sentir, con los innumerables cierres de medios: televisivos, radiales, impresos y más recientemente bloqueo de portales informativos.`1` Ni hablar de los constantes hostigamientos y persecución judicial de los comunicadores críticos contra las políticas del Estado.`2`

El gobierno venezolano a la par que intenta aplacar las voces de las disidencias, ha edificado un aparataje mediático que **intenta justificar su permanencia en el poder a través de discursos ideologizantes que falsean la realidad.** Estos elementos mediáticos giran en torno al Ministerio de Comunicación y su canal estrella: "Venezolana de Televisión", el cual intenta posicionar mensajes “normalizantes” en la población venezolana.

En esta construcción discursiva emanada desde los “medios públicos”, los desmanes de Venezuela son causados única y exclusivamente por el “bloque imperialista”, así EE. UU. y sus aliados son los villanos y el gobierno de Nicolás Maduro es una especie de redentor terrenal, al mejor estilo de Jesús Cristo en la cruz.

**El hambre, la desigualdad, la carencia de servicios públicos y los bajísimos salarios son presentados por esta mediática como actos heróicos, en un discurso tan tenebroso que romantiza la desigualdad.**

Recientemente, en el marco de la convocatoria “electoral” para las “votaciones parlamentarias” que se realizarán el 6 de diciembre de 2020, el Partido oficialista (PSUV) ha desplegado una campaña con el slogan *“Ven Vamos Juntos”*, para esta campaña se elaboró un tema musical, el cual analizaremos, como ejemplo paradigmático de la política Gebbeliana que crea el gobierno venezolano.

A continuación, se muestra una serie de imágenes extraídas del video clip de la canción *“Ven Vamos Juntos”*:

![](/images/uploads/diseno_sin_titulo.png)

Lo primero que salta a la vista son estos tonos pasteles que evocan una tranquilidad, neutralidad, avance y calma; unos colores que llevan al espectador a un espacio ilusorio, donde hay confianza (color azul) y esperanza (color verde). Está claro que el mensaje que intentan posicionar es de un país, donde la gente está alegre, segura y esperanzada. (Aunque usted no lo crea)

Como se ve, los personajes llaman al espectador, con una gestualidad familiar en el caso del adulto mayor y de más confianza y energía en el caso de la imagen de la mujer.

![](/images/uploads/diseno_sin_titulo_1_.png)

En las tres imágenes siguientes se muestra unas escenas donde los colores pasteles siguen dominando. En la primera escena hay tres personajes en un juego de color verde, el cual ha sido “destruido” por el personaje de sweater amarillo. El diseño de esta escena está construido para colocar al personaje con el sweater amarillo (que evoca al partido opositor “Primero justicia”), en el papel del villano, el cual le da un manotón al juego en común -de color verde- es decir, **Primero Justicia le da un manotón a la esperanza de la sociedad venezolana**. (Han pensado bien cada escena)

La imagen dos muestra una escena donde se encuentran un paciente y una doctora con un fondo azul, dando a entender que el sistema de salud nacional es confiable. La última imagen da la sensación de un adulto mayor muy feliz y lleno de esperanza. (Nada más alejado a la Venezuela actual)

**La campaña *“Ven vamos juntos”* evoca un país que no existe, muy alejado de la realidad diaria de cualquier persona que habita este país.** Esta campaña y su tema musical se fundamentan en los valores, la confianza, la alegría y la esperanza. Estos valores se entrelazan con tres grupos: Jóvenes, mujeres y adultos mayores. Como se pudo observar en las imágenes, los personajes lucen rozagantes, festivos, en un ambiente de prosperidad, con ropa nueva y llena de color. (¡sí ropa nueva!)

Pero la realidad venezolana está bien lejos de ser lo que se plantea en ese video clip. Según el más reciente estudio elaborado por un grupo de universidades, sobre la calidad de vida de los venezolanos: Encuesta de Condiciones de Vida (ENCOVI) **al menos 79,3 % de la población vive en pobreza extrema y el 96,2 % por debajo de la línea de pobreza.**`3` **Poco queda por decir antes semejantes cifras.**

Estos datos contrastan enormemente con la propaganda oficial. Mientras la campaña *“Ven vamos juntos”* nos muestran unos jóvenes felices y con un futuro prometedor, la realidad del país nos dice que Venezuela ha envejecido. El llamado bono demográfico que la nación debía tener entre el 2000 y el 2045, es decir, el momento donde la población joven superaría a la población más envejecida desapareció producto de la inmensa masa de jóvenes venezolanos que fueron exiliados por la economía en crisis de la nación, **“prácticamente ya tenemos indicadores de dependencia demográfica que debimos alcanzar en 2045”.**`4`

Ya los venezolanos más jóvenes no pueden costear un sistema de pensiones para mantener a la población adulta mayor. Este último grupo etáreo (abuelos y abuelas) es, sin duda, uno de los que más sufre los embates del caos, **su pensión mensual equivale a un salario mínimo Bs 400.000 o 0,85 centavos de dólar, sí ni un dólar al mes.** Realidad que dista de estar cerca de ese abuelo feliz y alegre que vimos en las imágenes más arriba.

**El país del Partido Socialista Unido de Venezuela, existe solo en los medios públicos de difusión masiva del Estado.** Esto con el objetivo claro de posicionar en la mente de los venezolanos una mentira insostenible. “Ven vamos juntos” es solo una muestra de las sombras que sistemáticamente los factores de poder intentan echar sobre la población, para mantenerla en la caverna y gobernar, sin tantos ruidos ni quejas. **El *chavismo tardío* en el poder, ha hecho del Estado, una maquina salvaje, la sangre y la mentira son dos elementos indisolubles de su nueva práctica política.**

**`1`** [El venezolano news](https://elvenezolanonews.com/2020/10/12/varios-portales-de-noticias-en-venezuela-sufrieron-un-bloqueo-de-24-horas/)

`2`Informe de la Alta Comisionada de las Naciones Unidas para los DDHH: A/HRC/44/20

`3`Victor Salmeron, Encovi 2019-2020 [¿Qué nos dice esta radiografía sobre la calidad de vida de los venezolanos?](https://prodavinci.com/encovi-2019-2020-que-nos-dice-esta-radiografia-sobre-la-calidad-de-vida-de-los-venezolanos/)

`4`Anitza Freites, directora del Instituto de Investigaciones de la UCAB. Citada en Victor Salmeron, Encovi 2019-2020 [¿Qué nos dice esta radiografía sobre la calidad de vida de los venezolanos?](https://prodavinci.com/encovi-2019-2020-que-nos-dice-esta-radiografia-sobre-la-calidad-de-vida-de-los-venezolanos/)