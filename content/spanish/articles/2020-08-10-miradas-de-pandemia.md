---
title: Miradas de Pandemia
cover: /images/uploads/miradas-de-pandemia.jpg
date: 2020-08-10 00:00:00
authors:
  - Micaela Pereira
sticky: true
comments: true
tags:
  - Fotografia
  - Fotoreportaje
  - Argentina
  - COVID-19
categories:
  - Cuerpo
description: En este contexto de COVID 19, a más de cuatro meses de cuarentena y
  aislamiento social, las temporalidades han perdido su lógica lineal.
---
En este contexto de COVID 19, a más de cuatro meses de cuarentena y aislamiento social, las temporalidades han perdido su lógica lineal.

Ahora podríamos pensar el pasado - futuro de una manera alternativa: la línea recta del tiempo se conforma poco a poco como un cubo.

El afuera constituido como una vida pasada (pesada), una especialización del tiempo, un detrás de la ventana. 

Salir a buscar miradas, vendría a ser, salir a buscar un presente, una impresión de él.
La mirada de un otro impone un presente absoluto, un diálogo entre la distancia.

Mientras las pantallas nos quedan chicas, mientras la virtualidad, que se va convirtiendo en realidad, nos ocasiona imaginarios exteriores, las miradas nos traen al lugar-espacio, de nuevo aquí, a la piel, al cuerpo, a los sentidos.

La búsqueda de mensajes en las paredes, las formas de caminar, los escenarios de las esquinas, las persianas bajas, los barbijos y las máscaras. También las rejas que se ven y las que no se ven, las propias y las ajenas. Todo parece de película, me comprometo a construir el relato.

Ahora que los escenarios son virtuales, inestables, la foto viene a imponer un punto de partida, un piso ante tanta información, redes invisibles, datos en número, cifras alarmantes.

La mirada, como lugar de construcción, en la ciudad de los plásticos, de las telas, de las capas, de los metros. Las ventanas como límites, la evidencia de las distancias que siempre estuvieron y ahora están más explícitas.

En mi afán de retención y captura, comprendo lo que veo, o al menos me acerco un poco a un alivio, a construir ese afuera que imagino desde adentro, a dejar un registro donde no bastan las palabras, ni la historia como la hemos vivido para tomarla de referencia.
Toda fotografía es un autorretrato. 

![](/images/uploads/1.jpg)

![](/images/uploads/2.jpg)

![](/images/uploads/4.jpg)