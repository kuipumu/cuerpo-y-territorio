---
title: "El proceso constituyente y las elecciones presidenciales en Chile "
subtitle: ¿Transitando hacia un modelo post- neoliberal?
cover: /images/uploads/118467521_chileapruebo5.jpg
caption: Fuente BBC
date: 2021-06-23T16:44:56.304Z
authors:
  - Kalil Abu-Qalbein Koda
tags:
  - Chile
  - elecciones
  - presidente
  - constituyente
categories:
  - Politica y Economia
comments: true
sticky: true
---
En noviembre de este año, Chile decidirá quién será el nuevo gobierno y gobernante que presidirá al país en los próximos cuatro años. Estas son, sin duda, las elecciones presidenciales más importantes desde el retorno de la democracia en los 90´, y tal vez la más importante en toda la historia según el estricto rigor democrático. No solo porque al fin se superará el duopolio presidencial que intercalaron Michelle Bachelet (2006- 2010; 2014-2018) y Sebastián Piñera (2010-2014; 2018-2022) en la última década y media, traspasándose el ejecutivo entre el bloque centroizquierda, encabezado por el Partido Socialista (PS), y la derecha, comandada por Renovación Nacional (RN), respectivamente. Sino porque hay una serie de factores y condiciones que demuestras que no estamos simplemente frente a un mero cambio de gobierno, sino que, ante todo, nos enfrentamos a un proceso de transformación estructural que abre la posibilidad histórica de superar al modelo neoliberal.

Dentro de la vía institucional nos encontramos con dos caminos por los cuales se transita de forma paralela para desembocar, esperemos, en esta gran transformación. Ambos caminos, sin embargo, se entrecruzan, yuxtaponen y co- construyen. De manera tal que es imprescindible generar una coherencia y cohesión entre ambos, para que el cambio elemental sea efectivo. Puesto que, si uno llega a buen puerto, pero el otro no, quedaremos constreñido en una contradicción de difícil, muy difícil salida.

Uno de estos caminos es el del proceso constituyente, lo que fuese el polémico resultado político de la revuelta del 2019 y que, a propósito, está recién comenzando. En principio, la ciudadanía se hizo presente de manera contundente en el plebiscito del pasado 25 de octubre del 2020 a favor del “Apruebo” de una nueva constitución, siendo la “convención constituyente” (100% de los miembros/as elegidos popularmente) el mecanismo predilecto para llevarlo a cabo, en perjuicio de la “comisión mixta” (50% miembros/as del congreso y 50% elegidos popularmente). Reflejando, una vez más, la crisis de representación de los poderes centralizados del Estado y el ahínco de la gente de disputar y hacer política desde sus territorios. En una votación un tanto más pareja pero asimismo desastrosa para los defensores del modelo, y favorable para la inmensa mayoría, fueron electos a mediados de mayo de este año los y las 155 constituyentes por distrito, quienes buscarán elevar las demandas y los deseos de la ciudadanía de manera participativa y vinculante. Finalmente, este cuatro de julio tendrán su primera sesión en el Congreso Nacional de Santiago, para iniciar con la laboriosa responsabilidad de discutir, sistematizar y escriturar una Nueva Constitución Política del Estado, en un plazo aproximado de un año, para luego someterlo al plebiscito de salida como instancia última.

El otro camino, para nada menos intrincado, es el de la carrera presidencial. Al momento, son ocho los candidatos y tres las candidatas, once en total, los que se perfilan para disputar la máxima autoridad política del país, quien tendrá la responsabilidad histórica de inaugurar y llevar a cabo la eventual nueva Carta Magna en gestión. El proceso constará de dos etapas: las primarias del 18 de julio y las finales del 21 de noviembre. La primera tendrá la participación de dos coaliciones antagónicas. El pacto “Chile Vamos” resolverá quién de los cuatro candidatos representará al sector mayoritario de la derecha, siendo Joaquín Lavín (UDI), el alcalde de Las Condes, el que corre con ventaja. Mientras que el bloque “Apruebo Dignidad” tendrá que decidir entre el diputado Gabriel Boric (CS) y Daniel Jadue, alcalde de Recoleta, miembro del Partido Comunista e incansable luchador de la causa Palestina, su patria primigenia, su lucha primera. Siendo este último, no solo el aventajado en las primarias, sino el que se perfila como el “candidato del pueblo”, llamado a triunfar en las presidenciales definitorias y comandar la mudanza societal que se despunta. Mientras que los y las candidatas restantes podrán presentarse mediante pactos o de forma independiente, pero que sin embargo no tendrán cabida en las primarias, y saltarán directamente a las de fin de año.

En definitiva, ¿Por qué hablo de la transición hacia un modelo post- neoliberal? ¿A qué va este antenombre? Es que, sin el afán de inventar retóricas impertinentes, podemos afirmar que estamos frente a una crisis social total del modelo neoliberal, y que todos los obstinados esfuerzos que se han realizado en estos últimos dos años apuntan a su inminente superación, como dé lugar. Más aún, si la estructura ceñida y los pilares sobre los cuales se erigió el modelo fueron desmoronados, o están pronto a desmoronarse, como consecuencia del prolongado terremoto social.

También, recuerdo haber leído con entusiasmo la división temporal que Fernanda Wanderley (2009) (1) escribe sobre Bolivia, indicando que dicho país se encaminó a este estadio post, entre el 2006 y el 2009, ante la llegada de un presidente indígena que efectivamente rompió con los paradigmas profundamente enquistados en la memoria histórica y colectiva de dicho país, de una tenaz raigambre colonial. Además, se impulsó en conjunto todo un proyecto político popular y reivindicativo que supo dar origen a una Nueva Constitución Política del Estado y un nuevo pacto social; cambiando la matriz productiva, garantizando los derechos sociales primordiales, reconociendo a la inconmensurable diversidad étnica preexistente al Estado-nación boliviano y promoviendo la reinvención o mantención de sus modos de vida ancestrales, con todo lo que ello significa. ¿Bolivia dejó de ser capitalista? No, en lo absoluto. ¿Elevó su calidad de vida? ¿Avanzó hacia una sociedad más justa? ¿Pudo establecer un pacto social adecuado para el Buen Vivir? Sin dudas que sí, de manera tal que los gobiernos del Movimiento Al Socialismo (MAS) encabezado por Evo Morales son, por escándalo, los mejores y más representativos de toda su historia nacional.

Creo que en Chile está sucediendo algo muy similar, pero evidentemente ajustado a su particularidad histórica. Por ello, he de considerar que este proceso transformador está ad-puertas de superar dos de los principales avatares del neoliberalismo: El fin del fantasma del anticomunismo y la sustitución de la Constitución Política del 80´, formulada y escriturada desde las entrañas de la dictadura militar. Recordemos que el neoliberalismo es el modelo de sociedad conspicuo que se instaló en Chile luego de la intervención fáctica de Estados Unidos al gobierno de la Unidad Popular encabezado por Salvador Allende, cuyo boicot eclosionó en el violento derrocamiento por parte de las fuerzas armadas, aquel oscuro 11 de septiembre de 1973. El régimen tiránico que se impuso inmediatamente después del Golpe de Estado fue la etapa de elaboración del modelo, el cual se concretó mediante las dos vicisitudes anteriormente mencionadas, es decir, la construcción de una doctrina total y absolutamente anticomunista; y la creación de una nueva institucionalidad, desde cero, con humillante benevolencia a Estados Unidos y a merced de las aspiraciones de los grupos económicos que se alinearon al mercado mundial capitalista.

Para ello, se levantó un régimen militar que atropelló sin mesura los derechos humanos en general, y con una crudeza facinerante a todo rastro de oposición o resistencia. La arquitectura autoritaria del régimen le permitió promulgar en el año 1980 la Carta Fundamental del Estado aún vigente en nuestros días, su más fiel centinela. En ella, quedaron cristalizados los principios sociales, políticos, morales y, especialmente, económicos de un nuevo paradigma del libre mercado que, a diferencia del liberalismo clásico, expandió las competencias del sector privado más allá de lo estrictamente comercial, incorporando así a pilares como la educación, salud, jubilación como meras empresas, reduciendo el rol del Estado al mínimo posible.

Pero no solo eso, como si fuera poco, sino que evidentemente estableció el marco normativo y legislativo que rigió a nuestra sociedad durante estos treinta años, sin tener la posibilidad real de realizar cambios profundos por la vía institucional. Esto fue lo que Jaime Guzmán, quien fuese su principal autor intelectual, llamó “democracia protegida”. Él lo explicó así, según sus propias palabras: “La Constitución debe procurar que si llegan a gobernar los adversarios, se vean constreñidos a seguir una acción no tan distinta a la que uno mismo anhelaría, porque – valga la metáfora – el margen de alternativas que la cancha imponga de hecho a quienes juegan en ella sea lo suficientemente reducido para ser extremadamente difícil lo contrario” (2). Esta famosa y tenebrosa declaración se valora por su sincera ambivalencia; pues en un par de líneas esclarece el problema y la solución.

He allí la apremiante necesidad de tirar este estatuto supremo al basurero de la historia y conformar uno nuevo, sin herencias ni transmutaciones. Por esta razón fundamental la constitución tiene que ser inédita, y de todas maneras lo será. Ya que por primera vez en toda la historia nacional se conformará una carta magna de manera democrática y vinculante en su construcción. En tanto, cabe destacar que esta será la primera constitución en el mundo con paridad de género asegurada (3)Sin duda, una reivindicación histórica para las mujeres de Chile que hasta hace setenta años atrás se les privaba del derecho a voto. 

Del mismo modo que emergen otros sujetos históricamente subyugados, como los y las representantes de los pueblos originarios que buscarán consagrar la tan anhelada, y hasta ahora ausente, categoría Plurinacional del Estado de Chile, ampliando el horizonte de la imprescindible interculturalidad. A estos aspectos se le suman otros que son primordiales para hablar de una genuina superación del modelo. Como la desprivatización de los derechos sociales en materia de educación, vivienda, salud, pensiones. Como también la impostergable fijación del agua como un bien nacional de uso público, asegurando que sea inapropiable para privados. Porque sí, Chile es el único país del mundo que faculta constitucionalmente el aprovechamiento del agua por parte de los privados (4). Siendo este último, además de los aspectos anteriormente mencionados, los pilares que edifican la arquitectura del modelo, como también las piedras angulares de muchos de los conflictos desencadenados.

Si bien ya se ha evidenciado un importante cambio sociocultural en Chile, acumulado durante décadas y dinamitado en el estallido social, como también a partir del remezón que generó la pandemia, exigiendo la reconversión de la vida en general, urge la necesidad de materializar los cambios que el país necesita en un nuevo proyecto político. Porque la gente se cansó de estar inserta en un modelo impuesto de facto y arbitrariamente, la cual especuló como milagroso el resultado de -el ladrillo-, el programa económico ceñido por los Chicagos Boys, cuyo único milagro fue mostrar tendenciosas cifras que hablaban de un éxito macroeconómico para una plutocracia minoritaria, mientras que la situación socioeconómica de la inmensa mayoría quedaba barrida por debajo de esta alfombra estadística. Los estudios económicos demuestran que Chile es de los países más desiguales del mundo; el 1% más rico tiene, proporcionalmente, mayor concentración de la riqueza que el 1% de países como EEUU, Inglaterra y Japón, además, sus ingresos son 40 veces mayores al ingreso per cápita del 81% de la población total (5) Esta situación se aplica perfectamente al antipoema de Nicanor Parra: Hay dos panes. Usted se come dos. Yo ninguno. Consumo promedio: dos panes por persona.

Por lo tanto, es clave que la próxima presidencia esté comandada por alguien que sepa dirigir un proyecto político genuinamente transformador, y que no intente seguir la lógica en la que tanto se basó la izquierda neoliberal: la de realizar el cambio social mínimo mientras se parchan las grietas de un modelo sistemáticamente injusto y tendencioso. No hay que ser un vidente para predecir que la instancia última de esta carrera enfrentara sin más a Joaquín Lavín y Daniel Jadue, representantes de los partidos más extremos de la derecha e izquierda respectivamente. Mientras que el primero fue fiel colaborador de la dictadura militar, admirador eterno del tirano Pinochet y de Jaime Guzmán, y la mejor carta de la derecha, el centro, y seguramente algún que otro sector de los que se hacen llamar de izquierda, para poder sostener los fragmentos del caducado modelo o para truncar el cambio de las mayorías; el segundo es, sin dudas, todo lo contrario.

Hace muchísimo tiempo que Daniel Jadue es por lejos el mejor político de Chile. Si dibujamos un breve recorrido en su carrera, debemos comenzar señalando que a los once años de edad, en tiempos de dictadura, se inició en el activismo por Palestina, integrando las filas del Frente Popular para la Liberación de Palestina, organización revolucionarias de aspiración marxista- lenninista que lucha contra la colonización de Israel en Palestina y en contra de su ideología colonial: el sionismo. 

Cuestión no menor, considerando no solo la ideología imperante en la dictadura chilena, sino que también la sabida injerencia que tuvo el sionismo en el mismo régimen como un vector del imperialismo. De hecho, Estados Unidos luego de haber promocionado y financiado el Golpe de Estado a Allende, se retira y deja en su lugar a Israel como el principal proveedor de armadas al régimen de Pinochet, como también lo hizo con gran parte de las dictaduras militares de esa naturaleza en América Latina, demostrando con creces que Israel se basa en una economía de guerra (6) No obstante, en 1993 abandona dicha organización debido al escandaloso “Acuerdo de Oslo” pactado entre las Autoridades Palestinas con la potencia ocupante, lo que me parece más que acertado.

Inmediatamente después se afilia al Partido Comunista de Chile, pavimentando un loable recorrido. Titulado de arquitecto y sociólogo por la Universidad de Chile, direccionó ambas tesis de grado para atender una temática tan vital como el derecho al acceso de una vivienda social justa y digna. Ligado a esto, se dedicó a trabajar en áreas de Gestión Local del Desarrollo y Gestión Municipal. Luego de verse frustrado su aspiración de ser alcalde en Recolecta durante dos períodos consecutivos, el 2004 y 2008, logró consagrarse en el año 2012, reafirmando su buen trabajo mediante la reelección en 2016 y 2020.

 Desde su llegada a la alcaldía impulsó un proyecto con perspectiva antineoliberal. Contra todo pronóstico de los sectores que lo tacharon tempranamente de populista, desempeñó un innovador y exitoso programa de gobierno local, ciudadano y participativo que supo torcer con la lógica del Estado-mercado. Iniciativas como la Escuela Abierta, la Óptica, Farmacias, Inmobiliarias, Librerías, Energías, Dentista, entre otras que se apellidaron como “Popular”, demostraron que era posible una nueva forma de política, con mayor participación del Estado en derechos sociales, y con menores facultades lucrativas.

La candidatura de Jadue no solo es histórica en el sentido de que sería el primer presidente militante del Partido Comunista en toda la historia de Chile, sino porque también sería el retorno del socialismo verdadero, aunque sea ideológico, luego de los intentos obstinados de la derecha golpista de sepultar las ideas y el proyecto societal que, alguna vez y en otras condiciones históricas, encarnó democráticamente Salvador Allende. 

Como él mismo dijo en su último discurso mientras las Fuerzas Aéreas bombardeaban la moneda: ¡Sigan ustedes sabiendo que, mucho más temprano que tarde, se abrirán las grandes alamedas por donde pase el hombre libre para construir una sociedad mejor! Las grandes alamedas parecieron abrirse en la revuelta, cuando la indignación de todo un país se levantó ante un gobierno indolente y criminal. Ahora nos queda traducir esa indignación en un proyecto de estado.

No puedo cerrar sin antes mencionar que particularmente para mí que, al igual que Daniel Jadue, soy Palestino de origen y dedico gran parte de mi vida en el activismo por esta causa digna, justa y humanitaria, valoro esta coyuntura también como una oportunidad infalible de que se genere un retroceso tanto del imperialismo como especialmente del sionismo en Chile. Porque hace muchísimo tiempo que las causas son globales, y es que en realidad nunca estuvieron aisladas entre sí. 

En Chile, donde alberga la mayor comunidad palestina fuera del mundo árabe, se asienta el gobernante que más insumos bélicos y policiales le compra a Israel después de Pinochet, Piñera (7). Insumos utilizados para militarizar el Wallmapu, el etnoterritorio ancestral del pueblo- nación Mapuche, y para reprimir de manera inescrupulosa al pueblo protestante ¡Al sionismo, ni un paso más!

Es por ello que en conjunto con compañeros y compañeras activistas de la causa hemos conformado un comando a nivel nacional llamado “Palestinxs x Jadue”, de manera autoconvocada y autogestionada, para impulsar y alimentar de contenido el proceso electoral. Como nosotros, han brotado más de seiscientos comandos nacionales y más de cien internacionales. 

Lo que demuestra que Daniel Jadue no solo es el candidato del Partido Comunista, ni del pacto “Apruebo Dignidad”, sino que es el candidato predilecto, elegido por la gente de a pie, común y corriente. Esperemos que este respaldo ciudadano se traduzca en las urnas, y que por fin se supere al modelo neoliberal y cada vestigio de su corporalidad, desde lo material hasta lo ideal.

Finalmente, quedaron fuera muchos temas centrales de la discusión. Como discutir a fondo el programa presidencial (8)que se estrenó hace poco, el cual será su carta de presentación para las campañas. También, en concreto, cuáles son las propuestas que impulsarán las transformaciones estructurales que tanto clama la ciudadanía. Espero, sin embargo, sobre todo para la gente que no habita en Chile, haber ayudado a comprender, aunque sea someramente, el momento histórico que estamos atravesando. Entendiendo también que estas transformaciones no son exclusivamente de un país, por lo que es clave impulsarlo en y desde la indómita América Latina. ¡Por una Latinoamérica unida, diversa, libre y soberana! ¡Venceremos!

#### \
Referencias 

1 Véase: Wanderley, F. 2009. Crecimiento, empleo y bienestar social: ¿Por qué Bolivia es tan desigual? Plural Editores. Bolivia.

2 Recuperado en: <https://www.latercera.com/reconstitucion/noticia/la-influencia-de-jaime-guzman-en-la-constitucion-del-80-mitos-y-verdades/3DEDI7WOAFAULBTLN3W22MDQAE/>

3 Recuperado en: <https://www.eleconomistaamerica.cl/politica-eAm-cl/noticias/10851118/10/20/Chile-redactara-la-primera-Constitucion-del-mundo-en-paridad-de-genero.html>

4 Recuperado en: <https://www.ing.uc.cl/noticias/estudio-a-92-constituciones-identifica-a-chile-como-el-unico-pais-con-expresa-propiedad-privada-de-derechos-de-agua/>

5 Recuperado en: <https://www.ciperchile.cl/2013/03/22/chile-el-mejor-pais-del-mundo-si-usted-es-un-super-rico/>

6  Véase el libro publicado por el Movimiento BDS de América Latina titulado “El militarismo israelí en América Latina”. Lo puede encontrar en: <https://bdscolombia.org/wp-content/uploads/2018/11/El-militarismo-israel-en-Am%C3%A9rica-Latina.pdf>

7 Recuperado en: <https://interferencia.cl/articulos/pinera-es-el-gobernante-que-mas-insumos-belicos-y-policiales-ha-comprado-israel-desde>

8 Lo puede encontrar en su página web: <https://www.danieljaduepresidente.cl/wp-content/uploads/2021/06/Programa-DJ.pdf>







<!--EndFragment-->



<!--EndFragment-->

<!--EndFragment-->



<!--EndFragment-->



<!--EndFragment-->



<!--EndFragment-->