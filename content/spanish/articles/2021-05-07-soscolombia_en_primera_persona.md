---
title: SosColombia en primera persona
subtitle: "A Propósito del Paro Nacional en Colombia "
cover: /images/uploads/soscolombia2.jpeg
caption: Imagen Publico.es
date: 2021-05-07T22:02:57.297Z
authors:
  - Patricia Lepratti
tags:
  - SosColombia
  - Derechos Humanos
  - ParoNacional
  - Colombia
  - DerechosHumanos
  - Mujeres
  - Resistencia
categories:
  - Cuerpo y Territorio
comments: true
sticky: true
---
*“Mientras hablo con ustedes estoy esperando que mi novio llegue, esperando con todo mi corazón para que no le pase nada”*

Este miércoles 5 de mayo, durante la transmisión en vivo del foro ***Los feminismos latinoamericanos: una mirada desde (nos) otras,** organizado por la Revista Cuerpo y Territorio* (ver al final), el relato de una de las panelistas invitadas sobre la actual situación en las calles y hogares de **Colombia nos conmovió.** Entró pateando la puerta de nuestras casas a cientos o miles de kilómetros, nos llenó los ojos de lágrimas y nos dejó sin palabras.

De acuerdo con el programa, esperábamos que **Juliana Rincón (filósofa y socióloga)** nos hablara de su experiencia como activista **ecofeminista en el Putumayo colombiano**. Sin embargo, visiblemente angustiada, Juliana nos pidió disculpas. “Para mi es realmente difícil acompañarlas en este espacio, porque mientras hablo con ustedes (…) estoy esperando que mi novio llegue, esperando con todo mi corazón para que no le pase nada”.

Desde una ciudad militarizada y con los caceroleos como sonido de fondo, Juliana nos cuenta que ya son **30 las personas asesinadas por las fuerzas policiales y militares** durante los últimos días mientras que el número de desaparecidos aumenta cada día (ochenta y siete según The New York Times del 6 de mayo 2021).

“Necesitamos todos los ojos en Colombia, necesitamos que no se olviden de ningún rostro y ningún nombre. Porque si no tuviéramos redes sociales en este momento, no tendríamos ningún registro de lo que está pasando”.

El dos de mayo, mientras Cali vivía su quinta jornada de movilizaciones, **Nicolás Guerrero** era asesinado. El momento en el que el **joven grafitero de 21 años** cae herido de muerte por un arma de fuego de la policía de Cali era transmitido en vivo desde la cuenta de Instagram de otro de los manifestantes, el DJ Juan de León.

**“Si no tuviéramos esas transmisiones en vivo no sabríamos lo que está pasando”**, doce Juliana, quien señala a los medios tradicionales de Colombia como a un actor más en la larga historia de violencia que desangra a ese país latinoamericano.

“No tenemos más protección que nuestras redes de afecto (…) **Llega la noche y llega lo peligroso**. Tenemos videos de policías entrando a las casas de la gente (…) Aprovecho este espacio para pedirle a policías y militares que obedecen órdenes, que obedezcan al fuero de su propia conciencia”.

“**En Colombia no hay pena de muerte, pero nos la están aplicando** (…) ¡¿Qué narrativa es esa en la que un pelado por pegarle una patada a un policía para que sus amigos puedan escapar merece ser asesinado?!” se pregunta Juliana haciendo alusión al asesinato de otro joven en Cali, también registrado y difundido en redes sociales.

“Esta es una biopolítica del miedo. No sabemos qué va a pasar. Estamos todos asustados (…) Por eso en este momento yo les pido su apoyo y solidaridad. No sabemos cuando nos van a cortar internet, no sabemos en qué momento nos van a venir a buscar a nuestra casa…**A los colombianos nos acostumbraron a la muerte,** nos acostumbraron a que un muerto es una cifra más”

“**Para tener paz necesitamos democracia y participación política**. No podemos participar políticamente si no tenemos una vida libre de violencias. Es este momento la situación es de sobrevivencia. Todos estamos buscando sobrevivir…Y necesitamos sobrevivir con ayuda de ustedes”.

Quienes no vivimos en Colombia seguramente hemos leído, visto o escuchado noticias sobre lo que allí sucede. Quizás nos hemos solidarizado con la causa de los manifestantes y hemos expresado en nuestras redes sociales el repudio a la represión policial, la criminalización de la protesta y la militarización de las ciudades colombianas. Pero este resumen del relato de Juliana busca ir un poco más allá, **busca conectarnos con las emociones**, la ansiedad y el miedo que pueden vivirse en momentos como los que vive actualmente Colombia.

“Estamos con ustedes”, del mismo modo como estamos con nuestros hermanos y nuestras hermanas… con el corazón.

#### Foro ***Los feminismos latinoamericanos: una mirada desde (nos) otras***

https://www.youtube.com/watch?v=ISNAt2LBGDk