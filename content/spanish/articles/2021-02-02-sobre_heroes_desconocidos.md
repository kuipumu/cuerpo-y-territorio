---
title: "Sobre héroes desconocidos "
subtitle: " "
cover: /images/uploads/tests-antigenos-cribados-sanidad_1cbe62a8_990x586.jpg
date: 2021-02-02T20:07:44.018Z
authors:
  - Iris Mendizábal
tags:
  - Coronavirus
  - PCR
  - Historia
  - Ciencia
  - Química
  - Laboratorios
categories:
  - Cuerpo
comments: true
sticky: true
---
<!--StartFragment-->

La PCR (Reacción en cadena de la polimerasa) es una técnica de laboratorio que, **valiéndose del “copyright” de la naturaleza** (como la mayoría de las genialidades que se le ocurren a ese pequeño porcentaje de humanos creativos), permite descubrir si tenemos el material genético del conocidísimo “Coronavirus” intercalado en el adn de nuestras células, de esas que forman parte de nuestro sistema respiratorio.

Probablemente haya sido la técnica más utilizada en los laboratorios del mundo durante todo el año pasado, y lo será por un buen tiempo más. Vale aclarar que además de ser empleada para detectar casos positivos de Covid-19 (el hecho que la llevó al estrellato), se utiliza también para estudios de criminalística forense, investigación en alimentos y en ecología, y para otros diagnósticos médicos. **El “Proyecto Genoma Humano”, un enorme esfuerzo de la ciencia, allá por el año 2000, para conocer completamente el genoma de la especie humana también se lo debemos a la PCR.** Tan enorme es su poder que el científico Stephen Scharf, la describe de la siguiente forma: *“Lo verdaderamente asombroso de la PCR es que no fue inventada para resolver ningún problema; una vez que existió, los problemas para los cuales puede ser aplicada, comenzaron a emerger”.*

**Pero lejos de pretender aburrir con detalles técnicos sobre su esencia,** me pareció que podría interesarles la divertida y extraña historia que se esconde detrás de su invención. La misma involucra básicamente a dos seres vivos: Uno de ellos es una bacteria de aguas termales que disfruta de vivir en ambientes con temperaturas superiores a los 100°C. A partir de esta bacteria se purificó una enzima que es fundamental para la ejecución de la técnica. El otro protagonista de esta historia es un hombre, uno que pocos conocen, alguien que nos ofreció una solución tecnológica para mejorar la vida de millones de personas: el señor **Kary Mullis.**

Segundo entre 4 hermanos, Kary Mullis nació el 28 de diciembre de 1944 en el estado de Carolina del Norte, Estados Unidos. Su padre era marinero y su madre agente del Estado. 

Desde joven demostró un especial interés por la ciencia y por las innovaciones tecnológicas y en 1966 obtuvo su graduación en Química, en el “Georgia Institute of Technology in Atlanta”. Cursó luego estudios de posgrado obteniendo su PhD en Bioquímica. La historia cuenta que era un estudiante indisciplinado y desestructurado, y que no le gustaba seguir reglas. Haciendo gala de sus “excentricidades”, después de obtener su máximo título académico, **Mullis decidió alejarse de la ciencia y dedicarse a escribir ficción.** Pero, por suerte para todos nosotros, al cabo de un tiempo consideró que no tenía talento para el arte y volvió a los datos duros.

El momento creativo se dio mientras trabajaba para una empresa de biotecnología conocida como Cetus Corporation, en California, a mediado de la década del 80. Las versiones aceptadas y publicadas cuentan que **la idea de la PCR se le ocurrió mientras manejaba su auto hacia los bosques californianos un viernes por la tarde.** Sin embargo, entre pasillos de universidades y mesadas de laboratorio se cuenta otra historia, sobre la que no existen demasiadas certezas: aparentemente, la idea que le dio al mundo la mejor técnica biotecnológica del momento, surgió mientras Mullis pedaleaba una bicicleta bajo los efectos del ácido lisérgico.

Con estupefacientes o sin ellos al científico se le ocurrió, circulando en algún rodado por las rutas de California, un invento que cambió la biología molecular, la medicina y el estudio entero de la naturaleza en el mundo. La sorpresa para el resto de la comunidad biotecnológica fue tal, que **en poco tiempo Laboratorios Roche inició un litigio por la patente de la idea,** argumentando que la base del descubrimiento le pertenecía a un científico japonés de los años 60´. El laboratorio obtuvo finalmente la patente de la PCR, invirtió millones de dólares en su mejora y puesta a punto, que por supuesto luego recuperó con miles de millones de dólares de ganancias.

Pero, a pesar de todo, el mérito del señor Mullis fue reconocido **cuando finalmente resultó ganador del premio Nobel de química en el año 1993, justamente por haber inventado la valiosa técnica sobre la que trata esta nota.**

Hoy, después de un año entero de escuchar a cientos de personas hablando de pandemias, infecciones y de cosas de las que poco saben; de ver a otros tantos opinando sobre estadísticas, curvas, y bla, bla, bla… Después de tantos grandes nombres sonando en nuestras bocas, porque dijeron tal o cual cosa sobre el tema que a todos nos importa. Hoy, después de haber oído a políticos asegurando haber ganado batallas que se les iban de las manos, o de intelectuales intelectualizando la naturaleza, y después de la codiciosa carrera por las vacunas, yo los invito a reflexionar sobre nuestros héroes desconocidos, **esos que no aparecen en ningún lado, esos que nadie nombra.** Un hombre con delirios de escritor y una bacteria de aguas calientes. Ahí está nuestro progreso tecnológico. El verdadero, sin relatos, sin discursos. Funcionando a toda máquina adentro de los laboratorios, prestando servicio a la sociedad.

**La ciencia que nos salva la vida.**

Ni más. Ni menos.

**Fuentes:**

1) Shampo M, Kyle R. 2002. Kary B. Mullis, Noble Laueate for Procedure to Replicate DNA. Mayo Foundation for Medical Education and Research.

2) Bartlett J, Stirling D. A short story of the Polymerase Chain Reaction. Methods in Molecular Biology. Vol 226. PCR Protocols, Second Edition. Totowa, NJ.