---
title: "Comentarios al Manual para el armado de sectas, neorreligiones y otros
  ensalmos del alma "
subtitle: " "
cover: /images/uploads/dibujo_pa_el_cadaver.jpg
caption: "Ilustración: El Cayapo"
date: 2020-12-01T23:57:07.440Z
authors:
  - Roberto Salazar
tags:
  - Revolución
  - Ideas
  - Ideales
  - Lider
  - Cuerpo
  - Secta
  - Movimientos
  - Ideología
categories:
  - Cuerpo
comments: true
sticky: true
---
<!--StartFragment-->

> *`Me ha pasado mi amigo Doppelgänger, no sin cierto dejo de burla en sus modos, este escrito que aquí reproduzco. No sé si más bien lo inventó él mismo y aun así me lo dio sin revelármelo, puesto que no tiene nombre de autor y fecha, apenas un título rimbombante y conociendo yo en él su tendencia a la ficción y él en mi la tendencia a atormentarme con estos temas, seria de lo más esperado. De todas formas, no pude dejar de hacer unas notas propias al margen del texto que, por no sin cierta vanidad, comparto.`*

**Introducción**

Por supuesto que usted podrá notar que la palabra secta no es muy agradable y además puede que usted no busque salvar almas. Quizás porque sus ideales o su revolución no son exactamente una religión. Da igual lo que piense. A continuación, le expondré principios generales para poder cambiar al mundo a través de las ideas. No varia demasiado si usted tiene una convicción política, económica, social o religiosa. Da igual si usted quiere cambiar el alma, una nación, la verdad, lograr la felicidad. También puede consultar a otras fuentes, puesto que las hay. Cuídese de la filosofía y de la ciencia. Guarde este breve manual, difúndalo o destrúyalo. Suerte.

> `El escritor nos habla de frente, sin rodeos. Casi con desdén. Nos advierte de un título provocativo, quizás para llamar nuestra atención. Y nos dice claramente que las vías para cambiar al mundo son las mismas. Lo que cambian son las metas. Nos implica en cierta complicidad luego. Ya es claramente un sectario que juega con nosotros.`

**Sobre los principios**

Lo primero con lo que usted debe contar es con una convicción de que está para grandes cosas. Que aquella visión que tiene de cambiar al mundo es correcta y digna de ser esparcida por toda la humanidad. La vacilación no es propia de la fundación de un gran movimiento. La duda es un lujo que no puede permitirse.

> `En efecto. Las sectas inician con un amor propio de parte del líder. De su fuerza de voluntad, tenacidad y auto encumbramiento. Ya vemos porque hay que cuidarse de la ciencia o filosofía. Las sectas parten de un principio de certeza y no de cuestionarse la realidad o las ideas propias. El germen del líder sectario.`

**Condiciones del líder**

El carisma natural o la presencia magnética de su persona resultará crucial. Por supuesto que es un diamante en bruto a pulir. No cuestione las capacidades que tiene. Cualquier pueda ser atractivo para la gente en algún grado. Revise su arsenal. Intelecto, presencia, circunstancias. Ármese una historia, créese una narrativa. Construya un personaje. No tiene que ser usted mismo, tiene que ser el que se necesite para lo que se necesite. Distribuya sus falencias. Si nadie lo quiere ver, que se concentren en lo que dice. Si se enreda hablando, escriba. Sea cómico o trágico o dramático. Intelectual o emocional. Solo tenga éxito.

> *`Esto lo conocemos claramente en la política. La demagogia propia de ciertos líderes de masas. Pensemos en Hitler y su capacidad como orador pese a no ser un líder particularmente encantador de ver. Es la idea del hombre-creado-a-si-mismo. El héroe que pasó por infortunios. Y nos advierten que no importa que sea reales o no las historias del líder, lo importante es que sea capaz de hacerlas pasar por reales.`*

**Discípulos**

Necesitará apoyo. Dinero, medios, recursos, personas. Fundamental estos últimos. Rodéese de aquellos que a los cuales les genere fascinación. Esos son la base. Luego seduzca a aquellos a los que usted solo les agrada. Con los indecisos, rételos. Serán suyos. Y a los que los puedan adversar, derrótelos. Humíllelos de ser posible. Le granjeará respeto entre el movimiento que, sin darse cuenta, ya irá tomando forma.

> `Acá está comprimiendo en unas líneas lo que podemos ver en la moderna serie The Vow y la creación de una secta no religiosa en donde el líder ejerce una influencia total en sus discípulos. El líder de esa secta, Keith Raniere, resultó finalmente condenado por trata de mujeres. Pero en un inicio Raniere no es más que eso: fascinación, seducción, furia, humillación, respeto.`

**Modos de dirección**

Siendo el líder, hágase indispensable. Delegue y distribuya roles, sin dejar de tener la última palabra. Resuelva dificultades por vía deliberativa pero que no haya dudas en donde reside el poder. Congratule y apruebe el buen trabajo y aleatoriamente sea ferozmente crítico. La vitalidad debe ser el centro de la personalidad de un líder. Y la vida es cambio, es sorpresa, es incertidumbre. Deje la puerta abierta para hacerle respuestas novedosas a sus seguidores. No cree un estilo fijo. Cambie de ánimo. Retoque las ideas, siempre inacabadas, siempre en evolución. Sea el interpretador último del movimiento. Alfa y omega.

> *`Dirían los psicólogos, esto es hacer reforzamiento aleatorio. Es decir, dar premios a una conducta deseada que se ejecuta solo a veces y no siempre, genera más adicción. Parecido a la lógica de los algoritmos de Candy Crush. El líder no cede el poder, el líder no es predecible, el líder te premia o te castiga. El líder lleno de vitalidad, hombre de acción, hombre de decisiones. Nada de reflexión. Y siempre dice que eso por lo que luchan está por construir. Sea el Reino de los Cielos, La Revolución, La Democracia, La Felicidad.`*

**Organización del movimiento**

Organice a los primeros seguidores en función a sus capacidades. Si le falta un talento, procure encontrarlo. Otorgue un nombre al movimiento y establezca un espacio físico. Cree ritos y costumbres. Otorgue distintivos. Genere jerarquías equilibradamente administradas. Disuelva cualquier grupo natural y excluyente que se vaya formando. Pida compromiso casi exclusivo con el movimiento. Haga dudarlos constantemente del esfuerzo que están haciendo. Tenga unos favoritos, elíjalos para el círculo más íntimo. El resto querrá pertenecer. Expulse alguna vez a algún favorito, así nadie se cree intocable.

> *`Acá se refleja maneras de organización conocidas por fanáticos famosos como el mencionado Raniere, Jim Jones, Joseph Smith. La inestabilidad del líder, el poder absoluto que detenta, el encanto, su capacidad para generar identificaciones y hacer espacios a rituales nuevos. Es cierto que no lo vemos solo en sectas relativamente pequeñas, sino en movimientos aún más amplios como la Ciencia Cristiana de Mary Baker-Eddy, el fascismo italiano y su devenir más moderno, los populismos`*

**De las resistencias iniciales**

Si ya usted pertenece a algún movimiento, probablemente le dirán que está haciendo una nueva religión. Si no pertenece a ninguno, le considerarán una secta. En ambos casos, hereje. Entienda que le odiarán y difamarán. Victimícese. Ocupe el lugar de David contra Goliat. Remarque contradicciones de los poderosos. Muéstrese humilde, idealista, inofensivo. Transmita que su movimiento es una reacción natural a la esclerosis de las viejas castas. Busque a los desencantados. No les tema a los parias. Ningún árbol se tala por la copa, todos por la base.

> *`La distinción entre secta y religión no es menor. Martin Lutero y la creación del protestantismo pronto derivó en casi una religión nueva. Lutero era un sacerdote católico. No así con casos como los de Ron Hubbard, padre de la Cienciologia, que pasa de ser un escritor de ciencia ficción hasta la formación de un culto y su litigio por ser reconocido como religión para evadir impuestos. En Lutero y en Hubbard vemos que se ubican en este lugar del oprimido por los sectores dominantes y denuncian el socavamiento de la tradición por su propia decadencia. Habría que preguntarse cuál movimiento no surge de este modo. De Jesucristo a Lenin.`*

**Estrategia de lucha**

No importa la idea que tenga para cambiar la historia: debe decir que existió un pasado idílico y que un grupo, aquel que lo ataca a usted, fue precisamente el que lo pervirtió. Un movimiento demasiado vanguardista es difícil de entender. Rescate cierto aspecto de la tradición, ese que ha sido más olvidado por ser obvio y combínelo con lo más logrado de su pensamiento. Un viejo conocido con nuevos ropajes es a lo que usted apostará. Actualice con cierta frecuencia algún punto importante de su ideario. Preséntelo como una mejora. Compárelo con lo decrépito que resulta lo viejo. No ataque demasiado frontal el antiguo dogma, puesto que ya habrá logrado la animadversión de muchos. El resto del trabajo lo harán sus enemigos.

> *`La antropología sustenta en parte esta afirmación. Los mitos de las sociedades primitivas parten de una estructura básica y es la pérdida de un lugar edénico, del Paraíso, por alguna traición y el líder y padre de cada cultura es precisamente el que logra recomponer algo de esto. Sin ir tan atrás, la moderna dialéctica hegeliana nos habla de esta lucha. Conservar lo antiguo en lo nuevo, el malabarismo barroco de ciertas sectas y movimientos, sin duda.`*

**Difusión y crecimiento**

Sea creativo en cómo generar adeptos. El movimiento debe ser inicialmente proselitista. Debe haber crecimiento para motivar a los recién llegados y para desacomodar a los adeptos más antiguos. Utilice canales de transmisión de sus ideas novedosos, sobre todo aquellos inexplorados por sus adversarios. No excluya a medios pequeños o remotos. No desdeñe el valor de lo más moderno para mantener la exposición de su grupo. Consiga figuras de otros ámbitos que puedan servir de representantes. Aproveche una coyuntura desagradable para colarse en el imaginario popular como movimiento fresco, atrevido y valiente. No haga nada ilegal, le llamaran mafioso. Tenga paciencia y tenga prisa.

> *`La lógica de crecimiento del movimiento es similar a la organización de los partidos políticos modernos. Desde la posguerra, se viene perfeccionando la capacidad de lograr difusión de idearios políticos de manera más profesional, segmentada. Marketing electoral. Redes sociales, fake news y trolleos al día de hoy. Un gran ejemplo de esto está en Brasil. El movimiento de Edir Macedo, líder de un movimiento religioso muy popular que ha crecido en parte a la capacidad de controlar grandes medios de comunicación y trabajar con comunidades que la Iglesia Católica había olvidado.`*

**Control absoluto**

Cuide la infiltración de enemigos a la causa. Tendrán diferentes máscaras y voces. Desconfíe de aquellos demasiado cerebrales, confiados e invulnerables. Apunte a los más débiles y desamparados como el epicentro de su movimiento. Mézclelos con algunos miembros exitosos y carismáticos. No promueva ninguna habilidad para lograr mayor poder en la organización fuera de la constancia, emocionalidad, vulnerabilidad y lealtad. Con los de mayor responsabilidad en sus tareas, redoble el control. Demándeles compromiso absoluto. Haga porosa la frontera entre vida privada y vida en el movimiento. Tenga accesibilidad absoluta a los de mayor jerarquía. No tolere rechazos. No tolere deserciones.

> *`Alain Denault habla de la mediocracia como el mando de los mediocres. En donde quizás gobierne el gran líder, pero debajo de él solo estén los obedientes. Nadie con demasiado talento. Lo vemos en la película de Paul Thomas Anderson, The Master. Esa consagración absoluta a la idea o al movimiento la hemos visto en causas políticas, sociales y religiosas. Y como no asociarlas al epicentro de uno de los grandes problemas de lo que va de siglo: el fundamentalismo y el terrorismo. La paranoia creciente en los movimientos sectarios es, por supuesto, un lugar común. De manual.`*

**Poder difuso**

Cuando llegue a la masa crítica para hacer el movimiento indetenible, haga la organización más vertical. Colóquese en la cima y atraviese todos los niveles, pero que los miembros pasen a entenderse según ciertos rangos. Inicie un lento proceso de atenuación de su presencia. Cuide el desgaste de su imagen. Conviértase en un oráculo para los miembros de mayor jerarquía. Sea ambiguo. Deje que otros desarrollen sus propias iniciativas sin aprobar o desaprobar. Hágase inalcanzable, un tanto remoto a la prisa de las circunstancias. Refuerce los rituales de su movimiento, permítase grandes símbolos y celebraciones para los seguidores. Elija muy bien que batallas dar y procure evitar grandes derroches de energía.

> *`La serie Wild Wild Country refleja de manera extraordinaria esto. La formación de un movimiento sectario por parte del gurú hindú Bagwhan (luego conocido como Osho), y como luego de un crecimiento absolutamente acelerado, el líder va desapareciendo del centro de la escena y termina siendo una figura oracular. Un oráculo es aquel que da una respuesta y que el que la recibe la debe interpretar puesto que no es inequívoca. La santificación del líder es necesaria y se requiere un cierto alejamiento. Lo oculto es parte de la verdad, según leemos en Heidegger. Secta madura, intuimos, termina en ser justamente eso que buscaba combatir. Oscuridad y tradición.`*

**Estabilización**

Repliegue el movimiento a su base. Evite expansiones innecesarias. Mantenga a los miembros más probos en los mismos lugares. No haga permutaciones superfluas. Tema a los riesgos y aventuras. No cambie demasiado las formas que estaban desde un inicio. Revitalice aquello desgastado sin que se note que es un refrescamiento. Prefiera la estabilidad al progreso, la reforma a la revolución, el remedio a la cirugía. En caso de crisis, sea austero en las medidas. Se puede dar el lujo de aguardar a que la tormenta pase. Puede permitirse dudas. Cultive el no hacer nada.

> *`Volver a las bases, a las raíces, revitalizar lo originario, cuidado con los aspavientos emocionales. La secta acá es no demasiado distinta a la monarquía inglesa, a la Iglesia Católica, a los partidos políticos tradicionales. Como en boca de la Reina Isabel de Olivia Coleman en la serie The Crown “A veces es mejor no hacer nada”. La estabilidad en lugar del progreso fue la esclerosis que atacó a la Revolución Cubana, el repliegue a sus bases fue la derrota del partido demócrata de EEUU a manos del populista Trump. Ya no hablamos entonces de la secta como aquel movimiento pequeño y rompedor si no de instituciones más longevas. De sectas a sectarismos.`*

**Sobre traidores**

Prepárese para enfrentar desertores, apóstatas y cazadores de fortunas. Ignóreles y combátalos con tozudez a partes iguales. Con aquellos que estuvieron en el movimiento, ejerza toda su influencia emocional para acorralarles. Tendrán sentimientos encontrados. Difunda viejas falencias de ellos que conoce, por medio de terceros. Con los que no conoce tanto, táchelos de ambiciosos y resentidos. Con los forasteros a la organización, haga énfasis en su enorme sed de poder. Infíltrelos hasta cierto punto, genere divisiones entre el líder y sus miembros más antiguos. Y fundamentalmente insista en que lo importante es su movimiento y no tiene si no energías para dedicarse a lo que se dedica. Sea discreto.

> *`Lo de lo que nos habla en el fondo este Manual es de las más profundas dificultades y contradicciones que vemos en construir ciertos movimientos que al menos podríamos pensar que buscan el bien. Aunque el camino al infierno está hecho de buenas intenciones, dice el proverbio. La trituradora de hombres que es cualquier organización, las pasiones humanas, los celos, las envidias, la acumulación de poder. El Príncipe de Maquiavelo, El arte de la guerra de Sun Tzu. Como se adquiere y se mantiene el poder, de forma despiadada. Rechazamos claramente sectas religiosas o criminales como las de Aum en Japón o La Familia de Charles Manson, los movimientos ideológicos totalitarios como el fascismo o el comunismo, el fundamentalismo religioso de la Inquisición o de los ciertos califatos. Pero el sectarismo es posible en todos los esfuerzos humanos, desde la Teología de la liberación, el feminismo, el neoliberalismo y la democracia.`*

**Rumbo fijo**

A partir de acá, puede crear su propio manual. Diríjase a donde se quiera dirigir. Y nunca deje de tomarse las cosas en serio, puesto que lo frágil empieza en lo poco convencido que esté en insistir. Siempre insista.

> *`Dice Javier Cercas que los verdaderos líderes políticos nunca dejan de aspirar de volver al poder. Lamentablemente esa terquedad la vemos en algunos fanáticos en sus ganas de imponer sus ideas y crear sus organizaciones. Están convencidos. El antídoto que creo que podemos tener contra semejante voluntad de poder no es si no, precisamente, no tomarnos las cosas demasiado en serio. La risa, el desenfado es lo que precisamente hace tambalear al poder e impide que crezca o se consolide. Como descubre Guillermo de Baskerville en el Nombre de la Rosa de Umberto Eco. A los totalitarios de hoy, seguramente no lo podemos enfrentar con risa. Pero aquellos que se están formando hoy y que nos entusiasman, que nos emocionan porque son joviales y distintos, a esos, lo siento, pero siempre con una risita en la boca. Quizás algún día escriba el Manual contra el sectarismo. Empezaré con algo como “no te tomes nada demasiado en serio, ni siquiera esta manual”.`*