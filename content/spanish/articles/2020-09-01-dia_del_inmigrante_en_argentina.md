---
title: Día del Inmigrante en Argentina
cover: /images/uploads/inmigrantes-1.jpg
date: 2020-09-01T21:54:53.087Z
authors:
  - Patricia Lepratti
tags:
  - Migración
  - Continente
  - Países
  - Argentina
  - Latinoamérica
  - Xenofobía
  - DíaDelInmigrante
  - Inmigrante
  - Cultura
  - Multiculturalidad
categories:
  - Diversidad
comments: true
sticky: true
---
<!--StartFragment-->

El 4 de septiembre Argentina celebra el Día del Inmigrante. Esta fecha fue decretada en 1949 por el entonces presidente Juan Domingo Perón y recuerda una disposición del gobierno revolucionario de 1812, según la cual se invitaba a individuos de todas las naciones a fijar su domicilio en el territorio de las recientemente liberadas Provincias Unidas del Sur.

Tanto la resolución de 1812, como la de 1949, tienen lugar en momentos históricos en los cuales las autoridades que gobernaban el actual territorio argentino, buscaban atraer determinados flujos de población para trabajar la tierra y participar en el desarrollo de la industria local.

Sin embargo, en otras coyunturas históricas, las normativas en torno a la inmigración, presentan un carácter más restrictivo y la llegada de inmigrantes, más que un motivo de celebración, ha significado un elemento de preocupación ante el cual se despliegan una serie de discursos y prácticas que buscan limitar su ingreso o facilitar su expulsión.

Un rápido repaso por las diferentes políticas migratorias llevadas adelante por el Estado argentino desde el siglo XIX hasta la fecha, nos muestra los cambios sufridos por estas políticas públicas y por la concepción de “inmigrante” que en ellas se encuentra. En este sentido, el propósito de esta nota es, precisamente, subrayar el carácter histórico y, por lo tanto, cambiante de lo que socialmente se construye como “inmigración” e “inmigrante”.

No obstante, antes de cualquier análisis sobre la categoría “inmigrante”, es necesario hacer referencia a una construcción social previa: aquella que diferencia entre nacionales y no nacionales (extranjerxs). La definición de quién es considerado nacional y quién es extranjerx está asociada a la constitución de Estados-nacionales y a una forma de organización política que implica la integración de tres elementos: un gobierno, un territorio y una población.

Esta asociación entre territorio, gobierno y población resulta, también, de una construcción social que en nuestra región tiene lugar desde hace poco más de doscientos años.

Así, desde finales del siglo XIX y hasta la Primera Guerra Mundial, Argentina, como otras jóvenes naciones latinoamericanas, promovió la inmigración con objetivos colonizadores y civilizatorios. Es decir, la política migratoria argentina de ese momento, representaba una estrategia para transformar la matriz demográfica y productiva heredada de la colonia. El “inmigrante” deseado, entonces, era el varón trabajador o campesino de origen europeo. Desde entonces, los flujos migratorios y las políticas públicas en torno al tema han variado mucho.

De acuerdo con el último censo realizado en 2010, en Argentina residen dos millones de extranjerxs, el 80% provenientes de países latinoamericanos. Más allá de que lxs primerxs inmigrantes trasatlánticos que llegaron a la Argentina no estuvieron exentxs de discriminación y desigualdad en la inclusión laboral con respecto a la población nativa, en la actualidad, asistimos a discursos que a nivel global consideran a la inmigración y a lxs inmigrantes un problema a controlar y limitar.

En este sentido y, a pesar de que la última Ley de Migraciones promulgada en 2004 (N° 25871), busca asegurar derechos básicos a todxs lxs residentes extranjerxs en Argentina, recientemente fuimos testigos de la aplicación de prácticas inéditas de criminalización de la inmigración. Así, en agosto 2016 se anunció un convenio entre la Dirección Nacional de Migraciones, el Ministerio de Seguridad Nacional y el gobierno porteño para crear el primer centro de detención de migrantes del país y en enero de 2017, el presidente Mauricio Macri firmó el Decreto Nacional de Urgencia 70/2017, según el cual se buscaba facilitar la expulsión de extranjerxs en una clara asociación entre migración y delito.

Este cambio de paradigma tuvo lugar en simultáneo con políticas neoliberales de ajuste fiscal y reducción del rol social del Estado. En este contexto, lxs inmigrantes eran culpabilizados por la falta de empleo, la escasez y el mal funcionamiento de servicios públicos, y por el aumento de la delincuencia. Demasiadas responsabilidades para una minoría que apenas supera el 4% de la población residente en el territorio argentino.

Es necesario señalar que la población principalmente afectada por tales discursos y prácticas xenófobos son lxs migrantes nacidos en países como Bolivia, Perú, y aquellos de origen africano (como fue relatado por nuestra compañera Florencia Cencig en su nota sobre migrantes senegaleses). Otrxs extranjerxs, en cambio, sí son asimilados al discurso de agentes de desarrollo (pues cuentan con formación universitaria, están asociados a determinados niveles de ingreso y consumo, etc.); en un claro ejemplo de las múltiples dimensiones que entran en juego en la construcción social de ese “otro”-“inmigrante”, que no solo es extranjerx, sino al que también se le asigna una clase y una “raza” diferente a lo que se considera identificatorio de lo “nacional”.

Resulta siempre sorprendente que sociedades latinoamericanas, como la argentina, tan marcadas por la emigración a partir de las últimas décadas del siglo XX, resuenen al eco de tales discursos y prácticas nacionalistas. Las historias nacionales y hasta personales de argentinxs, bolivianxs, colombianxs, uruguayxs, españolxs, italinanxs etc., en fin, de la humanidad, se han ido constituyendo a partir de múltiples desplazamientos en busca de nuevas y mejores oportunidades de vida, que pocas veces tienen que ver con el objetivo de delinquir o de usurpar.

Espero que esta breve reflexión en torno al Día del Inmigrante en Argentina, cumpla su objetivo de subrayar el carácter social e histórico de la categoría “inmigrante”, que como las categorías “nacional”, “argentino” o “ciudadano” no son naturales ni eternas, sino pasibles de cambio. Y ojalá, los futuros cambios fortalezcan la idea de que migrar es un derecho, porque migrar es humano.

<!--EndFragment-->