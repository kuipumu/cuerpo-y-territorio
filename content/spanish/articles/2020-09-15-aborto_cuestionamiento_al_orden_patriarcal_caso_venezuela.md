---
title: "Aborto, cuestionamiento al orden patriarcal: caso Venezuela"
subtitle: " "
cover: /images/uploads/generoconclase.org.ve_-_foto_de_aybori_oropeza.jpg
date: 2020-09-15T22:57:59.464Z
authors:
  - Niyireé S Baptista S
tags:
  - Aborto
  - Género
  - Feminismo
  - Mujeres
  - Patriarcado
  - Derecho
  - Venezuela
  - Latinoamerica
  - Testimonio
  - AbortoLegal
  - AbortoEnVenezuela
  - AbortoClandestino
  - 28S
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

> *“Mi mano sostenía la suya mientras Selin masajeaba suavemente sus caderas. Le untamos aceite en los pies para calentarlos un poco, estaban fríos y ausentes de color, al igual que su cara, contenida en una expresión de dolor perenne, como cuando una se desbarata por dentro, de a pedazos.*
>
> *La fiebre llegaba a 39 grados y ella, toda, era un manojo de estremecimientos involuntarios a los que la había sometido la ilegalidad de aquel proceso. El miedo y la culpa corrían de su entrepierna y se mezclaban con la sangre desprendida de su útero. Soltaba quejidos fuertes y endurecía el entrecejo para paliar con carantoñas aquel desgarramiento. Quizá pensaba que en esa circunstancia lo mejor era ser fuerte y aguantar.*
>
> *Tuve miedo cuando le agarre el cabello y vi su carita desfallecer. “No quiero tener un hijo”, me había dicho días antes, a lo que yo respondí: “Cuenta conmigo”.* 

**Algunas personas creen que las mujeres pedimos aborto legal porque vamos a ir corriendo a embarazarnos y después decir “listo, voy a abortar”**. Ninguna mujer quiere pasar por eso. Un aborto es una situación tortuosa desde todo punto de vista, en el que nos sentimos tan vulnerables y estamos tan desprotegidas, porque así nos lo han impuesto, por un maldito empeño de poseer el control de nuestros cuerpos y obligarnos a parir hijos que no queremos, porque si no, nos matan y nos violan. Da lo mismo, siempre lo hacen. Después de 12 horas ella está mejor. La abrazamos y le dijimos: *“¡Estamos contigo!, ¡somos tribu!, ¡somos mujeres! Esta fue tu decisión y eso es lo que cuenta”.*

*Testimonio de una acompañante de abortos.*

#### **La realidad del aborto en América Latina**

Este 2020 se cumplen 20 años desde que las mujeres latinoamericanas alzaron sus voces en demanda del derecho a la interrupción voluntaria del embarazo y aún hoy esta sigue siendo una de las reivindicaciones de las agendas feministas y de mujeres, ya no solo de América Latina sino del mundo entero.

Datos proporcionados por la OMS establecen que **una de cada tres mujeres que se práctica un aborto clandestino muere** y que 800.000 son hospitalizadas al año. De 55.000 abortos inseguros que se practican mundialmente, el 30% corresponden a América Latina y el Caribe. En la región, los únicos países que tienen marcos jurídicos que permiten la interrupción voluntaria del embarazo, sin ningún tipo de causales y en las primeras semanas de gestación, son Cuba, Uruguay, Puerto Rico, Guyana y Guayana Francesa1.

**El 28 de septiembre se conmemora el Día de la Acción Global por el Acceso al Aborto Legal y Seguro** con el fin de promover marcos legales que despenalicen el aborto y brinden las políticas necesarias para garantizar que las mujeres puedan acceder a este derecho. La iniciativa de este día se llevó a cabo en el V Encuentro Feminista Latinoamericano y del Caribe en 1990.

![](/images/uploads/palabrademujer.wordpress.com.jpg)

Mujeres pertenecientes a distintos países de América Latina (Bolivia, Brasil, Colombia, Argentina, Uruguay, Chile, El Salvador, Perú, México, entre otros) se manifestaron ante el riesgo de muerte que corren las mujeres al realizarse abortos clandestinos, debido a las restricciones legales que sufren en los diferentes países para decidir sobre sus cuerpos, **siendo estos la principal causa de muerte de mujeres en la región sudamericana** 2.

**¿Qué realidad se oculta tras la negativa de los diferentes Estados a la libre elección de las mujeres sobre su maternidad y sus cuerpos?** El cuerpo de las mujeres como territorio, el sometimiento del cuerpo político de la mujer por parte del poder social y subjetivo de los hombres y el Estado, el cual se ejerce de forma pública y privada, a través de diferentes formas de violencia que no son solo físicas sino también simbólicas.

Poder ejercido mediante discursos médicos, sociales, científicos y psicológicos que enarbolan significados sociales que luego son inscritos sobre los cuerpos de las mujeres para mantener el control, la violencia y la opresión sobre ellas, es decir, dispositivos de control y disciplinamiento, en términos de la biopolítica de Foucault.

Hablamos, entonces, de que los procesos de penalización o despenalización del aborto no son solamente asuntos de índole jurídica, sino el reflejo de la infraestructura de la política sexual en el entramado de las relaciones de poder y la diferencia sexual que ubica a las mujeres como objetos de intercambio y cuerpos de reproducción. Para Valdivia y Jaime, esta infraestructura son **“herramientas que reproducen o desplazan el estatus político de las mujeres”** 3 porque muestra las imposiciones que se ciernen sobre sus cuerpos debido a las lógicas patriarcales.

Como señala Nelly Minyersky 4, **la interrupción voluntaria del embarazo interpela al orden patriarcal**, pues remite a un cuestionamiento sobre cómo está pensado el orden social y el poder porque muestra la inequidad de género tras develar las problemáticas de orden moral, jurídico y social que se esconden; deja en evidencia las ineficiencias del sistema de salud pública; muestra que **el aborto no es un hecho del espacio privado y reformula su dirección hacía lo público**; explica la escisión del placer como derecho y la reproducción como elección; revierte la lógica de una sexualidad normativa y naturalizada con un modelo único de familia hegemónica y heteronormada; y redefine la libertad de elección de las mujeres.

**Un proceso de aborto clandestino es innegablemente una violencia para la mujer**, tanto física como psicológica, por todos los significados sociales a los que esta aparejado el embarazo, la maternidad y la tenencia de las y los hijos. Sin contar con el riesgo de muerte al que se expone. Al decir de Fernández, “Una mujer que decida abortar y cuente con las condiciones materiales para hacerlo en un país donde dicha práctica se encuentra penalizada y debe realizarse en circuitos clandestino produce efectos psíquicos, no necesariamente generados por el aborto en sí, sino por su penalización y clandestinización” 4.

#### **La realidad del aborto en Venezuela**

![](/images/uploads/albaciudad.org_-_foto_de_red_de_aborto_seguro.jpg)

**En Venezuela la legalización del aborto está sometida a un ostracismo político por parte de las autoridades del Estado y de las instituciones que llevan la batuta en materia de derechos de la mujer.** El código penal, que data de 1987 y que ha sido modificado en 2000, 2005 y 2006, tipifica que el aborto solo se puede producir en caso de que la parturienta esté en peligro de muerte. **La interrupción voluntaria del embarazo es penalizada con dos a seis años de prisión**. No obstante, Venezuela es el tercer país con mayores cifras de embarazo en adolescentes de América Latina y en los últimos informes obtenidos del Ministerio de Salud, se indicaba un altísimo porcentaje en mortalidad materna (66%), del cual un 13% correspondía a abortos clandestinos.

Además, a esto se suma la precariedad de la situación venezolana en el contexto actual en el que el acceso a métodos anticonceptivos y formas de planificación familiar, en especial, para las mujeres con escasos recursos, está bastante limitado. En este panorama, **el aborto sigue siendo una realidad restringida e ilegal para las mujeres venezolanas.**

A pesar de que los Derechos Sexuales y Derechos Reproductivos (DSDR) entran como Derechos Humanos y que la OMS reconoció en 1994 a la Salud Sexual y Salud Reproductiva como parte esencial de la salud, **en este país latinoamericano se vulneran estos derechos.**

Las mujeres, adolescentes y jóvenes en edad reproductiva se ven expuestas a maternidades no deseadas o a abortos clandestinos y en ambos casos se les vulnera al no atender a tratados internacionales como la Convención Belém Do Pará sobre la eliminación contra todas las formas de discriminación de violencia contra la mujer (1994) en la que Venezuela participó, e incluso a la propia Constitución Nacional que establece en su art. 76 que se garantizará y asegurará servicios de planificación familiar integral a las mujeres. 

**Las organizaciones y colectivas de mujeres y feministas han realizado diversas acciones para visibilizar, promover y exigir la legalización del aborto en el país.** Ejemplos de ello es que en junio de 2018 se consignó frente a la Asamblea Nacional Constituyente una propuesta de legalización del aborto y de los DSDR y otras organizaciones lanzaron a finales de 2019 una campaña para la discusión sobre la despenalización del aborto denominado “Madre, si yo decido”. Pero **la falta de interés del Estado y su gobernanza, ha bastado como respuesta**.

**Referencias**

* [Fucsia](https://www.fucsia.co/actualidad/personajes/articulo/dia-despenalizacion-del-aborto-28-de-septiembre/73963)
* [Mujeres en red](http://www.mujeresenred.net/spip.php?article179)
* [Cepal](https://oig.cepal.org/es/leyes/leyes-sobre-aborto)
* [Juntas nos cuidamos ](https://juntasnoscuidamos.org/el-aborto-en-venezuela)
* Fernández, A. Las lógicas sexuales: amor, política y violencias.
* Jaime, M. y Valdivia, F. (Eds.). (2020). Mujeres, aborto y religiones en Latinoamérica. Debates sobre política sexual, subjetividades y campo religioso.
* Minyersky, N. Derecho al aborto. Nuevas perspectivas.

<!--EndFragment-->