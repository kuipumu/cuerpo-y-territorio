---
title: '"Chicos, fuego" '
subtitle: Y de pronto se para el mundo
cover: /images/uploads/p1190065.jpg
caption: "Fotografias María Bélen Natali y Adriana Vallejos "
date: 2021-04-20T12:17:09.651Z
authors:
  - Paula Fausti
  - La Circular Transfem
tags:
  - Mujeres
  - Fuego
  - Ambiente
  - Comunidad
categories:
  - Cuerpo y Territorio
comments: true
sticky: true
---
**En el monte no hay sirena, la forma que tienen de enterarse si hay un incendio es mediante el olor.** De pronto el olfato se vuelve clave para la supervivencia. **Se sube a un espacio abierto elevado, y se busca de dónde proviene la columna de humo**. Esa es la sirena que tienen Adriana, guardaparque del Parque Provincial San Cayetano, y Belén, investigadora del CONICET de la Estación Biológica en el mismo Parque, en la Provincia de Corrientes, Argentina.

Por su trabajo y porque es donde viven, ellas fueron unas de las tantas personas que le pusieron el cuerpo a los incendios ocurridos el año pasado, donde **se quemaron más de 1 millón de hectáreas en todo el país.**

**En Latinoamérica, durante la pandemia, al mes de Octubre del 2020 se registraron 17.326 focos de incendio** en la mayor selva tropical del planeta, la Amazonía brasilera, viéndose dañados nuestros humedales y sierras, nuestras tierras y nuestro aire.

Desde Argentina, conmovidas por esta realidad, rememoramos los incendios del año anterior en nuestro país para repensar estas catástrofes que amenazan la vida humana y la biodiversidad. **Nos parece relevante situar e historizar los episodios anteriores** al ecocidio de la Patagonia, con la intención de comprenderlo en una serie de muchos episodios recurrentes que nos dañan a nosotres y a nuestro planeta.

En un mundo atentado por los intereses económicos de las elites-corporaciones **nos parece necesario volver al cuerpo para despertar nuestros sentidos y valorar la experiencia como faro.** En este sentido nos sentimos en la necesidad de re-unirnos a compartir nuestras experiencias y sentires. Así fue que tuvimos la oportunidad de juntarnos mediante video-llamada entre compañeres de las revistas “La Circular Transfem” y “Cuerpo y Territorio” para compartir nuestras resonancias al respecto.

Luego de esta instancia nos pareció necesario escuchar testimonios de quienes habían protagonizado alguno de estos sucesos, enfrentando al fuego en el territorio mismo, entonces, Belén y Adriana nos contaron sus experiencias del año anterior, en los incendios del Parque Provincial San Cayetano (Corrientes).

**En las imágenes que nos iban transmitiendo comenzamos a “latir incendio”, a sentirnos fuertemente atravesadas por ese “humo-alarma” que anuncia que se acerca lo que arrasa**. Comenzamos a sentir el miedo y a sentirnos parte de ese compromiso. También, las ganas de ponernos la mochila y salir a intentar frenar aquello que arrasa con todo... con los montes, con los animales, con nuestros hábitats, con nuestro aire, con nuestra agua, con nuestros cuerpos, ¡¡con nuestro planeta!!

![Fotografias María Bélen Natali y Adriana Vallejos ](/images/uploads/p1190132.jpg)

Algunos interrogantes guiaron el intercambio y nos siguen movilizando respecto de estas situaciones...

**¿Qué significa perder el hábitat donde vivís?, ¿qué se siente en esta situación de catástrofe?** Difícil armar un hilo ordenado de lo que se siente. Es difícil poner en palabras todas las emociones. Se empieza por lo sensitivo: el olor. El cuerpo trata de defenderse de tanto ardor, los mocos salen negros. Duele la cabeza, “se te parte en mil pedazos”. La piel arde, se pone roja, quema la cara.

**¿Qué se siente? Impotencia. Frente a un Estado ausente, que desfinancia sistemáticamente las áreas que supuestamente debe proteger.** Un Estado ausente y cómplice. Porque los incendios, a esta altura ya lo sabemos, son intencionales, de origen antrópico y responden a intereses económico-políticos. En Corrientes son intereses del agronegocio, en Córdoba vienen de los negociados inmobiliarios, en la Comarca Andina responden a la megaminería. Nombre la práctica extractivista que prefiera y ahí lo tiene: los incendios, el desmonte, la contaminación del agua, las fumigaciones, las megafactorías de animales, tierra arrasada. **Responden a un único llamado, al de la Necropolític**a, al de un capitalismo de la muerte ya fuera de control, que arrasa con todo, que no va a parar, porque se alimenta de la vida. Un Mundo del Revés donde lo que se vende es lo muerto: la tierra muerta, el chancho muerto, el árbol muerto.

¿Qué queda? Miedo. Porque esto puede volver a ocurrir, porque no se está haciendo nada para que no suceda.

Pero, ¿qué es un incendio? Los testimonios son crudos: “El tiempo parece que no existe, se para el mundo. Todo está gris, no se ve nada. **Los ojos te lloran, sentís cómo te quema la piel. Los animales se intoxican por el humo, ves a los pájaros en el piso con la boca abierta.** Es instantáneo, en cuestión de horas todo puede quemarse. No es como las inundaciones, que acá (Corrientes) son habituales porque se dan naturalmente. Si bien cada vez son más frecuentes, ocurren de a poco, son predecibles y dan tiempo a los animales para que busquen espacios altos donde resguardarse.”

Donde hubo fuego, quedan más que cenizas…

Belén se dedica a estudiar la transmisión de enfermedades desde animales domésticos a animales silvestres. Su investigación comprende el estudio de diferentes animales que viven en el monte, entre ellos los monos. Nos cuenta que esta investigación lleva más de 40 años y que conocen a cada mono que vive en el Parque, incluso tienen nombre y apellido. Los monos viven en grupos llamados “tropas”. Con mucho dolor nos comparte que en un recorrido post incendio vieron a estas tropas calcinadas. La imagen quita el aliento: familias enteras convertidas en cenizas.

![](/images/uploads/p1190125.jpg "Mono Aullador Negro y Dorado (Alouatta caraya)")

Adriana cuenta que es común que los pastizales se prendan fuego, pero que el monte, al estar siempre húmedo, se mantenía protegido y por eso el fuego no llegaba. Sin embargo, la sequía extrema del año pasado hizo que todo esté muy seco y el fuego llegó. Empezamos a ver las consecuencias de la crisis climática: cada vez se dan mayores condiciones para que se propaguen los incendios.

**¿Quién protege al monte? “Las áreas protegidas están totalmente desprotegidas” frase paradójica que pareciera encerrar algún tipo de enigma.** Las personas que cuidan esos territorios también están desprotegidas. Poner el cuerpo en esas situaciones tan límite no es gratuito. “No comés, no dormís, no tomás agua. El cuerpo está cansado pero la adrenalina hace que sigas.” Se sigue hasta que el cuerpo pone un límite y dice basta, no doy más.

Nuestros territorios-cuerpo-tierra, al decir de Lorena Cabnal, están gritando “no damos más”. **No podemos seguir así, esto tiene que cambiar.** El límite ya fue cruzado, no podemos volver atrás, pero sí podemos apostar a horizontes regenerativos que ayuden a sanar y reconstruir otro mundo posible, donde quepan otros mundos, al decir zapatista.

![Fotografias María Bélen Natali y Adriana Vallejos ](/images/uploads/p34.jpeg)

¿De dónde sacar las fuerzas para seguir?

**Adriana y Belén coinciden en que la fuerza está en lo colectivo.** En la comunidad de vecinos y vecinas que se juntó y organizó para dar frente al fuego, tratando de apagarlo con lo que había a mano: el barro, ramas caídas. Pero también con los mensajes que llegan a la distancia, una vez que volvió la señal y se tuvo un momento de descanso. Estar presente, acompañar. Y también difundir, dar voz.

¿Qué nos queda ahora? Adriana dice: “Darlo todo”. Queda en cada une hacerse esta pregunta y tratar de darle respuesta. **Sabemos que no es fácil parar la máquina, frenar este engranaje que nos tiene sumides en la vorágine cotidiana del día a día.** El trabajo, el estudio, la vida social, la militancia, la crianza. Pero escuchar las voces de estas mujeres que día a día guardianan el monte nos mueve el piso y nos hace replantearnos todo.

“Al amanecer estamos acostumbradas a levantarnos con los aullidos de los monos. Es el monte gritando.” El silencio al día siguiente de los incendios aturde: ¿dónde están ahora? Encontraron un grupo de 5 monos que se habían salvado, Adriana dice “ellos son los verdaderos resilientes”. Durante los días siguientes las cámaras-trampa fotografiaron a otros animales que sobrevivieron: zorros, guazunchos, aves. Ahora el monte volvió a estar verde, los troncos de los árboles están calcinados, pero ya se ven algunos retoños. La primavera trajo algunas flores. **La vida se hace paso, con su gran fuerza pujante, y** **mientras quede un pedazo de monte, vamos a estar ahí defendiéndolo”**

![Fotografias María Bélen Natali y Adriana Vallejos ](/images/uploads/p36.jpeg)

Desde “La Circular Transfem” y “Cuerpo y Territorio” queremos decir que este tema es urgente todo el año, no solo cuando ocurren los “hechos”. En este escuchar escribiendo y sintiendo, nos abrazamos para decir basta a esta manera de seguir arrasando con la vida. **Escuchamos acuerpadas, acompañándonos, conteniéndonos y confiando en la fuerza de la vida.** Comprometides a visibilizar con urgencia los arrasamientos a nuestras tierras, nuestros hábitats, nuestros cuerpos.

Seguimos gritando, estamos ardiendo.

Fotos de la recorrida post incendio tomadas por María Belén Natalini y Adriana Vallejos en el Parque Provincial San Cayetano, Corrientes, Argentina.
