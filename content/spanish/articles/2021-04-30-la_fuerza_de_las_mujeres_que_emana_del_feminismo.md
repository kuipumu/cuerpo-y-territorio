---
title: La fuerza de las mujeres que emana del feminismo
subtitle: "Un panorama sobre las corrientes feministas de Brasil "
cover: /images/uploads/ato_em_sp_2020_-_julia_chequer_-_brasil_de_fato.jpeg
date: 2021-04-30T21:21:01.285Z
authors:
  - Michele de Mello
tags:
  - Mujeres
  - Feminismo
  - Genero
  - Brasil
categories:
  - Genero
  - Cuerpo
  - Territorio
comments: true
sticky: true
---
Hablar sobre el **feminismo en Brasil** no es una tarea fácil. Si entendemos feminismo como la lucha por más derechos a las mujeres, y a su vez, por condiciones más justas y libres de vida para todas y todos, veremos que desde que empezó la batalla por el derecho a la Patria / Matria estaban las mujeres.

Empezando por **Dandara dos Palmares**¹, quien libró batallas al lado de Zumbi dos Palmares **contra la esclavitud.** Seguida de la guerrera indígena **Clara Camarão**, que luchó contra la invasión holandesa en las costas de Pernambuco, noreste del país. Las dos heroínas del siglo XVII abrieron caminos para la lucha por la Independencia, protagonizada por **Maria Quiteria de Jesús**, conocida como la Joana D’arc brasileña.

Si seguimos en el curso de la historia, Brasil, así como otras naciones de Latinoamérica, acompañó las distintas olas vividas por el movimiento feminista en el mundo.

Una primera ola² a finales del siglo XIX e inicio del siglo XX, marcada por la **conquista de los derechos civiles** básicos, como el derecho a leer y estudiar en 1827. Más tarde, vino la lucha por la **participación en la vida política**, con la elección de la primera alcaldesa latinoamericana, **Alzira Soriano, en 1929**; el derecho al voto, conquistado en 1932; seguido de la elección de la primera diputada estadal en 1934: una mujer negra, periodista y escritora, **Antonieta de Barros**, en Santa Catarina, y una médica, **Carlota Pereira de Queirós**, electa diputada federal por el estado de São Paulo. Ambas fueron constituyentes y perdieron sus cargos con la imposición del régimen autoritario del Estado Novo, de Getúlio Vargas en 1937.

Ya la **segunda ola** podría ser ubicada en el fin de los años 60 hasta la década de los 80, con las reivindicaciones vinculadas a los derechos reproductivos de la mujer y la lucha contra la dictadura, poniendo peso en la participación femenina en la Constituyente de 1988.

Ya con la redemocratización empieza una **tercera ola del feminismo** brasileño que profundiza en el debate sobre la relación entre el feminismo y la lucha por un horizonte de transformación social profunda. Identificando las raíces de la opresión de género, apuntando las diferencias entre las reivindicaciones de mujeres blancas y mujeres negras o entre las trabajadoras y burguesas.

Hay autoras que ubican una cuarta ola que empezaría con la generación Z, los centennials y millenials, que amplían y comunican las banderas del feminismo, a través de las redes sociales.

Además de un breve análisis por la línea del tiempo, también se puede identificar los distintos matices del feminismo de acuerdo a sus corrientes político-ideológicas.

### Feminismo popular

![](/images/uploads/mst_parana.jpeg)

Reúne los **movimientos sociales y de mujeres campesinas**, como la Marcha de las Margaritas, organizada por la Confederación Nacional de los Trabajadores Agrícolas (CONTAG), desde el año 2000. Empezó para denunciar el asesinato de una lideresa sindical campesina y hoy moviliza a decenas de miles de mujeres el 12 de agosto de cada año, en Brasília, para defender sus derechos a la tierra, a la vida, en contra del extractivismo, del latifundio monocultor y en defensa de los derechos de las mujeres.

Dentro del espectro del feminismo popular también están las organizadoras de la **Marcha Mundial de las Mujeres** en territorio brasileño. Año tras año, el evento logra defender banderas unitarias de las mujeres trabajadoras en todo el planeta, como la exigencia de paridad salarial y la equidad de género en todos los espacios, lo que incluye mayor representación en los espacios de decisión política.

### Feminismo Liberal

En la nueva era de las redes sociales el feminismo liberal tiene en las influencers sus mejores representantes. Es este sector que tiene empatía con determinadas reivindicaciones históricas del movimiento feminista, sin embargo, difícilmente apoyará de manera contundente una bandera que afecte el grano del patriarcado.

Si en otro momento fueron las feministas liberales las que lideraron las manifestaciones por el derecho al sufragio, hoy su militancia difícilmente saldrá de las redes sociales para ocupar las calles con protestas.

**Defienden los derechos reproductivos, el fin del femicidio** -- que en Brasil cobra una nueva víctima cada seis horas pero siempre buscando salidas legalistas dentro del orden capitalista.

### Feminismo Radical

![](/images/uploads/ato_em_sp_2020_-_elineudo_meira.jpeg)

Las feministas radicales representan el lado opuesto del espectro político de los feminismos. Entienden que el patriarcado – como un conjunto de signos, condiciones y determinaciones que históricamente favorecen a los hombres – está tan permeado en la formación del macho que es imposible que ellos algún día puedan ser considerados feministas.

Es cierto que todo el movimiento feminista defiende que el protagonismo es de las mujeres, pero para el feminismo radical el hombre difícilmente podrá ser un aliado.

**Son directamente contrarias a la prostitución.** Contrastando con las liberales que afirman que ser trabajadora sexual sería una elección, las radicales denuncian la doble explotación de la mujer.

Entienden que la industria pornográfica completa un ciclo de objetificación de la mujer en el imaginario masculino, que favorece la dominación patriarcal, y refuerza actitudes cómplices de la violación sexual.

Aunque también defienden que la imagen de lo femenino es una construcción social, en general, no aceptan a mujeres trans dentro de su movimiento. Entienden que la carga histórica de haber vivido un período de su vida performando una versión masculina y disfrutado de determinados privilegios, genera diferencias muy marcadas entre las mujeres con el sexo biológico femenino y las mujeres trans.

### Feminismo Socialista

**Surge desde la organización de mujeres comunistas y socialistas** dentro de sus propios partidos, ya sea para hacer denuncias del machismo de sus organizaciones o para poner en el orden del día la necesidad de debatirse el feminismo desde una perspectiva clasista.

Las feministas socialistas extrapolan sus organizaciones con la visión de que el patriarcado es un pilar importantísimo del capitalismo y logró profundizar un sistema de explotación-opresión que demandará la disposición de todas, todes y todos para ser destruido.

### Feminismo negro

Sobre todo a partir de la tercera ola, feministas negras empezaron a decir basta para su invisibilidad dentro del movimiento feminista en general.

Mientras las feministas blancas luchaban por el derecho al trabajo y la vida pública, las mujeres negras eran esclavizadas por hombres y mujeres blancas.

Es en este momento donde toma mayor espacio la comprensión de que el racismo no puede ser tratado como un obstáculo más en la construcción de una sociedad libre de opresiones de género, sino como un factor determinante.

Así surgen conceptos como interseccionalidad para entender la manera como se alinean y se fortalecen tres formas de opresión: de color, género y clase.

Todas estas diferenciaciones, sin embargo, no representan una debilidad. Al revés comprueban los siglos de opresión que tenemos recorridos, siglos de hombres que nos callaron, nos objetivan y nos impidieron influir en las decisiones que determinan nuestras vidas.

Toda la diversidad del movimiento feminista comprueba lo fructífero que puede ser dar espacio y voz para que las mujeres puedan debatir, estudiar y unificar su comprensión de las Chagas de la vida misma.

### La diversidad no debe representar competitividad.

Eso lo ha comprobado el movimiento feminista el último **8 de marzo**. En los cuatro cantos del planeta, las mujeres protagonizamos actos simbólicos o incluso marchas multitudinarias con banderas que son urgentes y que unifican nuestra lucha.

Mientras atravesamos la mayor pandemia del último siglo, nosotras sentimos el peso redoblado de lo que representa la emergencia sanitaria: aumento del desempleo, explosión de la violencia contra las mujeres y femicidios, la explotación del trabajo doméstico con los cuidados, y en muchos casos también el sostén financiero, de toda la familia.

A pesar de que representamos 70% de los profesionales de salud y educación en el mundo, el nivel de **desempleo** entre las mujeres fue casi el doble que entre los hombres en 2020, según datos de la Organización Internacional del Trabajo (OIT)4. Entre los 14 millones de brasileños que empezaron el año sin trabajo, 8.5 millones eran mujeres.

Por este escenario, también somos la mayoría de los 118 millones que entraron en situación de pobreza, segundo la Comisión Económica para América Latina y el Caribe (Cepal)5.. La situación fue agravada por la pandemia, pero es estructural. ONU Mujeres señala que por cada 100 hombres en el mundo, existen 132 mujeres en situación similar6.

Como si fuera poco, las casas se convirtieron en ambientes aún más inseguros para las mujeres. Durante la pandemia aumentaron en 40% los casos de feminicidio en todo el mundo, según las Naciones Unidas. La media fue de 15 millones de mujeres asesinadas cada tres meses en lo que va de confinamiento.

Por eso las banderas que resonaron en el último Día Internacional de la Mujer fueron: por vacunación masiva, por renta básica universal, por políticas que realmente protejan a las mujeres de la violencia y por justicia para las compañeras que les quitaron la vida.

En el caso de Brasil, todas las pautas se articulan con la exigencia de la salida inmediata de Jair Bolsonaro del poder, un presidente que defiende una ideología fascista, que empleó una política irresponsable que generó la crisis sanitaria actual. Son casi 400 mil muertos, mil veces más que los brasileños caídos en la Segunda Guerra Mundial7. Según estudios independientes8, si las condiciones de contagios se mantienen, llegaremos a un millón de muertos por covid-19 hasta octubre de 2021.

![](/images/uploads/mst_minas_gerais.jpg)

Caracterizarlo como un gobierno genocida parece ser poco.

Somos las mujeres la cara del pueblo latinoamericano. Somos las primeras en sentir qué representa la precarización de las condiciones básicas de reproducción de la vida. Somos las que entienden de qué se trata una ‘recesión’, porque son las que luchan por el pan diario.

De esta forma, en un momento de agravamiento de la crisis estructural del modo de producción capitalista, son las mujeres que dan ejemplo de lectura, organización y unidad.

Que sigamos diversas, pero unidas, por un horizonte que nos permita soñar aún más.

#### Referencias

1. RODRIGUES, S. Conheça a história do feminismo no Brasil in: Revista Azmina. 2020. Disponible en: [https://azmina.com.br/reportagens/feminismo-no-brasil](https://azmina.com.br/reportagens/feminismo-no-brasil/)
2. FRANCHINI, B. S. O que são as ondas do feminismo? in: Revista QG Feminista. 2017. Disponible en: <https://medium.com/qg-feminista/o-que-s%C3%A3o-as-ondas-do-feminismoeeed092dae3a>.
3. Fórum Brasileiro de Segurança Pública. Anuário Brasileiro de Segurança Pública. 2020. Disponible en:<https://forumseguranca.org.br/wp-content/uploads/2021/02/infografico-2020-final-100221.pdf>
4. Organización Internacional del Trabajo (OIT). ILO Monitor: COVID-19 and the world of work. Seventh edition. 2020. Disponible en: [https://www.ilo.org/wcmsp5/groups/public/---dgreports/---dcomm/documents/briefingnote/wcms_767028.pd](https://www.ilo.org/wcmsp5/groups/public/---dgreports/---dcomm/documents/briefingnote/wcms_767028.pdf)
5. Comisión Económica para América Latina y el Caribe (Cepal). Pandemia provoca aumento en los niveles de pobreza sin precedentes en las últimas décadas e impacta fuertemente en la desigualdad y el empleo .2020. Disponible en: [https://www.cepal.org/es/comunicados/pandemia-provoca-aumento-niveles-pobreza-sin-precedentes-ultimas-decadas-impact](https://www.cepal.org/es/comunicados/pandemia-provoca-aumento-niveles-pobreza-sin-precedentes-ultimas-decadas-impacta)
6. Organización de las Naciones Unidas para las Mujeres. La pandemia de la COVID-19 y sus efectos económicos en las mujeres: la historia detrás de los números.2020. Disponible en: https://www.unwomen.org/es/news/stories/2020/9/feature-covid-19-economic-impacts-on-women
7. Observatório do Terceiro Setor. Covid-19 já matou mais militares brasileiros do que a 2ª Guerra Mundial. 2020. Disponible en: <https://observatorio3setor.org.br/noticias/covid-19-ja-matou-mais-militares-brasileiros-do-que-a-2a-guerra-mundial/>
8. KOUPRIANOV, A. Brasil pode atingir até 1 milhão de mortes por covid-19 até outubro de 2021. 2021.Disponible en: <http://acoluna.org/2021/04/18/brasil-pode-atingir-1-milhao-de-mortes-por-covid-19-ate-o-fim-de-2021/>

#### Michele de Mello

Periodista brasileña, licenciada por la Universidad Federal de Santa Catarina (UFSC).
Directora del documental "Aislados: Cuba y Estados Unidos, del bloqueo al
reacercamiento"/Ex-presentadora de noticias en el canal TeleSUR
Corresponsal del multimedio Brasil de Fato en Caracas, Venezuela.
Militante del Polo Comunista Luiz Carlos Prestes (PCLCP)