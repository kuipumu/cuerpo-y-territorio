---
title: Rompiendo el silencio sobre el abuso sexual
subtitle: " "
cover: /images/uploads/doisneau.jpg
date: 2020-10-16T01:57:14.831Z
authors:
  - Niyireé S Baptista S
tags:
  - ViolenciaDeGénero
  - Mujeres
  - Initimidad
  - Maltrato
  - Violación
  - Relato
  - Cuerpo
  - Violencia
  - Niños
categories:
  - Cuerpo
comments: true
sticky: true
---
<!--StartFragment-->

*`A la niña de botitas azules que inspiró la mujer que soy ahora`*

Hay realidades que una se calla porque pesan demasiado para ser sostenidas por un cuerpo. Aún hoy, decir “yo sobreviví al abuso sexual”, sigue siendo una herida que flamea espumosa cada cierto tiempo. **Mi yo-niña se hizo una coraza de acero y apelmazó sus tristezas en una cajita para que doliera menos;** cuando cerraba los ojos podía escapar del dolor y la angustia y me acostaba a dormir abrazando la inocencia que me fue despojada. La pequeña nunca dejó de sonreír ni de soñar los mundos, que hoy de adulta comprendo que nos salvaron.

**Hablar del abuso sexual nunca es una tarea fácil y menos cuando una todavía lleva a cuestas años de dolor silencioso.** La historia que plasmaré aquí no busca en ninguna medida ser un artículo escabroso y tampoco procurar lástima a quien lo lea, si hay alguna razón por la que me he propuesto hablar sobre este tema, **es una necesidad de soltar las palabras que se juntan una por una en la naciente de mi garganta.** 

Tomo aire, se me aguarapan los ojos y la rabia y la tristeza punzan fuertemente en medio de mi pecho. Me quedo inmóvil frente a las teclas de la laptop, pero **recuerdo que la valentía es una de las características de mujeres como yo: marcadas, heridas con una cicatriz inmensa en medio del alma.** Durante días he pensado en cómo escribir este artículo, en qué decir y cómo abordarlo, luchando con el marasmo de sensaciones que se revuelcan ardientes en la mitad de mi estómago. Ninguna certeza viene a mi cabeza, lo único que me mueve es la necesidad de hablar por mi niña y todas las niñas que alguna vez y aún hoy, son parte de las estadísticas silenciosas del abuso sexual.

#### **La realidad del abuso sexual: la cultura de la violación**

Las estadísticas actuales de organizaciones como UNICEF1 o Defend innocence2 cifran que **1 de cada 5 niñas, niños y adolescentes son abusados sexualmente antes de cumplir los 18 años,** es decir, una población infantil que ronda los 1000 millones de habitantes. 

A pesar de que esta es una realidad para ambos sexos, las niñas son las más vulneradas: 1 de cada 10, es víctima de algún tipo de violencia sexual. Venezuela es un país que no escapa a estas realidades. Según el informe anual de la Fundación Habla3, publicado **en 2019, el abuso sexual constituye el 50% de las violencias sexuales cometidas hacia la infancia;** además, el informe apunta que el principal foco donde ocurren estos hechos es dentro del núcleo familiar. En el 95% de los casos el agresor es un hombre, familiar o conocido de la víctima; y un 79% de las agresiones están dirigidas hacia las niñas y las adolescentes, quienes son propensas a sufrir abusos sexuales desde el nacimiento hasta los 17 años de edad. 

A pesar de lo alarmante que son estas cifras, la veracidad de los datos pueden ser más desgarradores si se toma en cuenta la cantidad de niñas que callan, por miedo y vergüenza de ser juzgadas. **El 85% de los casos de abuso sexual infantil no lo develará y solamente el 20% de los casos llegan al sistema judicial**4. Estas realidades se ven cruzadas en medio de una sociedad patriarcal, cuyos pilares fundamentales se centran en la cultura de la violación y en la apropiación de los cuerpos de las mujeres. Es así como el abuso sexual atraviesa la clase social, religión, etnia o país, y deja atrás la creencia de que esta realidad sólo se vive en zonas de pobreza o países de “subdesarrollados”; un ejemplo de ello es que **EE. UU. presenta las tasas más altas de abusos sexuales de los países de Occidente.**

**La cultura de la violación es la normalización en la sociedad de la violencia sexual.** Una cultura que ignora o minimiza los actos sexuales que perjudican y violentan los cuerpos de las mujeres, niñas y niños, a la vez que promueve la misoginia, expresada a través de los medios de comunicación, estereotipos, roles de género, la prostitución, la pornografía, el complejo Lolita, y todo el aparataje simbólico que encasilla a la sexualidad, en especial, a la femenina, como una mercancía para satisfacer los deseos de lo masculino. 

Así, los cuerpos de las mujeres son vistos como propiedades, objetos, para poseer y sobre el cual ejercer control a través del poder, subyugando completamente a las mujeres. En este sentido, **la cultura de la violación impone que toda mujer o niña que denuncie una agresión sexual será cuestionada**, se le culpará juzgando su forma de vestir, su actuar y su consentimiento; además, se minimizará el daño y se le hará sentir desprotegida al brindar salidas a los agresores y al permitir el mantenimiento del statu quo de las estructuras de poder patriarcal, dentro de las cuales los gobiernos y las instituciones a cargo de hombres siguen legislando a favor de leyes que mantienen y perpetúan este flagelo y dentro de las cuales cada día se devela la violencia ejercida sobre las mujeres y las niñas en medio de lo que se denominan “grandes escándalos sexuales”.

#### **Debemos derrumbar las máscaras del silencio**

“Yo tenía muchas máscaras”, cuenta una chica sobreviviente de abuso sexual en el libro El abuso sexual: la verdad acerca de los abusos sexuales de la doctora Patti Feuereisen. **El dolor del abuso sexual dura el tiempo que la víctima necesita para sanar, romper el silencio y hablar de lo sucedido.** Es ese el primer paso. Sin embargo, muchas mujeres, a lo largo de su vida, esconden esa herida y nunca la dejan salir, por miedo, culpa o vergüenza.

 En todos los grupos de mujeres que he estado, siempre me encuentro con una o tres chicas que hemos sido víctimas de abuso sexual, y la gran mayoría ha pasado por situaciones de acoso o violencia. Es importante aclarar que **el abuso sexual o violación, no necesariamente se enmarca dentro de tocamientos o penetración,** sino también en palabras y acciones que nos incomodaron y que transgredieron nuestra intimidad.

Es doloroso pensar que muchos de los primeros recuerdos de las mujeres es haber sufrido abusos sexuales en la infancia. Estas realidades nos arrebatan nuestra autoestima y fortaleza, más aún cuando los perpetradores son personas en las cuales se depositó confianza y amor. 

De allí que es importante dejar oír nuestras voces, manifestar que las violaciones existen y que suceden a diario en la vida de las mujeres, y lamentablemente constituyen una realidad histórica que se suele ocultar dentro de las familias, en la sociedad y en nuestra cultura, lo que hace que las niñas crezcan en ambientes desprotegidos y las hacen incluso más vulnerables a continuar sufriendo de abusos en la edad adulta. La Dra. Patti Feuereisen explica que **la vergüenza del abuso sexual no está conectada con el acto en sí, sino que es el secretismo de todo ello lo que causa vergüenza, lo que produce un atontamiento silencioso que alcanza el centro de lo que la chica es.** Es por ello que debemos recordar que somos sobrevivientes de un acto de violencia y que jamás es nuestra responsabilidad.

Cada cierto tiempo despierto asustada soñando y nuevamente recorre mi cuerpo la sensación de miedo que me atemorizaba de niña. Ya no dejo que mi cuerpo se escape de mí como antes, que lo observaba de lejos, con asco, como si no me perteneciera. He aprendido a afrontar el dolor, a dibujarme mundos más hermosos, donde siempre pervive la niña de botas azules. **Puedo decir ahora que mi infancia no fue triste por completo y ella, la niña que fui y que me acompaña siempre**, me mira feliz y me dice: “somos la mujer que queríamos ser”, y yo le respondo feliz: “lo hicimos juntas”.

Referencias

1. [UNICEF](*<https://www.dw.com/es/unicef-unos-120-millones-de-ni%C3%B1as-y-j%C3%B3venes-han-sufrido-abuso-sexual/a-17902991>*)
2. [Defendinnocence.org](*<https://defendinnocence.org/es/obten-la-informacion/>*)
3. Informe Anual: Cifras de Abuso Sexual Infantil y Adolescente en Venezuela. Fundación Habla, 2019.
4. [](https://www.elmundodelosasi.org/el-abuso-sexual-infantil-en-cifras/)[El abuso sexual infantil en cifras](*<https://www.elmundodelosasi.org/el-abuso-sexual-infantil-en-cifras/>*)
5. Feuereisen, Patty; El abuso sexual. La verdad acerca de los abusos sexuales. España, Neo Person, 2005.
6. <https://www.invisiblegirlsthrive.com/>

<!--EndFragment-->