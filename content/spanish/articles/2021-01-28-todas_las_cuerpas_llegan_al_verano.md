---
title: Todas las cuerpas llegan al verano
subtitle: " "
cover: /images/uploads/whatsapp_image_2021-01-26_at_8.10.04_pm.jpeg
date: 2021-01-28T18:46:00.384Z
authors:
  - Shirly Dana
tags:
  - Cuerpos
  - Feminismo
  - Género
  - Mujeres
  - Derechos
  - Aceptación
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

Desde que somos niñes nos transmiten de una u otra forma, que la cuerpa que importa es la que ven les demás, esa que es observada y juzgada. De alguna forma nos enseñan a odiar nuestra propia corporalidad y a intentar que quepa dentro de los estándares sociales de la belleza hegemónicos que establece de alguna forma la sociedad.

Las palabras con las que nos definimos están teñidas de ese odio y, entonces, es muy difícil luego rearmar y restablecer un vínculo de amorosidad con esa cuerpa que es la que nos va a acompañar en nuestro tránsito por esta vida. A la larga, todos esos microdaños ejercidos hacia nuestra cuerpa repercuten en la forma en la que la vivimos, desterrando las posibilidades de placer, de disfrute y de amorosidad para con nosotres mismes.

Cuando era chica, era todo un evento “llegar al verano”, y una se preparaba haciendo dietas extravagantes y realizando ejercicio físico hasta el cansancio porque había que llegar bien. Me resulta inevitable repensar esas épocas, analizando las publicidades que exhibían todo el tiempo corporalidades delgadas como estandarte de la belleza. Funcionaban como el objetivo al cual había que llegar, sin importar qué ni cómo. La mirada social puesta sobre nuestras corporalidades solamente reforzaba esa necesidad de encajar en la construcción social de la belleza en torno a los cuerpos hegemónicos y delgados.

Salir a comprar ropa, cualquier tipo de ropa, siempre fue un evento estresante. Los talles que nos quedaban no estaban dentro de los que se fabricaban, salvo en esos locales con “talles grandes”, pero muy pocas veces en locales con ropa que estuviera a la moda en aquel entonces. Además, estas imposiciones sociales se reforzaban con la insistencia desde la medicina hegemónica que las corporalidades gordas y disidentes no eran saludables. Esta línea y perspectiva de la medicina pone la tónica en la salud como vía por la cual insistir en el moldeado de las cuerpas para que se ajusten a esa “normalidad” socialmente impuesta, profundizando el mandato de la delgadez. ¿Acaso ser delgade es la única forma de estar o ser saludable?

Por suerte hoy la realidad de las personas que tenemos corporalidades disidentes cambió. No del todo, pero hacia allá va. Hay muches militantes de cuerpas gordas, hay libros y poemas que relatan las vivencias desde adentro. Hay cada vez más marcas que cumplen con la ley de talles (ley n°27521), permitiéndonos comprar ropa en (casi) cualquier lugar que queramos. 

Cada vez se escuchan menos comentarios como “estás más linde, estás más flaque” y hay menos opiniones para dar de las cuerpas de les demás. Los halagos . No creo que el camino ya esté del todo allanado ni mucho menos con discusiones y debates saldados, pero la única forma de poder expresarnos y vivir con libertad es esta, haciéndonos presentes en la redes sociales, quitándole el velo de la “normalidad” (insistimos sobre este concepto de normalidad entre comillas puesto que hace referencia a las corporalidades que desde la postmodernidad fueron vanagloriadas como corporalidades deseables, bellas, como esa meta a la cual llegar. Cuando digo normalidad me refiero a esas cuerpas jóvenes, esbeltas, delgadas, con medidas 90-60-90 y, dato no menor, de color blanco) que recae sobre las cuerpas. 

![](/images/uploads/whatsapp_image_2021-01-26_at_8.10.11_pm.jpeg)

No estamos acá para encajar bajo ninguna etiqueta ni talle, en ninguna publicidad ni tampoco en los estándares socialmente impuestos, no queremos amoldarnos a sus formas ni a sus concepciones de belleza. Estamos acá y somos, queremos ser. Como dice Susy Shock, reivindico mi derecho a ser une mounstrue y que otres sean lo normal.

> *Cuando me siento*
>
> *el botón del jean me aprieta*
>
> *me marca la piel*
>
> *y el alma.*





> *No, este no me queda chico,*
>
> *pero lo siento como si*
>
> *creo que es por el tiro alto*
>
> *porque es el que siento calza mejor.*





> *Qué incómoda esa sensación*
>
> *de la gota de sudor que,*
>
> *despacito,*
>
> *va bajando por mí espalda*
>
> *y se deposita ahí, en el medio*
>
> *ente el jean y la piel del culo.*





> *Que fastidio, lo duro del jean*
>
> *contra mí piel*
>
> *más con el calor, en verano*
>
> *pero con pollera es peor*
>
> *me rozan los muslos*
>
> *y se pasan*
>
> *y se lastiman*
>
> *y duelen*
>
> *y vuelve mí vieja*
>
> *intentando hacerme entrar*
>
> *en jeans que no me van*
>
> *saltando para calzarlos*
>
> *y abrochándolos acostada*
>
> *porque sí, porque son lindos.*



> *Por suerte hoy mis jeans*
>
> *no son así*
>
> *son grandes y sueltos*
>
> *siempre de mí talle*
>
> *los busco y elijo*
>
> *de esos que sientan bien en mí cadera*
>
> *y que me dejan mover*
>
> *cuando no, son calzas*
>
> *blandas y versátiles.*





> *Pero la piel...*
>
> *no la puedo despojar de esa sensación*
>
> *a la piel de mí panza*
>
> *cuando el pantalón aprieta*
>
> *y te recuerda que está ahí*
>
> *trayendo a todos esos otros*
>
> *(pantalones)*
>
> *que usé y en vez de elegirlos*
>
> *los odié.*