---
title: Sobre barbijos, derechos y obligaciones
subtitle: " "
cover: /images/uploads/file-20200419-152571-1p3iiem.jpg
date: 2020-11-29T20:47:21.268Z
authors:
  - Patricia Lepratti
tags:
  - DDHH
  - DeclaracionDDHH
  - ONU
  - Pandemia
  - Derechos
  - Covid
  - Confinamiento
  - Mundo
  - Políticas
categories:
  - Política y Economía
comments: true
sticky: true
---
<!--StartFragment-->

**El 10 de diciembre de 1948 fue aprobada por la Asamblea General de las Naciones Unidas la Declaración Universal de Derechos Humanos.** Setenta y dos años después, en un año muy particular, la pandemia del Covid 19 y las medidas implementadas - o no - por los distintos gobiernos para controlar su propagación pusieron en debate las diferentes concepciones en torno a los derechos humanos. Sobre todo, en lo que se refiere a su carácter individual o social.

En este sentido, **la idea que intentaré defender aquí es que los derechos humanos siempre son sociales.** Han nacido como fruto de luchas colectivas y tienen por objeto normativizar las relaciones sociales a nivel universal.

Reconozco que defender una noción de los derechos humanos como creación social y, por lo tanto, histórica y cambiante, significa alejarse de la idea de unos derechos inherentes a la condición humana. Pero si esto puede parecer un sacrilegio, referir los derechos humanos a una esencia humana trascendental – por su parte- desnuda a los seres humanos de su historia, de los lazos, las comunidades y las instituciones que han construido.

**Separar los derechos humanos de las condiciones sociohistóricas en las que han surgido y en las que se desarrollan, ha permitido a la ideología liberal idealizar la libertad individual frente al reconocimiento de las luchas colectivas.**

Sin embargo, esta idea de derechos puramente individuales es difícil de sostener si nos pensamos viviendo solxs en una isla, sin contacto con nadie. ¿Quién respeta o violenta nuestros derechos? Sólo soy portador o portadora de derechos (y obligaciones) cuando aparece un/a otrx, cuando estoy en relación, cuando vivo en comunidad.

*“Nadie se salva solo, nadie salva a nadie, todos nos salvamos en comunidad”* dijo Paulo Freire. Y me tomo el atrevimiento de agregar: nadie “es” solx. Todxs lxs seres humanos particulares hemos sido gestados en el vientre de otra persona, hemos sido alimentados por ella, y esta persona -a su vez-fue sostenida por otras. Es decir, **somos lo que somos en comunidad. La comunidad es necesaria para la vida.**

El aislamiento padecido este año nos ha mostrado que nuestra vida tiene sentido en contacto con otxs y en esta relación con otxs tenemos responsabilidades. La primera de esas obligaciones es su reconocimiento. A partir de entonces podemos iniciar una actividad inherentemente humana, a saber: el diálogo, el debate en busca de mejores maneras de relacionarnos.

**Es más, una declaración como la que estamos celebrando hoy es un acto comunicativo, que implica a más de una persona.** En este sentido, aunque el individuo sea el sujeto de la Declaración Universal de Derechos Humanos, la misma fue proclamada por una institución (social) como la ONU, y tenía como objetivo establecer nuevas normas para regular las relaciones de la comunidad internacional resultante de la segunda guerra mundial.

Durante los setenta y dos años desde su aprobación, se ha discutido sobre la efectividad de este instrumento que no ha podido detener las guerras ni los atropellos hacia la vida de los que lamentablemente somos testigos a diario. Sin embargo, representa un lenguaje común a la gran diversidad de seres humanos que habitamos este planeta. Un paradigma desde el cual expresarnos, demandar y defender nuestro derecho a ser reconocidxs como miembros de una comunidad local, nacional, global.

**Somos en vínculo con otrxs y por eso nos contagiamos, porque somos humanxs, en China, en Argentina, en Inglaterra y en Uruguay.**

Al final las pandemias, como las guerras, pasan, se terminan. **Lo importante es la manera en que definimos nuestra humanidad para atravesarlas.**



<!--EndFragment-->