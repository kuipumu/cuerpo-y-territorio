---
title: 'Huella de Elefante'
subtitle: 'Crónica de una demolición'
description: 'Era el primer viernes del mes de junio, pero aunque viernes, era también el primer día de la residencia, aquel lugar al que costó tanto llegar, pero al final encontré.'
cover: 'images/uploads/huella-de-elefante.jpg'
caption: 'Foto: Belén Hernández'
authors:
  - Shirly Dana
categories:
  - Cuerpo
tags:
  - Salud 
  - Medicina 
  - Elefante Blanco 
  - Politicas Publicas 
  - Demolicion 
  - Hospital
date: 2020-08-10 00:00:00
comments: true
sticky: true
---
Era el primer viernes del mes de junio, pero aunque viernes, era también el primer día de la residencia, aquel lugar al que costó tanto llegar, pero al final encontré. Ese día tomé el 92 para el otro lado, el mismo colectivo que me acompañó durante gran parte de la adolescencia ahora se convertía en el bondi que usaría todas las mañanas para llegar al centro de salud. Salí con una hora de tiempo. Además de desconocer el recorrido y lo que llevaría, no quería llegar tarde. Al bajar, tendría que caminar alrededor de cinco cuadras. Escuchaba Artaud mientras caminaba, sonriendo con todo el cuerpo. 

Conocía el barrio, porque viví en Lugano hasta cumplir los 5 y mis tíos vivieron ahí hasta mis 15 años. En el camino atravesamos Mataderos, barrio que formó parte de casi todos los caminos que me trajeron hasta acá. Y cuando estaba casi por llegar, entre la neblina de las mañanas de otoño, estaba ahí. Enorme, mucho más grande de lo que yo había imaginado. Tan grande que tapaba los primeros rayos de sol. El día que fui a conocer el centro de salud, lo ignoré por completo. Sabía que existía, incluso estaba ya la película que hablaba del Elefante, pero aún así nunca había estado por ahí.

Arriba de las escaleras, en la antesala de la sala de espera, hay una reja que loa bordea, dejando un espacio abierto y comunicado con la planta baja. La reja, al igual que la baranda de la escalera, es roja, pero el color permanece escondido detrás de carteles de mil colores. No es casual que el primer cartel que llamó mi atención fue un rompecabezas con una foto del Elefante pegado no bien terminaba la escalera. Este rompecabezas estaba sobre una cartulina negra y, encima de él, había pequeñas notas de post-it con mensajes escritos a mano; “qué difícil cuando no estés”, no hay mucho más para decir, resume los recuerdos y también las emociones. Aquel día me resultó muy fácil empatizar con ese mensaje. Los recuerdos, los emblemas, difíciles de soltar. 

Mi compañero y yo tuvimos una clase introductoria ese día. El Elefante Blanco, además de ser un edificio abandonado, fue construido con el objetivo de convertirse en un hospital, el primer hospital en la comuna 8 de la Ciudad de Buenos Aires. Comenzó a construirse en el año 1938, pero nunca fue terminado y finalmente quedó abandonado. El edificio se convirtió en el hogar de miles de personas, en paredes de millones de casas a su alrededor y en un ícono del barrio, sin dudas. También funcionó el centro de salud que ahora está sobre la Avenida Piedrabuena. En el año 2011 comenzó el proyecto para su demolición, relocalizando a las personas que vivían en su interior y en las zonas aledañas. 

![Foto: Belén Hernández](/images/uploads/huella-de-elefante-1.jpg)

Aquel primero de junio, cuando llegué al barrio por primera vez, el edificio ya estaba deshabitado y preparándose para la demolición. La calle que llevaba al mismo era de tierra. Hacia la derecha había una valla armada con palos cuadrados de madera y una tela símil arpillera pero de color negro. Delimitaba el comienzo del terreno en donde estaba el Elefante del resto del barrio. Tenía algunos agujeros y también en una parte estaba totalmente desmontada, permitiendo el paso a través de ella. Mientras sucedía el día laboral habitual la mitad de nuestras ventanas daban al edificio, que semana a semana iba desapareciendo. En el barrio circulaban sensaciones encontradas, por les vecines relocalizades y por los posibles cambios que traería la nueva construcción. 

En Julio, cuando aún se jugaban partidos del mundial, apareció dentro de las máquinas, la bola negra que pendula. Ésa que sólo la había visto en los dibujos animados y en las películas formaba parte del escenario cotidiano del trabajo. Hasta que un día ya no se veía más el gris, sólo las sombras de las máquinas y la rapidísima construcción del nuevo edificio. Pero sobre todo, la gente. Me asombraba ver la felicidad con la que hablaban de la demolición. Esperaban que terminara pronto. Decían que era un lugar donde se juntaban les pibes y “hacían cosas malas” y que, además, podría caerse en cualquier momento. A mí me costaba, claro. No sólo porque era un ícono del barrio y soltar no se me da con facilidad, sino también por la representación que implicaba del sistema de salud. Un sistema que era nacional, que buscaba garantizar el acceso de la población a un hospital en la comuna, pero que, sin embargo, continúa marcando diferencias y profundizando su fragmentación en pedazos cada vez más pequeños.

Para el mes de octubre, ya no quedaban rastros del viejo Elefante y en el centro de salud se percibían los cambios. Era un tema de conversación entre mates, a la hora del almuerzo e incluso durante la atención. Circulaba también la esperanza de la nueva construcción y las implicancias que pudiera traer. Y es justamente ahí en donde empieza lo más interesante.

Casi tan rápido como sucedió la demolición, se construyó el edificio que alojaría al nuevo Ministerio de Hábitat y Desarrollo de la Ciudad de Buenos Aires. La calle que entra al barrio por el costado del centro de salud ocultó su tierra bajo unos adoquines nuevos. Cada cuadra que rodeaba el terreno del nuevo edificio se adoquinó. Al igual que su predecesor está vallado. Hay una reja metálica de dos metros de alto, que recorre cada una de las veredas del edificio. Adentro hay un parque. En él montaron juegos para niñes de todas las edades, canchas pequeñas de básquet y unas mesas de cemento cuadradas con asientos cuadrados para tomar mate. Esas mesas son grises, no las pintaron de colores. Son las pequeñas huellas que quedaron del Elefante. Con cada modificación que sucedía, se sumaba une policía al paisaje. Y no nos olvidemos de las rejas. La plaza también quedó atrapada tras las rejas, con horario de apertura y cierre. 

![](/images/uploads/huella-de-elefante-2.jpg)

Afuera del barrio también se sintieron los cambios. Primero fueron los carteles de las paradas e incluso las casitas. Todes sabíamos que el 80 y el 141 paraban juntos sobre Piedrabuena, pero ahora cada bondi tenía su propia casita. La parada del 103 y el 97 en Eva Perón no estaba marcada, pero todes sabíamos adónde esperarlos. Después empezamos a ver circular al 50, colectivo que pasaba por Lugano, pero no hacía ese recorrido. Nos reíamos de estas modificaciones, las volvíamos carne. 

Y eventualmente llegaron las personas que iban a trabajar en el Ministerio. Las diferencias se hacían cada vez más notorias, los códigos de vestimenta e incluso la forma de caminar, porque estamos sobre una villa, caminar estaba sobrevalorado. Veíamos gente bajar de los colectivos y correr hasta llegar al trabajo, a resguardarse en la jaula. 

Les vecines sonreían y hablaban de la ausencia de las ratas, del paisaje que ahora veían desde sus ventanas. Ya no había Elefante. Les trabajadores del centro de salud contaban con congoja aquellas épocas de la salita en el Elefante, mirando con los ojos vidriosos el espacio en donde estaba. Ya no había Elefante. No estaba ese edificio emblemático que hacía las veces de ícono barrial popular y de recordatorio de todo eso que iba a ser, pero nunca fue. Y están las calles asfaltadas, las cloacas en esa zona, están también los juegos, más colectivos y con mejor frecuencia. Resulta inevitable preguntarse por qué y para quiénes todas estas pequeñeces. 