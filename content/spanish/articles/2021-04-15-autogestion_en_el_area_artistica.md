---
title: Autogestión en el área artística
subtitle: "Una reflexión desde la música "
cover: /images/uploads/20201230_233235.jpg
date: 2021-04-15T20:05:06.887Z
authors:
  - karol Dinamarca
tags:
  - Cultua
  - arte
  - musica
  - grupo
  - autogestión
categories:
  - Cultura
comments: true
sticky: true
---
Sin duda, despedimos el año 2020 con un montón de ideas un poco inquietantes en nuestra cabeza. Entre ellas, dedicamos gran parte de nuestros pensamientos a entender un poco la Vida y la Muerte; a explicarnos a nosotros mismos el por qué de las pandemias, y de por qué murió tanta gente; buscamos información al respecto, y decidimos cuidarnos y proteger a los nuestros.

Probablemente, falleció algún ser querido o cercano debido al Covid 19, y entendimos lo importante que es pasar tiempo con nuestra familia y amigos. **También se cayeron grandes proyectos personales; cambiamos de dirección en cuanto a estudios o proyecciones profesionales, y aquí es donde quisiera detenerme.**

**Los artistas de mi país tuvieron que dar un giro muy rápido en cuanto a labores profesionales;** muchos se encontraron con contratos a honorarios finalizados, eventos cancelados, término de talleres musicales por necesidades económicas de las Municipalidades o entes responsables, etc. Y no quedó otra opción que comenzar a buscar oportunidades (o inventarlas al menos) para poder asi ganarse un sustento. Es aquí cuando viene la palabra mágica, la AUTOGESTIÓN.

**En cierto modo, cada artista debió buscar su propio camino, inclusive los que contábamos con cierta estabilidad laboral**. Meses atrás, se había anunciado un dramático recorte de presupuesto estatal hacia cultura, y una de las instituciones más afectadas sería la Orquesta Sinfónica Nacional de Chile. Finalmente, se logró dar vuelta esta situación, debido a una contra mediática de importancia y gestiones políticas internas. De esta forma, la Orquesta recupera su presupuesto, y ahora puede emprender y prepararse para los nuevos y grandes desafíos que conlleva este 2021. Mientras recibíamos la noticia del recorte presupuestario, algunos colegas y yo, nos planteamos la idea de crear un nuevo emprendimiento para contrarrestar la rebaja en los sueldos, o incluso, mirar hacia nuevos horizontes musicales.

**Sin embargo, me percaté de que un grupo muy grande de colegas músicos decidió emprender con ideas propias y creativas, creando contenido novedoso y de calidad, e hicieron de las redes sociales un muy buen uso de publicidad**. Algunos de ellos trabajaban desde hace mucho tiempo como artistas freelance, por lo tanto, intuyo que ya tenían un poco más de experiencia en época de vacas flacas. Uno de los cuartetos de música de cámara que ha llamado mi atención, por la positividad y proactividad a lo hora de buscar nuevos caminos, es el Cuarteto Austral, conformado por 4 talentosas jóvenes músicas chilenas, todas con estudios superiores de su instrumento. Han realizado colaboraciones importantes con grandes músicos chilenos, tales como Isabel Parra e Inti Illimani, y su actividad no se ha detenido, ni siquiera durante esta Pandemia Mundial. Más bien todo lo contrario; han realizado actividades musicales intensas, conquistando nuevos seguidores a sus redes sociales.

Otro gran acierto artístico, ha sido la creación de "La Flauta Mágica y el Exótico Cajón Perdido" Esta obra es es una producción realizada por diversos cantantes de la escena nacional, y en un trabajo en conjunto con el Teatro Municipal dieron vida a esta ópera para niños. Se escribieron guiones y se realizó un acabado vestuario personalizado para cada títere que participa de esta obra. Sin duda, "Calcetinovia" se ganó el corazón de muchos niños que permanecen en sus casas viviendo esta cuarentena.

Y existe otro grupo de cámara que llama mi interés, y por muchas razones; el gestor de este conjunto es un amigo violinista y colega de orquesta, llamado Ricardo González. Él, hace ya cinco años más o menos, decidió perseguir su sueño, y crear así el "Grupo Cámara Boecio".

**Esta agrupación de carácter independiente constituida por una orquesta y coro de cámara, comenzó su proyecto ofreciendo servicios musicales enfocados a diferentes necesidades, tratando de originar así trabajo remunerado para sus músicos.** Sin embargo, al vivir la dura realidad del músico chileno en Chile, al cual se le pide en muchas ocasiones que baje sus exigencias salariales o que definitivamente toque gratis, realizó un cambio en su gestión y proyección.

En vista de este nuevo escenario Grupo Cámara Boecio incluyó obras corales de gran relevancia e inició atractivas temporadas de música de cámara en iglesias, muchas de ellas gratuitas, incluyendo en la interpretación a su Coro de Cámara, siempre listo y dispuesto a colaborar. Tocaron en universidades, centros comunitarios y en todo lugar donde su proyecto tuviera cabida y allí encontraron el amor y reconocimiento de parte de todo el público que asistió a sus conciertos.

Muchas personas integraron durante breves temporadas este conjunto musical, pero hay otras que estuvieron desde los orígenes del grupo hasta el presente. Ellos han demostrado su lealtad a toda prueba y gran amor por la profesión, quedándose en el grupo, sabiendo incluso que muchas veces, no habrá remuneración por sus actividades. **En diversos momentos han postulado a distintos fondos de música, pero como suele ocurrir en estas postulaciones, la mayoría de las veces no se logra concretar la ayuda estatal por múltiples factores, perdiendo la opción de ganar algún aporte económico, pero esto nunca disminuyó sus ánimos de seguir aportando al rubro musical.**

Y así todos estos valiosos músicos, han sacado fuerzas de flaqueza para darle vida y paso a sus sueños, lo cual, ennoblece mucho nuestra profesión. Aprendieron nuevas herramientas de trabajo online, se enfrentaron a las dificultades técnicas de creación de contenido para redes sociales, difundieron su trabajo a través de todo tipo de canales de comunicación y trabajaron en conjunto para lograr así su finalidad: trascender en tiempos de pandemia.

**Mi intención jamás es "romantizar" / la autogestión, porque también conozco su lado oscuro, pero sí quisiera rescatar los mejores valores que se pueden obtener de ella, tales como la perseverancia, resistencia, y la disciplina** absoluta para ir en búsqueda de concretar tus proyectos, sean cuáles sean.

**La pandemia llegó para quitarnos algunas cosas, pero también nos dió paso y nos regaló tiempo para reflexionar sobre nosotros mismos, nuestras decisiones y acciones** : ¿qué estoy haciendo para vivir mi sueño? ¿qué necesito para alcanzar mis objetivos? Y quizás la pregunta más dura ¿lo que estoy haciendo es suficiente para lograr mis anhelos?

Me despido cordialmente de todos los lectores de la Revista Cuerpo y Territorio, deseándoles lo mejor para este año 2021, y especialmente, a todos los artistas chilenos, de norte a sur, que este año lo dieron todo por seguir viviendo de la música.

Columna dedicada con especial afecto a Grupo Cámara Boecio, y a su incansable gestor, Ricardo González.