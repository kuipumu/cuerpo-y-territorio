---
title: Breve reseña acerca de los feminismos y los cuidados de la salud
subtitle: "Un recorrido histórico del papel  de las mujeres en el cuidado "
cover: /images/uploads/whatsapp_image_2021-04-23_at_19.33.00_1_1.jpeg
date: 2021-04-30T15:01:39.897Z
authors:
  - Shirly Dana
tags:
  - Cuidados
  - Salud
  - Feminismo
  - Genero
  - Mujeres
categories:
  - Genero
  - Cuerpo
  - Territorio
comments: true
sticky: true
---


*"Fuego a todo lo que no nos deja ser"*

Otras maneras - BIFE



**Uno de los stickers que se van desgastando en el termo del mate es de color rojo y en letras negras dice “somos las nietas de todas las brujas que nunca pudiste quemar”.** En el centro tiene dibujado en blanco y negro una fogata con leños ardiendo. Esta no es la primera ni la última vez que vamos a hablar sobre la historia de las brujas de la Edad Media. En aquella época, como recupera el libro “La mujer de la ilusión” de Ana María Fernández, la medicina posiciona a las corporalidades femeninas como “varones fallados o incompletos”, arrastrando esta concepción desde la Edad Antigua. A esta definición por comparación se agrega la concepción cristiana, perpetuando la asociación de las mujeres con lo instintivo, lo irracional. 

Como decíamos anteriormente, en su discurso sostienen que las corporalidades femeninas son más débiles que las masculinas, por poseer un “sexo incompleto o no desarrollado del todo”. Además, consideran que las corporalidades femeninas están habitadas por el demonio, otorgándoles deseos sexuales insaciables. En el contexto social medieval este hecho las configuraba como fuente de constante tentación, que ponía en peligro la vanagloriada castidad de los varones. Estos eran algunos de los motivos que derivaron en la desconfianza desarrollada contra ellas y en su persecución. Con la confluencia de estos discursos se desprende la denominación de brujas a algunas de las feminidades que vivían en la época.

**Las brujas eran aquellas mujeres o feminidades que no entraban en la concepción de “buenas” y que se comportaban de forma revolucionaria para la época**. En su mayoría, eran pobres, no se casaban y realizaban tareas que en aquel entonces estaban vinculadas con roles masculinos, como trabajar fuera de los hogares, insertándose en el mercado laboral. Algunas de sus inserciones laborales tenían que ver con la posibilidad de brindar cuidados para la salud de las personas dado que, entre otros saberes, conocían cómo utilizar las plantas y hierbas para el desarrollo de remedios naturales. **Asimismo, contaban con el saber relacionado a la procreación, acompañando a las personas gestantes durante los embarazos, trabajo de parto, parto y puerperio, incluso luego durante la crianza de les niñes**. Conocían también el ciclo sexual femenino y, lo que no es menor, compartían estos saberes. 

Todas estas acciones y la realización de magia y hechicería eran consideradas “brujerías” y encuadradas en la “superstición femenina”. Además del ámbito laboral, su posicionamiento en relación con sus prácticas sexuales (extramaritales) y el autoconocimiento de su propia corporalidad continuaba alejándolas de esa definición de “buenas” que construía la sociedad de la época. Por último, estas brujas no actuaban de forma solitaria, sino que lo hacían en comunidad, viviendo autónomas por fuera de las tareas domésticas, desafiando de forma constante el rol que la sociedad medieval había designado a las feminidades. No resulta casual que gran parte de estos hitos fueron luchas llevadas a cabo por el movimiento feminista, conquistando la equidad entre los géneros en el ejercicio de la ciudadanía, en los espacios laborales e incluso en las relaciones vinculares, entre otras áreas.

Estos actos revolucionarios fueron los que motivaron la caza de dichas brujas, condenándolas a morir en la hoguera por practicar brujerías y por “salirse de la norma” socialmente aceptada de aquel entonces. Frente a esta situación, se monopolizaron los saberes asociados a la salud, unificando los mismos en la educación universitaria, a la cual sólo podían acceder los varones ricos de la sociedad. De esta forma, fueron justificadas la persecución y asesinato no sólo de las brujas, sino de cualquier cuadore o sanadore no formado en la universidad, no hegemónique.

En esta línea, en el libro El Calibán y la Bruja, Silvia Federici dice que **“la caza de brujas está relacionada con una nueva división sexual del trabajo que confinó a las mujeres al trabajo reproductivo”**, buscando reestructurar los roles sociales y familiares de las mujeres para satisfacer al patriarcado y al creciente capitalismo, que comienza a instalarse en el marco de dicha caza, entre otros hechos igual de sanguinarios. Continúa estigmatizando a esas brujas revolucionarias, que desafiaban el poder viviendo por fuera de los matrimonios y buscando insertarse en el mercado de producción. En ese contexto, de una u otra forma, se desarrolla el imaginario social y cultural que deposita en los roles femeninos habitar y cuidar la casa, la posibilidad de procrear y formar una familia, en tanto que los roles masculinos quedaban asociados a la fuerza de trabajo, con el fin de proveer para el hogar y las personas que lo habitan. 

En las décadas subsiguientes continuaron incesantes luchas para continuar conquistando derechos laborales para las mujeres y otras feminidades, continúa aún en tensión la obligación tácita de continuar ocupando los roles de cuidado dentro de la casa, volviendo la labor de las mujeres al menos doble, una de ellas no remunerada y poco reconocida. En relación a este punto, en el libro “Género y salud, las políticas en acción”, compilado por Débora Tajer, habla justamente de esta diferencia, exponiendo que si bien se logró aceptar el rol productivo, aún sigue sin legitimarse la redistribución de las tareas dentro del hogar.

**Estas inequidades entre géneros también continúan presentes en relación con la salud y los cuidados de la misma. El acceso a estudiar carreras en salud en principio fue diferencial y a favor de las masculinidades, pero con el paso del tiempo y la disminución (entre otras cosas) de la rentabilidad de dichas carreras terminó por inclinar la balanza hacia las feminidades.** La gran mayoría de inscriptes en carreras de salud son feminidades, perpetuando esas habilidades desarrolladas por las brujas y potenciandolas en torno al cuidado y al acompañamiento de las personas en sus padecimientos.

 Existe un acceso diferencial a los servicios sanitarios para las personas de los distintos géneros, así como también la diferencia existe en lo ofrecido por dichos servicios. Les profesionales de la salud, a lo largo de nuestra formación, buscamos saldar estas inequidades incorporando la perspectiva de género en nuestra atención como una de las herramientas que permiten construir puentes. 

En relación con este concepto, continuando con lo que expone el libro mencionado anteriormente, lo definen como poder “incorporar el modo en que las asimetrías sociales entre varones y mujeres determinan diferencialmente el proceso salud-enfermedad-atención” y cuidado de la salud. El abordaje de los cuidados de salud no puede ser igual para todas las personas, dado que la forma en la que se transitan los estados de salud, de enfermedad y la posibilidad de oscilar entre éstos está determinada por múltiples factores sociales, culturales económicos y también genéricos, desprendiéndose de esto que su abordaje debe ser individualizado y adecuado.

En relación al rol femenino en las sociedades de América Latina, recuperando lo expuesto en el primer capítulo del libro “Géneros y salud, las políticas en acción”, se encuentra en estrecha relación con las tareas de cuidados, volviéndose les principales cuidadores de su familia, así como también ocupando el rol de acompañar a las personas en el tránsito de las enfermedades y sus procesos individuales de sanación. Se esboza la relación existente entre estos roles designados y habitados por feminidades a las características que los imaginarios sociales depositan en elles, describiéndoles como seres empátiques, receptives y comprensives.

Como última reflexión, Pabla Perez en su libro “Ginecología natural”, describe en un capítulo entero cómo nuestras cuerpas y los órganos que las componen no parecen estar habitados por nosotres mismes. Hace un recorrido por los nombres, problematizando que los mismos fueron adjudicados por varones cis al descubrir las partes y llamarlas con sus apellidos, como las trompas de Falopio. Propone como un acto amoroso hacia nosotres descolonizar estas cuerpas para renombrarlas y renombrar a esos órganos que llevan consigo huellas patriarcales de personas ajenas a nuestra vivencia corporal, tanto colectiva como individual.

La reivindicación de las brujas en los movimientos feministas y transfeministas actuales está relacionada con la revolución, con la posibilidad de habitar esos roles sociales elegidos, más allá de aquellos designados. Esas brujas en su época fueron revolucionarias, rompiendo la norma socialmente impuesta y sentando las bases de muchas de las luchas que tuvieron lugar luego de esas. 

Ese no es el único sticker feminista que luce mi termo. Todos los que lleva lo son. De esta misma forma, el feminismo atraviesa mi vida, la de mis amigues, la de la sociedad actual en la que vivimos y nos desarrollamos profesionalmente. Hace más o menos cuatro años, en una marcha por el 8M, encontramos a una chica que tenía un cartel escrito a mano. No era el cartel más elaborado, no tenía dibujos ni tenía frases emblemáticas del feminismo. Este cartel tan sencillo y tan profundo decía sólamente “el feminismo me salvó la vida”. Mi amiga que la encontró entre la multitud, le pidió permiso para fotografiarla, las dos al borde del llanto. Y esto también es el feminismo, lo que hacemos con todo esto que nos transforma y nos sigue dando la fuerza para luchar por nuestros derechos, trascendiendo los géneros, edades y los roles que las sociedades capitalistas, neoliberales y patriarcales buscan imponernos.

### Referencias 

Fernández, A. M. (2012). La mujer de la ilusión. Paidós.

Tajer, D. (2021). Género y salud, las políticas en acción. Lugar.

Federici, S. (1998). El Calibán y la Bruja. Tinta limón.

Perez, P. (2015). Manual introductorio a la Ginecología Natural. Ginecosofía.