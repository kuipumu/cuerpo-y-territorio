---
title: "La República Bananera del siglo XXI "
subtitle: "   "
cover: /images/uploads/5288178.jfif
date: 2021-02-02T19:49:42.808Z
authors:
  - Valentina Mena Castro
tags:
  - Política
  - EEUU
  - RepúblicaBananera
  - Latinoamérica
  - Historia
  - Elecciones
  - DonaldTrump
  - JoeBiden
  - Actualidad
categories:
  - Política y Economía
comments: true
sticky: true
---
**El sueño de unión de las repúblicas americanas de George Washington comprendía una gran contradicción**; el colonialismo intrínseco a la idea de imperio reinante cuyo centro serían los Estados Unidos de América, se encontraba plagado de racismos, desconocimientos de las demás culturas y conocimientos ancestrales de los diferentes pueblos como los afro, los nativos americanos y los pobladores de territorios anexados.

Por una parte, se tiene a los esclavos provenientes de África, que con su mano de obra crearon una economía extractiva basada en el cultivo de trigo, tabaco, algodón, oro, entre otros. Por otra parte, los nativos americanos desde la llegada de colonos británicos sufrieron estragos en su población, siendo masacrados en su mayoría. Finalmente, es importante reconocer el papel de los territorios anexados como Luisiana, Texas, Oregón, Florida, Filipinas, las Islas Vírgenes, Alaska y Hawái que permitieron aparte de expandir el país territorialmente hablando, la exploración de nuevos climas y consigo nuevos mercados como hierro y carbón, al igual que el fortalecimiento de la empresa del algodón y el tabaco en los estados del sur basada en mano de obra esclava.

Cuando el país detiene su expansión territorial a principios del siglo XX, cambia su política exterior y pasa a un modelo de imperialismo enfocado en los productos culturales, al igual que una economía netamente capitalista y una agenda política más agresiva con los demás países; **esto causa un impacto social, político y cultural en toda Latinoamérica que define en los siglos XX y XXI los futuros del continente.**

El expansionismo de los Estados Unidos una vez independiente de Gran Bretaña, se caracterizó por la imposición de su modelo económico extractivista implantado en su territorio, en donde se destaca por ejemplo la fiebre del Oro en California entre 1848 hasta principios de 1960. Posteriormente, este modelo lo implanta en los países vecinos (y no tan cercanos directamente) a su centro, por medio de la creación de empresas de frutas como la United Fruit Company, fundada en 1899 y cuyo principal objetivo era la exportación hacia los Estados Unidos de frutas, principalmente el banano, de territorios como Honduras, Guatemala, Colombia, Ecuador, etc.

A raíz de la fundación de estas compañías, **varios estadounidenses migraron temporalmente a países de centro y sur América.** Durante esta interacción y como fruto del pensar ser una nación más avanzada en materia económica, tecnológica, cultural y democrática, el escritor William Sydney Portner, más conocido como O. Henry, escribe el libro “Repollos y Reyes” en 1904, mientras se encontraba exiliado en Honduras debido a la malversación de fondos que llevó a cabo en Austin, Texas. Allí crea una historia inspirada en sus vivencias en Honduras cambiando el nombre de este territorio por uno ficticio, la República de Anchurias, describiéndola de la siguiente forma:

**En la constitución de esta pequeña y marítima república bananera había una sección olvidada...**

El libro fue muy popular desde su lanzamiento y desde entonces se entendió de forma peyorativa **que una República Bananera era un lugar corrupto, políticamente inestable, empobrecido, atrasado, tercermundista, sin libertad, con un sistema democrático fallido y en el que su economía dependía de unos pocos productos de escaso valor agregado, en este caso las bananas.** Es así como este término se volvió un sinónimo cuya intención se enfocaba en resaltar el servilismo dentro de esta sociedad y las dictaduras que son características de estos territorios, cuyo único objetivo es la explotación de monocultivos sin preocuparse por el fomento de la educación, la empresa propia, la democracia, etc.

Esta idea de la inestabilidad política en Latinoamérica se reforzó durante la Guerra Fría, momento en el cual Estados Unidos necesitaba crecer y mantener su influencia en el mundo con el fin de mantener el sistema económico Capitalista en contra del Comunismo practicado en la Unión Soviética. Por esta razón, l**a “democracia” estadounidense decidió apoyar numerosos golpes de estado en América para así tener gobiernos títeres que les permitieran mantener su economía y su sistema económico estable.**

Es importante resaltar en este punto, **la gran ironía en la que incurrió y aún incurren los estadounidenses** en utilizar este término hacia los países latinoamericanos de forma despectiva, ya que aún en pleno 2021 es utilizado con la mayor ligereza posible. En la actualidad, la lucha por la presidencia de los Estados Unidos ha sido tensa, ya que por una parte, se tenía al expresidente Donald Trump a la cabeza del país y en busca de la reelección bajo ideales de republicanos, y por otra parte, se tiene al demócrata Joe Biden, con una carrera muy larga en la política y reconocido por ser ex vicepresidente de Barack Obama.

En este contexto, las opiniones se encuentran totalmente divididas, ya que la agenda política, económica, migratoria, de salud pública y ecológica se encuentran mediadas por la emergencia de la pandemia del COVID-19, **siendo estas las elecciones con mayor cantidad de votos enviados por correo en la historia de este país.** Los resultados preliminares de estas elecciones daban como ganador al demócrata Biden. Sin embargo, y alegando la gran cantidad de votos enviados por correo, el expresidente Trump requirió volver a realizar el conteo de votos.

Durante este periodo de tiempo entre 3 de noviembre de 2020 y enero 16 del 2021, se empezó una contienda por medios de comunicación, en donde el expresidente Trump en estado de negación seguía afirmando que él continuaba con su período presidencial a pesar de que las urnas demostraban lo contrario, al mismo tiempo, **se encontraron audios de Trump pidiendo que se falsearan los resultados para lograr la victoria en Georgia, estado fundamental para la presidencia.** A raíz de este ataque continuo en sus ruedas de prensa, como también en redes sociales, el 16 de enero se presentaban los resultados finales ante el congreso de la nación, encabezado por el exvicepresidente Mike Pence.

A lo largo de este día, Trump lanzó un comunicado bastante agresivo, denunciado que era un **atentado contra la democracia el no nombrarlo como presidente y que por ello, convocaba al pueblo que estuviese a su favor a pronunciarse.** A partir de aquí, los protestantes simpatizantes de Trump realizaron un acto histórico, allanaron el Capitolio Nacional con todos los congresistas dentro, a lo que solamente el expresidente respondió que apreciaba a los insurgentes pero que debían retractarse. Este hecho grabado en vivo fue una muestra clara de la inestabilidad política estadounidense, y de la fuerza de los grupos de extrema derecha como los Proud Boys, Oath Keepers, QAnon, entre otros.

De esta forma, **el país que predicaba ser el centro de la democracia se convierte en uno más de la lista en donde la extrema derecha ha causado desmanes en nombre de esta.** Así, afirmaciones de personajes como el expresidente George Bush *“así es como se disputan los resultados en una república Bananera”* cobran vida hoy, en un Estados Unidos con crisis económica, social, racial, política y de salud. Parece que, de una forma inesperada, el sueño de George Washington se lleva a cabo, ahora toda América incluyendo a Estados Unidos, **es una república… bananera.**

**Bibliografía: **

1) Henry, O .(1904) [Cabbages and Kings.](<http://www.gutenberg.org/files/2777/2777-h/2777-h.htm>)

2) RT (2019) [Los golpes de Estado apoyados por EE.UU. en Latinoamérica desde 1948.](<https://actualidad.rt.com/actualidad/306266-historico-golpes-estado-eeuu-america-latina>)

3) France 24 (2020) [El Colegio Electoral confirmó la victoria de Joe Biden en las elecciones presidenciales de EE. UU. ](<https://www.france24.com/es/ee-uu-y-canad%C3%A1/20201214-elecciones-eeuu-biden-presidente-trump-colegio-electoral>)[](https://www.france24.com/es/ee-uu-y-canad%C3%A1/20201214-elecciones-eeuu-biden-presidente-trump-colegio-electoral)

4) Gardner, A. (2021) [‘Solo quiero encontrar 11,780 votos’: Trump presiona al secretario de Estado de Georgia para que recalcule la votación a su favor. ](<https://www.washingtonpost.com/es/politics/2021/01/04/llamada-trump-grabacion-georgia-votos/>)

5) USA News (2021) [Transcript of Trump's Speech at Rally Before US Capitol Riot. ](<https://www.usnews.com/news/politics/articles/2021-01-13/transcript-of-trumps-speech-at-rally-before-us-capitol-riot>)

6) Raju, M. Barret,T. (2021) [US Capitol secured, 4 dead after rioters stormed the halls of Congress to block Biden's win. ](<https://www.cnn.com/2021/01/06/politics/us-capitol-lockdown/index.html>)
