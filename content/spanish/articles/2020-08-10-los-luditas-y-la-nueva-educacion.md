---
title: Los luditas y la nueva educación
cover: images/uploads/los-luditas-y-la-nueva-educacion.jpg
date: 2020-08-10 00:00:00
authors:
  - Roberto Salazar
sticky: true
comments: true
tags:
  - Educación
  - COVID-19
  - Confinamiento
  - Sociedad
  - Aprendizaje
  - Pandemia
categories:
  - Educación
description: Los luditas aparecen a principios de siglo XIX en Inglaterra como
  respuesta a la marcha incipiente de la Revolución Industrial.
---
Los luditas aparecen a principios de siglo XIX en Inglaterra como respuesta a la marcha incipiente de la Revolución Industrial. Tomando el nombre por un tal Ned Ludd, son recordados por sabotear o destruir las maquinarias de las industrias, en reclamo a un posible deterioro de las precarias condiciones laborales existentes por unos nuevos desarrollos técnicos. 

A pesar de que los luditas (lejos de estar organizados) también se oponían al uso de ciudadanos extranjeros para los oficios que ellos cubrían o de la masificación de la enseñanza del trabajo a nuevos aprendices, se mantuvieron como el epítome del rechazo al desarrollo tecnológico. 

Este antagonismo a la tecnología, que ha tenido partidarios y detractores desde entonces, parece haber recibido un importante revés con la nueva realidad del COVID-19 y las medidas de aislamiento tomadas por los gobiernos en la inmensa mayoría de países. 

Ha sido precisamente la tecnología lo que ha permitido, además de combatir al virus y evitar una contracción mayor de la economía mundial, mantener el funcionamiento de los modos de vida de la población a través de los dispositivos electrónicos y de la principal red de conexión actual: el internet.

El (mal llamado) aislamiento social, ha posibilitado maximizar el uso de la tecnología existente para encontrar nuevas formas de organización del trabajo, el entretenimiento, el consumo, el transporte, de las actividades productivas y no productivas. 

Esta suerte de aceleracionismo, tomando el planteamiento del politólogo Nick Srnicek, de extremar las capacidades que tienen los avances tecnológicos para poder avanzar hacia nuevas organizaciones y modos de vida permitiría sobrellevar crisis sanitarias que apuntan a no ser inusuales en el futuro.


La educación es un área donde las tecnologías vienen tomando espacio desde hace algunos años con resultados alentadores. La educación superior parece haberse beneficiado de la facilidad del internet para compartir investigaciones y hallazgos (no se puede obviar que de allí nace la iniciativa del World Wide Web), clases virtuales y a distancia, usos de simulaciones y materiales didácticos muy sofisticados e incluso utilización de redes sociales como complementos a entornos educativos (YouTube, Facebook). 

El aceleracionismo educativo en pandemia llega a cotas interesantes como los del escritor Jorge Carrión, en donde lleva a cabo un taller de escritura totalmente vía Whatsapp. 

La educación media y básica, entretanto, ha venido siguiendo otros derroteros. Sin menospreciar el énfasis en las tecnologías más innovadoras de los países asiáticos para sus estudiantes más jóvenes y en una masificación importante del uso del internet y plataformas virtuales en colegios en gran parte de los países desarrollados en Occidente, la educación sigue centrándose en la escolarización física (asistencia al recinto escolar) y el uso complementario de los desarrollos tecnológicos (subordinado a la lección impartida por el docente en el aula). 

El énfasis en la asistencia a una institución escolar obedece a, tomando en cuenta razones estrictamente educativas, el encuentro con los otros. El ir a la escuela no es solamente para aprender a leer, escribir y a hacer cálculos aritméticos como se pensaba hasta bien entrado el siglo XX, sino para el desarrollo socioemocional del niño. 

Es en el vínculo con el otro, en la presencia física con maestros y compañeros, en el aprendizaje de rituales de interacción (como decía el sociólogo Erving Goffman) en donde las habilidades sociales fundamentales para la vida pueden tomar forma. 

El desarrollo socioemocional de niños y adolescentes ha sido utilizado como argumento por educadores para descreer del uso de la tecnología como progreso de la experiencia educativa. 

Resulta una falsa dicotomía: no se trata de elegir entre uno y otro. Basta echar un vistazo a los modos de vida en la actualidad. No solo desde que el educador Marc Prensky que popularizó el término “nativo digital” y se observan niños aprendiendo intuitivamente a usar una computadora, si no también en la explosión de uso de redes sociales como TikTok durante la cuarentena para conectar con otros.

 El desarrollo socioemocional actual está mediado también por la conectividad vía internet, las redes sociales y el intercambio virtual. No solo dentro de las paredes de la escuela se aprende a estar con el otro. 
Como en los luditas, existen no pocos docentes que esconden detrás del desdén hacia la tecnología una tendencia a conservar los modos de educar. Hay implicaciones políticas, económicas y sociales que son indisociables al momento de pensar la educación.  

Pero los debates, que son necesarios para poder hacer reformas sostenibles y plurales, en ocasiones son superados por las contingencias de la realidad. Los educadores ahora se han visto obligados a enseñar on-line. Y no ha sido una decisión meramente educativa, si no han jugado factores de orden político, económico y de salud mental. Mantener las clases, en consonancia con esto último, resulta un sostén fundamental para conservar cierto aire de normalidad en la vida de aislamiento físico.

Pero los educadores deben orientarse por lo educativo y revaluar sus prácticas. NY no basta con la creatividad para generar interés en la enseñanza de contenidos (que no es poco), . Es rescatar desde la tecnología la experiencia por la cual se acudía a las aulas físicas: el intercambio con el otro y, la interacción con los semejantes. Resulta fundamental acelerar con el internet la proximidad al semejante, seguir desarrollando las habilidades sociales de los estudiantes, la cercanía de la relación alumno-docente y alumno-alumno. Es innegable que no sustituirá a la proximidad física, pero es necesario mejorar la proximidad virtual. 

Intercambios en grupos de Facebook o Whatsapp, edición de videos con un estilo similar a TikTok, uso de plataformas virtuales de intercambio como Zoom o Skype para aulas virtuales masivas. Los recursos están y cada vez más amplios.


La estética de la cuarentena ha sido los recuadros de los rostros de personas en Zoom por las reuniones de trabajo o webinars. El rostro resulta fundamental para detectar al otro, para reconocer lo humano, como nos plantean los estudios de etología, pasando por las neurociencias y por la filosofía (alegórica) de Emmanuel Levinas. 

La educación es también verse los rostros. Verse a la cara y conectarse con el otro a través de la pantalla, es el desafío que los profesores deben sostener en esta nueva educación. 

Los docentes, expertos en conocimientos educativos y cultivados en rituales de interacción, deben llevar su saber hacer, su savoir-faire, a los nuevos dispositivos. Sería llevar, al contrario de la famosa parábola, vino viejo ena odres nuevos. 

Serían unos luditas que valgan la pena.