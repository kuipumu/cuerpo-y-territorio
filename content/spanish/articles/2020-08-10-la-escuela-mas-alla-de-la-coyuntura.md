---
title: 'La escuela más allá de la coyuntura'
description: 'La puesta en escena de una continuidad pedagógica por otros medios ha expuesto con vehemencia que el imperativo de brindar a todxs lxs estudiantes la posibilidad de aprender se estrella con una pared de condiciones objetivas.'
cover: 'images/uploads/la-escuela-mas-alla-de-la-coyuntura.jpg'
authors:
  - Juan Manuel Barreto
categories:
  - Educación
tags:
  - Educación
  - COVID-19 
  - Confinamiento 
  - Sociedad 
  - Aprendizaje 
  - Pandemia
date: 2020-08-10 00:00:00
comments: true
sticky: true
---
La puesta en escena de una continuidad pedagógica por otros medios ha expuesto con vehemencia que el imperativo de brindar a todxs lxs estudiantes la posibilidad de aprender se estrella con una pared de condiciones objetivas.

La conexión a Internet no forma parte de un derecho ciudadanx, las dificultades e imposibilidades para acceder a dispositivos tecnológicos como computadoras y celulares reproducen asimetrías, el capital cultural y económico del cual disponen las familias para acompañar a lxs estudiantes en sus aprendizajes difiere abismalmente, la práctica docente combinada con tareas de cuidado deviene en pendiente de alta complejidad. Desigualdades sociales escondidas en la normalidad homogeneizadora de la escuela que irrumpen con una fuerza descarnada.

La coyuntura nos plantea considerables interrogantes y demandas que parecieran encaminarse a  trascender el corto plazo. 
La tarea de idear e implementar estrategias para sostener las trayectorias educativas durante este período de aislamiento social no invalida, por lo tanto, la necesidad de repensar la escuela y aquellas cuestiones que la atraviesan.

Un aspecto que es imprescindible dejar en claro es que el sistema escolar (en este caso, el argentino, pero podríamos trasladar la afirmación a cualquier otro país) no puede resolver de manera aislada e individual los problemas sociales que enfrenta la comunidad de la cual forma parte. La educación es una herramienta de transformación humana necesaria pero se manifiesta, también,  insuficiente por sí sola.

Es de suma importancia, consecuentemente, definir políticas públicas integrales que aborden la pobreza, la marginalidad, el desempleo y que consideren el impacto de estas problemáticas en la institución escolar. La escuela es, sin dudas, un sistema abierto, dinámico y  en constante interacción con su entorno. 

Sin ignorar lo expuesto, ¿cómo repensar el sistema escolar teniendo en cuenta que, a pesar de las numerosas críticas y embates recibidos a lo largo de su existencia, la escuela continúa siendo la mejor forma de enseñar masivamente?, ¿cómo generar las condiciones para que todxs los estudiantes puedan aprender?
En primera medida, promoviendo  la búsqueda de consensos y acuerdos  que involucren a lxs diversxs actorxs de las comunidades educativas (estudiantes incluidxs), dado que las decisiones tomadas en exclusividad por lxs supuestxs expertxs han solido ser, por lo menos, incompletas y sesgadas.

Por otro lado, convencidxs de que la escuela no es un dispositivo de acreditación sino un espacio pedagógico, de encuentro y socialización, de construcción de lo común.

Desarmando, a su vez,  esa creencia en torno al acto educativo que establece que lxs docentes debemos poseer respuestas ante todo lo que acontece en el aula, no habiendo lugar para los errores, para los espacios en blanco, para los paréntesis. Poner en tela de juicio esta inercia, interrumpir el binomio pregunta/respuesta, desactivar el mecanismo que nos impone comprenderlo todo es, de algún modo, la aceptación de nuestra propia vulnerabilidad y fragilidad.
No menos fundamental resulta desechar cualquier mirada que nos invite a interpretar los denominados fracasos escolares en función de las deficiencias de lxs estudiantes, desterrando los enfoques deficitarios que intenten explicar los problemas de aprendizaje a partir de las limitaciones del propix sujetx, cual rasgos o características que atentan contra su educabilidad. 

Adoptemos una perspectiva inclusiva, cuyo foco esté puesto en las dificultades que tienen que atravesar lxs estudiantes en su proceso de aprendizaje durante su estadía en la escuela, en ese intercambio entre el sujeto educativo y su ambiente. Aboquémonos a remover o limitar -retomando un concepto acuñado por Tony Booth y Mel Ainscow, profesores británicos de la Universidad de Cambridge y Manchester- las barreras de acceso para el aprendizaje y la participación escolar que operan sobre las condiciones de educabilidad de lxs niñxs y adolescentes.

Probablemente estos tiempos sean indispensables para que visibilicemos y cuestionemos las brechas sociales que se exhiben implacables en este presente que nos ha expulsado de las aulas pero, asimismo, aquellas barreras que han venido mimetizándose con la escuela, a tal punto de naturalizarse. Tal vez sean tiempos propicios para introducir nuevas preocupaciones, rediscutir sentidos en cada comunidad educativa, reforzar la noción de educación en términos de  bien social que la escuela distribuye y, sobre todo, como acto de resistencia a la reproducción de desigualdades.

¿Cuánto de lo que enseñamos interpela a nuestrxs estudiantes?, ¿cuál es la finalidad de la escuela?, ¿puede ser la escuela solamente futuro o el sentido de lo escolar no es trascendente,  construyéndose colectivamente en un marco espacio-temporal presente, único e irrepetible?