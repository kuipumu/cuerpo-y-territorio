---
title: Ciudadanos, ciudadanas, ciudadanes
subtitle: "23 de septiembre: Día del voto femenino en Argentina"
cover: /images/uploads/voto-femenino.jpg
date: 2020-09-23T13:37:22.268Z
authors:
  - Patricia Lepratti
tags:
  - VotoFemenino
  - Argentina
  - Voto
  - Mujeres
  - Equidad
  - Género
  - Política
  - Feminismo
  - Latinoamerica
  - Derechos
  - LoPersonalEsPolitico
categories:
  - Género
comments: true
sticky: true
---
**El 23 de septiembre de 1947 fue promulgada la Ley de voto femenino en Argentina.**

No se si a ustedes les pasa lo mismo que a mí, pero siempre me resulta sorprendente la cercanía histórica de esa fecha. Hace apenas setenta y tres años que las mujeres tienen formalmente permitida la participación y la decisión política en Argentina.

**Si además, tenemos en cuenta que el derecho al voto "universal" masculino fue sancionado en 1912 nos encontramos con treinta y cinco años de diferencia en el ejercicio de este derecho.**

Hace algunas semanas, leí un artículo de Ruth Lister (politóloga y activista del Reino Unido) titulado "Ciudadanía y Género" que fue el que me motivó a escribir esta breve nota en un mes en el que, además, fueron votadas la ley de paridad de género para los directorios de empresas y la ley de inclusión y un cupo laboral para travestis, transgénero y transexuales en Argentina.

En su artículo Lister plantea la pregunta, **¿La ciudadanía está mediada por el género?** Los números de más arriba nos estarían dando una respuesta a esa pregunta y son bastante ilustrativos de la diferencia - o mejor dicho, desigualdad - en la que hombres y mujeres han accedido a derechos sociales y políticos. Sin embargo, señala la autora británica, se ha tendido a ver esta diferencia como una "aberración histórica" que se considera actualmente saneada.

En este sentido, las denominadas leyes de cupo o de paridad de género, suelen despertar acalorados debates. De un lado se encuentran aquellos que sostienen que el rezago y la exclusión histórica de los ámbitos de representación política de la mitad de la población debe ser saneada por medio de normativas específicas y, desde el lugar opuesto, **se entiende que esas desigualdades ya han sido resueltas y que cada persona - sin importar su género - debe construir su trayectoria y sus espacios de participación política** de manera individual en base a sus propios méritos y esfuerzos.

Este último argumento se apoya en el ideal de igualdad ciudadana presente en las constituciones de todas nuestras naciones latinoamericanas. Sin embargo, este ideal viene siendo fuertemente debatido desde hace más de un siglo y - creo yo- hoy en día muy pocxs de nosotrxs pensamos que las posibilidades y modalidades de participación política de Pedro (un cartonero de José C. Paz) son iguales a las de Eva (una estudiante de Derecho que vive en la Ciudad Autónoma de Buenos Aires). Es decir que, tales debates han hecho visible, para gran parte de la población, **la incidencia que las desigualdades socioeconómicas tienen sobre el ejercicio de la ciudadanía.**

El feminismo, que en primer lugar luchó por el derecho formal a la participación y desición ciudadana de las mujeres, en una segunda etapa, ha venido haciendo lo propio por exponer como las adscripciones de género facilitan o limitan la práctica de ese derecho.

El género es una de las tantas dimensiones que atraviesa el ejercicio de la ciudadanía. Una entre tantas sí. Pero es la única que nos atraviesa a todxs. Por eso, antes de continuar me gustaría hacer una aclaración: **cuando hablamos de género, por lo menos en esta nota, estamos hablando de una forma de organización de las relaciones sociales basadas en el sexo.** Es decir, el género es una categoría relacional según la cual el trabajo necesario para la reproducción de una sociedad es dividido entre un actor social femenino y otro actor social masculino. Y esta es otra de las tantas causas del feminismo: evidenciar el carácter relacional e histórico, no natural, de los roles de género que, en tanto históricos, son pasibles de cambio.

Ahora bien, esta división social (no natural) del trabajo, también ha dividido, históricamente, la reproducción social en dos ámbitos: el público y el privado. Ambos ámbitos se definen en oposición al otro, o mejor dicho, en relación de oposición, uno frente al otro. De acuerdo con esta división, en el espacio público encontramos actividades como la producción, la política, etc. Mientras que, en el doméstico, se realizarían las actividades de cuidado y de reproducción de la vida.

En síntesis, si pensamos en relaciones de género, no solo estamos hablando del ámbito privado, sino también del espacio público y de las categorías con las que nos manejamos en ambos. ¿A donde quiero llegar con esto?, al hecho de que **la ciudadanía ha sido concebida, inicialmente, como una categoría masculinizada, desvinculada de las actividades relativas a la reproducción y el ciudado.**

“El ciudadano” de nuestras primeras constituciones no era un sujeto abstracto y universal que abarcaba a todos los seres humanos. Era un varón, blanco, propietario de tierras, que no estaba involucrado en las tareas productivas ni de cuidado. Los otros varones, no blancos y sin propiedades fueron incluidos en esa categoría luego de varias décadas de lucha en diferentes lugares del mundo. Como vimos, el derecho a la participación política de las mujeres fue reconocido aún más tarde.

Sin embargo cabe preguntarnos ¿esta ampliación al derecho de participación ciudadana ha transformado la forma en la que pensamos la ciudadanía? En otras palabras, **¿el concepto de ciudadano/ciudadana/ciudadane ha dejado de estar vinculado a una división sexual del trabajo según la cual lo público está separado de lo privado?**

**Este lunes 28 de septiembre tendremos una edición especial de Cuerpo y Territorio con motivo del Día Internacional de la Despenizalición del Aborto,** un tema que venía ocupando las agendas políticas y las calles de muchos de nuestros países antes de que los excepcionales acontecimientos de este 2020 nos metieran de lleno en el ámbito doméstico junto con actividades que antes pensábamos como exclusivas de lo público (trabajo productivo, sesiones parlamentarias, educación formal, etc.).

En la primera edición de la revista, el artículo de nuestra compañera Solmarena Torres nos habla sobre la “crisis de cuidado” profundizada durante la pandemia y sobre cómo la distribucion de tareas en el ámbito privado continúa siendo diferente a nivel social entre hombres y mujeres. **[¿Cómo nos repartimos las tareas? ](https://revistacuerpoyterritorio.com/2020/08/31/como-nos-repartimos-las-tareas/)pregunta Solmarena.** Y yo replanteo la pregunta: **¿cómo nos repartimos el tiempo entre lo público y lo privado, lo productivo y lo reproductivo? ¿Por qué la inclusión de las actividades productivas en el ámbito doméstico representó una crisis para las actividades de cuidado?**

La estrecha dependencia entre lo público y lo privado atraviesa, entonces, todos los escenarios (pre-pandémicos, pandémicos y seguramente post-pandemicos). ¿Es posible entonces seguir pensando la ciudadanía, los derechos y las obligaciones que nos asigna, sin pensar en su interrelación con las actividades reproductivas? **¿Podemos desvincular el ejercicio de la cuidadanía del derecho a decidir sobre cuándo y cómo gestar y cuidar?**

Otro de los grandes aportes de la teoría feminista a toda la teoría social ha sido el haberle puesto cuerpo a categorías abstractas como: trabajador, migrante, ciuadano…Los ciudadanos, las ciudadanas y les ciudadanes de hoy ya no podemos seguir pensándonos como cuerpos no gestantes o desvinculados del trabajo reproductivo. **Nuestros derechos y obligaciones como miembrxs de una sociedad deben tener en cuenta temas relativos a la reproducción de la vida que antes eran realizados por los no ciudadanxs, aquellos que eran excluidos de las eferas de decisión pública (mujeres, analfabetxs, personas esclavizadas, etc.).**

Mantener una visión de la ciudadanía no mediada por los vínculos entre la esfera pública y la privada, implica continuar sosteniendo un modelo de participación política que invisibiliza la reproducción de la vida como una actividad social que nos involucra a todxs. Es más, **una sociedad que habilita el derecho a la participación política a las mujeres, pero les niega su derecho a decidir cuándo gestar y cuidar es una sociedad que las mantiene en un plano de desigualdad en el ejercicio de ese derecho.**

**Hoy más que nunca "Lo personal es político".**



<!--EndFragment-->