---
title: '"El arte es mi refugio a orillas del mundo"'
subtitle: "  "
cover: /images/uploads/manos-tocando-guitarra_1098-16668.jpg
date: 2020-11-15T00:11:20.185Z
authors:
  - Florencia Cencig
tags:
  - DiaDelMusico
  - Cultura
  - Musica
  - Argentina
  - MusicaEnPandemia
categories:
  - Cultura
comments: true
sticky: true
---
¿Cuánto ritmo hay en nuestros días?, ¿qué imágenes resuenan en nuestra cabeza mientras escuchamos una canción?, ¿qué melodía nos permite viajar en el tiempo y volver a ese instante en el cual fuimos plenamente felices? o tristes, tal vez ¿Y esas letras?, esas letras que nos embadurnan de añoranzas y nos erizan la piel.

La música tiene el maravilloso poder de situarnos en cualquier lugar que nos imaginemos. Tiene esa pizca mágica que nos permite transportarnos a momentos, olores, sabores y personas que nos marcaron profundamente. Hasta tiene ese “qué sé yo”, que te vuela a otros universos en el preciso instante en que se abre el telón y tu banda preferida empieza a tocar. Y como si fuera poco, hasta puede llegar a ser sanadora.

Dicen por ahí, que noviembre es el mes de la música y, en medio de este 2020 pandémico y atípico, es imposible no preguntarnos: **¿Qué significa ser un artista en tiempos de pandemia?** Y quién mejor que Jorge Emilio Rodríguez, santiagueño de los pies a la cabeza y ciudadano de Termas de Río Hondo, para explicarnos qué implica ser un músico argentino en medio de este contexto de emergencia mundial.

Jorge creció rodeado de poesía y melodías. Parte de su adolescencia la transitó en la enorme Buenos Aires pero, actualmente, vive en la tierra que lo vio crecer. Como otros tantos artistas, ya hace varios meses que tuvo que resignificar su trabajo, y cambiar aquellas peñas, escenarios y callecitas llenas de turistas, a las cuales embellecía con su arte, por plataformas virtuales y recitales de streaming.

Si bien esta pandemia dejó a la luz el torbellino de injusticias que implica este sistema en el cual vivimos, uno de los sectores más golpeados fue el artístico ya que, históricamente, los profesionales de la cultura son invisibilizados por el Estado.

Pese a que el arte, en cualquiera de sus formas, está presente en todos los días de nuestras vidas como algo esencial, nunca se le otorgó el lugar y el reconocimiento correspondiente. Y esto se torna, más grave aún, para aquellos artistas autogestivos e independientes, como Jorge.

Sin embargo, él utiliza su voz, no solo para deleitarnos con su poética, sino para gritar y transmitir la compleja situación que están atravesando los y las trabajadoras. La cual se ve agravada por el contexto actual, pero violentada desde siempre.

¿Qué sucede con los presupuestos destinados a cultura?, ¿qué lugar tiene el arte en la currícula escolar?, ¿qué tipo de programas existen para promover el trabajo de artistas autogestivos? **¿Cómo se justifican los recortes presupuestarios en el ámbito cultural a nivel latinoamericano?**

La música se utiliza como herramienta para diversos tratamientos terapéuticos y curativos. La música es un recurso educativo que aparece en diferentes niveles de formación. La música está en las calles y acompaña los pedidos colectivos de la sociedad. La música une a la familia, a los amigos, a las personas. Le pone color a nuestros espacios de ocio, y se convierte en un lenguaje universal que conecta a gente de diferentes partes del mundo.

Por todo ello, Jorge, como muchos otros, manifiesta la urgencia de regularizar y gestionar políticas, en pos de los derechos de sus trabajadores, y decide poner en palabras este sinfín de interrogantes. Nos enseña que la música pertenece al mundo de los derechos culturales y, como tal, debe tener el reconocimiento pertinente del Estado.

Yo, mientras tanto, me cebo un matecito amargo y me saboreo esa zambita de fondo que este cantautor tituló “La nostalgiosa”, y los invito a conocer más de nuestra música argentina, de nuestro origen y de los artistas que reivindican nuestras raíces, a través de sus voces y sus acordes.

Además de esperar con ansias el primer disco solista de Jorge Rodriguez, podemos tener un adelanto de su trabajo en su[ canal de youtube](https://www.youtube.com/channel/UCSqJ18vErmYhFub2qS5s8BQ?view_as=subscriber) y en su IG [@_jorgerodriguezok](https://www.instagram.com/_jorgerodriguezok/?hl=es)

**22 de noviembre, día de las y los músicos. ¡Apoyemos a los artistas autogestivos e independientes!**

{{< youtube VqGAGi6SJ80 >}}