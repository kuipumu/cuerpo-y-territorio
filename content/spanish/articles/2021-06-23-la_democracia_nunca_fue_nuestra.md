---
title: La democracia nunca fue nuestra
subtitle: "Lectura breve del proceso electoral presidencial en el Perú "
cover: /images/uploads/czsodxi27hai7rfp6rcuurp7ry.jpg
caption: "Imagen El Universal "
date: 2021-06-23T16:14:26.621Z
authors:
  - Josefina Rodríguez
tags:
  - Peru
  - elecciones
  - Castillo
  - Keiko
categories:
  - Politica y Economía
comments: true
sticky: true
---
Acepté la invitación a escribir este corto texto sobre las elecciones en Perú sin darme cuenta de la enorme tarea que tendría en frente, al tener que resumir todo lo que ha estado ocurriendo en esta coyuntura. Y es así, reconociendo la humildad de mis palabras y el tremendo reto que acepté, que trataré de brindar una lectura rica, condensada y reflexiva para ustedes sobre lo que ha acontecido durante estos meses electorales. Pues bien, Perú ha llevado a cabo, este año, unas elecciones presidenciales inéditas, en parte por ser las elecciones del bicentenario (es en el año 2021 en el que el oficialismo celebra los 200 años de la independencia).

 Estas elecciones tan simbólicas y solemnes, sin embargo, han estado insertas en un contexto social turbulento y caótico, en una grave crisis multidimensional (laboral, educativa, económica, alimentaria, política, de salud). Esta crisis, cabe recalcar, fue encendida por una historia cruda de desigualdades estructurales, opresión y -como algunas antropólogas(1) acuñaron- de necropolítica; historia que fue puesta de manifiesto, revelada, con la catástrofe de la COVID-19.

Esta turbulencia se manifestó en la jornada de lucha a nivel nacional por la crisis política del mes de noviembre en el 2020, ocasionada por la repartija de la clase política en el congreso -que devino en la vacancia del segundo presidente en menos de cinco años y un golpe de Estado-, y que estuvo plagada de persecución, tortura policial y asesinato, esto último hacia nuestros compañeros Inti Sotelo Camargo (24 años) y Jack Bryan Pintado (22 años) (2) También, se exhibió en la inauguración del año del bicentenario con masivas protestas de obreras y obreros del campo (fundamentalmente, en los departamentos de La Libertad e Ica), de trabajadores de neolatifundios de producción agrícola para la exportación (uno de los sectores que ha tenido mayor impulso económico en el país a punta de explotación laboral). 

Estas protestas se dieron por condiciones subhumanas de trabajo en el campo y experimentaron un nivel de represión tan agudo que la policía asesinó a más compañeros: Jorge Yener Muñoz (19 años), K.N.R. (16 años) y Reynaldo Reyes Ulloa (26 años). También, llegaron a perseguir, golpear y entrar a los hogares de humildes trabajadores agrícolas, llegando a dañar a niños con bombas lacrimógenas y producir daños irreversibles en la población y sus viviendas.

Es así, pues, que recibimos la contienda electoral: con una masiva desconfianza hacia las instituciones de Estado, vistas, desde abajo, como espacios casi lúdicos, corrompidos y arcas de dinero para aquellos que acceden al poder; con una perspectiva lúgubre sobre lo que nos depara, en general, como país y con respecto a nuestras expectativas de vida; también, con duelos colectivos por el asesinato de nuestros compañeros por parte de la Policía Nacional del Perú (institución represora que carga con una oscura historia de brutalidad sistemática y criminalización de la protesta).

Así, ya aterrizándonos en las elecciones, al oficializarse aquellos partidos políticos que postularon al Poder Legislativo y el Poder Ejecutivo, nos dimos con la sorpresa de que la derecha, históricamente ganadora y unificada en este país, estaba dividida. Nuevos partidos de derecha y ultraderecha, como Victoria Nacional (3), Avanza País (4) y Renovación Popular (5), se asomaban como posibles postores (y favoritos en la capital, Lima Metropolitana), mientras que otros más tradicionales, como los alicaídos Partido Popular Cristiano (6)y Fuerza Popular (7) aparecían como opciones que muy pocos consideraban por la deleznable reputación que tenían. Acción Popular (8), Alianza para el Progreso (9) y el Partido Morado (10), entre otros, se presentaron como partidos algo más moderados, pero aún como parte del establishment y con muy poca popularidad detrás. En suma, pese a estar fragmentada la oferta de derecha, en el fondo seguían representando a la misma clase, aunque algunos, novedosamente, con un discurso más radical.

Desde izquierda, por su parte, dividida como siempre, se presentaban los partidos Juntos por el Perú, Frente Amplio (11) y Perú Libre (PL) (12), eminentemente (13)**.** Juntos por el Perú (JP) fue presentado como el partido preferido de la izquierda en los medios de comunicación (aunque aún con muy poca popularidad a nivel general), principalmente por recabar participación de muchos tecnócratas y renombrados académicos. Este partido, en aras de recoger mayor impulso hacia el final de la primera vuelta electoral, fue moderando su discurso en materia económica para “calmar” a la clase empresarial y apelando más a las clases medias, a tal punto que perdió fuerza y apoyo popular. 

Perú Libre, por su parte, era el partido de la izquierda con menores reflectores sobre sí; estigmatizado por la derecha y la izquierda liberal por tener un discurso mucho más radical en propuestas económicas, así como por tener una línea ideológica abiertamente socialista e ideas conservadoras sobre problemáticas sociales. 

También, por ser encabezado por el líder sindical, campesino, rondero y maestro de escuela pública, Pedro Castillo; y por el polémico ideólogo del partido, Vladimir Cerrón, ex gobernador regional de Junín sentenciado por corrupción y militante comunista conocido por su dogmatismo y conservadurismo. 

Quisiera anotar también que, a comparación de la derecha, los partidos de la izquierda sí representaban (y eran representados por) a clases diferentes. Esto en tanto JP, por ejemplo, estaba compuesto eminentemente por políticos de clase media y de Lima; mientras PL, por su parte, por clases populares y de provincias (Cajamarca, Junín, etc.). Los discursos, las agendas y el electorado, en ese sentido, eran diferentes.

El cierre de la primera vuelta nos dejó varias lecciones. Entre ellas, se reiteró lo poco que podíamos confiar en los medios de comunicación y las encuestadoras privadas, en tanto tejieron una narrativa que no tuvo, finalmente, correlato con la realidad. Por un lado, en los medios de prensa veíamos, escuchábamos y leíamos más a los candidatos de la derecha; y, con respecto a la izquierda, más fue invitada Verónika Mendoza (14)  a espacios periodísticos que Marco Arana (y ni qué decir de Pedro Castillo). 

Por otro lado, las encuestas arrojaban a George Forsyth o Yohny Lescano como los favoritos para la presidencia, inclusive. Esto, sin mencionar la compleja campaña de terruqueo (15), desde los medios de comunicación, hacia los candidatos de izquierda para censurarlos y, así, fortalecer a la fragmentada oferta contrincante. Los aplastantes resultados (16), en contraste, indicaron como preferido por voto popular al profesor Pedro Castillo (con un 18,9%) y su partido Perú Libre y, en segundo lugar -y con un amplio margen de diferencia- a la candidata corrupta Keiko Fujimori (con un 13,4%) y su organización Fuerza Popular.

Ahora bien, un punto importante que he omitido adrede hasta este momento ha sido la demanda por una nueva constitución, agenda surgida en la opinión pública a raíz de las jornadas de lucha en el territorio desde la segunda mitad del año 2020 (17). Una de las consignas más importantes del partido Perú Libre ha sido la asamblea popular constituyente para superar la actual constitución política del Perú, hecha en el marco de la dictadura de Alberto Fujimori (18)**.** 

Esta constitución ha abierto las puertas a décadas de flexibilización laboral, arrebato de derechos laborales y seguridad social, coaptación de territorios de comunidades campesinas y originarias, así como paquetazos políticos por la privatización de bienes comunes, entre otras medidas graves para la población (pero beneficiosas para la empresa privada). De ese modo, una de las agendas centrales de Perú Libre afronta directamente al establishment económico en el país, sin mencionar el hecho de que el discurso del partido a favor de una economía popular representa una amenaza directa hacia los intereses de la clase política, los grupos financieros y sus monopolios.

La estrategia del fujimorismo ha sido, pues, por un lado, defender el orden establecido por su padre, el orden que parió la constitución del 93 durante la dictadura, y la protección a los grupos económicos más grandes del país. A su vez, ha revivido -nuevamente- a uno de sus componentes ontológicos: el terrorismo, fenómeno sin el cual el fujimorismo no puede existir ni ser entendido (19).

 Esto, para construir al candidato Pedro Castillo, su partido Perú Libre y sus miembros como una amenaza frontal hacia la comunidad nacional, por ser los representantes del “terrorismo y el odio”. Pero, como si esto no fuera suficiente, también recogió la actual crisis política y social del país vecino de Venezuela, así como la precarización de muchos inmigrantes en el país, como arma política para atacar al contrincante y su agenda, desde el argumento de que sus propuestas “comunistas” (al margen de que no lo son) “solo traerán pobreza y caos al país, como en Venezuela”. 

Con esto último, podemos hacer un símil bastante curioso con lo que fue la campaña electoral del actual presidente ilegítimo de Colombia (o dictador), Iván Duque. Naturalmente, a la campaña de Fujimori se le aunó la clase financiera, dentro de la cual están los grupos El Comercio, Alicorp, Intercorp, otros dueños de grandes estudios de abogados e inmobiliarias y demás.

De ese modo, la segunda vuelta fue una campaña extremadamente violenta y agresiva por parte de la derecha, una derecha que escaló rápidamente al fascismo y con los medios de comunicación de su lado. Por parte de la izquierda, el frente tardó (demasiado, creo yo) en formarse, ya sea por discrepancias políticas en temas de inclusión social (20)o por la radicalidad en temas económicos de la agenda de Perú Libre, una demanda necesaria para muchas en el país.

 No quiero dejar de mencionar, sin embargo, que el racismo (21) no es ajeno a ningún espectro político y que el terruqueo también estuvo presente en el discurso de sectores acomodados de la izquierda. Así, el candidato Pedro Castillo estuvo fundamentalmente respaldado por la campaña popular y la movilización de bases.

Desde un discurso bastante próximo, sencillo y sincero, Pedro Castillo fue emergiendo más y más como un candidato del pueblo y para el pueblo. Partiendo desde su propia experiencia como campesino, rondero, maestro de escuela pública y sindicalista, hablando de sus propias carencias -que son las que vive la mayoría del país- y la urgencia de asegurar la calidad de vida para la población, fue calando en la opinión pública (a pesar de los intentos racistas de la prensa por desacreditarlo como poco preparado, ignorante o pro terruco). A partir de su trayectoria limpia de corrupción, fue perfilándose como la opción necesaria para afrontar el crimen organizado, encarnado por Fuerza Popular. 

Desde un complejo y constante proceso de articulación con organizaciones de base de diversos sectores de la sociedad, se fue garantizando la inclusión de diferentes agendas populares para su virtual gobierno. Así, se fue construyendo el masivo espaldarazo a Perú Libre, plataforma desde la cual Castillo fue impulsado para lograr, finalmente, la victoria en las elecciones presidenciales con un ceñido, pero potente margen de diferencia en la segunda vuelta (22).

Una victoria clara en casi todas las regiones del país a excepción de la costa, zona donde históricamente se ha concentrado el poder político, económico y tecnológico del Perú. Si bien hay demandas de comunidades que aún no son atendidas (como las afroperuanas, por ejemplo), así como aún hay agendas que disputar en torno a las disidencias sexuales, hermanos inmigrantes venezolanos, entre otras; muchas sostenemos un respaldo crítico hacia el candidato por el programa de lucha popular detrás, uno que está logrando tejer alianzas de clase en función a las desigualdades estructurales que nos condenan a la precariedad y que van más allá de lo institucional. Entre tanto, nos mantendremos vigilantes sobre su acción o estrategia política en el devenir de su gobierno para ver si, durante el proceso, finalmente cumple con la defensa de la demanda popular.

Aún así, se nos avecina una de las etapas más críticas a nivel social y político hasta el momento, desde los años 2000. A nivel institucional, en el parlamento, ya tenemos una coalición de derecha considerable (con 24 escaños para Fuerza Popular, 13 para Renovación Popular, 7 para Avanza País, sin contar los 16 de Acción Popular y los 15 de Alianza para el Progreso), que desde ya promete ingobernabilidad y confrontación directa hacia el Ejecutivo (23)

A su vez, el periodo catastrófico de la pandemia nos ha radicalizado al develar las contradicciones de la promesa neoliberal y esta coyuntura electoral ha servido de acelerador; y sigue siendo un proceso que involucra tanto a quienes son reconocidos como ciudadanos oficiales, como a los que no. Las elecciones, a su vez, han puesto de manifiesto el carácter profundamente racista y desigual de la política peruana, así como el rol de los medios de comunicación concentrados, que fungen de aparato propagandístico por la formación de sentidos comunes en pro del mantenimiento del modelo económico en el territorio. La arena política a nivel institucional, en ese sentido, ha puesto en tela de juicio el verdadero carácter de lo que significa la democracia en el Perú, así como de quiénes tienen la legitimidad de participar en ella y quiénes no.

Mientras escribo estas palabras, las instituciones oficiales del país aún no reconocen pública y legalmente a Pedro Castillo como el candidato ganador de la carrera electoral del 2021. Fundamentalmente, por el desconocimiento del proceso por parte de la candidata Fujimori, quien, gracias al apoyo de millonarios estudios de abogados de Lima (24), está buscando judicializar (y hasta anular) los resultados de las elecciones (25). 

Actualmente, su equipo legal está buscando apelar ante las instancias oficiales para retrasar la proclamación de Castillo como presidente electo y seguir construyendo una falsa narrativa de fraude. A su vez, sus tradicionales y nuevos seguidores levantan el reclamo por la participación de las Fuerzas Armadas para perpetrar un golpe de Estado en nombre de “la democracia” y por su candidata. 

Mientras escribo estas palabras, múltiples organizaciones de base, como la Central Única de Rondas Campesinas del Perú (CUNARC); la Organización Nacional de Mujeres Indígenas Andinas y Amazónicas (ONAMIAP); la Federación Nacional de Mujeres Campesinas, Artesanas, Indígenas, Nativas y Asalariadas del Perú (FENMUCARINAP); la Asociación Interétnica de Desarrollo de la Selva Peruana (AIDESEP); la Confederación General de Trabajadores del Perú (CGTP), entre muchas otras más, se han pronunciado y se siguen autoconvocando en la ciudad de Lima (frente al Jurado Nacional de Elecciones, entidad encargada de sacramentar los resultados oficiales) por defender el voto popular y enfrentarse a lo que, en el fondo, es el producto de siglos de opresión económica y racial, y se expresa en una jugada sucia por eliminar sus demandas por un cambio total y desconocer su legitimidad como sujetos políticos y de derecho.

Si bien trabajadoras y trabajadores populares, asalariados o informales, mujeres obrando por seguir manteniendo los barrios con alimento y cuidado a raíz de las crisis, comunidades originarias y defensores del territorio continúan sufriendo las consecuencias de la marginalización, racialización y el empobrecimiento por mantener a flote lo que para otros (muy pocos) es estabilidad económica, sí podemos decir que las elecciones presidenciales del bicentenario han sido desbordadas por la agenda popular que manifiesta la necesidad de una vida digna -por tener agua en los hogares, por mantener a las familias bajo un techo, por acceder a educación de calidad, por tener alimento sobre la mesa, por la soberanía de los pueblos sobre sus territorios. Esta es una agenda que nos seguirá impulsando, a mediano y largo plazo, para fortalecer y renovar la organización de base y los frentes clasistas a por la reivindicación de derechos que nos han sido arrebatados, más allá del marco institucional y formal del Estado, con esperanza y solidaridad, y desde abajo. Esta es una agenda que, innegablemente, nos hace recordar que, en el Perú, a sus 200 años de vida republicana, la democracia nunca fue nuestra.

#### Referencias

1 Como Maria Eugenia Ulfe en el webinar Comunidades andinas y nuevas convivencias sociales: Aportes desde una antropología remota y a distancia o Silvia Romio en el texto Necropolítica: historia de la compleja relación entre Amazonía peruana, epidemias y salud pública

2 De hecho, la imagen de la Policía Nacional del Perú (PNP) estaba tan desacreditada, producto de la brutalidad sistemática contra el pueblo y el mediatizado crimen cometido hacia Inti y Bryan, que realizaron un operativo para alzar su reputación, denominado “Olimpo”, supuesto operativo de combate contra el terrorismo. A partir de este, la PNP encarceló a 70 personas, aproximadamente, bajo la sospecha de “terrorismo”. Ante las críticas, el jefe de la Dircote (Dirección Nacional Contra el Terrorismo - PNP), en ese entonces, Óscar Arriola, defendió el operativo, sosteniendo que el delito cometido era de peligro abstracto (ver Planes y culpas del operativo Olimpo de la revista Ideele).

3 Liderado por el ex alcalde de La Victoria y denunciado por violencia doméstica, el candidato George Forsyth

4 Liderado por el ex asesor económico del dictador, genocida y convicto Alberto Fujimori, el candidato Hernando de Soto.

5 Liderado por el dueño de la multimillonaria empresa de transporte Inka Rail y católico ultra conservador, el candidato Rafael López Aliaga

6 Liderado por el conservador candidato Alberto Beingolea, quien intentó -sin éxito- moderar el discurso de su partido

7 iderado por la hija de Alberto Fujimori, íntimamente relacionada con el narcotráfico e investigada por el Ministerio Público por lavado de activos y enriquecimiento ilícito, la candidata Keiko Fujimori

8 Partido que operó el golpe de Estado de noviembre del 2020 (que llevó al poder a su congresista, Manuel Merino de Lama) y liderado por el ex congresista acusado de hostigamiento sexual Yohny Lescano

9  Liderado por el dueño de la Universidad César Vallejo y otras empresas educativas con fines de lucro, el candidato César Acuña

10 Liderado por el candidato Julio Guzmán; este es un partido que, en el discurso, se presenta como uno de centro, pero que, en la práctica, es de derecha (recordemos la postura represiva del gobierno de Sagasti frente a las protestas del agro en diciembre del 2020 y enero del 2021)

11 Liderado por el candidato Marco Arana, ex congresista y organizador de diversas protestas en Cajamarca contra el proyecto Conga de la minera Yanacocha; el partido no cuenta con mucha legitimidad después de su desempeño durante el periodo 2016-2020 y frente a la vacancia de Martín Vizcarra

12 Liderado por el candidato Pedro Castillo, dirigente sindical conocido por liderar la histórica huelga magisterial del año 201

13 Los partidos Democracia Directa y Renacimiento Unido Nacional son leídos por algunos como de izquierda, pero no se autodenominan como tales en sus idearios ni emplean plenamente discursos de izquierda

14 Candidata del partido Juntos por el Perú y ex congresista del partido Gana Perú

15 En el Perú, se le dice terruqueo a la práctica de acusar a alguien de ser terrorista, promover el terrorismo o tener vínculos con el terrorismo con la intención de revocar su legitimidad social o política, y de desprestigiar a la persona que tenga un discurso oponente y desafiante hacia la derecha o el status quo; sobre todo, se practica hacia personas racializadas. Es una estrategia censuradora que instrumentaliza el dolor histórico del Conflicto Armado Interno -periodo de guerra interna entre el Estado, Sendero Luminoso y el Movimiento Revolucionario Túpac Amaru durante 30 años, aproximadamente.

16  Con 17 millones de votantes en total, aproximadamente

17 En la transición del 2000, cuando el Fujimorismo cayó, se alzaron muchas voces por una nueva constitución, aunque desde los sindicatos y organizaciones populares más politizadas.

18 Esta constitución fue hecha por una asamblea constituyente con mayoría simple del partido oficialista y con un control amplio (a punta de presión o sobornos) del resto de partidos políticos, cada uno con una cantidad ínfima de escaños, entre 1 a 4 por partido (para un total de 80 escaños). De la mano con la constitución de Pinochet en Chile, la constitución de Fujimori es una de las más desiguales en cuestión de derechos sociales y es el germen legal de la instauración del neoliberalismo en el Perú.

19 Esto en la medida en que la tradición “fujimorista”, como una forma de hacer y participar en política, se funda en uno de los mitos más grandes alrededor del dictador Alberto Fujimori, que sostiene que él “derrotó” al terrorismo

20 Uno de ellos, por ejemplo, es el descarte de la aplicación del enfoque de género en la currícula nacional; o los discursos violentos con respecto a las disidencias sexuales por parte de varios miembros del partido

21 Para esto, quisiera recordar puntualmente cómo es que el integrante del equipo técnico de Juntos por el Perú, Pedro Francke, compartió un fragmento del periodista César Hildebrandt en el que se acusaba al partido Perú Libre de ser una izquierda telúrica y aldeana, tan irrelevante que no terminarían, ni siquiera, en un pie de página.

22 Con más de 18 millones de votantes en total -un millón de personas más salieron a votar a comparación de la primera vuelta-, Castillo se llevó la victoria con un 50.1%.

23 Similar o peor, diría yo, a la de los últimos cinco años. No quiero dejar de mencionar el temor que muchas tenemos de que, en el transcurso de su gobierno, el proyecto político o el candidato se moderen ante la presión de la derecha y los grandes grupos económicos, o que este se desentienda de las demandas populares y los compromisos asumidos, tal como ocurrió en el gobierno de Ollanta Humala durante el periodo 2011-2016.

24Muchos vinculados con las movidas corruptas de la empresa Odebrecht

25 Desde la estrategia la anulación de actas (o mesas electorales) de zonas rurales y con eminente población campesina u originaria, inicialmente, y, posteriormente, con denuncias penales por fraude, etc.