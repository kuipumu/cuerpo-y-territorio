---
title: "Foro: Los feminismos latinoamericanos Una mirada desde (Nos) Otras"
subtitle: "Reseña a propósito del espacio de reflexión "
cover: /images/uploads/whatsapp_image_2021-05-31_at_8.28.28_pm.jpeg
date: 2021-06-01T00:06:30.898Z
authors:
  - Patricia Lepratti
tags:
  - Feminismos
  - Genero
  - experiencia
  - foro
  - Latinoamerica
categories:
  - Genero
comments: true
sticky: true
---
El pasado miércoles 5 de mayo tuvo lugar el Foro **Los feminismos latinoamericanos: Una mirada desde (Nos) Otras**, transmitido en vivo desde el canal de YouTube de la Revista Cuerpo y Territorio, disponible en el siguiente link: [https://www.youtube.coiDm/watch?v=NphkOLXqI](https://www.youtube.com/watch?v=iDNphkOLXqI)

Dicho foro encontró de manera virtual a las representantes y activistas de organizaciones feministas de diversos países de la región latinoamericana, que habían participado del Dossier sobre feminismos latinoamericanos disponible en la página web de la Revista: <https://revistacuerpoyterritorio.com/2021/04/30/editorial-dossier-los-feminismos-latinoamericanos-una-mirada-desde-nosotras/>

El Dossier, compilado y editado por nuestra compañera Niyireé Baptista, reúne doce artículos escritos por mujeres de Argentina, Brasil, Chile, Colombia, Honduras, México y Venezuela, con el objetivo de mostrar la actualidad de las luchas de las organizaciones de mujeres y feministas, sus experiencias, su historia y sus agendas, entendiendo las particularidades y divergencias de cada territorio.

Del mismo modo, el Foro **Los feminismos latinoamericanos: Una mirada desde (Nos) Otras,** buscó ser un momento de encuentro, intercambio y puesta en común de las temáticas desarrolladas en cada uno de los artículos que conforman el Dossier.

Participaron del Foro: **Shirly Dana y Brenda Berenstein (Argentina); Juliana Rincón Flores (Colombia), Constanza Vega y Jessabel Guamán (Chile), Lídice Ortega (Honduras), Tatiana Jiménez (México), Las Comadres Púrpuras (Venezuela) y** **Mireya Dávila** (historiadora venezolana residente en Argentina).

Moderaron el encuentro nuestras compañeras columnistas de la Revista Cuerpo y Territorio: **Niyiré Baptista, Valentina Mena y Verónica Vázquez.**

El foro contó con un cierre musical a cargo de otra de nuestras compañeras columnistas, **Karol Dinamarka**, con su interpretación de **"Sicilienne para violín y piano”**, de María Theresa von Paradis, que pueden disfrutar aquí: https://www.youtube.com/watch?v=xC5pWEmA3wg

Las primeras en tomar la palabra fueron las compañeras de Argentina **Shirly Dana y Brenda Berenstein** quienes, entre otros temas,subrayaron la relevancia de la primera marcha del movimiento “Ni una menos” el 3 de junio de 2015 en repudio a los femicidios y toda forma de violencia de género, como un hito en la historia del feminismo en Argentina. La consigna “Ni una menos” puso en todas las mesas familiares el debate sobre la violencia contra las mujeres. Del mismo modo, desde entonces, varias reivindicaciones feministas como el derecho a decidir el momento de gestar y maternar, fueron incluidas en la agenda del Estado argentino, llegando a sancionar la legalización del aborto en diciembre del 2020. A pesar de que la violencia por razones de género continúa siendo un tema preocupante en Argentina, donde una mujer fue asesinada cada 29 horas el año pasado, Shirly y Brenda ven con esperanza el involucramiento de las nuevas generaciones en las diversas organizaciones feministas y de mujeres, así como la extensión de la Educación Sexual Integral en los centros de educación primaria y secundaria del país.

Luego fue el turno de **Juliana Rincón**. De acuerdo con el programa, esperábamos una exposición de su experiencia como activista ecofeminista en el Putumayo colombiano. Sin embargo, visiblemente angustiada, desde una Bogotá militarizada y con caceroleos como sonido de fondo, Juliana nos describió en primera persona la situación de violencia y represión policial vivida en Colombia desde finales del mes de abril en el marco del Paro Nacional convocado para manifestarse frente a un conjunto de reformas tributarias, laborales, educativas y de salud promovidas por el gobierno colombiano. Para una lectura detallada del testimonio de Juliana sobre la situación en Colombia, pueden visitar el siguiente link: <https://revistacuerpoyterritorio.com/2021/05/07/soscolombia-en-primera-persona/>

Conmividxs tras el valiente testimonio de Juliana, continuamos con la participación de las compañeras de Chile, que vivieron violencias similares durante el mes de octubre de 2019.

**Constanza Vega y Jessabel Guamán** señalan que tras el estallido social de octubre 2019 en Chile, se han llevado al debate público nociones jurídicas que no contemplaban correctamente los abusos y la violencia sexual incluidas en las prácticas de represión policial y militar contra las manifestantes, dando cuenta de lo normalizada que está la violencia ejercida contra las mujeres en la sociedad chilena.

Ante la pregunta sobre cuáles son las principales demandas y dificultades de los feminismos en Chile en la actualidad, las compañeras hicieron referencia al proceso constituyente que está teniendo lugar en el país. En ese contexto, los movimientos feministas han elaborado sus estrategias de lucha en dos frentes. El primero tiene como interlocutor al Estado, mientras que los movimientos feministas autónomos prefieren continuar su lucha por fuera de las instituciones estatales. En este sentido, si bien las demandas por una participación mayor de las mujeres en cargos públicos esperan ser incorporadas en la nueva Constitución, la despenalización del aborto, es un tema que no ha podido ser incluido en el espectro político chileno, ya sea de izquierda o de derecha. Por lo tanto, reivindicaciones como el derecho al aborto legal deben ser canalizadas a través de otras estrategias.

Constanza y Jessabel comparten con Shirly y Brenda una visión esperanzadora sobre el futuro y las nuevas generaciones. “La educación en perspectiva de género y la educación sexual, es una bandera de lucha. Somos docentes y queremos construir una mirada no sexista”. También reafirman la necesidad de considerar los movimientos de mujeres indígenas y migrantes sumando interseccionalidad a la mirada feminista, incluyendo otras dimensiones como el origen étnico y nacional a la lucha contra las desigualdades de género.

**Lídice Ortega** desde Honduras realiza una síntesis de la situación de pobreza y violencia extrema que ha venido sufriendo su país, especialmente desde el último golpe de estado en el año 2009. Esta situación extrema se ve reflejada en las caravanas de migrantes que parten desde Honduras hacia los Estados Unidos desde 2018, dando cuenta de la falta de esperanza sobre la posibilidad de un mejor futuro para lxs hondureñxs en su tierra. Consultada sobre la agenda feminista en un escenario tan desolador, Lídice responde que uno de los focos principales de trabajo es asegurar a las mujeres una vida libre de violencia. Las consecuencias del aislamiento llevado adelante por los gobiernos para reducir los efectos de la pandemia de la Covid-19, han resultado en un recrudecimiento de la violencia contra las mujeres en ámbitos domésticos. En su relato, Lídice nos cuenta como muchas mujeres son perseguidas por acompañar a otras mujeres víctimas de violencia. En este contexto, las demandas por los derechos a la igualdad de género se suman a la de otros derechos humanos como el del acceso a la justicia.

En su participación, **Tatiana Jiménez** desde México denuncia el recorte de recursos públicos, en especial aquellos destinados a la ayuda hacia mujeres, por parte del gobierno de López Obrador. Los movimientos feministas y de mujeres en México han logrado llevar al estado muchas de sus reivindicaciones, como la paridad de género en cargos públicos, y la legalización del aborto en alguno de sus estados. No obstante, la reducción del gasto público atenta contra los derechos adquiridos y, por lo tanto, la agenda actual está marcada por la resistencia. Los movimientos feministas latinoamericanos necesitan estar en constante vigilancia. Asegurar en la práctica derechos adquiridos por ley es un gran desafío.

Si bien en materia de leyes México se encuentra en un lugar de reconocimiento a nivel de las instituciones internacionales, también registra el triste número de **11 *feminicidios* diarios**. Tatiana defiende el uso del término ***feminicidio*** ante el de ***femicidio***. De acuerdo con Marcela Lagarde ***feminicidio***, no sólo hace referencia a la matanza sistemática de mujeres, sino también, a la inactividad, el silencio, la omisión y la negligencia por parte de los estados, en la lucha contra estos crímenes y sus autores.

Esta lucha contra la violencia, deja poco espacio a las demandas relativas a las tareas cuidado y su lugar como trabajo fundamental en la reproducción de las sociedades. Sin embargo, Tatiana comparte con las compañeras del foro su esperanza en las nuevas generaciones para una distribución más equitativa del trabajo de cuidado y el cese de la violencia por razones de género.

Desde Venezuela, representando a las Comadres Púrpura, la **comadre Antumbra** nos cuenta que la organización surgió en un contexto de recrudecimiento de la desigualdad social, de pauperización de los salarios y de la emigración como único horizonte posible de mejoramiento de las condiciones de vida. Desde 2014 las Comadres se reúnen para escucharse y acompañarse entre tanto dolor, pero también para buscar respuestas y soluciones desde la experiencia del poder creador femenino. Con este objetivo las Comadres se han ido articulando con otras organizaciones, buscando que las luchas sociales puedan tejerse en conjunto. Se auto- denominan “las Comadres”, haciendo hincapié en su carácter de voz colectiva, en oposición a la individuación de la vida propuesta/impuesta por los poderes hegemónicos.

Como en las intervenciones anteriores, la comadre Antumbra denuncia que la violencia hacia las mujeres ha venido creciendo en Venezuela, acompañada por la inacción de un gobierno que si bien, se dice socialista, muestra actualmente su cara liberal y autoritaria, reprimiendo a las organizaciones sociales que se levantan para exigir sus derechos. En el marco de todas estas violencias, la violencia hacia las mujeres queda invisibilizada. Un reflejo de ello es que en el país desde 2017 no se publican cifras oficiales sobre femicidios.

Sin embargo, para las Comadres, resulta esperanzador ver que cada vez surgen más movimientos sociales en reclamo de derechos, la protección del medio ambiente, el acceso al trabajo, etc. En esas luchas las mujeres tienen una voz propia que se puede usar para mejorar las condiciones de vida. Acompañar es también una forma de resistir.

Finalmente, **Mireya Dávila**, historiadora venezolana residente en Argentina, nos habla sobre la importancia de integrar el tema de las migraciones en las agendas feministas. Es necesario considerar cómo el género impacta en la decisión y en la experiencia migratoria. Mireya analiza específicamente el caso de Argentina, que es el país de Sudamérica que más recibe personas migrantes. Del total de estas personas, más de la mitad (54%) son mujeres. En ese desplazamiento que es la migración surgen algunas nuevas identidades y otras se resignifican: migrante, trabajadora, mujer. A veces se viven situaciones de empoderamiento respecto de su lugar de origen (la liberación de situaciones de violencia, la independencia económica) y otras veces, se experimentan nuevas dimensiones de discriminación y desigualdad social (xenofobia, racismo, sobreexplotación laboral, abuso por parte de los funcionarios fronterizos). En síntesis, la migración está atravesada por desigualdades estructurales del patriarcado y por ello debe ser parte importante de las agendas y preocupaciones feministas.

**El cierre:** Si bien las preguntas que guiaron el foro buscaban dar cuenta de las particularidades de cada territorio y cada lucha, resulta notable los puntos en común que aparecieron entre cada una de las participaciones, tal es el caso de la violencia ejercida hacia las mujeres en contextos de pandemia, de violencia institucional y hasta en aquellos que podríamos denominar “normales”.

Acompañando el mensaje de esperanza compartido por cada una de las foristas a pesar de los difíciles contextos atravesados por todos y cada uno de los países latinoamericanos, cerramos el foro con la presentación musical a cargo de nuestra compañera columnista de Cuerpo y Territorio Karol Dinamarka.

Agradecemos y felicitamos a todxs quienes han hecho posible tanto el dossier como el foro, reafirmando el carácter profundamente latinoamericanista de la Revista Cuerpo y Territorio. Una vez más compartimos nuestras tragedias, nuestras esperanzas y nuestras luchas. Hoy más que nunca no estamos solas.

Les invitamos a leer los artículos de las compañeras participantes del foro en el siguiente link:

<https://revistacuerpoyterritorio.com/articles/>