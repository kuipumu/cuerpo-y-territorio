---
title: Trabajo, movilidad humana y ciudadanía en cuerpos y territorios
  transnacionalizados
subtitle: "  "
cover: /images/uploads/th_a42a72a425c164dd4407cbac84a3d92a_pesqueros_almadraba.jpg
date: 2020-11-15T00:45:24.208Z
authors:
  - Patricia Lepratti
tags:
  - TrabajadoresDelMar
  - Migrantes
  - Ciudadanos
  - Mundo
categories:
  - Política y Economía
comments: true
sticky: true
---
<!--StartFragment-->

¡Hola! ¿Cómo están? Yo feliz de que este sea ya nuestro cuarto encuentro en este espacio. Y llegado este punto, me gustaría hacer una síntesis de lo que hemos compartido hasta ahora. En el primer encuentro presentaba el tema que vengo analizando desde hace casi diez años: las diferentes dimensiones del trabajo marítimo. En particular, cómo el carácter transnacional de este tipo de actividad determina que el "trabajo" y la "vida" estén separados por miles de kilómetros. Luego, el segundo y tercer encuentro fueron referidos a la celebración en Argentina del Día del inmigrante (4 de septiembre) y de la Promulgación del Voto femenino (23 de septiembre). 

Presentados de esta forma parecen temas muy diferentes entre sí y quizás difíciles de conectar o asociar. Pero si los abordamos desde un punto de vista amplio podemos reconocer, eso espero, un intento de visibilizar las conexiones entre trabajo, movilidad humana y ciudadanía. Es decir, **¿cómo ejercen o reclaman derechos lxs trabajadores transfronterizxs?**

O de manera aún más concreta, la pregunta que (me) vengo planteando desde hace tiempo es: ¿cómo o ante quién reclama su salario o su derecho a la asistencia médica, un trabajador del mar peruano abandonado en Puerto Madryn (Argentina), si el barco para el que ha trabajado tiene pabellón panameño, pero es de una empresa española, y firmó el contrato de trabajo con una agencia marítima con sede en Montevideo?

Este tipo de situaciones son frecuentes en el trabajo marítimo y por ello lo considero un ámbito en el que se pueden observar de manera exponencial temas relativos a la movilidad del trabajo, del capital, y de las normas que regulan esta relación constitutiva del mundo capitalista en el que vivimos.

Durante las últimas décadas hemos sido testigos de cómo nuestros gobiernos promocionan las ventajas de invertir en nuestros países, sus “recursos naturales”, su “capital humano”, y hemos conocido y sufrido sus esfuerzos por dar “buenas señales” a los inversores extranjeros: estabilidad macroeconómica, recorte de obligaciones en cuanto a la seguridad social, etc. Este coqueteo con los dueños del capital, en el caso de las industrias marítimas (mercante, pesquera, cruceros), es mucho más directo. No se lo seduce con sutilezas, directamente se pone el pabellón nacional en venta. **Si. Hay un mercado de banderas, Banderas de Conveniencia.**

El régimen de Banderas de Conveniencia (BDC) se inició a principios de los años 1920, cuando Estados Unidos cambió la inscripción de dos de sus trasatlánticos a la bandera panameña para eludir la Ley Seca, que impedía vender y consumir alcohol a bordo de sus naves. Desde la segunda mitad del siglo XX su uso se ha hecho cada vez más frecuente a nivel mundial con el objetivo de abaratar costos y evadir controles.

No es casual que en el mercado de Banderas de Conveniencia encontremos pabellones como los de: Bahamas, Islas Caimán, Gibraltar, Islas Marshall, Mauricio, Antillas, Liberia, etc., es decir, territorios colonizados por grandes imperios marítimos como el Reino Unido, Estados Unidos, Francia, Holanda, etc.

Este hecho, además, ha fragmentado las posibilidades de supervisión de la situación de los trabajadores marítimos, ya que el uso de Banderas de Conveniencia generalmente va de la mano con la contratación de trabajadores de orígenes nacionales diferentes a los del pabellón y a los de la empresa naviera. Tampoco hay aportes a la seguridad social del país de origen de lxs trabajadorxs, de modo que la reproducción del trabajo: salud, jubilación, educación de las nuevas generaciones, queda librada a las posibilidades de cada individuo y de sus redes sociales particulares.

En síntesis, el carácter móvil del trabajo (ya sean trabajadores marítimos, “brazeros” para las agroindustrias en Estados Unidos o Europa, o cualquier otro tipo de trabajo transfronterizo), posibilita un sueño eterno del capital, a saber: **disponer del trabajo sin tener que pagar por la reproducción del mismo**.

Y si continuamos en este espiral de desposesión, no querer tomar parte de los procesos de reproducción de un trabajo realizado por seres humanos, significa deshumanizarlo (tal y como se hacía con las poblaciones de las antiguas colonias). Es decir, se desdibuja su carácter social y político, volviéndolo pura mercancía. De ahí que la división entre trabajadorxs nacionales y extranjerxs o entre ciudadanxs e inmigrantes sea considerado uno de los mayores ejes de inequidad en el mundo actual, más aún cuando también están implicadas jerarquías por adscripción de raza, género y origen nacional entre trabajadorxs.

Sin embargo, he usado la palabra “posibilita” y no “cumple” el sueño de desposesión del carácter humano del trabajo porque, aunque no sean consideradxs ciudadanxs, esxs trabajadores resisten y, en ocasiones, encuentran caminos alternativos para reclamar o ejercer derechos.

Evidenciar estos diferentes caminos para hacer valer y reclamar derechos **nos posibilita pensar la ciudadanía desde otro lugar que no sea el de la pertenencia a un estado-nación,** de modo tal que las poblaciones que se desplazan o han sido desplazadas no continúen siendo desposeídas de su carácter social y político.

En la actualidad, **más de 200 millones de personas vivimos en países de los que no somos ciudadanxs**, pero muchos seguimos pensando la ciudadanía, el acceso a derechos y a la protección social en relación a un estado-nación (al estado-nación donde hemos nacido). Sin embargo, la “ciudadanía”, como las categorías “migrante” y “trabajador” es una categoría histórica. Y si podemos notar fácilmente la transnacionalización de nuestras economías, de nuestros consumos, de nuestras relaciones laborales y hasta íntimas, podemos también pensar que los escenarios en los que disputamos derechos y obligaciones se han transnacionalizado.

De todas maneras, los hechos ocurridos en este año tan especial, nos ha demostrado que el Estado sigue siendo un actor muy importante en el mencionado escenario transnacional. Lo que implica que no se deben abandonar los históricos caminos de organización colectiva para el reclamo y ejercicio de derechos, pero sí que es necesario actualizarlos, incluyendo a quienes hasta ahora han sido excluidos.

**Por eso me gusta tanto participar de un espacio tan diverso y transnacional como el de la Revista Cuerpo** y Territorio, en el que escribimos y nos leemos argentinxs, colombianxs, chilenxs, mexicanxs, uruguayxs, venezolanxs, y al que seguramente seguiremos sumando voces y lectorxs. Por eso también, espero que entiendan por qué **en nuestro próximo encuentro me gustaría escribir sobre la Declaración Universal de Derechos Humanos**.

¡Nos estamos leyendo!