---
title: El cine y el mundo laboral
subtitle: "Festival Internacional de Cine sobre el trabajo "
cover: /images/uploads/construir_cine_2021-1021x580.jpg
date: 2021-05-31T20:46:48.882Z
authors:
  - Yuliana Fuentes Fuguet
tags:
  - Cine
  - trabajo
  - trabajadores
  - mayo
categories:
  - Cultura
comments: true
sticky: true
---
**En el mes de mayo, mes donde se conmemora las luchas por reivindicar los derechos laborales** y por más justicia social de las trabajadoras y los trabajadores, se realizó la 8° edición del **Festival Internacional de Cine sobre el mundo del trabajo, Construir Cine 2021**, donde compitieron 44 films que integran las 6 categorías oficiales, que tiene como propósito principal el descubrimiento y promoción de películas que abordan temas **universales que afectan a los trabajadores de todo el mundo y que consideren el trabajo como una fuerza de cambio en la vida de las personas.**

**El cine a través de sus formas de reflejar la realidad, sirve como observatorio de los grandes temas de la humanidad,** así como es capaz de abordar situaciones de la vida cotidiana, logrando que nos identifiquemos con las historias y los personajes.

En este contexto de pandemia mundial donde el tema laboral se ha visto tan afectado, les compartiré algunas películas que fueron seleccionadas en las distintas categorías de Construir Cine 2021, y todas las historias están vinculadas al mundo del trabajo. Con la intención de reforzar la consigna #YoMeQuedoEnCasa pero reflexionando sobre el tema.

### “Cosiendo futuros” – Enrique Rey Monzón

![](/images/uploads/cosiendofuturos.jpg)

Un grupo de mujeres africanas cuenta las dificultades y barreras que entraña dejar tu país y llegar a otro donde no tienes nada ni a nadie y además, en muchas ocasiones, se desconoce el idioma.

### “Pasaje de ida” – Víctor Augusto Mendívil Garavito (Perú)

![](/images/uploads/po-pasajedeida-230x325.jpg)

Esta es la historia de Don Rafael Castrillón, juguetero y ex Mister Perú. Su historia es un pasaje de ida hacia la fabricación del juguete de madera.

### “La llamada” – Octavio Maya Rocha (México)

![](/images/uploads/lallamada.jpg)

Retratar la realidad que se vive en distintos puntos de América y Europa en relación a la pandemia generada por el Covid-19 es el eje del cortometraje titulado La llamada, del que parte fue grabado desde la casa del actor panameño Daniel Márquez y que se integrará al proyecto.

### “Jueves de comadres” - Noemi Chantada Puime (España; 2021)

![](/images/uploads/las_comadres.jpg)

\
Aurora vive sola, sola con sus recuerdos y tareas, esperando cada día esa llamada, esa visita de su hijo, pero eso no sucede. Hasta que llega a su vida Mary y con su alegría y su cariño intenta habitar esa anciana soledad.

### “Solo cerca del mar” – Andrés Ossa Jahr (Chile)

![](/images/uploads/solocercadelmar.jpg)

En Quintay, una alejada caleta vive Manuel (15) un niño que camina con su estómago crujiente, tiene hambre, pero no solo alimenticio sino que también afectivo. Adaptación libre del cuento "El vaso de leche" de Manuel Rojas.

### “La vendedora de lirios” – Igor Galuk (Argentina)

![](/images/uploads/2d9e14950db22a6783b45624f9cbc5a5_big.jpg)

Jacinta y su nieta Indira son migrantes bolivianas. Durante la primavera trabajan recolectando flores en la costa del río que luego venden en el cementerio del pueblo de Magdalena.

### “Poder emprender” - Lucila de Oto

![](/images/uploads/e1wxs6_wyamzejq.jpg)

Este cortometraje presenta una mirada a las complejas sensaciones que experimenta la mayoría de los trabajadores que busca revalorizar la satisfacción del trabajo artesanal.

### “Transición - XBUK ALO?'”- Aron Marty (Suiza)

![](/images/uploads/e1wx6qbweaygddl.jpg)

Este documental nos muestra al peluquero Many y su abanico de navajas y utensilios. Siempre con una sonrisa en la cara y con mucho encanto se encarga de los rizos, barbas y rastas de sus clientes.

### “Cheto cheto” - Fabio Marcelo Zurita (Argentina)

![](/images/uploads/cheto.jpg)

Tras las rejas un grupo de adolescentes comienza a disfrutar del cine y deciden escribir y tomar la cámara. Este largometraje nacional nos muestra al cine como valor social, como factor de transformación e inclusión.

### “Women of steel” - Robynne Murphy (Australia)

![](/images/uploads/mujeres.jpg)

Un documental emocionante y humorístico de los altibajos de un grupo de mujeres, decididas a vencer a la empresa más poderosa de Australia por discriminarlas laboralmente.

### “FIRASTYA” - Vitthal Machindra Bhosale (India)

![](/images/uploads/e1sme1eweaerijy.jpg)

Esta película basada en hechos reales nos revela el camino de trabajo, voluntad y esfuerzo que encara un niño de escasos recursos.

### “En busca del profesor precario” - Gerry Potter (In search of professor precarious)

![](/images/uploads/in-search-of-professor-precarious-1-e1620311904556.jpg)

Este documental canadiense expone el trabajo precario y la lucha contra la explotación laboral del profesorado contratado en la enseñanza superior en Canadá.

### “Nacido y Criado'”- Jorge Donoso (Brasil y Chile)

En este largometraje chileno, Yorman decide no ir a su país natal para despedirse de su madre agonizante. Él se siente abandonado y solo quiere destacarse jugando al fútbol.

### ”Sugar on the weaver´s chair” - Harvan Agustriansyah (Indonesia)

![](/images/uploads/187053733_1785205218343220_2746739877132154746_n.jpg)

Tres mujeres en diferentes zonas de Indonesia deben enfrentarse a su realidad y redefinir su rol en la sociedad.

### “Fantasma vuelve al pueblo” - Augusto González Polo (Argentina)

![](/images/uploads/fantasma.jpg)

Evoca la conciencia del trabajador como herramienta de cambio y la consideración de todos los animales como seres sintientes.

### “Los peces también saltan” - Diana Cardini (Argentina)

![](/images/uploads/69544427_2415038432103051_7950055366216122368_n_1340_c.jpg)

Documental sobre el cementerio de la Chacarita, con historias transitadas por los personajes, sus oficios, rituales y sus formas específicas de lidiar con el otro mundo.