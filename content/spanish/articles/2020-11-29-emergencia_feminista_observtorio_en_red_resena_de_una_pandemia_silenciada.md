---
title: "Emergencia Feminista observatorio en red: Reseña de una pandemia silenciada"
subtitle: " "
cover: /images/uploads/descarga-1.jpg
caption: " "
date: 2020-11-29T21:23:01.705Z
authors:
  - Valentina Mena Castro
  - Niyireé S Baptista S
  - Shirly Dana
tags:
  - Genero
  - ViolenciaDeGenero
  - 25N
  - Ella
  - EncuentroFeminista
  - Reportaje
  - Latinoamerica
  - Colombia
  - Argentina
  - Venezuela
  - Historia
  - RedInternaciopnalDeFeminismo
  - Feminismo
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

En el primer Encuentro Feminista Latinoamericano y del Caribe en el año 1981 se estableció el 25 de noviembre como el Día internacional de Lucha contra la violencia de género, la elección de este día obedece a la conmemoración de la muerte de las hermanas Mirabal.

Desde 1930 hasta 1961 en República Dominicana se había establecido el régimen de terror del dictador Rafael Leónidas Trujillo. Durante los años 60 en América Latina se suscitaron una serie de cambios políticos que pusieron fin a diferentes gobiernos dictatoriales como el golpe de Estado de Rojas Pinilla en Colombia y la dictadura de Pérez Jiménez en Venezuela, mismo momento histórico en el cual comenzó a organizarse en República Dominicana un movimiento clandestino de izquierda en contra de la dictadura de Trujillo.

En este último participaban las hermanas Patria, Minerva y María Teresa Mirabal, junto a sus compañeros y a un centenar de luchadores antidictatoriales. Luego de un período de encarcelamiento y tortura de Minerva y María Teresa, las tres hermanas continuaron encabezando la resistencia y el 25 de noviembre fueron brutalmente asesinadas, junto con el chofer que manejaba el jeep que las trasladaba. Fueron golpeados y luego arrojados por el barranco simulando un accidente. La Organización de las Naciones Unidas (ONU), en 1999, otorgó carácter oficial a esta fecha conmemorativa. De allí a que cada 25 de noviembre se conmemora como el Día Internacional contra la violencia hacia las mujeres.

En este sentido, podemos decir que la violencia según la OMS en el informe de violencia y salud del año 2002 debe ser entendida como “el uso intencional de la fuerza o el poder físico, de hecho o como amenaza contra uno mismo, otra persona o un grupo o comunidad, que cause lesiones, muerte, daños psicológicos, trastornos de desarrollo o privaciones”. No obstante, es importante tener en cuenta que estas formas de dominación son excluyentes y puede presentarse victimización por varios de estos factores en un mismo caso. Es así como dentro de las características de la violencia se encuentra contemplada la violencia física, sexual, económica, psicológica y patrimonial.

**Los sistemas patriarcales sientan sus bases en la dominación del varón por sobre la mujer ejercida de distintas formas y con múltiples modalidades**. Expresan estas diferencias en las jerarquías sociales en tanto relaciones económicas, culturales, religiosas, militares, simbólicas cotidianas e históricas. Todos estos sistemas son históricos, no naturales y buscan que la diferencias biológicas entre los géneros justifiquen la perpetuación de la dominación ejercida por parte de los varones, extendiendo la brecha entre géneros y buscan “naturalizarla”.

Esta visión dicotómica de los géneros y las sociedades fomenta la discriminación y violencia ejercida sobre las minorías. Persisten las armas de dominación patriarcal no sólo en términos de violencia ejercida a las personas individuales, sino a los colectivos, como ser obstaculizando todavía en muchos países latinoamericanos el acceso al aborto legal y seguro, presuponiendo la heterosexualidad en todas las personas, asumiendo los géneros teniendo base en las expresiones del mismo, entre muchas otras manifestaciones de dicha dominación.

#### Contexto regional caso Argentina, Colombia y Venezuela

En **Argentina**, además, el movimiento de mujeres y feminidades se organizó y el 3 de junio de 2015 salimos a las calles bajo el lema de **“Ni una menos”** a exigir el cese de la violencia machista y patriarcal. La Ley 26.485 de protección integral para prevenir la violencia contra las mujeres, la Ley 26.150 del Programa Nacional de Educación Sexual Integral sientan un marco legal para afrontar la erradicación de la violencia en Argentina. En el año 2015 se llevó adelante un observatorio ahora que sí nos ven. Sin embargo, continuar hablando de violencia de género sin hacer mención del patriarcado y sus tramas significa ignorar el trasfondo de esta situación.

En el caso de **Colombia**, por medio de la Ley 1257 de 2008 se dictamina que las normas de sensibilización, prevención y sanación de formas de violencia y discriminación contra las mujeres hacen parte de los Códigos Penales y de Procedimiento Penal. Esta ley se encuentra soportada a su vez bajo la ley 264 de 1996. Ambas leyes tienen la particularidad de cobijar a todas las mujeres y aplica en todos los casos para las violencias de género, incluyendo a violencias que se ejercen a personas por su orientación e identidad de género.

Acompañando a estas dos leyes, desde el año 2000 se inserta la categoría de feminicidio en la Jurisdicción Penal por medio de la ley 599 junto con la ley 1761 Rosa Elvira Cely. Esta última, existe a raíz del atroz crimen de Rosa Elvira y determina que se debe “garantizar la investigación y sanción de las violencias contra las mujeres por motivos de género y discriminación, así como prevenir y erradicar dichas violencias y adoptar estrategias de sensibilización de la sociedad colombiana, en orden a garantizar el acceso de las mujeres a una vida libre de violencias que favorezca su desarrollo integral y su bienestar, de acuerdo con los principios de igualdad y no discriminación”

En el caso de **Venezuela**, en 2007 se legisló la Ley Orgánica sobre el derecho a una vida libre de violencia, en la cual se tipificaban 19 tipos de violencia, pero no se incluía el femicidio. No obstante, para el 2014 se introdujo una modificación en la Ley para que quedara establecido el femicidio y la inducción al suicidio como 2 formas de violencia, por lo que la Ley venezolana hasta ahora establece 21 tipos de violencia, donde se incluye el femicidio como expresión de violencia maxima, así como también el la violencia obstetrica, la violencia simbolica y la patrimonial, entre otras.

De esta manera, la Ley venezolana se convirtió en una de las más avanzadas en la región latinoamericana. No obstante, existen muchas trabas por parte de las instituciones del Estado venezolano y los cuerpos de seguridad para la garantía de los derechos de las mujeres y del debido cumplimiento de la ley.

En ese sentido, muchos son los casos que quedan impunes y cada día crecen más las mujeres víctimas de violencia machista. Sin embargo, las organizaciones feministas y de mujeres están luchando por una modificación en la ley para incluir el derecho a decidir sobre nuestros cuerpos y por la realización de los reglamentos que indiquen el debido proceso de justicia que se debe llevar a cabo en estos casos.

#### Emergencia Feminista observatorio en red

**El pasado 7 de noviembre desde el ELLA (Red Internacional de Feminismo) en alianza con la Red Latinoamericana contra la violencia de género, realizaron un encuentro que se denomino como Emergencia Feminista observatorio en red**, dicho encuentro buscó la visibilización de la violencia machista de las que son victimas las mujeres en América. El observatorio se enfocó en darle la palabra a diferentes mujeres provenientes de contextos diversos en toda América, incluyendo a Canadá, parte de las islas del Caribe, Centro y Suramérica, con el fin de que cada una de ellas expusieran ante nosotras, las asistentes, cómo se encuentra su país en lo que respecta al tema de violencia de género. Las organizaciones de cada país en su mayoría eran mujeres y feministas que abogan por hacer visible la inmensa pandemia de muerte de mujeres que sufre el continente y que no escapa a edades, clases sociales o etnias.

Llama la atención que cada una de las situaciones son producidas por entornos particulares, sin embargo, la emergencia de la violencia de género es un fenómeno que se encuentra presente en todos estos lugares en mayor o menor medida, en lo que respecta a las mujeres y las violencias cotidianas que sufren se especificó que en su mayoría las violencias que más se repiten son la violación o abuso sexual desde la temprana infancia, la prostitución forzada y el femicidio. Las organizaciones que expresaron las cifras de violencia en sus países de origen dejaban claro que la actuación por parte del Estado es en ocasiones nula, y que las mujeres son doblemente violentadas y agredidas no solo por la violencia machista, sino también por la estatal que no hace nada frente a la situación que se vislumbra como emergencia.

**La compañeras también denunciaban la problemática existente respecto a la interrupción voluntaria del embarazo, ya que en la mayoría de estos píses el acceso al aborto seguro, gratituto y legal, sigue siendo un punto de lucha de las organizaciones y colectivas feministas.** Por otro lado, se destacó, como las agresiones de violencia han aumentado durante la pandemia, a excepción de países como Uruguay y en un inicio Puerto Rico. Uno de los grandes llamados de las organizaciones es, en primer lugar, a que se atienda esta emergencia y en segundo lugar, que se reporten las cifras y los casos, ya que son muy pocos los países que llevan registros de las cifras de violencia hacia las mujeres.

Por otra parte, el encuentro también contó con un grupo de periodistas feministas de la organización Feminacida, las cuales hablaron de **la importancia de la visibilización de estas realidades en los medios de comunicación y de qué manera se deben abordar los casos,** para no ser amarillistas, ni revictimizar a las mujeres que enfrentan estas situaciones.

Bibliografía:

[Observatorio Nacional de Violencias ](https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/ED/GCFI/guia-ross-observatorio-violencia-genero.pdf)

[Congreso de la República de Colombia (2008) Ley 1257 de 2018]([https://www.mintic.gov.co/portal/604/articles-3657_documento.pdf](https://www.google.com/url?q=https://www.mintic.gov.co/portal/604/articles-3657_documento.pdf&sa=D&ust=1606688281264000&usg=AOvVaw0iGH4yFNhFxtSzEIjNzsEk))

[El feminicidio en Colombia. Universidad La Gran Colombia]([https://repository.ugc.edu.co/bitstream/handle/11396/4622/Feminicidio%20en%20Colombia.pdf?sequence=1&isAllowed=y](https://www.google.com/url?q=https://repository.ugc.edu.co/bitstream/handle/11396/4622/Feminicidio%2520en%2520Colombia.pdf?sequence%3D1%26isAllowed%3Dy&sa=D&ust=1606688281259000&usg=AOvVaw2NM-X-QNt1d5ajfSxzVxs8))
