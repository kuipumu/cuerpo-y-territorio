---
title: "Identidad: el poder de conectar y pertenecer"
subtitle: " "
cover: /images/uploads/cata_linamb_68818415_2616867661711365_4065514890950888407_n.jpg
date: 2020-10-01T01:34:13.461Z
authors:
  - Catalina Medina Barrios
tags:
  - Psicología
  - SíndromeDelImpostor
  - Convivencia
  - Personalidad
  - Trabajo
  - Confianza
  - Autoconocimiento
categories:
  - Cuerpo
comments: true
sticky: true
---
<!--StartFragment-->

Somos seres sociales, lo que nos lleva a vivir en comunidad, a estar en “manada”, a relacionarnos y convivir con los demás.

Siempre he sido más de escuchar que de hablar, ahora entiendo que ese es un gran poder para lo que hago. Sin embargo, crecí pensando que no era tan bueno, crecí pensando que las personas extrovertidas tenían mayor capacidad para aprovechar las oportunidades y les era más sencillo pertenecer y la verdad es que yo era muy introvertida y mi curva para construir confianza era demasiado lenta frente a lo que veía como referencia, lo que me hacía en varios momentos pensar que tenía que cambiar, que tenía que ser diferente y en ocasiones crear una máscara para poder ir por eso que creía era más fácil conseguir comportarme de manera distinta.

Sin mucho éxito, viví varias situaciones con esa máscara que generaba una sensación rara dentro de mí, además de desgastarme emocionalmente (no lo sabía, pero **las personas introvertidas no necesitamos tantos estímulos externos para estar felices**) ya que al ponerme una máscara lo que hacía era exponerme a estímulos que me sobrecargaban y por ende agotaban física, mental y emocionalmente. Digo que sin mucho éxito, porque al final sentía más una sensación de fracaso que de éxito.

Después de renunciar a mi último trabajo, frustrada porque no entendía qué hacer conmigo misma, me senté a revisar mi vida.

Quede sorprendida, me sorprendí porque no entendía cómo una persona introvertida, que muchas veces no le gusta hablar con la gente y prefiere sentarse a ver el pasto, trabaja como relacionista, crea alianzas, diseña y organiza eventos y crea comunidades, a lo que respondí de una que era suerte (claramente el síndrome del impostor respondiendo por mi), que realmente no era que yo hiciera algo, solo que la gente está ahí y yo simplemente los presento.

Conociendo que hago parte del [70% de las personas que sufrimos del síndrome del impostor,](https://time.com/5312483/how-to-deal-with-impostor-syndrome/) volví a preguntarme y la respuesta fue otra, **me di cuenta que podría generar estas relaciones tan fuertes y reales porque conocía a estas personas,** había escuchado y compartido experiencias de sus mejores momentos y también de sus peores, sin juzgarlas, porque ellas también me conocían en mi esplendor, esplendor que me daba vergüenza y del que me burlaba, pero que ellos conocían y que apreciaban. Entre esas personas y yo, había transparencia en lo que éramos, teníamos, queríamos y esperábamos ser, lo que hacía más sencillo para mi poder encontrar la pieza ideal para el rompecabezas que la otra persona estaba armando y viceversa.

{{< youtube ZQUxL4Jm1Lo >}}

En ese momento entendí que gracias a las identidades de cada uno de nosotros, a eso que nos hace completamente diferentes, es que creamos comunidades mucho más fuertes, dinámicas y moldeables, ¿cómo me di cuenta? en mi círculo encuentro empresarios, políticos, activistas, artistas, etc., con ideologías, creencias, paradigmas y formas de ser totalmente diferentes pero con los que yo he podido construir alianzas, crear eventos y construir conversación en pro de algún tema o interés en común y valioso para nuestros trabajos, nuestros equipos, clientes o comunidad, sin utilizar máscaras, sin juzgamientos y siendo todos totalmente reales.

Tener este entendimiento me hizo ver que es una forma muy sencilla y comúnmente descrita para crear nuestras relaciones, pero no fácil de implementar, ¿la razón? **siempre estamos tratando de ser quien no somos porque creemos que así podemos pertenecer**, como en un momento yo utilicé máscaras para mostrarme más extrovertida de lo que era por naturaleza, ¿cuántas personas no están en lo mismo constantemente?, utilizando máscaras frente a su identidad sexual, a sus creencias, sus debilidades, las diferencias de aprendizaje o escondiendo las ideologías religiosas y políticas en las que creen, solo por no ser diferente y entonces ser parte de algo.

Desde pequeños nos enlistamos en una vida del “deber ser” para pertenecer, ésta nos va llevando a perdernos de nosotros mismos, **creando seres que internamente no reconocemos pero que “pertenecen”.**

Haber podido revisar mi vida y entenderme como un ser diferente con muchas cosas que no están en la lista “del deber ser” pero las cuales me han llevado a ser más feliz y a **lograr más sensaciones de éxito que las que se supone que te “llevan al éxito”**, me dio la oportunidad de reconocer cara a cara cada parte de mi, esta vez sin mi tono burlón o mi sarcasmo, desde mis pensamientos, mis creencias, habilidades, debilidades, miedos, frustraciones, hasta mis emociones más complejas (positivas y densas) mostrándome con mi vida de ejemplo que “generamos conexiones como resultado de nuestra autenticidad” como lo dice Brené Brown.

**Porque al reconocernos con todo eso que somos y no somos, es mucho más sencillo compartirnos a los demás,** mostrar nuestra identidad a través de la autenticidad, abriéndonos a saber que somos el cúmulo de experiencias y que somos una nueva persona cada día, porque lo que viví ayer es una experiencia nueva para lo que soy hoy y lo que viva hoy será experiencia para mi yo de mañana.

Este es mi aprendizaje de vida más grande hasta ahora, la razón por la que comencé a querer trabajar el reconocimiento del individuo y a poner el individuo en el centro de la construcción de comunidades, porque eso es lo que yo he hecho sin darme cuenta, **porque es lo que he construido mientras trataba de cambiarme de carril.**

**Hoy mi invitación es a que te explores constantemente y que te compartas tal cual eres, para que construyas relaciones reales, transparentes que te lleven a donde desees como lo han hecho conmigo.**

<!--EndFragment-->