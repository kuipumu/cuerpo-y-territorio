---
title: Canciones para escuchar ser libre y sanar
subtitle: "Las mujeres Latinoaméricanas y la música "
cover: /images/uploads/music-1285165_1920.jpg
date: 2021-04-30T20:28:07.404Z
authors:
  - Selene Sánchez
tags:
  - Cultura
  - Musica
  - Mujeres
  - Genero
  - Feminismo
categories:
  - Cultura
  - Genero
  - Cuerpo
  - Territorio
  - Cuerpo y Territorio
comments: true
sticky: true
---
La música es una expresión que se encuentra presente a lo largo de nuestras vidas, todas y todos parecemos tener una lista de reproducción o sountrack que nos acompaña en las diferentes etapas que transitamos como seres humanas y humanos. Es así como me di a la tarea de recopilar una pequeña lista de cinco canciones de artistas mujeres latinoamericanas, que han sido importantes en algunas etapas de mi vida y que hoy les comparto a petición del equipo de trabajo de la Revista Cuerpo y Territorio.

1. ### Eu – Mariana Froes 

   Es una cantautora y compositora brasileña. Eu es una canción del año 2020, que habla del desespero que sentimos y expresamos en el cuerpo, al no saber lidiar con una misma como mujer y con los pensamientos que en situaciones complejas llenan nuestra cabeza. Es ideal para escuchar cuando se está buscando un escape a una misma. Su letra es bellísima y fácilmente te puedes identificar.

{{< youtube ILNAM5KDuhk >}}

2. ### Ese camino – Julieta Venegas cantante y compositora mexicana

   Ese camino, es una canción maravillosa del año 2015, que habla de los caminos que recorremos como mujer desde la infancia hasta la adultez, tiene un ritmo que la vuelve una canción bastante alentadora y positiva. Ideal para encontrarse con una misma y tener fe en que lo que escogimos ser, es el camino correcto.

{{< youtube jKvmNUfmFfI >}}

3. ### **Deja – Liliana Saumet (Bomba estéreo)**

   Liliana Saumet es una cantante Colombia, perteneciente a la agrupación Bomba estéreo donde hace de voz principal. Recientemente sacaron su nuevo álbum Agua, en donde resalta su canción Deja, una canción que habla de las inseguridades y miedos a los que nos enfrentamos en la vida. Esta pieza musical es ideal escuchar cuando se tiene o no, la certeza de querer renunciar a una misma, o simplemente cuando ya no vemos más posibilidades a lo que hacemos. Es perfecta porque su letra te hace reflexionar y entrar en el punto de no auto cuestionar tanto nuestras acciones y seguir adelante.

{{< youtube cIugIFbwi70 >}}

4. ### Mérida – Liana Malva 

   Es una cantante y compositora venezolana proveniente de la gran Sabana. Tiene un electico estilo musical, y sus canciones hacen referencia a su vida en el ambiente natural en el que creció y llaman a la reflexión de la precaria crisis ecológica que existe en el país. Mérida es una canción ideal para reconectar con la naturaleza externa que nos arropa y la interna que nos habita. Cuando nos sentimos desconectadas de nosotras mismas, esta canción nos recoloca en nuestro ser.

{{< youtube GV3z8eNLMbs >}}

5. ### Nostalgia Andina – Betsayda Machado 

   Es una cantante de música tradicional venezolana, que nació en Caracas, pero se crió en Barlovento. En cada una de sus canciones proclama nuestra herencia ancestral africana e indígena. Con la canción Nostalgia Andina, expresa esa sensación de anhelo por aquello que se tuvo pero que ya no se tiene más. Esta pieza musical además de conectarnos con nuestras esencias tradicionales, nos transporta a esos lugares en los que alguna vez estuvimos y quisiéramos retornar. Ideal para escuchar en un momento nostálgico.

{{< youtube ZUMH3pjkizQ >}}