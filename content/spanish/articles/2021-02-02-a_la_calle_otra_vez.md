---
title: ¡A la calle otra vez!
subtitle: " "
cover: /images/uploads/dscn1711.jpg
date: 2021-02-02T17:22:09.996Z
authors:
  - Jefersson Leal
tags:
  - Venezuela
  - Política
  - Régimen
  - CrisisSocial
  - Latinoamérica
  - Historia
  - Dictadura
  - LibertadDeExpresion
  - Represión
  - Estudiantes
  - Política
  - Crisis
  - Protestas
  - Universidad
categories:
  - Política y Economía
comments: true
sticky: true
---
Hace 64 años, el movimiento estudiantil de la Universidad Central de Venezuela (UCV) generó una huelga para **romper el silencio contra la dictadura militar encabezada por Marcos Pérez Jiménez.** Unos días antes ya varios liceos de Caracas habían iniciado una serie de protestas que sirvieron de antesala a los acontecimientos que sucedieron ese 21 de noviembre de 1957.

La juventud universitaria de entonces decidió alzar su voz contra la dictadura y sus horrendos crímenes contra la población. Ese momento marcó un hito, fue el día en que los/as estudiantes se alzaron pidiendo “elecciones libres” y muerte al dictador. **Dos meses después, el 23 de enero de 1958, la dictadura caería y se iniciaría un nuevo capítulo en la muy mellada historia de Venezuela.**

Seis décadas después, el 23 de enero del 2021 un grupo de activistas por los derechos humanos, militantes sociales, estudiantes, jubilados/as, feministas y trabajadorxs se concentraron en “La puerta Tamanaco” de la UCV, **para romper el silencio contra el autoritarismo del Estado venezolano y sus constantes violaciones a los derechos laborales, civiles y humanos de la población.** `(1)`



![](/images/uploads/dscn1597.jpg)

Un gran mensaje fue colgado en la entrada de la universidad **“Por la vida y la dignidad, rompamos el silencio”**. Con esa consigna las organizaciones sociales de distintas áreas daban la bienvenida a la actividad. Un mensaje corto, pero poderoso, que en buena medida refleja el conjunto de demandas que constantemente son expresadas por la sociedad venezolana.

Ante un clima de persecución política, alzar la voz, romper el miedo, se convierte en un acto verdaderamente heroico. **Pese a las apariencias y los silencios mediáticos el pueblo venezolano** no ha parado de hablar y de denunciar la horrenda situación que en la actualidad se vive.

Según el Observatorio Venezolano de Conflictiva social **en el año 2020 se registraron al menos unas 9.633 protestas en toda Venezuela.** Las exigencias varían, pero se concentran en: demanda de mejores servicios básicos, derechos laborales, acceso combustible, derechos a la participación política, derecho a la salud, a la alimentación y a la justicia. `(2)`

![](/images/uploads/dscn1679.jpg)

Mención especial merecen las 5.951`(3)` protestas por derecho a la vivienda y servicios básicos que se registraron en el país durante el 2020 en el marco de la pandemia por la COVID-19. Los servicios mes a mes sufren un deterioro importante, estamos presenciando cómo se desmorona el sistema de agua, luz, transporte, comunicación y gas doméstico. Este último ha devuelto a más de una región del país a cocinar leña, pese a que **la nación posee uno de los reservorios de gas natural más grandes del planeta.**

Pero salir a la calle, no es una decisión fácil de tomar, en un país con pocos espacios para la participación política y democrática, las manifestaciones constantemente están en riesgo de ser reprimidas y sus participantes apresados/as por los cuerpos de seguridad del Estado venezolano. Es que los sistemas autoritarios se sustentan en el miedo para funcionar, **cuando ese miedo desaparece de la población, se traslada a sus ejecutores y el sistema se tambalea hasta caer**.

En el año 2020, al menos 412 protestas fueron reprimidas por los cuerpos de seguridad, o por las organizaciones parapoliciales que depende del partido de gobierno`(4)`; **415 personas fueron encarceladas y 6 personas fallecieron**`(5)` **como resultado de las políticas represivas dirigidas por el Estado venezolano** encaminadas a la criminalización la protesta y los espacios democráticos de participación.

![](/images/uploads/dscn1664.jpg)

Construir un tejido amplio, diverso, capaz de recoger las múltiples demandas que hoy pide la sociedad venezolana es un paso fundamental para poder zafarnos del atolladero histórico en donde estamos. **El fracaso de un proyecto, no puede ser el fracaso de la nación.** Cimentar las bases de un movimiento que pueda reconfigurar una nueva forma de hacer política es un paso que hay que dar.

Pero la calle, entendido como espacio de participación, debate y acción de distintos sectores sociales, se resiste al silencio obligatorio, pese a las muy **elaboradas estrategias de miedo que desde los despachos de los burócratas se diseñan día a día,** nuevos imaginarios, diversos, no polarizados y autónomos se están erigiendo y han comenzado a tejer, de a poco, una alternativa.

**Referencias:** 

`1)`[ Ciudadanos convocan a la calle en conmemoración del 23 de enero](https://www.lamananadigital.com/ciudadanos-convocan-a-la-calle-en-conmemoracion-del-23-de-enero/) 

`2)` Conflictividad social – Venezuela 2020, Observatorio venezolano de conflictividad social, p,19.

`3)` Ídem

4. Ibidem, p. 40
5. Ídem