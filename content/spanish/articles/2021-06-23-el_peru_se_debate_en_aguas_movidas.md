---
title: El Perú se debate en aguas movidas
subtitle: "Una mirada de la actual coyuntura política en Perú "
cover: /images/uploads/57172807_303.jpg
caption: "Fuente: DW"
date: 2021-06-23T17:04:33.621Z
authors:
  - Raquel Neyra
tags:
  - Peru
  - elecciones
  - presidenciales
categories:
  - Politica y Economia
comments: true
sticky: true
---
Con la República, los blancos criollos aspiraron en realidad a desarrollarse a semejanza del centro europeo, a “modernizarse” mientras conducían las riendas del país. Mantuvieron el “patrón colonial del poder” (Quijano, 2000) con el control de la economía, de la autoridad estatal y militar, al mismo tiempo que controlaron el conocimiento, la parte epistémica-cultural, es lo que Quijano llama la colonialidad. Introdujeron nuevas técnicas de minería, desarrollaron nuevas empresas (trapiches e ingenios azucareros, muchas veces con esclavos africanos o con trabajadores endeudados chinos), promovieron nuevos productos de exportación muy ligados a la química agraria con el guano, fundaron nuevas academias de ciencias. Se impuso el español, que consideraron superior a las lenguas indígenas.

Se reforzó la idea de raza ,retomada de los conquistadores, con la cual los blancos criollos van a instituir la división del poder y del trabajo. El racismo forma parte integrante de la acumulación y de la desposesión del territorio, es tanto causa como efecto, tal vez más causa que efecto. Los gamonales aprovechan la descampada de los españoles para acaparar más tierras y relegar a las comunidades a las tierras altas o selva adentro. Esta invasión del territorio se ejerció con suma violencia y sometimiento de la población indígena. Empieza la extracción del guano, seguida del caucho y de la extracción de minerales en mayor volumen. Los grandes centros mineros se expanden sobre las tierras de los comuneros, La Oroya (hoy quinta ciudad más contaminada del mundo), Cerro de Pasco, Cuajone, Toquepala, Antamina; también se construyen refinerías de petróleo. Más tarde serán los grandes proyectos de irrigación, Olmos, Majes, Siguas, para favorecer a empresas agroexportadoras, muchas veces violando leyes y voluntades.

Hay que esperar los años 60 para ver un reflejo de luz representado por la reforma agraria del expresidente Velasco cuya gran virtud fue poner a la palestra los problemas del indio, años atrás identificados por Mariátegui. Los partidos políticos de izquierda reivindicaban luchas clasistas. Es la época de los grandes sindicatos. Se rescatan y difunden las luchas indígenas por la tierra. A pesar de eso, el extractivismo siguió y las diferencias raciales subsistieron.

Es necesario conocer estos aspectos históricos para entender lo que está sucediendo hoy en día en el Perú.

La hiperinflación, la violencia de Sendero Luminoso y la represión indiscriminada durante el primer gobierno de Alan García, dieron paso al gobierno del expresidente Alberto Fujimori que prometía pacificar el país. Los partidos de izquierda se debilitaron, víctimas también de la inestabilidad creada por Sendero Luminosos y sus ataques. El sr. A. Fujimori se dedicó a rematar las empresas públicas, a implementar la Ley de Minería que rige hasta el día de hoy, una ley que divide al país en cuadrículas que cualquier persona puede adquirir para realizar un proyecto, ya sea minero, gasífero, de extracción de biomasa, etc. En esas épocas, A. Fujimori teje su telaraña de poder con su asesor Montesinos (hoy los dos en la cárcel); su hija Keiko participa y aprende del padre dictador, gozando del dinero de la corrupción (estudios financiados en universidades privadas, etc.).

El ritmo de la extracción se acelera. Los presidentes subsiguientes, Toledo, Humala, nuevamente García y finalmente Kuczynski y Vizcarra continuarán por la misma senda implantando diversas medidas para facilitar la inversión y la proliferación de proyectos, sobre todo mineros. El Perú se convierte en el primer extractor de oro, estaño, plomo en América Latina y el segundo a nivel mundial de cobre, zinc y plata. El 60% de las exportaciones son minerales. Mientras, la resistencia a los proyectos continúa y más de 105 defensores ambientales entregaron sus vidas desde 2003 hasta hoy, además de centenares de personas muertas en conflictos de índole social. 

Durante el segundo gobierno de Belaúnde se impulsó la colonización de las tierras amazónicas con la construcción de carreteras (la Marginal de la Selva) favoreciendo la llegada de habitantes del Ande en busca de nuevas fortunas. Los gobiernos subsiguientes prosiguen con el acaparamiento de tierras hasta llegar al denominado Baguazo. Al mismo tiempo se expande el narcotráfico, y hoy Perú es el primer productor mundial de cocaína. La familia Fujimori tiene las ropas teñidas de polvo blanco.

Keiko Fujimori ansía el poder y compite con O. Humala por la presidencia (2011) y pierde. Lo vuelve a intentar contra Kuczynski (2016) y pierde. Los recuerdos nefastos de la dictadura fujimorista alimentaron su derrota. Siguió construyendo su imperio, hoy está siendo juzgada por financiamientos ilícitos, implicada en el caso Lava Jato. Estuvo 13 meses en prisión preventiva (2019) y gracias a su control del poder judicial, pudo salir de prisión.

A inicios del 2020, el país se encontraba en una disputa manifiesta entre los diferentes grupos de poder, representados en el congreso, enfrentados entre sí por la hegemonía del país, entre ellos Fuerza Popular de K. Fujimori. Paralelamente prosiguen las investigaciones y condenas por corrupción contra 4 expresidentes, Alejandro Toledo (2001-2006) arrestado en los EEUU y con pedido de extradición, Alan García (2006-2011) que se suicidó el día de su arresto, Ollanta Humala (2011-2016) que terminó la prisión preventiva y Pedro Pablo Kuczynski (2016-2018) en arresto domiciliario; y la exalcaldesa de Lima Susana Villarán. No olvidemos que aún cumple arresto domiciliario el expresidente A. Fujimori por sus crímenes y continúan las investigaciones en torno a numerosos otros políticos (1) 

En 2019, el Perú se situaba en el nivel 36 en una escala de 0 (mayor percepción de corrupción) a 100 (menor percepción) según la Organización de Transparencia Internacional (2). La pandemia irrumpe en un país con datos alucinantes: el Perú sigue siendo el primero de América Latina en casos y muertes por tuberculosis (3), destina alrededor del 3,16% del PIB al sector salud público, lo que representa alrededor de 186 USD por año per cápita y el 15% del gasto público total. Estas cifras son muy bajas en comparación al aumento sostenido del PIB en los últimos diez años producto de la extracción y exportación de minerales. Los sectores rurales son los más afectados, la tasa de mortalidad infantil se eleva a 22/1000 (la urbana 13/1000), el 49% de los niños en el área rural padece de anemia (36% en el sector urbano), el 25% de los niños padece de desnutrición crónica en el área rural (INEI, 2019) (4) 

Pero es en la región selva que todos los indicadores se disparan, el 20% de la población padece de enfermedades crónicas. (ENAHO, 2019). Así mismo, las encuestas determinan que el 48% de la población no accede al sistema de salud, público o privado, por no tener ingresos suficientes o no existir disponibilidad (5). Perú, un país con 15, 9 camas por mil habitantes (6) y tan solo 1528 camas en UCI para el Covid-19 (7) .

La destrucción del sector público educativo ha llevado al país a ocupar los últimos lugares en el sistema de evaluación PISA de la OCDE (8) y va a la zaga de los países latinoamericanos en educación. En 2019, el Perú solo dedicaba el 3,9% de su PIB en educación (BM, 2020) (9).

La infraestructura escolar en las zonas rurales es catastrófica, casi sin material educativo. Los maestros de las zonas rurales hacen malabares para poder enseñar. Esta situación será determinante y discriminante frente a la pandemia (10). Las políticas introducidas por el gobierno del expresidente Fujimori de liberalización del sistema educativo, mermaron en la educación pública y permitieron el surgimiento de miles de centros educativos privados primarios, secundarios y universitarios (BALARIN, 2016). La educación se convirtió en un negocio. La calidad educativa se puso en jaque.

La pandemia termina de destruir el resto y deja casi 6,5 millones de desempleados, formales o informales (11), en un país con el 70% de la población trabajando en la informalidad (12). Somos el primer país del mundo en tasa de mortalidad por Covid-19 por 100 000 habitantes. Ante esta situación, fueron los pueblos indígenas los que primero sufrieron los estragos de la pandemia, el contagio afectó a 25 mil personas en el año 2020. Y causó la muerte a miles.

La situación de abandono está estrechamente ligada a la colonialidad, gobiernos sucesivos que no consideran al indígena como un poblador a cabalidad; gobiernos que ven al territorio como fuente de ingresos y no de vida; gobiernos que esperan que los indígenas se enganchen como caballos a la carreta del “desarrollo capitalista”.

En el 2019, se mantuvo el “ratio” de 67,5% de los conflictos con causa socioambiental. Entre el año 2019 y el inicio del año 2020, los conflictos aumentaron en un 6%, los conflictos socioambientales representaron el 78,4% de ellos, de los cuales la minería provocó más del 64% y los hidrocarburos más del 16% (Defensoría del Pueblo, 2020).

Con el mal manejo de la crisis provocada por la pandemia, el expresidente Vizcarra perdió popularidad y comenzaron las investigaciones en su contra por presuntos delitos de favores de empresas constructoras cuando era gobernador regional. Pero seguía siendo una piedra en el zapato por llevar adelante las investigaciones de corrupción de 68 congresistas y sacar adelante la reforma universitaria; muchos congresistas son dueños de universidades privadas que no recibieron la acreditación correspondiente. Esta situación evidencia una pugna por el poder entre las derechas, entre derechas rancias y prepotentes y otras más laxas. El descontento popular aumentó (13) ,los bonos no llegaron, la salud siguió en un estado catastrófico, la prensa denunció estafas y favoritismos a nivel gubernamental, los estudiantes y escolares se quedaron meses sin estudiar a falta de internet. Miles de violaciones, desapariciones y feminicidios fueron denunciadas, miles de niñas se convirtieron en madres (14) . La mujer sufre otra vez la doble opresión. No hay luz a la salida del túnel, del largo túnel.

Esta situación fue aprovechada por el congreso corrupto que inconstitucionalmente dicta la vacancia del presidente Vizcarra. Con los votos de la izquierda en el congreso. Pero fue la gota que rebasó el vaso. La juventud convoca a manifestarse. Las calles se abarrotan y exigen la renuncia de los corruptos en el poder. El inconstitucional nuevo gobierno de Merino dura cinco días. Dos jóvenes son asesinados, sus imágenes se difunden ampliamente y levantan indignación. El gobierno de facto se ve obligado a renunciar.

Este tipo de protestas no llegó todavía a conectar con los asesinatos de los defensores ambientales que se dieron ese año. Subsiste la distancia física y emocional de la capital con las provincias, y la falta de conciencia y de reconocimiento por las causas ambientales.

### Y llega el maestro de pura casualidad

Con el llamado a elecciones comienza la carrera en pos de los partidos con inscripción oficial. Ya hace muchos años que la política se había convertido en un negocio, los partidos políticos de hoy tienen poca tradición y aún menos ideología. Se han convertido en meros carros de batalla que cargan con todo aquél que quiera y pueda subir, muchas veces mediante financiamiento. Es así que el maestro chotano Pedro Castillo Terrones inicia su búsqueda de partido y es Perú Libre que se ofrece como vientre de alquiler, ya que su fundador, Vladimiro Cerrón (ex gobernador regional de Junín) está inhabilitado de ejercer cargos públicos por tener una sentencia por corrupción. Al mismo tiempo, la fiscalía pide 30 años de prisión para Keiko Fujimori. Esto explicará en parte su ansia desesperada por llegar al poder para gozar de impunidad.

La mafia fujimorista y sus aliados, levantarán los viejos argumentos de terrorismo y comunismo en contra de Castillo, argumentos que no lograron prosperar. El pueblo sale exhausto de la pandemia y de la corrupción. Las diferencias físicas raciales y culturales fueron enarboladas para tratar de demostrar que el país no podía ser gobernado por un “ignorante”. La fractura social y cultural se acentuó. Pero lo que realmente está en juego es el poder y el dinero.

Al consolidarse la figura del futuro presidente, que tenía o no tenía programa, el dólar se disparó y los pronósticos económicos se vinieron a la baja. Rápidamente, al ver el triunfo inminente, los empresarios, que nunca pierden, se reacomodaron a la nueva figura electoral llamando a la Sra. Fujimori a aceptar los resultados electorales. Era mejor para ellos estar bien con dios y con el diablo, son dueños de muchos proyectos extractivos en el país. El consejero económico de Pedro Castillo salió corriendo a calmar a los mercados. El dólar bajó, el BM augura un 10% de crecimiento este año.

Y la ecología? Nadie habla de ella, inexistente. Y los defensores ambientales? Castillo quiere que las empresas extractivas tributen más, su lema “no más pobres en un país rico” es un lema justificado por siglos de abusos y explotación y muerte. Dependerá del nuevo gobierno no retomar las sendas extractivistas y no basar toda su economía en la extracción. Porque los pueblos indígenas y otros, lo esperan a la vuelta de la esquina para reclamarle salud y seguridad alimentaria, y la protección de sus tierras. Esperemos que el gobierno de Castillo recapacite en cuanto a sus afirmaciones sobre la ideología de género, su posición anti-aborto y la diversidad de géneros.

Pero su gobierno no la tendrá fácil. Lo demuestra las últimas artimañas del actual congreso que ha convocado a una cuarta legislatura, justo antes de entregar el poder el 28 de julio, con el propósito de dictar leyes que dificulten su gobierno, leyes constitucionales como el voto de confianza. Además, el nuevo congreso electo le es adverso, la mafia fujimorista y sus aliados son mayoritarios. Se esperan tiempos muy difíciles en los cuales una vieja derecha rancia, que llama al golpe militar a gritos, luchará el todo por el todo para conservar su poder. Pero frente a esto están los pobladores olvidados del Ande que también lucharán el todo por el todo para ganar en reconocimiento.

#### Referencias

1 **https://lavajato.ojo-publico.com/**

2 Transparency Internacional, 2020, Our work in Peru, <https://www.transparency.org/en/countries/peru#>, 2 de agosto 2020

3 https://www.who.int/countries/per/es/

4 <https://www.inei.gob.pe/media/MenuRecursivo/publicaciones_digitales/Est/Endes2019/> https://repositorio.ins.gob.pe/handle/INS/1160

5 https://www.inei.gob.pe/media/MenuRecursivo/publicaciones_digitales/Est/Lib0387/indice.htm

6 https://www.minsa.gob.pe/reunis/recursos_salud/index_camas_hospitalarias.asp

7 https://covid19.minsa.gob.pe/sala_situacional.asp

8 https://www.oecd-ilibrary.org/education/pisa-2018-results-volume-i_5f07c754-en;jsessionid=yC0qNplGzZoIDgtx5xEDlZ-H.ip-10-240-5-116

9  https://datos.bancomundial.org/indicator/SE.XPD.TOTL.GD.ZS?locations=PE

10 https://www.defensoria.gob.pe/wp-content/uploads/2020/08/Serie-Informes-Especiales-N%C2%BA-027-2020-DP-La-educaci%C3%B3n-frente-a-la-emergencia-sanitaria.pdf

11 Fuente INEI, 2020

12 Cámara de Comercio, Producción, Turismo y Servicios, https://perucamaras.org.pe/

13 INEI, 2020

14 Ministerio de la Mujer y Poblaciones Vulnerables