---
title: Prender el fogón y hacer la comida
subtitle: La distribución desigual de la energía
cover: /images/uploads/len_a.jpg
caption: "Foto El Nacional "
date: 2021-04-07T18:30:10.588Z
authors:
  - Jefersson Leal
tags:
  - Leña
  - Mujeres
  - Desigualdad
  - Contaminación
  - Combustibles
categories:
  - Cuerpo y Territorio
comments: true
sticky: true
---
Hace poco más de un millón de años, un pariente muy cercano a los homos sapiens, logró dominar el fuego. Era el Homo Erectus, quien, sin saberlo, estaba cambiando radicalmente la forma de vida de los homínidos hasta entonces. Con el control del fuego, se logró la cocción de los alimentos, una mejor absorción de los nutrientes y poder, por fin, tener mayor dominio sobre la oscuridad.

**Pero la historia, pese a la creencia muy popular, no siempre es de avances. A menudo hay movimientos históricos que nos sitúan en situaciones que aparentemente se habían superado**. Como, por ejemplo, cocinar a la leña, que para algunos pareciera cosa del pasado, o a lo sumo una rareza de fin de semana.

Aunque la realidad, como de costumbre, dice otra cosa. En las zonas más empobrecidas del planeta la quema de biomasa (leña, carbón y estiércol) representa la principal fuente de obtención de energía para la cocción de los alimentos. **Esta realidad arropa al menos unas tres mil millones de personas, ubicadas en Asia, Latinoamérica y África.** (1)

La obtención de combustible es vital para el desarrollo de la vida diaria, todos los días hay que cocinar, pero la obtención de energía no sólida es un privilegio para una gran parte de la población terrestre, donde la electricidad, el gas licuado y el etanol no pertenece a la realidad cotidiana de las familias más vulnerables del planeta.

El monóxido de carbono, benceno, butadieno, e hidrocarburos poli-aromáticos son algunos de los componentes que se originan al utilizar la madera como combustible para cocinar. **Estos productos generan graves riesgos para la salud de aquellas poblaciones que están constantemente expuestas a ellos.**

**Dentro de este contexto son las mujeres, niñas y niños, quienes están en riesgo. Son las mujeres, mientras cocinan, las que pasan más horas al día inhalando las partículas que se originan por la quema de la madera.** Estas partículas se han asociado con el incremento de “…neumonía y otras infecciones agudas de las vías respiratorias inferiores entre los niños menores de 5 años. Es tres veces más probable que las mujeres expuestas al humo interior sufran enfermedades pulmonares obstructivas crónicas (EPOC), como bronquitis crónica o enfisema, en comparación con las mujeres que cocinan con electricidad, gas u otros combustibles más limpios. El uso del carbón duplica el riesgo de cáncer de pulmón, en particular en las mujeres”.(2)

**Según la Organización Mundial de Salud, para el 2016, se contabilizaron 7 millones de muertes en todo el mundo relacionadas con la contaminación del aire.** Son las poblaciones más rurales las que se ven principalmente afectadas por este flagelo, ya que tienen poco acceso a combustibles no sólidos para cocinar los alimentos.

## **América Latina**

**En América Latina, cocinar en fogón es una realidad que viven miles de familias. Se estima que en la región “44 % de la población (…) carece de acceso \[a cocinas modernas], un 15 % tiene severas barreras para lograrlo**, especialmente en las zonas rurales; mientras un 29 % está en transición para tener servicios mejorados de cocción, principalmente en las ciudades” (4)

Para el 2007, al menos 19 millones de centroamericanos utilizaban el fogón como método de cocción de los alimentos. (5) Para el 2011 el Ministerio de Salud de Nicaragua estimó que “el 70% de las mujeres mayores de 60 años que cocinaban con leña en las zonas rurales de las provincias de Jinotega, León y Matagalpa padecían algún tipo de enfermedad pulmonar obstructiva crónica”(6)

**Paraguay calculó, para el año 2017, que un 44,2 % de la energía que se consumió ese año en el país provenía de la quema de biomasa, siendo la leña el elemento más utilizado**. En Perú, desde el 2007 se inició un programa que ha dotado al menos a 750.000 personas con cocinas a leña mejoradas (8)

## **Venezuela**

**Venezuela pese a ser la octava reserva de gas natural en el mundo, vive en la actualidad una de las peores crisis energéticas de su historia reciente.** Según el Director Gas Energy Latin América, Antero Alvarado, unas cuatro millones de familias en Venezuela cocina a leña por la falta de gas doméstico y electricidad constante en sus hogares.

Esta situación es el reflejo de la quiebra de la principal industria del país, la petrolera, que en los años recientes ha vivido un desmontaje sistemático de la extracción de petróleo, y por tanto de gas. Según Eudis Girot, dirigente sindical del sector petróleo, se estaría produciendo 9000 bls de gas propano, cuando la demanda de la población se ubica en unos 52.000 bls diarios (10)

En Venezuela, solo el 11 % de la población tiene acceso a gas mediante tubería, es decir el 89 % de las familias necesitan cilindros de gas propano para poder cocinar y hervir agua, pero **solo 6 de cada 10 familias tienen acceso al gas en cilindros** (11). Lo que ha ocasionado que buena parte de las familias que no tienen acceso al gas, ni flujo constante de energía eléctrica tenga que cocinar con fogones improvisados dentro de sus casas o en sus alrededores. 

Carolina, habitante de la Victoria Edo. Aragua, una región del centro de Venezuela, nos dice que a falta de gas, utilizaba “una cocinita eléctrica (…) con el caracol eléctrico nos hicimos una cocina como artesanal, (…) pero como el caracolito duraba un mes, mensualmente había que comprarlo (…) para que no se dañara tan rápido usábamos la leña (…) para cocinar las cosas que tardaban más, para poder rendir la vida del caracol” (12)

La población venezolana, ya bastante golpeada por la gran crisis económica que lleva ya 6 años, no puede costear los altos precios del gas propano en el mercado paralelo, donde una bombona de gas, puede costar de 3 a 10 salarios mínimos.

Añadido a todo esto, las familias tienen que lidiar con las constantes fallas de la energía eléctrica, Carolina nos dice **“procurabamos cocinar antes de que se fuera la luz (…) nos tocaron muchos casos que cocinando se iba la luz, la comida se quedaba allí, esperando que llegara la luz para poder cocinar”** (13)

Además, cuenta que “tenía que empezar a cocinar a las 9 o 10 de la mañana para poder almorzar a las doce” lo que dice que la actual situación de **escasez generalizada de gas propano en Venezuela afecta principalmente a las mujeres, ya que tienen que dedicar más horas a la cocción de los alimentos.** Escenario que las expone a las partículas y gases emanados de los fogones, que ponen en grave riesgo su salud.

Estamos en pleno siglo XXI, pero el acceso a la energía sigue siendo un privilegio que gozan muy pocos. Siendo las mujeres y las niñas una de las poblaciones más vulnerables, se colocan en riesgo cada vez que van a cocinar. Superar este panorama implica pensar un mejor acceso a energías no sólidas para las poblaciones que hoy cocinan en base a biomasa, pero también implica reflexionar sobre las relaciones desiguales entre hombres y mujeres.

### Referencias

(1)Energía doméstica y salud, combustibles para una vida mejor, p.

(2)*Ibidem*, p. 10

(3)<https://www.paho.org/es/temas/calidadaire#:~:text=La%20exposici%C3%B3n%20a%20la%20contaminaci%C3%B3n%20del%20aire%20en%20la%20vivienda,el%20c%C3%A1ncer%20de%20pulm%C3%B3n%2C%20el>

(4)https://elpais.com/economia/2020-11-10/cocinar-es-un-riesgo-para-casi-la-mitad-de-los-latinoamericanos.html

(5) https://www.americaeconomia.com/negocios-industrias/buscan-reducir-el-uso-de-lena-en-centroamerica

(6)Ver:https://elpais.com/economia/2013/05/26/agencias/1369580372_295003.html

(7)Ver: https://blogs.iadb.org/energia/es/energia-para-cocinar-en-america-latina-y-el-caribe-desafios-de-paraguay/

(8)Ver:https://www.latinamerica.undp.org/content/rblac/es/home/ourwork/climate-and-disaster-resilience/successstories/improving_cook_stoves_and_access_to_energy.html

(9)Ver: https://www.cambio16.com/4-millones-de-familias-en-venezuela-cocinan-con-lena

(10) *Ídem.*

(11) Ver: https://www.cinco8.com/periodismo/por-que-escasea-el-gas-para-cocinar-en-venezuela/

(12)Entrevista realizada para este artículo, vía telefónica.

(13) *Ídem*