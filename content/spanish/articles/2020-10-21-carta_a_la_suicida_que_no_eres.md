---
title: Carta a la suicida que no eres
subtitle: " "
cover: /images/uploads/simon-prades-01.jpg
caption: "Ilustración: La Tinta"
date: 2020-10-21T12:28:45.555Z
authors:
  - Roberto Salazar
tags:
  - Suicidio
  - SaludMental
  - Bienestar
  - Salud
  - Japón
  - Argentina
  - Familia
categories:
  - Cuerpo
comments: true
sticky: true
---
 

*Isla de Margarita, octubre 2020*

***Querida Mariana,***

No sé si lo leíste. Pero gente famosa se está matando en Japón.

Sé qué te parece extraño que te hable de esto. Hice silencio sobre el tema desde entonces. No quise hablar con nadie y sobre todo contigo. Hice lo que consideré mejor. En realidad, hice lo que pude.

Lo de los japoneses me recordó a aquel libro que me comentaste de pasada que leías, para que hablara de eso, pero yo seguía en mis treces. Después, lo compré y lo leí. Los suicidas del fin del mundo: *crónica de un pueblo patagónico*, de Leila Guerriero. Un pueblo en el remoto sur empezó a tener una ola de suicidios. De ese país donde estudiaste y te casaste. Ese país que nadie entiende.

Desde entonces empecé, de a poco, a investigar sobre porque la gente se mata. Digo, a leer, pero no a hablar. Propio de un científico, como dice mamá. No sé. Seguro te acuerdas del libro; nadie entendía porque en un periodo cortísimo varios jóvenes del pueblo, demasiados, decidieron acabar con sus vidas. Si la crisis económica, si las privatizaciones, si la falta de arraigo, si el frío, si el viento. Lo que dice Leila es que como empezó, acabó. Campaña de apoyo a los familiares y residentes del lugar de por medio. Al final a nadie le importó tanto porque era la periferia. Bueno, en realidad a nadie le importó porque nadie entiende el suicidio.

Japón es el fin del mundo también. Extremo Oriente, mas allá no hay nada. Tiembla bastante, es una isla, no es un gran clima. Su gente no expresa sus emociones con los demás, son “para adentro” como los sureños y aunque no lo creamos de este lado del mundo, tienen crisis económicas y hay gente que lo pierde todo. Japón y Argentina unidos. Que diría Simon Kuznets, aquel Nobel que los colocó como ejemplo de éxito y fracaso económico, respectivamente. Los economistas tampoco entienden nada.

Pero habría que preguntarle a tu marido, el psiquiatra, porque se suicidan los famosos. Si entendemos a los famosos como los exitosos. Menuda mentira. Cuando el show se acaba son humanos cualesquiera, diría Héctor Lavoe. Lo que seguro sabe tu marido ese tal efecto Werther, que dice que las muertes por suicidio son contagiosas si son difundidas de formas masiva, como el destino del personaje Werther, de ese escritor alemán que no leí, pero leía el abuelo. Werther se suicida por amor. Bueno, los europeos se empezaron a matar cuando leyeron esa novela. Yo pensé que era una exageración, pero las estadísticas dicen (y vaya que creo en ellas) que cuando se mató Marilyn, o se mató Kurt Cobain, que incluso cuando ¡Robin Williams! Aumentaron las tasas de suicidio.

Cuando lo de papá, no salió en ningún lugar, ni en la prensa, ni en la televisión. Apenas se lo susurraban a los vecinos, como si fuese una blasfemia. Yo no sé si las religiones lo ven como un atentado a sus dioses. Culpar al suicida no tiene sentido.

Tampoco a la familia. Difícil fue escuchar aquel sermón del médico que nos atendía en aquel momento. Decisión personal, circunstancias adversas, palabrerías vagas. Hay poco que decir. A ti te sirvió ir después a terapia. Yo nunca fui de hablar. Por ahí me equivoco. Hablar no está mal. En su momento, al menos.

Una vez le pregunté a tu marido, mientras conversábamos del futbol o de plantas, no recuerda ya, si servían realmente los tratamientos para los que se querían matar. Me vio con una mirada adusta. Me imagino que sospechaba de algún posible desequilibrio de mi parte. Tuve que aclararle que había leído el libro que me comentaste. Aunque aún desconfiando, me explicó que sí. Que hay políticas públicas serias, que sostenidas en el tiempo permiten reducir la incidencia de las muertes. Que hay tratamientos cada vez más especializados. Que la prevención, servicios de salud, condiciones de vida digna. Me pareció sensato todo lo que dijo. Le dije que no te comentara nada, creo que cumplió su palabra. Después me embarqué en leer más.

Seguro leíste a un tal Durkheim. Autor clásico de tu carrera. El tipo tiene un mamotreto que versa sobre el suicidio. Sabes cómo termina. No hay causa única del suicidio. Ni el clima, ni las crisis económicas, ni el sexo, ni la edad, ni la religión. Desde el siglo XIX seguimos sin entender nada. Quizás sepan los especialistas prevenirlo, tratarlo, pero que lo entiendan, pues no.

No quedé conforme y me fui a las estadísticas, a los meta-análisis. A las matemáticas, a mi mundo. Busqué la manera de establecer un modelo, una regresión múltiple, uno de los delirios de nosotros a los que nos gustan los números. Me descargue los datos de la OMS, de las distintas organizaciones. Me construí un bonito algoritmo que predecía la muerte por suicidio por país y por población. Incluso me atreví a diseñar uno por individuo. Bueno, querida, tenemos augurada una muerte por suicidio tu y yo según el algoritmo. El mayor peso en la varianza de semejante presagio se debe a que somos hijos de suicida y presenciamos la escena suicida. Y eso que no somos del fin del mundo.

Yo no presencié nada. Creo que siempre creíste eso. Ni siquiera cuando lo bajaron de la viga. No tuve el coraje. Solo llameé al tío Ricardo por instrucciones de mamá. Luego tú llegaste y pensaste que había visto. Nunca te lo aclaré. Solo asentí cuando la burocracia nos preguntó detalles. Nadie recuerda donde estaba yo o que hice. En ese momento, no se comprendía que ese detalle podría marcar la predicción entre los “suicidables”.

Por supuesto, mi creación no sirve para nada. Metí a papá en el algoritmo y arrojó que estaría con vida. Lo mismo para una lista de célebres suicidas. Lo dejé hasta allí. Hasta que vi lo de la noticia de Japón. Volví y se me ocurrió meter a un japonés suicida famoso de otro tiempo, Mishima, en la búsqueda y también me arrojó que no se hubiese suicidado. El algoritmo no entendía nada.

Cuando te fuiste a estudiar al exterior pensé que hacías bien. Así yo no tenía que seguir evadiéndote. Sé que eres inteligente y que a donde fueras te iría bien. Cuando te casaste me sentí feliz por ti, porque siempre bromeabas con papá de que no te ibas a casar. Creo que no pudiste elegir mejor esposo, aunque no me guste su aire intelectual. En todo caso, mi opinión no es tan importante. Me basta que seas feliz a pesar de papá. Él no nos acabó la vida.

En estos días, vi esa película de Lars Von Trier en donde celebra matrimonio una pareja y la novia está deprimida. Después se acaba el mundo, literalmente. Me dejó inquieto y no sé el por qué. Creo que porque me recuerda a esa novia loca que tuve una vez. Creo que porque todo se va al carajo y el personaje más cuerdo de la película se suicida antes del fin. Y la melancólica está ahí al final de todo y pese a todo.

Lo asocio (libremente, como diría tu marido) con un escritor que se llama Emil Cioran que seguro conoces. Es el tipo más lúcido que leí en mi vida, salvo quizás Einstein. Si quieres entender la sinrazón de la vida, en unas pocas páginas te transmite que estar vivo es una opción más, una posibilidad más. Nacer y morir, disfrutar y sufrir. No hay razón ni fundamentación de un carajo. Como citaba el abuelo, la vida es un cuento contado por un estúpido lleno de ruido y furia.

Pero no. Me resisto a eso. Me resisto a la no fundamentación. Me resisto a que no haya si no pura estupidez. Pero no porque no sea verdad, que lo es. No porque no haya sentido último, que no lo hay. Sino que la vida misma es un complot contra esa verdad. No hacemos si no burlarnos una y otra vez de la muerte, hasta que nos encuentre descuadrados, desprevenidos, cansados de sortearla.

Para matarse a uno mismo tiene que fallar ese engaño. Las conspiraciones contra la muerte tienen que ser por fin develadas, como un truco de magia del que nos enteramos su funcionamiento y que se hace obvio y vergonzoso. Al final es lo que hacen los terapeutas, re-enseñar trucos de magia a aquellos lúcidos que se dieron cuenta de que los dados están cargados a favor de la vida.

No, hermana, yo solo veo en nosotros unos grandes escapistas. Pese a que nuestro Houidini se nos ahogó, lo seguimos intentando. Aunque no nos compartamos el truco entre nosotros. Porque el mago nunca revela su número. Porque el mago es terco en creer que es sobrenatural lo que hace.

Creo en los sortilegios de los psiquiatras, de las instituciones mentales, de las líneas telefónicas de atención al suicida, de las mallas protectoras en los puentes, de las comunidades preocupadas por sus vecinos, de los políticos críticos contra las políticas de desterritorialización, de los comunicadores con información responsable, de los artistas que, aun mostrando el centro mismo de la vida y la muerte, se atreven a ensayar sus trucos para todos.

La película de Von Trier me inquieta porque al final la protagonista solo tenía una chocita endeble hecha con madera para protegerse de un planeta que esta por chocar con La Tierra. Pero siendo una melancólica, habiendo enloquecido todo el mundo a su alrededor, consiguió contagiar al niño que la acompaña la idea de que siempre es posible construir algo aun al final de las cosas. ¿No es un contagio digno del fin del mundo, desde Japón hasta la Argentina?

Creo que retomaré lo de mi algoritmo. La próxima vez mejor te llamo.

> ***Julián***