---
title: "El cine según Cortázar "
subtitle: “Si yo fuera cineasta me dedicaría a cazar crepúsculos…”
cover: /images/uploads/1_portada.jpg
date: 2020-08-31T15:59:42.858Z
authors:
  - Rafael Farrera
tags:
  - Cine
  - Cultura
  - Literatura
  - Argentina
  - Bélgica
  - Natalicio
  - Arte
  - Guión
categories:
  - Cultura
comments: true
sticky: true
---
En los albores de la Primera Guerra Mundial nacía un 26 de agosto de 1914, en algún lugar al sur de Bruselas, la capital de Bélgica, el niño que más tarde se convertiría en uno de los más importantes escritores de la literatura universal contemporánea. Pocas palabras se le pueden agregar a la biografía de Julio Cortázar, pilar fundamental de la generación dorada de escritores latinoamericanos y enormísimo cronopio.

Pero sobre la etapa como guionista de Cortázar y las adaptaciones de sus textos al cine quizás no se conoce tanto, a pesar de ser muy prolífica. El autor de Rayuela trabajó en el proceso creativo de alguna de las siguientes películas y en otras simplemente se quedó en el papel de crítico y espectador.

Con motivo de su natalicio realizaremos una aproximación, una lista con películas basadas en sus textos, tratando de aportar con los enlaces a las películas y los cuentos en PDF disponibles en internet.

- - -

## La cifra impar (1962)

{{< youtube 5irgKkPnWzw >}}

Manuel Antín se encargó se realizar tres adaptaciones, la primera de ellas es una cinta en blanco y negro inspirada en el cuento “Cartas de mamá”, hallado en el libro Las armas secretas de 1959. El relato sigue a Luis, quien se ha mudado a París para vivir con quien fuera la ex pareja de su hermano recién fallecido. Habiendo dejado atrás Buenos Aires, un día recibe una carta de su madre comentándole que su hermano muerto ha preguntado por él. Esta película logró captar la atención del propio Cortázar y entusiasmarlo para más tarde colaborar con Antín en sus siguientes proyectos.

### (Cuento) [Cartas de mamá](https://www.literatura.us/cortazar/cartas.html)

> “Es tan raro que el cine valga para mí como una experiencia realmente profunda, como eso que te da la poesía o el amor y a veces alguna novela y algún cuadro...” Le escribió Julio Cortázar a Manuel Antín en una correspondencia fechada al 10 de julio de 1962. Luego de su breve encuentro en Francia en el Festival de Cannes, ambos estuvieron intercambiando cartas donde hablaron sobre su gusto por Luis Buñuel, el movimiento surrealista y la corriente cinematográfica de la Nouvelle Vague. Dichas conversaciones fueron gestando lo que sería su próxima película.

- - -

## Circe (1964)

{{< youtube RBYXH6ENduU >}}

El resultado de esta unión entre Cortázar y Antín fue la adaptación del relato homónimo aparecido en el libro Bestiario de 1951. La historia se centra alrededor de la relación entre Mario y Delia, una mujer fatal que ha perdido a sus dos anteriores novios en extrañas situaciones, los animales y la muerte la persiguen, pero a pesar de los rumores que la rodean Mario queda hipnotizado por ella. La conversión de este texto lleno de influencias mitológicas a la gran pantalla es todo un reconocimiento al estilo literario del Cortázar guionista.

### (Cuento) [Circe](https://www.literatura.us/cortazar/circe.html)

- - -

## Intimidad de los parques (1965)

{{< youtube tYLs5YXguN8 >}}

Un año después llegaría la última de las adaptaciones de Manuel Antin de mano de Raymundo Calcagno y Héctor Grossi quienes combinaron dos de los cuentos de Cortázar: “El ídolo de las Cícladas” y “Continuidad de los parques” editados en el libro Final del juego de 1956.

De nuevo la trama se posa sobre otro triángulo amoroso conformado por Héctor, su esposa Teresa y su antiguo amante Mario, con quien recorren las ruinas de Machu Picchu en un viaje de estudios en donde hallan un monolito de piedra que les cambia la percepción de sus vidas. Cortázar no estuvo conforme con el resultado porque en el propio cuento la trama no se desarrollaba en Perú sino en Grecia.

### (Cuento) [Continuidad de los parques](https://narrativabreve.com/2014/07/cuento-julio-cortazar-continuidad-parques.html)

- - -

## El perseguidor (1962)

{{< youtube DWyz2oY8oCU >}}

Por esos mismos años otro argentino tenía el ojo puesto en la obra de Cortázar para realizar su ópera prima, en esta ocasión fue el compositor Osías Wilenski quien adaptó uno de los cuentos más emblemáticos del escritor: “El perseguidor” (Las armas secretas, 1959), texto que a su vez está inspirado en la vida de Charlie Parker, bajo el pseudónimo de Johnny Carter, un delirante y talentosísimo saxofonista avasallado por los excesos y un ritmo de vida frenético que lo llevaría a su propia destrucción.

Se comenta que Cortázar no estuvo satisfecho con el resultado final de la película porque no se pudo incorporar el planteamiento filosófico y los conflictos temporales expuestos en el cuento.

### (Cuento) [El perseguidor](https://www.ucm.es/data/cont/docs/119-2014-02-19-Cortazar.ElPerseguidor.pdf)

- - -

## Blow up (1966)

![](/images/uploads/6_blowup.jpg)

Quizás esta sea una de las adaptaciones más famosas de la obra de Cortázar. La película del reconocido director italiano Michelangelo Antonioni cuenta la historia de un fotógrafo de moda que por accidente captura la imagen de un posible asesinato y se obsesiona con resolver el misterio. La premisa está basada en “Las babas del diablo” de Las armas secretas de 1959, un cuento inspirado en una anécdota que Sergio Larraín, mítico fotógrafo chileno de la agencia Magnum Photos y amigo personal de Cortázar, le contó sobre una vez en París que realizando una serie de disparos de la Catedral de Notre Damme, capturó a una pareja teniendo relaciones sexuales justo en el borde del encuadre de la fotografía.

### (Película) [Blow-Up](https://www.ok.ru/video/1143445391967)

### (Cuento) [Las babas del diablo](http://biblio3.url.edu.gt/Libros/Cortazar/babas.pdf)

- - -

## Weekend (1967)

![](/images/uploads/7_weekend.jpg)

Otro importante cineasta internacional como lo fuera el francés Jean-Luc Godard, uno de los máximos representantes de la nouvelle vague, también se vio atraído por el universo cortazariano y se inspiró de manera libre en el cuento “La autopista del sur” (Todos los fuegos del fuego, 1966), para llevar a cabo Weekend, una película con un lenguaje poético y un estilo muy propio del director que se centra en el accidentado viaje por carretera de una familia al campo.

### (Película) [Le weekend ](https://ok.ru/video/1498351209161)

### (Cuento) [La autopista del sur ](http://www.secst.cl/upfiles/documentos/18072016_1145pm_578dbe61f36da.pdf)

- - -

## L´ingorgo (1978)

{{< youtube 0LwiV0G6ARA >}}

Así también, unos años más tarde, el director italiano Luigi Comencini de la mano de los primerísimos actores Marcello Mastronianni y Gerard Depardieu, llevarían al cine “El gran embotellamiento”, basado en el mismo cuento con el que trabajó Jean Luc Gordard.

Sin embargo, esta adaptación sería un poco más fiel al texto, en la cual un gran atasco que se prolonga extensamente en el tiempo, someterá a sus personajes a participar en ese mini ecosistema social creado en la propia ruta. Por tratarse de la misma premisa, resulta interesante apreciar los distintos puntos de vista con que cada director abordó la historia.



- - -

## Furia (1999)

![](/images/uploads/9_furia.jpg)

Con una joven y desconocida Marion Cotillard a la cabeza del reparto, el director francés Alexandre Aja probó suerte con su adaptación del cuento “Graffiti” (Queremos tanto a Glenda, 1980). Ambientada en la época de la dictadura militar en Argentina, la película busca recrear la sensación de vivir en una sociedad bajo el yugo de un régimen totalitarista donde las expresiones mínimas de libertad como el amor o el arte se encuentran prohibidas por la autoridad.

### (Película) [Furia](https://archive.org/details/90db35b85b_201807)

### (Cuento) [Graffiti](https://www.literatura.us/cortazar/graffiti.html)

- - -

## Mentiras piadosas (2008)

![](/images/uploads/10_mentiras_piadosas.jpg)

Esta película dirigida por Diego Sabanés genera una sensación bastante similar a la provocada por el cuento en el que se basa: “La salud de los enfermos” (Todos los fuegos del fuego, 1966); donde las fronteras entre la ficción y la realidad se difuminan cuando el hijo menor de la familia se va a París a incursionar en su carrera de músico, pero van pasando los meses y el chico no se comunica, al no recibir noticias de él, la madre muy enferma se preocupa y todos los demás miembros de la familia deciden falsear unas cartas provenientes de Europa para para preservar la salud de la matriarca.

### (Película)[Mentiras piadosas](https://ok.ru/video/1567823301321)

### (Cuento) [La salud de los enfermos](https://www.literatura.us/cortazar/enfermos.html)

- - -

## Diario para un cuento (1998)

![](/images/uploads/11_diario_para_un_cuento.jpg)

Hasta la fecha la única película en tener al propio Cortázar como personaje principal fue la realizada por la cineasta checa Jana Bokova, quien se quedó prendada por la figura del escritor tras su viaje a la Argentina como realizadora de documentales de la cadena BBC sobre el tango y el folclore rioplatense. En la película el nombre del protagonista es Elías, un traductor de profesión apasionado por el Jazz, la lectura de ciertos autores muy particulares y otros guiños que asemejan a las vivencias de un joven Cortázar en Buenos Aires en la década de los 50. Un relato basado en el cuento de título homónimo a la película y que se encuentra en el último de sus libros: Deshoras, 1982.

### (Cuento) [Diario para un cuento](https://www.literatura.us/cortazar/diariopa.html)

[](https://www.literatura.us/cortazar/diariopa.html)

Y así continúa la lista con otros títulos menos difundidos y de difícil acceso a ellos en línea, siendo esto una demostración de la trascendencia del autor y su estrecho vínculo con el cine. En el cuento “Cazador de Crepúsculos” publicado en el libro Un tal Lucas de 1979, Cortázar reflexiona sobre su experiencia si se hubiera dedicado al cine.

> *“Si yo fuera cineasta me dedicaría a cazar crepúsculos. Todo lo tengo estudiado menos el capital necesario para la safari, porque un crepúsculo no se deja cazar así nomás, quiero decir que a veces empieza poquita cosa y justo cuando se lo abandona le salen todas las plumas, o inversamente es un despilfarro cromático y de golpe se nos queda como un loro enjabonado, y en los dos casos se supone una cámara con buena película de color, gastos de viaje y pernoctaciones previas, vigilancia del cielo y elección del horizonte más propicio, cosas nada baratas. De todas maneras, creo que si fuera cineasta me las arreglaría para cazar crepúsculos, en realidad un solo crepúsculo, pero para llegar al crepúsculo definitivo tendría que filmar cuarenta o cincuenta, porque si fuera cineasta tendría las mismas exigencias que con la palabra, las mujeres o la geopolítica.*

![](/images/uploads/12_cierre.jpg)

- - -

## Bibliografía:

* [Enfilme](https://enfilme.com/notas-del-dia/julio-cortazar-y-el-cine)
* [Culturamas](https://www.culturamas.es/2013/09/25/cortazar-guionista-2/)
  -[CulturaColectiva](https://culturacolectiva.com/cine/10-peliculas-basadas-en-los-cuentos-de-cortazar)
* [Telam](https://www.telam.com.ar/notas/201408/75289-10-peliculas-basadas-en-cuentos-de-cortazar.html?utm_content=buffer08ca6&utm_medium=social&utm_source=facebook.com&utm_campaign=buffer)
* [Enfilme](https://enfilme.com/notas-del-dia/julio-cortazar-y-el-cine)
* [El Espectador](https://www.elespectador.com/noticias/cultura/cortazar-y-el-cine/)
* [Yaconic](https://www.yaconic.com/cortazar-en-el-cine/) 
* [MoreliaFilmfest](https://moreliafilmfest.com/la-obra-de-julio-cortazar-en-el-cine/)