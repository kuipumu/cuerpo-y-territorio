---
title: "Y ahora ¿quién podrá defendernos? "
subtitle: " "
cover: /images/uploads/abandoned-theatre-in-pripyat.jpg
date: 2021-06-07T19:12:48.351Z
authors:
  - Karol Dinamarca
tags:
  - Cultura
  - Chile
  - Pandemia
  - Constituyente
  - Arte
categories:
  - Cultura
comments: true
sticky: true
---
**En Chile, acabamos de vivir un proceso eleccionario histórico y excepcional**. En esta ocasión y por primera vez, se elige gobernador regional, cargo que viene a reemplazar la figura del Intendente, una de las mayores autoridades a nivel nacional. Como si esto fuera poco, también tuvimos la oportunidad de elegir a quienes serían los miembros **constituyentes** redactores de nuestra nueva carta magna, o sea, los escritores de nuestra **nueva constitución**.

Debido al **estallido social** iniciado durante el año 2019, se originaron muchas polémicas con respecto a esta constitución, algunas de ellas con respecto a su origen y redacción. En efecto, este texto constitucional fue promulgado el año 1980, en pleno periodo de dictadura militar del general Augusto Pinochet.

Los resquemores con respecto a este texto no solo abarcan su origen temporal, sino también a sus constantes reformas: más de 50 hasta el año 2020, realizadas en período de democracia

A pesar de todos los cambios realizados, esta carta magna mantiene un **aire de ilegitimidad.** Concepto que surgió fuerte y claro, en cada nueva protesta multitudinaria en muchos lugares de Chile.

A pesar del obvio descontento popular, la baja convocatoria a las urnas de la población chilena, abrió un nuevo **debate político con respecto al voto**. Se suponía que en esta ocasión, la mayoría de las personas que participaron en el plebiscito 2020, también lo harían en las elecciones municipales y constituyentes En 2020 las cifras oficiales del Servicio Electoral de Chile (SERVEL) arrojaron históricas cifras de participación de votantes, concluyendo que esa votación tuvo la mayor participación desde la instauración del voto voluntario en el año 2012. Con el 99,85% de las mesas escrutadas, la opción **"Apruebo"** se impuso con el **78,27%** de los votos (5.886.421 de votos) mientras la alternativa **"Rechazo"** alcanzó el **21,74%** de las preferencias, con 1.634.107 de sufragios.

Un escenario muy distinto arrojó la votación del mes de mayo. Según la última actualización del 17 de mayo solo un 43% de la población apta para votar participó en esta elección. Probablemente, la gran ganadora fue la **abstención**. Con estos resultados, las preguntas comienzan a aparecer: ¿estamos siendo realmente ciudadanos activos en las decisiones de nuestro país?, ¿debe volver a ser obligatorio el voto? ¿El proceso electoral es un sistema caduco?

Por suerte, en estas elecciones fueron elegidxs muchos candidatxs por **listas independientes** y también candidatxs con carreras afines a redactar nuestra nueva constitución (abogados constitucionalistas, profesores, etc)

**La presencia femenina** también arrojó un alto porcentaje de elección, circunstancia muy esperada por todxs nosotrxs, debido a la lucha contra las desigualdades que venimos dando hace ya tantos años.

Esta situación arroja nuevas conclusiones. Dos de ellas llaman mucho mi atención: la evidencia del **descrédito hacia los partidos tradicionales**, ya que no lograron convencer al pueblo chileno de sus "propuestas sociales", alcanzado un muy bajo porcentaje de escaños en la elección de constituyentes y municipales. La gobernación de Santiago, capital de Chile, se debate entre dos rivales de partidos totalmente distintos; uno de ellos es Claudio Orrego, perteneciente al partido Democratacristiano y Karina Oliva, militante del "partido Comunes". Serán unas elecciones emocionantes, porque ambos candidatos representan a distintos estratos sociales (o eso nos quieren hacer ver) y eso puede generar grandes sorpresas a la hora de la votación. Situación similar ocurre con las **próximas elecciones presidenciales**, ya que, al parecer, existen dos representantes de distintas bancadas que estarían peleando fuertemente el sillón presidencial. **¿A qué partido político le interesará más la cultura? ¿A la derecha dura o al comunismo?**

La segunda situación que llama especialmente mi atención, es que el tema **cultural y artístico** del país aún sigue en **completo abandono**. Tengo amigos y conocidos profesionales de la música que decidieron embarcarse en una carrera **política constituyente** o **municipal**, creando diversos proyectos en donde incluían el acercamiento de la población a la **educación musical**, conciertos de música de distintos estilos, planes artísticos, etc. Lamentablemente, **no alcanzaron la votación mínima** para ganar un puesto, quedando fuera de competencia, lo que me hace pensar en quién será nuestra cara o imagen representativa ante la redacción de la nueva Constitución o ante las Municipalidades, cuando a la mesa de discusión se presente el ítem artístico-cultural. ¿Qué ocurrirá cuando se planteen debates acerca de derechos culturales y sobre si estos pueden ser incluidos en la nueva Constitución? **¿Quiénes representarán nuestro sentir y nuestras necesidades?**

Mientras escribo esta columna, recibo noticias desde Instagram y páginas de periódicos acerca de una famosa empresa de telecomunicaciones que lanza una campaña publicitaria muy desafortunada, en donde sus protagonistas no gozan ni un poco con alguna manifestación artística, dejando en claro que su único interés es el fútbol. Y por supuesto que el spot publicitario saca ronchas. Estamos de acuerdo en que todos tenemos derecho a disfrutar de distintos hobbies y a ser felices a través de ellos; con lo que no estamos de acuerdo, es con que la **publicidad vaya en desmedro de nuestro rubro**. Muy mala jugada ahí. Claro, tal cual como hace el gobierno, la empresa de telecomunicaciones no logra conectar con las necesidades del público al cual quiere vender su producto.

También me entero de que se produjo la cuenta pública 2021, a cargo del actual presidente Sebastián Piñera. Al parecer, se vislumbra una cuenta sin muchos anuncios de peso, sobre todo para educación y solo algunos avances sobre cultura. Nada se habló con respecto a la **precarización del trabajador artístico** ni sobre los dramáticos cierres de espacios laborales. No se habló de ayudas, bonos o leyes que amparen de alguna forma al rubro. A pesar de diversas manifestaciones, entrevistas, discursos y comunicados de parte de técnicos, actores y músicos, el gobierno vuelve a caer en el “modus operandi” de siempre: no escuchar, no ver y no tener empatía con su propio pueblo.

Termino esta columna enviándoles **mucho apoyo** a todos los colegas que han sufrido las consecuencias de esta muy mal manejada **Pandemia**. Debemos estar atentos, apoyarnos y resistir. Ante el abandono, sólo **nos queda resistir.**