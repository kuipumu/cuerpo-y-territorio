---
title: La ciudad y sus barrios
subtitle: Sobre la división social del territorio en América Latina
cover: /images/uploads/caracas.jpg
date: 2021-04-15T20:19:19.643Z
authors:
  - Luis Gabriel Aparicio
tags:
  - Ciudad
  - barrios
  - latinoamerica
  - desigualdad
  - margenes
  - urbano
categories:
  - Territorio
comments: true
sticky: true
---
“*América Latina es la región más desigual del mundo*”, esta es una afirmación con la cual nos encontramos muy frecuentemente en investigaciones, estudios y noticias.

¿Pero cuáles son las implicaciones reales de esta frase?

Ciertamente América Latina no es la región que más concentra riqueza en el planeta aunque tampoco es la más pobre. No es la región que produce más alimentos, ni la que más exporta recursos naturales, aunque parte importante de los alimentos y recursos energéticos que se consumen en el mundo se produzcan en esta parte del mundo.

¿Pero entonces cómo se afirma que está región es la más desigual del mundo?

**Pues es un tema de concentración y distribución de los recursos que dispone. En América Latina es en donde peor se distribuyen los recursos entre sus habitantes,** es decir, donde la brecha entre quienes poseen grandes riquezas con quienes están bajo el umbral de la pobreza es más amplia que en cualquier otra parte del planeta.

**Según un informe del PNU del año 2019, el 10 % más rico en América Latina concentra el 37% de la riqueza, mientras el 40% más pobre recibe menos del 13% de los ingresos** (1).

Estas diferencias económicas y sociales, que de por sí se encuentran muy marcadas, también se encuentran expresadas en el territorio y en cómo se encuentran distribuidos espacialmente sus habitantes.

**Aunque las diferencias y contradicciones entre el campo y la ciudad son un tema muy importante, en esta ocasión vamos a hablar sobre la distribución del territorio dentro de las ciudades**.

Las grandes ciudades latinoamericanas comparten muchas características en común, entre ellas, la no integración de toda la población en sus núcleos urbanos y la presencia de asentamientos autoconstruidos en el margen y dentro de las fronteras de la ciudad, son sin duda de las más resaltantes.

En todo el continente existen diferentes términos para referirse a los espacios autoconstruidos **“Barrios” en Venezuela o Colombia, “Favelas” en Brasil, “Tugurios” en Centroamérica, “Villas miseria” al sur del continente, que coinciden con los términos “Squatters” y “Slums” en los países angloparlantes.**

Se pueden definir como “Asentamientos residenciales de desarrollo progresivo, construidos a partir de invasiones de terrenos que no pertenecen a sus residentes y sin un plan, o más específicamente, sin un proyecto que cubra los requerimientos que debe contemplar cualquier urbanización producida reguladamente en la misma ciudad…” (2)

**En concreto se trata de zonas pobladas al límite de la ciudad o dentro de ella, pero marginadas en su concepción sociológica,** pues no se encuentran urbanizadas, y generalmente registran muchas carencias en cuanto a servicios, salud, educación y empleo, por lo que se caracterizan por tener altos índices de pobreza entre sus habitantes. (3)

Cada barrio tiene un origen distinto, pero en general se trata de poblaciones que han migrado en diferentes momentos desde los campos, otras regiones, e incluso países, hacia las ciudades en busca de mejores condiciones de vida, trabajo y educación.

Y que al no poder acceder a terrenos, viviendas o alquileres dentro de la ciudad por la falta de recursos o los altos costos, optan por la construcción en terrenos que carecen de valor comercial para el mercado, es decir en cerros, colinas, quebradas, etc.

**En una gran parte de los países de América Latina se da un proceso de urbanización acelerada desde mediados del siglo XX**, y aunque con sus particularidades en cada país, algo que tienen en común estos procesos es que han ido creciendo paralelamente tanto la parte planificada o formal de la ciudad, como la parte no planificada o informal.

Agustín Blanco Muñoz (4) nos dice sobre este tema, que para que una población pueda ser considerada “urbana”, debe poder incorporarse de forma efectiva e integral a los bienes de la urbanización y a los beneficios de los servicios urbanos.

**Es decir, que las poblaciones que habitan los barrios autoconstruidos, aunque estando dentro de los límites de ciudad, se encuentran al margen de lo “urbano”**, pues no son población totalmente integrada a la ciudad ya que no son beneficiarios directos de la urbanización en las ciudades.

Esta división económica y social del territorio, es la que produce que dentro de un mismo espacio urbano conviva la riqueza y la pobreza, urbanizaciones y barrios. A veces visibles y otras veces detrás de grandes muros para evitar que se note la desigualdad en la cual conviven los habitantes de la ciudad.

En los barrios, en general, el Estado siempre ha sido el mayor ausente, desde sus orígenes a principios del siglo XX hasta la actualidad. Las políticas dirigidas a estos espacios durante muchos años fueron las de invisibilizar su existencia o la eliminación de los mismos, y la reubicación de sus habitantes.

**Estas políticas asistenciales nunca llegaron a aportar una solución real o definitiva en donde fueron implementadas, ejemplos de esto se dieron en la ciudad de Caracas en la década de 1950 o en Río de Janeiro durante las décadas 1960 y 1970** (5). En donde se emprendieron planes para la eliminación de barrios o favelas y la construcción a gran escala de viviendas para reubicar a los pobladores, pero que terminaron siendo esfuerzos insuficientes ante la cantidad de viviendas y habitantes.

Programas de este tipo se implementaron y aún se implementan en varios países del continente, aunque el consenso general durante las últimas décadas de teóricos e investigadores expertos en el tema, es el abordaje conjunto entre Estado y comunidades para la intervención de los espacios y el mejoramiento de las condiciones de vida y dotación de servicios urbanos a estos espacios.

Principios como el del “reconocimiento al barrio” (6) que hablan sobre **visibilizar el barrio como un fenómeno social, y aceptarlo como una parte integral en las ciudades han tomado gran fuerza durante los últimos años**, y han tenido resultados importantes en países como Brasil, Colombia y Venezuela.

**Cada barrio y sus habitantes están dotados con elementos históricos, culturales e identitarios que los vinculan con el espacio que habitan, ya que ellos han contribuido a construirlo con su trabajo y esfuerzo**. Lo que hace que se cree un vínculo con el territorio y la comunidad que se habita, más allá de las condiciones en las cuales puede encontrarse la vivienda y su entorno.

Uno de los mayores desafíos que afronta América Latina en las próximas décadas es la reducción de la desigualdad en cada país. Esto también implica la integración de todos los habitantes de las ciudades al disfrute de los derechos de la ciudad, que en casos como Caracas (donde casi 50% de lo construido es autoconstrucción en los barrios) o Río de Janeiro (que concentra la mitad de su población en las Favelas), se convierte en un tema de primer orden para sus habitantes.

### Referencias

(1) Gerardo Lissardy, Por qué América Latina es "la región más desigual del planeta", BBC News Mundo, Nueva York, 6 febrero 2020. <https://www.bbc.com/mundo/noticias-america-latina-51390621>

(2) Sosa, Allan Y, Análisis al Discurso de las Elites (Acerca de la Formación de Barrios en Caracas entre junio de 1957 a julio de 1958), Trabajo Especial de Grado, Universidad Central de Venezuela, Facultad de Humanidades y Educación, Escuela de Historia, 2009.

(3) Aparicio, Luis G, La construcción de la Caracas moderna como desafío de integración urbana en América Latina (Estudio histórico sobre la construcción de la ciudad de Caracas y sus desigualdades desde el siglo XX hasta el siglo XXI), Caracas, Banco de Desarrollo de América Latina (CAF), 2019. <https://www.caf.com/50/media/3132/la-construcci%C3%B3n-de-la-caracas-moderna-como-desaf%C3%ADo-de-integraci%C3%B3n-urbana-en-am%C3%A9rica-latina-3278_5940_pol%C3%ADticas_p%C3%BAblicas.pdf>

(4) Blanco Muñoz, Agustín, Oposición entre ciudad y campo en Venezuela. UCV, FACES división de publicaciones, 1974.

(5) Styliane Philippou, La integración de las Favelas de Brasil en el Foro Democrático de la Ciudad, Cuba, 16° Convención Científica de Ingeniería y Arquitectura, Instituto Superior Politécnico José Antonio Echeverria en La Habana, 2012. <https://www.researchgate.net/publication/264496380_LA_INTEGRACION_DE_LAS_FAVELAS_DE_BRASIL_EN_EL_FORO_DEMOCRATICO_DE_LA_CIUDAD_THE_INTEGRATION_OF_BRAZIL'S_FAVELAS_INTO_THE_DEMOCRATIC_FORUM_OF_THE_CITY>

(6) Bolívar, Teolinda y Baldó, Josefina, La Cuestión de los barrios, Caracas, Monte Avila Editores, Fundación Polar, Universidad Central de Venezuela, 1996.