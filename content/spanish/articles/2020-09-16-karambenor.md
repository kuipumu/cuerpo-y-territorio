---
title: KARAMBENOR
subtitle: Mujeres senegalesas en Argentina
cover: /images/uploads/karambenor4.jpg
date: 2020-09-15T23:15:05.797Z
authors:
  - Florencia Cencig
tags:
  - Senegal
  - Mujeres
  - Género
  - Cultura
  - Argentina
  - Migración
  - Baile
  - África
  - Covid19
categories:
  - Género
comments: true
sticky: true
---
<!--StartFragment-->

Al compás de tambores y movimientos corporales que te invitan a perpetuar una sonrisa de oreja a oreja y volar a través de los ritmos africanos, **“Karambenor”, reivindica sus raíces**, a partir de encuentros desbordados de bailes, celebración y música.

Se trata de una agrupación creada en el año 2012, conformada por mujeres que residen en Argentina, provenientes de Casamance, una región al sur de Senegal y alejada de la capital, Dakar. La misma tiene como objetivos; **visibilizar su cultura a través de diversas prácticas autogestionadas**, promover acciones que permitan su integración a la sociedad argentina, y generar sistemas de apoyo y ayuda mutua entre los ciudadanos de la Diola.

A través de reuniones mensuales caracterizadas por comidas y vestimentas típicas de su país, elaboran estrategias y programas de difusión cultural, como también, organizan eventos con el fin de recaudar fondos para sostener a la comunidad y cooperar con otros migrantes recién llegados.

![](/images/uploads/karambenor3.jpg)

Pero, **¿qué sucede con estas mujeres fuertes en tiempos de pandemia?**, donde se imposibilitan esos abrazos cálidos entre hermanas senegalesas, que las hacen sentir un poco más cerca de casa, y donde los festejos africanos están completamente silenciados.

Sus encuentros tuvieron que resignificarse y adoptar la virtualidad como forma de contacto. A través de las pantallas, se reúnen para repensar y proyectar las actividades que llevarán a cabo cuando se pueda volver a la ansiada “normalidad” y las celebraciones no nos parezcan extrañas y peligrosas.

Pero, paralelamente a las tareas que planean a futuro, **ellas establecen redes de contención que se intensificaron en medio de la crisis sanitaria y económica que estamos viviendo**. Como he mencionado en el artículo anterior, son varias barreras y problemáticas que atraviesan los africanos al llegar al suelo argentino, las cuales se potenciaron, aún más, debido a la alarmante situación actual.

Las integrantes de la agrupación, en su mayoría, son vendedoras ambulantes, con lo cual, **la pandemia arrasó con sus ingresos económicos y debilitó profundamente su actividad laboral**. Muchxs senegalesxs, frente a la ausencia estatal y las constantes amenazas de desalojo, por parte de los propietarios de las viviendas y pensiones que alquilan, no tuvieron otra opción que romper con el aislamiento social y volver a la venta callejera para poder sobrevivir.

![](/images/uploads/karambenor5.jpg)

Debido a esto, en las últimas semanas, **fueron víctimas de la represión policial**, algo que no es novedoso para ellxs, ya que con o sin pandemia, son objeto de la sistemática violencia institucional.

Desde Karambenor exponen que todo el trabajo de asistencia en medio de este contexto es llevado a cabo por A.R.S.A (**Asociación de Residentes Senegaleses en Argentina**), una organización sin fines de lucro que, además de difundir su cultura y construir sistemas de cooperación y ayuda para su comunidad, tiene como objetivo principal **promover la creación de un Consulado Senegalés en Argentina.**

Es paradójico que, teniendo una deuda enorme en cuanto a políticas migratorias, **este grupo de mujeres nos invitan a ser parte de su mundo**, celebrar sus raíces y fusionarlas con las nuestras. Por ello, mientras la realidad se trate de barbijos, distanciamiento social y alcohol en gel, podemos acercarnos a su universo africano a través de su facebook “[Karambenor Argentina](https://www.facebook.com/karambenorargentina/)”.

En su página encontramos diferentes contenidos que nos permiten viajar virtualmente y sentirnos en tierra senegalesa. Es un medio a través del cual, dan a conocer sus costumbres y difunden todos los eventos que diseñan y realizan. De modo que, en cuanto regresemos a las distintas actividades sociales, **sus integrantes proponen jornadas que son imperdibles para aquellos amantes de los encuentros interculturales**, en los cuales se puede aprender y disfrutar de sus tradiciones.

![](/images/uploads/karambenor6.jpg)

Más allá de las responsabilidades a nivel político y estatal, **pienso que socializar este tipo de organizaciones, es una forma de sostener los espacios autogestivos y apoyar a las minorías que, históricamente, son invisibilizadas** y a partir de las devastadoras consecuencias que nos está dejando la pandemia, necesitarán más que nunca, la solidaridad de toda la sociedad.

Karambenor significa “ayuda mutua” y si hay algo que nos enseñan los y las ciudadanas senegalesas es, justamente, la reciprocidad y la cooperación. Será momento, entonces, de entender que si a ese Otro le va bien, nos va bien a todos.

Por eso, para aquellas personas que, en medio de esta crisis mundial, reafirman sus lógicas y libertades individuales, **los invitamos a problematizar la forma delimitada con la que miran al mundo**, y a re-construirse con las bases de la conciencia social y de la identidad colectiva.

**¡Karambenor para todxs!**

<!--EndFragment-->