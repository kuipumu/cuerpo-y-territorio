---
title: Entrevista a Sor Alexa
subtitle: "  "
cover: /images/uploads/lamajadesnuda_1_.jpg
date: 2021-03-10T12:47:54.581Z
authors:
  - Roberto Salazar
tags:
  - IndustriaPorno
  - Porno
  - Sexo
  - Feminismo
  - ArtistaPorno
  - Latinoamerica
  - MundoPorno
categories:
  - Cuerpo
comments: true
sticky: true
---
Si algo nos genera fascinación y vergüenza en una suerte de matrimonio arreglado, por lo menos desde la invención de la cinematografía, es el porno. O lo porno. O la pornografía. Puesto que en la medida que nos acercamos a definirlo, a asirlo, a atraparlo, se desplaza un poco más, nos deja a la zaga cual tortuga de Zenón, **probablemente porque nuestro obstinado raciocinio, no siempre exento de mojigatería, compite contra un rival desigual: el deseo.** Dadas las condiciones, nos tendremos que servir de algún subterfugio que nos devele en parte los secretos de uno de los hijos dilectos, aunque no reconocido, de la contemporaneidad.

Desde Cuerpo y Territorio nos acercamos entonces a un alfarero que da forma a las fantasías infinitas, imposibles, que materializa a caballo entre repudio y aplausos (y cada vez más de esto último) las complejas idas y vueltas de la excitación de los cuerpos y la estimulación de las mentes. Sor Alexa, nombre artístico de una ex actriz, productora y directora de cine para adultos, además de estudiante de filosofía y letras que apenas rebasando la veintena de años **nos deslumbra con una visión rica, compleja y no siempre complaciente, del cine porno y sus (in)visibles tentáculos.**

A continuación, les ofrecemos parte de este muy interesante encuentro:

**CUERPO Y TERRITORIO:** Seguramente te han hecho hasta el cansancio estas dos preguntas “¿Por qué empezaste en el mundo porno?” y “¿Qué hizo tu familia al enterarse?” pero te haré una meta pregunta que sería más bien, ¿Por qué crees que te preguntan eso?

> **SOR ALEXA:** *Tienes toda la razón, suele ser la pregunta inicial de como llegue al porno la que busca romper el hielo. O por qué elegí este nombre artístico. La gente no sabe muy bien cómo abordar el tema en general, les intimida una artista porno y sobre todo si ya la han visto en acción (risas).*
>
>  *Creo que hay una suerte de cliché, sobre todo en Latinoamérica, de esperar encontrar una historia dramática detrás, como si no pudiera ser igual que elegir un oficio o profesión como cualquier otra. ¿Quién le pregunta a un obrero, a un taxista, a un médico, como se inició en ese mundo? Estoy seguro de que se vería raro (risas).*

**CT:** Muchas personas creen que hay una historia de necesidad económica, al tratar de acercarla a la prostitución, o una suerte de ninfomanía desbordada.

> **SA:** *Y si pasan ambas, ¡Bingo! Te respondo con mi caso: ni lo uno ni lo otro. Me gusta mi cuerpo, disfrutar del sexo, estar frente a una cámara, posar, actuar. Todo junto. La reacción del público es clave. Que te vean, que logres efecto en los demás, elogios. Eso está o estaba desde el principio. Piensa que mi generación creció viendo porno de una forma u otra, así que no es impensado de convertirte en lo que la gente llama estrella porno. Yo prefiero artista porno, me parece mucho mejor orientando.*

**CT:** Es decir… se trata de pasar un buen rato y si además con eso puedes vivir, buenísimo.

> **SA:** *Si, claro. Evidentemente a las artistas porno nos gusta coger, el sexo es importante en nuestra vida, pero hay algo que mostrarnos frente a la cámara, de hacerlo bien, de buscar lo erótico, de causar deseo que es central. Que no pasa solo por las ganas de tener sexo o ganar plata, aunque están ambas, claro.*

**CT:** No te sientes cómoda con la denominación estrella porno, que suena más al rótulo que da la industria fílmica y su manera de comercializar el porno.

> **SA:** *Si, está tomado del Star-System de Hollywood, de promocionar el nombre por encima de la producción. Algo ya anacrónico de cierto modo, puesto que en gran parte no se hacen grandes películas como hace 40 o 50 años, incluso 20. Cuando uno empieza en el porno la posibilidad de fama está presente, convertirse en una actriz porno reconocida mundialmente y los privilegios que eso tiene por supuesto que es algo que se puede anhelar.*
>
>  *¿Quién no quiere ser un cantante conocido en todo el planeta o que los libros que uno pueda escribir lo lean en todas partes? En el porno es igual, aunque como en otras artes eso te puede llevar por el camino incorrecto.*

**CT:** Haciendo cosas que no quieres o rodeándote de la gente incorrecta…

> **SA:** *Piensa que la mayoría de las artistas porno empiezan muy jóvenes. Algunas ni saben que lo que quieren, entran a probar. Les ofrecen villas y castillas, les dicen esto y otras cosas, pero queman sus expectativas muy rápidamente. La carrera porno es casi efímera para la mayoría de las chicas. Es duro porque de alguna manera tus videos quedan en la web posiblemente para siempre, la famosa huella digital. No siempre empiezas con la cabeza tan ordenada de lo que quieres hacer o trabajas con la gente que te pueda orientar correctamente.*

**CT:** Es una industria muy difícil, dominada por hombres, que hacen contenidos básicamente para hombres. Además, altamente concentrada en pocas productoras que dominan el mercado.

> **SA:** *Sí y una de las batallas que se ha venido dando desde hace algunos años es la de hacer una industria porno feminista. No solo producir contenido feminista, es decir de salir de ese centramiento en el hombre y su placer sexual, sino en un porno que valore el trabajo femenino, le pague adecuadamente, les mejore las condiciones laborales. En conclusión, darles más poder a las mujeres.* 
>
> *Esto es aún más difícil hoy día en que el porno gratis es dominante, que está en control de unos pocos que solo lo ven como un negocio. Hacer porno más cercano a lo artístico, algo que logran muy bien directoras como por ejemplo Erika Lust o Tristán Taormino, cine que disfrutan tanto hombre como mujeres, es bien difícil por un tema de financiamiento. La industria está concentrada en PornHub o sitios similares que tienen una cantidad de material casi infinito, gratuito y que se mantiene a base de publicidad y no de lo que los usuarios paguen por el producto.* 
>
> *Empezar con una productora de cero y hacerla sustentable es muy cuesta arriba.*

**CT:** Entiendo que tú también has dirigido y producido porno. Tengo que confesar que no he visto tus producciones, probablemente por algo de lo que cuentas, que no es fácil acceder al porno… ¿alternativo? O indie de cierto modo.

> **SA:** *A lo que trato de acercarme es a un porno que tenga un sentido más estético o por los menos para mí. El porno mainstream llega a ser aburrido en algún punto, centrado en el pene y la penetración. Además de absolutamente poco verosímil en algunos casos. Yo creo en reintroducir el juego erótico, esa proporción entre lo visible y lo oculto, lo explícito y lo implícito, ese cuidado en los detalles, en esa sensación de algo bien hecho, que es propio del arte.* 
>
> *No es volver al cine softporn, donde no haya escenas explicitas, o al viejo cine erótico demasiado centrado en la narración. Creo que eso ya no lo podremos disfrutar igual después de estos tiempos donde todo es transparente. Pero la pornografía puede hacerse menos obvia y a eso es lo que apunto cuando busco filmar algo.*

**CT:** Es decir, tu cine no es de acrobacias, sexo maratónico, eyaculaciones u orgasmos continuos y cuerpos irreales.

> **SA:** *Igual ese es un lugar común del porno, que todo es miembros masculinos enormes, tetas y culos perfectos en mujeres, y orgasmos fingidos. El porno abarca más cantidad de fantasías y cuerpos de lo que se piensa en primer momento. Los consumidores de pornografía encuentran fetiches y rarezas y para eso también hay producciones. Pero como la industria sigue la lógica de mayores ganancias, se tiende a hacer las escenas más solicitadas, más pop, por decirlo de alguna manera. Así que sí, está el porno de Pornhub.com, falocéntrico, mainstream, menstream, y existe el porno BDSM, gay, lésbico, feminista, fetichista, trans y pare de contar, que también tiene público y no menor.*

**CT:** Lo que nos lleva a preguntar algo que sabemos que es difícil de hacer, que es preguntarnos por lo que es el porno.

> **SA:** *El porno en este momento para mi es el material producido para generar excitación sexual y con un centramiento explícito principalmente en los cuerpos y en los orificios sexuales. Se le ha dado otros usos, que lo relacionan con todo aquello que sea excesivo, que rompa códigos, que vaya en contra de los tabús.* 
>
> *Se inventa el warporn por ejemplo, que tiene que ver con lo excitante que es ver escenas de sexo o de violencia en un contexto de guerra. O la pornografía de Marco Fiorito, que es porno extremo, mostrando defecaciones, esfínteres y partes internas del cuerpo, por decir lo menos radical. Creo que el porno pierde especificidad si nos quedamos solo con los extremo e iconoclasta que puede llegar a ser o si tomamos en cuenta solo lo del contenido explícito que genera excitación.*

**CT:** Pensaba en otras acepciones también, pornfood, porntravel o en el periodismo o en el cine la ya conocida pornomiseria.

> **SA:** *Si, porn se usa casi de prefijo para decir algo explícito y que busque un estado de excitación. Un plato de comida demasiado provocador o una imagen de una playa demasiado irresistible. Cuando leo esas expresiones me dan risa, aunque pienso que ayudan a desprejuiciar la palabra porno. Con pornomiseria es similar, aunque más que excitación lo que busca es una reacción en el lector o espectador.* 
>
> *Hay una película vieja, Precious, que trata sobre el sufrimiento de la protagonista. Bueno eso no tiene nada de excitación. Eso es sufrir casi igual que la protagonista o al menos acompañarla de cierto modo. Es un voyerismo por el sufrimiento que me parece que trasciende lo pornográfico como yo lo entiendo, que está del lado de generar excitación principalmente sexual y ciertamente no demasiado normalizada, estándar o como dije antes, obvia.*

**CT:** Primero tengo que decir que Precious no es una película tan vieja para algunos de nosotros (risas). Pensaba en Georges Bataille, toda su obra muy centrada en lo erótico, la relación del sexo y la muerte. Es decir, hay algo de la experiencia con el fin de la vida, con la ruptura, con el quiebre que está muy presente en el sexo. Digamos que el sexo es de cierta forma violento.

> **SA:** *Si, hay una manera de entender el sexo como el exceso de ciertos límites, de traspasar lo calculable, de la desaparición de lo racional. Estar arrebatado de placer, no poder controlarse, sería como yo lo entiendo la desaparición del yo o del ser.*
>
>  *La desaparición del ser más extrema, por supuesto, es la muerte. Bataille en su literatura lo plantea muy bien, entre la dialéctica prohibir-transgredir y el erotismo que es una suerte de tratamiento a esa dialéctica. O también el tema vida-muerte, yo-otro, porque lo erótico incluye al otro y es una disolución con lo otro o con el otro en el momento de verdadera excitación. Son temas muy interesantes que no tienen cabida en una industria porno de hoy que está pensada para pasar 12 minutos en la pantalla, porque dicen las estadísticas que es el tiempo promedio que pasan las personas en la página porno.* 
>
> *Todo el potencial revolucionario del porno, que no tiene que ser revolucionario per se, pero puede llegar a serlo, se desvanece en el orgasmo masturbatorio del porno mainstream.*

**CT:** Es decir que el porno puede hacer la revolución si se lo propone. Pienso en la serie Magnifica 70 de HBO y como las producciones porno burlaban la censura en la dictadura de los 70 en Brasil. Aunque no es que exactamente depusieron a los militares.

> **SA:** *No, para nada (risas). Ese porno era inofensivo para la dictadura, se solapaba con la comedia, aunque fue muy interesante. Igual que el cine de rumberas en México en los 40, fueron antecedentes importantísimos para la apertura al cine porno y quizás para pensar al sexo como parte cotidiana de la vida y que no se impusiera simplemente el conservadurismo y su represión. Lo porno de alguna manera reafirmó la libertad del individuo de consumir lo que la parecía adecuado. Siempre estuvo en tensión con la censura, claro, pero mientras más se censure el porno probablemente encontremos una sociedad menos abierta y más reaccionaria.*

**CT:** Hay un viejo debate entre las feministas con respecto al lugar del porno y el uso del cuerpo de la mujer como objeto. Es decir, las que están a favor de abolir el porno por ser una práctica que reproduce los regímenes de sometimiento de la mujer como fetiche del hombre y que perpetúa una posición subordinada al goce del macho. También están las no abolicionistas.

> **SA:** *Creo que he dejado claro que sería de las segundas, definitivamente. Entiendo el punto de que gran parte del porno está centrado en el hombre, que hay mucha gente, sobre todo los más jóvenes, que puede entender que el sexo se trata de dar placer al hombre y que la mujer debe procurarlo. Además de la belleza hegemónica que puede promover.* 
>
> *Pero eso es algo transversal a la sociedad y aboliendo el porno, como si tal cosa fuese posible, no soluciona el problema. En la lucha política feminista de hoy, el porno más bien debe tener un valor estratégico. En clave militante, sería ceder un baluarte importante de influencia en nuestras sociedades. Así como crecen chicos y chicas pensando que el sexo es una cosa por lo que ven en el porno, también podrían pensar una cosa distinta y poder disfrutar del sexo o de su cuerpo de mejor manera.* 
>
> *Se trata, creo yo, de apropiarnos como feministas de la industria del porno, hacerla menos industria, transformarla, convertirla en algo además de excitante, en algo útil, valioso, quizás revolucionario. Puede ser un fin, claro, porno por porno pero también puede ser un medio.*

**CT:** Está la obra muy conocida del Marqués de Sade, que uno se pregunta si lo revolucionario es por lo explicita que eran en lo sexual, que me parece por supuesto que lo eran, o por sus ideas más o menos heréticas a la sociedad de la época.

> **SA:** *Bueno, en Filosofía en el tocador se ve muy claramente eso que dices. Está lo absolutamente rompedor cuando describía las escenas sexuales y están las ideas más filosóficas que desarrolla el protagonista, que son como dices heréticas para las costumbres de la época. Me parece que la prohibición de su obra iba más por lo segundo que por lo primero. A diferencia de las pornochanchadas, las películas que hablamos de Brasil, porque no eran especialmente peligrosas en lo político.*
>
> *¿Podemos derribar al patriarcado a través del porno? Ahí si lo intentarían abolir los hombres (risas).*

**CT:** Sin embargo, el porno sigue teniendo un lugar muchas veces incómodo, no del todo aceptado, es casi un placer culposo, algo que hasta es mal visto en algunos lugares que no son precisamente conservadores.

> **SA:** *La clásica salida ingeniosa de hombres y mujeres cuando hablan de porno para no pecar ni de demasiado pacatos ni de demasiado entusiastas es algo así como “Si, a veces veo porno, pero realmente me aburre”. Bueno, si ves porno de hoy en día es cierto, puede ser repetitivo, pero además esta esa culpabilidad de disfrutar de ver porno porque es un pobre sustituto a la relación sexual con la pareja. Eso es juntar peras con manzanas, por supuesto.* 
>
> *Parte de la batalla cultural que debe dar el porno es esa, una construcción distinta con la pornografía que puede hacer la sociedad. En Latinoamérica es aún más acentuado esto puesto que somos sociedades de fuerte arraigo católico, que tiende a culpabilizar al deseo, donde el individualismo no es bien visto. Ni hablar de las artistas porno y su relación con la sociedad o de tolerar la idea que se puede ganar dinero cogiendo frente a una cámara como medio aceptable de vida.*

**CT:** Llegamos al tema latinoamericano y se complica todo, por supuesto. Tuviste que salir de tu país para hacer pornografía con mejores productoras. Pero tú lo hiciste por elección, muchas chicas lo hacen por un tema económico, sobre todo vía webcam que es lo que últimamente ha ayudado a muchas mujeres en posición de desventaja.

> **SA:** *Si, además el tema webcam en pandemia ha sido una solución para toda la industria porno digamos de productoras más grandes, puesto que no se ha podido hacer todo el porno que se puede.*
>
>  *OnlyFans se ha sugerido como una manera de quitarle poder a los grandes conglomerados del porno. Me parece que lo que termina haciendo es dejando en cierta precariedad a los artistas porno. Cada quien trabaja a destajo, con pocas redes de apoyo, con limitaciones técnicas y por poco dinero. La realidad es que sí, digamos que se logra mejores ingresos en comparación a otras áreas, pero aún sigue siendo muy bajo. Además de la competencia que existe con gente que se dedica a bailes sensuales sin ser pornográfico o más bien las que están en el mundo de la prostitución, que es otra cosa.* 
>
> *Esto, como dices, en Latinoamérica se agudiza. Si te quieres dedicar al porno en un sentido más amplio, con producciones más elaboradas sea que sigan la lógica falocéntrica o no, es muy difícil hacerlo en la región. Ojalá podemos hacer nuestro propio porno en algún momento, pero no de manera periférica, para reducir costos y con precariedad.*

**CT:** ¿Se puede regular lo porno? Al actuar siempre en los márgenes, terminan siendo evanescentes algunos aspectos y no sé si podemos decir que roza lo ilegal cuando por supuesto aparece ligada a horrores.

> **SA:** *Parte de la relación de tensión que tienen aún nuestras sociedades, incluso en el hemisferio norte, con el porno deja como consecuencia que no haya simpatías por este campo. Entonces no tiene demasiado acceso a financiación, nadie les presta dinero a artistas porno, no les permiten todas las locaciones, no terminan de darles un acceso a la justicia adecuado, de admitir algunas querellas en tribunales porque piensan que los problemas de la industria se deben resolver por sí mismo.* 
>
> *Esto empuja a parte del medio trabajar en lo que tu llamas los márgenes, al límite de lo legal, que tiene por supuesto el gran problema que conglomera a gente que tiene visiones de lo porno muy distintas, locas, e ilegales. Ahí es que aparecen los temas de pedofilia, zoofilia con maltratos, violencia sexual, que pone al porno en un lugar problemático. Si de alguna manera el marco legal está claro, es transparente, sería más fácil que el porno pudiera manejar estos problemas.* 
>
> *Lo legal y lo ilegal en el deseo humano siempre es una construcción y debe ser objeto de debate, pero yo soy de las que piensa que una base amplia que permita tener reglas claras de como funcione la pornografía, es mucho mejor que la posición en las sombras a la que muchas veces nos empujamos. ¿Reglas clara dije? Soné como una neoliberal (risas). Bueno, claro que puede haber una regulación, si lo pensamos como industria. Si lo pensamos como arte, que es a lo que pienso que debe aspirar la pornografía así se quede en una satisfacción solitaria de una paja (risas), ahí no estoy tan segura. No creo en esto de la cultura de la cancelación. En todo caso, la cancelación podría excitar, ¿no?*

**CT:** Ya para ir cerrando, ¿Cómo ves a la pornografía en los próximos años? Tiene el destino entonces de las artes, cada vez más precarizada o más bien el de las industrias, concentrada y adictiva.

> **SA:** *Interesante lo de adictiva. Sí, el porno puede ser adictivo, lo trabaja en un libro Gary Wilson sobre el efecto que tiene en la sexualidad sobre todo masculina y cierto paralelismo con la adicción. Es un porno construido para hacerse adictivo, para mostrar los planos más hipnotizantes de los cuerpos cogiendo.* 
>
> *Es lo que pasa con los juegos como Candy Crush o los me gusta de las redes sociales, pura adicción. Volvemos a lo mismo de antes, si el porno no tiene una relación más abierta con la sociedad, si solo está en los márgenes, no podrá discutir este tipo de problemas que podrían ser nocivos. Lo mismo para pensar salir de sus monopolios, se tiene que dar una discusión más amplia.* 
>
> *Con respecto al arte, si, es difícil porque cada vez más el arte en general está sin fondos y subyugada a su posibilidad de convertirse en producto o ser rentable. Soy un poco ingenua en ese sentido, la manera de que el porno se imponga como arte es que pueda tener una influencia amplia en la sociedad.* 
>
> *Que pueda formar parte de la educación sexual, de las construcciones que hacemos de lo erótico, del deseo, de los cuerpos, de la mujer, hombre, queer, trans. El porno puede hacernos cuestionar los cuerpos hegemónicos, la heteronormatividad, el género, el disfrute de las mujeres por sí mismas, nos puede hacer disfrutar con nuestra pareja o solos, historizarnos, contarnos como sociedad, ponernos limites a lo que queremos y a lo que no queremos. Necesitamos más gente que se tome en serio el porno, que lo trabaje como sensibilidad, con pasión. Hagamos el porno y no la guerra (risas).*

**CT:** Una última entonces que casi paso por alto… ¿por qué “Sor Alexa”? ¿Cómo elegiste ese nombre?

> **SA:** *Sor viene como una forma de homenaje y provocación a la vez a Sor Juana Inés de la Cruz, figura femenina máxima de las letras latinoamericanas. Conmemoró a la mujer, pero a través de algunos actos no muy correctos para la Iglesia (risas). Alexa viene de la primera artista porno que seguí cuando empecé a ver porno. Y además coincide con Alexa la asistente virtual, a la cual soy fan, tengo que decir.*

**CT:** Muchas gracias Sor Alexa, estoy seguro que te ganaste a más de uno en apoyar tu arte.

> **SA:** *¡Espero que sí! Apoyen a las producciones hechas por mujeres, por latinoamericanas y de cine porno alternativo.*