---
title: “Como irte, como quedarte, como moverte en todas direcciones”, territorio
  de infancia
subtitle: " "
cover: /images/uploads/20210305_131541.png
caption: " "
date: 2021-03-10T15:23:33.763Z
authors:
  - Florencia Cencig
tags:
  - Infancia
  - Niñez
  - Colombia
  - Libertad
  - Educación
  - BibliotecaPopular
  - CostaCaribe
  - CaribeColombiano
  - Conocimiento
  - Niños
  - Niñas
  - Derechos
categories:
  - Educación
comments: true
sticky: true
---
Después de un 2019 donde la primavera me obligó a llegar hasta mis raíces, para volver a florecer, decidí agarrar mi mochila y emprender un nuevo viaje. Esta vez, el destino era Colombia. **Dos meses enteros para conocer su cultura, aprender su historia, probar sus comidas y bailar su música.**

Fueron incontables los paisajes increíbles que vieron mis ojos. Todavía recuerdo aquella sensación de tibieza producida por el mar Caribe, y esos atardeceres en los cuales te quedabas espasmódico por la magia que destellaba el cielo. Sin embargo, **cada vez que quiero desandar mi viaje por la costa colombiana, hay algo que llega inmediatamente a mi cabeza, topándose con todo lo anterior: La Biblioteca Popular de la Isla Barú.**

En realidad, en términos correctos, Barú no es una isla sino una península, pero eso es solo algo anecdótico. Lo que sí es imprescindible no pasar por alto, es lo que sucede en aquella biblioteca.

Gabi, es la creadora lunática de ese espacio destinado a niñxs y adolescentes. No solo para residentes de la “isla”, sino de pueblitos aledaños. **Un lugar donde las infancias son libres; tienen voz y voto, preguntan todas sus dudas y construyen ideas, juegan, leen, crean historias, y generan vínculos de amistad y empatía.**

**Lxs chicxs que asisten a la biblioteca atraviesan diferentes realidades.** En mayor o menor medida, vivencian situaciones de vulneración de derechos y problemáticas que son invisibles para el Estado y su agenda pública. Pero, Gabi, ahí está, siendo una eterna creyente de la transformación y del trabajo colectivo.

![](/images/uploads/20210305_131500.png)

A través de herramientas y prácticas artísticas y literarias, Gabi, aborda diversas temáticas referidas a los derechos humanos. Trabajan la ESI (Educación Sexual Integral), la diversidad, la inclusión y el respeto por el ambiente. **Ella los empuja, de a poquito, a un mundo distinto,** por fuera del adultocentrismo y la heteronorma, y los invita a alzar sus voces desde la pluralidad.

Gabi, apuesta por el protagonismo infantil y las infancias libres. Entiende que es urgente y necesario que las niñeces sean protagonistas de sus propios procesos de desarrollo y construcción de identidades. **Considera primordial que los chicxs tengan un rol activo**, y participen de las decisiones que involucran a su crianza y su educación, lejos del paternalismo y del prehistórico paradigma de minoridad.

En este sentido, el desarrollo del protagonismo infantil, necesita de entornos sociales y educativos que **apoyen recorridos de empoderamiento de los niños, niñas y adolescentes**, en el efectivo ejercicio de sus derechos. En términos de Jáuregui Virto, A. (2018), es elemental un acompañamiento socioeducativo que promueva la plena participación de las infancias, en las diversas dimensiones que forman parte de sus vidas.

Siguiendo esta idea, Gabi, comprendió que es esencial que los chicxs se empapen de sus derechos y garantías, q**ue sean seres sentipensantes, y que tengan la información necesaria para exigir lo que les corresponde.** Esta mujer atrevida y cósmica, construyó en la isla, redes de apoyo entre los niñxs, cuidadores y educadores, en pos del bienestar integral de las niñeces; y contribuyó desde un espacio no formal, a fomentar la participación ciudadana intergeneracional

Cuanta razón tenía Galeano; *"Día tras día se niega a los niños el derecho de ser niños. El mundo trata a los niños pobres como si fueran basura, el mundo trata a los niños ricos como si fueran dinero, y a los del medio, a los que no son ni pobres ni ricos, el mundo los tiene bien atados a la pata del televisor para que desde muy temprano acepten como destino la vida prisionera. Mucha magia y mucha suerte tienen los niños que consiguen ser niños".*

![](/images/uploads/20210305_131443.png)

Mientras sucedía la vida, ella les preguntaba a los pequeños integrantes del lugar: -**¿De qué color amaneció tu corazón hoy?** El mío amaneció multicolor todos los días en aquella isla. Para mí, Gabi, es la guardiana de las infancias libres. Y mis tardes con Lion y Fede en la biblioteca, hicieron volver a esa Flor que jugaba interminables horas en el jardín de casa.

Entonces, siento que "hay lugares que no pueden contener su sol, que hacen que brote esperanza y se nos prenda el corazón”. Pero, también, **hay personas que no pueden contener su sol y hacen que brote esperanza, como Gabi.**

**Bibliografía:**

\- Jáuregui Virto, A. (2018). “El protagonismo infantil comunitario como estrategia socioeducativa de protección a la infancia en barrios”. Asociación Umetxea Sanduzelai.