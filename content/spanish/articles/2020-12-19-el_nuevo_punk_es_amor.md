---
title: El nuevo Punk es AMOR
subtitle: " "
cover: /images/uploads/wyron-a-n2pmaqxi-gm-unsplash.jpg
date: 2020-12-19T16:38:55.055Z
authors:
  - Catalina Medina Barrios
tags:
  - Punk
  - Amor
  - Revolución
  - Sociedad
  - Ego
  - Miedo
  - Sistema
  - Vida
categories:
  - Cuerpo
comments: true
sticky: true
---
La primera vez que leí la frase ***“El nuevo punk es amor”*** estaba tratando de entender el punk en un libro empresarial.

Seguramente estaba como tú en este momento, completamente perdida de lo que estaba leyendo, sumado a que no tenía la más mínima idea del cambio que esto iba a producir en mí. Sin embargo, después de un par de meses entendí la profundidad de esa frase y lo punk que quería ser.

**Ahora hablamos de nueva normalidad, que para mí no hay mucha diferencia entre una y otra a parte de lo presencial;** aún sufrimos de ansiedad, aún nos comparamos, estamos llenos de miedos, seguimos pegados al celular o la computadora, aún vivimos con estrés, aún nos falta sinceridad, tenemos emociones sin expresar, fotos con filtros, mensajes de texto, notificaciones, depresión, trastornos alimenticios, trastornos mentales, pocas relaciones reales. Tengo que decir que algunas han aumentado en esta “nueva normalidad”, pero otras también han disminuido, todos hemos caído en alguno de esos comportamientos, sin embargo, muy pocos los relacionan con el amor.

**Sí, somos una sociedad que nos da miedo amar**, nos da miedo amarnos a nosotros mismos por creer que es egoísta, una sociedad que le da miedo abrazar o confiar porque cree que el otro le hará daño, **somos una sociedad que está enferma por falta de amor** y entendiendo esto comprendí porque el nuevo punk es amor.

Cuando comenzamos a hacer el trabajo para sanar esos trastornos o para bajarle al estrés o llevar la depresión o la ansiedad o a trabajar el despegarnos de las redes y no compararnos, llegamos a un momento en que nos damos cuenta que todo va a nuestra falta de amor propio y con él a nuestra sensación de insuficiencia y merecimiento, nos lleva a un miedo y el miedo es del ego, es nuestra manera de protegernos de eso que creemos que puede hacernos daño, así no que no pienso hablar mal de él y **por eso concuerdo con que el punk es amor.**

Sí, seguramente es más normal comparar el amor con el movimiento hippie, de paz y amor, de amarnos los unos a los otros y sí, es una forma. Sin embargo, en nuestra caja de cristal actual debemos ser más contundentes, debemos ser más de romper y construir de nuevo, más radicales y eso nos lo brinda el punk, el punk rompe, el punk choca, **el punk disrumpe a donde va y eso es lo que debemos hacer ahora por nosotros, expresar nuestro amor, escuchar el amor, compartir el amor** y esto se traduce a relaciones más sinceras, a conversaciones transparentes, a disfrutar sin juzgar, a compartir sin esperar nada a cambio y todo bajo la fuerza del hacer, no de esperar que alguien más lo haga por nosotros.

Amar nos lleva a confiar en el otro y a aceptar que no sabemos todo, mostrándonos la posibilidad de soltar situaciones, tareas y relaciones que produzcan estrés, depresión o ansiedad. **Amar nos lleva a encontrar nuestra propia manera de hacer las cosas,** lejos de los algoritmos que nos envían a un mundo que no existe pero del que creemos pertenecer o luchamos para pertenecer. **El amor nos saca de la mediocridad en la que vivimos, el amor genera caos como el punk, genera caos en un mundo plano y miedoso como el que vivimos.**

Así que mi invitación es que te expreses tal cual sientes: que seas auténtico, que ames tu individualidad, que compartas tus argumentos, que los cuestiones, que camines por la incertidumbre y escuches tu intuición o corazón. **¡Mi invitación es que vivas de una manera más punk!**

Un pequeño recomendado, léete el libro *Think Punk.*