---
title: Un Estado Incongruente
subtitle: " "
cover: /images/uploads/bullets.jpg
caption: "Ilustración: Pawel Kuczynski"
date: 2020-10-16T01:07:28.798Z
authors:
  - Laura González
tags:
  - EdiciónEspecial
  - 12deoctubre
  - Identidad
  - Latinoamerica
  - Continente
  - América
  - Venezuela
categories:
  - Territorio
comments: true
sticky: true
---
Sabemos que el 12 de octubre de 1942 Cristóbal Colón llegó a América, este hecho marcó la historia del territorio. Actualmente se celebra y conmemora en todos los países del continente esta fecha cargada de representación de diversas maneras. Forma parte de los calendarios cívicos y tiene un simbolismo importante en relación a las identidades nacionales. En casi todo el continente Americano esta conmemoración apareció a principios del siglo XX como el “Dia de la Raza” haciendo referencia al mestizaje de sangre y culturas que, supuestamente, define la ocupación española. Con distintos nombres se celebra o conmemora este hecho en todo el continente además de España.

“Así, el 12 de octubre refleja ese aspecto no resuelto de la organización social en América Latina, esa brecha interna producto de la exclusión oligárquica que se renovó con el liberalismo criollo y el capitalismo periférico, se buscó redimir con los nacionalismos populares homogeneizantes y que ahora los mismos indígenas y afrodescendientes cuestionan ante su recreación en el contexto neoliberal”. (Grimson, Castellano, Valezquez, Basto:2018)

En Venezuela, el 12 de octubre del 2002 el presidente Hugo Chávez decretó el "Día de la Resistencia Indígena” en sinónimo de lucha de los aborígenes contra los colonizadores, aunque fue criticada por la oposición venezolana y también sancionada por la Asamblea Nacional de Venezuela como festividad en el decreto 2028, con fecha del 12 de octubre de 2002. Por otra parte, la constitución venezolana describe que somos un país multiétnico y pluricultural, considerando que predomina el mestizaje entre blancos, negros, indígenas y no hay una segregación notoria entre etnias y grupos culturales que habitan dentro país. Además en la reforma constitucional de 1999 que incluyó los derechos de los pueblos indígenas.

Todo este contexto es necesario exponerlo, ya que actualmente aquellos a los cuales conmemoramos y honramos cada 12 de octubre todos los movimientos sociales, su integridad física y territorial es violentada en Venezuela. Es importante saber que la problemática indígena en Venezuela se visualiza desde la colonización y la evangelización para poseer su territorio y la riquezas desde la época colonial. También al revisar la constitución, donde el Estado reconoce la existencia de los pueblos indígenas y organización política, económica, cultural, más el uso de sus costumbres idioma y su culto religioso, reconociendo su territorio como ancestral donde se garantiza su vida.

Este factor es el territorio, el territorio como un todo desde la concepción indígena, donde coexisten y se relacionan de manera natural y espiritual, donde comienzan sus primeras formas de vida, entonces me interesé en trabajar sobre acontecimientos, desplazamientos forzados de ciertos grupos indígenas. Sabemos que actualmente Venezuela está pasando una situación de crisis económica y social, donde los pueblos indígenas no son ajenos a esta realidad, es más, han sido víctimas de sucesos como violencia por la fuerza cívico-militar, la minería, la falta de recursos para su alimentación y salud, pero el territorio es inhabitable ya el hábitat natural ha sido deforestada o hay prácticas de extractivismo, todas esta razones son un posible factor para que haya desplazamiento de grupos indígenas de su territorio.

En todo el territorio nacional hay pueblos indígenas que padecen del desplazamiento forzado, esta vez hablaré sobre un caso específico, el de los "Waraos" ubicados en el estado Delta Amacuro, al nororiente del país, pues ha sido una situación alarmante porque han sido un éxodo inmenso, según los datos presentados por las estadísticas de instituciones internacionales como la Agencia de las Naciones Unidas, Acnur y la Organización Internacional de Migración (OIM), refieren que la cantidad de Waraos desplazados supera la cifra de 6.000 movilizados en tan solo tres años para el 2019.

El desplazamiento de los Waraos se remonta hasta la década de los 60 con el cierre del Caño Manamo, una política de control hidrográfico que implicó un gran impacto ambiental. Esta medida provocó la muerte de más de 5.000 waraos por el estancamiento de agua derivado del cierre hidrográfico del Caño Manamo.

Agosto de 1990 es otra fecha que marcó el éxodo de las comunidades Waraos. El brote de cólera en ese tiempo generó una estampida de los Waraos hasta Barrancas del Orinoco, Los Barrancos de Fajardo, Tucupita, Caracas, Valencia y otras urbes. Además, el deterioro de las infraestructuras en las 320 comunidades indígenas del estado Delta Amacuro: escuelas derrumbadas, dispensarios colapsados y ambulatorios en abandono progresivo por falta de médicos, paramédicos e insumos médicos que empeoran y agudizan la mala calidad de vida de los este grupo en Delta Amacuro.

Para finales de 2016, 2017 y comienzos de 2018, se estima que aumentó la cifra de Waraos movilizados que cruzaron la frontera hacia Brasil (Romina 2018). En un reporte, la entonces gobernadora del estado de Roraima (Brasil), Zuely Campos, señaló la presencia de 3.500 Waraos para el primer trimestre de 2018 en la localidad de Pacaraima y Boa Vista, zona fronteriza con Venezuela. La ruta de los Waraos ha sido siempre la misma: población de los caseríos de los municipios Antonio Díaz y Pedernales que se desplazan hasta Tucupita, San Félix, Santa Elena de Uairén y la frontera de Venezuela con Brasil.

Sin embargo, para el último trimestre de 2018 se conoce una nueva ruta, la población de la zona sur que corresponde a las comunidades adyacentes del eje Sierra Imataca que se desplaza de las comunidades por el Orinoco hasta su desembocadura y parte del mar Caribe hasta la frontera con la República de Guyana. Para finales del tercer trimestre de 2018, la Organización Internacional para las Migraciones (OIM) reportó la presencia de 793 indígenas Waraos en la zona fronteriza de la República de Guyana en calidad de desplazados, que llegan a la región en embarcaciones improvisadas a través del Orinoco y parte del mar Caribe.

En los últimos tres años se ha comprobado un nuevo factor en el desplazamiento de los Waraos: grupos armados que se organizan en sindicatos, militares, guerrillas y mafias ilegales que buscan el control de la zona por la explotación del oro en la zona del Arco Minero.

La situación de los Waraos es lamentable aparte de ser inconstitucional ya que atenta con la integridad de los pueblos indígenas y su permanencia en el territorio originario. Croes menciona “los primeros acuerdos en cuanto a las medidas que ambas partes, pueblos indígenas y Estado deben aplicar de forma conjunta para garantizar el verdadero respeto a los sitios sagrado” (Croes, González, Maneriro:2018).

Esta misma realidad que no es ajena a grupos o comunidades indígenas como el caso de los: Yukpa, los Pemones, y los Pumé; que también han sido forzado a desplazarse por asuntos que tiene que ver con el crimen organizado, la minería o por no poder acceder a los servicios de primera necesidad como la salud o la alimentación, teniendo que buscar alternativas como retirarse de su territorio, esencial y elemental para los pueblos indígenas, porque es donde comienza sus formas de vida para mantener su cultura. Donde el gobierno venezolano conmemora el Día de la Resistencia Indigena a través de los medios de comunicación con discursos decoloniales, mientras legitiman políticas violentas neoliberales en su territorio.

## Referencias Bibliográficas

- Constitución de la República Bolivariana de Venezuela. (1999). Gaceta oficial N° 5098

- Croes, G. González, J. Maneiro, M. (2018) “Sitios Sagrados Naturales Cartografías Indígenas y el conocimiento de geografía sagrada”, edición Filven 2018, Caracas.

- Grimson, A. , Castellanos, A. , Velásquez Nimatuj, I. A. Bastos Amigo, S. (2018) “Nación y racismo. [El día 12 de octubre en la construcción de las sociedades latinoamericanas](encartesantropologicos.mx/nacion-racismo-12-octubre/)”. Encartes, 02, 245-254.

- Romina. P (2018) [“Migración masiva de waraos es el rostro humano en el Día de los Pueblos Indígenas”](https://www.amazoniasocioambiental.org/es/radar/migracion-masiva-de-waraos-es-el-rostro-humano-en-el-dia-de-los-pueblos-indigenas/)

- Observatorio de Ecología Política (2019) [“Aumenta éxodo masivo de waraos desde el Delta del Orinoco”](<http://www.ecopoliticavenezuela.org/2019/09/27/aumenta-exodo-masivo-waraos-desde-delta-del-orinoco/>)

- Colaboradores de Wikipedia. (2020, 12 octubre).[ Día de la Resistencia Indígena](https://es.wikipedia.org/wiki/D%C3%ADa_de_la_Resistencia_Ind%C3%ADgena). Wikipedia, la enciclopedia libre.
