---
title: Migrar también es un asunto feminista
subtitle: Una aproximación a las experiencias migrantes en Argentina
cover: /images/uploads/marcha_de_mujeres_migrantes._imagen_de_internet.jpg
date: 2021-04-30T17:10:28.446Z
authors:
  - Mireya Dávila Brito
tags:
  - Mujeres
  - Feminismo
  - Migracion
  - Genero
  - Territorio
categories:
  - Genero
  - Cuerpo
  - Territorio
  - Cuerpo y Territorio
comments: true
sticky: true
---
Argentina es el país de Sudamérica que más recibe personas migrantes. **La oleada migratoria se inició en los años 90, conformada mayormente por mujeres, provenientes de los países limítrofes (Paraguay y Bolivia).** Según datos de AMUMRA (2019) (1), alrededor de dos millones de personas (4,9% de la población total) nacieron en suelo extranjero: paraguayxs, bolivianxs, peruanxs, colombianxs, chilenxs y, más recientemente desde el año 2017, venezolanxs. **El 71% de lxs migrantes está en edad de trabajar (15 a 64 años)**; el 82,7% se asientan en la Provincia de Buenos Aires y el 53,97% -más de la mitad- son mujeres. **Este breve artículo se interesa por las experiencias de mujeres -cis y transgéneros- migrantes y trabajadoras**.

Las mujeres pobres y trabajadoras se desplazan desde sus regiones nativas a Argentina con el mismo fin: **subsistir y existir.** Detrás de la migración se encuentra el despojo, la explotación y la desigualdad social en sus países de origen. **En estas mujeres y cuerpos feminizados impactan, con fuerza, distintas violencias de género, a veces, se superponen unas a otras.** Se trata de mujeres cis y trans que durante su trayectoria -antes de migrar, en el tránsito y a su llegada- experimentan diversas formas de violencias: física, sexual, económica y simbólica. **La migración es, pues, un asunto feminista,** en tanto, las desigualdades estructurales del patriarcado y las violencias que atraviesan estas mujeres deben ubicarse en el cruce del género, la clase y la racialización que las constituye como sujetas sociales en situación de precariedad y vulnerabilidad, así como también, en agentes de su propia emancipación.

Muchísimas han llegado al país buscando una “mejor calidad de vida”; decenas atestiguan que huyeron -con hijes o sin ellxs- amenazadas por sus parejas o ex parejas. Todas coinciden con que atravesaron fronteras para preservar sus vidas y las de sus hijes. Para ellas, la migración no fue sólo un asunto de subsistencia sino una **estrategia para continuar existiendo**.

 En el caso de las **mujeres trans** tampoco es muy distinto. Asumir una identidad de género distinta al sexo biológicamente asignado y trascender el binarismo varón/mujer para ir hacia la construcción de una identidad propia, les costó el desarraigo. Las personas trans a muy temprana edad son expulsadas de sus familias, son discriminades y violentades en la escuela y en sus entornos sociales y, en consecuencia, quedan al margen del mercado laboral. **El promedio de vida ronda los 35 años de edad** y, la mayor parte, transcurren bajo violencia sistemática e institucional: discriminación, negación de derechos civiles, marginación, detenciones arbitrarias, agresiones sexuales, violencia física y transfemicidios.

**Las mujeres indígenas y originarias del campo migran porque no son dueñas de la tierra que trabajan**; se encuentran sujetas a patrones, capataces y maridos, son jornaleras sometidas a largas faenas, sin acceso a la educación y a la salud sexual y reproductiva. La inmensa mayoría de mujeres vienen a la Argentina, porque buscan sostener sus vidas y las de sus familias; envían remesas a sus lugares de origen y de ellas depende la manutención de hijxs, madres, padres, abuelxs y hermanxs. Sobre sus cuerpos recae el trabajo remunerado y los cuidados. 

También se encuentran aquellas mujeres víctimas de desplazamientos forzosos; **escapan de la violencia política, tales son los casos de Colombia y Paraguay**. En suma, son múltiples y heterogéneas las razones que empujan a las mujeres a migrar y están relacionadas con el modo en que está organizado el trabajo en el capitalismo y en el patriarcado.

### Trabajadoras y migrantes

![](/images/uploads/protesta_migrante_2018_-foto-andy_ferreira.jpeg)

Uno de los oficios más precarizado, racializado y feminizado en el mercado laboral es el trabajo doméstico. Acá es realizado, en su mayoría, por mujeres migrantes que, al no conocer sus derechos y no poseer la residencia formal en el país, se ven obligadas a realizar tareas mal remuneradas y sin gozar de los derechos de la Seguridad Social (seguro de salud, aportes jubilatorios y seguro por accidente laboral). **Las trabajadoras de casas** particulares se hallan bajo las órdenes de empleadorxs que las convierten en mano de obra barata y, en algunos casos, se encuentran en condiciones serviles, con sobreexplotación en jornadas extensas y con sobrecarga de tareas físicas. **A la precarización se le suma el racismo** de la que son víctimas; con frecuencia las trabajadoras reciben malos tratos, despidos injustificados y vejaciones, por ejemplo, tienen que soportar las sospechas que recaen sobre ellas cuando son acusadas de la pérdida de objetos o dinero. En todos los casos, la violencia hacia estas mujeres tiene un componente racista.

**Argentina es un país con alta tasa de precarización y abaratamiento de la fuerza de trabajo**. El empleo no registrado, mal llamado “en negro”, sobrepasa los cinco millones de empleadxs bajo esta modalidad, según datos del INDEC (2020)(2). En esta cifra también se inscriben las migrantes empleadas del comercio, trabajadoras en talleres textiles -algunos clandestinos- y empleadas de mantenimiento y servicios.

### Las migraciones en la agenda feminista

![](/images/uploads/imagen-revista_anfibia.jpeg)

Varias organizaciones sociales feministas (3) agrupan a mujeres migrantes y trabajan, desde el territorio, para llevar adelante transformaciones en el campo legal y jurídico; además reclaman derechos sociales y políticos bajo el lema “migrar es un derecho” y “ningún ser humano es ilegal”. Aunque recientemente, el Estado argentino derogó el DNU 70/2017(4), decretado por el gobierno neoliberal de Mauricio Macri, que determinaba una política migratoria regresiva expresada en expulsiones, dificultad para regularizar el estatus migratorio, violencia institucional y racismo; las agrupaciones feministas no bajan los brazos, porque todavía queda muchísimo por hacer para vivir una vida libre de violencias de género, económica y simbólica.

El aumento de la vulnerabilidad económica y social de las migrantes en el contexto de pandemia, repercute en su exclusión de los planes y programas sociales para mitigar la crisis económica. **Aquellas que trabajan por cuenta propia como vendedoras en la calle de comida o de mercadería** se han visto restringidas frente al crecimiento de contagios por coronavirus. Por tanto, en los actuales momentos de emergencia nacional, las feministas migrantes sostienen en red a mujeres de diversas nacionalidades. Al encontrarse fuera de sus países de origen, sin parientes ni amistades que le apoyen, las migrantes se sostienen unas a otras en la lucha por sus derechos, resuelven día a día la manutención y continúan criando a sus hijes; cocinan en los comedores y ollas populares; articulan con otras organizaciones; denuncian las injusticias y el racismo, y tejen redes que les permite agenciar sus propias vidas.

Alcanzar la igualdad plena de las mujeres migrantes, erradicar las violencias a las que son sometidas durante el cruce de las fronteras y a su llegada a Argentina; garantizar su inclusión social como sujetas de derecho es la apuesta del feminismo popular, interseccional y migrante.

#### Referencias 

1. <https://amumra.org.ar/mujeres-migrantes-contra-la-violencia-en-el-mundo-del-trabajo/> (Consultado abril 2021)

2. <https://www.indec.gob.ar/uploads/informesdeprensa/mercado_trabajo_eph_3trim20E927D146A5.pdf> (Consultado abril 2021)

3. Varias son las organizaciones y secretarías de género en las agrupaciones políticas que trabajan con migrantes desde una perspectiva feminista: AMUMRA, Ni Una Migrante Menos, Campaña Nacional Migrar no es Delito, Migrantes x Migrantes, Identidad Marrón, entre otras.

4.<https://www.boletinoficial.gob.ar/detalleAviso/primera/241471/20210305?busqueda=2%C2%A0%C2%A0> (Consultado abril 2021)

#### Mireya Dávila Brito

Feminista y migrante venezolana en Argentina. Historiadora (UCV) y docente en la Universidad Nacional Arturo Jauretche. Redactora en la Revista Hamartia y facilitadora del taller de Historia en el Foro de Pensamiento Crítico (UTN).