---
title: Ojos que no ven, corazones que arden
subtitle: " "
cover: /images/uploads/ojos-que-no-ven_-corazones-que-arden.png
caption: "Creaciones visuales: Adelaida Camacho"
date: 2020-10-18T18:56:44.233Z
authors:
  - Alexis Moreira
tags:
  - Chile
  - Represión
  - Política
  - Latinoamérica
  - Carabineros
  - Violencia
  - Sociedad
  - Impunidad
  - AbusoDePoder
categories:
  - Política y Economía
comments: true
sticky: true
---
<!--StartFragment-->

**Lo que para algunxs era sólo un aumento en la tarifa del boleto del metro, fue para muchxs, entre quienes me incluyo, aquella gota que rebalsó el vaso.** Ese vaso que es Chile, y esa gota que era una más de las reiteradas medidas tomadas por los gobiernos post dictadura cívico militar. Otra medida que en lugar de avanzar en la protección social, profundiza las desigualdades latentes en el país.

**“No son 30 pesos, son 30 años”** fue una de las tantas consignas que se propagaron entre las estaciones de las siete líneas de metro de Santiago en las voces alzadas y organizadas de estudiantes secundarios. Ese malestar adolescente se contagió en las generaciones mayores, les hicieron ver que ya no debían seguir calladxs ante la injusticia, que **aquella alegría que tanto se prometió en los gobiernos “democráticos” nunca llegó ni llegará.**

**El 18 de octubre de 2019,** cuatro días después de que secundarios y universitarios se manifestaron por primera vez mediante la coordinada evasión masiva al subterráneo de la capital del país, el malestar se convirtió en fuego. Un fuego que envolvió no sólo estaciones y vagones del metro, sino que también se hizo presente junto a las multitudes de personas en las calles, y a los enfrentamientos con la fuerte y desmedida represión por parte de Carabineros, la policía chilena.

**Esta fecha marca un hito en la historia del Chile actual**, ya que es el punto de inflexión en la construcción de una nueva sociedad. Este proceso ha recibido muchas denominaciones, pero quiero quedarme con dos de ellas que, a la luz de los hechos, me resultan a lo menos paradójicas: **“despertar social” y “estallido social”**.

Y sostengo esta suerte de paradoja porque desde aquel día de octubre a febrero de 2020, según cifras del Instituto Nacional de Derechos Humanos, se registraron un total de **459 víctimas de trauma ocular por acción de las fuerzas policiales.** En otras palabras, personas que - sin importar si participaban o no de las concentraciones populares - sufrieron daño parcial o total en sus ojos. Del total de víctimas, 26 personas sufrieron el estallido del globo ocular y 9 la pérdida de visión por trauma ocular irreversible. No sé si notaron la paradoja. **Muchxs no volvieron a despertar como lo hacían habitualmente, debido al estallido de sus ojos.**

La mayoría de estos sucesos ocurrieron debido a disparos perpetrados por la policía, aplicando protocolos de dudosa actualización o en otros casos derechamente ante el incumplimiento de ellos, **al ejecutar disparos y lanzamiento de proyectiles directamente a la parte superior del cuerpo de quienes se manifestaban.** Lo peor de todo, es que la mayoría de estas repudiables agresiones siguen sin ser resueltas por la justicia, principalmente por la falta de colaboración de Carabineros en la entrega de evidencia que permita esclarecer los hechos, identificar responsables y dictar sentencias.

En los siguientes párrafos compartiré dos historias que, a pesar de la tragedia, han iluminado la resistencia que han realizado muchxs chilenxs a lo largo del país. Son, por lo demás, **relatos vivientes del negligente y despiadado actuar de la policía chilena.**

El viernes 8 de noviembre, el estudiante de psicología de 21 años, Gustavo Gatica se encontraba en las cercanías de plaza Dignidad (ex plaza Italia) fotografiando lo que hasta ese momento, era un nuevo día de manifestaciones sociales en Chile. Eran pasadas las 18 horas cuando, repentinamente, tambalea herido. **Desde sus ojos emanaban hileras de sangre que prontamente le cubrieron el rostro.**

A su lado, afortunadamente, se encontraba Jaime Bastías, unx de lxs tantxs asistentes a la protesta. No conocía a Gustavo. No dudó en tomarlo del brazo y prestarle su ayuda.

Minutos más tarde, es Jaime quien vía telefónica le informa a Enrique, hermano de Gustavo, lo que había sucedido.

Grupos de voluntarios de rescate le prestaron los primeros auxilios. **La imagen de Gustavo sentado en la vereda shockeado con hileras de sangres en los ojos dio la vuelta al mundo en cosa de horas.**

Si bien en aquella fecha, ya existían muertxs en el contexto del estallido social por acción de Carabineros, lo gráfico de la escena lograba retratar el riesgo al que están expuestxs y **la violencia a la que están sometidxs lxs chilenxs por defender y exigir sus derechos.** Literalmente es poner tu cuerpo al frente de un campo de batalla en donde las fuerzas de poder y de represión son la norma, y por lo demás, tu gran desventaja.

*“Nos tomamos de la mano”*, dice Enrique en una entrevista publicada por la BBC. Tomarle la mano fue lo primero que hizo instintivamente una vez que logró encontrarse con Gustavo en la Clínica Santa María.

Desde ese día, Gustavo fue sometido a una serie de intervenciones y operaciones para tratar de salvar en alguna medida parte de su visión. Pese a los esfuerzos médicos, el 26 de noviembre se confirma su **ceguera total.** Ese mismo día, a merced de una dolorosa coincidencia, **los ojos de Fabiola Campillai también fueron apagados para siempre.**

![](/images/uploads/en-sus-manos-la-sangre.png)

Fabiola, de 36 años, madre de tres hijxs, bombero y futbolista, salió de su casa minutos antes de las 21 horas en la comuna de San Bernardo, ubicada en la zona sur de la conurbación de Santiago. Se dirigía al paradero donde diariamente aborda el bus de acercamiento hasta su lugar de trabajo, la empresa Carozzi, en la que se desempeñaba como auxiliar de producción.

Ese día fue acompañada por su hermana, Ana María. Habitualmente, era su esposo Marco Antonio, quien la acompañaba hasta aquel paradero cuando Fabiola debía trabajar en el turno de noche.

A unas cuadras de donde vivían, un piquete de Carabineros vigilaba una manifestación que por ese momento, no registraba excesos de violencia. Ante ello, Fabiola y Ana María, avanzan en dirección hacia el paradero, cuando tras un fuerte estruendo y una incontrolable humareda, **Fabiola cae paralizada con el rostro ensangrentado.**

*“En eso giramos y yo lo único que sentí fue ‘paf’. No pensé que le había llegado a ella porque como salía humo… Pero cuando la veo que cae casi tiesa… y sangraba… cayó para atrás, sangrando de todos lados. Yo le tomé la carita y el ojo estaba afuera, lo tenía colgando, afuerita. Yo dije ‘ese ojo lo perdió’, altiro”*, es el crudo relato que Ana María, entregó en una entrevista para el Centro de Investigación Periodística CIPER. **Una bomba lacrimógena lanzada por Carabineros había impactado de lleno en el rostro de su hermana Fabiola.**

Con el apoyo de sus vecinxs, Fabiola fue trasladada después de un largo recorrido entre centros asistenciales cercanos, hasta la Clínica Láser de Las Condes ya de madrugada. En dicho lugar el diagnóstico fue lapidario: **Fabiola había perdido totalmente la visión.** Tras ello fue trasladada al Instituto de Salud del Trabajador, en donde fue intervenida en varias ocasiones para reconstruir su rostro.

Tras una semana en coma inducido, **Fabiola se enfrenta por primera vez a una nueva forma de despertar.** No sólo ha perdido la visión, sino que además perdió el sentido del olfato y el gusto. Tres de cinco sentidos arrebatados de golpe.

Desde los hechos relatados, ha pasado cerca de un año. Y la justicia aún se hace esperar. **Si bien para los casos de Gustavo y Fabiola, han existido avances, estos han sido tardíos y han dejado de manifiesto el encubrimiento que existe por parte de autoridades policiales y civiles.** Evidencia de ello, es que en una suerte de arenga, filtrada a la opinión pública, se escucha claramente al General Director de Carabineros señalar *“tienen todo el apoyo, todo el respaldo, de este general director. Cómo lo demuestro. A nadie voy a dar de baja por procedimiento policial. Aunque me obliguen, no lo voy a hacer”.*

Si hay algo que conmueve, es escuchar a Gustavo y Fabiola, hablar sobre lo sucedido. **En ambos se unen pedidos de justicia.** Pero también en sus relatos se unen la entereza y la tranquilidad porque saben que lo que les sucedió escapa a su responsabilidad. Ellxs a pesar de no poder despertar como de costumbre, duermen con la paz de ser consecuentes con su vida, su origen y sus convicciones, porque como dijo Gustavo en una entrevista: ***“Vivir sin paz, es peor que no ver”.***

En ambos casos, y para las 34 muertes acaecidas en este contexto y las más de 2.500 acciones legales presentadas por el INDH en las que se denuncian vulneraciones y violaciones a los derechos humanos, se exige justicia, no solo a los autores materiales de estos actos, sino a las líneas de mando correspondientes a la jerarquía policial, y a **las** **responsabilidad políticas de las autoridades que con su deliberada omisión y negacionismo de estos hechos**, avalan el uso de la fuerza del Estado como medida de represión social.

El próximo 25 de octubre se abre una nueva oportunidad para comenzar a sanar heridas abiertas desde antes de octubre de 2019. **Lxs chilenxs podrán participar de un plebiscito en donde se apruebe o rechace la redacción de una nueva constitución, que borre la anteriormente instaurada en dictadura.**

Soy consciente de que una nueva constitución no es la solución a todas las problemáticas multidimensionales que conviven en Chile. **Pero si estoy convencido de que el diálogo por sobre la imposición de la fuerza, es el camino que se debe seguir**. Ya no va más ese tétrico lema nacional que nos remarcaron con fuerza en sus 100 años de existencia: ***“por la razón o la fuerza”***. **Debe ser por la razón siempre, por la fuerza nunca. Nunca más.**

> *Por Gustavo, por Fabiola, por quienes perdieron la vida, por quienes sufrieron mutilaciones, heridas, torturas, violaciones y abusos. Por sus familias. **Por todxs aquellxs que después del estallido han tenido un nuevo despertar.***

#### Los dos collages digitales especialmente creados para esta columna son de: Constanza Lineros (IG: @adelaidacamachovisual).



**Referencias:**

* [Cronología del estallido social de Chile](<https://www.dw.com/es/la-cronolog%C3%ADa-del-estallido-social-de-chile/a-51407726>)
* [](https://www.dw.com/es/la-cronolog%C3%ADa-del-estallido-social-de-chile/a-51407726)[Información víctimas trauma ocular](https://radio.uchile.cl/2020/08/24/sin-una-reparacion-integral-las-victimas-de-trauma-ocular-a-diez-meses-del-estallido-social/)
* [Ojos arrancados](<https://revistacitrica.com/chile-ojos-arrancados.html>)
* [Entrevista a Gustavo Gatica](<https://www.bbc.com/mundo/noticias-america-latina-50601375>)
* [Entrevista a Fabiola Campillai](*<https://www.ciperchile.cl/2020/01/27/fabiola-campillai-para-mi-no-hay-justicia-tus-ojos-no-puede-haber-nada-que-te-los-devuelva/>*)

<!--EndFragment-->