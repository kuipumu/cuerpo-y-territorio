---
title: 'Morir de Chile'
description: 'En más de una ocasión nuestros planes no se ajustan a lo que deseamos. Esta es una de esas ocasiones. La reciente partida de mi abuela paterna me dirige inevitablemente hacia otro lugar, hacia otras reflexiones.'
cover: 'images/uploads/morir-de-chile.jpg'
caption: 'Fotografía Alexis Moreira. Archivo personal. Enero 2016.'
authors:
  - Alexis Moreira
categories:
  - Cuerpo
tags:
  - Chile
  - Salud
  - Coronavirus
  - Adultos Mayores
  - Salud 
date: 2020-08-10 00:00:00
comments: true
sticky: true
---
En más de una ocasión nuestros planes no se ajustan a lo que deseamos. Esta es una de esas ocasiones. La reciente partida de mi abuela paterna me dirige inevitablemente hacia otro lugar, hacia otras reflexiones. 

Y es que la muerte de un ser amado tiene ese efecto, y si bien la pena ha sido compañera de estos días, lo cierto es que también se me hace necesario hacer eco de aquellos pensamientos que se han suscitado desde su muerte, y que me han generado tal interés que me han llevado a reflexionar de manera más profunda acerca de la vida de las personas adultas mayores en Chile. 
Esto no quiere decir que no sea consciente de su situación, sino que por el contrario, mis ánimos buscan indagar sobre las bases que permiten que esa precaria realidad se mantenga impune, como si la tercera edad fuese el comienzo de una condena hacia la inevitable muerte. 

Maria Uberlinda, la “Tita”, pasó sus últimos días rodeada de su familia. Tuvo ese privilegio que no tienen las más de 460 mil personas adultas mayores que viven solas en Chile. Fue una mujer que vivió casi toda su vida en el sur del país, pasando sus últimos días en su casa de Llanquihue en la que convivió con Alfredo, su esposo por más de 50 años con quien formó una familia de cuatro hijas y tres hijos. 

La recuerdo como una abuela querendona de sus ocho nietas y ocho nietos, de un carácter fuerte y a la vez lúdico, con un desarrollo espiritual superior no solo por la experiencia de su andar, sino también por su afición a la lectura y a sus años de tallerista en grupos comunitarios de la iglesia católica. 
Tenía sus convicciones claras, y estas nunca fueron impedimento para mantener a su familia unida. Y así fue. Lo logró. Sin embargo, más allá de lo relatado bajo la óptica del nieto, quiero pensar en ella como mujer adulta mayor en Chile. Y me interesa reflexionar sobre ello, porque en sus últimos años de vida  la Tita comenzó a estar más callada en las reuniones familiares, a esconder sus preocupaciones. Esto fue, lo que a mi juicio, con el paso del tiempo la enfermó.
 
El pensar y repensar sobre esas preocupaciones me develaron algunos recuerdos. 
Si hay algo que rememoro de manera muy latente y como una constante durante su vida, es que siempre se lamentó por las dificultades de administrar los gastos de casa, rol que cumplió hasta que comenzó a estar bajo el cuidado de sus hijas e hijos.

Y a pesar de que siempre había un plato de comida en su mesa, los aprietos por llegar a fin de mes se sucedieron aún más después de enfermar. Y no es para menos, en Chile enfermarse cuesta caro, más aún si eres adulto mayor. La mayoría de las personas adultas mayores se ven obligadas a endeudarse, no sólo para cubrir sus medicamentos y tratamientos médicos, sino también para cubrir otras necesidades básicas; como alimentación, vivienda, servicios, entre otros. La “Tita” no estuvo ajena a eso.
 
Según datos de Chile Deudas y un estudio efectuado por la Universidad San Sebastián y Equifax, publicado en el 2019, en Chile existen 600 mil adultos mayores endeudados y en promedio, sus deudas triplican los ingresos mensuales obtenidos por ellos (un poco menos de 600 mil pesos chilenos). 

La Tita se murió endeudada, o mejor dicho, se murió por sus deudas. Y entonces de inmediato siento que ella no vivió dignamente. Mucho menos, si en sus últimos años debía viajar periódicamente cerca de 200 kilómetros para ser sometida a tratamientos médicos, porque no existía prestación de servicios de ese tipo en algún recinto público de salud  cercano. 

Y aquí pienso en ese centralismo cruel que se sustenta en Chile, que seguro es una realidad compartida con los demás países latinoamericanos.  Viajó en innumerables ocasiones trasladada con la incondicional compañía de sus hijas e hijos, nietas y nietos. 

Viajó tanto hasta que un día muy fiel a sus formas decidió dejar el tratamiento porque ya estaba cansada. Y con justificación, porque no sólo se cansó de los viajes, sino que se cansó de seguir luchando en ese país llamado Chile. Ese país que la enfermó. Porque tuvo que criar como pudo a sus siete hijos e hijas en plena dictadura cívico militar, y después a gran parte de sus nietas y nietos, en “democracia”. Distintas formas de gobierno, pero el mismo modelo económico. Por momentos más cruel, por momentos más benevolente, menos con las personas adultas mayores. 

Con ellas no existe, ni existió espacio para la benevolencia. Porque si Chile comparte algo con el Covid-19 es su población de riesgo.
 
Por lo anterior es que creo que lamentablemente la Tita, al igual que tantas y tantos, se murió a causa de esta enfermedad llamada Chile. Y morir de Chile no es una muerte digna. Menos para alguien que cumplió siempre las reglas del sistema. Un sistema que además es patriarcal y machista. Le asignó roles. Es mujer. Debe parir, criar, cuidar, limpiar, cocinar, educar y una infinidad de verbos más que cumplir. Y ahora que lo pienso nunca le pregunté si maternar fue una decisión. O si se sintió en completa libertad y autonomía de hacer lo que quiso con su cuerpo y con su vida. O si fue feliz.
  
Nunca trabajó formalmente y aquí nos cruzamos con otra realidad. Ante la insuficiencia de recursos, principalmente por el bajo monto de las pensiones de jubilación, las personas adultas mayores se ven obligadas a trabajar. Y el mercado laboral en lugar de ofrecer empleos dignos, los excluye. Así lo constatan los datos de la Encuesta Nacional de Empleo del Instituto Nacional de Estadísticas (INE) la tasa de ocupación informal más alta se evidencia constantemente en el tramo de 65 años y más (55,1% en el trimestre octubre-diciembre 2019). ¿No es acaso esto violento?
 
La Tita entonces no estuvo sola, pero si olvidada. No por su familia, sino por el Estado. Porque cuando un Estado se encuentra supeditado al mercado, envejecer se convierte en un pecado. Y muchas personas adultas mayores no soportan vivir en pecado. Resulta al menos paradójico para la Tita, quien si hay algo que hizo hasta sus momentos finales fue rezar. Seguramente en más de una ocasión pidió por tener un mejor pasar, vivir en un mejor lugar. Y para sus últimos meses de vida el país salió del letargo. ¿Será que sirvieron sus plegarias? Algo me dice que síi. O al menos eso me gusta creer. Su partida es parte de un momento de inflexión. Después de ella ya no puedes seguir siendo lo mismo. Si ya lo dijo Cerati: “poder decir adiós es crecer.”

> En memoria de mi amada Tita y de todas aquellas personas adultas mayores que, de manera explícita o silenciosa, han sido olvidadas y vulneradas en sus derechos en Chile y Latinoamérica.