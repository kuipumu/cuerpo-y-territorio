---
title: Historias tejidas en los cuerpos y territorios del sur de América (Parte I)
subtitle: " "
cover: /images/uploads/comunidad-mapuche-lof-paynefilu.jpg
caption: "Mural: Julián Roura"
date: 2020-10-10T03:48:15.181Z
authors:
  - Juan Manuel Barreto
tags:
  - EdiciónEspecial
  - 12deoctubre
  - Identidad
  - Latinoamerica
  - Continente
  - Experiencias
  - Viajes
  - América
  - Historia
  - Argentina
  - Pampa
  - Cultura
  - PueblosOriginarios
categories:
  - Territorio
comments: true
sticky: true
---
<!--StartFragment-->

> *`Cuenta el viejo Antonio, que contaron los más viejos`*
>
> *`de los viejos que poblaron estas tierras`*
>
> *`que los más grandes dioses,`*
>
> *`los que nacieron el mundo, no se pensaban parejos todos`*

**“La historia de los otros, Los otros Cuentos"**

 ***Subcomandante Marcos***

> *`La poesía es el hondo susurro`*
>
> *`de los asesinados`*
>
> *`el rumor de hojas en el otoño`*
>
> *`la tristeza por el muchacho`*
>
> *`que conserva la lengua`*
>
> *`pero ha perdido el alma`*
>
> *`La poesía, la poesía, es un gesto`*
>
> *`un sueño, el paisaje`*

**“La llave que nadie ha perdido”**

***Elicura Chihuailaf***

Hacer visible lo invisible. Escuchar lo que se oculta. Hacer conscientes los hilos que nos conectan con otras historias, con otros universos, con memorias albergadas en nuestras tierras y cuerpos.

Ecos de tiempos lejanos que continúan vivos en el presente. Conversaciones y relatos alrededor del fuego; signos, sonidos y fragancias traídos por el viento; señales escondidas esperando ser descifradas.

¿Qué somos? ¿Qué nos define? ¿De dónde venimos y hacia dónde vamos? ¿Qué cantos ancestrales resuenan en nuestras voces, en nuestras narraciones, en nuestras leyendas? Puntos de partida para construir un diálogo con el otrx que es también un nosotrxs que se resiste y nos incomoda.

Nos arrimamos al desafío de contar nuestra América adentrándonos en territorios y exploraciones que nos alejen de las representaciones más habituales y de los sentidos comunes instalados, y nos acerquen a otras formas de acceder al conocimiento, a otras dimensiones de la realidad, a otras maneras de estar disponibles para interpretar lo que nos pasa y lo que sucede a nuestro alrededor.

¿Cómo establecer puentes desde la diversidad de nuestras culturas que no respondan a jerarquías impuestas por una visión etnocentrista del mundo? ¿Cómo indagar en lo que somos reconociendo el mestizaje en nuestra sangre?

Civilización y barbarie, pulcritud y hediondez, blanquidad y morenidad, ¿qué encubren estos binarismos?, ¿dónde habita nuestra identidad? ¿Por qué razones la concepción occidental entiende a la oralidad como una carencia asumiendo inferior toda cultura que no esté sostenida en algún tipo de escritura?

Decir para intervenir, para transformar, para comprometerse. Transparentar para descolonizar, para traer el pasado mientras nos proyectamos hacia el futuro.

> `La historia es un ojo,`
>
> `sumergido en las noches,`
>
> `palabras`
>
> `para no ser dichas`
>
> `sino para mirarnos en ellas`
>
> `como si fueran`
>
> `un espejo roto.`

**“Ese difícil oficio de leer a Encina”**

***Bernardo Colipán***

> `En lenguaje indómito`
>
> `nacen mis versos`
>
> `de la prolongada noche del exterminio`

***“La voz de mi padre”***

***Graciela Huinao***

Cuando la expedición comandada por Cristóbal Colón partió rumbo a lo desconocido con la intención de llegar a la India navegando hacia el poniente, aún se creía que el sol giraba en torno a la Tierra y que nuestro planeta era plano. La aventura hacia el occidente a través del Mar Tenebroso –así se lo llamaba al océano Atlántico- desafiaba el conocimiento que se tenía en la época y por ello fue que cuanto más días pasaron en alta mar, mayor se hizo la incertidumbre y la preocupación entre la tripulación de aquellos barcos que parecían dirigirse a un abismo donde el mundo se acababa. Por lo que dejó registrado el Almirante en su diario, hoy sabemos que el “Descubrimiento” casi se ve frustrado apenas unos días antes por un motín de marineros que se negaba a continuar avanzando.

La imagen de los europeos hundiendo sus botas en la arena blanca de las Bahamas con estandartes, espadas y cruces, es una figura mítica fundadora en el imaginario colectivo de nuestro continente con la que se pretendió transmitir la idea de que el encuentro entre estos dos mundos fue, principalmente, un asunto amable y pacífico. Sin embargo, el recibimiento no fue igual en todos lados y los acontecimientos que se dieron en el Río de la Plata -y en toda el área de lo que posteriormente sería el territorio argentino y zonas aledañas- nos muestran otra cara de cómo fue la reacción de lxs nativxs frente a la llegada de los conquistadores en estas latitudes, 26 años después de aquel episodio en el Caribe.

Al respecto, es mucho lo que se ha escrito acerca de lo que realmente pasó con aquella expedición encabezada por Juan Díaz de Solís que en febrero de 1516, buscando un paso que conectara los dos océanos, ingresó por la desembocadura de un río tan ancho cuyos tripulantes confundieron con un mar y debido a la baja salinidad de sus aguas denominaron Mar Dulce. Lo que se sabe a ciencia cierta es que luego de ingresar por el estuario del río, la expedición se detuvo en una isla para dar entierro a uno de los despenseros de la flota cuyo nombre era Martín García.

Se sabe también que poco después los españoles observaron un grupo de nativxs que desde la costa oriental, a la altura de Punta Gorda, les hizo señas de que se arrimaran para luego emboscarlos y darles muerte. Si efectivamente quienes acompañaron a Díaz de Solís en aquel desembarco fueron asados y comidos luego es un enigma que permanecerá en el tiempo ya que mucho se ha especulado al respecto de la veracidad de los testimonios que llevaron de vuelta a España quienes sobrevivieron a aquel episodio fundacional para la región.

Lo concreto es que la resistencia frente a la avanzada europea se dio desde el primer momento y tanto fue así que los primeros asentamientos cristianos en el territorio fueron totalmente destruidos por timbúes y querandíes, poblaciones nómades que habitaban la pampa desde tiempos remotos junto a varias otras naciones de las cuales hoy solo sabemos pocas cosas.

Y aquí hay un asunto que vale resaltar: si bien la reacción de algunos pueblos originarios fue desde el primer contacto de hostilidad, esto en verdad fue la excepción ya que la norma general en casi todo el continente fue la de recibirlos con hospitalidad en una actitud de diálogo con lo desconocido. En todos los casos, lo que se vio fue que al tiempo de establecer relaciones lxs indixs entendieron la intención y los propósitos que traían los españoles y solo entonces el conflicto se desataría de manera abierta.

La actitud defensiva contra los invasores no se limitó solo a un primer momento y a los pueblos de la pampa, sino que surgieron focos de resistencia a lo largo y a lo ancho de toda América. Asentamientos españoles fundados en territorios indígenas serían sistemáticamente destruidos en Salta, Jujuy, Tucumán, Catamarca, La Rioja, como así también por Chile y Paraguay, obligando a los españoles a mudar de locación sus enclaves más de una vez debido al constante asedio.

La guerra resultó cruenta y los alzamientos de los pueblos que se negaban a ser reducidos en mitas y encomiendas se convirtieron en una constante a lo largo de los siglos. Incluso más: hubo regiones que fueron capaces de resistir a la invasión y que terminaron de ser anexadas, no ya por los españoles, sino por el Estado naciente: el Chaco y la Patagonia.

> Lee la segunda parte de esta entrega haciendo clic [AQUÍ](https://revistacuerpoyterritorio.com/2020/10/10/historias-tejidas-en-los-cuerpos-y-territorios-del-sur-de-america-parte-ii/)

#### ***Otras entregas de esta edición especial "12 de octubre"***

[¡Más que nunca, las venas están abiertas](https://revistacuerpoyterritorio.com/2020/10/10/mas-que-nunca-las-venas-estan-abiertas/)!

[Las doce Malinches o el mito que seremos](https://revistacuerpoyterritorio.com/2020/10/10/las-doce-malinches-o-el-mito-que-seremos/)

#### Además, los invitamos a conectarse a la tertulia de YouTube live este 12 de octubre para conversar, desde otras perspectivas, sobre esta fecha tan importante en América haciendo clic [AQUÍ](https://www.youtube.com/watch?v=Bddgfi3tARU&feature=youtu.be)

![](/images/uploads/12_de_octubre_la_historia_no_contada_de_america_9_.png)