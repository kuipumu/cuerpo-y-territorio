---
title: Ningún lugar en especial
subtitle: Conversación con Autogestores de Buenos Aires
cover: /images/uploads/foto_de_federico_scheuer.jpg
date: 2020-10-16T02:20:00.801Z
authors:
  - Federico Scheuer
tags:
  - Inclusión
  - Cultura
  - DiscapacidadIntelectual
  - Sociedad
  - Educación
  - Igualdad
categories:
  - Diversidad
comments: true
sticky: true
---
<!--StartFragment-->

**Cuerpo y territorio** tuvo la oportunidad y el honor de compartir una conversación con **Autogestores de Buenos Aires,** una agrupación de personas con discapacidad intelectual cuyo objetivo es visibilizar y dar voz al colectivo, realizando diversas acciones e intervenciones en la comunidad. Autogestores de Buenos Aires, asimismo, integra el **Movimiento de Autogestores Argentinos** \[1], una verdadera red que intercomunica y articula a los grupos de las distintas provincias desde 2007, con expectativas y planes de multiplicarse para potenciar un cambio de mirada respecto a la discapacidad.

Parafraseando a les integrantes del grupo, ser autogestor implica escuchar a las otras personas con discapacidad para representarlas y ayudar a cambiarles la realidad, trabajando contra el estigma y la desigualdad.

La autogestión es una forma de militancia que se da en varios países y con diversos enfoques (podemos encontrarla como self-advocacy), pero el caso argentino presenta un fuerte componente social y comunitario que lo distingue de versiones más individualistas que postulan la reivindicación de la persona por parte de sí misma.

 Les Autogestores de Buenos Aires, y el movimiento a escala federal, buscan interpelar y concientizar a sus localidades para volverlas más inclusivas mediante múltiples estrategias. Tarea tan noble como ardua. 

Por mencionar algunas acciones concretas que realizan: dan charlas en universidades y escuelas; colaboran con los gobiernos locales (por ejemplo, traduciendo documentos públicos a un lenguaje sencillo o revisando accesibilidad de páginas web) y les presentan reclamos puntuales; se capacitan continuamente en reuniones semanales para luego capacitar a otres; y realizan investigaciones sobre temas que afectan a las personas con discapacidad (sobreprotección, accesibilidad en medios de transporte, mujer y discapacidad).

El intercambio con Autogestores de Buenos Aires fue extremadamente enriquecedor. Es evidente que se trata de personas que se han constituido como verdaderos líderes y referentes en el campo de la discapacidad y sus voces representan un input insoslayable para pensar e intervenir sobre el mismo. 

Un primer tramo de la conversación se centró en sus experiencias en el mundo del trabajo. La mayoría se encuentra trabajando, además de contar con trayectorias muy interesantes que incluyen pasantías, programas de empleo en el sector público, trabajo protegido y colocaciones en el mercado abierto de trabajo, entre otras formas ocupacionales. Tal como se aprecia, son experiencias dinámicas: vidas donde ha habido acontecimientos, intentos (unos exitosos, otros no tanto), reflexión, y apoyos. 

Es alentador pensar que estas biografías lograron eludir el destino monótono de la vida institucionalizada, el cual (¡insisto!) destruye capital social. Y no es casual: ser autogestor es también una defensa contra las tendencias segregatorias, y un instrumento para desarticular esas lógicas, como veremos a continuación.

> En palabras de Diego Del Castillo: ***“las personas con discapacidad pueden conseguir trabajo, pero todo depende del grado de discapacidad”; a lo cual Camila Alfonso añadiría que “también depende del lugar. Porque hay lugares (por ejemplo, McDonald’s) que tiene que ser una discapacidad marcada (…) Toman gente con una discapacidad, ponele Síndrome de Down, que se nota más la discapacidad que la mía que es mental leve. Y tiene que verse la discapacidad, porque ¡claro! Tiene que verse que incluyen a las personas con discapacidad, y ¡no está bien!, porque yo también tengo una discapacidad como cualquier otro”.***
>
> ***“Nos tienen que poner como si fuera tus pares, no como ‘ah bueno, sos una persona con discapacidad, entonces te incluyo’, ¡para eso ni lo hagas!”*** opinó Verónica Peña, al mismo respecto.

Les pregunté qué lugar deseaban para las personas con discapacidad después de la pandemia. Camila respondió algo muy esclarecedor:

> ***“Ninguno. Porque es como antes. Antes de la pandemia las personas con discapacidad no tienen que tener un lugar, son comunes como cualquiera. O sea, una persona que no tiene discapacidad ¿ocupa un lugar? No. Para mí es así. Las personas con discapacidad son personas que les cuesta algo. A veces, si uno se pone a pensar, hay personas con discapacidad que hacen más cosas o que pueden lograr más cosas que una persona normal que no tiene una discapacidad. Y eso no está visto. Y hay discapacidades que por ejemplo no se tienen en cuenta. La mía, por ejemplo, que es intelectual mental leve, la mía no se tiene mucho en cuenta. Lo toman como que soy una persona normal que no tengo nada. Y por ahí en mi caso me cuesta el estudio mucho, por eso yo no pude hacer una carrera, y me cuesta un montón, y empiezo cursos o cosas que las tengo que dejar porque no… pero las empiezo. Me cuestan un montón y no las puedo seguir. Pero, o sea, como que hay algunas que no están vistas o lo toman como que una persona con discapacidad tiene que tener un lugar. ¿Por qué tiene que tener un lugar? ¡No! Somos personas como cualquiera, somos normales. A algunos nos cuesta más que a otros, o tenemos alguna limitación de no poder hacer alguna cosa, pero yo no lo veo como que tiene que estar en algún lugar (…) pero la gente nos pone en un lugar aunque no queremos. Aunque queremos tratar de que la gente no nos ponga en ese lugar, ellos nos ponen.”***

En el mismo sentido, Caryna Moreno (facilitadora del grupo\[2]) señaló el valor de ***“pasar desapercibido en el montón, ser un común más, una persona que tiene una historia muy similar a la otra, y no que se haga esa diferenciación de especial.”*** Y su colega facilitador, Sebastián González Chiappe, agregó que ***“las personas sin discapacidad no tenemos un lugar en el mundo, lo buscamos nosotros. Las personas con discapacidad deberían tener esa posibilidad también”.***

Me tomé el atrevimiento de combinar estos testimonios para titular el artículo porque comparto el punto de vista, y además me parece una formulación suficientemente clara: ¡no queremos estar en ningún lugar en especial! Esto implica subrayar lo indeseable e injusto de que se adjudique un destino diferencial y estandarizado a las personas por tener tal o cual limitación. No estar en ningún lugar en especial es abrir la posibilidad de estar en todos los lugares comunes (o sea, de la comunidad a la que se pertenece). Pero lejos se está de dicho objetivo.

***“Hay padres que no dejan que los hijos trabajen o que vayan a la escuela, o piensan que no pueden trabajar”***, contaba Federico Rodríguez en ese sentido.

Quienes no tenemos una discapacidad damos por descontado que nadie debería dificultarnos (ni mucho menos prohibirnos) integrar ciertas instituciones comunes, sobre todo en la vida adulta. Tal vez el noviazgo sea un claro ejemplo de ello.

Federico contó cómo en el marco de las charlas que brindan a la comunidad, suelen preguntar ***“si los padres los dejan tener novio o novia a las personas”***. A partir de lo observado, concluyó que ***“no tienen que decidir por nosotros, tenemos que decidir nosotros si queremos tener novio o novia”.***

Relaciones de amistad, de convivencia, sexuales, de compañerismo, de confianza mutua; todas ellas también configuran el capital social al que me refería, y del cual las personas con discapacidad (con mayor intensidad para la discapacidad intelectual) casi no participan. Esto repercute sobre el capital social de todes, empobreciéndolo. Solidaridades que se marchitan sin siquiera florecer.

***“El certificado de discapacidad te da muchas cosas para la salud, la educación, el transporte. Pero el certificado no te hace conocer gente”***\[3] escribía Diego en un brillante artículo publicado en 2012. Humildemente, creo que esta debería pasar a ser una preocupación central en el abordaje de la discapacidad intelectual.

Un fenómeno que preocupa al grupo es la sobreprotección, y han colaborado en notables trabajos de investigación al respecto.

***“Lo que nosotros hacemos cuando vamos a distintos lugares a dar charlas sobre el tema ese, se lo hacemos a los chicos y a los padres después, les hacemos una encuesta (…) Por lo general, los padres dicen que los dejan ir solos a un lugar, por ejemplo a bailar, y los chicos dicen que no, que es mentira. Los padres, por no quedar mal, nos dicen todo que sí, nos mienten”*** comentó Verónica; a lo que Jorge Peláez agregaría, ***“sacamos después el porcentaje de todas las encuestas que hacemos”.***

Tanto las investigaciones como los demás frutos del trabajo de Autogestores se presentan en Congresos y Jornadas en las cuales, según Jorge, ***“las personas con discapacidad están en el mismo nivel que toda la gente”***. La Declaración de Rosario sobre la Autogestión\[4] es un documento ejemplar donde se pone de manifiesto el ideario transformador del Movimiento.

También les pregunté si veían en esta crisis pandémica algún tipo de oportunidad para la inclusión:

***“Tengo varios compañeros que no saben manejar la tecnología y están sin papás también, y ahí hay distinta realidad. Pero nosotros la mayoría sabemos manejar la parte de la tecnología y por eso hay que ayudarlas a las demás personas que no sepan manejarla”***, respondió Federico, poniendo sobre la mesa otro tema de vital importancia. Afortunadamente el grupo continúa sus actividades mediante plataformas digitales desde el comienzo de la cuarentena.

Tal vez el contexto crítico que transitamos nos obligue a pensar sobre las tan mencionadas distancias sociales, fenómeno conocidísimo por las personas con discapacidad. Para muchas de ellas, el aislamiento social y las barreras para establecer el “contacto estrecho” no son cuestiones demasiado novedosas.

***“Las personas con discapacidad vivimos permanentemente en pandemia”*** señaló Diego, con acuerdo de sus compañeres.

*Gracias Autogestores de Buenos Aires por compartir su trabajo e ideas revolucionarias con Cuerpo y Territorio.*

*Autogestores: Camila Alfonso, Diego Del Castillo, Verónica Peña, Jorge Peláez, Federico Rodríguez. Facilitadores: Caryna Moreno, Sebastián González Chiappe.*

- - -

\[1] [Autogestores Argentinos](http://www.autogestoresargentinos.org.ar/)

\[2] Les facilitadores son personas que brindan apoyos y acompañamiento al grupo para fortalecerlo.

\[3] Del Castillo, Diego (2012). “Juntos por una única lucha: una sociedad integrada” en Revista Nuestros Contenidos N°1. Fundación AFIDI y Fundación ITINERIS.

\[4] [Declaración de Rosario](http://www.itineris.org.ar/archivos/biblioteca/DeclaracionDeRosario.pdf)

<!--EndFragment-->