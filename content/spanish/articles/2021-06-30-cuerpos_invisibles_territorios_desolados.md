---
title: Cuerpos invisibles, territorios desolados
subtitle: '"es poco probable que a alguien se le ocurra relacionar a la “loca de
  los gatos” con la crisis ambiental actual"'
cover: /images/uploads/boys-1807545_960_720.jpg
date: 2021-06-30T21:04:32.187Z
authors:
  - Irama La Rosa
tags:
  - Territorio
categories:
  - Territorio
comments: true
sticky: true
---
El debate sobre cambio climático y sus efectos en la vida toda del planeta, es un debate que ha venido creciendo en distintos espacios de la sociedad debido a la emergencia de una pandemia global que interpela directamente los modelos de desarrollo económico industrial que hemos venido apuntalando con el crecimiento urbano actual y sus patrones desmedidos de consumo.

Dentro del conjunto de narrativas que se escuchan en medios de comunicación, redes sociales y espacios comunitarios, encontramos dos palabras muy recurridas una es la palabra animal para explicar el origen del virus, mientras que la otra es el cuidado para convocar medidas de prevención.

Recientemente se presentó en la Asamblea Nacional venezolana un proyecto de Ley sobre Sistemas de Cuidados para la Vida, que viene a sumar dentro de un amplio andamiaje legal, una cantidad de instrumentos sobre protección, dirigidos a distintos actores sociales que por distintas razones se encuentran en situaciones de vulnerabilidad, por ejemplo niños, ancianos, personas sexo-diversas, mujeres y personas con salud disminuida o diversidad funcional.

En la brillante exposición de la diputada María Rosa Jiménez el centro de la propuesta son las personas cuidadoras y desde la perspectiva de género las mujeres como protagonistas del cuidado hacia otros y otras que sufren una condición comprometida de salud o dependencia. En ese contexto hubo una acotación muy importante referida a la inamovilidad laboral de los y las cuidadore(a)as que experimentan distintos tipos de acoso, segregación, persecución y despido por el tiempo que dedican al cuidado y se supone afecta la productividad en sus lugares de trabajo. Evidentemente persiste una concepción patriarcal capitalista y no una visión de la Reproducción Social de la Vida como recurso imprescindible para el desarrollo que representa un valor real en el ámbito económico de la sociedad.

En ese contexto, pudiéramos decir que esta propuesta de Ley es todo un avance en la conquista de un derecho humano fundamental que es el derecho al cuidado, que además viene a reforzar los derechos sobre igualdad de género que fundamentalmente favorecen a las mujeres como las principales sujetas garantes de la reproducción social de la vida.

Ahora bien, existen ciertos elementos que nos parecen medulares en este debate y no lo vemos presentes, uno de ellos tiene que ver con la Sociología del Derecho, que supone ampliar la mirada con la articulación de los distintos instrumentos legales que se están discutiendo actualmente, dirigidos a la protección de todos los sujetos vulnerables que requieren cuidados. Todo ello relevando la necesaria vinculación con territorios concretos para garantizar la aplicabilidad de un sistema integrado de cuidados, que para el caso venezolano, pudiéramos decir que lo representaría otra propuesta de Ley como es la de las Ciudades Comunales.

Lo anterior significa desde una visión interseccional, ecológica y de desarrollo endógeno, que incorporamos un sentido más amplio para la preservación y reproducción social de la vida, al considerar a seres sintientes no humanos y a sus cuidadore(a)s como parte de este sistema de cuidados.

De hecho, dentro de las fragilidades que más perturban la opinión pública actual, por lo menos las que se reseñan en medios y redes sociales, encontramos que son las situaciones de crueldad y maltrato animal como abandono de fauna urbana, comercialización de fauna silvestre, animales de granja para alimentación humana, tauromaquia, toros coleados, peleas de gallo, tracción a sangre, prostitución de simias, pornografía con animales y experimentación en laboratorios, entre otras, las que movilizan mayor diversidad de sensibilidades en contra de tales prácticas, especialmente las de juventudes activistas por el derecho animal.

La difusión de los contenidos sobre estos temas son divulgados por distintas instituciones públicas y no gubernamentales que se dedican al proteccionismo de una fauna mayoritariamente urbana y doméstica, es decir, perros y gatos. Otra fauna silvestre que convive en la ciudad adaptándose a este hábitat como son guacamayas, loros, zarigüeyas, serpientes entre otras especies -incluso más exóticas- reciben atención de otras instituciones que suelen tener un carácter más científico, mientras que otras son más activistas de confrontación, en ambos casos, tienen mayor alcance internacional como son por ejemplo Audubom y Peta Latina, esta última por cierto, muy popular entre activistas que practican el veganismo como práctica filosófica en contra de la violencia que profesa un estilo de vida anti-consumo de alimentos de procedencia animal.

Es importante decir que dentro de todo este universo de personas que se preocupan por la fauna, especialmente la fauna urbana vulnerable, la mayoría SOMOS mujeres quienes nos dedicamos al rescate, cuidado y protección de fauna doméstica en situación de calle y lo hacemos de manera individual, en medio de infinitas precariedades, porque no es una actividad remunerada ni reconocida socialmente.

Aunque no es la idea hacer diferenciación de género -porque cada vez crece el número de hombres cuidadores de fauna doméstica- vemos que en medios y redes sociales, existe cantidad de contenidos que hacen burla de la figura de cuidadoras de animales como “loca de los gatos”. Memes, caricaturas, videos, artículos de prensa, noticias audiovisuales, entre otros contenidos, muestran este estereotipo de mujer despeinada, desarreglada y rostro desvariado rodeada de cantidad de felinos como la representación de los hijos de quien no se ha “realizado” con la maternidad.

En el campo científico, la Psicología también trata el tema como personas con Síndrome de Noé, que es un trastorno psiquiátrico que consiste en la acumulación de animales de compañía (perros o gatos), sin proporcionarle los cuidados necesarios a los animales ni a sí misma(o)s con el auto-cuidado personal y afectando además al entorno de la comunidad por temas relacionados a la higiene colectiva. Las explicaciones sobre este tema recaen exclusivamente en factores individuales de la salud mental de la persona que sufre el trastorno. Problemas asociados a la familia, el abandono, la soledad, el miedo a relacionarse socialmente, pérdidas, duelos o fracasos laborales entre otros, son las razones que buscan explicar el Síndrome de Noé, pocas o ninguna de estas explicaciones se relacionan con las causas sociales de lo que Sigmund Freud describió como el malestar en la cultura.

En ese contexto, es poco probable que a alguien se le ocurra relacionar a la “loca de los gatos” con la crisis ambiental actual y sus causas profundas generadas por las perversiones de un modelo socioeconómico identificado con el crecimiento sostenido de la producción y las magnitudes del consumo desmedido de los recursos, sistema que parafraseando a Marcuse posee una racionalidad irracional que pone de manifiesto su capacidad de convertir lo superfluo en necesario y la destrucción en construcción.

Si tomamos en cuenta la racionalidad antropocéntrica y patriarcal detrás de una sociedad desbordada de consumo, no hay duda que es la sociedad toda la que está enferma de insensibilidad por su nulo sentido de otredad y absolutamente funcional a todo tipo de crueldad instrumental hacia los más frágiles, con lo cual, pudiéramos decir que esta figura de “la loca de los gatos” es un síntoma de un mal mayor.

Muchas desechadas del sistema, son cuerpos invisibles en territorios desolados de toda atención, que luego de ser explotados por el capitalismo salvaje de la modernidad, quedan abandonados en las grietas urbanas y rurales de la pobreza. Son campesino(a)s, indígenas, comunidades afro, trabajadores informales, delincuentes pero fundamentalmente mujeres cuidadoras que son desechadas por la crueldad del sistema patriarcal en calles, manicomios, barrios y montes de cualquier poblado perdido de este inmenso sur.

En los paisajes urbanos no es extraño observar mujeres caminar solitarias con cuencos de comida para alimentar animalitos ferales. En medio de bendiciones de algunos que alaban su desprendida labor y las maldiciones de otros asqueados de su propia animalidad, no existen apoyos comunitarios, refugios u otras políticas públicas reales que compartan tales cuidados ni que cuiden a quienes los ejecutan.

En tiempos de pandemia, comienzan a cobrar fuerza otros paradigmas y formas de interpretar la realidad que dibujan visos de esperanza para formular nuevas políticas de humanidad. Tal como decía nuestro inolvidable Galeano, se trata de una razón sensible que proviene del sentimiento y no de la razón instrumental. Una razón sustentada en la empatía, la solidaridad y sororidad transformadora que postula pedagogías de la ternura contra la crueldad y que reconoce saberes ancestrales donde la humanidad forma parte de la naturaleza, es pachamama, Abya Yala por la comunalidad y el buen vivir.

La razón sensible conforma el sustrato básico de las nuevas tendencias de la política expresadas en lo jurídico, la acción colectiva de los movimientos sociales, la educación popular liberadora y las propuestas de convivencia para otros dentro de los territorios. Configura además desde los enfoques de la reproducción social de la vida una episteme para referenciar el desarrollo de los pueblos a partir de sus luchas por la descolonización, la soberanía agroalimentaria y el bien común.

Lo anterior supone que para conformar un sistema de cuidados en los territorios, tendría que afianzarse el enfoque de reproducción social más que el estrictamente productivo económico. La propuesta de ciudades comunales debería sintetizar en el territorio todo el compendio de leyes de protección social para construirse como ciudades cuidadoras como modelo donde incluso las infraestructuras se reconstruyen y diseñan para atender las vulnerabilidades.

El enfoque de la reproducción de la vida significa que las nuevas ciudades se construyen con derechos y más derechos. Izaskun Chinchilla en su libro “La ciudad de los cuidados” (2020) sostiene que ante las consecuencias del cambio climático y la COVID, se incita a que las y los ciudadano(a)s con diferentes edades, condiciones físicas y mentales; diversas procedencia culturales y costumbres; distintas tendencias sexuales, entre otras muchas disparidades, participen en la redacción de una declaración de derechos que contemple el derecho a la esperanza climática, la movilidad ecológica, derecho a la calidad del aire, acceso equitativo a los recursos naturales, derecho al descanso, derechos de las especies no humanas, derecho al juego, al ocio y al desarrollo cognitivo pleno.

Desde la perspectiva de género que plantean algunas feministas, plantearíamos la necesidad de desfamiliarizar y desfeminizar la política pública, para hacer que el cuidado sea un asunto de toda la sociedad y no exclusivamente de mujeres. Agregaríamos entonces a nuestra declaración para una ciudad cuidadora, el derecho a la ternura como pedagogía de amor para la enseñanza del respeto hacia las múltiples especies que habitamos los espacios urbanos en nuestra diversidad para cuidarnos, entre-cuidarnos como un deber de todos, todas y todes.

### Referencias <!--StartFragment-->

Batthyány, Karina. (Coord.). (2020). Miradas latinoamericanas a los cuidados. Buenos Aires-México DF. CLACSO y Fondo de Cultura Económica.

Marcuse, Herbert. (2016). El hombre Unidimensional. Barcelona-España. Editorial Austral

Chinchilla, Izaskun. (2020). La Ciudad de Los Cuidados. Madrid. Editorial Catarata

Segato, Rita. (2018). Contra Pedagogías de la Crueldad. Buenos Aires. Editorial Prometeo.

Tafalla, Marta. (2019). Ecoanimal. Una estética, plurisensorial, ecologista y animalista. Madrid. Plaza y Valdés.

Verdú ANA; José Tomás García. (2010). La ética animalista y su contribución al desarrollo social. Papeles de relaciones ecosociales y cambio global, Nº 112 2010/11, pp. 13-29, Madrid.

Otras Fuentes:

Carosio, Alba. (2021) Políticas del Cuidado. Con la Vida en el Centro. Conferencia para CIEPAZ. (Video por publicar julio 2021 en youtube)

Contreras, Miguel Ángel. (2020). Entrevista por Marx José Gómez Liendo en IBEROAMÉRICA SOCIAL XV 21/12/2020 Disponible: https://www.researchgate.net/publication/347541582

#### Irama La Rosa

![](/images/uploads/foto_ciudad_feral_1_.jpg)

Socióloga egresada de la Universidad Central de Venezuela, Especialista en Planificación Global de IVEPLAN, Magíster en Política Social del Centro de Estudios del Desarrollo (CENDES-UCV). Actualmente candidata a Doctora en Gestión Social de la Ciencia, Tecnología e Innovación en la Universidad Nacional Experimental Simón Rodríguez. En el campo docente es profesora e investigadora de la Escuela de Sociología de la UCV y asesora de proyectos institucionales y comunitarios a través de la organización Red de la Calle, que es una de las organizaciones que conforman el Consorcio de Investigadoras e Investigadores Eco-animalistas por una cultura de paz (CIEPAZ). Ha publicado artículos e informes alusivos a su línea de investigación sobre jóvenes y espacio público; socialización de las ciencias y eco-animalismo como cultura de paz.