---
title: "  No logré cumplir mi meta, ¿ahora qué hago?"
subtitle: ' "estar siempre lista pero por supuesto, no olvidándome de vivir"'
cover: /images/uploads/no_logre_cumplir_mi_meta_ahora_que_hago.jpg
date: 2021-07-15T14:25:46.121Z
authors:
  - karol Dinamarca
tags:
  - Cultura
  - Mísica
  - Enfuerzo
  - Sueño
  - Vida
  - Meta
  - Estudios
categories:
  - Cultura
comments: true
sticky: true
---
Soy Karol Dinamarca, músicx violinista. A los 23 años participé en un **concurso público** para lograr un puesto de la **Orquesta Sinfónica Nacional de Chile**, y lo logré. Era la primera vez que ganaba un puesto profesional para poder desempeñar la carrera que había estudiado y poder ganarme la vida a través de ella.

**En verdad fui muy feliz** al recibir la noticia de haber ganado, aunque quizás en un principio me mostré un poco incrédula; había perdido el concurso en dos ocasiones anteriores, y cuando participé por **tercera vez** tenía muy claro que había grandes posibilidades de volver a perder. Sin embargo, el mismo día de ese concurso desperté muy feliz, relajada y confiada en mi duro estudio anterior. También estaba **confiada** de que en ese momento ya no podía tocar mejor o peor; me sentía perfectamente coordinada física y mentalmente, me sabía de memoria todo lo que tenía que tocar. ¡Y cómo no, si tuve casi 2 años de preparación para **aprender** un largo repertorio solista y orquestal! (Como punto aparte de esta historia, sigo considerando que las partes orquestales son más difíciles que las obras solistas, pero entrar en detalle es desviar el motivo de la columna.)

Muy pocas veces me he sentido tan **segura** como ese día y creo que no entendí del todo el largo proceso vivido anteriormente porque para mí el tiempo pasó muy rápido y viví todo ese período de forma un poco "express". Tenía solo 23 años y la **juventud pasa rápida y alocadamente**. Por eso, cuando volví a **emprender** otros proyectos, algunos pocos tuvieron **éxito** y otros definitivamente me dejaron una **amarga experiencia**. Se me había olvidado que para conseguir frutos, realmente debía hacer un **grandísimo esfuerzo diario** y constante en el tiempo para conseguirlos. Y una de las cosas más importantes: "saber qué guerras pelear y cuáles no".

Los "imponderables" son imposibles de manejar, pero **todo lo que depende de unx mismx es responsabilidad propia.** Levantarse temprano para ejercitarse, desayunar bien y estudiar, ir a clases, descansar y volver a estudiar es un esfuerzo propio que debe integrarse a los hábitos de casa músico. Pero así como la vida misma, no todo sigue el mismo curso y todo puede cambiar. **A veces uno se pierde** en relaciones amorosas poco sanas que quitan mucho tiempo y energía, vicios de distinto tipo y así una lista larga de factores que probablemente no ayudan mucho a seguir arduamente la disciplina. Así me ocurrió a mi. Después de un largo tiempo estudiando imparablemente, recurrí a un **breve descanso de 2 semanas que se convirtió en años**. El descanso era totalmente necesario, porque los niveles de estrés en este tipo de instancias es altísimo, pero creo que el descanso duró mucho tiempo.

Obviamente, ocurrieron cosas buenas y **logré algunas metas importantes**, pero no volví a estudiar profundamente. Y eso comenzó a acumularse en mi hoja de vida y el poco estudio y esfuerzo, comenzó a **ganar terreno**. Se me olvidó que esta carrera es eterna, y que uno nunca para de estudiar. De pronto, como una luz sobre mi cabeza, la motivación llegó a mi y comencé a intentar concursos que por supuesto, arrojaron negativos resultados; si bien el desempeño no era malo, quedaba al debe en mi perfomance. Pero al menos **me sacó de la parálisis** de esfuerzo y creatividad en la que me encontraba. Me rodeé de **personas que me querían** y me apoyaban. Volví a tener clases con un antiguo maestro, al cual le debo mucho. Siempre ha sido muy paciente conmigo (comencé a estudiar tarde violín, pero eso no fue impedimento para comenzar un entrenamiento con él). Mi pareja me apoyaba en mi nuevo proyecto de volver al estudio intenso y mi familia también.

Durante muchos meses me sentí un poco perdida y muy frustrada por todo el tiempo que "perdí", pero a pesar de todo, mantuve la fe y volví a embarcarme en nuevos proyectos. Pero por supuesto, hubo un proyecto en especial que me tuvo estudiando de sobremanera; aprendí mucho, puse a prueba mis propias facultades de aprendizaje, y por sobre todo mi paciencia, que siempre ha sido poca. Pero por distintos motivos, tomé la decisión de no insistir y retirar mis fuerzas de ese plan. Fue un dolor grande e intenso y me culpé muchísimo. Intenté perdonarme rápidamente, llenarme de trabajo y no vivir de forma sana el luto que me originó ese proyecto fallido. **Por supuesto que la sombra de ese dolor omitido me pasó la cuenta, y enfermé**. Ahí fue cuando tuve que **mirar hacia dentro** y conversar conmigo. Primero, lloré mucho. Después, ir al medico y ver qué ocurría conmigo, hacer exámenes, etc . Luego comprender, entender, hablar con mis cercanos de lo que sentía y recurrir a su apoyo y amor. También fui a quedarme a casa de mi papá, para recibir todo su amor y comprensión (y exquisita comida, debo decirlo)

Hablé profundamente con una amiga íntima, que tiene mucha paciencia y sabiduría de la vida.**¡Qué bien me hizo conversar con ella!** Desde esta columna se lo agradezco infinitamente porque desde ese momento viví mi situación desde otra perspectiva. Me ayudó a sanar y a retomar las ganas de seguir creyendo en mí. Por supuesto hablé con mi profesor y antes de que yo hablara algo, él ya tenía soluciones y apreciaciones de la situación vivida. También se lo agradezco infinitamente.

Y así comencé a sentirme tan bien, que volví a creer en mí, en mis anhelos y sueños y **ahora estudio de forma rigurosa** siempre, para que tarde o temprano, estos se cumplan. Primera regla aprendida, no abandonar el estudio, estar siempre lista pero por supuesto, **no olvidándome de vivir, de amar y de reír mucho.** De equivocarme, de aprender del error y de tomarme todo el tiempo que necesite para vivir el proceso y disfrutar de él. **Ni rápido ni lento, ni mucho ni poco**, sino que insistir y seguir insistiendo y si no queda más opción... INSISTIR.

**Esta columna va dedicada a mis amigos, pareja, familiares, y sobre todo colegas del rubro, que saben muy bien de todos los sentimientos expresados en este escrito.**