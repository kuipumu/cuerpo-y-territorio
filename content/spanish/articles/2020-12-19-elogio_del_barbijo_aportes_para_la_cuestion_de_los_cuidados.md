---
title: Elogio del barbijo, aportes para la cuestión de los cuidados
subtitle: " "
cover: /images/uploads/11-8.jpg
date: 2020-12-19T19:34:30.680Z
authors:
  - Federico Scheuer
tags:
  - Pandemia
  - RedDeCuidados
  - Covid
  - Barbijo
  - Ciudadanos
  - Prevención
  - Sociedad
  - ""
categories:
  - Cuerpo
comments: true
sticky: true
---
**Concluye un año signado por los cuidados. Y la cuestión no es menor.** En Argentina, *“cuidar a quienes nos cuidan”* es una consigna que resonó, como también lo hicieron los aplausos desde las viviendas para honrar al personal de la salud allá por marzo. Luego, el dolor y el hastío tenderían a diluir aquel optimismo inicial.

**Los cuidados se constituyen como una esfera de actividad económica muy sustantiva, altamente heterogénea, e históricamente feminizada e invisibilizada por una mano también invisible.** No obstante, estas valiosísimas (aunque des-preciadas) tareas, vienen ganando cada vez más fuerza como ítem del debate público. Considero que debemos estos avances, entre tantos otros, fundamentalmente al empuje y la solidez del movimiento de mujeres y feminismos.

A mediados de este año, la flamante Mesa Interministerial de Políticas de Cuidado, integrada por doce organismos del Estado Nacional Argentino, produjo un documento titulado *“Hablemos de cuidados”* donde deja plasmado el compromiso de actuar en virtud de “reconocer al cuidado como una necesidad, un trabajo y un derecho (a cuidar y a ser cuidada/o/e)”. **Claro, qué más apropiado que un desastre sanitario mundial para darnos cuenta que todes, de una u otra manera, estamos atravesades por esta cuestión del cuidar.** Brevemente, intentaré aportar algunas ideas en esa dirección.

El 2020 nos pintó un paisaje urbano trastocado. Entre sus rasgos particulares, no pueden dejar de mencionarse los barbijos o tapabocas como signo de un cuidado colectivo. **Estos implementos revisten gran interés como fenómeno cultural, incluso más allá de su función estrictamente sanitaria**. Primero, porque son máscaras.

Parafraseando a Sennett (citado en Bauman, 2002), podemos definir la civilidad como aquella actividad que nos protege mutuamente -a uno del otro, y viceversa, aclaración tan tonta como necesaria- a la vez que nos habilita a disfrutar del estar juntos. Y la poderosísima imagen que utiliza el autor para ilustrar la esencia de la civilidad es la de usar máscaras. **En ese sentido, lo público (y lo público-urbano), la convivencia de una sociedad civil, se construye “tapando” algo del orden de lo individual para posibilitar algo entre todes.**

Sennett se refiere a las circunstancias de poder y los sentimientos privados, que ocultos tras las “máscaras” habilitan una “sociabilidad pura”, no contaminada por cada quien. Creo que los barbijos, en tanto máscaras, actúan en ese sentido. **Son evidencia constante y masiva de que la sociedad sí existe.** Existe, en este caso, sobre nuestras caras, y opera sobre nuestras interacciones para propiciar y simbolizar un cuidado recíproco. Me refiero, claro, al “usar barbijo” más que al barbijo como tal; al gesto más que al objeto. Veo en ese gesto una notable forma de civilidad. No como bunker anti-contagio individual, sino como red horizontal de cuidados y de consideración del otre. **En todo caso, si reviste alguna dimensión individual, es precisamente la de obturar el peligroso spray de uno mismo sobre el prójimo.**

Cabe admitir algo antes de seguir avanzando en tan antipática dirección como la de esta apología del barbijo: las también llamadas mascarillas representan, sin dudas, una molestia. Nos sofocan existencialmente, además de exponernos a nuestro propio y ofensivo aliento. Si pudiésemos, nos las arrancaríamos con pasión en una orgía de caras desnudas y libres; mordiendo, besando, tosiendo, mateando.

**Quizás molestan porque perturban nuestra ilusión de ser seres plenamente autónomos.** Ese pretendido sujeto independiente, como tal, detesta usar barbijo porque no elige usarlo. Y porque entiende que lo reduce a la condición de mero objeto de la relación unidireccional (así la conceptualiza) del cuidado. Según esta narrativa -y aquí me mando sin comillas-, sólo algunas categorías poblacionales, por ser naturalmente vulnerables (acaso inferiores), deberían soportar la humillante atención de un cuidador o cuidadora. El resto, los invulnerables, los completos, nos cuidamos solos.

Claro, esta empobrecida y ombligo-céntrica cosmovisión es incapaz de registrar interdependencias y reciprocidades; de subsumirse a un nosotres que nos cuida; de ver la sociedad como otra cosa que una simple suma de individuos compitiendo, “mostrando los dientes”. Dientes que el barbijo tal vez vino a tapar, al menos por un tiempo.

Hoy se ve bastante gente sin barbijo por las calles de la Ciudad de Buenos Aires. **Me pregunto cuántos se los sacan porque el verano se está sintiendo con creciente intensidad, cuántos por necesidad tras largas horas de llevarlo puesto, y cuántos como declaración de principios.** Dentro del análisis paisajístico de la pandemia, la quema de barbijos en el Obelisco (centro de Bs. As.), por pequeña que fuera, es una imagen muy elocuente. Allí, el barbijo se mete también por las hendijas de la política partidaria.

Volvamos a la conceptualización de los cuidados mediante un ejemplo sectorial, entre tantos que podrían abordarse. Quienes nos desenvolvemos en el campo de la discapacidad (sobre todo discapacidad intelectual) quedamos muchas veces entrampados en estas tendencias individualistas que ven en los cuidados una humillación. El significante “cuidar” causa vergüenza, porque lo concebimos como linealidad unidireccional y cosificadora, en lugar de la concepción reticular que aquí propongo. Varios fantasmas nos asaltan en torno al cuidado: es asistencialismo, es sobreproteger, es generar dependencia, es poca cosa…

Tal vez porque nadie duda que “a les niñes se les debe cuidar”, y porque la infantilización de las personas adultas con discapacidad es un grave problema; concatenamos esas proposiciones de manera tal que nos queda salpicada la palabra “cuidar” de esas otras sustancias problemáticas. **Sabemos que al nivel del lenguaje se dan las más confusas de las batallas.**

En el afán por motivar la “vida independiente” (en parte malinterpretando la filosofía de ese movimiento) intervenimos para que el paciente o usuario se cuide solo en un sospechoso futuro. En esa línea, los cuidados se entienden como instancias penosas e inevitables en el incierto camino a la completitud.

Quisiera poner en valor el trabajo del llamado personal asistencial (sobre todo enfermeras, auxiliares de enfermería y cocineras), que sostiene y da sentido a muchas instituciones cuyo aporte más valioso es el de cuidar. Cualquier mirada crítica sobre la vida institucionalizada de las personas con discapacidad, tal como la que sostiene esta columna de Cuerpo y Territorio, no puede perder de vista nunca la importancia de esos cuidados dignificantes. **Claro, estos no tienen por qué estar centralizados en una burbuja de segregación. Ahí está la trampa. Segregar para “cuidar” vs cuidar para incluir.**

La contracara de lo anterior remite al propio funcionamiento del aparato institucional segregador. Éste tenderá a desvirtuar y distorsionar los cuidados, ya sea sobredimensionando las necesidades de cuidados de sus usuaries y extralimitando las competencias de les cuidadores hasta niveles invasivos y violentos; o bien organizando los “cuidados” (léase vigilancia, adiestramiento) en torno al mantenimiento del día a día institucional. Muchas personas no tienen la oportunidad de definir (o siquiera intentar definir) un proyecto de vida porque se las posterga indefinidamente bajo regímenes terapéuticos donde los “cuidados” se ponen al servicio de esa postergación, a la vez que se los considera el escalafón más bajo del personal, configurando una verdadera cadena de opresión. Esos son, a grandes rasgos, algunos dilemas del cuidar institucional. ¿Qué se está cuidando?...

**Pero el concepto del cuidado que se desprende sutilmente de la experiencia social de la pandemia, permite superar esa visión tan acotada y distorsiva.** Entonces, idealmente, nos cuidamos entre todes (y de todes), reconociéndonos como seres cuidados. Para ese estatus de pertenencia a una comunidad que reconoce y cuida sus cuidados quisiera proponer el término “cuidadanía” como horizonte de sentido. Y ya no en el subsuelo del capitalismo, sino en su más notoria superficie. Y ya no colisionando con un ideal de independencia como “cuidarse solo”, sino como parte fundamental de la vida independiente.

Este reconocimiento opera sin perjuicio de darnos dispositivos asimétricos y focalizados de cuidado, posibles de reformularse, refinarse y evolucionar en virtud de proyectos de desarrollo. La clave es que esas asimetrías (entre cuidadores y cuidados) y focalizaciones (en grupos con mayor o más específica necesidad), no flotan en el vacío, sino que se inscriben en una red que nos contiene a todes como sujetos de derechos. Red que, a todas luces, debemos reconocer, fortalecer y volver inclusiva.

El documento que mencioné más arriba también propone *“reorientar todo lo que el Estado hace en materia de cuidado para que ello aporte a la igualdad de género”*. **Está claro, la reivindicación de los cuidados no debe avanzar sino a condición de reconocerse inmersa en una cultura patriarcal de estereotipos que asigna coercitivamente estas tareas a unos grupos** (sobre todo mujeres e intersecciones mujer-adulta mayor, mujer-pobre, mujer-inmigrante) a la vez que tienden a desligar a otros. Entonces, la cuestión del cuidar debe ensamblarse concienzudamente con la del “decidir cuidar”, estando ésta última supeditada a una redistribución más justa entre sexos, grupos etarios, estratos sociales, y/o al acceso a servicios de cuidados profesionales allí donde sean necesarios.

Resumiendo: una incipiente “cultura de los barbijos” pone de relieve la importancia de cuidarnos de forma colectiva y sugiere una conceptualización reticular del cuidado; y ésta última, por su parte, echa luz sobre la necesidad de fortalecer y sobre todo de equilibrar los componentes del entramado social de los cuidados. Y de definir su sentido en torno al proyecto de las comunidades.

**La imagen de una sociedad que reconoce, regula y remunera su red de cuidados se vuelve más nítida en un contexto de crisis tan sanitaria como económica y social.** En ese sentido, destaco la expresión verbal de la Mesa Interministerial competente en la materia, con la expectativa de que se traduzca en acciones concretas y tendientes a construir cuidadanía.

**Bibliografía**

`1.` Bauman, Z.: Modernidad Líquida. Buenos Aires. Fondo de Cultura Económica. 2002

`2. `Mesa interministerial de Políticas de Cuidado: *[Hablemos de Cuidados. 2020.](<https://www.argentina.gob.ar/sites/default/files/mesa-interministerial-de-politicas-de-cuidado.pdf>)*