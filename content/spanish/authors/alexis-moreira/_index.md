---
title: 'Alexis Moreira'
description: 'Proviene de Calbuco, una pequeña ciudad ubicada en el sur de Chile. Cursó estudios en Ciencias Políticas y Administrativas en la Universidad de Concepción y desde 2018 vive en Buenos Aires.'
cover: 'images/authors/alexis-moreira.jpg'
---
Proviene de Calbuco, una pequeña ciudad ubicada en el sur de Chile. 
Cursó estudios en Ciencias Políticas y Administrativas en la Universidad de Concepción y desde 2018 vive en Buenos Aires, momento desde el cual cursa estudios de Maestría en Políticas Públicas para el Desarrollo con Inclusión Social en la Facultad Latinoamericana de Ciencias Sociales FLACSO.

Posee un marcado interés en la investigación y producción de conocimiento ligado al área de las políticas públicas vinculadas a Derechos Humanos, en particular, en materia de derechos sexuales y reproductivos.
A su vez, posee inquietudes artísticas desarrollándose como DJ (en IG @cuatik0) en fiestas y espacios autogestivos, y ahora se suma al equipo de columnistas de la revista para reflexionar sobre las realidades invisibilizadas en Latinoamérica, y así enriquecer la discusión sobre las temáticas que interpelan al Estado, a la sociedad y a lxs individuxs en la construcción de un mejor lugar en el que vivir. 