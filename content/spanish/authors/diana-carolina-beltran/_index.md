---
title: 'Diana Carolina Beltrán'
description: 'Nació a finales del 90 y disfrutó de su natal Caracas los primeros 26 años de vida. Es periodista, fotógrafa acreditada por el Colegio Nacional de Periodistas de Caracas, viajera, amante de los animales, tía enamorada, feminista.'
cover: 'images/authors/diana-carolina-beltran.jpg'
---
Nació a finales del 90 y disfrutó de su natal Caracas los primeros 26 años de vida. Es periodista, fotógrafa acreditada por el Colegio Nacional de Periodistas de Caracas, viajera, amante de los animales, tía enamorada, feminista  y, como buena caribeña, encuentra la felicidad en una taza de café, el mar, el sabor de la guayaba y el plátano en cualquiera de sus presentaciones.

Ejerció su oficio en varias salas de redacción venezolanas. Como Au Pair dio clases de español en Londres donde adoptó el gusto por el té con la medida justa de leche. Es diplomada en Estrategias de Marketing y Canales Digitales en Colombia y se prepara para comenzar sus estudios de especialización en Dirección Estratégica de Marketing en Argentina. 

Como orgullosa latinoamericana con gusto por la historia, la política y la sociedad, ha decidido darle vida a Cuerpo y Territorio, un espacio digital que reúne a pensadorxs vinculados a diversas disciplinas en diferentes partes del continente para expresar ideas, testimonios, relatos, problemáticas e historias inspiradoras enmarcadas en temáticas sociales que den a conocer, desde una amplia perspectiva, diferentes realidades.

Para la realización de este proyecto ha contado con el apoyo y las sonrisas de su familia, amigos y conocidos, a todos y cada uno de ellos: ¡Gracias!