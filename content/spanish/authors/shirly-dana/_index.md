---
title: 'Shirly Dana'
description: 'Nació en el barrio de Lugano, Ciudad de Buenos Aires, en el otoño de 1991. Su relación con la escritura empezó tan pronto aprendió a leer y a escribir, apasionándola desde el primer momento.'
instagram: 'arteytierratalleres'
cover: 'images/authors/shirly-dana.jpg'
---
Nació en el barrio de Lugano, Ciudad de Buenos Aires, en el otoño de 1991. Su relación con la escritura empezó tan pronto aprendió a leer y a escribir, apasionándola desde el primer momento.
 
Explora los géneros de cuentos cortos, ensayo y poesía. Amplía su mirada viajando por diferentes países y culturas. Además aborda otras áreas de expresión a través de la acrobacia aérea y la guitarra. 
Es feminista y lucha por defender y garantizar derechos humanos y sociales, buscando equilibrar las diferencias de oportunidades.
Es médica y actualmente es residente de medicina general y docente universitaria. 

En el marco de su formación realiza trabajo territorial y comunitario en barrios populares, involucrándose en diversas realidades que volvieron a traer su interés por escribir ficción y crónicas documentales sobre historias de vida invisibilizadas.