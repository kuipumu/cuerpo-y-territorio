---
title: 'Florencia Cencig'
description: 'Licenciada en Trabajo Social, recibida de la Universidad Pública. Mujer argentina, escritora, apasionada de su profesión, y eterna viajera.'
cover: 'images/authors/florencia-cencig.jpg'
---
Licenciada en Trabajo Social, recibida de la Universidad Pública. 
Mujer argentina, escritora, apasionada de su profesión, y eterna viajera. 
Le encanta investigar sobre las culturas latinoamericanas y africanas. 
Amante del mate y de su máquina de escribir. 
Feminista y aficionada de los libros que invitan a construir ideas y pensamientos libres y emancipados.