---
title: 'Paula Fausti'
description: 'Paula es psicóloga con perspectiva de género recibida en la Facultad de Psicología de la Universidad de Buenos Aires.'
instagram: 'arteytierratalleres'
cover: 'images/authors/paula-fausti.jpg'
---
Psicóloga con perspectiva de género recibida en la Facultad de Psicología de la Universidad de Buenos Aires. Desde muy chica tiene dos grandes pasiones: el amor por la naturaleza y el arte. Gracias a ello, los viajes y la música siempre la acompañaron. Hoy trabaja como psicóloga y tallerista en el ámbito educativo y comunitario. En su tiempo libre le gusta andar en bici, leer, tocar la guitarra y cuidar de sus plantas.
