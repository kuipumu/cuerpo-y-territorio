---
title: 'Catalina Dowbley'
description: 'Catalina Dowbley es comunicadora. Estudió Comunicación Social en la Universidad Nacional de La Plata. Colaboró en distintos medios de comunicación, en donde realizó coberturas de temáticas en Derechos Humanos, acceso a derechos y tendencias de actualidad. Los últimos años trabajó en campañas y comunicación política.'
cover: 'images/authors/catalina-dowbley.jpg'
---
Catalina Dowbley es comunicadora. Estudió Comunicación Social en la Universidad Nacional de La Plata. Colaboró en distintos medios de comunicación, en donde realizó coberturas de temáticas en Derechos Humanos, acceso a derechos y tendencias de actualidad. Los últimos años trabajó en campañas y comunicación política. 

En el último tiempo necesitó pelearse un poco con el oficio del periodismo, para volver a encontrarse con él. 

La apasiona poner todo en cuestión desde una perspectiva filosófica, social y humanitaria. Y sobre todo, encontrar la manera de dar lugar a voces que permitan multiplicar las preguntas e intentar crear una salida colectiva a las problemáticas que nos atraviesan en la actualidad. 

Para Cuerpo y territorio trabajará con temas vinculados a la Soberanía Alimentaria, la Justicia Ecológica y Social, y modelos de sustentabilidad desde una perspectiva holística que es, sobre todo, política.  