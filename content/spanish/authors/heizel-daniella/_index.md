---
title: 'Heizel Daniella'
description: 'Es una joven artista venezolana. Leal, sistemática, tranquila y amante del café con leche espumoso por la mañana.'
cover: 'images/authors/heizel-daniella.jpg'
---
Es una joven artista venezolana. Leal, sistemática, tranquila y amante del café con leche espumoso por la mañana. 
Estudia actualmente Diseño de Ilustración ya que siempre se caracterizó por ser una artista apasionada por el diseño y la comunicación. Se desarrolló, más que todo, en el área ilustrativa y fotográfica. Pero, en paralelo a eso, fomenta la escritura, enfocándose en el realismo mágico para contar historias reales y ficticias. 

Desde Argentina, ilustrará temáticas sociales de interés para toda la región como columnista de este espacio.