---
title: 'Rafael Farrera'
description: 'Rafael Farrera es natural del sur de Venezuela y actualmente vive en Santiago de Chile, recibió una formación en Historia de la Universidad Central de Venezuela, pero debido a su vínculo pasional con el cine, laboralmente se desempeña como realizador audiovisual.'
cover: 'images/authors/rafael-farrera.jpg'
instagram: 'cinenserio'
---
Rafael Farrera es natural del sur de Venezuela y actualmente vive en Santiago de Chile, recibió una formación en Historia de la Universidad Central de Venezuela, pero debido a su vínculo pasional con el cine, laboralmente se desempeña como realizador audiovisual.

Sin ánimos de experto comenta sobre películas en la cuenta @cinenserio y ha participado como colaborador en distintos proyectos de índole literaria.

Amante de las letras, pero quizás no tan bueno con ellas, le interesa tratar y explorar, entre otras cosas, lo que el cine puede aportar a la conversación de temas culturalmente arraigados a nuestra sociedad.