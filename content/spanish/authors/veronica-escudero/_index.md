---
title: 'Verónica Escudero'
description: 'Nació en Caracas, Venezuela en 1992 y egresa de la Universidad Central de Venezuela a sus 24 años como Médica Cirujana.'
cover: 'images/authors/veronica-escudero.jpg'
---
Nació en Caracas, Venezuela en 1992 y egresa de la Universidad Central de Venezuela a sus 24 años como Médica Cirujana. Actualmente reside en Buenos Aires, Argentina donde ejerce su profesión como médica general en la Provincia de Buenos Aires, se prepara para iniciar residencia en traumatología y ortopedia.

Es apasionada de las bellas artes. La escritura siempre ha sido un pilar en su vida. Se destaca en otras áreas como coach nutricional, personal trainer e instructora de yoga.
Ella redacta sus ideas en un formato que abarca su realidad en un marco de sensibilidad y emoción, integrando así una crítica sobre el contexto social que la rodea. 