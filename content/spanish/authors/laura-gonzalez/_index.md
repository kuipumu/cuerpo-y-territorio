---
title: 'Laura González Morales'
description: 'Nació hace 24 años en el estado Aragua Venezuela. Es estudiante de Antropología en la Universidad Central de Venezuela y confiesa tener una experiencia romántica con el cacao, sus orígenes y vinculaciones.'
cover: 'images/authors/laura-gonzalez.jpg'
---
Nació hace 24 años en el estado Aragua Venezuela. Es estudiante de Antropología en la Universidad Central de Venezuela y confiesa tener una experiencia romántica con el cacao, sus orígenes y vinculaciones. Este ha sido transversal en su gusto y dedicación por el aprovechamiento de la tierra, el cariño por el medio ambiente, la siembra y la cosecha, más por crecer y disfrutar de una ubicación geográfica históricamente fértil y conocida mundialmente por la calidad única de sus plantaciones.
Es diplomada en Ciencias y Tecnología del Cacao en su misma casa de estudios, lo que hizo que descubriera el amplío e importante mundo que guarda el cacao y la importancia que tiene su divulgación. 