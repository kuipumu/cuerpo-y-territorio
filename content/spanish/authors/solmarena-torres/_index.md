---
title: 'Solmarena Torres'
description: 'Gestora de arte, egresada de la Universidad de Ciencias y Artes de Chiapas, UNICACH). Es especialista en Artes Visuales y Literatura.'
cover: 'images/authors/solmarena-torres.jpg'
---
Gestora de arte, egresada de la Universidad de Ciencias y Artes de Chiapas, UNICACH). Es especialista en Artes Visuales y Literatura. 
Su obra creativa tanto como su gestión aborda temas feministas, digitales, gitch art, post internet. Desde el 2015 es directora de la plataforma de gestión de arte Chiapas Art Project, donde se desarrollan los festivales: Cine Parlante, Metadatos y proyectos de sensibilización creativa con perspectiva de género para niños y niñas. 

Ha publicado en diversas recopilaciones de poesía y revistas académicas de
México. Su obra fotográfica ha sido presentada en diversas exposiciones colectivas en Chiapas. Participado en colectivas feministas de arte en México.
Actualmente es directora del Centro Cultural de Chiapas Jaime Sabines.