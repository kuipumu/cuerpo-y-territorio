---
title: 'Roberto Salazar'
description: 'Venezolano y desde hace más de tres años residente de la ciudad de Buenos Aires. Licenciado en Psicología con especialización en Clínica. '
cover: 'images/authors/roberto-salazar.jpg'
---
Venezolano y desde hace más de tres años residente de la ciudad de Buenos Aires. Licenciado en Psicología con especialización en Clínica. 

Ha ejercido su práctica en ámbitos públicos y privados y ha trabajado con distintas poblaciones en contextos vulnerables. 

También trabaja en educación, con experiencias que van desde nivel inicial y primaria, hasta educación superior. 

Interesado en cultura, arte, tecnología y política, su curiosidad lo lleva acercarse a distintos modos de reflexionar y abordar los espacios que habita. 