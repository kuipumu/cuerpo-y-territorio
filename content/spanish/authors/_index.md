---
title: Somos
---

En el cuerpo se plasman las luchas, las pasiones, las angustias y las resistencias. La simbolización del cuerpo permite analizar(nos), tratar de comprendernos y entender el concepto de territorio como espacio de apropiación y escenario donde hacemos vida y donde se generan conflictos significativos que cambian en el curso del tiempo.

Cuerpo y territorio es una revista digital que reúne a pensadorxs vinculados a diversas disciplinas con el fin de servir como medio para expresar ideas, testimonios, relatos, problemáticas e historias inspiradoras enmarcadas en temáticas sociales que visibilicen realidades y formas de vida.

Creemos en la difusión del pensamiento, la palabra y las artes como una herramienta de desarrollo cultural, donde tiene lugar tanto la mirada colectiva como la individual.