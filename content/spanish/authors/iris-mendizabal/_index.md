---
title: 'Iris Mendizábal'
description: 'Nacida en Argentina, reside actualmente en la ciudad de Junín, en el interior de la provincia de Buenos Aires.'
cover: 'images/authors/iris-mendizabal.jpg'
---
Nacida en Argentina, reside actualmente en la ciudad de Junín, en el interior de la provincia de Buenos Aires. Es estudiante avanzada de Licenciatura en Genética en una universidad pública. 
El objetivo de su desarrollo profesional se encuentra orientado hacia el estudio de la producción sustentable y de la naturaleza.
Enamorada y respetuosa de todas las formas de vida, piensa que es posible el
desarrollo de un futuro ser humano menos antropocéntrico y más amable con el
medioambiente.