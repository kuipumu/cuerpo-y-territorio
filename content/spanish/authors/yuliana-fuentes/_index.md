---
title: 'Yuliana Fuentes Fuguet'
description: 'Valentina Mena Castro nació en Bogotá, esta es sido la ciudad que la ha visto crecer hasta llegar a graduarse como historiadora.'
cover: 'images/authors/yuliana-fuentes-fuguet.jpg'
instagram: 'chuli.cff'
---
Yuliana Fuentes Fuguet. Realizadora Audiovisual. Estudió en La Escuela de Medios y Producción Audiovisual EMPA, Caracas – Venezuela. Actualmente reside en Argentina, escribe la columna de cultura para la Radio Comunitaria FM Raíces Rock de La ciudad de La Plata. Cree en la difusión de trabajos audiovisuales como herramienta educativa y de acercamiento para visibilizar las distintas luchas, conquistas y la cultura de los pueblos latinoamericanos, así como de otras regiones del mundo. Esta densidad emotiva convierte al séptimo arte en una poderosa herramienta para generar cambio en el ser humano.

“Luces, color, movimiento y música... El cine es emoción en estado puro.”
