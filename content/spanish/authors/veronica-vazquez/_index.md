---
title: 'Verónica Vazquez'
description: 'Verónica Vazquez (1980) es docente y socióloga argentina, licenciada
por la Universidad de Buenos Aires UBA. Actualmente está concluyendo la
formación pedagógica para ser profesora.'
cover: 'images/authors/veronica-vazquez.jpg'
---
Verónica Vazquez (1980) es docente y socióloga argentina, licenciada
por la Universidad de Buenos Aires UBA. Actualmente está concluyendo la
formación pedagógica para ser profesora.

En 2006 realizó las prácticas de voluntariado con el Programa
Facultad Abierta, perteneciente a la Secretaría de Extensión de la Facultad de Filosofía y Letras UBA.

En 2008, tuvo el grato honor de ser protagonista del armado y
conformación de la primera red de cooperativas gráficas y empresas
recuperadas por sus trabajadores ERT denominada Red Gráfica. Toda esta
experiencia práctica fue contada en un libro de redacción colectiva “Las
Empresas Recuperadas: autogestión obrera en Argentina y América Latina”
(Editorial de la Facultad de Filosofía y Letras, 2009).

Para fines del 2011 fue incorporada a la empresa recuperada Idelgraff coop de
trabajo en calidad de asociada, para ser luego la primer integrante femenina del
consejo de administración. 

En el 2015 condujo el programa "Recuperadas" en Radio Gráfica FM 89.3 y coordinó publicaciones internas. Suele escribir con el propósito de acortar la distancia entre las palabras y las cosas.

Además es amante del jazz, la literatura y el yoga. Desde su vasta experiencia
ha podido comprobar que lo personal, también es político.