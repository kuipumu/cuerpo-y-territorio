---
title: 'Niyireé S. Baptista S.'
description: 'Historiadora, investigadora y educadora.
Egresada de la Universidad Central de Venezuela.'
cover: 'images/authors/niyiree-s-baptista-s.jpg'
instagram: 'niyiree_baptista'
---
(Caracas-Venezuela, 1990). Historiadora, investigadora y educadora.
Egresada de la Universidad Central de Venezuela. Realiza estudios de maestría en literatura latinoamericana en la Universidad Simón Bolívar (Miranda-Venezuela). Su primer cuento, “Ella”, fue publicado en 2019 en el libro Cuentos de mujeres para mujeres (Editorial Mestiza, Valparaíso- Chile). Ha realizado diversas investigaciones en el área de feminismos y género. Su principal línea de investigación se centra en el cuerpo, la maternidad y las sexualidades. Ha participado en diferentes ponencias en la que destaca su publicación “Maternidad, una cuestión de poder” en la Revista Venezolana de Estudios de la Mujer y Consejo Latinoamericano de Ciencias Sociales
(2019). Actualmente se dedica a la docencia como oficio y a la escritura como pulsión de vida.