---
title: 'Micaela Pereira'
description: 'De Buenos Aires, Argentina. Apasionada por todas las artes.
Estudiante de Arquitectura en la Facultad de Arquitectura, Diseño y Urbanismo FADU de la Universidad de Buenos Aires.'
cover: 'images/authors/mica-pereira.jpg'
---
De Buenos Aires, Argentina. Apasionada por todas las artes.
Estudiante de Arquitectura en la Facultad de Arquitectura, Diseño y Urbanismo FADU de la Universidad de Buenos Aires. Lleva varios años desenvolviéndose entre la fotografía y las artes plásticas, entre el dibujo y la pintura, entre otras cosas.
Viene a contarnos e invitarnos, a través de las imágenes, historias, críticas y realidades desde una mirada realista y poética. 