---
title: 'Karol Dinamarca'
description: 'Tiene 31 años, nació en Valparaíso y reside actualmente en Santiago de Chile.
Ella es músico violinista de la Orquesta Sinfónica Nacional de Chile, cumpliendo su labor de violín 2 tutti en esa institución musical.'
cover: 'images/authors/karol-dinamarca.jpg'
---
Tiene 31 años, nació en Valparaíso y reside actualmente en Santiago de Chile.
Ella es músico violinista de la Orquesta Sinfónica Nacional de Chile, cumpliendo su labor de violín 2 tutti en esa institución musical.

Su relación con la música comenzó a construirse a los 12 años, en una orquesta infantil en Valparaíso, desenvolviéndose en años posteriores como músico becada por diversas orquestas e instituciones culturales de su país.

Comenzó sus estudios formales en la Facultad de Artes de la Universidad de Chile, y profundizó sus conocimientos de técnica e interpretación con el Maestro Violinista chileno Sergio Prieto, ex concertino de múltiples orquestas en el extranjero y también de su propio país.

Ella es una amante de la Música docta, sinfónica y ópera.

Karol abordará temáticas artísticas y culturales, las cuales compartirá en esta revista.