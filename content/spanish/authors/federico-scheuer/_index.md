---
title: 'Federico Scheuer'
description: 'De la ciudad de Buenos Aires, Argentina. Es estudiante de Políticas Sociales en la Universidad Nacional de Tres de Febrero.'
cover: 'images/authors/federico-scheuer.jpg'
---
De la ciudad de Buenos Aires, Argentina. Es estudiante de Políticas Sociales en la Universidad Nacional de Tres de Febrero y se desempeña en el campo de la discapacidad, participando en diversas organizaciones y capacitándose en dicha temática desde 2015.