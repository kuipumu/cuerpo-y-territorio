---
title: 'Mercedes Estruch'
description: 'Tiene 27 años, vive en la ciudad de Mar del plata, Provincia de Buenos Aires. Es Feminista y activista de la diversidad corporal.'
cover: 'images/authors/mercedes-estruch.jpg'
---
Tiene 27 años, vive en la ciudad de Mar del plata, Provincia de Buenos Aires. Es Feminista y activista de la diversidad corporal. Es diplomada en Géneros, Políticas y participación de la Universidad Nacional General Sarmiento UNGS.
 
Actualmente integra la ONG AnyBody Argentina y se encuentra terminando las carreras de profesorado y licenciatura en Sociología en la Universdiad Nacional de Mar del Plata UNMDP. En la misma casa de estudios se desarrolla como Becaria del Centro de Extensión Universitaria  CEU en " Pueblo Camet" y como tallerista de Educación Sexual Integral dentro de l"Programa de género y acción comunitaria" en la Facultad de Ciencias de la Salud y Servicio Social FCSySS.