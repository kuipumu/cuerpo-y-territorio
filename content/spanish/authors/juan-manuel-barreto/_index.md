---
title: 'Juan Manuel Barreto'
description: 'Nació en el barrio de Quilmes, provincia de Buenos Aires, pero el amor por Independiente lo unió de por vida a Avellaneda.'
cover: 'images/authors/juan-manuel-barreto.jpg'
website: 'https://temporadaalaintemperie.wordpress.com.'
---
Nació en el barrio de Quilmes, provincia de Buenos Aires, pero el amor por Independiente lo unió de por vida a Avellaneda. Es Licenciado en Relaciones Internacionales y Docente. Ejerce la docencia en secundarias públicas de la Ciudad de Buenos Aires y se prepara para cursar la Licenciatura en Educación. Apasionado de la literatura, se niega a considerar al policial negro como categoría menor. “Quita el velo de lo que intentamos esconder bajo la alfombra”, afirma. 
Escritor en proceso, no se decide entre los relatos de ficción y los ensayos. Desde un tiempo a esta parte, comparte sus reflexiones y cuentos en su blog [temporadaalaintemperie](https://temporadaalaintemperie.wordpress.com).