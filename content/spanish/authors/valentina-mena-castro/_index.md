---
title: 'Valentina Mena Castro'
description: 'Valentina Mena Castro nació en Bogotá, esta es sido la ciudad que la ha visto crecer hasta llegar a graduarse como historiadora.'
cover: 'images/authors/valentina-mena-castro.jpg'
instagram: 'Valemenacastro'
---
Valentina Mena Castro nació en Bogotá, esta es sido la ciudad que la ha visto crecer hasta llegar a graduarse como historiadora. Tiene 23 años y  le encanta la música, esta iba a ser su carrera principal ya que estudióviolín desde la tierna edad de 6 años, sin embargo, la historia siempre estuvo presente en su vida y fue su decisión al final.

Dentro de sus pasiones se cuentan viajar, caminar por su ciudad y estudiar fenómenos que a veces no parecen propios de la historia, ya que la interdisciplinariedad es fundamental para ella. Dentro de sus temas de interés para escribir están su natal Colombia, las relaciones históricas de larga duración, los videojuegos, el feminismo, la tecnología y la música.
