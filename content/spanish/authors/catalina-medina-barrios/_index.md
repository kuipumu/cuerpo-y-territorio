---
title: 'Catalina Medina Barrios'
description: 'Bogotana, edición de los 90`s, relacionista, networker & creadora de comunidades.'
cover: 'images/authors/catalina-media-barrios.jpg'
---
Bogotana, edición de los 90`s relacionista, networker y creadora de comunidades.
Cree en el poder de la autenticidad e individualidad como herramienta principal para la construcción de comunidades sostenibles.
Estudió Administración de Empresas, ha trabajado en el sector social y creativo en emprendimientos, startups y ONGs en las áreas de mercadeo, comunicación, comercial y alianzas en Colombia y LATAM, además lidera la creación de comunidades en espacios colaborativos y movimientos globales como Fuckup Nights en Bogotá y Pechakucha Nights Bogotá. 