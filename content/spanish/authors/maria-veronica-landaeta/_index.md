---
title: 'María Verónica Landaeta'
description: 'Vero Landaeta nació en Caracas y es actriz desde los 14 años. Está radicada en Buenos Aires desde 2018. Es tesista de Antropología por la Universidad Central de Venezuela. Su investigación es sobre los procesos performáticos en contextos de conflictividad política. Intenso, ¿eh?'
instagram: 'arteytierratalleres'
cover: 'images/authors/maria-veronica-landaeta.jpg'
---
Vero Landaeta nació en Caracas y es actriz desde los 14 años. Está radicada en Buenos Aires desde 2018.

Es tesista de Antropología por la Universidad Central de Venezuela. Su investigación es sobre los procesos performáticos en contextos de conflictividad política. Intenso, ¿eh?

No se dejen llevar por eso: ama la cerveza, baila salsa y todo lo que active su ritmo Caribe. También hace yoga y compagina la vocación escénica con su vocación social: el ejercicio artístico es su principal militancia.

Acá va a escribir sobre temas vinculados con la cultura, el arte y la identidad, siempre con el compromiso de recomendarnos música y lugares para ir a bailar en la post-pandemia. 