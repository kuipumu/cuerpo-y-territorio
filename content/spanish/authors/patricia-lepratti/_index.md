---
title: 'Patricia Lepratti'
description: 'Antropóloga social, diplomada en Gestión y Control de Políticas Públicas en la Facultad Latinoamericana de Ciencias Sociales FLACSO y Magister en Ciencias Humanas en la Universidad de la República de Uruguay UDELAR.'
cover: 'images/authors/patricia-lepratti.jpg'
---
Antropóloga social, diplomada en Gestión y Control de Políticas Públicas en la Facultad Latinoamericana de Ciencias Sociales FLACSO y Magister en Ciencias Humanas en la Universidad de la República de Uruguay UDELAR.

Actualmente se encuentra realizando su formación doctoral en Ciencias Sociales en el Instituto de Desarrollo Económico y Social de la Universidad Nacional General Sarmiento UNGS-IDES.
A lo largo de su trayectoria académica, ha abordado temas relacionados a tres ejes fundamentales: movilidad humana, ciudadanía y gobernanza. 

Específicamente, desde el año 2012, investiga temas relativos a los trabajadores del mar como sujetos transnacionales en un mercado de trabajo global.  