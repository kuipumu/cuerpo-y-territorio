---
title: 'Jefersson Leal'
description: Es un joven caraqueño apasionado de la historia, la enseñanza y la fotografía. Ha cursado estudios sobre Historia de América en la Universidad Central de Venezuela.'
cover: 'images/authors/jeffersson-leal.jpg'
instagram: "lealphotove"
---
Es un joven caraqueño apasionado de la historia, la enseñanza y la fotografía. Ha cursado estudios sobre Historia de América en la Universidad Central de Venezuela, es facilitador en temas de género y equidad. Actualmente se desempeña como profesor de Historia. 