const webpack = require("webpack")
const path = require('path')
const TerserJSPlugin = require('terser-webpack-plugin')

module.exports = {
  mode: 'development',

  entry: {
    admin: ['./assets/scripts/src/admin.js']
  },

  module: {
    rules: [
    ]
  },

  resolve: {
    modules: ['node_modules', './'],
  },

  plugins: [
  ],

  optimization: {
    minimizer: [new TerserJSPlugin({})],
  },

  output: {
    filename: "scripts/dist/[name].js",
    path: path.resolve(__dirname, "assets/"),
  },
}